// Pimp System Clientside System


include("sh_pimpsystem.lua")

surface.CreateFont("pimpMenuHeader", {
    font = "Roboto",
    size = 18,
    weight = 500
})

surface.CreateFont("pimpMenuSubHeader", {
    font = "Roboto",
    size = 14,
    weight = 500
})

pimp.clientPrice = pimp.config.defaultPrice
pimp.pimpTimes = 0

local col = Color(pimp.config.color.r, pimp.config.color.g, pimp.config.color.b, 190)
local acol = Color(pimp.config.color.r, pimp.config.color.g, pimp.config.color.b, 120)

function pimp.loadTimes()
    if (file.Exists("pimptimes.txt", "DATA")) then
        pimp.pimpTimes = tonumber(file.Read("pimptimes.txt", "DATA"))
    end
end

function pimp.saveTimes()
    file.Write("pimptimes.txt", tostring(pimp.pimpTimes))
end



function pimp.changePimpPrompt()
	if backPanel then backPanel:Close() end
	local bg = vgui.Create( "DFrame" )
		bg:SetSize( 500, 150 )
		bg:Center()
		bg:MakePopup()
		bg:SetTitle( "" )
		bg:ShowCloseButton( false )
		bg.Init = function(self)
			self.startTime = SysTime()
		end
		bg.Paint = function( self, w, h )
			Derma_DrawBackgroundBlur( self, self.startTime )
			
			draw.RoundedBox( 0, 0, 0, w, h, Color(20, 20, 20, 200) )
			
			--draw.SimpleText( "Prostitute - Change Pimp", "pimpMenuHeader", 20, 15, Color( 255, 255, 255, 255 ) )
			draw.SimpleText( "Who do you want to set as your Pimp?", "pimpMenuSubHeader", w / 2, 20, Color( 167, 167, 167, 255 ), TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Note: Your existing Pimp will be notified of this change.", "pimpMenuSubHeader", w / 2, 70, Color( 167, 167, 167, 240 ), TEXT_ALIGN_CENTER )
			
			surface.SetDrawColor( Color( 255, 255, 255, 255 ) )
		end
		textOpen = true
		
		local cl = vgui.Create( "DButton", bg )
		cl:SetSize( 30, 20 )
		cl:SetPos( bg:GetWide() - 30, 0 )
		cl:SetText( "X" )
		cl:SetFont( "pimpMenuSubHeader" )
		cl:SetTextColor( Color( 255, 255, 255, 255 ) )
		cl.Paint = function( self, w, h )
			local kcol
			if self.hover then
				kcol = col
			else
				kcol = acol
			end
			draw.RoundedBoxEx( 0, 0, 0, w, h, acol, false, false, true, true )
			draw.RoundedBoxEx( 0, 1, 0, w - 2, h - 1, kcol, false, false, true, true )
		end
		cl.DoClick = function()
			bg:Close()
			textOpen = false
		end
		cl.OnCursorEntered = function( self )
			self.hover = true
		end
		cl.OnCursorExited = function( self )
			self.hover = false
		end
		
		local hl = vgui.Create( "DComboBox", bg)
		hl:SetPos(bg:GetWide() / 2 - 100, 40)
		hl:SetSize( 200, 20 )
		
		hl:AddChoice( "No One", -1 )
		for k,v in pairs(player.GetAll()) do
			if pimp.isPimp(v) then
				hl:AddChoice( v:Name(), v:UniqueID() )
			end
		end
		
		local target --Should hold a UID
		hl.OnSelect = function( panel, index, value, data )
			target = data
		end
		
		local ybut = vgui.Create( "DButton", bg )
		ybut:SetSize( bg:GetWide() / 2 - 20, 30 )
		ybut:SetPos( bg:GetWide() / 2 - 20, bg:GetTall() - 40 )
		ybut:SetText( "Change Pimp" )
		ybut:SetFont( "pimpMenuHeader" )
		ybut:SetTextColor( Color(255,255,255) )
		ybut.Paint = function( self, w, h )
			local gcol
			if self.hover then
				gcol = col
			else
				gcol = acol
			end
			draw.RoundedBox( 0, 0, 0, w, h, col )
			draw.RoundedBox( 0, 1, 1, w - 2, h - 2, gcol )
		end
		ybut.DoClick = function()
			if target == -1 then
				RunConsoleCommand( "pimp_unsetpimp" )
			elseif IsValid(pimp.getPimp(LocalPlayer())) and target==pimp.getPimp(LocalPlayer()):UniqueID() then
				pimp.notify(LocalPlayer(), pimp.getPimp(LocalPlayer()):Nick().." is already your pimp!")
			elseif target then
				RunConsoleCommand( "pimp_setpimp", target )
			end
			bg:Close()
			textOpen = false
		end
		ybut.OnCursorEntered = function( self )
			self.hover = true
		end
		ybut.OnCursorExited = function( self )
			self.hover = false
		end
end
	
function pimp.openMenu()
    local backPanel = vgui.Create("DFrame")
        backPanel:SetSize(400, 290)
        backPanel:SetTitle("TTT-FUN - Prostitute Menu")
        backPanel:MakePopup()
        backPanel:Center()
		
        backPanel.Paint = function(self)
            surface.SetDrawColor(Color(20, 20, 20, 200))
            surface.DrawRect(0, 0, self:GetWide(), self:GetTall())

            surface.SetDrawColor(col)
            surface.DrawRect(0, 40, self:GetWide(), 40)

            surface.SetTextColor(245, 245, 245, 255)
            surface.SetFont("pimpMenuHeader")

            local text = "You have no pimp, you get all profits"
			
			local pmp = pimp.getPimp( LocalPlayer() )
            if IsValid(pmp) then
                text = "Your pimp is ".. pmp:Nick() .. ", their cut is ".. pimp.GetCut(pmp) .. "%."
            end

            local w,h = surface.GetTextSize(text)
            surface.SetTextPos(self:GetWide() /2 - w/2, 40 + (20 - h/2))

            surface.DrawText(text)

			----
            surface.SetDrawColor(col)
            surface.DrawRect(10, 135, 200, 30)

            local text = "Your price is: $" .. (pimp.clientPrice or 200)
            local w,h = surface.GetTextSize(text)

            surface.SetTextPos(10 + (100 - w/2), 135 + (15 - h/2))
            surface.DrawText(text)
			
			----
            surface.DrawRect(10, 180, self:GetWide() - 20, 35)

            local text = "You've earned $" .. (pimp.totalEarnt or 0) .." so far."
            local w,h = surface.GetTextSize(text)

            surface.SetTextPos(10 + ((self:GetWide() - 20)/2 - w/2), 180 + (35/2 - h/2))
            surface.DrawText(text)

			----
            surface.DrawRect(10, 220, self:GetWide() - 20, 35)

            local text = "You've given $" .. (pimp.pimpTotal or 0) .." to the pimp."
            local w,h = surface.GetTextSize(text)

            surface.SetTextPos(10 + ((self:GetWide() - 20)/2 - w/2), 220 + (35/2 - h/2))
            surface.DrawText(text)
        end

    local changePrice = vgui.Create("DButton", backPanel)
        changePrice:SetPos(220, 135)
        changePrice:SetSize(backPanel:GetWide() - 230, 30)
        changePrice:SetText(" ")

        changePrice.Paint = function(self)
            local color = acol
            if (self.Hovered) then
                color = col
            end

            surface.SetDrawColor(color)
            surface.DrawRect(0, 0, self:GetSize())

            surface.SetFont("pimpMenuHeader")
            surface.SetTextColor(245, 245, 245, 255)

            local text = "Change Price..."
            local w,h = surface.GetTextSize(text)

            surface.SetTextPos(self:GetWide() /2 - w/2, self:GetTall()/2 - h/2)
            surface.DrawText(text)
        end

        changePrice.DoClick = function()
			local pnl = vgui.Create( "DFrame" )
			pnl:SetSize( 400, 160 )
			pnl:SetPos( (ScrW()/2)-200, (ScrH()/2)-75 )
			pnl:SetDraggable( false )
			pnl:SetTitle( "Prostitute - Set Price" )
			pnl:MakePopup()
			--pnl:ShowCloseButton( false )
			pnl.stime = CurTime()
			pnl.Paint = function( s,w,h )
				Derma_DrawBackgroundBlur( pnl, pnl.stime )
				
				surface.SetDrawColor(Color(20, 20, 20, 200))
				surface.DrawRect(0, 0, s:GetWide(), s:GetTall())
				
				surface.SetDrawColor(col)
				surface.DrawRect(0, 40, s:GetWide(), 40)
				
				surface.SetTextColor(245, 245, 245, 255)
				surface.SetFont("pimpMenuHeader")

				local text = "Set your price"
				
				local w,h = surface.GetTextSize(text)
				surface.SetTextPos(s:GetWide()/2 - w/2, 40 + (20 - h/2))
				surface.DrawText(text)
			end
			
			local entry = vgui.Create( "DTextEntry", pnl )
			entry:SetPos( 10, 90 )
			entry:SetSize( 380, 20 )
			entry:SetNumeric( true )
			
			local set = vgui.Create("DButton", pnl)
			set:SetPos(10, 120)
			set:SetSize(pnl:GetWide()/2 - 20, 30)
			set:SetText(" ")
			
			set.Paint = function(self)
				local color = acol
				if (self.Hovered) then color = col end
				
				surface.SetDrawColor(color)
				surface.DrawRect(0, 0, self:GetSize())
				
				surface.SetFont("pimpMenuHeader")
				surface.SetTextColor(245, 245, 245, 255)
				
				local text = "Set price"
				local w,h = surface.GetTextSize(text)
				surface.SetTextPos(self:GetWide() /2 - w/2, self:GetTall()/2 - h/2)
				surface.DrawText(text)
			end
			set.DoClick = function( s )
				local v = tonumber( entry:GetValue() )
				if type(v)~="number" then pimp.notify(LocalPlayer(), "Invalid number!") return end
				
				LocalPlayer():ConCommand("pimp_setprice " .. tostring(v))
				pnl:Close()
			end
			
			local cancel = vgui.Create("DButton", pnl)
			cancel:SetPos( (pnl:GetWide()/2) + 10, 120)
			cancel:SetSize(pnl:GetWide()/2 - 20, 30)
			cancel:SetText(" ")
			
			cancel.Paint = function(self)
				local color = acol
				if (self.Hovered) then color = col end
				
				surface.SetDrawColor(color)
				surface.DrawRect(0, 0, self:GetSize())
				
				surface.SetFont("pimpMenuHeader")
				surface.SetTextColor(245, 245, 245, 255)
				
				local text = "Cancel"
				local w,h = surface.GetTextSize(text)
				surface.SetTextPos(self:GetWide() /2 - w/2, self:GetTall()/2 - h/2)
				surface.DrawText(text)
			end
			cancel.DoClick = function( s )
				pnl:Close()
			end
		end

    local changePimp = vgui.Create("DButton", backPanel)
        changePimp:SetPos(10, 90)
        changePimp:SetSize(backPanel:GetWide() - 20, 30)
        changePimp:SetText(" ")

        changePimp.Paint = function(self)
            local color = acol
            if (self.Hovered) then
                color = col
            end

            surface.SetDrawColor(color)
            surface.DrawRect(0, 0, self:GetSize())

            surface.SetFont("pimpMenuHeader")
            surface.SetTextColor(245, 245, 245, 255)

            local text = "Set your Pimp as..."

            local w,h = surface.GetTextSize(text)

            surface.SetTextPos(self:GetWide() /2 - w/2, self:GetTall()/2 - h/2)
            surface.DrawText(text)
        end

        changePimp.DoClick = function()
            pimp.changePimpPrompt()
			backPanel:Close()
        end
end

local pimpPanels = {}

function pimp.openRequest(ply, price, id)
    for k, v in pairs(pimpPanels) do
        if !ValidPanel(v) then
            table.remove(pimpPanels, k)
        end
    end

    if IsValid(ply) and pimp.isProstitute(ply) then
        surface.PlaySound("common/warning.wav")
        local col = Color(pimp.config.color.r, pimp.config.color.g, pimp.config.color.b, 190)
        local acol = Color(pimp.config.color.r, pimp.config.color.g, pimp.config.color.b, 120)

        local backPanel = vgui.Create("DFrame")
            backPanel:SetSize(300, 150)
            backPanel:SetPos(10 + (310 * #pimpPanels), 200)
            backPanel:SetTitle("Server Name")
            backPanel:ShowCloseButton(false)

            backPanel.Paint = function(self)
                if (!IsValid(ply)) then
                    backPanel:Close()
                    return
                end

                surface.SetDrawColor(20, 20, 20, 200)
                surface.DrawRect(0, 0, self:GetSize())

                surface.SetTextColor(col)
                if (string.len(ply:Nick()) <= 10) then
                    surface.SetFont("pimpMenuHeader")
                else
                    surface.SetFont("pimpMenuSubHeader")
                end

                local text = ply:Nick() .. " asks you if you want some \"fun\""

                local w,h = surface.GetTextSize(text)
                surface.SetTextPos(self:GetWide()/2 - w/2, 30)
                surface.DrawText(text)
            end

            local ppid = #pimpPanels + 1;
            table.insert(pimpPanels, backPanel)

        local accept = vgui.Create("DButton", backPanel)
            accept:SetPos(10, 60)
            accept:SetSize(backPanel:GetWide() - 20, 30)
            accept:SetText(" ")

            accept.Paint = function(self)
                local color = acol
                if (self.Hovered) then
                    color = col
                end

                surface.SetDrawColor(color)
                surface.DrawRect(0, 0, self:GetSize())

                surface.SetFont("pimpMenuHeader")
                surface.SetTextColor(245, 245, 245, 255)

                local text = "Accept Request ($" .. price .. ")"
                local w,h = surface.GetTextSize(text)

                surface.SetTextPos(self:GetWide()/2 - w/2, self:GetTall()/2 - h/2)
                surface.DrawText(text)
            end

            accept.DoClick = function()
                if (ply:GetPos():Distance(LocalPlayer():GetPos()) > 200) then
                    pimp.notify(LocalPlayer(), "You need to be closer to accept the request!")
                    return
                end
                LocalPlayer():ConCommand("pimp_acceptrequest " .. id)
                if (pimpPanels[ppid]) then
                    table.remove(pimpPanels, ppid)
                end
                backPanel:Close()

                gui.EnableScreenClicker(false)
            end

        local deny = vgui.Create("DButton", backPanel)
            deny:SetPos(10, 100)
            deny:SetSize(backPanel:GetWide() - 20, 30)
            deny:SetText(" ")

            deny.Paint = function(self)
                local color = acol
                if (self.Hovered) then
                    color = col
                end

                surface.SetDrawColor(color)
                surface.DrawRect(0, 0, self:GetSize())

                surface.SetFont("pimpMenuHeader")
                surface.SetTextColor(245, 245, 245, 255)

                local text = "Deny Request"
                local w,h = surface.GetTextSize(text)

                surface.SetTextPos(self:GetWide()/2 - w/2, self:GetTall()/2 - h/2)
                surface.DrawText(text)
            end

            deny.DoClick = function()
                if (pimpPanels[ppid]) then
                    table.remove(pimpPanels, ppid)
                end
                backPanel:Close()

                gui.EnableScreenClicker(false)
            end


        timer.Simple(38, function()
            if ValidPanel(backPanel) then
                backPanel:Remove()
            end
        end)
    end
end

function pimp.pimpRequest(ply, id)
    for k, v in pairs(pimpPanels) do
        if !ValidPanel(v) then table.remove(pimpPanels, k) end
    end

    if pimp.isProstitute(ply) and pimp.isPimp(LocalPlayer()) then
        surface.PlaySound("common/warning.wav")
        local col = Color(pimp.config.color.r, pimp.config.color.g, pimp.config.color.b, 190)
        local acol = Color(pimp.config.color.r, pimp.config.color.g, pimp.config.color.b, 120)

        local backPanel = vgui.Create("DFrame")
            backPanel:SetSize(300, 150)
            backPanel:SetPos(10 + (310 * #pimpPanels), 200)
            backPanel:SetTitle("Server Name")
            backPanel:ShowCloseButton(false)

            backPanel.Paint = function(self)
                if (!IsValid(ply)) then backPanel:Close() return end

                surface.SetDrawColor(20, 20, 20, 200)
                surface.DrawRect(0, 0, self:GetSize())

                surface.SetTextColor(col)
                if (string.len(ply:Nick()) <= 10) then
                    surface.SetFont("pimpMenuHeader")
                else
                    surface.SetFont("pimpMenuSubHeader")
                end

                local text = ply:Nick() .. " wants you to be their pimp!"

                local w,h = surface.GetTextSize(text)
                surface.SetTextPos(self:GetWide()/2 - w/2, 30)
                surface.DrawText(text)
            end

            local ppid = #pimpPanels + 1;
            table.insert(pimpPanels, backPanel)

        local accept = vgui.Create("DButton", backPanel)
            accept:SetPos(10, 60)
            accept:SetSize(backPanel:GetWide() - 20, 30)
            accept:SetText(" ")

            accept.Paint = function(self)
                local color = acol
                if (self.Hovered) then
                    color = col
                end

                surface.SetDrawColor(color)
                surface.DrawRect(0, 0, self:GetSize())

                surface.SetFont("pimpMenuHeader")
                surface.SetTextColor(245, 245, 245, 255)

                local text = "Accept Request"
                local w,h = surface.GetTextSize(text)

                surface.SetTextPos(self:GetWide()/2 - w/2, self:GetTall()/2 - h/2)
                surface.DrawText(text)
            end

            accept.DoClick = function()
                if (ply:GetPos():Distance(LocalPlayer():GetPos()) > 200) then
                    pimp.notify(LocalPlayer(), "You need to be closer to accept the request!")
                    return
                end
                LocalPlayer():ConCommand("pimp_acceptrequest " .. id)
                if (pimpPanels[ppid]) then
                    table.remove(pimpPanels, ppid)
                end
                backPanel:Close()

                gui.EnableScreenClicker(false)
            end

        local deny = vgui.Create("DButton", backPanel)
            deny:SetPos(10, 100)
            deny:SetSize(backPanel:GetWide() - 20, 30)
            deny:SetText(" ")

            deny.Paint = function(self)
                local color = acol
                if (self.Hovered) then
                    color = col
                end

                surface.SetDrawColor(color)
                surface.DrawRect(0, 0, self:GetSize())

                surface.SetFont("pimpMenuHeader")
                surface.SetTextColor(245, 245, 245, 255)

                local text = "Deny Request"
                local w,h = surface.GetTextSize(text)

                surface.SetTextPos(self:GetWide()/2 - w/2, self:GetTall()/2 - h/2)
                surface.DrawText(text)
            end

            deny.DoClick = function()
                if (pimpPanels[ppid]) then
                    table.remove(pimpPanels, ppid)
                end
                backPanel:Close()

                gui.EnableScreenClicker(false)
            end


        timer.Simple(38, function()
            if ValidPanel(backPanel) then backPanel:Remove() end
        end)
	end
end

local function getHeadPos(ply)
    local bn = ply:LookupBone("ValveBiped.Bip01_Head1")
    if (bn) then
        return ply:GetBonePosition(bn) + Vector(0, 0, 3)
    end
end

local loveMaterial = Material("icon16/heart.png")

pimp.activePimpHits = {}
pimp.activePimpHits[1] = {ply = player.GetBots()[1], price = 200}

hook.Add("HUDPaint", "pimp.drawHUDOverlay", function()
    if (pimp.activeJob) then
        if (pimp.activeJob.pros == LocalPlayer()) then
            if IsValid(pimp.activeJob.ply) then
                local pos = getHeadPos(pimp.activeJob.ply)
                if (pos) then
                    local scrpos = pos:ToScreen()

                    if (scrpos.visible) then
                        surface.SetDrawColor(20, 20, 20, 110)
                        surface.DrawRect(math.Clamp(scrpos.x - 170/2, 0, ScrW() - 170), math.Clamp(scrpos.y - 33/2, 0, ScrH()-33), 170, 33)

                        surface.SetDrawColor(255, 255, 255, 255)
                        surface.SetMaterial(loveMaterial)
                        surface.DrawTexturedRect(math.Clamp(scrpos.x - 170/2, 0, ScrW() - 170) + 10, math.Clamp(scrpos.y - 33/2, 0, ScrH()-33) + 5, 23, 23)

                        surface.SetFont("pimpMenuHeader")
                        surface.SetTextPos(math.Clamp(scrpos.x - 170/2, 0, ScrW() - 170) + 50, math.Clamp(scrpos.y - 33/2, 0, ScrH()-33) + 7)
                        surface.SetTextColor(235, 235, 235, 255)
                        surface.DrawText("Active client")
                    end
                end
            end

        elseif (pimp.activeJob.ply == LocalPlayer()) then
            if IsValid(pimp.activeJob.pros) then
                local pos = getHeadPos(pimp.activeJob.pros)
                if (pos) then
                    local scrpos = pos:ToScreen()

                    if (scrpos.visible) then
                        surface.SetDrawColor(20, 20, 20, 110)
                        surface.DrawRect(math.Clamp(scrpos.x - 170/2, 0, ScrW() - 170), math.Clamp(scrpos.y - 33/2, 0, ScrH()-33), 170, 33)

                        surface.SetDrawColor(255, 255, 255, 255)
                        surface.SetMaterial(loveMaterial)
                        surface.DrawTexturedRect(math.Clamp(scrpos.x - 170/2, 0, ScrW() - 170) + 10, math.Clamp(scrpos.y - 33/2, 0, ScrH()-33) + 5, 23, 23)

                        surface.SetFont("pimpMenuHeader")
                        surface.SetTextPos(math.Clamp(scrpos.x - 170/2, 0, ScrW() - 170) + 50, math.Clamp(scrpos.y - 33/2, 0, ScrH()-33) + 7)
                        surface.SetTextColor(235, 235, 235, 255)
                        surface.DrawText("Press E to start")
                    end
                end
            end
        end
    end

    if (pimp.effectOn) then
        if (pimp.effectBFade) then
            pimp.effectBC = pimp.effectBC - (400 * FrameTime())
            if pimp.effectBC <= 0 then
                pimp.effectOn = 0
                pimp.effectBFade = false
            end
        end

        surface.SetDrawColor(10, 10, 10, pimp.effectBC)
        surface.DrawRect(0, 0, ScrW(), ScrH())

        if (pimp.effectC > 0) then
            pimp.effectC = pimp.effectC - (170 * FrameTime())
        end

        surface.SetDrawColor(235, 235, 235, pimp.effectC)
        surface.DrawRect(0, 0, ScrW(), ScrH())
    end

    if (pimp.isPimp(LocalPlayer())) then
        for __, v in pairs(pimp.activePimpHits) do
            if IsValid(v.ply) and (v.pimp == LocalPlayer()) then
                local plPos = getHeadPos(v.ply):ToScreen()

                local col = Color(pimp.config.color.r, pimp.config.color.g, pimp.config.color.b, 190)
                surface.SetDrawColor(col)
                surface.DrawRect(plPos.x - 170/2, plPos.y - 66/2, 170, 45)

                surface.SetFont("pimpMenuHeader")
                surface.SetTextColor(245, 245, 245, 255)
                local w,h = surface.GetTextSize(v.ply:Nick())
                surface.SetTextPos(plPos.x - w/2, plPos.y - 66/2 + 4)

                surface.DrawText(v.ply:Nick())

                local text = "Distance: " .. math.ceil(v.ply:GetPos():Distance(LocalPlayer():GetPos())/70) .. "m"

                local w,h = surface.GetTextSize(text)
                surface.SetTextPos(plPos.x - w/2, plPos.y - 66/2 + 4 + h)

                surface.DrawText(text)
            end
        end
    end

    if (pimp.healthPulsate and pimp.healthPulsate > 0) then
        pimp.healthPulsate = pimp.healthPulsate - 2
        surface.SetDrawColor(20, 20, 20, pimp.healthPulsate)
        surface.DrawRect(0, 0, ScrW(), ScrH())
    end
end)

function pimp.payWindow(price)
    local col = Color(pimp.config.color.r, pimp.config.color.g, pimp.config.color.b, 190)
    local acol = Color(pimp.config.color.r, pimp.config.color.g, pimp.config.color.b, 120)
    local backPanel = vgui.Create("DFrame")
        backPanel:SetSize(400, 150)
        backPanel:SetTitle("Server Name - Pay for Services")
        backPanel:Center()
        backPanel:MakePopup()
        backPanel:ShowCloseButton(false)

        backPanel.Paint = function(self)
            surface.SetDrawColor(20, 20, 20, 200)
            surface.DrawRect(0, 0, self:GetSize())
        end

    local payButton = vgui.Create("DButton", backPanel)
        payButton:SetPos(10, 40)
        payButton:SetSize(backPanel:GetWide() - 20, 40)
        payButton:SetText(" ")
        payButton.Paint = function(self)
            local color = acol
            if (self.Hovered) then
                color = col
            end

            surface.SetDrawColor(color)
            surface.DrawRect(0, 0, self:GetSize())
            surface.SetFont("pimpMenuHeader")

            local text = "Pay Fee ($" .. price .. ")"
            local w,h = surface.GetTextSize(text)

            surface.SetTextColor(245, 245, 245, 255)
            surface.SetTextPos(self:GetWide()/2 - w/2, self:GetTall()/2 - h/2)
            surface.DrawText(text)
        end

        payButton.DoClick = function(self)
            LocalPlayer():ConCommand("pimp_payfee")
            backPanel:Close()
        end

    local runButton = vgui.Create("DButton", backPanel)
        runButton:SetPos(10, 90)
        runButton:SetSize(backPanel:GetWide() - 20, 40)
        runButton:SetText(" ")
        runButton.Paint = function(self)
            local color = acol
            if (self.Hovered) then
                color = col
            end

            surface.SetDrawColor(color)
            surface.DrawRect(0, 0, self:GetSize())

            surface.SetFont("pimpMenuHeader")

            local text = "Run"
            local w,h = surface.GetTextSize(text)

            surface.SetTextColor(245, 245, 245, 255)
            surface.SetTextPos(self:GetWide()/2 - w/2, self:GetTall()/2 - h/2)
            surface.DrawText(text)
        end

        runButton.DoClick = function(self)
            LocalPlayer():ConCommand("pimp_feerun")
            backPanel:Close()
        end
end

function pimp.pimpWindow()
    local col = Color(pimp.config.color.r, pimp.config.color.g, pimp.config.color.b, 190)
    local acol = Color(pimp.config.color.r, pimp.config.color.g, pimp.config.color.b, 120)

    local backPanel = vgui.Create("DFrame")
        backPanel:SetSize(300, 340 + (#team.GetPlayers(_G[pimp.config.ProstituteTeam]) * 30))
        backPanel:SetTitle("Server Name - Pimp Manager")
        backPanel:Center()
        backPanel:MakePopup()

        backPanel.Paint = function(self)
            surface.SetDrawColor(20, 20, 20, 200)
            surface.DrawRect(0, 0, self:GetSize())

            surface.SetDrawColor(col)
            surface.DrawRect(0, 50, self:GetWide(), 40)
            surface.DrawRect(0, 100, self:GetWide(), 40)
            surface.DrawRect(0, 150, self:GetWide(), 40)
            surface.DrawRect(0, 200, self:GetWide(), 40)

            surface.SetFont("pimpMenuHeader")
            surface.SetTextColor(245, 245, 245, 255)

            local text = "You have made $" .. (pimp.pimpProfit or 0) .. " so far"
            local w,h = surface.GetTextSize(text)

            surface.SetTextPos(self:GetWide()/2 - w/2, 50 + (20 - h/2))
            surface.DrawText(text)

            local text = "You have " .. #team.GetPlayers(_G[pimp.config.ProstituteTeam]) .. " prostitute(s)"
            local w,h = surface.GetTextSize(text)

            surface.SetTextPos(self:GetWide()/2 - w/2, 100 + (20 - h/2))
            surface.DrawText(text)

            local t = string.ToMinutesSeconds(0)
            if (pimp.teamTime) then
                t = string.ToMinutesSeconds(CurTime() - pimp.teamTime)
            end

            local text = "You have been a pimp for " .. t
            local w,h = surface.GetTextSize(text)

            surface.SetTextPos(self:GetWide()/2 - w/2, 150 + (20 - h/2))
            surface.DrawText(text)

            local text = "You have been a pimp ".. pimp.pimpTimes .. " time(s) before"
            local w,h = surface.GetTextSize(text)

            surface.SetTextPos(self:GetWide()/2 - w/2, 200 + (20 - h/2))
            surface.DrawText(text)

        end

    local changeCut = vgui.Create("DButton", backPanel)
        changeCut:SetPos(10, 250)
        changeCut:SetSize(backPanel:GetWide() - 20, 30)
        changeCut:SetText(" ")

        changeCut.Paint = function(self)
            local color = acol
            if (self.Hovered) then
                color = col
            end

            surface.SetDrawColor(color)
            surface.DrawRect(0, 0, self:GetSize())

            surface.SetFont("pimpMenuHeader")
            surface.SetTextColor(245, 245, 245, 255)

            local text = "Change your cut"

            local w,h = surface.GetTextSize(text)

            surface.SetTextPos(self:GetWide() /2 - w/2, self:GetTall()/2 - h/2)
            surface.DrawText(text)
        end
		
		changeCut.DoClick = function()
			local pnl = vgui.Create( "DFrame" )
			pnl:SetSize( 400, 160 )
			pnl:SetPos( (ScrW()/2)-200, (ScrH()/2)-75 )
			pnl:SetDraggable( false )
			pnl:SetTitle( "Pimp - Set Cut" )
			pnl:MakePopup()
			--pnl:ShowCloseButton( false )
			pnl.stime = CurTime()
			pnl.Paint = function( s,w,h )
				Derma_DrawBackgroundBlur( pnl, pnl.stime )
				
				surface.SetDrawColor(Color(20, 20, 20, 200))
				surface.DrawRect(0, 0, s:GetWide(), s:GetTall())
				
				surface.SetDrawColor(col)
				surface.DrawRect(0, 40, s:GetWide(), 40)
				
				surface.SetTextColor(245, 245, 245, 255)
				surface.SetFont("pimpMenuHeader")

				local text = "Set your cut"
				
				local w,h = surface.GetTextSize(text)
				surface.SetTextPos(s:GetWide()/2 - w/2, 40 + (20 - h/2))
				surface.DrawText(text)
			end
			
			local entry = vgui.Create( "DTextEntry", pnl )
			entry:SetPos( 10, 90 )
			entry:SetSize( 380, 20 )
			entry:SetNumeric( true )
			
			local set = vgui.Create("DButton", pnl)
			set:SetPos(10, 120)
			set:SetSize(pnl:GetWide()/2 - 20, 30)
			set:SetText(" ")
			
			set.Paint = function(self)
				local color = acol
				if (self.Hovered) then color = col end
				
				surface.SetDrawColor(color)
				surface.DrawRect(0, 0, self:GetSize())
				
				surface.SetFont("pimpMenuHeader")
				surface.SetTextColor(245, 245, 245, 255)
				
				local text = "Set cut"
				local w,h = surface.GetTextSize(text)
				surface.SetTextPos(self:GetWide() /2 - w/2, self:GetTall()/2 - h/2)
				surface.DrawText(text)
			end
			set.DoClick = function( s )
				local v = tonumber( entry:GetValue() )
				if type(v)~="number" then pimp.notify(LocalPlayer(), "Invalid number!") return end
				
				LocalPlayer():ConCommand("pimp_setcut " .. tostring(math.Clamp(v,0,100)) )
				pnl:Close()
			end
			
			local cancel = vgui.Create("DButton", pnl)
			cancel:SetPos( (pnl:GetWide()/2) + 10, 120)
			cancel:SetSize(pnl:GetWide()/2 - 20, 30)
			cancel:SetText(" ")
			
			cancel.Paint = function(self)
				local color = acol
				if (self.Hovered) then color = col end
				
				surface.SetDrawColor(color)
				surface.DrawRect(0, 0, self:GetSize())
				
				surface.SetFont("pimpMenuHeader")
				surface.SetTextColor(245, 245, 245, 255)
				
				local text = "Cancel"
				local w,h = surface.GetTextSize(text)
				surface.SetTextPos(self:GetWide() /2 - w/2, self:GetTall()/2 - h/2)
				surface.DrawText(text)
			end
			cancel.DoClick = function( s )
				pnl:Close()
			end
		end
	
    local pimpContainer = vgui.Create("DPanelList", backPanel)
        pimpContainer:SetPos(10, 300)
        pimpContainer:SetSize(backPanel:GetWide() - 20, 300)
        pimpContainer:SetSpacing(10)
        pimpContainer:EnableVerticalScrollbar(true)

    function pimpContainer:Update()
        self:Clear(true)

        for __, v in pairs(team.GetPlayers(_G[pimp.config.ProstituteTeam])) do
			if not pimp.getPimp(v)==LocalPlayer() then continue end
			
            local pPanel = vgui.Create("DPanel")
                pPanel:SetHeight(25)
                pPanel.Paint = function(self)
                    surface.SetDrawColor(col)
                    surface.DrawRect(0, 0, self:GetSize())

                    surface.SetFont("pimpMenuHeader")
                    surface.SetTextColor(245, 245, 245, 255)

                    surface.SetTextPos(5, 3)
                    surface.DrawText(v:Nick())
                end

            local pButton = vgui.Create("DButton", pPanel)
                pButton:SetPos(pimpContainer:GetWide() - 50, 0)
                pButton:SetSize(50, 25)
                pButton:SetText(" ")

                pButton.Paint = function(self)
                    local color = acol
                    if (self.Hovered) then
                        color = col
                    end

                    surface.DrawRect(0, 0, self:GetSize())

                    local text = "Fire"
                    surface.SetFont("pimpMenuSubHeader")
                    surface.SetTextColor(245, 245, 245, 255)

                    local w,h = surface.GetTextSize(text)

                    surface.SetTextPos(self:GetWide()/2 - w/2, self:GetTall()/2 - h/2)
                    surface.DrawText(text)
                end

                pButton.DoClick = function()
                    LocalPlayer():ConCommand("pimp_fire " .. v:UniqueID())
                    timer.Simple(0.8, function()
                        if ValidPanel(pimpContainer) then
                            pimpContainer:Update()
                        end
                    end)
                end

            pimpContainer:AddItem(pPanel)
        end
    end

    pimpContainer:Update()
end

// Net functions

net.Receive("pimp.effectStart", function()
    local times = net.ReadInt(8)
    pimp.effectC = 0
    pimp.effectBC = 255
    pimp.effectBFade = false

    timer.Create("pimp.effectPulse", 1, times, function()
        pimp.effectC = math.random(100, 190)
    end)

    pimp.effectOn = true
end)

net.Receive("pimp.syncPimpHits", function()
    pimp.activePimpHits = net.ReadTable()
end)

net.Receive("pimp.effectEnd", function()
    timer.Simple(1, function() pimp.effectC = 500 end)
    timer.Simple(3, function() pimp.effectBFade = true end)
end)

net.Receive("pimp.notify", function()
    local msg = net.ReadString()
    pimp.notify(LocalPlayer(), msg)
end)

net.Receive("pimp.price", function()
    local price = net.ReadInt(16)
    pimp.clientPrice = price
end)

net.Receive("pimp.syncNewJob", function()
    pimp.activeJob = net.ReadTable()
end)

net.Receive("pimp.request", function()
    local pros = net.ReadEntity()
    local price = net.ReadInt(16)
    local id = net.ReadInt(16)

    pimp.openRequest(pros, price, id)
end)
net.Receive("pimp.request2", function()
    local pros = net.ReadEntity()
    local id = net.ReadInt(16)

    pimp.pimpRequest(pros, id)
end)

net.Receive("pimp.wipeActiveJob", function()
    pimp.activeJob = nil
end)

net.Receive("pimp.updateTotalPaid", function()
    pimp.totalEarnt = net.ReadInt(16)
    pimp.pimpTotal = net.ReadInt(16)
end)

net.Receive("pimp.payWindow", function()
    local price = net.ReadInt(16)
    pimp.payWindow(price)
end)

net.Receive("pimp.healthPulsate", function()
    pimp.healthPulsate = 200
end)

net.Receive("pimp.pimpProfitSync", function()
    pimp.pimpProfit = net.ReadInt(16)
    if pimp.pimpProfit > 10000 or pimp.pimpProfit < 1 then
        pimp.pimpProfit = 100
    end
 end)

local pIsPimp = false

timer.Create("pimp.checkIfPimp", 3, 0, function()
    if (pimp.isPimp(LocalPlayer()) and !pIsPimp) then
        pimp.pimpTimes = pimp.pimpTimes + 1
        pimp.teamTime = CurTime()

        pIsPimp = true

        pimp.saveTimes()
    end

    if (!pimp.isPimp(LocalPlayer()) and pIsPimp) then
        pIsPimp = false
    end
end)

local pimpGunHalos = {}

net.Receive("pimp.gunFire", function()
    local ply = net.ReadEntity()
    local wep = net.ReadEntity()
    local target = net.ReadEntity()
    local hitpos = net.ReadVector()

    if (ply == LocalPlayer()) and IsValid(wep) then
        wep.firing = true
        timer.Simple(0.6, function()
            wep.firing = false
            wep.target = target
            wep.hitpos = hitpos
        end)
    end

    if IsValid(target) then
        table.insert(pimpGunHalos, target)

        timer.Simple(0.6, function()
            for k, v in pairs(pimpGunHalos) do
                if (v == target) then
                    table.remove(pimpGunHalos, k)
                end
            end
        end)
    end
end)

local killPink = 0

net.Receive("pimp.gunKill", function()
    LocalPlayer():PrintMessage(HUD_PRINTCENTER, "The pimp has got revenge!")

    killPink = 0

    hook.Add("HUDPaint", "pimp.killGunHUD", function()
        killPink = killPink + (FrameTime() * 200)
        local color = pimp.config.color

        color.a = math.Clamp(killPink, 0, 200)

        surface.SetDrawColor(color)
        surface.DrawRect(0, 0, ScrW(), ScrH())
    end)

    timer.Simple(0.7, function()
        hook.Remove("HUDPaint", "pimp.killGunHUD")
    end)
end)


hook.Add("PreDrawHalos", "pimp.gunDrawHalos", function()
    halo.Add(pimpGunHalos, pimp.config.color, 5, 5, 2)
end)

local ActiveHearts = {}
local HeartMat = Material( "icon16/heart.png" )
local ColWhite = Color(255,255,255)
hook.Add( "PostDrawOpaqueRenderables", "pimp.effectDrawHearts", function()
if not pimp.config.ActiveEffects then return end
	for _,ply in pairs(player.GetAll()) do
		if ply:GetNWBool( "IsUsingProsServices" ) and ((not ply.Pimp_NextHeart) or ply.Pimp_NextHeart<=CurTime()) then
			table.insert( ActiveHearts, {
				startpos= ply:EyePos() + Vector(math.random(-10,10), math.random(-10,10), math.random(-60,10)),
				pos = ply:EyePos(),
				start=CurTime(),
				die=CurTime()+1,
			})
			ply.Pimp_NextHeart = CurTime()+0.1
		end
	end
	
	render.SetMaterial( HeartMat )
	for k,v in pairs( ActiveHearts ) do
		if CurTime()>=v.die then ActiveHearts[k]=nil continue end
		
		local delta = 1-( (v.die-CurTime())/(v.die-v.start) )
		
		ColWhite.a = delta*255
		
		v.pos = Vector( v.startpos[1], v.startpos[2], v.startpos[3]+ (5*delta) )
		render.DrawSprite( v.pos, 5, 5, ColWhite )
	end
end)

pimp.loadTimes()