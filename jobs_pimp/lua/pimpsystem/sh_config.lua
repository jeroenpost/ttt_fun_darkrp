// Pimp System Configuration 
// All the times are in seconds

pimp.config.newDarkRP = true // Set this to true if you're using above 2.4.3
pimp.config.UseProvidedJobs = true // use the jobs provided?

// Money Values

pimp.config.MaxPrice = 1000 // Maximum price that a prostitute can choose to charge for their "services"
pimp.config.MinPrice = 50 // Maximum price that a prostitute can choose to charge for their "services"
pimp.config.pimpCut = 50 // Percentage of the cut that the pimp gets (Default, can be changed by user)
pimp.config.defaultPrice = 200 // Default price of services
pimp.config.hitAddition = 500 // Price added on when killing a fleeing customer
pimp.config.noPimpRisk = 50 // Percentage of chance of the prostitute not getting paid with no pimp.

// Name values
pimp.config.ProsSWEPName = "Prostitute" // Display name of the SWEP
pimp.config.PimpSWEPName = "Pimp Hand" // Display name of the SWEP
pimp.config.PimpGunSWEPName = "Pimp Gun" // Display name of the gun swep
pimp.config.PimpTeam = "TEAM_PIMP" // Name of variable for Pimp team
pimp.config.ProstituteTeam = "TEAM_PROSTITUTE" // Name of variable for prostitute team

// Time Values

pimp.config.exhaustTime = 5 // How long the prostitute has to wait
pimp.config.requestTimeout = 30 // Time it takes for the request to timeout.
pimp.config.jobTimeout = 40 // Time it takes for a job to timeout once accepted.
pimp.config.effectDelay = 5 // The time it takes for the effect to start after the job is completed.
pimp.config.pimpChangeDelay = 30 --Cooldown on changing pimp

-- Player releated
pimp.config.wantOnAskCop = true // Set the prostitute to wanted when asking a cop for "services"
pimp.config.wantOnCopNear = true --Set wanted when there is a cop nearby
pimp.config.hitGoOnDeath = true // Hit disappears when pimp dies
pimp.config.canSuicide = false // Can the player commit suicide whilst having a job with the prostitute.

-- Weapons
pimp.config.slapDamage = 15 // The amount of damage the pimp slap does.
pimp.config.PimpGunRange = 500 // Range of pimp gun
pimp.config.PimpGunCooldown = 60 // cooldown of gun in seconds
pimp.config.PimpGunCustomSound = false --Use the custom sound for the pimp gun

// Misc
pimp.config.color = Color(255, 0, 255) // Color of chat message identifier and menu
pimp.config.identifier = "[Pimp]" // Identifier in chat
pimp.config.stdChance = 40 // The percentage chance of a bad event happening
pimp.config.ActiveEffects = true --Use the effects when a player is actively using the "services"
