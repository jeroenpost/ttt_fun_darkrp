// Pimp System Serverside File


include("sh_pimpsystem.lua")
util.AddNetworkString("pimp.notify")
util.AddNetworkString("pimp.price")
util.AddNetworkString("pimp.request")
util.AddNetworkString("pimp.request2")
util.AddNetworkString("pimp.syncNewJob")
util.AddNetworkString("pimp.wipeActiveJob")
util.AddNetworkString("pimp.effectStart")
util.AddNetworkString("pimp.effectEnd")
util.AddNetworkString("pimp.payWindow")
util.AddNetworkString("pimp.syncPimpHits")
util.AddNetworkString("pimp.updateTotalPaid")
util.AddNetworkString("pimp.healthPulsate")
util.AddNetworkString("pimp.pimpProfitSync")
util.AddNetworkString("pimp.slapAnimation")
util.AddNetworkString("pimp.gunFire")
util.AddNetworkString("pimp.gunKill")

function pimp.pay(ply, amount)
    if amount < 0 or amount > 10000 then return end
    local p = pimp.getPimp( ply );

    if IsValid(p) then
        local cut = math.floor(amount * (pimp.GetCut(p) / 100))
        if (pimp.config.newDarkRP) then
            p:addMoney(cut)
            ply:addMoney(amount - cut)
        else
            p:AddMoney(cut)
            ply:AddMoney(amount - cut)
        end

        pimp.notify(ply, "You just got paid $" .. math.floor(amount/2) .. " (".. (pimp.GetCut(p)) .. "% went to the pimp)")
        pimp.notify(p, ply:Nick() .. " just got paid, you earnt $" .. math.floor(amount/2))

    else
        if (pimp.config.newDarkRP) then
            ply:addMoney(amount)
        else
            ply:AddMoney(amount)
        end
        pimp.notify(ply, "You were just paid $" .. amount .. ".")
    end
end

function pimp.setPrice(ply, price)
    if IsValid(ply) and pimp.isProstitute(ply) then
        price = math.Clamp(price, pimp.config.MinPrice, pimp.config.MaxPrice)
        ply.clientPrice = price

        net.Start("pimp.price")
            net.WriteInt(ply.clientPrice, 16)
        net.Send(ply)

        pimp.notify(ply, "You set your price to $" .. price ..".")
    end
end

concommand.Add("pimp_setprice", function(ply, __, args)
    if args[1] and pimp.isProstitute(ply) then
        local price = tonumber(args[1]);
        pimp.setPrice(ply, price)
    end
end)

pimp.activeRequests = pimp.activeRequests or {};

function pimp.startRequest(ply, target)
    if !IsValid(ply) or !IsValid(target) then return end

    if (!pimp.isProstitute(ply)) then return end

    if (pimp.isProstitute(target) or pimp.isPimp(target)) then return end

    if (ply._pimpin) then pimp.notify(ply, "Wait until you're finished!") return end

    if (target._pimpin) then pimp.notify(ply, "They're busy!") return end

    if (pimp.config.wantOnAskCop) and (pimp.config.newDarkRP and target:isCP()) or (!pimp.config.newDarkRP and target:IsCP()) then
        ply:wanted(target, "Prostitution");
    end

    ply._pRequestee = target
    timer.Create("pimp.endRequest:"..ply:UserID(), 40, 1, function()
        if IsValid(ply._pRequestee) then
            pimp.notify(ply, ply._pRequestee:Nick() .. " denied your request")
            ply._pRequestee = nil
        end
    end)

	if target:IsBot() then
		pimp.acceptRequest( target, ply, 0 )
	else
		local index = #pimp.activeRequests + 1;

		pimp.activeRequests[index] = {pros = ply, ply = target, cost = (ply.clientPrice or pimp.config.defaultPrice), time = CurTime()}
		net.Start("pimp.request")
			net.WriteEntity(ply)
			net.WriteInt(ply.clientPrice or 200, 16)
			net.WriteInt(index, 16)
		net.Send(target)
		
		pimp.notify(ply, "You asked " .. target:Nick() .. " if they'd like to buy your service.")
	end

    pimp.copRisk(ply)
end
function pimp.PimpRequest( pros, pmp )
	if not (pimp.isProstitute(pros) and pimp.isPimp(pmp)) then return end
	
    local index = #pimp.activeRequests + 1;
    pimp.activeRequests[index] = {pros = pros, ply = pmp, ForPimp=true, time = CurTime()}
    net.Start("pimp.request2")
        net.WriteEntity(pros)
        net.WriteInt(index, 16)
    net.Send(pmp)
end

function pimp.acceptRequest(ply, pros, price)
    if IsValid(ply) then
        if (ply:GetPos():Distance(pros:GetPos()) > 200) then
            pimp.notify(ply, "You need to be closer to accept the request")
            return
        end

        if (pimp.config.newDarkRP and !ply:canAfford(price)) or (!pimp.config.newDarkRP and !ply:CanAfford(price)) then
            pimp.notify(ply, "You cannot afford her price!")
            return
        end

        if (pros._pimpin) then
            pimp.notify(ply, pimp:Nick().. " is currently busy!")
            return
        end

        if (pimp.config.newDarkRP) then
            ply:addMoney(-price)
        else
            ply:AddMoney(-price)
        end

        timer.Destroy("pimp.endRequest" .. pros:UserID())

        pimp.startJob(ply, pros, price)
        pros._pRequestee = nil
    end
end

function pimp.startJob(ply, pros, price)
    if IsValid(ply) and IsValid(pros) then
        pimp.notify(ply, "Take her somewhere out of sight")
        pimp.notify(pros, ply:Nick() .. " accepted your job offer, follow them to some place quiet.")

        ply._pimpin = true
        pros._pimpin = true

        timer.Create("prostitute.timeout:"..ply:UniqueID(), 120, 1, function()
            if (IsValid(ply)) then
                pimp.notify(ply, "You were too slow! She has walked away!")
                ply._pimpin = false
                if (pimp.config.newDarkRP) then
                    ply:addMoney(price)
                else
                    ply:AddMoney(price)
                end

                net.Start("pimp.wipeActiveJob")
                net.Send(ply)
            end

            if IsValid(pros) then
                pros._pimpin = false
                pimp.notify(pros, "They were too slow, your deal is over!")
                net.Start("pimp.wipeActiveJob")
                net.Send(pros)
            end
        end)

        local pjd = {ply = ply, pros = pros, price = price}
        ply._pimpJobDetails = pjd
        pros._pimpJobDetails = pjd

        net.Start("pimp.syncNewJob")
            net.WriteTable(pjd)
        net.Send({ply, pros})
		
		if ply:IsBot() or pros:IsBot() then
			pimp.startEffect(ply, pros, function()
				pimp.startPay(ply, pros, ply._pimpJobDetails.price)
				pimp.endJob(ply, pros)
			end)
		end
    end
end

function pimp.startEffect(ply, pros, callback)
    if (IsValid(ply) and IsValid(pros) and pimp.isProstitute(pros) and (ply._pimpJobDetails and pros._pimpJobDetails)) then
		ply:SetNWBool( "IsUsingProsServices", true )
        ply:Freeze(true)
        pros:Freeze(true)
        local times = math.random(6, 14)
        net.Start("pimp.effectStart")
            net.WriteInt(times, 8)
        net.Send({ply, pros})

        local i = 0;
        timer.Create("pimp.soundEffect:"..ply:UniqueID(), 1, times, function()
            i = i + 1;
            ply:EmitSound("hostage/hpain/hpain" .. math.random(1, 6) ..".wav")
            if (i == times) then
                net.Start("pimp.effectEnd")
                net.Send({ply, pros})
				
				ply:SetNWBool( "IsUsingProsServices", false )
				
                timer.Simple(1, function()
                    ply:EmitSound("player/death5.wav")

                    ply:Freeze(false)
                    pros:Freeze(false)

                    timer.Destroy("prostitute.timeout:" .. ply:UniqueID())
                    callback()
                end)
            end
        end)
    end
end

function pimp.endJob(ply, pros)
    if IsValid(ply) then
        net.Start("pimp.wipeActiveJob")
        net.Send(ply)

        ply._pimpin = false
        ply._pimpJobDetails = false
    end

    if IsValid(pros) then
        net.Start("pimp.wipeActiveJob")
        net.Send(pros)

        pros._pimpin = false
        pros.pimpJobDetails = false
    end
end

function pimp.startPay(ply, pros, price)
    if IsValid(ply) and IsValid(pros) then
        ply._prosHasPaid = {ply = ply, pros = pros, price = price, paid = false}
        local pi = pimp.getPimp( pros )
        if (not ply:IsBot()) and IsValid(pi) and ((pimp.config.wantOnAskCop) and ((pimp.config.newDarkRP and !ply:isCP()) or (!pimp.config.newDarkRP and !ply:IsCP()))) then
            net.Start("pimp.payWindow")
                net.WriteInt(price, 16)
            net.Send(ply)
        else
            pimp.payFee(ply, pros, price)
        end
    end
end

function pimp.payFee(ply, pros, price)
    if IsValid(ply) and IsValid(pros) then
        if price < 0 or price > 10000 then return end
        local pi = pimp.getPimp( pros )
        local cut = math.floor(price * (pimp.GetCut(pi) / 100))

        pros._pworkTotalFees = pros._pworkTotalFees or 0
        pros._ppimpTotalFees = pros._ppimpTotalFees or 0

        pimp.notify(ply, "You paid $" .. price .. ".")

        if (IsValid(pi)) then
            pros._pworkTotalFees = pros._pworkTotalFees + math.floor(price-cut)
            pros._ppimpTotalFees = pros._ppimpTotalFees + math.floor(cut)

            if (pimp.config.newDarkRP) then
                pros:addMoney(math.floor(price - cut))
                pi:addMoney(math.floor(cut))
            else
                pros:AddMoney(math.floor(price - cut))
                pi:AddMoney(math.floor(cut))
            end

            pi._totalPMEarnt = pi._totalPMEarnt or 0
            pi._totalPMEarnt = pi._totalPMEarnt + cut

            net.Start("pimp.pimpProfitSync")
                net.WriteInt(pi._totalPMEarnt, 16)
            net.Send(pi)

            pimp.notify(pros, "You have been paid $"..math.floor(price - cut).." ($" .. math.floor(cut) .. " went to pimp) for the job!")
            pimp.notify(pi, pros:Nick() .. " just got paid, you earnt $" .. cut)
            ply._prosHasPaid = nil
        else

            if (math.random(1, 100) <= pimp.config.noPimpRisk) then
                ply._prosHasPaid = nil
                pimp.notify(pros, "You have not been paid for this job. When you do not have a pimp you have a "..tostring(pimp.config.noPimpRisk).."% chance of payment failing.")
                pimp.playerEffect(ply)
                return
            end

            if (pimp.config.newDarkRP) then
                pros:addMoney(price)
            else
                pros:AddMoney(price)
            end

            pros._pworkTotalFees = pros._pworkTotalFees + price

            pimp.notify(pros, "You have been paid $" .. math.floor(price) .. " for the job!")
            ply._prosHasPaid = nil
        end
        net.Start("pimp.updateTotalPaid")
            net.WriteInt(pros._pworkTotalFees, 16)
            net.WriteInt(pros._ppimpTotalFees, 16)
        net.Send(pros)

        pimp.playerEffect(ply)
    end
end

function pimp.feeRun(ply, pros, price)
    if IsValid(ply) and IsValid(pros) then
        pimp.notify(pros, ply:Nick() .. " decided to run with the money!")
        pimp.startJack(ply, pros, price)
        pimp.playerEffect(ply)
    end
end

concommand.Add("pimp_payfee", function(ply)
    if (ply._prosHasPaid) then
        pimp.payFee(ply, ply._prosHasPaid.pros, ply._prosHasPaid.price)
    end
end)

concommand.Add("pimp_feerun", function(ply)
    if (ply._prosHasPaid) then
        pimp.feeRun(ply, ply._prosHasPaid.pros, ply._prosHasPaid.price)
    end
end)

function pimp.startJack(ply, pros, price)
    if IsValid(ply) and IsValid(pros) then
        local pi = pimp.getPimp( pros )

        if IsValid(pi) then
            pimp.notify(pi, "One of your girls, " .. pros:Nick() .. " just got jacked for $" .. price ..". ".. ply:Nick() .. " refused to pay up.")
            pimp.notify(pi, "Kill them to receive the payment!")

            pimp.notify(ply, "You are now wanted by the pimp, if you are killed by him the fee will be taken + $" .. pimp.config.hitAddition .. " compensation.")
            pimp.activePimpHits = pimp.activePimpHits or {}
            ply._activePimpHit = true

            table.insert(pimp.activePimpHits, {ply = ply, pros = pros, price = price, time = CurTime(), pimp = pi})
            net.Start("pimp.syncPimpHits")
                net.WriteTable(pimp.activePimpHits)
            net.Send(pi)
        end
    end
end

local pGoodEffects = {}
local pBadEffects = {}

pGoodEffects[1] = function(ply)
    local h = ply:Health()
    h = h + math.random(5, 15)

    ply:SetHealth(h)
    pimp.notify(ply, "You feel refreshed, and gained health.")
end

pGoodEffects[2] = function(ply)
    local rSpeed = ply:GetRunSpeed()
    rSpeed = rSpeed + math.random(5, 10)

    ply:SetRunSpeed(rSpeed)
    pimp.notify(ply, "You feel elated, and gained run speed.")
end

pGoodEffects[3] = function(ply)
    local armor = ply:Armor()
    armor = armor + math.random(5, 15)

    ply:SetArmor(armor)
    pimp.notify(ply, "You feel revitalised, and gained armor.")
end

pGoodEffects[4] = function(ply)
    local h = ply:GetJumpPower()
    h = h + math.random(5, 15)

    ply:SetJumpPower(h)
end

pBadEffects[1] = function(ply)
    local h = ply:Health()
    h = h - math.random(5, 30)

    if (h<=0) then
        ply:Kill()
    else
        ply:SetHealth(h)
    end

    pimp.notify(ply, "You contracted crabs, and lost health.")
end

pBadEffects[2] = function(ply)
    pimp.notify(ply, "You contracted herpes, and got set on fire.")
    ply:Ignite(math.random(3, 6))
end

pBadEffects[3] = function(ply)
    local i = 0
    local uid = ply:UniqueID()
    timer.Create("pimp.AidsKill:" .. uid, 5, 20, function()
        if IsValid(ply) and ply.Pimp_FlaggedForEffect then
            net.Start("pimp.healthPulsate")
            net.Send(ply)
            i = i + 1
        else
            timer.Destroy("pimp.AidsKill" .. uid)
        end

        if (i == 20) then
            ply:Kill()
        end
    end)

    pimp.notify(ply, "You contracted AIDS, you will die.")
end

pBadEffects[4] = function(ply)
    local rSpeed = ply:GetWalkSpeed()
    rSpeed = rSpeed - math.random(5, 10)

    ply:SetWalkSpeed(rSpeed)
    pimp.notify(ply, "You contracted HIV, and lost walk speed.")
end

function pimp.playerEffect(ply)
	ply.Pimp_FlaggedForEffect = true
    timer.Simple(math.random(math.max(pimp.config.effectDelay - 5, 0), pimp.config.effectDelay + 5) , function()
        if IsValid(ply) and ply.Pimp_FlaggedForEffect then
            if (math.random(1, 100) <= pimp.config.stdChance) or ((pimp.config.wantOnAskCop) and (pimp.config.newDarkRP and ply:isCP()) or (!pimp.config.newDarkRP and ply:IsCP())) then
                local eff = table.Random(pBadEffects)
                eff(ply)
            else
                local eff = table.Random(pGoodEffects)
                eff(ply)
            end
        end
    end)
end

function pimp.copRisk(ply)
    if not (pimp.config.wantOnCopNear and IsValid(ply)) then return end

--    local pAround = team.GetPlayers(TEAM_POLICE)
--    table.CopyFromTo(team.GetPlayers(TEAM_CHIEF), pAround)
	local pAround = ents.FindInSphere( ply:GetPos(), 500 )
	if not pAround then return end
    local copAround = false
    for __, v in pairs(pAround) do
         if IsValid(v) and v:IsPlayer() and (v.IsCP and v:IsCP()) or (v.isCP and v:isCP()) then
            copAround = v
            break
        end
    end

    if (copAround and (math.random(1, 3) == 1)) then
        pimp.notify(copAround, ply:Nick() .. " is attempting to sell a prostitution service!")
    end
end

hook.Add("KeyPress", "pimp.doStuffUse", function(ply, key)
    if (key == IN_USE) then
        if (!ply._pimpin) then return end

        local tr = ply:GetEyeTrace()
        if IsValid(tr.Entity) and tr.Entity:GetPos():Distance(ply:GetPos()) < 200 then
            local ent = tr.Entity
            if (IsValid(ent) and ent:IsPlayer() and ply._pimpin and ent._pimpin) then
                if (ply._pimpJobDetails and (ply._pimpJobDetails.pros == ent)) then
                    pimp.startEffect(ply, ent, function()
                        pimp.startPay(ply, ent, ply._pimpJobDetails.price)
                        pimp.endJob(ply, ent)
                    end)
                end
            end
        end
    end
end)

local function plyGetMoney(ply)
    if (pimp.config.newDarkRP) then
        return ply:getDarkRPVar("money")
    else
        return ply.DarkRPVars.money
    end
end

hook.Add("PlayerDeath", "pimp.playerDeathHandle", function(ply, __, attacker)
    if IsValid(ply) then
		ply:SetNWBool( "IsUsingProsServices", false )
		ply.Pimp_FlaggedForEffect = false
		
        if (ply._pimpin and IsValid(ply._pimpJobDetails.pros)) then
            if (pimp.config.newDarkRP) then
                ply:addMoney(ply._pimpJobDetails.price)
            else
                ply:AddMoney(ply._pimpJobDetails.price)
            end
            pimp.endJob(ply, ply._pimpJobDetails.pros)
            pimp.notify(ply, "You died during a request, it has been canceled and you have not been charged.")
        end

        if (ply._activePimpHit and IsValid(attacker) and attacker:IsPlayer()) then
            if (pimp.activePimpHits) then
                for k, v in pairs(pimp.activePimpHits) do
                    if (v.pimp == attacker and v.ply == ply) then
                        local money = v.price + pimp.config.hitAddition
                        if (pimp.config.newDarkRP and !ply:canAfford(v.price + pimp.config.hitAddition)) or (!pimp.config.newDarkRP and !ply:CanAfford(v.price + pimp.config.hitAddition)) then
                            pimp.notify(ply, "You could not afford to pay the pimp, he has taken what you had left.")
                            money = plyGetMoney(ply)
                        end

                        if money < 0 or money > 10000 then return end

                        if (pimp.config.newDarkRP) then
                            ply:addMoney(-money)
                            if IsValid(v.pros) then
                                v.pros:addMoney(math.floor(money/2))
                                pimp.notify(v.pros, "Your pimp has killed " .. ply:Nick() .. " who ripped you off. You have been given $" .. math.floor(money/2))
                                pimp.notify(v.pimp, "You have killed " .. ply:Nick() .. " who ripped off one of your girls. You have been given $" .. math.floor(money/2))
                            else
                                attacker:addMoney(money)
                            end
                        else
                            ply:AddMoney(-money)
                            if IsValid(v.pros) then
                                v.pros:AddMoney(math.floor(money/2))
                                pimp.notify(v.pros, "Your pimp has killed " .. ply:Nick() .. " who ripped you off. You have been given $" .. math.floor(money/2))
                                pimp.notify(v.pimp, "You have killed " .. ply:Nick() .. " who ripped off one of your girls. You have been given $" .. math.floor(money/2))
                            else
                                attacker:AddMoney(money)
                            end
                        end


                        pimp.notify(ply, "You were killed by the pimp of the prostitute you ripped off. You have been robbed of the full amount + $" .. pimp.config.hitAddition .. ".")

                        table.remove(pimp.activePimpHits, k)

                        net.Start("pimp.syncPimpHits")
                            net.WriteTable(pimp.activePimpHits)
                        net.Send(attacker)
                    end
                end
            end
        end

        if (pimp.config.hitGoOnDeath && pimp.activePimpHits) then
            for k, hit in pairs(pimp.activePimpHits) do
                if (hit.ply == ply) then
--                    table.remove(pimp.activePimpHits, k)
                    if IsValid(hit.pimp) then hit.pimp._activePimpHit = nil end

                    table.remove(pimp.activePimpHits, k)

                    net.Start("pimp.syncPimpHits")
                        net.WriteTable(pimp.activePimpHits)
                    net.Send(pimp.GetPimps() or {})
                end
            end
        end

        timer.Destroy("pimp.AidsKill" .. ply:UniqueID())
    end
end)


hook.Add("PlayerDisconnected", "pimp.handleDisconnected", function(ply)
    if IsValid(ply) then
        if (ply._pimpin and ply._pimpJobDetails) then
            pimp.endJob(ply, ply._pimpJobDetails.pros)
        end
    end
end)

hook.Add("CanPlayerSuicide", "pimp.handleSuicide", function(ply)
    if (!pimp.config.canSuicide and ply._pimpin) then
        pimp.notify(ply, "You cannot suicide whilst being with a prostitute.")
        return false
    end
end)

concommand.Add("pimp_acceptrequest", function(ply, __, args)
    if (args and args[1]) then
        if (type(tonumber(args[1])) == "number") then
            if (pimp.activeRequests[tonumber(args[1])]) then
                local req = pimp.activeRequests[tonumber(args[1])]
                if (IsValid(req.pros) and (CurTime() - req.time) <= 40 and pimp.isProstitute(req.pros)) then
					if pimp.isPimp( ply ) and req.ForPimp then
						pimp.SetPimp( req.pros, ply )
						table.remove(pimp.activeRequests, index)
					else
						pimp.acceptRequest(ply, req.pros, req.cost)
						table.remove(pimp.activeRequests, index)
					end
                end
            end
        end
    end
end)

function pimp.SetPimp( pros, pmp )
	if pimp.isProstitute( pros ) and pimp.isPimp( pmp ) then
		pros:SetNWEntity( "pimp", pmp )
		
		pimp.notify(pros, "Your pimp is now ".. pmp:Nick() )
		pimp.notify(pmp, "You are now ".. pros:Nick().."'s pimp" )
	end
end
function pimp.FirePros( pros, pmp )
	if IsValid(pmp) and pimp.getPimp(pros)~=pmp then return false end
	
	pros:SetNWEntity( "pimp", pros ) --Clear value
	return true
end
concommand.Add( "pimp_unsetpimp", function(ply,c,a)
	if pimp.isProstitute(ply) then
		local pmp = pimp.getPimp( ply )
		if pimp.FirePros( ply ) then
			if IsValid(pmp) and pimp.isPimp(pmp) then
				pimp.notify(pmp, ply:Nick().." has quit!" )
				pimp.notify(ply, "You have left your pimp, you are on your own!" )
			else
				pimp.notify(ply, "You have no pimp, you are on your own!" )
			end
		end
	end
end)
concommand.Add( "pimp_setpimp", function( ply,c,a )
	if pimp.isProstitute(ply) then
		if ply.NextPimpRequest and ply.NextPimpRequest>CurTime() then pimp.notify(ply, "You must wait another ".. tostring(math.ceil(pl.NextPimpRequest-CurTime())).." seconds to change pimp" ) return end
		
		local opmp = pimp.getPimp( ply )
		local npmp = player.GetByUniqueID(a[1])
		if IsValid(npmp) and pimp.isPimp(npmp) then
			ply.NextPimpRequest = CurTime() + pimp.config.pimpChangeDelay
			
			if npmp:IsBot() then
				pimp.SetPimp( ply, npmp )
				if IsValid(omp) then pimp.notify(opmp, ply:Nick().." has left you for "..npmp:Nick().."!") end
			else
				pimp.PimpRequest( ply, npmp )
				pimp.notify(ply, "You have requested "..npmp:Nick().." be your pimp!")
			end
--			pimp.SetPimp( ply, npmp )
--			if IsValid(omp) then pimp.notify(opmp, ply:Nick().." has left you for "..npmp:Nick().."!") end
		end
	end
end)
concommand.Add("pimp_fire", function(ply, __, args)
    if (pimp.isPimp(ply) and args[1]) then
        local uid = args[1]
        local target = player.GetByUniqueID(uid)
		
		if not IsValid(target) then return end
		
		if pimp.FirePros( target, ply ) then
			pimp.notify( target, "You were fired by "..ply:Nick()..", you are now on your own" )
			pimp.notify( ply, "You have fired "..target:Nick() )
		else
			pimp.notify( ply, "You can't fire "..target:Nick()..", you're not their pimp!" )
		end
--[[
        if IsValid(target) then
            if (pimp.config.newDarkRP) then
                target:changeTeam(TEAM_CITIZEN, true)
            else
                target:ChangeTeam(TEAM_CITIZEN, true)
            end

            pimp.notify(target, "You were demoted by " .. ply:Nick())
        end --]]
    end
end)

concommand.Add( "pimp_setcut", function( ply, _, args )
	if not (IsValid(ply) and pimp.isPimp(ply)) then return end
	
	local num = tonumber(args[1])
	if type(num)~="number" then pimp.notify(ply, "Invalid cut amount!") return end
	ply:SetNWInt( "Pimp_PimpCut", math.Clamp( num, 0, 100) )
	pimp.notify(ply, "Your cut is now "..tostring(math.Clamp( num, 0, 100)).."%")
end)
