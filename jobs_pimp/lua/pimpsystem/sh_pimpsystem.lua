// Pimp System Shared File


pimp = pimp or {} --Autorefresh and bad coders do not mix
pimp.config = {}

include("sh_config.lua")

function pimp.GetPimps() return team.GetPlayers( _G[pimp.config.PimpTeam] ) end

function pimp.getPimp( ply )
	if not IsValid(ply) then return false end
	
	local pmp = ply:GetNWEntity( "pimp" )
	if pimp.isPimp( pmp ) then return pmp else return false end
--    local pimps = pimp.GetPimps()
--    if (#pimps > 0) then
--        return pimps[1]
--    else
--        return false
--    end
end

function pimp.GetCut( pmp )
	if not (IsValid(pmp) and pimp.isPimp(pmp)) then return pimp.config.pimpCut end
	
	return math.Clamp(pmp:GetNWInt( "Pimp_PimpCut", pimp.config.pimpCut ), 0, 100)
end

function pimp.notify(ply, message)
    if CLIENT then
        chat.AddText(pimp.config.color, (pimp.config.identifier .. ": "), Color(230, 230, 230), message)
    else
        net.Start("pimp.notify")
            net.WriteString(message)
        net.Send(ply)
    end
end

function pimp.isProstitute(ply)
    if !IsValid(ply) then return end
    return (ply:Team() == _G[pimp.config.ProstituteTeam])
end

function pimp.isPimp(ply)
    if !IsValid(ply) then return end
    return (ply:Team() == _G[pimp.config.PimpTeam])
end
