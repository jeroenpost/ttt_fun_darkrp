if (SERVER) then
    include("pimpsystem/sv_pimpsystem.lua")
    AddCSLuaFile("pimpsystem/cl_pimpsystem.lua")
    AddCSLuaFile("pimpsystem/sh_pimpsystem.lua")
    AddCSLuaFile("pimpsystem/sh_config.lua")

else
    include("pimpsystem/cl_pimpsystem.lua")
end