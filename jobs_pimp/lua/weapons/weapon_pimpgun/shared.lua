// Pimp System Pimp Weapon
 
 
SWEP.Author         = "Matt Walton"
SWEP.Contact        = "[RRP] Chris"
SWEP.Instructions   = "Left click to initiate request, Right click to open menu"
 
SWEP.ViewModel      = "models/weapons/v_pistol.mdl"
SWEP.ViewModelFlip  = false
SWEP.AnimPrefix     = "rpg"
SWEP.WorldModel     = "models/weapons/w_pistol.mdl"
 
SWEP.Spawnable = false
SWEP.AdminSpawnable = true
SWEP.Category = "PimpSystem"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""
 
SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""
 
if CLIENT then
    SWEP.PrintName = pimp.config.PimpGunSWEPName
    SWEP.Slot = 1
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false
else
    AddCSLuaFile("shared.lua")
end
 
function SWEP:Initialize()
    self:SetHoldType("normal")
end
 
SWEP.lastFire = 0
 
function SWEP:PrimaryAttack()
    if (self.Owner:Team() != _G[pimp.config.PimpTeam]) then
        return
    end
 
    if SERVER then
        local tr = self.Owner:GetEyeTrace()
 
        if (IsValid(tr.Entity) and tr.Entity:IsPlayer()) and (self.Owner:GetPos():Distance(tr.Entity:GetPos()) < pimp.config.PimpGunRange) then
            if (CurTime() > (self.lastFire + pimp.config.PimpGunCooldown))  then
                local ply = tr.Entity
 
                if (ply._activePimpHit) then
                                        if not pimp.activePimpHits then return end
                    for k, v in pairs(pimp.activePimpHits) do
                        if (v.pimp == self.Owner and v.ply == ply) then
                                                        if pimp.config.PimpGunCustomSound then
                                                                self.Owner:EmitSound("sound/pimpgun/raygun_fire.wav")
                                                        else
                                                                self.Owner:EmitSound("ambient/machines/teleport3.wav")
                                                        end
 
                            ply:Freeze(true)
 
                            timer.Simple(0.6, function()
                                ply:Freeze(false)
                                                                ply:TakeDamage( ply:Health()*10, self.Owner, self )
--                                ply:Kill()
                            end)
 
                            net.Start("pimp.gunFire")
                                net.WriteEntity(self.Owner)
                                net.WriteEntity(self)
                                net.WriteEntity(ply)
                                net.WriteVector(tr.HitPos)
                            net.Send(self.Owner)
 
                            net.Start("pimp.gunKill")
                                net.WriteEntity(self.Owner)
                            net.Send(ply)
 
                            table.remove(pimp.activePimpHits, k)
 
                            net.Start("pimp.syncPimpHits")
                                net.WriteTable(pimp.activePimpHits)
                            net.Send(self.Owner)
 
                            ply:PrintMessage(HUD_PRINTCENTER, self.Owner:Nick() .. " got his revenge for their prostitutes!")
                            for __, admin in pairs(player.GetAll()) do
                                if admin:IsAdmin() then
                                    admin:PrintMessage(HUD_PRINTCONSOLE, self.Owner:Nick() .. " got revenge on " .. ply:Nick() .. " for killing one of their prostitutes!")
                                end
                            end
 
                            self.lastFire = CurTime()
                        end
                    end
                 end
 
 
            else
                pimp.notify(self.Owner, "Your gun needs to recharge for another ".. math.ceil((self.lastFire + pimp.config.PimpGunCooldown)-CurTime()) .." seconds!")
            end
        end
    end
 
end
 
/*function SWEP:DrawHUD()
    if (self.firing && IsValid(self.target) && self.hitpos) then
        local attach = self.Owner:GetViewModel():LookupAttachment("muzzle")
 
        LocalPlayer():SetEyeAngles((self.hitpos - LocalPlayer():GetShootPos()):Angle())
 
        local tr = LocalPlayer():GetEyeTrace()
 
        local Pos = self.Owner:GetViewModel():GetAttachment(attach).Pos
		local Particles = ParticleEmitter( Pos )
        local I = 1
        while( I < 5 ) do
                local CentrePos = ( Pos + ( ( tr.HitPos - Pos ) * math.Rand( 0, 1 ) ) )
                local particle = Particles:Add( "sprites/light_glow02_add", CentrePos )
                particle:SetVelocity( Vector( math.random( -60, 60 ), math.random( -60, 60 ), math.random( -60, 60 ) ) )
                particle:SetDieTime( 0.4 )
                particle:SetLifeTime( 0 )
                particle:SetStartSize( 20 )
                particle:SetColor( math.random( 0, 255 ),  math.random( 0, 255 ),  math.random( 0, 255 ), 200 )
                particle:SetEndSize( 0 )
                I = I + 1
        end
        Particles:Finish()
    end
end*/
 
if CLIENT then
	local function PimpGunDrawParticles()
		for k,v in pairs( player.GetAll() ) do
			if( IsValid( v ) and v:Alive() and IsValid( v:GetActiveWeapon() ) and v:GetActiveWeapon():GetClass() == "weapon_pimpgun" and v:GetActiveWeapon().firing and IsValid( v:GetActiveWeapon().target ) and v:GetActiveWeapon().hitpos )then
				local tr = v:GetEyeTrace()
				local Pos
				if( LocalPlayer() == v ) then
					local attach = v:GetViewModel():LookupAttachment("muzzle")
					Pos = v:GetViewModel():GetAttachment(attach).Pos
					LocalPlayer():SetEyeAngles( (LocalPlayer():GetActiveWeapon().hitpos - LocalPlayer():GetShootPos()):Angle() )
				else
					Pos = v:GetActiveWeapon():GetPos()
				end
				local Particles = ParticleEmitter( Pos )
				local I = 1
				while( I <= 4 ) do
						local CentrePos = ( Pos + ( ( tr.HitPos - Pos ) * math.Rand( 0, 1 ) ) )
						particle = Particles:Add( "sprites/light_glow02_add", CentrePos )
						particle:SetVelocity( Vector( math.random( -20, 20 ), math.random( -20, 20 ), math.random( -40, 40 ) ) )
						particle:SetDieTime( 0.4 )
						particle:SetLifeTime( 0 )
						particle:SetStartSize( 15 )
						particle:SetColor( math.random( 0, 255 ),  math.random( 0, 255 ),  math.random( 0, 255 ), 200 )
						particle:SetEndSize( 0 )
						I = I + 1
				end
				Particles:Finish()		
			end
		end
	end
	hook.Add( "Think", "PimpGunDrawParticles", PimpGunDrawParticles )
end

function SWEP:SecondaryAttack()
return end