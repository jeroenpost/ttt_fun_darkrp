// Pimp System Pimp Weapon


SWEP.Author = "Matt Walton"
SWEP.Contact = "[RRP] Chris"
SWEP.Instructions = "Left click to initiate request, Right click to open menu"

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false
SWEP.AnimPrefix         = "rpg"
SWEP.WorldModel        = ""

SWEP.Spawnable = false
SWEP.AdminSpawnable = true
SWEP.Category = "PimpSystem"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""

if CLIENT then
    SWEP.PrintName = pimp.config.PimpSWEPName
    SWEP.Slot = 1
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false
else
    AddCSLuaFile("shared.lua")
end

function SWEP:Initialize()
    self:SetHoldType("normal")
end

function SWEP:Deploy()
    if not SERVER then return end

    self.Owner:DrawViewModel(false)
    self.Owner:DrawWorldModel(false)
end

local v
function SWEP:Think()
    if SERVER then
        v = self.Owner:GetViewModel()
        if IsValid(v) and (self:GetNextPrimaryFire() < CurTime()) and (v:GetSequence() != 0) then
            v:ResetSequence(0)
        end
    end
end

function SWEP:SlapAnimation()
    if SERVER then
        net.Start("pimp.slapAnimation")
            net.WriteEntity(self.Owner)
        net.Broadcast()
    end

    self:SetHoldType("melee")
    self.Owner:SetAnimation(PLAYER_ATTACK1)

    timer.Simple(0.3, function()
        if IsValid(self) then
            self:SetHoldType("normal")
        end
    end)
end

net.Receive("pimp.slapAnimation", function()
    local ply = net.ReadEntity()
    if !IsValid(ply) then return end

    local wep = ply:GetActiveWeapon()
    if !IsValid(wep) or !wep.SlapAnimation then return end

    wep:SlapAnimation()
end)

local lastSlap = 0

function SWEP:PrimaryAttack()
    if (self.Owner:Team() != _G[pimp.config.PimpTeam]) then
        return
    end

    if SERVER then
        local tr = self.Owner:GetEyeTrace()

        if (IsValid(tr.Entity) and tr.Entity:IsPlayer()) and (self.Owner:GetPos():Distance(tr.Entity:GetPos()) < 200) and pimp.isProstitute(tr.Entity) then
			if pimp.getPimp( tr.Entity )~=self.Owner then pimp.notify(self.Owner, "This isn't your prostitute!") return end
            if (CurTime() > (lastSlap + 5))  then
                local ply = tr.Entity
                ply:EmitSound("physics/body/body_medium_impact_hard1.wav")

                local pPitch = math.Rand( -20, 20 )
                local pYaw = math.sqrt( 20 * 20 - pPitch * pPitch )

                ply:ViewPunch(Angle(pPitch, pYaw, 0))
				
				ply:TakeDamage( pimp.config.slapDamage, IsValid(self.Owner) and self.Owner or self, self )

--                local h = ply:Health()
--                h = h - pimp.config.slapDamage
--
--                if (h <= 0) then
--                    ply:Kill()
--                else
--                    ply:SetHealth(h)
--                end
                lastSlap = CurTime()

            else
                pimp.notify(self.Owner, "The pimp hand must rest!")
            end
        end
    end

    self:SlapAnimation()
end

function SWEP:SecondaryAttack()
    if CLIENT then
        if (IsFirstTimePredicted()) then
            pimp.pimpWindow()
        end
    end
end