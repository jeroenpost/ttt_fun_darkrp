// Pimp System Prostitute SWEP


SWEP.Author = "Matt Walton"
SWEP.Contact = "[RRP] Chris"
SWEP.Instructions = "Left click to initiate request, Right click to open menu"

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false
SWEP.AnimPrefix         = "rpg"
SWEP.WorldModel        = ""

SWEP.Spawnable = false
SWEP.AdminSpawnable = true
SWEP.Category = "PimpSystem"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""

if CLIENT then
    SWEP.PrintName = pimp.config.ProsSWEPName
    SWEP.Slot = 1
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false
else
    AddCSLuaFile("shared.lua")
end

function SWEP:Initialize()
    self:SetHoldType("normal")
end

function SWEP:Deploy()
    if not SERVER then return end

    self.Owner:DrawViewModel(false)
    self.Owner:DrawWorldModel(false)
end


function SWEP:PrimaryAttack()
    if (self.Owner:Team() != _G[pimp.config.ProstituteTeam]) then
        return
    end

    if SERVER then
        local tr = self.Owner:GetEyeTrace()

        if (IsValid(tr.Entity) and tr.Entity:IsPlayer() and !(tr.Entity:Team() == _G[pimp.config.PimpTeam]) and !(tr.Entity:Team() == _G[pimp.config.ProstituteTeam])) then
            local target = tr.Entity

            if (IsValid(self.Owner) and (self.Owner:GetPos():Distance(target:GetPos()) < 200)) then
                self.Owner.pLastUsed = self.Owner.pLastUsed or 0

                if (self.Owner.pLastUsed > CurTime()) then
                    pimp.notify(self.Owner, "You're exhausted, you should wait a while before asking again.")
                    return
                end

                self.Owner.pLastUsed = CurTime() + pimp.config.exhaustTime
                pimp.startRequest(self.Owner, target)
            end
        end
    end
end

function SWEP:SecondaryAttack()
    if CLIENT then
        if (IsFirstTimePredicted()) then
            pimp.openMenu()
        end
    end
end