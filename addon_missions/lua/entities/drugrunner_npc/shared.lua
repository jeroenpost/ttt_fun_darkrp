ENT.Base = "base_ai"
ENT.Type = "ai"
ENT.AutomaticFrameAdvance = true
ENT.PrintName = "DarkRP Drug Run NPC"
ENT.Author = "Fat Jesus"
ENT.Category = "Fat Jesus"
ENT.Instructions = "Press E" 
ENT.Spawnable = true
ENT.AdminSpawnable = true

ENT.AutomaticFrameAdvance = true
ENT.Spawnable = true
ENT.AdminSpawnable = true
			///// **** CONFIGS ****** \\\\

MNPC = {}
AllowedJobs = {}

MNPC.DarkRP25 = true -- If you are using DarkRP 2.5 then set this to true
MNPC.AutoWanted = false -- If you want the player to get wanted as soon as the accept a job set this to true.
MissionUIConvoTitle = "Missions" -- Convasation menu title
MissionUITitle = "Missions" -- The actual menu title
MissionUIUnderTitle = "Run for Money and XP" -- The actual extra title
MissionConvoButton = "Hi, I'm here to do a Mission." -- Convasation button text
MissionConvoLeaveButton = "Hi, uh.. I think i'm lost..." -- convasation button text *leave*
MissionNPCName = "[Missions]" -- The name of the NPC
MissionNPCWantedReason = "Illegal Mission" -- The wanted reason you will get wanted for.

//On complete
MissionCompleteLine = "MISSION" -- When you complete the mission what text on line 1?
MissionCompleteLine2 = "COMPLETE" -- When you complete the mission what text on line 2?
// On fail 
MissionFailedLine = "MISSION" -- When you fail the mission what text on line 1?
MissionFailedLine2 = "FAILED" -- When you fail the mission what text on line 2?


MissionNotAllowedJobs = { -- This has to be the real team names, and when editing this, remember a comma ^^
"Batman",
"Civil Protection","S.W.A.T","Mayor","Superman","Spiderman","Venom", "Alien", "Jesus", "Alien Queen"
}

NPC_Missions = {}

NPC_Missions["Meth"] = { -- Unique name, there can only be one of this.
Name = "Illegal METH Run", -- The name that's gonna be shown in the menu.
Description = "Bring this meth with you, and escape the Police!", -- The description in the menu.
Time = 50, -- The amount of seconds you have to be running with the item.
Reward = 1500, -- The reward ^^
XP = 500,
Model = "models/props_c17/canister_propane01a.mdl", -- The model..
Scale = 0.2, -- The scale of the prop 1 is the normal size.
Ang = Angle(120, 70,65), -- The angle it's being attached on the player.
Cooldown = 180, -- How long time is the cooldown lasting.
    Wanted = true
}


NPC_Missions["Weed"] = {
Name = "Illegal WEED Run",
Description = "Bring this weed with you, and escape the Police!",
Time = 120,
Reward = 1100,
XP = 1000,
Model = "models/props/cs_office/plant01.mdl",
Scale = 0.2,
Ang = Angle(120, 70,65),
Cooldown = 120,
    Wanted = true
}

NPC_Missions["Cocaine"] = {
Name = "Illegal COCAINE Run",
Description = "Bring this bag of cocaine with you, and escape the Police",
Time = 350,
Reward = 3500,
XP = 3000,
Model = "models/props_junk/cardboard_box003a_gib01.mdl",
Scale = 0.3,
Ang = Angle(0,10,45),
Cooldown = 240,
    Wanted = true
}


NPC_Missions["swag"] = {
    Name = "Illegal SWAG Run",
    Description = "Bring this Swag with you, and escape the Police",
    Time = 60,
    Reward = 2500,
    XP = 1000,
    Model = "models/props_interiors/VendingMachineSoda01a.mdl",
    Scale = 0.1,
    Ang = Angle(95,-20,60),
    Cooldown = 900,
    Wanted = true
}

NPC_Missions["head"] = {
    Name = "Illegal Decapitated head Run",
    Description = "Bring this head with you, and escape the Police",
    Time = 300,
    Reward = 1500,
    XP = 2000,
    Model ="models/maxofs2d/balloon_gman.mdl",
    Scale = 0.5,
    Ang = Angle(95,-20,60),
    Cooldown = 900,
    Wanted = true
}

NPC_Missions["baby"] = {
    Name = "Babysitter",
    Description = "Take care of this baby for 15 minutes",
    Time = 900,
    Reward = 3500,
    XP = 10000,
    Model = "models/props_c17/doll01.mdl",
    Scale = 0.5,
    Ang = Angle(95,-20,60),
    Cooldown = 1900,
    Wanted = false
}

NPC_Missions["snorter"] = {
    Name = "Childsitter",
    Description = "Take care of this child for 15 minutes",
    Time = 900,
    Reward = 4500,
    XP = 7800,
    Model = "models/maxofs2d/companion_doll.mdl",
    Scale = 0.2,
    Ang = Angle(95,-20,60),
    Cooldown = 1900,
    Wanted = false
}
