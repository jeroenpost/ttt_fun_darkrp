AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")
/*__________________________________________________
CONFIGS!
___________________________________________________*/

/*__________________________________________________
There is also configs in shared.lua
___________________________________________________*/


function ENT:SpawnFunction( ply, tr )

if ( !tr.Hit ) then return end

local SpawnPos = tr.HitPos + tr.HitNormal * 16

	drug = ents.Create( "drugrunner_npc" )
	drug:SetPos( SpawnPos )
	drug:Spawn()
	drug:Activate()

return ent

end 


function ENT:Initialize()
	self:SetModel("models/Characters/Hostage_01.mdl")
	self:SetHullType(HULL_MEDIUM)
	self:SetHullSizeNormal()
	self:SetNPCState(NPC_STATE_SCRIPT)
	self:CapabilitiesAdd(CAP_TURN_HEAD)
	self:SetSolid(SOLID_BBOX)
	self:SetUseType(SIMPLE_USE)
	self:DropToFloor()
	
end


function ENT:AcceptInput( Name, Activator, ply)
	if Name == "Use" and ply:IsPlayer() then

	if ply.GotJob then
		ply:SendLua(
		[[
		chat.AddText( Color(75,139,191), "]]..MissionNPCName.. [[", Color(255,255,255), " You are already on a run!")]])
	    return
    end

    if ply:HasWeapon("superman_fly") or ply:HasWeapon("sanic_boom") or ply:HasWeapon("climb_swep2") or ply:HasWeapon("flight") or ply:Team() == TEAM_POKEMON or ply:Team() == TEAM_JOKER or ply:Team() == TEAM_ALIEN then
        ply:SendLua(
            [[
            chat.AddText( Color(75,139,191), "]]..MissionNPCName.. [[", Color(255,255,255), " You are too fast, mission cannot be done")]])
        return
    end

    if tonumber(ply:GetNWInt("PlayerCD",0)) > 0  then
        drugrunner_maketimer(ply)
        ply:SendLua(
            [[
            chat.AddText( Color(75,139,191), " ]]..MissionNPCName.. [[", Color(255,255,255), " You have just completed a mission, come back again in ]]..string.ToMinutesSeconds(ply:GetNWInt("PlayerCD"))..[[.")]])
        return
    end
		if not table.HasValue(MissionNotAllowedJobs, team.GetName(ply:Team())) then
			umsg.Start("drugrunner", ply)
			umsg.Bool(ply.Cooldown)
			umsg.End()
		else
			ply:SendLua(
			[[
			chat.AddText(Color(75,139,191), " ]]..MissionNPCName..[[", Color(255,255,255), " You are not allowed to do missions runs with your current job.")]])
		end
	end
end

util.AddNetworkString("AcceptRun")
net.Receive("AcceptRun", function(len, ply)
	ID = net.ReadString()

    local RequiredTeamsCount = 0
    local RequiredPlayersCounted = 0



    if NPC_Missions[ID] and NPC_Missions[ID].Wanted then
        for k, v in pairs(player.GetAll()) do
            RequiredPlayersCounted = RequiredPlayersCounted + 1

            if table.HasValue( RequiredTeams, team.GetName(v:Team()) ) then
                RequiredTeamsCount = RequiredTeamsCount + 1
            end

            if RequiredPlayersCounted == #player.GetAll() then
                if RequiredTeamsCount < 1 then
                    DarkRP.notify(ply, 1, 5, "There has to be one police officer before you can do an illegal mission.")
                    return
                end
            end
        end
    end

    runner = ply
    ply.CModel = ents.Create("prop_physics")
    ply.GotJob = true
    ValidRunner()

    	umsg.Start("TheModel",ply)
	umsg.String(ID)
	umsg.Entity(runner)
	umsg.End()


	
	
	ply.ThirdPerson = true
	ply:SetNWFloat("JobTime",NPC_Missions[ID].Time)
	
	timer.Create("DoThis"..ply:EntIndex(),1,0,function()
        if not IsValid(ply) then return end
		ply:SetNWFloat("JobTime", ply:GetNWFloat("JobTime") - 1)
	end)
	if NPC_Missions[ID].Wanted then
        if MNPC.DarkRP25 then
            ply:wanted(nil, MissionNPCWantedReason)
        else
            ply:SetDarkRPVar("wanted", true)
            ply:SetDarkRPVar("wantedReason", MissionNPCWantedReason)
        end
    end
    ply:SetNWBool("mission_started", true)
    timer.Create("CompleteTheRun"..ply:EntIndex().."timer",1,NPC_Missions[ID].Time, function()
        if not IsValid(ply) then return end
        if not ply.timercountdownmission or  ply.timercountdownmission < 1 then
            ply.timercountdownmission = NPC_Missions[ID].Time
        end

        ply.timercountdownmission =  ply.timercountdownmission - 1
        ply:SetNWFloat("MissionCountDown",  ply.timercountdownmission-1)
        ply:SetNWInt("PlayerCD", ply.timercountdownmission-1)
    end)
	timer.Create("CompleteTheRun"..ply:EntIndex().."timer2",(NPC_Missions[ID].Time+1),1, function()
        if not IsValid(ply) then return end
        timer.Destroy("CompleteTheRun"..ply:EntIndex().."timer")
        -- COOLDOWN
        ply:SetNWInt("PlayerCD", NPC_Missions[ID].Cooldown)
        ply:SetPData("PlayerCD",NPC_Missions[ID].Cooldown)

        timer.Create("DoCD"..ply:EntIndex(),1,0,function()
            if not IsValid(ply) or tonumber(ply:GetNWInt("PlayerCD",0)) <= 0 then return end
            ply:SetNWInt("PlayerCD", tonumber(ply:GetNWInt("PlayerCD",0)) - 1)
        end)
        timer.Create("DoCD2"..ply:EntIndex(),60,0,function()
            if not IsValid(ply) or ply:GetNWInt("PlayerCD") <= 0 then return end
            ply:SetPData("PlayerCD",ply:GetNWInt("PlayerCD"))
        end)
        --
        ply.Complete = true
        ply.CModel:Remove()

        ply:EmitSound("ambient/machines/slicer1.wav",100,100)

        timer.Simple(5,function()
        ply.Complete = false
        end)
        ply.ThirdPerson = false
        ply.GotJob = false

        if MNPC.DarkRP25 then
            ply:addMoney(NPC_Missions[ID].Reward)
            ply:addXP(NPC_Missions[ID].XP)
            if ply:isWanted() then
                ply:unWanted()
            end
            ply:SetNWBool("mission_started", false)
            hook.Call("mission_"..ID.."_done", GAMEMODE, ply)
        else
            ply:AddMoney(NPC_Missions[ID].Reward)
            ply:addXP(NPC_Missions[ID].XP)
        end
            ply:SendLua(
            [[
            chat.AddText( Color(75,139,191), "]]..MissionNPCName..[[" , Color(255,255,255), " You have completed the mission, therefore you have been rewarded with ]]..NPC_Missions[ID].XP..[[XP and $]]..NPC_Missions[ID].Reward..[[.")]])
	end)

    hook.Call("casinoowner_getmoney",GAMEMODE, 15)
end)

function ValidRunner()
	local pos = Vector()
	local ang = Angle()
	
	if not runner.GotJob then return end
	local BoneIndx = runner:LookupBone("ValveBiped.Bip01_Spine2")
    if BoneIndx then
	    pos, ang = runner:GetBonePosition(BoneIndx)
        if pos and ang then
            runner.CModel:SetPos(pos + runner:GetAngles():Forward() * - 7 )
            runner.CModel:SetAngles(ang - NPC_Missions[ID].Ang)
            runner.CModel:SetModel(NPC_Missions[ID].Model)
            runner.CModel:SetParent(runner)
            runner.CModel:DrawShadow(false)
            runner.CModel:SetModelScale(NPC_Missions[ID].Scale,0)
        end
    end
end

hook.Add("PlayerInitialSpawn","missionsPlayertimetimer",function(ply)
    local cooldown = tonumber(ply:GetPData("PlayerCD",0) or 0)
    ply:SetNWInt("PlayerCD", cooldown )
    if cooldown > 0 then
        drugrunner_maketimer(ply)
    end
end)

function drugrunner_maketimer(ply)
    if not timer.Exists("DoCD_gb"..ply:EntIndex()) then
        timer.Create("DoCD_gb"..ply:EntIndex(),1,0,function()
            if not IsValid(ply) then return end
            ply:SetNWInt("PlayerCD", tonumber(ply:GetNWInt("PlayerCD")) - 1)
            if tonumber(ply:GetNWInt("PlayerCD")) < 1 then
                ply:SetPData("PlayerCD",0)
                ply:SetNWInt("PlayerCD",0)
                timer.Destroy("DoCD_gb"..ply:EntIndex())
            end
        end)
    end
end

hook.Add('PlayerDeath','playerisdeeeeeeeeeead', function(ply)
	if not ply.GotJob then return end
	Death(ply)
end)	

function Death(ply)
timer.Stop("DoThis"..ply:EntIndex())
timer.Stop("CompleteTheRun"..ply:EntIndex())
timer.Stop("CompleteTheRun"..ply:EntIndex().."timer")
ply.ThirdPerson = false
ply.GotJob = false
ply.Complete = false
ply.Failed = true
ply:SetNWInt("PlayerCD",120)
ply:SetNWBool("mission_started", false)
	if MNPC.DarkRP25 then
        if ply:isWanted() then ply:unWanted(nil) end
	else
		ply:SetDarkRPVar("wanted", false)
	end
ply.CModel:Remove()

end

hook.Add("playerArrested","missionsarrested_reset",function(ply,thetime,ply2)
    if not ply.GotJob then return end
    Death(ply)
end)


function ENT:Think()
	for k,v in pairs(player.GetAll()) do
		if v.Failed or v.Completed then 
			timer.Simple(5,function()
				v.Failed = false
				v.Completed = false
			end)
		end
    end
    self:NextThink( CurTime() + 3 )
    return true
end
