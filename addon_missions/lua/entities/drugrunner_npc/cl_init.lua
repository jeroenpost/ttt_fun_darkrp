include("shared.lua")

function ENT:Initialize()
end


surface.CreateFont( "LeFixFont", {
 font = "HUDNumber5",
 size = 21,
 weight = 9000,
} )

surface.CreateFont( "LeFixiFont", {
 font = "HUDNumber5",
 size = 23,
 weight = 9000,
} )


surface.CreateFont( "LeFixFont2", {
 font = "HUDNumber5",
 size = 16,
 weight = 9000,
} )

surface.CreateFont( "LeFixFont3", {
 font = "HUDNumber5",
 size = 18,
 weight = 9000,
} )

function ENT:Draw()
	self:DrawModel()
	--ply.CModel:DrawModel()
end

function ENT:Think()
end

local function NPCDisplay()
	for k, v in pairs(ents.FindByClass( "drugrunner_npc" )) do
        if v:GetPos():Distance(LocalPlayer():GetPos()) < 500 then
            local Pos = v:GetPos()
            local Ang = v:GetAngles()
            Ang:RotateAroundAxis(Ang:Forward(), 90)
            Ang:RotateAroundAxis(Ang:Right(), 270)

            cam.Start3D2D(Pos + Ang:Up() * 5 + Ang:Right() * -82, Ang, 0.2)
                draw.RoundedBox( 0, -60, 6, 120, 20, Color( 75, 139, 191, 255))
                draw.RoundedBox( 0, -60, 26, 120, 20, Color( 34, 34, 34, 200))
                draw.SimpleTextOutlined(MissionNPCName, "LeFixFont", 0, 5, Color(224,224,224,255 ), 1, 0, 0, Color( 0, 0, 0 ) )

                draw.SimpleTextOutlined(string.ToMinutesSeconds(LocalPlayer():GetNWInt("PlayerCD")), "LeFixFont", 0, 25, Color(224,224,224,255 ), 1, 0, 0, Color( 0, 0, 0 ) )

            cam.End3D2D()
        end
	end
end
hook.Add("PostDrawOpaqueRenderables", "NPCDisplay", NPCDisplay)


usermessage.Hook( "drugrunner", function( um )
	Cooldown = um:ReadBool()
	local ConvoFrame = vgui.Create( "DFrame" ); //Create a frame
    ConvoFrame:SetSize( 300, 130 ); //Set the size to 200x200
    ConvoFrame:SetPos( -250, ScrH()/2 + 200 ); //Move the frame to 100,100
	ConvoFrame:MoveTo(ScrW()/2 - 150, ScrH()/2 + 200, 0.4, 0, 5)
    ConvoFrame:SetVisible( true );  //Visible
    ConvoFrame:MakePopup( ); //Make the frame
	ConvoFrame:SetTitle("")
	ConvoFrame:ShowCloseButton( false )
	ConvoFrame.Paint = function()
	draw.RoundedBox( 0, 0, 0, ScrW(), 600, Color( 0, 0, 0, 240))
	draw.RoundedBox( 0, 0, 0, 525, 32, Color( 75, 139, 191, 255))
	draw.SimpleTextOutlined(MissionUIConvoTitle, "LeFixiFont", 145, 3.3,Color(224,224,224), TEXT_ALIGN_CENTER,0,0,Color(0,0,0))
	end

	local ConvoExit = vgui.Create("DButton",ConvoFrame)
	ConvoExit:SetPos(275, 0)
	ConvoExit:SetSize(25,25)
	ConvoExit:SetText("")
	ConvoExit.Paint = function()
		draw.SimpleTextOutlined("X", "Trebuchet24", 5, 2.5,Color(255,255,255), TEXT_ALIGN_CENTER,0,0,Color(0,0,0))
	end
	ConvoExit.DoClick = function()
	ConvoFrame:Remove()
	end
	
	local ConvoBT = vgui.Create("DButton",ConvoFrame)
	ConvoBT:SetPos(5, 64.5)
	ConvoBT:SetSize(290,25)
	ConvoBT:SetText("")
	ConvoBT.Paint = function()
		draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( 224, 224, 224, 255))
		draw.SimpleTextOutlined(MissionConvoButton, "Trebuchet24", 145, 1,Color(34,34,34), TEXT_ALIGN_CENTER,0,0,Color(0,0,0))
	end
	ConvoBT.DoClick = function()
		ConvoFrame:Remove()
		local sFrame = vgui.Create("DFrame")
		sFrame:SetSize(540,500)
		sFrame:SetPos(ScrW()/2 - 270, ScrH()/2 + 200)
		sFrame:MoveTo(ScrW()/2-270,ScrH()- 750, 0.4,0,5)	
		sFrame:SetVisible(true)
		sFrame:MakePopup()
		sFrame:SetTitle("")
		sFrame.Paint = function()
			draw.RoundedBox( 0, 20, 0, 500, ScrH(), Color( 34, 34, 34, 210))
			draw.RoundedBox( 0, 0, 0, 550, 85, Color( 75, 139, 191, 255))
			draw.RoundedBox( 0, 8, 25, 60, 50, Color( 224, 224, 224, 255))
			draw.RoundedBox( 0, 8, 10, 60, 8, Color( 224, 224, 224, 255))
			
			texture = surface.GetTextureID("vgui/white");
			
			surface.SetTexture(texture)
		    surface.SetDrawColor(75,139,191,255)
		    surface.DrawTexturedRectRotated(65,10,10,25,-130)
		    surface.DrawTexturedRectRotated(45,43,5,20,-135)
		    surface.DrawTexturedRectRotated(35,53,20,20,-135)
		  --  surface.DrawTexturedRectRotated(35,53,30,7,45)

			
			draw.SimpleTextOutlined(MissionUITitle, "HUDNumber5", 77, 18,Color(224,224,224), TEXT_ALIGN_LEFT,0,0,Color(0,0,0))
			draw.SimpleTextOutlined(MissionUIUnderTitle, "Trebuchet24", 114, 55,Color(224,224,224), TEXT_ALIGN_LEFT,0,0,Color(0,0,0))
			draw.SimpleTextOutlined("JOBS", "Trebuchet22", 40, 88,Color(224,224,224), TEXT_ALIGN_LEFT,0,0,Color(224,224,224))
			
		end
		local EXIT = vgui.Create("DButton",sFrame)
		EXIT:SetPos(440, 0)
		EXIT:SetSize(100,25)
		EXIT:SetText("")
		EXIT.Paint = function()
		--draw.RoundedBox( 0, 0, 2,ScrW(), 110, Color( 30, 30, 40, 255))
		draw.RoundedBox( 0, 0, 2, ScrW(), 110, Color( 75, 139, 191, 255))
		--draw.RoundedBox( 0, 1, 1,288, 23, Color( 0, 73, 153, 255))
		draw.SimpleTextOutlined("X", "Trebuchet24", 80, 1,Color(255,255,255), TEXT_ALIGN_CENTER,0,0,Color(0,0,0))
		end
		EXIT.DoClick = function()
		sFrame:Remove()
		end
			
		local DermaList = vgui.Create( "DPanelList", sFrame)
		DermaList:SetPos( 40,113)
		DermaList:SetSize( 480, 360 )
		DermaList:SetSpacing( 7 ) -- Spacing between items
		DermaList:EnableHorizontal( true ) -- Only vertical items
		DermaList:EnableVerticalScrollbar( false ) -- Allow scrollbar if you exceed the Y axis
		
		DermaList.VBar.btnUp.Paint = function()
		--draw.RoundedBox( 0, 0, 0, 525, 45, Color( 34, 34, 34, 255))
		end
		
		DermaList.VBar.btnDown.Paint = function()
		--draw.RoundedBox( 0, 0, 0, 525, 45, Color( 34, 34, 34, 255))
		end
		
		DermaList.VBar.btnGrip.Paint = function()
		draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( 224, 224, 224, 255))
		end
		
		DermaList.VBar.Paint = function()
		draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( 34, 34, 34, 255))
		end
		
		
		for k,v in pairs(NPC_Missions) do
		local DrugFrame = vgui.Create( "DPanel", DermaList )
		DrugFrame:SetSize(450, 150)
		DrugFrame:SetPos(2,0)
		DrugFrame.Paint = function()
		
		draw.RoundedBox( 0, 0, 0, 450, 27.5, Color( 75, 139, 191, 255))
		draw.RoundedBox( 0, 0, 27.5, ScrW(), ScrH(), Color( 34, 34, 34, 235))
		
		draw.RoundedBox( 0, 5, 90, 9, 20, Color( 224, 224, 224, 255))
		draw.RoundedBox( 0, 5, 101, 20, 9, Color( 224, 224, 224, 255))
		draw.RoundedBox( 0, 17, 90, 8, 8, Color( 224, 224, 224, 255))
		

		draw.RoundedBox( 0, 5, 122, 20, 8, Color( 224, 224, 224, 255))
		draw.RoundedBox( 0, 10, 118, 9, 8, Color( 224, 224, 224, 255))
		draw.RoundedBox( 0, 10, 127, 9, 8, Color( 224, 224, 224, 255))


		draw.NoTexture()
		surface.SetDrawColor(224,224,224,255)
		surface.DrawTexturedRectRotated(10,122,6,6, 45)
		surface.DrawTexturedRectRotated(10,130,6,6, 45)

		
		surface.DrawTexturedRectRotated(20,122,6,6, -45)
		surface.DrawTexturedRectRotated(20,130,6,6, -45)


		draw.SimpleTextOutlined(v.Name, "Trebuchet24", 7.5, 2,Color(224,224,224), TEXT_ALIGN_LEFT,0,0,Color(0,0,0))
		draw.SimpleTextOutlined(string.ToMinutesSeconds(v.Time), "Trebuchet22", 30, 88,Color(255,255,255), TEXT_ALIGN_LEFT,0,0,Color(224,224,224))
		draw.SimpleTextOutlined("$"..v.Reward.." + "..v.XP.."XP", "Trebuchet22", 30, 115,Color(255,255,255), TEXT_ALIGN_LEFT,0,0,Color(0,0,0))
		end
		DermaList:AddItem(DrugFrame)
			
		local Description = vgui.Create("DLabel", DrugFrame)
		Description:SetPos(10,-5)
		Description:SetSize(380, 110)
		Description:SetFont("LeFixFont2")
		Description:SetColor(Color(224,224,224))
		Description:SetWrap(true)
		Description:SetText(v.Description)
	
		local ConvoBT = vgui.Create("DButton",DrugFrame)
		ConvoBT:SetPos(340, 150-27)
		ConvoBT:SetSize(110,27)
		ConvoBT:SetText("")
		ConvoBT.Paint = function()
			draw.RoundedBox( 0, 0, 0,ScrW(), ScrH(), Color( 224, 224, 224, 255))

			draw.SimpleTextOutlined("ACCEPT", "Trebuchet24", 110/2, 2,Color(34,34,34), TEXT_ALIGN_CENTER,0,0,Color(0,0,0))
		
		end
		ConvoBT.DoClick = function()
		sFrame:Remove()
		if LocalPlayer():GetNWInt("PlayerCD") > 0 then return end
		net.Start("AcceptRun")
		net.WriteString(k)
		
		net.SendToServer()
		end
		
		end
		end

	local ConvoBT = vgui.Create("DButton",ConvoFrame)
	ConvoBT:SetPos(5, 97.5)
	ConvoBT:SetSize(290,25)
	ConvoBT:SetText("")
	ConvoBT.Paint = function()
		draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( 224, 224, 224, 255))
		draw.SimpleTextOutlined(MissionConvoLeaveButton, "Trebuchet24", 145, 1,Color(34,34,34), TEXT_ALIGN_CENTER,0,0,Color(0,0,0))
	end
	ConvoBT.DoClick = function()
	ConvoFrame:Remove()
	end
	
	
end)
