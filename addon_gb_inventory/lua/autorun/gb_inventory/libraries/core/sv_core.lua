-----------------------------------------------------------------------------
--	Database
---------------------------------------------------------------------------*/
gb_inventory.database = {}


function gb_inventory.database.addPlayerData( ply )
	local emptyInventory = {}
	for x = 1, gb_inventory.config.ySize do
		emptyInventory[ x ] = {}
		for y = 1, gb_inventory.config.xSize do
			emptyInventory[ x ][ y ] = false
		end
	end
    local q = string.format([[INSERT INTO gb_inventory ( steam, items ) VALUES(%s, %s);]], MySQLite.SQLStr(ply:SteamID()), MySQLite.SQLStr(util.TableToJSON( emptyInventory )))
    MySQLite.query(q)
	gb_inventory.database.fetchPlayerData( ply )
end

function gb_inventory.database.fetchPlayerData( ply )
     MySQLite.query( "SELECT items FROM gb_inventory WHERE steam = " .. MySQLite.SQLStr(ply:SteamID()) .. " LIMIT 1",function(q)
        if not q then
            gb_inventory.database.addPlayerData( ply )
        else
            ply.gb_inventory = util.JSONToTable( q[1].items )
            gb_inventory.sendPlayerData( ply )
        end
    end )
end

function gb_inventory.database.savePlayerData( ply )
	gb_inventory.removeInvalidItems( ply )
    MySQLite.query(string.format([[UPDATE gb_inventory SET items = %s WHERE steam = %s;]], MySQLite.SQLStr(util.TableToJSON( ply.gb_inventory )), MySQLite.SQLStr( ply:SteamID() )))
end

--USE ONLY IF YOU KNOW THAT YOU WANT TO GET RID OF WHOLE DATABASE AS THIS WIPES IT OUT
function gb_inventory.database.drop( ply, cmd, args )
	if not ply:IsSuperAdmin() then return end
	--sql.Query( "DROP TABLE gb_inventory" )
end
concommand.Add( "gb_inventory_dropdatabase", gb_inventory.database.drop )

-----------------------------------------------------------------------------
--	Functionality
---------------------------------------------------------------------------*/
function gb_inventory.GetInventorySize(ply)

    if (  gb.compare_rank(ply, {"vippp"})) then
        return( gb_inventory.config.InventorySizes["vippp" ] )
    end
    if (  gb.compare_rank(ply, {"vipp"})) then
        return( gb_inventory.config.InventorySizes["vipp" ] )
    end
    if (  gb.compare_rank(ply, {"vip"})) then
        return ( gb_inventory.config.InventorySizes["vip" ] )
    end

    return gb_inventory.config.InventorySizes['normal']
end

function gb_inventory.addItem( ply, class, model, amount, data )
    local item = gb_inventory.getItemEntry( class )
    if gb_inventory.getFreeSlots( ply, class ) >= 1 then

        local repeats = 0
        while( amount > 0 and repeats < 100 )
        do
            local x, y, space = gb_inventory.getFreeSlot( ply, class, data, amount )
            if ply.gb_inventory[ x ][ y ] == false then
                local newItem = {}
                newItem.mdl = model
                newItem.class = class
                newItem.data = data
                newItem.amount = 0
                if amount > gb_inventory.config.items[ class ].stackSize then

                    newItem.amount = gb_inventory.config.items[ class ].stackSize
                   amount = amount - gb_inventory.config.items[ class ].stackSize
                else
                    newItem.amount = amount
                    amount = 0
                end
                ply.gb_inventory[ x ][ y ] = newItem
            else
                ply.gb_inventory[ x ][ y ].amount = ply.gb_inventory[ x ][ y ].amount + amount
                amount = 0
                if ply.gb_inventory[ x ][ y ].amount > gb_inventory.config.items[ class ].stackSize then
                    amount = ply.gb_inventory[ x ][ y ].amount - gb_inventory.config.items[ class ].stackSize
                    ply.gb_inventory[ x ][ y ].amount = gb_inventory.config.items[ class ].stackSize
                end
            end
            repeats = repeats + 1
        end
        ply:EmitSound( "items/itempickup.wav" )
    end
    gb_inventory.sendPlayerData( ply )
    gb_inventory.database.savePlayerData( ply )
end

function gb_inventory.removeItem( ply, itemPos, amount )
    local x, y = itemPos[1], itemPos[2]
    if ply.gb_inventory[ x ][ y ] != false then
    ply.gb_inventory[ x ][ y ].amount = ply.gb_inventory[ x ][ y ].amount - amount or ply.gb_inventory[ x ][ y ].amount
    if ply.gb_inventory[ x ][ y ].amount == 0 then
        ply.gb_inventory[ x ][ y ] = false
    end
    end
    gb_inventory.database.savePlayerData( ply )
end


function gb_inventory.storeItem( ply, ent )
    if not ply.nextpickupimventory then ply.nextpickupimventory = 0 end
    if ply.nextpickupimventory > CurTime() then return end
	if gb_inventory.isItem( ent ) then
        ply.nextpickupimventory = CurTime() + 0.25
		local item = gb_inventory.getItemEntry( ent:GetClass() )
        if item.CanPickup and not item.CanPickup(ply,ent) then return end
		if gb_inventory.getFreeSlots( ply, class ) > 0 then
			local data = {}

            if item.pickupFunc then
                item.pickupFunc(item, ply, ent)
            end


			if item.vars then
				for _, var in pairs ( item.vars ) do
					data[ var ] = ent[ var ] or ent.dt[var ]
				end
            end

			gb_inventory.addItem( ply, ent:GetClass(), ent:GetModel(), item.fetchAmount( ent ) or 1, data )
			ent:Remove()
        else
            DarkRP.notify(ply, 2, 5,  "Your inventory is full.")
            ply:ChatPrint("Did you know VIP gives you more slots for your inventory? Type !donate for more info!")
        end
	end
end
hook.Add( "KeyPress", "gb_inventory_storeItem", function( ply, key )
	if key == gb_inventory.config.pickupKey1 and ply:KeyDown( gb_inventory.config.pickupKey2 ) then
		if ply:GetEyeTrace().Entity:GetPos():Distance( ply:GetPos() ) < gb_inventory.config.takeDist then
			gb_inventory.storeItem( ply, ply:GetEyeTrace().Entity, 1 )
		end
	end
end )

function gb_inventory.removeInvalidItems( ply )
	for x = 1, gb_inventory.config.ySize do
		for y = 1, gb_inventory.config.xSize do
			if ply.gb_inventory[ x ][ y ] != false then
				if not gb_inventory.config.items[ ply.gb_inventory[ x ][ y ].class ] then
					ply.gb_inventory[ x ][ y ] = false
				elseif ( ply.gb_inventory[ x ][ y ].amount > gb_inventory.config.items[ ply.gb_inventory[ x ][ y ].class ].stackSize ) or ( ply.gb_inventory[ x ][ y ].amount < 0 ) then
					ply.gb_inventory[ x ][ y ] = false
                elseif ( ply.gb_inventory[ x ][ y ].class == "spawned_weapon" and (not ply.gb_inventory[ x ][ y ].data or ply.gb_inventory[ x ][ y ].data == {}) ) then
                    ply.gb_inventory[ x ][ y ] = false
                end
			end
		end
	end
end

function gb_inventory.invholster( pl, args )
    local wep = pl:GetActiveWeapon()

    if (pl.NextInvHolster and pl.NextInvHolster > CurTime()) or (not IsValid(wep) or not wep:GetModel() or wep:GetModel() == "" or (wep.Primed && wep.Primed != 0)) then
        DarkRP.notify(pl, 1, 4, DarkRP.getPhrase("cannot_drop_weapon"))
        return ""
    end

    if GAMEMODE.Config.restrictdrop then
        local found = false
        for k,v in pairs(CustomShipments) do
            if v.entity == wep:GetClass() then
                found = true
                break
            end
        end

        if not found then return end
    end

    local canDrop = hook.Call("canDropWeapon", GAMEMODE, pl, wep)
    if not canDrop then
        DarkRP.notify(pl, 1, 4, DarkRP.getPhrase("cannot_drop_weapon"))
        return ""
    end
    -- and back to our regularly scheduled coding

    pl:addItem( "spawned_weapon", wep:GetModel(), 1, { ["WeaponClass"] = wep:GetClass(),["amount"] = 1,["clip1"] = wep.clip1,["clip2"] = wep.clip2,["ammoadd"] = wep.ammoadd} )

    pl:StripWeapon( wep:GetClass() )

end
hook.Add( "PostGamemodeLoaded", "gb_invinvholster", function()
DarkRP.defineChatCommand("invholster", gb_inventory.invholster)
end)

function gb_inventory.hasMatchingData( class, data1, data2 )
	if gb_inventory.config.items[ class ].vars then
		for var, value in pairs ( data1 ) do
			if data2[ var ] != value then
				return false
			end
		end
	end
	return true
end

util.AddNetworkString( "gb_Inv_dropItem" )
function gb_inventory.dropItem( ply, itemPos )
    if ply:isArrested() then return false end

	local x, y = itemPos[1], itemPos[2]
	local itemData = gb_inventory.getItemEntry( ply.gb_inventory[ x ][ y ].class )
	local item = ply.gb_inventory[ x ][ y ]
    if itemData.CanDrop and not itemData.CanDrop(ply, item) then return end

	local ent = ents.Create( item.class )

    if not ent.dt then ent.dt = {} end
	for var, value in pairs ( item.data ) do
		ent[ var ] = value
		ent.dt[ var ] = value
    end

    if itemData.dropFunc then
       itemData.dropFunc( ply, ply.gb_inventory[ x ][ y ], ent )
    end


    ply.gb_inventory[ x ][ y ].amount = ply.gb_inventory[ x ][ y ].amount - 1

	if ply.gb_inventory[ x ][ y ].amount <= 0 then
		ply.gb_inventory[ x ][ y ] = false
    end

    ent:SetPos( LocalToWorld( Vector( 50, 0, 35 ), Angle( 0, 0, 0 ), ply:GetPos(), ply:GetAngles() ) )
    ent:SetModel( item.mdl )
    ent.FPPOwner = ply
	ent:Spawn()



	gb_inventory.sendPlayerData( ply )
	gb_inventory.database.savePlayerData( ply )
end
net.Receive( "gb_Inv_dropItem", function( lenght, ply )
	local pos = net.ReadTable()
	gb_inventory.dropItem( ply, pos )
end )

util.AddNetworkString( "gb_Inv_useItem" )
function gb_inventory.useItem( ply, itemPos )
    if ply:isArrested() then return false end
	local x, y = itemPos[1], itemPos[2]
	local itemData = gb_inventory.getItemEntry( ply.gb_inventory[ x ][ y ].class )
	if itemData.useFunc then
        if itemData.canUseFunc and not itemData.canUseFunc(ply, ply.gb_inventory[ x ][ y ]) then
            return
        end
		ply.gb_inventory[ x ][ y ].amount = ply.gb_inventory[ x ][ y ].amount - 1
		itemData.useFunc( ply, ply.gb_inventory[ x ][ y ] )

		if ply.gb_inventory[ x ][ y ].amount <= 0 then
			ply.gb_inventory[ x ][ y ] = false
		end
		gb_inventory.sendPlayerData( ply )
	end
	gb_inventory.database.savePlayerData( ply )
end
net.Receive( "gb_Inv_useItem", function( lenght, ply )
	local pos = net.ReadTable()
	gb_inventory.useItem( ply, pos )
end )

util.AddNetworkString( "gb_Inv_moveItem" )
function gb_inventory.moveItem( ply, fromPos, toPos )
	local x, y = fromPos[1], fromPos[2]
	local nx, ny = toPos[1], toPos[2]

	if x == nx and y == ny then return end

	if ply.gb_inventory[ x ][ y ] == false then
		return
	end

	if ply.gb_inventory[ nx ][ ny ] == false then
		ply.gb_inventory[ nx ][ ny ] = table.Copy( ply.gb_inventory[ x ][ y ] )
		ply.gb_inventory[ nx ][ ny ].amount = 1
		ply.gb_inventory[ x ][ y ].amount = ply.gb_inventory[ x ][ y ].amount - 1
		if ply.gb_inventory[ x ][ y ].amount <= 0 then
			ply.gb_inventory[ x ][ y ] = false
		end
	else
		if ply.gb_inventory[ nx ][ ny ].class == ply.gb_inventory[ x ][ y ].class and gb_inventory.hasMatchingData( ply.gb_inventory[ x ][ y ].class, ply.gb_inventory[ x ][ y ].data, ply.gb_inventory[ nx ][ ny ].data ) then
			if ply.gb_inventory[ nx ][ ny ].amount < gb_inventory.config.items[ ply.gb_inventory[ nx ][ ny ].class ].stackSize then
				ply.gb_inventory[ nx ][ ny ].amount = ply.gb_inventory[ nx ][ ny ].amount + 1
				ply.gb_inventory[ x ][ y ].amount = ply.gb_inventory[ x ][ y ].amount - 1
				if ply.gb_inventory[ x ][ y ].amount <= 0 then
					ply.gb_inventory[ x ][ y ] = false
				end
			end
		end
	end

	gb_inventory.sendPlayerData( ply )
	gb_inventory.database.savePlayerData( ply )
end
net.Receive( "gb_Inv_moveItem", function( lenght, ply )
	--local pos = net.ReadTable()
	--gb_inventory.moveItem( ply, pos[1], pos[2] )
end )

util.AddNetworkString( "gb_Inv_moveStack" )
function gb_inventory.moveStack( ply, fromPos, toPos )
	local x, y = fromPos[1], fromPos[2]
	local nx, ny = toPos[1], toPos[2]

	if x == nx and y == ny then return end

	if ply.gb_inventory[ x ][ y ] == false then
		return
	end

	if ply.gb_inventory[ nx ][ ny ] == false then
		ply.gb_inventory[ nx ][ ny ] = ply.gb_inventory[ x ][ y ]
		ply.gb_inventory[ x ][ y ] = false
	else
		if ply.gb_inventory[ nx ][ ny ].class == ply.gb_inventory[ x ][ y ].class and gb_inventory.hasMatchingData( ply.gb_inventory[ x ][ y ].class, ply.gb_inventory[ x ][ y ].data, ply.gb_inventory[ nx ][ ny ].data ) then
			ply.gb_inventory[ nx ][ ny ].amount = ply.gb_inventory[ nx ][ ny ].amount + ply.gb_inventory[ x ][ y ].amount
			if ply.gb_inventory[ nx ][ ny ].amount > gb_inventory.config.items[ ply.gb_inventory[ nx ][ ny ].class ].stackSize then
				ply.gb_inventory[ x ][ y ].amount = ply.gb_inventory[ nx ][ ny ].amount - gb_inventory.config.items[ ply.gb_inventory[ nx ][ ny ].class ].stackSize
				ply.gb_inventory[ nx ][ ny ].amount = gb_inventory.config.items[ ply.gb_inventory[ nx ][ ny ].class ].stackSize
			else
				ply.gb_inventory[ x ][ y ] = false
			end
		else
			local oldItem = ply.gb_inventory[ nx ][ ny ]
			ply.gb_inventory[ nx ][ ny ] = ply.gb_inventory[ x ][ y ]
			ply.gb_inventory[ x ][ y ] = oldItem
		end
	end
	gb_inventory.sendPlayerData( ply )
	gb_inventory.database.savePlayerData( ply )
end
net.Receive( "gb_Inv_moveStack", function( lenght, ply )
	local pos = net.ReadTable()
	gb_inventory.moveStack( ply, pos[1], pos[2] )
end )

util.AddNetworkString( "gb_Inv_openInventory" )
function gb_inventory.open( ply )
	gb_inventory.removeInvalidItems( ply )
	gb_inventory.sendPlayerData( ply )
	net.Start( "gb_Inv_openInventory" )
	net.Send( ply )
end


concommand.Add("open_inventory", function(ply, cmd, arg) gb_inventory.open(ply) end)

local ShowCursor
-- open with F2
--function gb_inventory.PlayerBindPress( ply )
--        gb_inventory.open(ply)
--end
--hook.Add("ShowTeam","gb_inventory_openkey",gb_inventory.PlayerBindPress)



function gb_inventory.isItem( ent )
	if gb_inventory.config.items[ ent:GetClass() ] then
		return true
	else
		return false
	end
end

function gb_inventory.hasItem( ply, itemClass )
	local inventory = gb_inventory.getInventory( ply )
	for x = 1, gb_inventory.config.ySize do
		for y = 1, gb_inventory.config.xSize do
			if inventory[ x ][ y ] != false then
				if inventory[ x ][ y ].class == itemClass then
					return true
				end
			end
		end
	end
	return false
end

function gb_inventory.getInventory( ply )
	return ply.gb_inventory
end

function gb_inventory.getItemEntry( class )
	return gb_inventory.config.items[ class ] or false
end

function gb_inventory.getFreeSlots( ply, class, data )
	local space = 0



	for x = 1, ply.gb_inventory_sizeX do
		for y = 1, ply.gb_inventory_sizeY do
			if ply.gb_inventory[ x ][ y ] == false then
				space = space + 1
			else
				if class then
					if ply.gb_inventory[ x ][ y ].class == class and ply.gb_inventory[ x ][ y ].amount < gb_inventory.config.items[ class ].stackSize then
						if data then
							if gb_inventory.hasMatchingData( class, data, ply.gb_inventory[ x ][ y ].data ) then
								space = space + 1
							end
						else
							space = space + 1
						end
					end
				end
			end
		end
    end
	return space
end

function gb_inventory.getFreeSlot( ply, class, data, amount )
	--Check for items without full stacks first
    for x = 1, ply.gb_inventory_sizeX do
        for y = 1, ply.gb_inventory_sizeY do
			if ply.gb_inventory[ x ][ y ] != false then
				if ply.gb_inventory[ x ][ y ].class == class then
					if gb_inventory.config.items[ class ].stackSize - ply.gb_inventory[ x ][ y ].amount > 0 then
						if gb_inventory.hasMatchingData( class, ply.gb_inventory[ x ][ y ].data, data ) then
							return x, y, gb_inventory.config.items[ class ].stackSize - ply.gb_inventory[ x ][ y ].amount
						end
					end
				end
			end
		end
	end
	--Check for empty slots
    for x = 1, ply.gb_inventory_sizeX do
        for y = 1, ply.gb_inventory_sizeY do
			if ply.gb_inventory[ x ][ y ] == false then
				return x, y, gb_inventory.config.items[ class ].stackSize
			end
		end
    end
	return false
end


function gb_inventory.getNumberEmptySlots( ply )
    --Check for items without full stacks first
    local number = 0
    for x = 1, ply.gb_inventory_sizeX do
        for y = 1, ply.gb_inventory_sizeY do
            if not ply.gb_inventory[ x ][ y ] then
                number = number + 1
            end
        end
    end

    return number
end

function gb_inventory.getItemCount( ply, class, data )
	local amount = 0
	for x = 1, gb_inventory.config.ySize do
		for y = 1, gb_inventory.config.xSize do
			if ply.gb_inventory[ x ][ y ] != false then
				if ply.gb_inventory[ x ][ y ].class == class then
					if gb_inventory.hasMatchingData( class, data, ply.gb_inventory[ x ][ y ].data ) then
						amount = amount + ply.gb_inventory[ x ][ y ].amount
					end
				end
			end
		end
	end
	return amount
end

function gb_inventory.initPlayerData( ply )
	ply.gb_inventory = {}
	for x = 1, gb_inventory.config.ySize do
		ply.gb_inventory[ x ] = {}
		for y = 1, gb_inventory.config.xSize do
			ply.gb_inventory[ x ][ y ] = false
		end
	end
	gb_inventory.sendPlayerData( ply ) -- Send empty table first

    local sizes = gb_inventory.GetInventorySize(ply)
    ply:SetNWInt("gb_inventory_sizeX",sizes[1])
    ply:SetNWInt("gb_inventory_sizeY",sizes[2])
    ply.gb_inventory_sizeX = sizes[1]
    ply.gb_inventory_sizeY = sizes[2]

	timer.Create("inventorysize"..ply:SteamID(),10, 5, function()
        local sizes = gb_inventory.GetInventorySize(ply)
        ply:SetNWInt("gb_inventory_sizeX",sizes[1])
        ply:SetNWInt("gb_inventory_sizeY",sizes[2])
        ply.gb_inventory_sizeX = sizes[1]
        ply.gb_inventory_sizeY = sizes[2]


	end )

	timer.Simple(5,function()
	    gb_inventory.database.fetchPlayerData( ply ) -- Send fetched data after 5 seconds
	end)

end
hook.Add( "PlayerInitialSpawn", "gb_inventory_initPlayerData", gb_inventory.initPlayerData )
if server_id == 99 then
    hook.Add( "PlayerSpawn", "gb_inventory_initPlayerData", gb_inventory.initPlayerData )
end

util.AddNetworkString( "gb_Inv_playerData" )
function gb_inventory.sendPlayerData( ply )
    local itemjson = util.TableToJSON(ply.gb_inventory)
	net.Start( "gb_Inv_playerData" )
    local data = util.Compress(itemjson)
    net.WriteUInt(#data, 32);
    net.WriteData( data, #data )
    net.Send(ply)
end
-----------------------------------------------------------------------------
--	Meta-functions, for easier access or mods
---------------------------------------------------------------------------*/
local person = FindMetaTable( "Player" )

function person:addItem( class, model, amount, data )
	gb_inventory.addItem( self, class, model, amount, data )
end

function person:removeItem( itemPos, amount )
	gb_inventory.removeItem( self, itemPos, amount )
end

function person:storeItem()
	gb_inventory.storeItem( self, ent )
end

function person:dropStack( itemPos )
	gb_inventory.dropStack( self, itemPos )
end

function person:dropItem( itemPos )
	gb_inventory.dropItem( self, itemPos )
end

function person:useItem( itemPos )
	gb_inventory.useItem( self, itemPos )
end

function person:moveItem( fromPos, toPos )
	gb_inventory.moveItem( self, fromPos, toPos )
end

function person:moveStack( fromPos, toPos )
	gb_inventory.moveStack( self, fromPos, toPos )
end

function person:openInventory()
	gb_inventory.open( self )
end

function person:getFreeSlots( class )
	return gb_inventory.getFreeSlots( self, class )
end

function person:getFreeSlot( class, data, amount )
	return gb_inventory.getFreeSlot( self, class, data, amount )
end

function person:getInventory()
	return gb_inventory.getInventory( self )
end

function person:get_number_of_class(class)
    local number = 0
    for k,v in pairs(ents.FindByClass(class)) do
        if v.Getowning_ent and v:Getowning_ent() == self then
            number = number + 1
        end
    end
    return number
end

function person:get_number_of_printers()
    local number = 0
    for k,v in pairs(ents.FindByClass("vrondakis_printer")) do
        if v:Getowning_ent() == self then
            number = number + 1
        end
    end
    for k,v in pairs(ents.FindByClass("lcd_printer")) do
            if v:Getowning_ent() == self then
                number = number + 1
            end
        end
    self:SetNWInt("numberOfPrinters", number)
    return number
end

function person:inventory_count_item( itemname )
    local amount = 0
    local inventory = self:getInventory()
    for k, items in pairs( inventory ) do
            if not istable(items) then continue end
            for v, item in pairs( items ) do
                if not item then continue end
            if (item.class == itemname) then
                -- print("yes, got item")
                amount = amount + (item.amount or 1)
                if amount < 1 then amount = 0 end
               --  print("yes, got item "..amount)
            end
        end
    end
    return amount
end

function person:inventory_remove_item( itemname, removenumber)
    local removed = 0
    if not removenumber then
        removenumber = 1
    end
    if self:inventory_count_item( itemname ) <  removenumber then
        return false
    end
    local inventory = self:getInventory()
    for k, items in pairs( inventory ) do
        if not istable(items) then continue end
        for v, item in pairs( items ) do
            if not item then continue end
            if (item.class == itemname) then
                if removenumber > item.amount and removed < removenumber then
                    self:removeItem({k,v},item.amount)
                    removed = removed + item.amount
                elseif removed < removenumber then
                    local del = (removenumber-removed)
                    self:removeItem({k,v},del)
                    removed = removed-del
                end
            end
            end
        end

    return true
end