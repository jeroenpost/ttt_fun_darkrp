gb_inventory.config = {}
gb_inventory.config.items = {}
gb_inventory.config.themes = {}
gb_inventory.config.language = {}

----------------------------------------------------------------------------
--	Config vars
---------------------------------------------------------------------------*/
gb_inventory.config.xSize = 4				-- Amount of columns, leave this at 4 unless you want your inventory to mess up
gb_inventory.config.ySize = 14				-- Amount of rows
gb_inventory.config.takeDist = 100			-- Maxium distance at which you can pickup items
gb_inventory.config.theme = "fusionPalette" 	-- Name of the theme you want to use, see below if you want to create your own!
gb_inventory.config.openKey = "IN_DUCK"			-- List of possible key choices: http://wiki.garrysmod.com/page/Enums/KEY
gb_inventory.config.pickupKey1 = IN_USE	-- List of possible key choices: http://wiki.garrysmod.com/page/Enums/IN
gb_inventory.config.pickupKey2 = IN_DUCK
gb_inventory.config.pickupText = "CTRL + E"

gb_inventory.config.InventorySizes = {
    normal = { 3,4 },
    admin = { 6, 4},
    vip = { 8, 4 },
    vipp = { 10, 4 },
    vippp = { gb_inventory.config.ySize, gb_inventory.config.xSize },
    green = { 4,4 }
}



--------------------- -------------------------------------------------------
--	Themes
---------------------------------------------------------------------------*/

gb_inventory.config.themes[ "fusionPalette" ] =
{
	titleText			= Color( 28, 32, 36 ),
	frameBar 			= Color( 199, 244, 100 ),
	frameBackground		= Color( 39, 49, 59 ),
	panelBackground		= Color( 39, 49, 59 ),
	gripBtn				= Color( 217, 91, 67 ),
	gripBar				= Color( 217, 91, 67 ),
	itemHover			= Color( 217, 91, 67 ),
	itemAmount			= Color( 217, 91, 67 ),
}

-----------------------------------------------------------------------------
--	Language tables
---------------------------------------------------------------------------*/
gb_inventory.config.language[ "weaponNames" ] =
{
	weapon_mp52 = "MP5",
	weapon_ak472 = "AK47",
	lockpick = "Lockpick",
	ls_sniper = "Silenced Sniper",
	med_kit = "Med Kit",
	weapon_deagle2 = "Desert Eagle",
	weapon_fiveseven2 = "Five Seven",
	weapon_glock2 = "Glock",
	weapon_m42 = "M4",
	weapon_mac102 = "MAC10",
	weapon_p2282 = "P228",
	weapon_pumpshotgun2 = "Pump Shotgun",
}
-----------------------------------------------------------------------------
--	Item config
---------------------------------------------------------------------------*/
hook.Add( "PostGamemodeLoaded", "gb_invinvholster_command", function()
    DarkRP.declareChatCommand{
        command = "invholster",
        description = "Inventory your current weapon",
        delay = 1.5
    }
end)
----------------------------------------------------------------------------
--	Add any custom items below
---------------------------------------------------------------------------*/


function gb_inventory.config.addItem( item )
    local itemClass = item.class
    item.fetchName = item.fetchName or function() return false end
    item.fetchAmount = item.fetchAmount or function() return false end
    item.useFunc = item.useFunc or false
    gb_inventory.config.items[ itemClass ] = item
end

for _, filename in ipairs( file.Find( "autorun/gb_inventory/items/*.lua", "LUA" ) ) do
    local name = string.match( filename, "^(.+).lua$" )
    if ( name ) then
        if SERVER then AddCSLuaFile( "autorun/gb_inventory/items/" .. filename ) end

        include( "autorun/gb_inventory/items/" .. filename )

    end
end

list.Set("DesktopWindows", "Inventory", {title = "Open Inventory", icon = "icon16/briefcase.png", init = function(icon, window) window:Close() RunConsoleCommand("open_inventory") end})
