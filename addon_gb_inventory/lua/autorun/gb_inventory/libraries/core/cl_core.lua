surface.CreateFont( "gb_inventory_tiny", { font = "Default", size = 12, weight = 1000, antialias = true,  additive = false } )
surface.CreateFont( "gb_inventory_little", { font = "Default", size = 16, weight = 1000, antialias = true,  additive = false } )
surface.CreateFont( "gb_inventory_med", { font = "Default", size = 24, weight = 1000, antialias = true,  additive = false } )
surface.CreateFont( "gb_inventory_little3d", { font = "Default", size = 50, weight = 1000, antialias = true,  additive = true } )
surface.CreateFont( "gb_inventory_medium3d", { font = "Default", size = 100, weight = 1000, antialias = true,  additive = true } )

gb_inventory.menuInterface = gb_inventory.menuInterface or nil
function gb_inventory.open()
	if gb_inventory.menuInterface then
		if gb_inventory.menuInterface:IsVisible() then
			return 
		end
	end
	
	gb_inventory.removeInvalidItems()
	gb_inventory.menuInterface = vgui.Create( "gb_inv_inventory" )
end
net.Receive( "gb_Inv_openInventory", gb_inventory.open )


function gb_inventory.removeInvalidItems()
    for x = 1, gb_inventory.config.ySize do
        for y = 1, gb_inventory.config.xSize do
            if LocalPlayer().gb_inventory[ x ][ y ] != false then
                if not gb_inventory.config.items[ LocalPlayer().gb_inventory[ x ][ y ].class ] then
                    LocalPlayer().gb_inventory[ x ][ y ] = false
                elseif ( LocalPlayer().gb_inventory[ x ][ y ].amount > gb_inventory.config.items[ LocalPlayer().gb_inventory[ x ][ y ].class ].stackSize ) or ( LocalPlayer().gb_inventory[ x ][ y ].amount < 0 ) then
                    LocalPlayer().gb_inventory[ x ][ y ] = false
                end
            end
        end
    end
end

-- { user_id }

function gb_inventory.readPlayerData()
    local leng =  net.ReadUInt(32);
    local data = net.ReadData(leng)
    data =  util.Decompress(data)
    LocalPlayer().gb_inventory = util.JSONToTable( data )

    --print(data)

   --PrintTable(LocalPlayer().gb_inventory)

	if gb_inventory.menuInterface then
		if gb_inventory.menuInterface:IsVisible() then
			gb_inventory.menuInterface:update()
		end
	end
end
net.Receive( "gb_Inv_playerData", gb_inventory.readPlayerData )

        --[[
hook.Add( "PostDrawOpaqueRenderables", "gb_inventory_storeItemInfo", function()
	for _, e in pairs ( ents.GetAll() ) do
		if e:GetPos():Distance( LocalPlayer():GetPos() ) < gb_inventory.config.takeDist then
			if gb_inventory.config.items[ e:GetClass() ] then
				cam.Start3D2D( e:GetPos() + Vector( 0, 0, 7 ), Angle( 0, LocalPlayer():EyeAngles().yaw - 90, 90 ), 0.02 )
					surface.SetDrawColor( Color( 235, 189, 99, 50 ) )
					surface.DrawRect( -100, 0 + math.sin( CurTime() ) * 50, 200, 100 )
					draw.SimpleText( gb_inventory.config.pickupText, "gb_inventory_little3d", 0, 30 + math.sin( CurTime() ) * 50, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					draw.SimpleText( "[Store]", "gb_inventory_little3d", 0, 70 + math.sin( CurTime() ) * 50, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				cam.End3D2D()
			end
		end
	end
end )
        --]]
