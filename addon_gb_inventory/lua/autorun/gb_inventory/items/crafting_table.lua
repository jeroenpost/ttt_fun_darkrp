local ITEM = {}
ITEM.name = "Crafting Table"
ITEM.description = "A table for crafting stuff."
ITEM.class = "crafting_table"
ITEM.stackSize = 10
ITEM.vars = { "damage","Wood","Iron","Spring","Wrench"}
ITEM.candrop = true
ITEM.canuse = false
ITEM.dropFunc = function( ply, itemData, ent )
    timer.Simple(0.10,function()
        ent:SetNWFloat( "Wood", itemData.data.Wood )
        ent:SetNWFloat( "Iron", itemData.data.Iron )
        ent:SetNWFloat( "Spring", itemData.data.Spring )
        ent:SetNWFloat( "Wrench", itemData.data.Wrench )
    end)
end
ITEM.pickupFunc = function(itemData, ply, ent)
    ent.Wood= ent:GetNWFloat( "Wood" ) 
    ent.Iron = ent:GetNWFloat( "Iron" ) 
    ent.Spring = ent:GetNWFloat( "Spring" ) 
    ent.Wrench =ent:GetNWFloat( "Wrench" ) 
end
ITEM.fetchAmount = function( ent ) return 1 end
ITEM.fetchName = function( itemData ) return  itemData.ammoType end
gb_inventory.config.addItem( ITEM )

