local ITEM = {}
ITEM.name = "Gem"
ITEM.description =  "Diamond"
ITEM.class = "ore_diamond"
ITEM.stackSize = 10
ITEM.vars = {}
ITEM.candrop = true
ITEM.canuse = false
ITEM.Color = Color( 255,255,255,255)
gb_inventory.config.addItem( ITEM )
