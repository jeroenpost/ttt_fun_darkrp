local ITEM = {}
ITEM.name = "Ammo"
ITEM.description = "Some ammo to refill your guns"
ITEM.class = "spawned_ammo"
ITEM.stackSize = 1
ITEM.vars = { "ammoType","amountGiven"}
ITEM.candrop = true
ITEM.canuse = true
ITEM.useFunc = function( ply, itemData )
    ply:GiveAmmo( itemData.data.amountGiven, itemData.data.ammoType )
end
ITEM.pickupFunc = function(itemData, ply, ent)

end
ITEM.fetchAmount = function( ent ) return 1 end
ITEM.fetchName = function( itemData ) return  itemData.ammoType end
gb_inventory.config.addItem( ITEM )
