local ITEM = {}
ITEM.class = "spawned_money"
ITEM.name = "Money"
ITEM.description = "Cash"
ITEM.stackSize = 5000000000
ITEM.vars = { "amount" }
ITEM.candrop = true
ITEM.canuse = true
ITEM.useFunc = function( ply, itemData )
    ply:addMoney( itemData.amount )
    itemData.amount = 0
end
ITEM.dropFunc = function( ply, itemData )
    itemData.amount = 0
end
ITEM.pickupFunc = function(itemData, ply, ent)
    ent.amount = ent:Getamount()
end
ITEM.fetchAmount = function( ent ) return ent:Getamount() end
ITEM.fetchName = function( itemData ) return "Money" end
gb_inventory.config.addItem( ITEM )