local ITEM = {}
ITEM.name = "Red Phosphorus"
ITEM.description = "Red Phosphorus"
ITEM.class = "eml_redp"
ITEM.stackSize = 1
ITEM.vars = { "amount", "maxAmount"}
ITEM.candrop = true
ITEM.canuse = false
ITEM.Color = Color( 255, 0, 0, 255)
ITEM.dropFunc = function( ply, itemData, ent )
    timer.Simple(0.11,function()
        ent:SetNWInt("amount", itemData.data.amount);
        ent:SetNWInt("maxAmount", itemData.data.maxAmount);
    end)
end
ITEM.pickupFunc = function(itemData, ply, ent)
    ent.amount = ent:GetNWInt("amount", 0);
    ent.maxAmount =  ent:GetNWInt("maxAmount", 0);
end
ITEM.fetchAmount = function( ent ) return 1 end
ITEM.fetchName = function( itemData ) return  itemData.ammoType end
gb_inventory.config.addItem( ITEM )

