local ITEM = {}
ITEM.name = "Alcohol"
ITEM.description =  "The perfect driving companion."
ITEM.class = "durgz_alcohol"
ITEM.stackSize = 10
ITEM.candrop = true
ITEM.canuse = false
ITEM.vars = { }

gb_inventory.config.addItem( ITEM )

