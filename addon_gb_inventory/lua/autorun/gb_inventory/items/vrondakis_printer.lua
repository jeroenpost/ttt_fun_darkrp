local ITEM = {}
ITEM.name = "Printer"
ITEM.description = "Print money"
ITEM.class = "vrondakis_printer"
ITEM.stackSize = 1
ITEM.candrop = true
ITEM.canuse = false
ITEM.vars = { "vrondakisName","level","vrondakisType","vrondakisXPPerPrint","vrondakisMoneyPerPrint","vrondakisColor",
                "model","cmd","VIPOnly","vipName","SeizeReward"}

ITEM.useFunc = false
ITEM.pickupFunc = function(itemData, ply, ent) 
    ent.vrondakisName = ent.DarkRPItem.name
    ent.level = ent.DarkRPItem.level
    ent.vrondakisType = ent.DarkRPItem.vrondakisType
    ent.vrondakisXPPerPrint = ent.DarkRPItem.vrondakisXPPerPrint
    ent.vrondakisMoneyPerPrint = ent.DarkRPItem.vrondakisMoneyPerPrint
    ent.vrondakisColor = ent.DarkRPItem.vrondakisColor
    ent.model = ent.DarkRPItem.model
    ent.cmd = ent.DarkRPItem.cmd
    ent.VIPOnly = util.TableToJSON(ent.DarkRPItem.VIPOnly)
    ent.vipName = ent.DarkRPItem.vipName
    ent.SeizeReward =  ent.SeizeReward or (ent.DarkRPItem.vrondakisXPPerPrint * 3)
end
ITEM.dropFunc = function( ply, itemData, ent )
    if not ent.DarkRPItem then ent.DarkRPItem = {} end
    ent.DarkRPItem.name = itemData.data.vrondakisName
    ent.DarkRPItem.vrondakisType = itemData.data.vrondakisType
    ent.DarkRPItem.vrondakisXPPerPrint = itemData.data.vrondakisXPPerPrint
    ent.DarkRPItem.vrondakisMoneyPerPrint = itemData.data.vrondakisMoneyPerPrint
    ent.DarkRPItem.vrondakisColor = itemData.data.vrondakisColor
    ent.DarkRPItem.model = itemData.data.model
    ent.DarkRPItem.level = itemData.data.level
    ent.DarkRPItem.cmd = itemData.data.cmd
    if isstring(itemData.data.VIPOnly) then
    ent.DarkRPItem.VIPOnly = util.JSONToTable(itemData.data.VIPOnly)
    end
    ent.DarkRPItem.vipName = itemData.data.vipName

    ent.DarkRPItem.vrondakisOverheat = LevelSystemConfiguration.PrinterOverheat
    ent.DarkRPItem.PrinterMaxP = LevelSystemConfiguration.PrinterMaxP
    ent.DarkRPItem.vrondakisPrinterTime = LevelSystemConfiguration.PrinterTime
    ent.DarkRPItem.vrondakisIsBuyerRetarded = LevelSystemConfiguration.KeepThisToTrue
    ent.DarkRPItem.vrondakisEpileptic = LevelSystemConfiguration.Epilepsy
    ent:Setowning_ent( ply )
    ent.SeizeReward = itemData.data.SeizeReward
end

ITEM.fetchAmount = function( ent ) return 1 end
ITEM.fetchName = function( itemData ) return  itemData.vrondakisName end

ITEM.CanPickup = function( pl, ent )
    if not IsValid(ent:Getowning_ent()) or ( ent:Getowning_ent():SteamID() ~= pl:SteamID() ) and not table.HasValue( JOBS_criminals, pl:Team()) then
        DarkRP.notify(pl, 1, 4, "You can only pick up your own printers")
        return false
    end
    return true
end

ITEM.CanDrop = function(pl,item)
    if pl:get_number_of_printers() > 4 then
        DarkRP.notify(pl, 1, 4, "You can only have 5 printers spawned at the same time")
        return false
    end
    return true
end

gb_inventory.config.addItem( ITEM )

