local ITEM = {}
ITEM.name = "Gem"
ITEM.description =  "Emerald"
ITEM.class = "ore_emerald"
ITEM.stackSize = 10
ITEM.vars = {}
ITEM.candrop = true
ITEM.canuse = false
ITEM.Color = Color( 20,255,20,255)
gb_inventory.config.addItem( ITEM )

