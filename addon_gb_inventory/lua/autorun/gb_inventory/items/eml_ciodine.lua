local ITEM = {}
ITEM.name = "Crystallized Iodine"
ITEM.description = "Crystallized Iodine"
ITEM.class = "eml_ciodine"
ITEM.stackSize = 1
ITEM.vars = { "amount", "maxAmount"}
ITEM.candrop = true
ITEM.canuse = false
ITEM.Color = Color(220, 134, 159, 255)
ITEM.dropFunc = function( ply, itemData, ent )
    timer.Simple(0.11,function()
        ent:SetNWInt("amount", itemData.data.amount);
        ent:SetNWInt("maxAmount", itemData.data.maxAmount);
    end)
end
ITEM.pickupFunc = function(itemData, ply, ent)
    ent.amount = ent:GetNWInt("amount", 0);
    ent.maxAmount =  ent:GetNWInt("maxAmount", 0);
end
ITEM.fetchAmount = function( ent ) return 1 end
ITEM.fetchName = function( itemData ) return  itemData.ammoType end
gb_inventory.config.addItem( ITEM )

