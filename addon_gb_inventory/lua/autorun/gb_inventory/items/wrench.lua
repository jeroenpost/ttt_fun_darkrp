local ITEM = {}
ITEM.name = "Wrench"
ITEM.description =  "An engineer's favourite tool"
ITEM.class = "wrench"
ITEM.stackSize = 5
ITEM.vars = {}
ITEM.candrop = true
ITEM.canuse = false
gb_inventory.config.addItem( ITEM )
