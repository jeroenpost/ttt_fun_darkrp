local ITEM = {}
ITEM.name = "Meth"
ITEM.description = "Meth"
ITEM.class = "eml_meth"
ITEM.stackSize = 1
ITEM.vars = { "amount", "maxAmount","value"}
ITEM.candrop = true
ITEM.canuse = false
ITEM.Color = Color( 1, 241, 249, 255)
ITEM.dropFunc = function( ply, itemData, ent )
    timer.Simple(0.11,function()
        ent:SetNWInt("amount", itemData.data.amount);
        ent:SetNWInt("maxAmount", itemData.data.maxAmount);
        ent:SetNWInt("value", itemData.data.value);
    end)
end
ITEM.pickupFunc = function(itemData, ply, ent)
    ent.amount = ent:GetNWInt("amount", 0);
    ent.maxAmount =  ent:GetNWInt("maxAmount", 0);
    ent.value =  ent:GetNWInt("value", 0);
end

ITEM.CanPickup = function( pl, ent )
    if ent.dropped and ent.dropped > CurTime() then
        return false
    end
    return true
end
ITEM.CanDrop = function(pl,item)
    item.dropped = CurTime() + 5
    return true
end

ITEM.fetchAmount = function( ent ) return 1 end
ITEM.fetchName = function( itemData ) return  itemData.ammoType end
gb_inventory.config.addItem( ITEM )

