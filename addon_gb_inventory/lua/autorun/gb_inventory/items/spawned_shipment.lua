local ITEM = {}
ITEM.name = "Shipment"
ITEM.description = "A Shipment"
ITEM.class = "spawned_shipment"
ITEM.stackSize = 99999999
ITEM.vars = { "Contents","Amount","Class"}
ITEM.useFunc = false
ITEM.candrop = true
ITEM.canuse = true
ITEM.pickupFunc = function(itemData, ply, ent)
    ent.Contents = ent:Getcontents()
    ent.Amount = ent:Getcount()
    ent.Class = CustomShipments[ ent:Getcontents() ].entity
    timer.Destroy( ent:EntIndex() .. "crate" )
end

ITEM.canUseFunc = function( ply, itemData)
    if  ply:HasWeapon( itemData.data.Class ) then
        ply:SelectWeapon(itemData.data.Class)
        return false
    end
    return true
end

ITEM.useFunc = function( ply, itemData)
    if not ply:HasWeapon( itemData.data.Class ) then
        ply:Give( itemData.data.Class )
        itemData.data.Amount = itemData.data.Amount -1
        ply:SelectWeapon(itemData.data.Class)
    else
        ply:SelectWeapon(itemData.data.Class)
        return false
    end
end
ITEM.dropFunc = function( ply, itemData, ent )
    -- Shipments may be changed, lets check if it it still the same
    local shipment = CustomShipments[ itemData.data.Contents ]
    if ( not shipment or shipment.entity ~= itemData.data.Class ) then
        for k, v in ipairs( CustomShipments ) do
            if ( v.entity == itemData.data.Class ) then
                itemData.data.Contents = k
            end
        end
    end

    ent:Setcontents(  itemData.data.Contents )
    ent:Setcount( itemData.data.Amount )
    ent:Setowning_ent( ply )
    itemData.amount = 0
end

ITEM.fetchAmount = function( ent ) return ent:Getcount() end
ITEM.fetchName = function( itemData ) return  itemData.Class end

gb_inventory.config.addItem( ITEM )



