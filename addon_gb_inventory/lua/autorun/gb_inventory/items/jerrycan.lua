local ITEM = {}
ITEM.name = "Jerrycan"
ITEM.description = "Jerrycan"
ITEM.class = "jerrycan"
ITEM.stackSize = 1
ITEM.vars = { "FUEL_Carrying"}
ITEM.candrop = true
ITEM.canuse = false
ITEM.dropFunc = function( ply, itemData, ent )
    timer.Simple(0.11,function()
        ent.FUEL_Carrying = itemData.data.FUEL_Carrying
        ent.FUEL_CanUse = true
        ent:SetNWInt( "FUEL_Carrying", math.Round( itemData.data.FUEL_Carrying ) )
    end)
end
ITEM.pickupFunc = function(itemData, ply, ent)
    ent.FUEL_Carrying = ent:GetNWInt("FUEL_Carrying", 0);
end
ITEM.fetchAmount = function( ent ) return 1 end
ITEM.fetchName = function( itemData ) return  itemData.ammoType end
gb_inventory.config.addItem( ITEM )

