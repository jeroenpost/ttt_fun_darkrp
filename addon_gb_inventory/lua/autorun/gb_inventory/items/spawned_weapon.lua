local ITEM = {}
ITEM.name = "Weapon"
ITEM.description = "Pew! Pew! Pew!"
ITEM.class = "spawned_weapon"
ITEM.stackSize = 500000000
ITEM.vars = { "WeaponClass","amount2","clip1","clip2","ammoadd"}
ITEM.candrop = true
ITEM.canuse = true
ITEM.dropFunc = function( ply, itemData,ent )
    itemData.amount = 0
    if itemData.data.amount2 and ent.dt then
         ent.dt.amount = itemData.data.amount2
    end
end
ITEM.useFunc = function( ply, itemData )
    ply:Give( itemData.data.WeaponClass )
    local wep = ply:GetWeapon( itemData.data.WeaponClass )
    if wep.SetClip1 then
        wep:SetClip1( itemData.data.clip1 or 10 )
        wep:SetClip2( itemData.data.clip2 or 10 )
    end
end
ITEM.CanPickup = function(ply,ent)
    if  ent:Getamount() > 1 then DarkRP.notify(ply, 1, 4, "Unstack weapons first")return false end
    return true
end

ITEM.pickupFunc = function(itemData, ply, ent)
    ent.amount2 = ent:Getamount()
    ent.clip1 =  ent.clip1
    ent.clip2 =  ent.clip2
    ent.ammoadd = ent.ammoadd
end
ITEM.fetchAmount = function( ent ) return ent:Getamount() end
ITEM.fetchName = function( itemData ) return  itemData.WeaponClass end
gb_inventory.config.addItem( ITEM )

