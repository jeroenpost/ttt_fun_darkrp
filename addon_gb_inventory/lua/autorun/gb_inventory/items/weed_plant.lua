local ITEM = {}
ITEM.name =  "Weed Plant"
ITEM.description = "A plant that grows weed!"
ITEM.class = "weed_plant"
ITEM.stackSize = 5
ITEM.vars = { "Price", "BadLevel", "PlantLevel"}
ITEM.candrop = true
ITEM.canuse = false
ITEM.CanPickup = function(ply,ent)
    if ent.IsDead then return false end
end
ITEM.dropFunc = function( ply, itemData, ent )
    -- all of my hate towards the people who default their values in init
    timer.Simple( 0.1, function()
        ent:SetPrice( itemData.data.Price )
        ent:SetBadLevel( itemData.data.BadLevel )
        ent:SetPlantLevel( itemData.data.PlantLevel )
        ent:Setowning_ent(ply)

        if ( itemData.data.PlantLevel <= 5 ) then
            timer.Destroy( "LevelUp" .. tostring( ent:EntIndex() ) )
        end
    end )
end
ITEM.pickupFunc = function(itemData, ply, ent)

    ent.BadLevel= ent:GetBadLevel()
    ent.PlantLevel= ent:GetPlantLevel()
    ent.Price = ent:GetPrice()

end
ITEM.fetchAmount = function( ent ) return 1 end
ITEM.fetchName = function( itemData ) return  itemData.ammoType end

ITEM.CanDrop = function(pl,item)
    if (pl:get_number_of_class("neths_weed_plant") + pl:get_number_of_class("weed_plant")) > 15 then
        DarkRP.notify(pl, 1, 4, "You can only have 15 weed plants spawned at the same time")
        return false
    end
    return true
end
gb_inventory.config.addItem( ITEM )

