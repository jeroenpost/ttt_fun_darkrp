local ITEM = {}
ITEM.name = "Meth Stove"
ITEM.description = "Let's do some cooking!"
ITEM.class = "meth_stove"
ITEM.stackSize = 1
ITEM.vars = { "Damage"}
ITEM.candrop = true
ITEM.canuse = false
ITEM.dropFunc = function( ply, itemData, ent )
    timer.Simple(0.11,function()
        ent.damage = itemData.data.Damage
    end)
end
ITEM.pickupFunc = function(itemData, ply, ent)
end
ITEM.CanDrop = function(pl,item)
    if pl:get_number_of_class("meth_stove") > 0 then
        DarkRP.notify(pl, 1, 4, "You can only have 1 stove spawned at the same time")
        return false
    end
    return true
end

ITEM.fetchAmount = function( ent ) return 1 end
ITEM.fetchName = function( itemData ) return  itemData.ammoType end
gb_inventory.config.addItem( ITEM )
