local ITEM = {}
ITEM.name = "Attachment"
ITEM.description = "Fas2 Attachment"
ITEM.class = "fas2_gb_attachment"
ITEM.stackSize = 1
ITEM.vars = { "PrintName","Attachment"}
ITEM.useFunc = false
ITEM.candrop = true
ITEM.canuse = false
ITEM.pickupFunc = function(itemData, ply, ent)
end
ITEM.dropFunc = function( ply, itemData, ent )
    ent:SetNWString("PrintName", itemData.data.PrintName)
    ent.owner = ( ply )
end

ITEM.fetchAmount = function( ent ) return 1 end
ITEM.fetchName = function( itemData ) return  itemData.PrintName end

gb_inventory.config.addItem( ITEM )
