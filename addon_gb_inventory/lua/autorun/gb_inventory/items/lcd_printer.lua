local ITEM = {}
ITEM.name = "LCD Printer"
ITEM.description = "Print money"
ITEM.class = "lcd_printer"
ITEM.stackSize = 1
ITEM.candrop = true
ITEM.canuse = false
ITEM.vars = { "StoredMoney","StoredTemp","StoredExpl","StoredCool","StoredFan","StoredScreen",
                "StoredStorage","StoredArmor","VIPOnly","vipName"}

ITEM.useFunc = false

ITEM.dropFunc = function( ply, itemData, ent )
       ent:Setowning_ent( ply )
end

ITEM.fetchAmount = function( ent ) return 1 end

ITEM.CanPickup = function( pl, ent )
    if not IsValid(ent:Getowning_ent()) or ( ent:Getowning_ent():SteamID() ~= pl:SteamID() ) and not table.HasValue( JOBS_criminals, pl:Team()) then
        DarkRP.notify(pl, 1, 4, "You can only pick up your own printers")
        return false
    end
    return true
end

ITEM.CanDrop = function(pl,item)
    if pl:get_number_of_printers() > 4 then
        DarkRP.notify(pl, 1, 4, "You can only have 5 printers spawned at the same time")
        return false
    end
    return true
end

gb_inventory.config.addItem( ITEM )

