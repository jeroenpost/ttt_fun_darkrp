local ITEM = {}
ITEM.name = "Bitcoin Miner"
ITEM.description = "Mine Bitcoins"
ITEM.class = "bit_miner_heavy"
ITEM.stackSize = 1
ITEM.candrop = true
ITEM.canuse = false
ITEM.vars = { }

ITEM.useFunc = false
ITEM.pickupFunc = function(itemData, ply, ent) 



end
ITEM.dropFunc = function( ply, itemData, ent )
    if not ent.DarkRPItem then ent.DarkRPItem = {} end

    ent:Setowning_ent( ply )
    ent:SetOwnerName(ply:Nick())
end

ITEM.fetchAmount = function( ent ) return 1 end


ITEM.CanPickup = function( pl, ent )
    if not IsValid(ent:Getowning_ent()) or ( ent:Getowning_ent():SteamID() ~= pl:SteamID() ) and not table.HasValue( JOBS_criminals, pl:Team()) then
        DarkRP.notify(pl, 1, 4, "You can only pick up your own Miners")
        return false
    end
    return true
end

ITEM.CanDrop = function(pl,item)
    if pl:get_number_of_class("bit_miner_heavy") > 1 then
        DarkRP.notify(pl, 1, 4, "You can only have 2 heavy miners spawned at the same time")
        return false
    end
    return true
end

gb_inventory.config.addItem( ITEM )

