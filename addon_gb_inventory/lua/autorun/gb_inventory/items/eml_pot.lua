local ITEM = {}
ITEM.name = "Pot"
ITEM.description =  "Nice pot to do some cooking"
ITEM.class = "eml_pot"
ITEM.stackSize = 1
ITEM.vars = {}
ITEM.candrop = true
ITEM.canuse = false
gb_inventory.config.addItem( ITEM )
