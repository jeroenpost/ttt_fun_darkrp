local ITEM = {}
ITEM.name = "Uncooked Meth"
ITEM.description = "Uncooked Meth"
ITEM.class = "uncooked_meth"
ITEM.stackSize = 1
ITEM.vars = { "damage", "timer"}
ITEM.candrop = true
ITEM.canuse = false
ITEM.Color = Color( 150, 150, 150 )
ITEM.dropFunc = function( ply, itemData, ent )
    timer.Simple(0.11,function()
        ent:SetMethTimer( itemData.data.timer )
        ent.damage = itemData.data.damage
    end)
end
ITEM.pickupFunc = function(itemData, ply, ent)
    ent.damage = ent:GetNWInt("amount", 0);
    ent.timer =  ent:GetMethTimer()
end
ITEM.fetchAmount = function( ent ) return 1 end
ITEM.fetchName = function( itemData ) return  itemData.ammoType end
gb_inventory.config.addItem( ITEM )

