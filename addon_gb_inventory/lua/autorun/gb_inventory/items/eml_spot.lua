local ITEM = {}
ITEM.name = "Special pot"
ITEM.description =  "Nice pot to do some cooking"
ITEM.class = "eml_spot"
ITEM.stackSize = 10
ITEM.vars = {}
ITEM.candrop = true
ITEM.canuse = false
gb_inventory.config.addItem( ITEM )