local ITEM = {}
ITEM.name = "Drug Lab"
ITEM.description = "Don't let it be seen by the CPs!"
ITEM.class = "drug_lab"
ITEM.stackSize = 1
ITEM.vars = { "Price"}
ITEM.candrop = true
ITEM.canuse = false
ITEM.dropFunc = function( ply, itemData, ent )
    ent:Setprice(itemData.data.Price )
    ent:Setowning_ent( ply )
end
ITEM.pickupFunc = function(itemData, ply, ent)
    ent.Price =  ent:Getprice()
end
ITEM.fetchAmount = function( ent ) return 1 end
ITEM.fetchName = function( itemData ) return  itemData.ammoType end
gb_inventory.config.addItem( ITEM )
