
TEAM_BATMAN = DarkRP.createJob("Batman", {
    color = Color(0, 0, 0, 255),
    model = "models/player/superheroes/batman.mdl",
    description = [[Protect the city from criminals. Use your fist, runspeed and jumpheight to fight crime]],
    weapons = {"fists_strong", "dnascanner", "weaponchecker","realistic_hook"},
    command = "batman",
    max = 1,
    salary = 50,
    admin = 0,
    vote = false,
    hasLicense = false,
    level = 7,
    runspeed = 600,
    walkspeed =420,
    PlayerLoadout = function(ply) timer.Simple(1,function() if IsValid(ply) then ply:SetHealth(125) ply:SetArmor(100)  ply:SetJumpPower(350) end end) return false end,
    jobs_heroes = true
})


TEAM_SUPERMAN = DarkRP.createJob("Superman", {
    color = Color(0, 90, 0, 255),
    model = "models/player/superheroes/superman.mdl",
    description = [[Protect the city from criminals.
		Fly around looking for crime]],
    weapons = {"fists_strong","superman_fly", "superman_lasereyes"},
    command = "superman",
    max = 1,
    salary = 50,
    admin = 0,
    vote = false,
    hasLicense = false,
    level = 7,
    runspeed = 450,
    VIPOnly = {"vip","vipp","vippp"},
    vipName = "VIP",
    walkspeed = GAMEMODE.Config.walkspeed,
    PlayerLoadout = function(ply) timer.Simple(1,function() if IsValid(ply) then ply:SetHealth(200) ply:SetArmor(200)  ply:SetJumpPower(350) end end) return false end,
    jobs_heroes = true
})


TEAM_SPIDERMAN = DarkRP.createJob("Spiderman", {
    color = Color(255, 140, 0, 255),
    model = "models/player/tasm2spider.mdl",
    description = [[You are Spiderman. Use your powers to stop crime. You have a Web to fly around and to freeze people. Left click to shoot webs and right click to make people get stuck in your web]],
    weapons = {"spiderman","fists_strong"},
    command = "spiderman",
    max = 1,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    level = 5,
    hasLicense = false,
    VIPOnly = {"vip","vipp","vippp"},
    vipName = "VIP",
    PlayerLoadout = function(ply) ply:SetArmor(35) timer.Simple(1,function() if IsValid(ply) then GAMEMODE:SetPlayerSpeed(ply, GAMEMODE.Config.walkspeed, 300) end end) return false end,
    jobs_heroes = true
})


TEAM_JOKER = DarkRP.createJob("Joker", {
    color = Color(0, 50, 0, 255),
    model = "models/player/bobert/joker.mdl",
    description = [[You are Batman's Enemy. Try to make his live as hard as possible. You may also attack Spiderman and Superman. Make them feel pain and make their lives miserable in any way. You are having a super hard time killing them.]],
    weapons = {"fists","fas2_s_w_m29_44"},
    command = "joker",
    max = 1,
    salary = 50,
    admin = 0,
    vote = false,
    hasLicense = false,
    level = 20,
    runspeed = 400,
    walkspeed = 320,
    PlayerLoadout = function(ply) timer.Simple(1,function() if IsValid(ply) then ply:SetHealth(100) ply.gotjob = CurTime() ply:SetArmor(50) ply:SetJumpPower(300) GAMEMODE:SetPlayerSpeed(ply, 320,400) end end) return false end,
    PlayerDeath = function(ply, weapon, killer)
        if ply.gotjob + 120 < CurTime() then
            ply:teamBan(TEAM_JOKER, 120)
            ply:changeTeam(GAMEMODE.DefaultTeam, true)
            if killer:IsPlayer() then
                DarkRP.notifyAll(0, 4, "The Joker has been killed and is therefor demoted.")
            else
                DarkRP.notifyAll(0, 4, "The Joker has died and is therefor demoted.")
            end
        end
    end,
    jobs_heroes = true,
    jobs_vip = true
})

TEAM_VENOM = DarkRP.createJob("Venom", {
    color = Color(255, 140, 0, 255),
    model = "models/player/venom.mdl",
    description = [[You are Venom, the biggest enemy of Spiderman. You help out Gangers, SS, and all the evil to do illegal things. You do not co-operate with the CP's and never follow the laws.]],
    weapons = {"spiderman","fists_strong"},
    command = "venomplayer",
    max = 1,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    level = 15,
    hasLicense = false,
    VIPOnly = {"vip","vipp","vippp"},
    vipName = "VIP",
    PlayerLoadout = function(ply) ply:SetArmor(10) timer.Simple(1,function() if IsValid(ply) then GAMEMODE:SetPlayerSpeed(ply, GAMEMODE.Config.walkspeed, 300) end end) return false end,
    jobs_heroes = true,
    jobs_vip = true
})