TEAM_SANIC = DarkRP.createJob("Sanic", {
    color = Color(85, 0, 255, 255),
    model = "models/player/sanic.mdl",
    description = [[3FAST5U. Jour fastr ten evrywone. Kilz thu EgzMan. Only allowed to attack Doctor Robotnik AKA EggMan. Punch Eggman in the back, but don't walk into his bombs! Sanic can be very annoying and he should be, cuz hez 3fast6u]],
    weapons = {'fists_sanic','sanic_boom','silent_knife'},
    command = "becomesanic",
    max = 3,
    salary = 0,
    admin = 0,
    vote = false,
    level = 35,
    hasLicense = false,
    VIPOnly = {"vipp","vippp"},
    vipName = "VIP +",
    runspeed = 1950,
    walkspeed = 450,
    PlayerLoadout = function(ply) ply:SetHealth(225) ply:SetArmor(0) return false end,
    jobs_heroes = true,
    jobs_vipp = true
})

TEAM_EGGMAN = DarkRP.createJob("EggMan / Robotnic", {
    color = Color(255, 0, 0, 255),
    model = "models/player/eggman_npc.mdl",
    description = [[You only goal is to KILL SANIC. Kill him with your bombs.]],
    weapons = {'fists',"eggman_bombs"},
    command = "becomeeggman",
    max = 3,
    salary = 0,
    admin = 0,
    vote = false,
    level = 15,
    hasLicense = false,
    VIPOnly = {"vipp","vippp"},
    vipName = "VIP +",
    runspeed = 300,
    walkspeed = GAMEMODE.Config.walkspeed,
    PlayerLoadout = function(ply) ply:SetHealth(150) ply:SetArmor(10)   ply:SetJumpPower(350)  return false end,
    jobs_heroes = true,
    jobs_vipp = true
})

TEAM_SANTA = DarkRP.createJob("Santa", {
    color = Color(255,10,10, 255),
    model = "models/santa_claus/slow_fix.mdl",
    description = [[Santa gives away presents, takes your wishlist for christmas and makes HOHOHO sounds]],
    weapons = {'fists',"gb_santa_cane"},
    command = "becomesanta",
    max = 1,
    salary = 0,
    admin = 0,
    vote = false,
    level = 5,
    hasLicense = false,
    VIPOnly = {"vippp"},
    vipName = "VIP ++",
    jobs_vippp = true
})

TEAM_SHREK = DarkRP.createJob("Shrek", {
    color = Color(47,139,33, 255),
    model = "models/player/pyroteknik/shrek.mdl",
    description = [[Shrek is Love. And a great thief!]],
    weapons = {'fists',"weapon_kiss","keypad_cracker","carpick","swep_pickpocket"},
    command = "becomeshrek",
    max = 2,
    salary = 0,
    admin = 0,
    vote = false,
    level = 5,
    hasLicense = false,
    VIPOnly = {"vippp"},
    vipName = "VIP ++",
    jobs_vippp = true
})

TEAM_ART = DarkRP.createJob("Art Gallery Holder", {
    color = Color(255, 22, 13, 255),
    model = 		 {
        "models/player/Group01/Female_01.mdl",
        "models/player/Group01/Female_02.mdl",
        "models/player/Group01/Female_03.mdl",
        "models/player/Group01/Female_04.mdl",
        "models/player/Group01/Female_06.mdl",
        "models/player/group01/male_01.mdl",
        "models/player/Group01/Male_02.mdl",
        "models/player/Group01/male_03.mdl",
        "models/player/Group01/Male_04.mdl",
        "models/player/Group01/Male_05.mdl",
        "models/player/Group01/Male_06.mdl",
        "models/player/Group01/Male_07.mdl",
        "models/player/Group01/Male_08.mdl",
        "models/player/Group01/Male_09.mdl"
    },
    description = [[Make you own art gallery]],
    weapons = {'fists',},
    command = "becomeartgallery",
    max = 3,
    salary = 130,
    admin = 0,
    vote = false,
    level = 15,
    hasLicense = false,
    VIPOnly = {"vipp","vippp"},
    vipName = "VIP +",
    runspeed = 300,
    walkspeed = GAMEMODE.Config.walkspeed,
    PlayerLoadout = function(ply) ply:SetHealth(150) ply:SetArmor(10)   ply:SetJumpPower(350)  return false end,
    jobs_vipp = true
})


TEAM_NEWS = DarkRP.createJob("Camera Man (newsguy)", {
    color = Color(38, 194, 129),
    model = "models/player/group02/male_04.mdl",
    description = [[Be a cameraman, make your own news! Use your camera, and buy some tv's to show what you are recording!]],
    weapons = {'news_camera'},
    command = "becomenews",
    max = 3,
    salary = 50,
    admin = 0,
    vote = false,
    level = 5,
    hasLicense = false,
    VIPOnly = {"vipp","vippp"},
    vipName = "VIP +",
    jobs_vipp = true
})

TEAM_PIZZAGUY = DarkRP.createJob("Pizza Guy", {
    color = Color(255, 140, 0, 255),
    model = "models/player/odessa.mdl",
    description = [[Make pizza's for other users and deliver it to them!\nYou could make a Pizza Place, where people can get some Pizza,\ndrive around as PizzaGuy on the go, etc.]],
    weapons = {"fists"},
    command = "pizzaguy",
    max = 2,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    level = 5,
    hasLicense = false,
    VIPOnly = {"vip","vipp","vippp"},
    vipName = "VIP",
    jobs_vip = true
})

TEAM_GRAFFITI =  DarkRP.createJob("Graffiti Artist", {
    color = Color(255, 255, 51, 255),
    model = {  "models/conex/stickguy/stickguy.mdl", "models/player/group03/male_01.mdl","models/player/eli.mdl","models/player/monk.mdl"},
    description = [[You are an artist. Use your spraypaint to make the most beatiful artwork]],
    weapons = {"weapon_spraymhs"},
    command = "graffitiartist",
    max = 2,
    salary = 40,
    admin = 0,
    vote = false,
    hasLicense = false,
    VIPOnly = {"vipp","vippp"},
    vipName = "VIP +",
    jobs_vipp = true
})

TEAM_CINEMAOWNER = DarkRP.createJob("Cinema Owner", {
    color = Color(238, 99, 99, 255),
    model = "models/player/mossman.mdl",
    description = [[Play videos in the theater]],
    weapons = {"fists","weapon_popcorn"},
    command = "CinemaOwner",
    max = 1,
    salary = 75,
    admin = 0,
    vote = false,
    hasLicense = false,
    VIPOnly = {"vip","vipp","vippp"},
    vipName = "VIP",
    jobs_vip = true
})

TEAM_PLASTIC_SURGEON = DarkRP.createJob("Plastic Surgeon", {
    color = Color(27, 69, 179, 255),
    model = {"models/player/kleiner.mdl",
        "models/player/Group03m/male_02.mdl",
        "models/player/Group03m/Female_02.mdl","models/player/Group03m/Female_04.mdl","models/player/Group03m/Male_01.mdl","models/player/Group03m/Male_04.mdl"},
    description = [[Use your tools to change the appearance of other players for money.]],
    weapons = {"fists"},
    command = "surgeon",
    max = 3,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    VIPOnly = {"vippp"},
    vipName = "VIP ++",
    jobs_vippp = true
})

TEAM_COCAINE = DarkRP.createJob("COCAINE Brewer", {
    color = Color(227, 169, 79, 255),
    model = {"models/player/kleiner.mdl",
        "models/player/Group03m/male_02.mdl",
        "models/player/Group03m/Female_02.mdl","models/player/Group03m/Female_04.mdl","models/player/Group03m/Male_01.mdl","models/player/Group03m/Male_04.mdl"},
    description = [[Make some cash my brewing cocaine!]],
    weapons = {"fists"},
    command = "cocainebrewer",
    max = 5,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    VIPOnly = {"vippp"},
    vipName = "VIP ++",
    jobs_vippp = true
})




TEAM_CASINO = DarkRP.createJob("Casino Owner", {
    color = Color(20, 33, 145, 255),
    model = "models/zombie_jester/zombie_jester.mdl",
    description = [[You are the owner of the casino. You get $15 every time a slot machine is used, but you loose money when a player wins. You also get money if players use the arcare machines, $10 per use.]],
    weapons = {},
    command = "casinoowner",
    max = 1,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    level = 15,
    hasLicense = false,
    VIPOnly = {"vip","vipp","vippp"},
    vipName = "VIP",
    jobs_vip = true
})

