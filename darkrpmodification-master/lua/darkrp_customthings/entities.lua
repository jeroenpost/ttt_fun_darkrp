--[[---------------------------------------------------------------------------
DarkRP custom entities
---------------------------------------------------------------------------

This file contains your custom entities.
This file should also contain entities from DarkRP that you edited.

Note: If you want to edit a default DarkRP entity, first disable it in darkrp_config/disabled_defaults.lua
	Once you've done that, copy and paste the entity to this file and edit it.

The default entities can be found here:
https://github.com/FPtje/DarkRP/blob/master/gamemode/config/addentities.lua#L111

Add entities under the following line:
---------------------------------------------------------------------------]]--

AddCSLuaFile("darkrp_customthings/drugs.lua")
include("darkrp_customthings/drugs.lua");



DarkRP.createEntity("Crafting Table", {
    ent = "crafting_table",
    model = "models/props_wasteland/controlroom_desk001b.mdl",
    price = 2000,
    max = 1,
    cmd = "buythecraftingtable",
    level = 9,
    shop_crafting = true,
    shop = true
})

DarkRP.createEntity("Wood", {
    ent = "wood",
    model = "models/props_phx/construct/wood/wood_boardx1.mdl",
    price = 250,
    max = 15,
    cmd = "buythewood",
    level = 9,
    shop_crafting = true,
    shop = true
})

DarkRP.createEntity("Spring", {
    ent = "spring",
    model = "models/props_c17/TrapPropeller_Lever.mdl",
    price = 250,
    max = 15,
    cmd = "buythespring",
    level = 9,
    shop_crafting = true,
    shop = true
})

DarkRP.createEntity("Wrench", {
    ent = "wrench",
    model = "models/props_c17/tools_wrench01a.mdl",
    price = 250,
    max = 15,
    cmd = "buythewrench",
    level = 9,
    shop_crafting = true,
    shop = true
})

DarkRP.createEntity("Iron Bar", {
    ent = "ironbar",
    model = "models/Items/CrossbowRounds.mdl",
    price = 250,
    max = 15,
    cmd = "buytheironbar",
    level = 9,
    shop_crafting = true,
    shop = true
})

DarkRP.createEntity("Popcorn", {
    ent = "weapon_popcorn",
    model = "models/green_black/popcorn.mdl",
    energy = 25,
    price = 150,
    max = 1,
    cmd = "buypopcorn",
    shop_food = true,
    shop = true
})

DarkRP.createEntity("Candy 1", {
    ent = "durgz_candy1",
    model = "models/props_phx/misc/egg.mdl",
    energy = 25,
    price = 75,
    max = 15,
    cmd = "buycandy1",
    allowed = {TEAM_PIZZAGUY},
    shop_food = true,
    shop = true
})

DarkRP.createEntity("Candy Bhop", {
    ent = "durgz_candy_bhop",
    model =  "models/noesis/donut.mdl",
    energy = 25,
    price = 1250,
    max = 15,
    cmd = "buycandy2",
    allowed = {TEAM_PIZZAGUY},
    shop_food = true,
    shop = true
})

DarkRP.createEntity("Soda", {
    ent = "food_soda",
    model = "models/props_junk/PopCan01a.mdl",
    energy = 25,
    price = 25,
    max = 15,
    cmd = "buysoda",
    allowed = {TEAM_PIZZAGUY},
    shop_food = true,
    shop = true
})
DarkRP.createEntity("Veggy Pizza", {
    ent = "food_veggie_pizza",
    model = "models/workspizza02/workspizza02.mdl",
    energy = 85,
    price = 110,
    max = 15,
    cmd = "buyveggypizza",
    allowed = {TEAM_PIZZAGUY },
    shop_food = true,
    shop = true

})
DarkRP.createEntity("Pepperoni Pizza", {
    ent = "food_brocolli_pizza",
    model = "models/workspizza03/workspizza03.mdl",
    energy = 80,
    price = 105,
    max = 15,
    cmd = "buypepperonipizza",
    allowed = {TEAM_PIZZAGUY},
    shop_food = true,
    shop = true
})
DarkRP.createEntity("Hawaiian Pizza", {
    ent = "food_hawaii_pizza",
    model = "models/workspizza01/workspizza01.mdl",
    energy = 40,
    price = 95,
    max = 15,
    cmd = "buyhawiianpizza",
    allowed = {TEAM_PIZZAGUY},
    shop_food = true,
    shop = true
})

DarkRP.createEntity("Mega TV (VIP++)", {
    model = "models/props/cs_assault/billboard.mdl",
    ent = "whk_billboard_player",
    max = 5,
    cmd = "/buymegatv",
    price = 15000,
    shop_vip = true,
    shop = true,
    vip = {"vippp"},
    vipName = "VIP++"
})
DarkRP.createEntity("Television", {
    model = "models/props/cs_office/tv_plasma.mdl",
    ent = "whk_tv",
    max = 5,
    cmd = "/buytc",
    price = 2500,
    shop_vip = true,
    shop = true,
    vip = {"vip","vipp","vippp"},
    vipName = "VIP"
})
DarkRP.createEntity("Standalone Radio", {
    model = "models/props_lab/citizenradio.mdl",
    ent = "whk_radio",
    max = 5,
    cmd = "/buyradio",
    price = 1000,
    shop_vip = true,
    shop = true,
    vip = {"vip","vipp","vippp"},
    vipName = "VIP"
})
DarkRP.createEntity("Music player", {
    model = "models/props/cs_office/radio.mdl",
    ent = "whk_musicplayer",
    max = 5,
    cmd = "/buymusicplayer",
    price = 1500,
    shop_vip = true,
    shop = true,
    vip = {"vip","vipp","vippp"},
    vipName = "VIP"
})

DarkRP.createEntity("Blocked area", {
    model = "models/mechanics/solid_steel/box_beam_16.mdl",
    ent = "whk_blockedarea",
    max = 5,
    cmd = "/buyblockedarea",
    price = 1500,
    shop_vip = true,
    shop = true,
    vip = {"vip","vipp","vippp"},
    vipName = "VIP"
})

DarkRP.createEntity("Picture Frame", {
    model = "models/props_c17/frame002a.mdl",
    ent = "whk_pictureframe",
    max = 15,
    cmd = "/buypictureframe",
    price = 500,
    shop_vip = true,
    shop = true,
    vip = {"vip","vipp","vippp"},
    vipName = "VIP"
})

DarkRP.createEntity("Square Picture Frame", {
    model = "models/props/de_inferno/picture1.mdl",
    ent = "whk_squarepictureframe",
    max = 15,
    cmd = "/buysquarepictureframe",
    price = 550,
    shop_vip = true,
    shop = true,
    vip = {"vip","vipp","vippp"},
    vipName = "VIP"
})

DarkRP.createEntity("Table Picture Frame", {
    model = "models/props_lab/frame002a.mdl",
    ent = "whk_tablepicframe",
    max = 15,
    cmd = "/buytablepictureframe",
    price = 250,
    shop_vip = true,
    shop = true,
    vip = {"vip","vipp","vippp"},
    vipName = "VIP"
})

DarkRP.createEntity("Wide Picture Frame", {
    model = "models/maxofs2d/gm_painting.mdl",
    ent = "whk_widepictureframe",
    max = 15,
    cmd = "/buywidepictureframe",
    price = 850,
    shop_vip = true,
    shop = true,
    vip = {"vip","vipp","vippp"},
    vipName = "VIP"
})

DarkRP.createEntity("Billboard (VIP++)", {
    model = "models/props/cs_assault/billboard.mdl",
    ent = "whk_billboard",
    max = 1,
    cmd = "/buybillboard",
    price = 7500,
    shop_vip = true,
    shop = true,
    vip = {"vippp"},
    vipName = "VIP++"
})


DarkRP.createEntity("Soccer Ball", {
    ent = "sent_soccerball",
    model = "models/props_phx/misc/soccerball.mdl",
    price = 500,
    max = 2,
    cmd = "buysoccerball",
    shop_item = true,
    shop = true,
    shop_vip = true,
    vip = {"vip","vipp","vippp"},
    vipName = "VIP"
})

DarkRP.createEntity("Cat", {
    ent = "npc_siamesecat",
    model = "models/jeezy/animals/siamese_cat/siamese_cat.mdl" ,
    price = 2500,
    max = 10,
    cmd = "buycat",
    shop_item = true,
    shop = true,
    shop_vip = true,
    vip = {"vippp","vipp","vip"},
    vipName = "VIP"
})

DarkRP.createEntity("Cat Food", {
    ent = "ent_catfood",
    model = "models/props/jeezy/catfood_bowl/catfood_bowl.mdl" ,
    price = 250,
    max = 10,
    cmd = "buycatfood",
    shop_item = true,
    shop = true,
    shop_vip = true,
    vip = {"vip","vippp","vipp"},
    vipName = "VIP"
})

DarkRP.createEntity("Cat Water", {
    ent = "ent_waterbowl",
    model = "models/props/jeezy/pet_waterbowl/pet_waterbowl.mdl" ,
    price = 250,
    max = 10,
    cmd = "buywaterbowl",
    shop_item = true,
    shop = true,
    shop_vip = true,
    vip = {"vip","vippp","vipp"},
    vipName = "VIP"
})

DarkRP.createEntity("Radio (DJ)", {
    ent = "rb_radio",
    model = "models/props/cs_office/radio.mdl",
    price = 500,
    max = 6,
    cmd = "buyrbradio",
    shop_item = true,
    shop = true
})

DarkRP.createEntity("Mic", {
    ent = "rb_mic",
    model = "models/props_trainstation/trainstation_post001.mdl",
    price = 2500,
    max = 1,
    cmd = "buyrbmic",
    allowed = {TEAM_BROADCAST,TEAM_MAYOR,TEAM_SSOFFICER},
    shop_item = true,
    shop = true
})

-- ALL THE SIGHTS AND STUFF

DarkRP.createEntity("Acog Scope (Sight) ", {
    ent = "fas2_att_acog",
    model = "models/Items/BoxMRounds.mdl",
    price = 1250,
    max = 2,
    cmd = "buyacog",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("ECLAN C79 (Sight)", {
    ent = "fas2_att_c79",
    model = "models/Items/BoxMRounds.mdl",
    price = 1500,
    max = 2,
    cmd = "buyeclan",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("Comp M4 (Sight)", {
    ent = "fas2_att_compm4",
    model = "models/Items/BoxMRounds.mdl",
    price = 1750,
    max = 2,
    cmd = "buycompm4",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("EO Tech (Sight)", {
    ent = "fas2_att_eotech",
    model = "models/Items/BoxMRounds.mdl",
    price = 1750,
    max = 2,
    cmd = "buyeotech",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("Fore-Grip (Grip)", {
    ent = "fas2_att_foregrip",
    model = "models/Items/BoxMRounds.mdl",
    price = 1000,
    max = 2,
    cmd = "buygrip",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("Harris Bipod (Bi-Pod)", {
    ent = "fas2_att_harrisbipod",
    model = "models/Items/BoxMRounds.mdl",
    price = 1000,
    max = 2,
    cmd = "buybipod",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("Leupold MK4 (Sight)", {
    ent = "fas2_att_leupold",
    model = "models/Items/BoxMRounds.mdl",
    price = 1750,
    max = 2,
    cmd = "buyleupold",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("M21 20 Round Mag (Extended Magazine)", {
    ent = "fas2_att_m2120mag",
    model = "models/Items/BoxMRounds.mdl",
    price = 1000,
    max = 2,
    cmd = "buym21",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("MP5k 30 Round Mag (Extended Magazine)", {
    ent = "fas2_att_mp5k30mag",
    model = "models/Items/BoxMRounds.mdl",
    price = 1000,
    max = 2,
    cmd = "buymp5k30mag",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("PSO-1 (Sight) ", {
    ent = "fas2_att_pso1",
    model = "models/Items/BoxMRounds.mdl",
    price = 1750,
    max = 2,
    cmd = "buypso1",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("SG55 30 Round Mag (Extended Magazine)", {
    ent = "fas2_att_sg55x30mag",
    model = "models/Items/BoxMRounds.mdl",
    price = 1250,
    max = 2,
    cmd = "buysg55mag",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("SKS 20 Round Mag (Extended Magazine)", {
    ent = "fas2_att_sks20mag",
    model = "models/Items/BoxMRounds.mdl",
    price = 1250,
    max = 2,
    cmd = "buysks20mag",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("SKS 30 Round Mag (Extended Magazine)", {
    ent = "fas2_att_sks30mag",
    model = "models/Items/BoxMRounds.mdl",
    price = 1250,
    max = 2,
    cmd = "buysks30mag",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("Suppressor (Attatchment)", {
    ent = "fas2_att_suppressor",
    model = "models/Items/BoxMRounds.mdl",
    price = 2500,
    max = 2,
    cmd = "buysuppressor",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("Tritium Sights (Pistol Iron Sights)", {
    ent = "fas2_att_tritiumsights",
    model = "models/Items/BoxMRounds.mdl",
    price = 500,
    max = 2,
    cmd = "buytrit",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

DarkRP.createEntity("Wooden Stock (UZI Only)", {
    ent = "fas2_att_uziwoodenstock",
    model = "models/Items/BoxMRounds.mdl",
    price = 1250,
    max = 2,
    cmd = "buystock",
    allowed ={TEAM_BLACKMARKET},
    shop_weapons_att = true,
    shop = true
})

-- ALL AMMO FOR FA:S


DarkRP.createEntity("9x18 MM Ammo (60 Rounds)", {
    ent = "fas2_ammo_9x18",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy9x18mm",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity("9x19 MM Ammo (40 Rounds)", {
    ent = "fas2_ammo_9x19",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy9x19mm",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity("10x25 Ammo (60 Rounds)", {
    ent = "fas2_ammo_10x25",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy10x25mm",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity("12 Gauge Ammo (16 Rounds)", {
    ent = "fas2_ammo_12gauge",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy12gauge",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity("23x75MMR Ammo (20 Rounds)", {
    ent = "fas2_ammo_23x75",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy23x75mmr",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity("40MM HE Ammo (10 Rounds)", {
    ent = "fas2_ammo_40mm",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy40mmhe",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity(".44 Magnum Ammo (12 Rounds)", {
    ent = "fas2_ammo_44mag",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy44magnumammo",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity(".45 ACP Ammo (30 Rounds)", {
    ent = "fas2_ammo_45acp",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy45acpammo",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity(".50 AE Ammo (14 Rounds)", {
    ent = "fas2_ammo_50ae",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy50aeammo",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity(".50 BMG Ammo (20 Rounds)", {
    ent = "fas2_ammo_50bmg",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy50bmgammo",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity(".357 SIG Ammo (30 Rounds)", {
    ent = "fas2_ammo_357sig",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy357sigammo",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity(".380 ACP Ammo (60 Rounds)", {
    ent = "fas2_ammo_380acp",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy380acpammo",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity(".500 S&W Ammo (Raging bull) (10 Rounds)", {
    ent = "fas2_ammo_454casull",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy500swammo",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity("5.45x39MM Ammo (60 Rounds)", {
    ent = "fas2_ammo_545x39",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy545x39ammo",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity("5.56x45MM Ammo (60 Rounds)", {
    ent = "fas2_ammo_556x45",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy556x45ammo",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity("7.62x39MM Ammo (60 Rounds)", {
    ent = "fas2_ammo_762x39",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy762x39ammo",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity("7.62x51MM Ammo (40 Rounds)", {
    ent = "fas2_ammo_762x51",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buy7651ammo",
    shop_ammo = true,
    shop = true
})


DarkRP.createEntity("M67 Grenades (12 Grenades)", {
    ent = "fas2_ammo_m67",
    model = "models/Items/BoxMRounds.mdl",
    price = 175,
    max = 2,
    cmd = "buym27grenades",
    shop_ammo = true,
    shop = true
})

-- FIRE

DarkRP.createEntity("Gasoline Tank (Highly Explosive)", {
    ent = "ent_gas_tank",
    model = "models/props_junk/gascan001a.mdl",
    price = 5000,
    max = 3,
    cmd = "/buygastank",
    allowed = {TEAM_PYRO,TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createEntity("Fire Bomb", {
    ent = "fire_incendiary_grenade",
    model =  "models/weapons/w_eq_flashbang.mdl" ,
    price = 5000,
    max = 1,
    cmd = "/buygastank2",
    allowed = {TEAM_PYRO,TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createEntity("Keypad Cracker", {
    ent = "keypad_cracker",
    model =  "models/weapons/w_c4_planted.mdl" ,
    price = 3500,
    max = 1,
    cmd = "/buykeypadcracker",
    allowed = {TEAM_MAFFIABOSS},
    shop_weapons = true,
    shop = true
})


DarkRP.createEntity("Fire Stopper Grenade", {
    ent = "fire_stopper_grenade",
    model =  "models/weapons/w_eq_smokegrenade.mdl"  ,
    price = 5000,
    max = 1,
    cmd = "/buystoppergrenadefire",
    shop_weapons = true,
    shop = true
})

DarkRP.createEntity("Fire Extinguisher", {
    model = "models/props/cs_office/Fire_Extinguisher.mdl",
    ent = "fire_extinguisher",
    max = 1,
    cmd = "/buyfire_extinguisher",
    price = 500,
    shop_item = true,
    shop = true
})



DarkRP.createEntity("News TV", {
    model = "models/props_phx/rt_screen.mdl",
    ent = "news_tv",
    max = 5,
    cmd = "/buynewstv",
    price = 500,
    shop_item = true,
    shop = true,

})

DarkRP.createEntity( "Security Camera", {
    ent = "rprotect_camera",
    model = "models/tools/camera/camera.mdl",
    price = 750,
    max = 6,
    cmd = "buycameraprotect",
    shop_item = true,
    shop = true,
} )

DarkRP.createEntity( "Security Terminal", {
    ent = "rprotect_terminal",
    model = "models/props_phx/rt_screen.mdl",
    price = 1500,
    max = 12,
    cmd = "buyterminalprotect",
    shop_item = true,
    shop = true,
} )

AddEntity( "Security Scanner", {
    ent = "rprotect_scanner",
    model = "models/Items/battery.mdl",
    price = 4000,
    max = 6,
    cmd = "buyscannerprotect",
    shop_item = true,
    shop = true,
} )


DarkRP.createEntity("Miner Light", {
    ent = "bit_miner_light",
    model = "models/BITMINER/BitMinerLight.mdl",
    price = 4000,
    max = 6,
    cmd = "MinerLight",
    shop_printers = true,
    shop = true,
    customCheck  = function(ply) if (SERVER and ply:get_number_of_class("bit_miner_light") > 5) then
        DarkRP.notify(ply, 1, 4, "You can only have 6 spawned at the same time")
        return false
    end


     return true end,
})

DarkRP.createEntity("Miner Standard (VIP)", {
    ent = "bit_miner_medium",
    model = "models/BITMINER/BitMinerMedium.mdl",
    price = 15000,
    max = 4,
    cmd = "MinerStandard",
    shop_printers = true,
    shop = true,
    shop_vip = true,
    shop_vipprinters = true,
    level = 15,
    vip = {"vip","vipp","vippp"},
    vipName = "VIP",
    customCheck  = function(ply) if (SERVER and ply:get_number_of_class("bit_miner_medium") > 3) then
        DarkRP.notify(ply, 1, 4, "You can only have 4 spawned at the same time")
        return false
    end return true end,
})

DarkRP.createEntity("Miner Pro (VIP+)", {
    ent = "bit_miner_heavy",
    model = "models/BITMINER/BitMinerHeavy.mdl",
    price = 100000,
    max = 2,
    cmd = "MinerPro",
    shop_printers = true,
    shop = true,
    level = 30,
    shop_vip = true,
    shop_vipprinters = true,
    vip = {"vipp","vippp"},
    vipName = "VIP+",
    customCheck  = function(ply) if (SERVER and ply:get_number_of_class("bit_miner_heavy") > 1) then
        DarkRP.notify(ply, 1, 4, "You can only have 2 spawned at the same time")
        return false
    end return true end,
})


