


TEAM_VAMPIREHUNTER = DarkRP.createJob("Vampire Hunter", {
    color = Color(111, 255, 88, 255),
    model = {
        "models/player/soldier_stripped.mdl"},
    description = [[Kill those vampires!]],
    weapons = {"weapon_crossbow",},
    command = "vampirehunter",
    max = 24,
    salary = 45,
    admin = 0,
    vote = false,
    hasLicense = true,
    level = 15,
    jobs_special = true
})


TEAM_PARCOUR = DarkRP.createJob("Parkourist", {
    color = Color(150, 30, 30, 255),
    model = {"models/player/p2_chell.mdl","models/player/mossman.mdl"},
    description = [[As a Parkourist can can run around, hang on ledges and jump from everything.
		Impress all players with your skills. You can also teach other how to use parkour.
	]],
    weapons = {"climb_swep2"},
    command = "parkourist",
    max = 3,
    salary = 45,
    admin = 0,
    vote = false,
    hasLicense = false,
    runspeed = 350,
    walkspeed = GAMEMODE.Config.walkspeed,
    PlayerLoadout = function(ply)  ply:SetJumpPower(320) return false end,
    jobs_special = true
})


TEAM_JESUS = DarkRP.createJob("Jesus", {
    color = Color(0, 25, 220, 255),
    model = "models/player/jesus/jesus.mdl",
    description = [[Rule the world as Jesus! Use your miracles to heal the wounded]],
    weapons = {"jesus_miracles","superman_fly",},
    command = "jesus",
    max = 1,
    salary = 50,
    admin = 0,
    vote = false,
    hasLicense = false,
    level = 7,
    runspeed = 350,
    walkspeed = GAMEMODE.Config.walkspeed,
    PlayerLoadout = function(ply) timer.Simple(1,function() if IsValid(ply) then ply:SetHealth(125) ply:SetArmor(25)  ply:SetJumpPower(350) end end) return false end,
    jobs_special = true
})

TEAM_PRIEST = DarkRP.createJob("Priest", {
    color = Color(129, 99, 44, 255),
    model = {"models/player/Group02/male_08.mdl","models/player/Group02/male_06.mdl",
        "models/player/Group03m/Female_01.mdl","models/player/Group03m/Female_04.mdl","models/player/Group03m/Female_06.mdl"},
    description = [[Priests or Disciples follow jesus. They are not allowed to have gun or use force in any way. Priest build churches, pray and preach.]],
    weapons = {},
    command = "becomepriest",
    max = 12,
    salary = 40,
    admin = 0,
    level = 2,
    vote = false,
    hasLicense = false,
    jobs_special = true
})

TEAM_BUSDRIVER =  DarkRP.createJob("Bus Driver", {
    color = Color(255, 165, 51, 255),
    model = {  "models/conex/stickguy/stickguy.mdl", "models/player/group03/male_01.mdl","models/player/eli.mdl","models/player/monk.mdl"},
    description = [[Drive the bus around town.\nBuy a bus to drive around town. Stop at every busstop to let people in.\nFunded by the government, you earn lots of money.\nAs busdriver you can ONLY drive the route around town to serve people. Failing in doing so gets you demoted from this job.]],
    weapons = {},
    command = "busdriver",
    max = 2,
    salary = 40,
    admin = 0,
    vote = false,
    hasLicense = false,
    jobs_special = true
})


TEAM_TOWER =  DarkRP.createJob("Tow Truck Driver", {
    color = Color(255, 185, 51, 255),
    model = "models/player/monk.mdl",
    description = [[You have the ability to tow vehicles.\nHelp people with stranded cars by towing them out or filling their cars with gas.\nSpecialize yourselfin bumping out cars with props from the water.]],
    weapons = {"tow_attach","carpick","fists"},
    command = "tower",
    max = 2,
    salary = 45,
    admin = 0,
    vote = false,
    hasLicense = false,
    jobs_special = true
})

TEAM_BROADCAST = DarkRP.createJob("DJ", {
    color = Color(55, 215, 170, 255),
    model = "models/player/eli.mdl",
    description = [[You have the power of broadcasting any type of information to the civilians of the city.
		Begin your own radio station and play music and update everyone on whats going on.
		Host meetings with the mayor, or even the mob boss.]],
    weapons = {},
    command = "broadcaster",
    max = 2,
    salary = 45,
    vote = false,
    admin = 0,
    jobs_special = true
})

TEAM_HACKER = DarkRP.createJob("Hacker", {
    color = Color(255, 100, 20, 255),
    model = {"models/player/odessa.mdl", "models/player/Group03/Male_04.mdl",  "models/player/Group03/Male_05.mdl"},
    description = [[You can hack keypads, cars, doors and everything with your phone. You can be hired by players to assist in raids. You are not allowed to raid by yourself]],
    weapons = {"fists","weapon_hack_phone"},
    command = "becomehacker",
    max = 3,
    salary = 85,
    admin = 0,
    vote = false,
    level = 25,
    hasLicense = false,
    jobs_special = true
})

TEAM_GREENHAX = DarkRP.createJob("GreenHax", {
    color = Color(255, 100, 20, 255),
    model = {"models/player/jesus/jesus.mdl", "models/player/odessa.mdl", "models/player/Group03/Male_04.mdl",  "models/player/Group03/Male_05.mdl"},
    description = [[Elite Hacker]],
    weapons = {"fists","weapon_hack_phone"},
    command = "becomegreenhax",
    max = 3,
    salary = 95,
    admin = 0,
    vote = false,
    level = 35,
    hasLicense = false,
    NeedToChangeFrom = TEAM_HACKER,
    runspeed = 400,
    walkspeed = 320,
    PlayerLoadout = function(ply) ply:SetMaterial("camos/camo56")  timer.Simple(1,function() if IsValid(ply) then ply:SetHealth(150) ply:SetArmor(25)  ply:SetJumpPower(350) end end) return false end,
    jobs_special = true
})

TEAM_MECHANIC = DarkRP.createJob("Mechanic", {
    color = Color(190, 0, 23, 255),
    model = {
        "models/player/Group03/Male_07.mdl",
        "models/conex/stickguy/stickguy.mdl",
        "models/player/group03/male_01.mdl",
        "models/player/Group03/Male_02.mdl",
        "models/player/Group03/male_03.mdl",
        "models/player/Group03/Male_04.mdl",
        "models/player/Group03/Male_05.mdl",
        "models/player/Group03/Male_06.mdl",
        "models/player/Group03/Male_08.mdl",
        "models/player/Group03/Male_09.mdl",
        "models/player/Group03/Female_01.mdl",
        "models/player/Group03/Female_02.mdl",
        "models/player/Group03/Female_03.mdl",
        "models/player/Group03/Female_04.mdl",
        "models/player/Group03/Female_06.mdl",
        "models/player/alyx.mdl"},
    description = [[As a Mechanic you can fix cars.
		Offer your services to people without gas, or with broken cars]],
    weapons = {"fists", "vc_repair","carpick"},
    command = "mechanic",
    max = 2,
    salary = 45,
    admin = 0,
    vote = false,
    hasLicense = false,
    level = 5,
    jobs_special = true
})

TEAM_LAWYER = DarkRP.createJob("Lawyer", {
    color = Color(150, 80, 80, 255),
    model = {"models/player/gman_high.mdl","models/player/barney.mdl","models/player/mossman.mdl"},
    description = [[As a Laywer you can bail other players out of jail and represent them in front of the police.
	You can use /bail when looking at a jailed player to open the bail menu.
	]],
    weapons = {},
    command = "lawyer",
    max = 2,
    salary = 45,
    admin = 0,
    vote = false,
    hasLicense = false,
    jobs_special = true
})

TEAM_HOTELMANAGER = DarkRP.createJob("Hotel Manager", {
    color = Color(200, 200, 30, 255),
    model = {"models/player/Group02/male_08.mdl","models/player/Group02/male_06.mdl",
        "models/player/Group03m/Female_01.mdl","models/player/Group03m/Female_04.mdl","models/player/Group03m/Female_06.mdl"},
    description = [[The hotel manager can completely assume control of the hotel once they have been made the job.
		However, if someone already lives in one of the rooms, you may NOT kick them out.
		You may build a fading door in the main lobby to block non-residents.
		The lockpick is NOT for raiding or stealing.]],
    weapons = {"lockpick","fists"},
    command = "hotelmanager",
    max = 1,
    salary = 80,
    admin = 0,
    level = 2,
    vote = false,
    hasLicense = false,
    jobs_special = true
})


