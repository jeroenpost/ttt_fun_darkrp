
TEAM_THIEF = DarkRP.createJob("Thief", {
    color = Color(80, 45, 0, 255),
    model = {"models/player/group01/cookies114.mdl", "models/player/eli.mdl"},
    description = [[You are a thief.
		You can rob people and Raid their houses.
		Raids are only allowed onces ever 5 minutes and you have to announce your raids.
		Not allowed to rob AFK people, or people at spawn.]],
    weapons = {"carpick","swep_pickpocket", "keypad_cracker","fists",},--"weapon_kidnapper" },
    command = "thief",
    max = 5,
    salary = 55,
    admin = 0,
    vote = false,
    hasLicense = false,
    level = 5,
    PlayerLoadout = function(ply)
        local picks = ply:GetNWInt("lockpicks") or 0
        if picks < 3 then
            ply:SetLockpickCount( picks+3 )
        end
        return false
    end,
    jobs_criminals = true
})

TEAM_GANG = DarkRP.createJob("Gangster", {
    color = Color(75, 75, 75, 255),
    model = {
        "models/player/Group03/Female_01.mdl",
        "models/player/Group03/Female_02.mdl",
        "models/player/Group03/Female_03.mdl",
        "models/player/Group03/Female_04.mdl",
        "models/player/Group03/Female_06.mdl",
        "models/player/group03/male_01.mdl",
        "models/player/Group03/Male_02.mdl",
        "models/player/Group03/male_03.mdl",
        "models/player/Group03/Male_04.mdl",
        "models/player/Group03/Male_05.mdl",
        "models/player/Group03/Male_06.mdl",
        "models/player/Group03/Male_07.mdl",
        "models/player/Group03/Male_08.mdl",
        "models/player/Group03/Male_09.mdl"},
    description = [[The lowest person of crime.
		A gangster generally works for the Mobboss who runs the crime family.
		The Mob boss sets your agenda and you follow it or you might be punished.]],
    weapons = {},
    command = "gangster",
    max = 3,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    jobs_criminals = true
})

TEAM_MOBBOSS = DarkRP.createJob("Mob boss", {
    color = Color(25, 25, 25, 255),
    model = "models/player/gman_high.mdl",
    description = [[The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.
		He is not allowed to raid by himself.]],
    weapons = { "unarrest_stick"},
    command = "mobboss",
    max = 1,
    salary = GAMEMODE.Config.normalsalary * 1.34,
    admin = 0,
    vote = false,
    hasLicense = false,
    jobs_criminals = true
})

TEAM_MAFFIA = DarkRP.createJob("Mafia", {
    color = Color(65, 25, 65, 255),
    model = {"models/player/tuxmale_01player.mdl",
        "models/player/tuxmale_02player.mdl",
        "models/player/tuxmale_03player.mdl",
        "models/player/tuxmale_04player.mdl",
        "models/player/tuxmale_05player.mdl",
        "models/player/tuxmale_06player.mdl",
        "models/player/tuxmale_07player.mdl",
        "models/player/tuxmale_09player.mdl"},
    description = [[The Mafia organizes crimes around the city. The Maffia can work together with the gangsters to organize bigger crimes, but are usually on their own.]],
    weapons = {},
    command = "mafia",
    max = 10,
    salary = GAMEMODE.Config.normalsalary * 1.34,
    admin = 0,
    level = 15,
    vote = false,
    hasLicense = false,
    jobs_criminals = true
})

TEAM_MAFFIABOSS = DarkRP.createJob("Mafia Boss", {
    color = Color(25, 95, 25, 255),
    model = {
        "models/player/tuxmale_08player.mdl"},
    description = [[The Mafia Boss is the head of the maffia. He sets the agenda of the mafia and is at the top of the Mafia.]],
    weapons = { "unarrest_stick", "carpick"},
    command = "mafiaboss",
    max = 1,
    salary = 75,
    admin = 0,
    level = 35,
    vote = false,
    hasLicense = false,
    PlayerLoadout = function(ply)
        ply:PrintMessage( HUD_PRINTCENTER, "Note: You need to ask for a VALID reason to accept a hit!" )
        return false
    end,
    jobs_criminals = true
})

TEAM_MOB = DarkRP.createJob("HitMan", {
    color = Color(50, 25, 25, 255),
    model = {"models/player/guerilla.mdl","models/player/leet.mdl","models/player/arctic.mdl"},
    description = [[The HitMan can take "hit" requests from other players.
		Players need a good reason to request a hit, and you are not allowed to
		kill the same person multiple times in a row]],
    weapons = { "unarrest_stick", "weapon_real_cs_desert_eagle","fists"},
    command = "hitman",
    max = 3,
    salary = GAMEMODE.Config.normalsalary * 1.34,
    admin = 0,
    vote = false,
    hasLicense = false,
    level = 10,
    PlayerLoadout = function(ply)
        ply:PrintMessage( HUD_PRINTCENTER, "Note: You need to ask for a VALID reason to accept a hit!" )
        local picks = ply:GetNWInt("lockpicks") or 0
        if picks < 3 then
            ply:SetLockpickCount( picks+3 )
        end
        return false
    end,
    jobs_criminals = true
})


TEAM_BANKROBBER = DarkRP.createJob("Bank Robber", {
    color = Color(126, 21, 176, 255),
    model = {"models/player/arctic.mdl","models/player/group01/cookies114.mdl"},
    description = [[Your job is to rob the bank or the Police Armory.
		Stay out of the hands of the police. You can only rob the bank when there are 3 or more cops in the game]],
    weapons = {  "keypad_cracker","fists"},
    command = "bankrobber",
    max = 5,
    salary = 15,
    admin = 0,
    level = 10,
    vote = false,
    hasLicense = false,
    PlayerLoadout = function(ply)
        local picks = ply:GetNWInt("lockpicks") or 0
        if picks < 3 then
            ply:SetLockpickCount( picks+3 )
        end
        return false
    end,
    jobs_criminals = true
})


TEAM_HUNTER = DarkRP.createJob("Jeff the Bounty Hunter", {
    color = Color(255,0,255, 255),
    model = {"models/newinfec/Newhun.mdl"},
    description = [[You are a bounty hunter. Hunt down wanted people. For every wanted person you kill, you will be rewarded XP and Money. If you hurt an innocent person, you will die.]],
    weapons = {"hunter_knife"},
    command = "becomehunter",
    max = 3,
    salary = 15,
    admin = 0,
    vote = false,
    hasLicense = true,
    medic = false,
    PlayerLoadout = function(ply) ply.gotjob = CurTime() ply:SetArmor(25)  return false end,
    PlayerDeath = function(ply, weapon, killer)
        if ply.gotjob + 30 < CurTime() then
            ply:teamBan(TEAM_HUNTER, 120)
            ply:changeTeam(GAMEMODE.DefaultTeam, true)
        end
        return false
    end,
    jobs_criminals = true
})

TEAM_SS = DarkRP.createJob("SS Soldier", {
    color = Color(255,0,0, 255),
    model = {"models/player/soldier/soldier_2.mdl","models/player/soldier/soldier_3.mdl","models/player/soldier/soldier_g2.mdl"},
    description = [[You belong to a powerful group, the SS.
		Create your base with other SS Soldiers and be a tight group. You are allowed to create MEGA bases.]],
    weapons = {"fists"},
    command = "sssoldier",
    max = 10,
    salary = 65,
    admin = 0,
    vote = false,
    hasLicense = false,
    medic = false,
    jobs_criminals = true
})

TEAM_SSOFFICER = DarkRP.createJob("SS Officer", {
    color = Color(180,0,0, 255),
    model = {"models/player/soldier/soldier_1.mdl","models/player/soldier/soldier_g1.mdl"},
    description = [[You are the leader of the SS group.
		Keep every member in order]],
    weapons = {"fists"},
    command = "ssofficer",
    max = 1,
    salary = 75,
    admin = 0,
    vote = false,
    hasLicense = false,
    medic = false,
    jobs_criminals = true
})

