/*---------------------------------------------------------------------------
DarkRP Agenda's
---------------------------------------------------------------------------
Agenda's can be set by the agenda manager and read by both the agenda manager and the other teams connected to it.


HOW TO MAKE AN AGENDA:
AddAgenda(Title of the agenda, Manager (who edits it), {Listeners (the ones who just see and follow the agenda)})
---------------------------------------------------------------------------*/
-- Example: AddAgenda("Gangster's agenda", TEAM_MOB, {TEAM_GANG})
-- Example: AddAgenda("Police agenda", TEAM_MAYOR, {TEAM_CHIEF, TEAM_POLICE})


AddAgenda("Police agenda", TEAM_MAYOR, {TEAM_CHIEF, TEAM_POLICE, TEAM_SWAT})
AddAgenda("Gangster's agenda", TEAM_MOB, {TEAM_GANG})
AddAgenda("Mafia agenda", TEAM_MAFFIABOSS, {TEAM_MAFFIA})
AddAgenda("SS's agenda", TEAM_SSOFFICER, {TEAM_SS})
AddAgenda("Hobo's agenda", TEAM_HOBOKING, {TEAM_HOBO})
AddAgenda("Alien's agenda", TEAM_ALIENQUEEN, {TEAM_ALIEN})