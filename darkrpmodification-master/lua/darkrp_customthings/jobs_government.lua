
TEAM_MAYOR = DarkRP.createJob("Mayor", {
    color = Color(150, 20, 20, 255),
    model = "models/player/breen.mdl",
    description = [[The Mayor of the city creates laws to govern the city.
	If you are the mayor you may create and accept warrants.
	Type /wanted <name>  to warrant a player.
	Type /jailpos to set the Jail Position.
	Type /lockdown initiate a lockdown of the city.
	Everyone must be inside during a lockdown.
	The cops patrol the area.
	/unlockdown to end a lockdown]],
    weapons = {},
    command = "mayor",
    max = 1,
    salary = 125,
    admin = 0,
    vote = true,
    hasLicense = false,
    mayor = true,
    PlayerLoadout = function(ply) ply.gotjob = CurTime() return false end,
    PlayerDeath = function(ply, weapon, killer)
        if ply.gotjob + 120 < CurTime() then
            ply:teamBan(TEAM_MAYOR, 120)
            ply:changeTeam(GAMEMODE.DefaultTeam, true)
            if killer:IsPlayer() then
                DarkRP.notifyAll(0, 4, "The mayor has been killed and is therefor demoted.")
            else
                DarkRP.notifyAll(0, 4, "The mayor has died and is therefor demoted.")
            end
        end
    end,
    jobs_government = true
})

TEAM_POLICE = DarkRP.createJob("Civil Protection", {
    color = Color(25, 25, 170, 255),
    model = {"models/humans/nypd1940/male_01.mdl",
        "models/humans/nypd1940/male_02.mdl",
        "models/humans/nypd1940/male_03.mdl",
        "models/humans/nypd1940/male_04.mdl",
        "models/humans/nypd1940/male_05.mdl",
        "models/humans/nypd1940/male_06.mdl",
        "models/humans/nypd1940/male_07.mdl",
        "models/humans/nypd1940/male_08.mdl",
        "models/humans/nypd1940/male_09.mdl",
        "models/player/police.mdl", "models/player/police_fem.mdl"},
    description = [[The protector of every citizen that lives in the city.
		You have the power to arrest criminals and protect innocents.
		Hit a player with your arrest baton to put them in jail.
		Bash a player with a stunstick and they may learn to obey the law.
		The Battering Ram can break down the door of a criminal, with a warrant for their arrest.
		The Battering Ram can also unfreeze frozen props (if enabled).
		Type /wanted <name> to alert the public to the presence of a criminal.]],
    weapons = {"stungun","breathalizer","weapon_cuff_police","arrest_stick", "unarrest_stick", "weapon_real_cs_glock18", "stunstick", "door_ram", "weaponchecker","keypad_cracker","keys","fists"},
    command = "cp",
    max = 4,
    salary = GAMEMODE.Config.normalsalary * 1.45,
    admin = 0,
    level = 5,
    vote = true,
    hasLicense = true,
    runspeed = 450,
    walkspeed = GAMEMODE.Config.walkspeed,
    PlayerLoadout = function(ply) ply:SetHealth(125) ply:SetArmor(25)  return false end,
    jobs_government = true
})

TEAM_CHIEF = DarkRP.createJob("Civil Protection Chief", {
    color = Color(20, 20, 255, 255),
    model = "models/player/combine_soldier_prisonguard.mdl",
    description = [[The Chief is the leader of the Civil Protection unit.
		Coordinate the police force to enforce law in the city.
		Hit a player with arrest baton to put them in jail.
		Bash a player with a stunstick and they may learn to obey the law.
		The Battering Ram can break down the door of a criminal, with a warrant for his/her arrest.
		Type /wanted <name> to alert the public to the presence of a criminal.]],
    weapons = {"stungun","breathalizer","weapon_cuff_police","arrest_stick", "unarrest_stick", "weapon_real_cs_desert_eagle", "stunstick", "door_ram", "weaponchecker","keys","fists"},
    command = "chief",
    max = 1,
    salary = GAMEMODE.Config.normalsalary * 1.67,
    admin = 0,
    vote = true,
    hasLicense = true,
    chief = true,
    level = 10,
    runspeed = 450,
    walkspeed = GAMEMODE.Config.walkspeed,
    NeedToChangeFrom = TEAM_POLICE,
    ammo = {
        ["pistol"] = 60,
    },
    PlayerLoadout = function(ply) ply:SetHealth(125) ply:SetArmor(50) return false end,
    jobs_government = true
})



TEAM_SWAT = DarkRP.createJob("S.W.A.T", {
    color = Color(20, 20, 150, 255),
    model = {"models/player/riot.mdl","models/player/gasmask.mdl","models/player/urban.mdl","models/player/swat.mdl"},
    description = [[As S.W.A.T you enforce the laws. You are the highest class of Police. You can get weapons from the S.W.A.T Armory wen there is a lockdown.]],
    weapons = {"med_kit","riot_shield","weapon_cuff_police","arrest_stick", "unarrest_stick", "weapon_real_cs_desert_eagle", "stunstick", "door_ram","weaponchecker","fists_strong"},
    command = "becomeswat",
    max = 3,
    salary = GAMEMODE.Config.normalsalary * 1.9,
    admin = 0,
    vote = true,
    hasLicense = true,
    level = 25,
    ammo = {
        ["pistol"] = 60,
    },
    VIPOnly = {"vip","vipp","vippp"},
    vipName = "VIP",
    runspeed = 360,
    walkspeed = GAMEMODE.Config.walkspeed,
    PlayerLoadout = function(ply) ply:SetHealth(125) ply:SetArmor(80)  return false end,
    jobs_government = true
})

TEAM_K9 = DarkRP.createJob("K9 Police dog", {
    color = Color(20, 20, 150, 255),
    model = "models/glayer_dog.mdl",
    description = [[As K9 dog you help the police catch criminals]],
    weapons = {"k9_teeth","weaponchecker"},
    command = "becomek9",
    max = 3,
    salary = GAMEMODE.Config.normalsalary * 1.9,
    admin = 0,
    hasLicense = true,
    level = 25,
    VIPOnly = {"vippp"},
    vipName = "VIP++",
    runspeed = 460,
    walkspeed =380,
    PlayerLoadout = function(ply) ply:SetViewOffset(Vector(0,0,40))  timer.Simple(1,function() if IsValid(ply) then GAMEMODE:SetPlayerSpeed(ply, 380, 460) ply:SetJumpPower(380) end end) return true end,
    jobs_government = true,
    jobs_animal = true,
    jobs_vippp = true
})


TEAM_FIREFIGHTER =  DarkRP.createJob("Fire Fighter", {
    color = Color(255, 165, 51, 255),
    model = "models/fearless/fireman2.mdl",
    description = [[You have to turn off fires. Get a firetruck and respond to 911 calls]],
    weapons = {"fire_extinguisher", "fire_axe", "fire_hose"},
    command = "firefighter",
    max = 3,
    salary = 45,
    admin = 0,
    vote = false,
    hasLicense = false,
    jobs_government = true
})



TEAM_SECRET_AGENT = DarkRP.createJob("Secret Agent", {
    color = Color(255, 140, 0, 255),
    model = "models/player/smith.mdl",
    description = [[You are a secret agent. You are there to protect the Mayor from any harm. Always walk beside the mayor and make sure he is safe. You are willing to give your life for the Mayor]],
    weapons = {"fas2_glock20","fists","weapon_cuff_police","weaponchecker"},
    command = "secretagent",
    max = 3,
    salary = 75,
    admin = 0,
    vote = false,
    level = 25,
    hasLicense = true,
    runspeed = 360,
    walkspeed = GAMEMODE.Config.walkspeed,
    PlayerLoadout = function(ply) ply:SetArmor(5)
    timer.Create("secretservice_mayor"..ply:SteamID(),5,0,function()
        for k,v in pairs( player.GetAll()) do
            if v:Team() == TEAM_MAYOR then
                if  v:GetPos():Distance(ply:GetPos()) > 1750 then
                    ply:PrintMessage( HUD_PRINTCENTER, "Get closer to the mayor!" )
                    ply:TakeDamage(1,ply,ply)
                    return
                end
            end
        end
    end)
    return false end,
    jobs_government = true
})


TEAM_DETECTIVE= DarkRP.createJob("Detective", {
    color = Color(126, 200, 176, 255),
    model = {"models/player/barney.mdl","models/player/leet.mdl","models/player/arctic.mdl"},
    description = [[Your job is to solve crimes. Find body's and scan them with your DNA Scanner to retrive the DNA of the killer. You inform the police automatically about your findings, and you earn a 50 bucks per body]],
    weapons = {  "dnascanner", "weaponchecker", "weapon_real_cs_p228","fists" },
    command = "detective",
    max = 2,
    salary = 15,
    admin = 0,
    level = 3,
    vote = false,
    hasLicense = false,
    PlayerLoadout = function(ply)
        if not IsValid(ply.hat) then
            local hat = ents.Create("ttt_hat_deerstalker")
            if not IsValid(hat) then return end

            hat:SetPos(ply:GetPos() + Vector(0,0,70))
            hat:SetAngles(ply:GetAngles())

            hat:SetParent(ply)

            ply.hat = hat

            hat:Spawn()
        end
        return false
    end,
    jobs_government = true
})
