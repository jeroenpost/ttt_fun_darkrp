--[[---------------------------------------------------------------------------
DarkRP custom jobs
---------------------------------------------------------------------------

This file contains your custom jobs.
This file should also contain jobs from DarkRP that you edited.

Note: If you want to edit a default DarkRP job, first disable it in darkrp_config/disabled_defaults.lua
	Once you've done that, copy and paste the job to this file and edit it.

The default jobs can be found here:
https://github.com/FPtje/DarkRP/blob/master/gamemode/config/jobrelated.lua

For examples and explanation please visit this wiki page:
http://wiki.darkrp.com/index.php/DarkRP:CustomJobFields



Add jobs under the following line:
---------------------------------------------------------------------------]]--

if SERVER then
    AddCSLuaFile("darkrp_customthings/jobs_general.lua");
    AddCSLuaFile("darkrp_customthings/jobs_animal.lua");
    AddCSLuaFile("darkrp_customthings/jobs_criminals.lua");
    AddCSLuaFile("darkrp_customthings/jobs_government.lua");
    AddCSLuaFile("darkrp_customthings/jobs_dealers.lua");
    AddCSLuaFile("darkrp_customthings/jobs_heroes.lua");
    AddCSLuaFile("darkrp_customthings/jobs_special.lua");
    AddCSLuaFile("darkrp_customthings/jobs_vip.lua");
    AddCSLuaFile("darkrp_customthings/jobs_admin.lua");
end
include("darkrp_customthings/jobs_general.lua");
include("darkrp_customthings/jobs_animal.lua");
include("darkrp_customthings/jobs_criminals.lua");
include("darkrp_customthings/jobs_government.lua");
include("darkrp_customthings/jobs_dealers.lua");
include("darkrp_customthings/jobs_heroes.lua");
include("darkrp_customthings/jobs_special.lua");
include("darkrp_customthings/jobs_vip.lua");
include("darkrp_customthings/jobs_admin.lua");








--[[---------------------------------------------------------------------------
Define which team joining players spawn into and what team you change to if demoted
---------------------------------------------------------------------------]]--
GAMEMODE.DefaultTeam = TEAM_CITIZEN


--[[---------------------------------------------------------------------------
Define which teams belong to civil protection
Civil protection can set warrants, make people wanted and do some other police related things
---------------------------------------------------------------------------]]--
GAMEMODE.CivilProtection = {
	[TEAM_POLICE] = true,
	[TEAM_CHIEF] = true,
	[TEAM_MAYOR] = true,
    [TEAM_SWAT] = true,
}

--[[---------------------------------------------------------------------------
Jobs that are hitmen (enables the hitman menu)
---------------------------------------------------------------------------]]--
DarkRP.addHitmanTeam(TEAM_MOB)
DarkRP.addHitmanTeam(TEAM_MAFFIABOSS)
DarkRP.addHitmanTeam(TEAM_HITMAN)


JOBS_criminals = {TEAM_MOB, TEAM_MAFFIA, TEAM_MAFFIABOSS, TEAM_BANKROBBER, TEAM_GANG, TEAM_VENOM, TEAM_THIEF, TEAM_JOKER, TEAM_SHREK}
