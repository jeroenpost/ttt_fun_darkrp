DarkRP.createEntity("Drug lab", {
    ent = "drug_lab",
    model = "models/props_lab/crematorcase.mdl",
    price = 400,
    max = 3,
    cmd = "buydruglab",
    allowed = {TEAM_GANG, TEAM_MOB, TEAM_JOKER, TEAM_DRUGDEALER},
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Stove", {
    ent = "meth_stove",
    model = "models/props_c17/furnitureStove001a.mdl",
    price = 500,
    max = 1,
    cmd = "buyamethstove",
    allowed = {TEAM_JOKER, TEAM_DRUGDEALER},
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Uncooked Meth", {
    ent = "uncooked_meth",
    model = "models/props_c17/oildrum001.mdl",
    price = 250,
    max = 10,
    cmd = "buyuncokedmeth",
    allowed = {TEAM_JOKER, TEAM_DRUGDEALER},
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Weed Plant", {
    ent = "weed_plant",
    model = "models/props_junk/watermelon01.mdl",
    price = 650,
    max = 15,
    cmd = "buyweedplant",
    allowed = {TEAM_JOKER, TEAM_DRUGDEALER},
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Growable Weed Plant", {
    ent = "neths_weed_plant",
    model = "models/nv_weed_plant/plant_big.mdl",
    price = 750,
    max = 15,
    cmd = "buyweedplant2",
    allowed = {TEAM_JOKER, TEAM_DRUGDEALER},
    shop_drugs = true,
    shop = true
})


    -- ======= DRUGSSS ====================================

    DarkRP.createShipment( "Beer (5)", {
        model = "models/drug_mod/alcohol_can.mdl" ,
        entity =  "durgz_alcohol",
        price = 500,
        amount = 5,
        seperate = false,
        pricesep = 900,
        noship = false,
        allowed = {TEAM_JOKER, TEAM_DRUGDEALER, TEAM_COOK, TEAM_BARTENDER},
        shop_drugs = true,
        shop_food = true,
        shop = true
    })
    DarkRP.createShipment( "Asprin (10)", {
        model = "models/jaanus/aspbtl.mdl" ,
        entity =  "durgz_aspirin",
        price = 5000,
        amount = 10,
        seperate = false,
        pricesep = 200,
        noship = false,
        allowed = {TEAM_JOKER, TEAM_DRUGDEALER, TEAM_MEDIC},
        shop_drugs = true,
        shop = true
    })
    DarkRP.createShipment( "Cigarette (5)", {
        model = "models/boxopencigshib.mdl" ,
        entity =  "durgz_cigarette",
        price = 1200,
        amount = 5,
        seperate = false,
        pricesep = 300,
        noship = false,
        allowed = {TEAM_JOKER, TEAM_DRUGDEALER, TEAM_MEDIC, TEAM_ASSASIN,TEAM_PIMP,TEAM_PROSTITUTE},
        shop_drugs = true,
        shop = true
    })
    DarkRP.createShipment( "Cigarette", {
        model = "models/boxopencigshib.mdl" ,
        entity =  "durgz_cigarette",
        price = 350,
        amount = 5,
        seperate = true,
        pricesep = 300,
        noship = true,
        allowed = {TEAM_JOKER, TEAM_DRUGDEALER, TEAM_MEDIC, TEAM_ASSASIN,TEAM_PIMP,TEAM_PROSTITUTE, TEAM_MOB, TEAM_SS,TEAM_SSOFFICER},
        shop_drugs = true,
        shop = true
    })
    DarkRP.createShipment( "Cocaine (5)", {
        model = "models/cocn.mdl" ,
        entity =  "durgz_cocaine",
        price = 3500,
        amount = 5,
        seperate = false,
        pricesep = 900,
        noship = false,
        allowed = {TEAM_JOKER, TEAM_DRUGDEALER},
        shop_drugs = true,
        shop = true
    })
    DarkRP.createShipment( "Heroine (5)", {
        model = "models/katharsmodels/syringe_out/syringe_out.mdl" ,
        entity =  "durgz_heroine",
        price = 5500,
        amount = 5,
        seperate = false,
        pricesep = 1300,
        noship = false,
        allowed = {TEAM_JOKER, TEAM_DRUGDEALER},
        shop_drugs = true,
        shop = true
    })
    DarkRP.createShipment( "LSD (5)", {
        model = "models/smile/smile.mdl" ,
        entity =  "durgz_lsd",
        price = 4500,
        amount = 5,
        seperate = false,
        pricesep = 1200,
        noship = false,
        allowed = {TEAM_JOKER, TEAM_DRUGDEALER},
        shop_drugs = true,
        shop = true
    })

    DarkRP.createShipment( "Mushroom (5)", {
        model = "models/ipha/mushroom_small.mdl" ,
        entity =  "durgz_mushroom",
        price = 3999,
        amount = 5,
        seperate = false,
        pricesep = 900,
        noship = false,
        allowed = {TEAM_JOKER, TEAM_DRUGDEALER},
        shop_drugs = true,
        shop = true
    })
    DarkRP.createShipment( "PCP (5)", {
        model = "models/marioragdoll/Super Mario Galaxy/star/star.mdl",
        entity =  "durgz_pcp",
        price = 4500,
        amount = 5,
        seperate = false,
        pricesep = 950,
        noship = false,
        allowed = {TEAM_JOKER, TEAM_DRUGDEALER},
        shop_drugs = true,
        shop = true
    })
    DarkRP.createShipment( "Water (5)", {
        model = "models/drug_mod/the_bottle_of_water.mdl",
        entity =  "durgz_water",
        price = 50,
        amount = 5,
        seperate = false,
        pricesep = 10,
        noship = false,
        allowed = {TEAM_JOKER, TEAM_DRUGDEALER,TEAM_MEDIC},
        shop_drugs = true,
        shop = true
    })

    DarkRP.createShipment( "Weed (5)", {
        model = "models/katharsmodels/contraband/zak_wiet/zak_wiet.mdl",
        entity =  "durgz_weed",
        price = 750,
        amount = 5,
        seperate = false,
        pricesep = 200,
        noship = false,
        allowed = {TEAM_JOKER, TEAM_DRUGDEALER},
        shop_drugs = true,
        shop = true
    })

-- Cocaine Lab
local allowedTeam = TEAM_COCAINE

DarkRP.createEntity("Pot for growing Coca Plant", {
    ent = "ecl_plant_pot",
    model = "models/props_junk/terracotta01.mdl",
    price = 150,
    max = 5,
    cmd = "eclbuypp",

    allowed = allowedTeam,
    shop_drugs = true,
    shop = true
})


DarkRP.createEntity("Coca Plant Seed", {
    ent = "ecl_seed",
    model =  "models/props/cs_office/plant01_gib1.mdl",
    price = 250,
    max = 5,
    cmd = "eclbuyseed",

    allowed = allowedTeam,
    shop_drugs = true,
    shop = true
})


DarkRP.createEntity("Stove", {
    ent = "ecl_stove",
    model = "models/props_c17/furnitureStove001a.mdl",
    price = 5000,
    max = 1,
    cmd = "eclbuyst",

    allowed = allowedTeam,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Pot for cooking Cocaine", {
    ent = "ecl_pot",
    model = "models/props_c17/metalPot001a.mdl",
    price = 500,
    max = 4,
    cmd = "eclbuyp",

    allowed = allowedTeam,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Box for collecting leaves", {
    ent = "ecl_leafbox",
    model = "models/props_junk/cardboard_box004a.mdl",
    price = 150,
    max = 5,
    cmd = "eclbuycl",

    allowed = allowedTeam,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Kerosin", {
    ent = "ecl_kerosin",
    model = "models/props_junk/metal_paintcan001a.mdl",
    price = 750,
    max = 8,
    cmd = "eclbuyks",

    allowed = allowedTeam,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Gas for Stove", {
    ent = "ecl_gas",
    model = "models/props_junk/propane_tank001a.mdl",
    price = 500,
    max = 5,
    cmd = "eclbuygs",

    allowed = allowedTeam,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Gasoline", {
    ent = "ecl_gasoline",
    model = "models/props_junk/metalgascan.mdl",
    price = 1000,
    max = 2,
    cmd = "eclbuygl",

    allowed = allowedTeam,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Water for Drufing Leaves", {
    ent = "ecl_drafted",
    model = "models/props_junk/plasticbucket001a.mdl",
    price = 500,
    max = 4,
    cmd = "eclbuydl",

    allowed = allowedTeam,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Sulfuric Acid", {
    ent = "ecl_sulfuric_acid",
    model = "models/props_junk/garbage_milkcarton001a.mdl",
    price = 1000,
    max = 2,
    cmd = "eclbuysa",

    allowed = allowedTeam,
    shop_drugs = true,
    shop = true
})

