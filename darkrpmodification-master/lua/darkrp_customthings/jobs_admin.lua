TEAM_AOD = DarkRP.createJob("Admin On Duty", {
    color = Color(255, 30, 13, 255),
    model = "models/player/Combine_Super_Soldier.mdl",
    description = [[This is a non Rp class, respond to admin calls.]],
    weapons = {"weapon_tttfun_admingun","fists","dnascanner","carpick",},--"weapon_kidnapper"},
    command = "aod",
    max = 10,
    salary = 35,
    admin = 1,
    vote = false,
    hasLicense = false,
    jobs_admin = true
})