/*---------------------------------------------------------------------------
DarkRP custom shipments and guns
---------------------------------------------------------------------------

This file contains your custom shipments and guns.
This file should also contain shipments and guns from DarkRP that you edited.

Note: If you want to edit a default DarkRP shipment, first disable it in darkrp_config/disabled_defaults.lua
Once you've done that, copy and paste the shipment to this file and edit it.

The default shipments and guns can be found here:
https://github.com/FPtje/DarkRP/blob/master/gamemode/config/addentities.lua

For examples and explanation please visit this wiki page:
http://wiki.darkrp.com/index.php/DarkRP:CustomShipmentFields


Add shipments and guns under the following line:
---------------------------------------------------------------------------*/

DarkRP.createShipment("Restraint Rope", {
    entity = "weapon_cuff_rope",
    model = "models/props_lab/teleportring.mdl",
    price = 2500,
    max = 3,
    seperate = false,
    pricesep = 2500,
    amount = 1,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("Restraint Elastic", {
    entity = "weapon_cuff_elastic",
    model = "models/props_lab/teleportring.mdl",
    price = 3000,
    max = 3,
    seperate = false,
    pricesep = 3000,
    amount = 1,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("Plastic Handcuffs", {
    entity = "weapon_cuff_plastic",
    model = "models/props_lab/teleportring.mdl",
    price = 1750,
    max = 3,
    seperate = false,
    pricesep = 1750,
    amount = 1,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("Basic Handcuffs", {
    entity = "weapon_cuff_standard",
    model = "models/props_lab/teleportring.mdl",
    price = 750,
    max = 3,
    seperate = false,
    pricesep = 750,
    amount = 1,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("Shackles", {
    entity = "weapon_cuff_shackles",
    model = "models/props_lab/teleportring.mdl",
    price = 1750,
    max = 3,
    seperate = false,
    pricesep = 1750,
    amount = 1,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("Elastic Handcuffs", {
    entity = "weapon_cuff_elastic",
    model = "models/props_lab/teleportring.mdl",
    price = 2500,
    max = 3,
    seperate = false,
    pricesep = 2500,
    amount = 1,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("Leash Rope", {
    entity = "weapon_leash_rope",
    model = "models/props_lab/teleportring.mdl",
    price = 750,
    max = 3,
    seperate = false,
    pricesep = 750,
    amount = 1,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("Leash Elastic", {
    entity = "weapon_leash_elastic",
    model = "models/props_lab/teleportring.mdl",
    price = 1750,
    max = 3,
    seperate = false,
    pricesep = 1750,
    amount = 1,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("Angry Hobo", {
    model = "models/props_junk/shoe001a.mdl",
    entity = "angry_hobo",
    price = 300,
    amount = 1,
    seperate = false,
    pricesep = 300,
    noship = false,
    allowed = {TEAM_HOBO },
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("Lockpicks", {
    model = "models/props_c17/TrapPropeller_Lever.mdl",
    entity = "ent_lockpick",
    price = 1000,
    amount = 10,
    seperate = false,
    pricesep = 1000,
    noship = false,
    allowed = {TEAM_BLACKMARKET,TEAM_MOB,TEAM_THIEF, TEAM_BANKROBBER,TEAM_MAFFIABOSS},
    shop_item = true,
    shop = true
})

DarkRP.createShipment("Gas", {
    model = "models/props_c17/tools_wrench01a.mdl",
    entity = "jerrycan",
    price = 400,
    amount = 1,
    seperate = false,
    pricesep = 400,
    noship = false,
    allowed = {TEAM_MECHANIC, TEAM_TOWER},
    shop_item = true,
    shop = true
})

DarkRP.createShipment("Vehicle Repair Wrench", {
    model = "models/props_c17/tools_wrench01a.mdl",
    entity = "vc_repair",
    price = 500,
    amount = 1,
    seperate = false,
    pricesep = 500,
    noship = false,
    allowed = {TEAM_MECHANIC},
    shop_item = true,
    shop = true
})

DarkRP.createShipment("Spike Strip", {
    model = "models/props_phx/gears/rack18.mdl",
    entity = "spikestrip",
    price = 250,
    amount = 1,
    seperate = false,
    pricesep = 250,
    noship = false,
    allowed = {TEAM_POLICE,TEAM_CHIEF,TEAM_BLACKMARKET},
    shop_weapons = true,
    shop_items = true,
    shop = true
})


-- Farmers
DarkRP.createShipment("Banana Seeds", {
    model = "models/props_junk/cardboard_box004a.mdl",
    entity = "farming_seedboxbanana",
    price = 100,
    amount = 1,
    seperate = false,
    pricesep = 100,
    noship = false,
    allowed = {TEAM_FARMER},
    shop_item = true,
    shop_food = true,
    shop = true
})
DarkRP.createShipment("Melon Seeds", {
    model = "models/props_junk/cardboard_box004a.mdl",
    entity = "farming_seedboxmelon",
    price = 100,
    amount = 1,
    seperate = false,
    pricesep = 100,
    noship = false,
    allowed = {TEAM_FARMER},
    shop_item = true,
    shop_food = true,
    shop = true
})
DarkRP.createShipment("Orange Seeds", {
    model = "models/props_junk/cardboard_box004a.mdl",
    entity = "farming_seedboxorange",
    price = 100,
    amount = 1,
    seperate = false,
    pricesep = 100,
    noship = false,
    allowed = {TEAM_FARMER},
    shop_item = true,
    shop_food = true,
    shop = true
})
DarkRP.createShipment("Pizza Seeds", {
    model = "models/props_junk/cardboard_box004a.mdl",
    entity = "farming_seedboxpizza",
    price = 100,
    amount = 1,
    seperate = false,
    pricesep = 100,
    noship = false,
    allowed = {TEAM_FARMER},
    shop_item = true,
    shop_food = true,
    shop = true
})


-- Gun Dealer WEAPONS ====================================

DarkRP.createShipment("Ak47 (6)", {
    model = "models/weapons/w_rif_ak47.mdl",
    entity = "weapon_real_cs_ak47",
    price = 8000,
    amount = 6,
    seperate = false,
    pricesep = 2600,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("STEYR AUG A1 (6)", {
    model = "models/weapons/w_rif_ak47.mdl",
    entity =  "weapon_real_cs_aug",
    price = 8000,
    amount = 6,
    seperate = false,
    pricesep = 3200,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("UTG L96 AWP SNIPER (5)", {
    model = "models/weapons/w_snip_awp.mdl",
    entity =  "weapon_real_cs_awp",
    price = 14000,
    amount = 5,
    seperate = false,
    pricesep = 4200,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("FAMAS F1 (6)", {
    model = "models/weapons/w_rif_famas.mdl",
    entity =  "weapon_real_cs_famas",
    price = 7000,
    amount = 6,
    seperate = false,
    pricesep = 2900,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("FN FIVE-SEVEN (6)", {
    model = "models/weapons/w_pist_fiveseven.mdl",
    entity =  "weapon_real_cs_five-seven",
    price = 3000,
    amount = 6,
    seperate = false,
    pricesep = 2800,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("HK G3SG1 SNIPER (5)", {
    model =  "models/weapons/w_snip_g3sg1.mdl",
    entity =  "weapon_real_cs_g3sg1",
    price = 10000,
    amount = 5,
    seperate = false,
    pricesep = 3200,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("GALIL SAR 5.56MM (6)", {
    model =  "models/weapons/w_rif_galil.mdl",
    entity =  "weapon_real_cs_galil",
    price = 8000,
    amount = 6,
    seperate = false,
    pricesep = 3400,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment( "GLOCK 18 (5)", {
    model =  "models/weapons/w_pist_glock18.mdl",
    entity =  "weapon_real_cs_glock18",
    price = 3250,
    amount = 5,
    seperate = false,
    pricesep = 2000,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("EXPLOSIVE GRENADE (5)", {
    model =  "models/weapons/cstrike/c_eq_fraggrenade.mdl",
    entity =  "weapon_real_cs_grenade",
    price = 4500,
    amount = 5,
    seperate = false,
    pricesep = 1000,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("HK MP-5A5 (6)", {
    model = "models/weapons/w_smg_mp5.mdl" ,
    entity =  "weapon_real_cs_mp5a5",
    price = 6000,
    amount = 6,
    seperate = false,
    pricesep = 2800,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("FN P90 (6)", {
    model = "models/weapons/w_smg_p90.mdl" ,
    entity =  "weapon_real_cs_p90",
    price = 6000,
    amount = 6,
    seperate = false,
    pricesep = 2000,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment( "SIG-SAUER P228 (6)", {
    model = "models/weapons/w_pist_p228.mdl" ,
    entity =  "weapon_real_cs_p228",
    price = 2400,
    amount = 6,
    seperate = false,
    pricesep = 2000,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment( "BENELLI M3 SUPER 90 (6)", {
    model = "models/weapons/w_shot_m3super90.mdl" ,
    entity =  "weapon_real_cs_pumpshotgun",
    price = 8000,
    amount = 6,
    seperate = false,
    pricesep = 2000,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment( "XM1014 (6)", {
    model = "models/weapons/w_shot_xm1014.mdl" ,
    entity =  "weapon_real_cs_xm1014",
    price = 9000,
    amount = 6,
    seperate = false,
    pricesep = 2000,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})


DarkRP.createShipment( "STEYR SCOUT SNIPER (6)", {
    model = "models/weapons/w_snip_scout.mdl" ,
    entity =  "weapon_real_cs_scout",
    price = 10000,
    amount = 6,
    seperate = false,
    pricesep = 2000,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment( "SIG SG-550 SNIPER (6)", {
    model = "models/weapons/w_snip_sg550.mdl" ,
    entity =  "weapon_real_cs_sg550",
    price = 9000,
    amount = 6,
    seperate = false,
    pricesep = 2000,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment(  "SIG SG-552 (6)", {
    model = "models/weapons/w_rif_sg552.mdl" ,
    entity =  "weapon_real_cs_sg552",
    price = 8000,
    amount = 6,
    seperate = false,
    pricesep = 2000,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment( "SMOKE GRENADE (5)", {
    model = "models/weapons/w_eq_smokegrenade.mdl" ,
    entity =  "weapon_real_cs_smoke",
    price = 2000,
    amount = 6,
    seperate = false,
    pricesep = 900,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment( "STEYR TMP (6)", {
    model = "models/weapons/w_smg_tmp.mdl" ,
    entity =  "weapon_real_cs_tmp",
    price = 7000,
    amount = 6,
    seperate = false,
    pricesep = 15000,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment( "HK UMP-45 (6)", {
    model = "models/weapons/w_smg_ump45.mdl" ,
    entity =  "weapon_real_cs_ump_45",
    price = 6000,
    amount = 6,
    seperate = false,
    pricesep = 900,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})

-- Black Market Dealer WEAPONS ====================================

DarkRP.createShipment("FLASH GRENADE (2)", {
    model = "models/weapons/w_eq_flashbang.mdl",
    entity =  "weapon_real_cs_flash",
    price = 1200,
    amount = 5,
    seperate = false,
    pricesep = 3200,
    noship = false,
    allowed = {TEAM_GUN,TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment( "Silent Knife (1)", {
    model = "models/weapons/w_knife_t.mdl" ,
    entity =  "silent_knife",
    price = 3000,
    amount = 1,
    seperate = false,
    pricesep = 3000,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment( "HK USP .45 (6)", {
    model = "models/weapons/w_pist_usp.mdl" ,
    entity =  "weapon_real_cs_usp",
    price = 3000,
    amount = 6,
    seperate = false,
    pricesep = 2000,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("M249 SAW (6)", {
    model = "models/weapons/w_mach_m249para.mdl" ,
    entity =  "weapon_real_cs_m249",
    price = 11000,
    amount = 6,
    seperate = false,
    pricesep = 2000,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("UZI (6)", {
    model = "models/weapons/w_smg_mac10.mdl" ,
    entity =  "weapon_real_cs_mac10",
    price = 4000,
    amount = 6,
    seperate = false,
    pricesep = 2000,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("KNIFE (2)", {
    model = "models/weapons/w_knife_t.mdl" ,
    entity =  "weapon_real_cs_knife",
    price = 1500,
    amount = 2,
    seperate = false,
    pricesep = 2000,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("DUAL BERETTA ELITES (6)", {
    model = "models/weapons/w_pist_p228.mdl",
    entity =  "weapon_real_cs_elites",
    price = 4000,
    amount = 6,
    seperate = false,
    pricesep = 3600,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("MedKit (1)", {
    model = "models/Items/HealthKit.mdl",
    entity =  "med_kit",
    price = 2000,
    amount = 1,
    seperate = false,
    pricesep = 3600,
    noship = false,
    allowed = {TEAM_BLACKMARKET, TEAM_MEDIC},
    shop_weapons = true,
    shop_drugs = true,
    shop = true
})

-- ======== POKEMON WEPS
DarkRP.createShipment("Psybeam", {
    model = 'models/props_wasteland/panel_leverHandle001a.mdl',
    entity = "psybeam",
    price = 1500,
    amount = 1,
    seperate = false,
    pricesep = 3000,
    noship = false,
    allowed = {TEAM_POKEMON},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("Lightning Bolts", {
    model = 'models/props_wasteland/panel_leverHandle001a.mdl',
    entity = "jolteon_bolts",
    price = 1500,
    amount = 1,
    seperate = false,
    pricesep = 1500,
    noship = false,
    allowed = {TEAM_POKEMON},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("Fire Breath", {
    model = 'models/props_wasteland/panel_leverHandle001a.mdl',
    entity = "flareon_fire",
    price = 1500,
    amount = 1,
    seperate = false,
    pricesep = 1500,
    noship = false,
    allowed = {TEAM_POKEMON},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("Water Splash", {
    model = 'models/props_wasteland/panel_leverHandle001a.mdl',
    entity = "vaporeon_water",
    price = 500,
    amount = 1,
    seperate = false,
    pricesep = 500,
    noship = false,
    allowed = {TEAM_POKEMON},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("Tail Whip", {
    model = 'models/props_wasteland/panel_leverHandle001a.mdl',
    entity = "tail_whip",
    price = 500,
    amount = 1,
    seperate = false,
    pricesep = 500,
    noship = false,
    allowed = {TEAM_POKEMON},
    shop_weapons = true,
    shop = true
})

-- FA:S Weps


DarkRP.createShipment("AK-47", {
    model = "models/weapons/w_rif_ak47.mdl",
    entity = "fas2_ak47",
    price = 12000,
    amount = 3,
    seperate = false,
    pricesep = 2300,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("AK-74", {
    model ="models/weapons/w_rif_ak47.mdl",
    entity = "fas2_ak74",
    price = 12000,
    amount = 3,
    seperate = false,
    pricesep = 2300,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("G3A3", {
    model = "models/weapons/w_smg_mp5.mdl",
    entity = "fas2_g3",
    price = 15500,
    amount = 3,
    seperate = false,
    pricesep = 2500,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("Glock-20", {
    model = "models/weapons/w_pist_glock18.mdl",
    entity = "fas2_glock20",
    price = 4600,
    amount = 3,
    seperate = false,
    pricesep = 1000,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("M3 S90", {
    model = "models/weapons/w_smg_mp5.mdl",
    entity = "fas2_m3s90",
    price = 17500,
    amount = 3,
    seperate = false,
    pricesep = 2800,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("M24", {
    model = "models/weapons/w_rif_ak47.mdl",
    entity = "fas2_m24",
    price = 8000,
    amount = 3,
    seperate = false,
    pricesep = 1800,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})


DarkRP.createShipment("Mp5 A5", {
    model = "models/weapons/w_smg_mp5.mdl",
    entity = "fas2_mp5a5",
    price = 7000,
    amount = 3,
    seperate = false,
    pricesep = 1300,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})


DarkRP.createShipment("PP-19 Bizon", {
    model = "models/weapons/w_smg_biz.mdl",
    entity = "fas2_pp19",
    price = 7000,
    amount = 3,
    seperate = false,
    pricesep = 1300,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})
DarkRP.createShipment("Raging Bull", {
    model = "models/weapons/w_357.mdl",
    entity = "fas2_ragingbull",
    price = 5400,
    amount = 3,
    seperate = false,
    pricesep = 2700,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("RPK", {
    model = "models/weapons/w_rif_ak47.mdl",
    entity = "fas2_rpk",
    price = 13000,
    amount = 3,
    seperate = false,
    pricesep = 2750,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("RK-95", {
    model = "models/weapons/w_snip_sg550.mdl",
    entity = "fas2_rk95",
    price = 12500,
    amount = 3,
    seperate = false,
    pricesep = 2500,
    noship = false,
    allowed = {TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("SG552", {
    model = "models/weapons/w_snip_sg550.mdl",
    entity = "fas2_sg552",
    price = 12000,
    amount = 3,
    seperate = false,
    pricesep = 2500,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})

DarkRP.createShipment("SKS", {
    model = "models/weapons/world/rifles/sks.mdl",
    entity = "fas2_sks",
    price = 6400,
    amount = 3,
    seperate = false,
    pricesep = 1500,
    noship = false,
    allowed = {TEAM_GUN},
    shop_weapons = true,
    shop = true
})

-- FIRE

DarkRP.createShipment("Molotov Cocktail (Pyro Special)", {
    model = "models/props_junk/garbage_glassbottle003a.mdl",
    entity = "fire_molotov",
    price = 45000,
    amount = 5,
    seperate = false,
    pricesep = 10000,
    noship = false,
    allowed = {TEAM_PYRO,TEAM_BLACKMARKET},
    shop_weapons = true,
    shop = true
})



-- Pokeball
DarkRP.createShipment("PokeBall", {
    model = "models/weapons/w_pokeball_thrown.mdl",
    entity = "pokeball",
    price = 550,
    amount = 1,
    seperate = false,
    pricesep = 250,
    noship = false,
    allowed = {TEAM_POKEMONTRAINER},
    shop_weapons = true,
    shop_items = true,
    shop = true
})

-- Armor
DarkRP.createShipment("Armor (3x25)", {
    model = "models/gibs/shield_scanner_gib3.mdl",
    entity = "gb_armor",
    price = 1500,
    amount = 3,
    seperate = false,
    pricesep = 250,
    noship = false,
    allowed = {TEAM_MEDIC},
    shop_weapons = true,
    shop_items = true,
    shop = true
})
DarkRP.createShipment("Armor (3x100)", {
    model = "models/gibs/shield_scanner_gib3.mdl",
    entity = "gb_armor100",
    price = 3750,
    amount = 3,
    seperate = false,
    pricesep = 250,
    noship = false,
    allowed = {TEAM_MEDIC},
    shop_weapons = true,
    shop_items = true,
    shop = true
})
DarkRP.createShipment("Legal Marijuana ", {
    model = "models/katharsmodels/contraband/zak_wiet/zak_wiet.mdl",
    entity =  "durgz_weed",
    price = 1000,
    amount = 5,
    seperate = false,
    pricesep = 200,
    noship = false,
    allowed = {TEAM_MEDIC},
    shop_drugs = true,
    shop = true,
    shop_items = true,
})

DarkRP.createShipment("Tommygun", {
    model = "models/weapons/w_smg_p90_tommy.mdl",
    entity = "gb_tommygun",
    price = 1250,
    amount = 1,
    seperate = false,
    pricesep = 250,
    noship = false,
    allowed = {TEAM_MAFFIA, TEAM_MAFFIABOSS},
    shop_weapons = true,
    shop = true
})
