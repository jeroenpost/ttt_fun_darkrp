TEAM_POKEMON = DarkRP.createJob("Pokemon", {
    color = Color(90, 10, 10, 255),
    model = {
        "models/player/pokemon/buizel.mdl",
        "models/player_flareon.mdl",
        "models/player_jolteon.mdl",
        "models/player_vaporeon.mdl",
        "models/player_eevee.mdl",
        "models/player_espeon.mdl",
        "models/player_umbreon.mdl",
        "models/player_glaceon.mdl",
        "models/player_leafeon.mdl",
        "models/player_sylveon.mdl"

    },
    description = [[As a Pokemon you can buy pokemon Powers in the shop.\nYou can use the powers to help Police catch bad guys, or to protect your owner. You can be an independent Pokemon, or Pokemon with an owner. You should act like a pet, and you can only say your own name]],
    weapons = {"dog_bite","fists"},
    command = "pokemon",
    max = 15,
    salary = 45,
    admin = 0,
    vote = false,
    hasLicense = false,
    level = 5,
    PlayerLoadout = function(ply) ply:SetViewOffset(Vector(0,0,40))  timer.Simple(1,function() if IsValid(ply) then GAMEMODE:SetPlayerSpeed(ply, GAMEMODE.Config.walkspeed, 350) ply:SetJumpPower(350) end end) return false end,
    jobs_animal = true
})

TEAM_BIRD = DarkRP.createJob("Bird", {
    color = Color(90, 10, 10, 255),
    model = {
        "models/crow.mdl",
        "models/pigeon.mdl",
    },
    description = [[Fly around as a bird. You especially like pizza and bread]],
    weapons = {"flight"},
    command = "becomebird",
    max = 15,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = false,
    level = 5,
    PlayerLoadout = function(ply) ply:SetViewOffset(Vector(0,0,20))  timer.Simple(1,function() if IsValid(ply) then ply:SetRunSpeed(250) ply:SetJumpPower(250) end end) return true end,
    jobs_animal = true
})

TEAM_DRAGON = DarkRP.createJob("Dragon", {
    color = Color(90, 10, 10, 255),
    model = {
        "models/player/silviu/sillydragon_flying.mdl",
    },
    description = [[You are a courier! Transport stuff for people]],
    weapons = {"flight","dragon_fireball","flareon_fire"},
    command = "becomedragon",
    max = 15,
    salary = 0,
    admin = 0,
    runspeed = 350,
    walkspeed = 250,
    vote = false,
    hasLicense = false,
    PlayerLoadout = function(ply) ply:SetViewOffset(Vector(0,0,75)) ply:SetArmor(100) ply:SetHealth(200)  timer.Simple(1,function() if IsValid(ply) then ply:SetRunSpeed(350) ply:SetJumpPower(350) end end) return false end,
    VIPOnly = {"vippp"},
    vipName = "VIP++",
    jobs_vippp = true,
    jobs_animal = true
})

TEAM_VAMPIRE = DarkRP.createJob("Vampire", {
    color = Color(224, 222, 92, 255),
    model = {"models/player/demon_violinist/demon_violinist.mdl","models/player/jennkiller_m.mdl"},
    description = [[Vampires are in need for blood. Your hunger rises very quickly. Get blood before you die. You are allowed to keep someone hostage and use them as blood source. Vampires are very suspicious, Police may KOS when they see a vampire biting someone, or dragging bodies around.]],
    weapons = {"vampire_teeth"},
    command = "becomevampire",
    max = 24,
    salary = 40,
    admin = 0,
    level = 5,
    vote = false,
    runspeed = 420,
    walkspeed = GAMEMODE.Config.walkspeed,
    hasLicense = false,
    PlayerLoadout = function(ply)
        timer.Simple(1,function() if IsValid(ply) then GAMEMODE:SetPlayerSpeed(ply, 350,380) ply:SetJumpPower(380) end end)
        ply:SetNWInt("vampireHealth",100)
        timer.Create(ply:SteamID().."VampireTimer",3,0,function()
            local health = ply:GetNWInt("vampireHealth",100)
            if health > 0 then
                ply:SetNWInt("vampireHealth", health - 1)
            end
            if health <= 0 then health = 0 end
            if health == 10 then
                ply:PrintMessage( HUD_PRINTCENTER, "Your bloodlevel is super low! Get blood before you die!" )
            end
            if health < 1 then
                ply:TakeDamage(10,ply,ply)
            end
        end)
        return false end,
    jobs_animal = true
})

TEAM_ALIEN = DarkRP.createJob("Alien", {
    color = Color(70, 70, 70, 255),
    model = "models/player/slow_alien.mdl",
    description = [[Try to be invisible to normal players. Build a base and abduct people to study them toughtly. It is up to the Mayor how he responds to Aliens (friendly or not).]],
    weapons = {'alien_teeth',"dnascanner","realistic_hook", "alien_beam","disguiser",'fists',},--'weapon_kidnapper'},
    command = "becomealien",
    max = 8,
    salary = 0,
    admin = 0,
    vote = false,
    level = 10,
    hasLicense = false,
    VIPOnly = {"vip","vipp","vippp"},
    vipName = "VIP",
    runspeed = 850,
    walkspeed = 350,
    PlayerLoadout = function(ply) ply:SetHealth(125) ply:SetArmor(0) timer.Simple(1,function() if IsValid(ply) then GAMEMODE:SetPlayerSpeed(ply, 350,850) ply:SetJumpPower(550) end end) return false end,
    jobs_animal = true,
})

TEAM_ALIENQUEEN = DarkRP.createJob("Alien Queen", {
    color = Color(70, 70, 70, 255),
    model = "models/player/slow_alien.mdl",
    description = [[You are the queen of the aliens. You can set their agenda and you make up the rules of the aliens.]],
    weapons = {'alien_teeth',"dnascanner","realistic_hook", "alien_beam","disguiser",'fists_strong',},--'weapon_kidnapper'},
    command = "becomealienqueen",
    max = 1,
    salary = 0,
    admin = 0,
    vote = false,
    level = 20,
    hasLicense = false,
    VIPOnly = {"vipp","vippp"},
    vipName = "VIP+",
    runspeed = 880,
    walkspeed = 360,
    PlayerLoadout = function(ply) ply:SetHealth(150) ply:SetArmor(10) ply:SetMaterial("camos/camo20") timer.Simple(1,function() if IsValid(ply) then GAMEMODE:SetPlayerSpeed(ply, 380,880)  ply:SetJumpPower(580) end end) return false end,
    jobs_animal = true,
    jobs_vipp = true
})



TEAM_SCALES = DarkRP.createJob("General Scales", {
    color = Color(10, 244, 10, 255),
    model = "models/player/generalscales/generalscales.mdl",
    description = [[You are the enemy of the Aliens. You are not able to talk to humans, but you like Hobo's a lot, and are able to work together with the Hobo's to build homes]],
    weapons = {'fists'},
    command = "becomescales",
    max = 2,
    salary = 0,
    admin = 0,
    vote = false,
    level = 15,
    hasLicense = false,
    runspeed = 320,
    walkspeed = GAMEMODE.Config.walkspeed,
    PlayerLoadout = function(ply) ply:SetHealth(150) ply:SetArmor(0)   ply:SetJumpPower(350)  return false end,
    jobs_animal = true,

})