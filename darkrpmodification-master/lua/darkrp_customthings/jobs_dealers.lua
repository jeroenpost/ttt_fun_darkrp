
TEAM_GUN = DarkRP.createJob("Gun Dealer", {
    color = Color(255, 140, 0, 255),
    model = "models/player/monk.mdl",
    description = [[A Gun Dealer is the only person who can sell guns to other people.
		Make sure you aren't caught selling illegal firearms to the public! You might get arrested!]],
    weapons = {"fists"},
    command = "gundealer",
    max = 2,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    level = 2,
    hasLicense = false,
    jobs_dealers = true
})

TEAM_BLACKMARKET = DarkRP.createJob("Black Market Dealer", {
    color = Color(60, 60, 60, 255),
    model = "models/player/odessa.mdl",
    description = [[The black market dealer is similar to the gun dealer, but with different guns,
		and a couple other illegal items that are not guns.]],
    weapons = {"fists"},
    command = "blackmarketdealer",
    max = 2,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    level = 5,
    vote = false,
    hasLicense = false,
    jobs_dealers = true
})


TEAM_DRUGDEALER = DarkRP.createJob("Drug Dealer", {
    color = Color(255, 140, 0, 255),
    model = {

        "models/player/group03/male_01.mdl",
        "models/player/Group03/Male_02.mdl",
        "models/player/Group03/male_03.mdl",
        "models/player/Group03/Male_04.mdl",
        "models/player/Group03/Male_05.mdl",
        "models/player/Group03/Male_06.mdl",
        "models/player/Group03/Male_07.mdl",
        "models/player/Group03/Male_08.mdl",
        "models/player/Group03/Male_09.mdl",
        "models/player/Group03/Female_01.mdl",
        "models/player/Group03/Female_02.mdl",
        "models/player/Group03/Female_03.mdl",
        "models/player/Group03/Female_04.mdl",
        "models/player/Group03/Female_06.mdl",
    },
    description = [[A drug dealer can spawn drugs, methlabs and weed plants.
		Remember: All drugs are Illegal]],
    weapons = {"fists"},
    command = "drugdealer",
    max = 3,
    salary = 55,
    admin = 0,
    vote = false,
    level = 2,
    hasLicense = false,
    jobs_dealers = true
})

TEAM_PROSTITUTE = DarkRP.createJob("Prostitute", {
    color = Color(80, 0, 45, 255),
    model = {"models/player/alyx.mdl","models/player/p2_chell.mdl","models/player/mossman.mdl","models/player/barney.mdl","models/player/Group03/Male_09.mdl",
        "models/player/Group03/Female_01.mdl",},
    description = [[Provide some love to all players on the server.
		You can set a price to give some love, and are able to sell it players.]],
    weapons = {"weapon_prostitute","fists","weapon_kiss"},
    command = "prostitute",
    max = 6,
    salary = 20,
    admin = 0,
    level = 2,
    vote = false,
    hasLicense = false,
    candemote = true,
    mayorCanSetSalary = false,
    jobs_dealers = true
})

TEAM_PIMP = DarkRP.createJob("Pimp", {
    color = Color(80, 0, 45, 255),
    model = "models/player/barney.mdl",
    description = [[Earn money by selling services provided by your girls.\nYou are responsible to protect your girls from bad things happening to them.\nIn return your get a percentage of their earnings.]],
    weapons = {"weapon_pimp", "weapon_pimpgun","fists","weapon_kiss"},
    command = "pimp",
    max = 2,
    salary = 20,
    admin = 0,
    level = 3,
    vote = false,
    hasLicense = false,
    candemote = true,
    mayorCanSetSalary = false,
    jobs_dealers = true
})

