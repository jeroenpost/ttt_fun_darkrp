
local category = "jobs_general"

TEAM_CITIZEN = DarkRP.createJob("Citizen", {
    color = Color(20, 150, 20, 255),
    model = {
        "models/player/Group01/Female_01.mdl",
        "models/player/Group01/Female_02.mdl",
        "models/player/Group01/Female_03.mdl",
        "models/player/Group01/Female_04.mdl",
        "models/player/Group01/Female_06.mdl",
        "models/player/group01/male_01.mdl",
        "models/player/Group01/Male_02.mdl",
        "models/player/Group01/male_03.mdl",
        "models/player/Group01/Male_04.mdl",
        "models/player/Group01/Male_05.mdl",
        "models/player/Group01/Male_06.mdl",
        "models/player/Group01/Male_07.mdl",
        "models/player/Group01/Male_08.mdl",
        "models/player/Group01/Male_09.mdl"
    },
    description = [[The Citizen is the most basic level of society you can hold besides being a hobo. You have no specific role in city life.]],
    weapons = {},
    command = "citizen",
    max = 0,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    jobs_general = true
})


TEAM_HOBO = DarkRP.createJob("Hobo", {
    color = Color(80, 45, 0, 255),
    model = "models/player/corpse1.mdl",
    description = [[The lowest member of society. Everybody laughs at you.
		You have no home.
		Beg for your food and money
		Sing for everyone who passes to get money
		Make your own wooden home somewhere in a corner or outside someone else's door]],
    weapons = {"weapon_bugbait"},
    command = "hobo",
    max = 5,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    hobo = true,
jobs_general = true
})

TEAM_HOBOKING = DarkRP.createJob("HoboKing", {
    color = Color(80, 45, 60, 255),
    model = "models/player/danboard.mdl",
    description = [[The King of the lowest member of society. Everybody laughs at you.
		You have no home.
		Beg for your food and money
		Sing for everyone who passes to get money
		Make your own wooden home somewhere in a corner or outside someone else's door]],
    weapons = {"weapon_bugbait", "angry_hobo"},
    command = "hoboking",
    max = 1,
    NeedToChangeFrom = TEAM_HOBO,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    hobo = true,
    level = 6,
    jobs_general = true
})

TEAM_SKELETON = DarkRP.createJob("Spooky Skeleton", {
    color = Color(80, 45, 220, 255),
    model = {"models/player/skeleton.mdl"},
    description = [[The scariest job in the world. People are scared of you.
		Hobo's are your enemy
		You have no home.
		Beg for your food and money
		Sing without voice for everyone who passes to get money
		Make your own wooden home somewhere in a corner or outside someone else's door]],
    weapons = {"weapon_bugbait", "skeleton_swep"},
    command = "spookyskeleton",
    max = 10,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    hobo = true,
    level = 3,
    jobs_general = true
})

TEAM_MEDIC = DarkRP.createJob("Medic", {
    color = Color(47, 79, 79, 255),
    model = {"models/player/kleiner.mdl",
        "models/player/Group03m/male_02.mdl",
        "models/player/Group03m/Female_02.mdl","models/player/Group03m/Female_04.mdl","models/player/Group03m/Male_01.mdl","models/player/Group03m/Male_04.mdl"},
    description = [[With your medical knowledge you work to restore players to full health.
	Without a medic, people cannot be healed.
	Left click with the Medical Kit to heal other players.
	Right click with the Medical Kit to heal yourself.]],
    weapons = {"med_kit"},
    command = "medic",
    max = 3,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    medic = true,
    jobs_general = true
})



TEAM_SECURITY= DarkRP.createJob("Security Guard", {
    color = Color(22, 80, 80, 255),
    model = {"models/player/barney.mdl","models/player/leet.mdl"},
    description = [[People will pay for protection, use your weapon or buy one from the local gundealer. You should stand guard for one player, like the hotelmanager, pizzaguy, bank, etc.]],
    weapons = { "weapon_cuff_police","stunstick", "weaponchecker", "weapon_real_cs_p228", "fists"},
    command = "securityguard",
    max = 3,
    salary = 55,
    admin = 0,
    level = 5,
    vote = false,
    hasLicense = true,
    jobs_general = true
})


TEAM_MINER = DarkRP.createJob("Miner", {
    color = Color(60, 0, 60, 255),
    model = {"models/player/blockdude.mdl",
        "models/conex/stickguy/stickguy.mdl",
        "models/player/group03/male_01.mdl",
        "models/player/Group03/Male_02.mdl",
        "models/player/Group03/male_03.mdl",
        "models/player/Group03/Male_04.mdl",
        "models/player/Group03/Male_05.mdl",
        "models/player/Group03/Male_06.mdl",
        "models/player/Group03/Male_07.mdl",
        "models/player/Group03/Male_08.mdl",
        "models/player/Group03/Male_09.mdl",
        "models/player/Group03/Female_01.mdl",
        "models/player/Group03/Female_02.mdl",
        "models/player/Group03/Female_03.mdl",
        "models/player/Group03/Female_04.mdl",
        "models/player/Group03/Female_06.mdl",
        "models/player/alyx.mdl"},
    description = [[You are a miner. Mine ore and sell it to the ORE NPC]],
    weapons = {"fists", "bronze_pickaxe",},
    command = "miner",
    max = 10,
    salary = 45,
    admin = 0,
    vote = false,
    hasLicense = false,
    level = 3,
    jobs_general = true
})

TEAM_FARMER = DarkRP.createJob("Farmer", {
    color = Color(69, 180, 88, 255),
    model = {
        "models/player/Group03/Male_04.mdl",
        "models/player/Group03/Male_05.mdl",
        "models/player/Group03/Male_06.mdl",
        "models/player/Group03/Male_07.mdl",
        "models/player/Group03/Male_08.mdl",
        "models/player/Group03/Male_09.mdl",
        "models/player/Group03/Female_01.mdl",
        "models/player/Group03/Female_02.mdl",
        "models/player/Group03/Female_03.mdl",
        "models/player/Group03/Female_04.mdl",
        "models/player/Group03/Female_06.mdl",
        "models/player/alyx.mdl"},
    description = [[You can grow plants and food and sell it.
		Find a nice piece of groundto grow your plants and earn
		lots of cash and XP selling your crobs]],
    weapons = {"fists", "hoe",},
    command = "farmer",
    max = 5,
    salary = 45,
    admin = 0,
    vote = false,
    hasLicense = false,
    level = 5,
    jobs_general = true
})

TEAM_POKEMONTRAINER = DarkRP.createJob("Pokemon Trainer", {
    color = Color(220, 30, 30, 255),
    model = {"models/pkmn trainer red/ash.mdl",
        "models/pkmn trainer red/bwa.mdl",
        "models/pkmn trainer red/dsh.mdl",
        "models/pkmn trainer red/gen.mdl",
        "models/pkmn trainer red/hsh.mdl",
        "models/pkmn trainer red/ptn.mdl",
        "models/pkmn trainer red/red.mdl",

    },
    description = [[As a Pokemon Trainer you can catch pokemon. Buy a PokeBall from the shop and hunt down Wild Pokemons. Once you have Pokemons, you can spawn them with your PokeSpawner]],
    weapons = {"pokespawn","fists"},
    command = "pokemontrainer",
    max = 5,
    salary = 35,
    admin = 0,
    vote = false,
    hasLicense = false,
    level = 5,
    PlayerLoadout = function(ply)  ply.ownedpokemon = {} return false end,
    jobs_general = true
})

TEAM_DRUGADDICT = DarkRP.createJob("Drug Addict", {
    color = Color(120, 90, 120, 255),
    model = {
        "models/conex/stickguy/stickguy.mdl",
        "models/player/group03/male_01.mdl",
        "models/player/Group03/Male_02.mdl",
        "models/player/Group03/male_03.mdl",
        "models/player/Group03/Male_04.mdl",
        "models/player/Group03/Male_05.mdl",
        "models/player/Group03/Male_06.mdl",
        "models/player/Group03/Male_07.mdl",
        "models/player/Group03/Male_08.mdl",
        "models/player/Group03/Male_09.mdl",
        "models/player/Group03/Female_01.mdl",
        "models/player/Group03/Female_02.mdl",
        "models/player/Group03/Female_03.mdl",
        "models/player/Group03/Female_04.mdl",
        "models/player/Group03/Female_06.mdl",
        "models/player/alyx.mdl"},
    description = [[You are addicted to drugs. Get drugs every 5 minutes and gain XP, or die.]],
    weapons = {"fists",},
    command = "drug_addict",
    max = 10,
    salary = 45,
    admin = 0,
    vote = false,
    hasLicense = false,
    level = 5,
    PlayerLoadout = function(ply)

        ply:SetNWInt("vampireHealth",120)
        timer.Create(ply:SteamID().."VampireTimer",3,0,function()
            local health = ply:GetNWInt("vampireHealth",120)
            if health > 0 then
                ply:SetNWInt("vampireHealth", health - 3)
            end
            if health <= 0 then health = 0 end
            if health == 10 then
                ply:PrintMessage( HUD_PRINTCENTER, "You need Drugs!" )
            end
            if health < 1 then
                ply:TakeDamage(10,ply,ply)
            end
        end)
        return false end,
    jobs_general = true
})

