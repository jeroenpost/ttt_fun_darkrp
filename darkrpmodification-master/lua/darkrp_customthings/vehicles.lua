/*---------------------------------------------------------------------------
DarkRP custom vehicles
---------------------------------------------------------------------------

This file contains your custom vehicles.
This file should also contain vehicles from DarkRP that you edited.

For examples and explanation please visit this wiki page:
http://wiki.darkrp.com/index.php/DarkRP:Vehicles


Add vehicles under the following line:
---------------------------------------------------------------------------*/

DarkRP.createVehicle({
    name = "sgmcrownviccvpi",
    model = "models/sentry/crownviccvpi.mdl",
    price = 750,
    allowed = {TEAM_CHIEF,TEAM_POLICE}, -- Optional
   -- customCheck = customcheck -- Optional
   label = "Police Interceptor", -- Optional, will use name if not set,
shop_vehicle = true,
shop = true
})

DarkRP.createVehicle({
    name = "07sgmcrownviccvpi",
    model = "models/sentry/07crownvic_cvpi.mdl",
    price = 1000,
    allowed = {TEAM_CHIEF,TEAM_POLICE}, -- Optional
    -- customCheck = customcheck -- Optional
    label = "Police Interceptor 2" -- Optional, will use name if not set
    ,shop_vehicle = true,
    shop = true
})


DarkRP.createVehicle({
    name = "sgmcrownvicuc",
    model = "models/sentry/crownvicuc.mdl",
    price = 1250,
    allowed = {TEAM_CHIEF,TEAM_POLICE}, -- Optional
    -- customCheck = customcheck -- Optional
    label = "Undercover Car" -- Optional, will use name if not set
    ,shop_vehicle = true,
    shop = true
})

DarkRP.createVehicle({
    name = "07sgmcrownvicuc",
    model = "models/sentry/07crownvic_uc.mdl",
    price = 1500,
    allowed = {TEAM_CHIEF,TEAM_POLICE}, -- Optional
    -- customCheck = customcheck -- Optional
    label = "Undercover Car 2" -- Optional, will use name if not set
    ,shop_vehicle = true,
    shop = true
})
DarkRP.createVehicle({
    name = "bustdm",
    model =  "models/tdmcars/bus.mdl",
    price = 500,
    allowed = {TEAM_BUSDRIVER}, -- Optional
    -- customCheck = customcheck -- Optional
    label = "City Bus" -- Optional, will use name if not set
    ,shop_vehicle = true,
    shop = true
})