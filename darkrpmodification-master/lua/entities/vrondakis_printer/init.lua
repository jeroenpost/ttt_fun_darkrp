// Modified from core DarkRP.
AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

ENT.SeizeReward = 0
ENT.StoredMoney = 0
ENT.StoredXP = 0

local PrintMore
function ENT:Initialize()


	self:SetModel(self.DarkRPItem.model)
	self:SetColor(self.DarkRPItem.vrondakisColor)


	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	local phys = self:GetPhysicsObject()
	phys:Wake()

	self.sparking = false
	self.damage = 100
	self.IsMoneyPrinter = true
	timer.Simple(self.DarkRPItem.vrondakisPrinterTime,function() PrintMore(self) end)

	if(LevelSystemConfiguration.PrinterSound) then
		self.sound = CreateSound(self, Sound("ambient/levels/labs/equipment_printer_loop1.wav"))
		self.sound:SetSoundLevel(52)
		self.sound:PlayEx(1, 100)
	end

	self:SetNWString('PrinterName', self.DarkRPItem.name)
	self:SetNWInt('MoneyPerPrint', self.DarkRPItem.vrondakisMoneyPerPrint)
	self:SetNWInt('MoneyAmount', 0)
	self:SetNWInt('MaxConfig',self.DarkRPItem.PrinterMaxP)

	self:SetUseType(SIMPLE_USE)

    self.SeizeReward = self.DarkRPItem.SeizeReward


end

function ENT:OnTakeDamage(dmg)
    if dmg:GetDamage() > 500 then
          self:BurstIntoFlames()
    end

	return
end

function ENT:Destruct()
	local vPoint = self:GetPos()
	local effectdata = EffectData()
	effectdata:SetStart(vPoint)
	effectdata:SetOrigin(vPoint)
	effectdata:SetScale(1)
	util.Effect("Explosion", effectdata)
	DarkRP.notify(self:Getowning_ent(), 1, 4, DarkRP.getPhrase("money_printer_exploded"))
end

function ENT:BurstIntoFlames()
	DarkRP.notify(self:Getowning_ent(), 0, 4, "Your printer is touched by a CP officer!")
	self.burningup = true
	local burntime = 0.1
	self:Ignite(burntime, 0)
	timer.Simple(burntime, function() if IsValid(self) then self:Fireball() end end)
end

function ENT:Fireball()
	if not self:IsOnFire() then self.burningup = false return end
	local dist = math.random(20, 280) -- Explosion radius
	self:Destruct()

	self:Remove()
end

PrintMore = function(ent)
	if not IsValid(ent) then return end

	ent.sparking = true
	timer.Simple(1, function()
		if not IsValid(ent) then return end
		ent:CreateMoneybag()
	end)
end

function ENT:CreateMoneybag()
	if not IsValid(self) or self:IsOnFire() then return end

	local MoneyPos = self:GetPos()

	if(self.DarkRPItem.PrinterMaxP == 0) or ((self.StoredMoney+self.DarkRPItem.vrondakisMoneyPerPrint) <= (self.DarkRPItem.vrondakisMoneyPerPrint*self.DarkRPItem.PrinterMaxP)) then

		local amount = self.DarkRPItem.vrondakisMoneyPerPrint
		local xpamount = self.DarkRPItem.vrondakisXPPerPrint


		self.StoredMoney = self.StoredMoney + amount
		self.StoredXP = self.StoredXP + xpamount
		self:SetNWInt('MoneyAmount', self.StoredMoney)

	elseif math.random(1, 4) == 3 then
        if self.vip and math.random(1, 9) == 3 then
            self:BurstIntoFlames()
        else
             self:BurstIntoFlames()
        end
    else
        self:SetColor(Color(200,255,200))
    end

	self.sparking = false
	timer.Simple(self.DarkRPItem.vrondakisPrinterTime,function() PrintMore(self) end)
end

function ENT:GetStoredXPHighUser( level )

        return self.StoredXP

end

function ENT:Use(activator,caller)
	if(IsValid(activator)) then
		if(activator:IsPlayer()) then
			if(self.StoredMoney>0) then

               if activator ~= self:Getowning_ent() then
                   DarkRP.notify(activator,0,4,'You cannot harvest from other peoples printers');
                   return
               end

            --   PrintTable(self.DarkRPItem)
            --   PrintTable(activator.ranks)

               if self.DarkRPItem.VIPOnly and istable(self.DarkRPItem.VIPOnly) then
                   local canbuy = false
                   if gb.compare_rank(activator, self.DarkRPItem.VIPOnly) then
                       canbuy = true
                   end
                   if not canbuy then
                       activator:PrintMessage( HUD_PRINTTALK, "You need to be "..self.DarkRPItem.vipName  )
                       activator:Send3DShopNotice("You need to have "..self.DarkRPItem.vipName.." to harvest this printer")
                       return  false,true
                   end
                end
					if(activator:getDarkRPVar('level')<(self.DarkRPItem.level)) then return DarkRP.notify(activator,1 ,4, 'You need to be a higher level to use this!') end
					if not(self.StoredMoney==0) then
						activator:addMoney(self.StoredMoney)
					end

                    self:GetStoredXPHighUser(activator:getLevel())
					if  (self.StoredXP > 0) then
                        if self:GetNWString('PrinterType') == "levelprinter" then
                            self.StoredXP = 1000 + (activator:getDarkRPVar('level') *self.StoredXP) -- 20 * level
                            activator:addXP(self.StoredXP,false)
                         else
						    activator:addXP(self.StoredXP,false)
                         end
					end
					self:SetNWInt('MoneyAmount', 0)
					DarkRP.notify(activator,0,4,'You got '..self.StoredXP..' XP and '..self.StoredMoney..'$ from this printer.')
					self.StoredMoney = 0
					self.StoredXP = 0

			end
			
		end
	end
end
ENT.NextOwnerCheck = 0
function ENT:Think()



    if self.NextOwnerCheck < CurTime() then
        self.NextOwnerCheck = CurTime() + 5
        if not IsValid(self:Getowning_ent()) or not self:Getowning_ent():IsPlayer() then
            self:Remove()
        end
        if self:WaterLevel() > 0 then
            self:Destruct()
            self:Remove()
            return
        end
    end


	if not self.sparking then return end

	local effectdata = EffectData()
	effectdata:SetOrigin(self:GetPos())
	effectdata:SetMagnitude(1)
	effectdata:SetScale(1)
	effectdata:SetRadius(2)
	util.Effect("Sparks", effectdata)



end

function ENT:OnRemove()
	if self.sound then
		self.sound:Stop()
	end
end
