AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/props_junk/wood_crate001a_damaged.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
--	self:SetMoveType(MOVETYPE_VPHYSICS)
--	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
    self:SetMaterial("camos/camo46")
	local phys = self:GetPhysicsObject()
	phys:Wake()
end

function ENT:Use(activator,caller)
	if !IsValid(caller) or self:GetCash() <= 0 then return end
	if(not table.HasValue( JOBS_criminals, caller:Team())) then
		caller:ChatPrint("You are not a criminal!")
		return
    end

	if self:GetCash() < 0 then return end
	caller:addMoney(self:GetCash())
    local xp = (self:GetCash()*1.5)
    caller:addXP( xp )
	caller:ChatPrint("You have earned $" .. self:GetCash() .. " and "..xp.." XP from selling stolen stuff!")
	self:SetCash(0)
end

function ENT:GetPrice(ent)
    local c = ent:GetClass()
    if c == "vrondakis_printer" then
        for k, v in pairs(DarkRPEntities) do
            if not ent.vrondakisType and ent.DarkRPItem then  ent.vrondakisType = ent.DarkRPItem.vrondakisType end

            if v.ent ~= c or not v.vrondakisType or not ent.vrondakisType  then continue end
            if  v.vrondakisType == ent.vrondakisType then
                print( ent.vrondakisType.." "..v.vrondakisType)
                print( v.name )
                return v.price
            end
        end
        return false
    end

    for k, v in pairs(DarkRPEntities) do
        if v.ent ~= c then continue end
        return v.price
    end
    return false
end

function ENT:StartTouch(ent)
    if not IsValid(ent) or ent:IsWorld() or ent:IsPlayer() then return end
    local price = self:GetPrice(ent)
    if not price then self:EmitSound("items/medshotno1.wav") return end

		self:SetCash(self:GetCash() + price * 0.5)
		ent.Sellable = false
		ent:Remove()
    self:EmitSound("items/suitchargeok1.wav")

end

function ENT:OnRemove()
	--timer.Simple(1, SaveFarmingPositions)
end

function ENT:OnPhysgunFreeze(weapon, phys, ent, ply)
	if(ply:IsSuperAdmin()) then
	--	SaveFarmingPositions()
	end
end

function ENT:PhysgunPickup(ply, ent)
	return false --ply:IsSuperAdmin()
end

        function ENT:GetSellAbleItems()
            self.sellableitems = {}
            for k, v in pairs(DarkRPEntities) do
                self.sellableitems[v.ent] = true
            end
        end

        function ENT:Think()
            self:NextThink(CurTime() + 3)
            if not self.sellableitems then
                self:GetSellAbleItems()
            end

            local orgin_ents = ents.FindInSphere(self:GetPos(),25)

            for k,v in pairs( orgin_ents ) do

                if self.sellableitems[v:GetClass()] and not v:IsWorld() and not v:IsPlayer() then
                    self:StartTouch(v)
                end
            end
            return true
        end

