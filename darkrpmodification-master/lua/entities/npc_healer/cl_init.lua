include("shared.lua")

function ENT:Initialize()
end


hook.Add("PostDrawOpaqueRenderables", "gb_stolenstuffbox", function()
    for _, ent in pairs (ents.FindByClass("npc_healer")) do
        if ent:GetPos():Distance(LocalPlayer():GetPos()) < 500 then
            local Pos = ent:GetPos()
            local Ang = ent:GetAngles()
            Ang:RotateAroundAxis(Ang:Forward(), 90)
            cam.Start3D2D(Pos + Ang:Up() * 20.19, Ang, 0.14)
            draw.RoundedBox( 0, -130, -40, 260, 80, Color(0,0,0,255) )
            draw.SimpleText("Drop stolen stuff here...", "DermaLarge",0 , -20, Color(255,255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
            draw.SimpleText("$" .. ent:GetCash().." | "..(ent:GetCash()*1.5).." XP", "DermaLarge",0 , 20, Color(255,255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
            cam.End3D2D()
        end
    end
end)
