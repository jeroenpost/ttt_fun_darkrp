AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")


function ENT:Initialize()
	self:SetModel("models/items/cs_gift.mdl")
    if math.random(1,2) > 1 then
     self:SetMaterial("camos/custom_camo23")
    end
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
    self.presenttype = "xp"
	local phys = self:GetPhysicsObject()
	phys:Wake()
end

ENT.gavexp = false
function ENT:Use(ply)


    if ply:Team() == TEAM_SANTA or self.Owner == ply then
        self:printmessage(ply, "You can't pick up your own presents!")
        return
    end

    if IsValid(self.Owner) and not self.gavexp then
        self.gavexp = true
        self.Owner:addXP(500 + 5 * self.Owner:getDarkRPVar('level'))
    end

    if self.presenttype == "boom" then
        self:printmessage(ply, "You received a BOOM from Santa!")
        self:Explode()
        self:Remove()
    end
    if self.presenttype == "armor" then
        self:printmessage(ply, "You received 50 armor from Santa!")
        if ply:Armor() < 100 then
             ply:SetArmor( ply:Armor() + 50)
             self:Remove()
        end
        self:Remove()
    end
    if self.presenttype == "money" then
        self:printmessage(ply, "You received $5000 from Santa!")
            ply:addMoney(5000)
            self:Remove()
    end
    if self.presenttype == "weapon_ak" then
        self:printmessage(ply, "You received an AK47 from Santa!")
        ply:Give("fas2_ak47")
        ply:SelectWeapon("fas2_ak47")
        self:Remove()
    end
    if self.presenttype == "weapon_elites" then
        self:printmessage(ply, "You received Elites from Santa!")
        ply:Give("weapon_real_cs_elites")
        ply:SelectWeapon("weapon_real_cs_elites")
        self:Remove()
    end
    if self.presenttype == "xp" then
        local xp = 50 * ply:getDarkRPVar('level')
        self:printmessage(ply, "You received "..xp.."XP from Santa!")
        ply:addXP(xp)
        self:Remove()
    end
    if self.presenttype == "mushroom" then
        ply:SetFunVar("durgz_mushroom_high_start", CurTime())
        ply:SetFunVar("durgz_mushroom_high_end", CurTime() + 130)
        ply:SetGravity(0.135);
        ply.ishigh = true
        timer.Create("mushroom_end"..ply:SteamID(),50,2,function()
            if not IsValid(ply) then return end
            --if( activator:GetFunVar("durgz_mushroom_high_end") - 0.5 < CurTime() && activator:GetFunVar("durgz_mushroom_high_end") > CurTime() )then
            ply:SetGravity(1)
            -- end
        end)
        self:printmessage(ply, "You received a mushroom from Santa!")
        self:Remove()
    end
    if self.presenttype == "vip" then
        if gb.compare_rank( ply, {"vippp"}) then
            self:printmessage(ply, "You already have VIP++")
            return
        end

        table.insert(ply.ranks,"vippp")
        self:printmessage(ply, "You received temporary (until reconnect) VIP++ from Santa!")
        self:Remove()
    end
end

function ENT:printmessage(ply,message)
    ply:PrintMessage( HUD_PRINTCENTER, message )
    ply:ChatPrint(message)
end




function ENT:Explode()
    if IsValid(self.Owner) then
        for k,v in pairs(ents.FindInSphere(self:GetPos(),125)) do
            if v:IsPlayer() then
                v:TakeDamage( 30, self.Owner )
            end
        end

        local ent = ents.Create( "env_explosion" )

        ent:SetPos( self:GetPos()  )

            ent:SetOwner( self.Owner  )
            ent:SetPhysicsAttacker(  self.Owner )
        ent:Spawn()
        ent:SetKeyValue( "iMagnitude", 1 )
        ent:Fire( "Explode", 1, 1 )
    end



end
