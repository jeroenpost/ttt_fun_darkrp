AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include( 'shared.lua' )

function ENT:Initialize( )
	self:SetModel("models/props_phx/gears/rack18.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
end

function ENT:OnTakeDamage( dmg ) 
	return false
end

function ENT:AcceptInput( name, activator, caller )
    if ( name == "Use" && activator:IsPlayer( ) && IsValid(self.TrapOwner) ) then
		GAMEMODE:Notify(activator, 0, 4, "This trap belongs to: "..self.TrapOwner:Nick())
	end
end

function ENT:StartTouch( ent )
	if(ent:IsVehicle()) then
		if(ent:GetDriver():IsValid()) then
			ent:EmitSound("physics/rubber/rubber_tire_impact_hard3.wav")
            ent.VC_Health = 0
			ent:GetDriver():ExitVehicle()
			ent:Fire("TurnOff")
            ent:SetVelocity( Vector( 0, 0, 900 ) )
            self:Remove()
		end
	end
end

