--[[
	Author: Koz
	Steam: http://steamcommunity.com/id/drunkenkoz
	Contact: mybbkoz@gmail.com

	License:
	You are free to use this software however you like; however,
	you cannot redistribute this code in any way without consent
	from the original author, Koz.
]]--

ENT.Type = "anim"
ENT.Base = "food_base"

ENT.PrintName = "Soda"
ENT.Author = "GreenBlack"
ENT.Category = "GreenBlack"

ENT.Spawnable			= true
ENT.AdminOnly			= true