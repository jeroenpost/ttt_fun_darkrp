--[[
	Author: Koz
	Steam: http://steamcommunity.com/id/drunkenkoz
	Contact: mybbkoz@gmail.com

	License:
	You are free to use this software however you like; however,
	you cannot redistribute this code in any way without consent
	from the original author, Koz.
]]--

AddCSLuaFile( "shared.lua" )
include( "shared.lua" )

function ENT:Initialize()
	self:SetModel("models/props_junk/PopCan01a.mdl")
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetUseType( SIMPLE_USE )
	self.health = 10
	
	local phys = self:GetPhysicsObject()
	if phys:IsValid() then
		phys:Wake()
	end
end

function ENT:OnTakeDamage(dmg)
	self.health = self.health - dmg:GetDamage()
	if ( self.health <= 0 ) then
		self:Remove()
	end
end

function ENT:Use( activator )
	timer.Simple( 0, function()
		activator:EmitSound( "food/slurp.mp3", 50, 100 )
	end )

	if foodmod.mode == 2 then
		activator:setSelfDarkRPVar( "Energy", math.Clamp( ( activator:getDarkRPVar("Energy") or 100 ) + 25, 0, 100 ) )
	end
	
	if foodmod.mode == 1  then
		activator:SetHealth( math.Clamp( ( activator:Health() or 100 ) + 25, 0, 100) )
        activator:SetArmor( math.Clamp( ( activator:Armor() or 100 ) + 2, 0, 100) )
        gb_check_health_and_armor()
	end
	
	self:Remove()
	
	timer.Simple( 3, function()
		activator:EmitSound( "food/burp.mp3", 50, 100 )
	end )
end