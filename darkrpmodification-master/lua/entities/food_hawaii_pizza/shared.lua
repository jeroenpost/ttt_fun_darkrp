ENT.Type = "anim"
ENT.Base = "food_base"

ENT.Category = "GreenBlack"
ENT.PrintName = "Hawaii Pizza"
ENT.Author = "GreenBlack"
ENT.Purpose = "Eating"
ENT.Instructions = "Delicious pie of Hawaii Pizza!"

ENT.Spawnable			= true
ENT.AdminSpawnable		= true