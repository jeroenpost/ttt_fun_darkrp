AddCSLuaFile( "cl_init.lua" ) 
AddCSLuaFile( "shared.lua" ) 

include( "shared.lua" ) 
function SpawnArcadeMachine()
	if not file.IsDir("craphead_scripts", "DATA") then
		file.CreateDir("craphead_scripts", "DATA")
	end
	
	if not file.IsDir("craphead_scripts/arcade_machine/".. string.lower(game.GetMap()) .."", "DATA") then
		file.CreateDir("craphead_scripts/arcade_machine/".. string.lower(game.GetMap()) .."", "DATA")
    end

    local locations = {}
    locations['rp_downtown_v4c_v3_chirax'] = {}
    locations['rp_downtown_v4c_v3_chirax'][1] = "-3269.968750;-1917.788452;-195.968750;0.000;0.651;0.000;http://www.classicgamesarcade.com/games/defender.swf;Cosmic Defender;50;0;"
    locations['rp_downtown_v4c_v3_chirax'][2] = "-3269.979248;-1795.206543;-195.968750;0.000;-0.791;0.000;http://www.ponger.org/fullscreen/galaxians.swf;Galaxian;50;2;"
    locations['rp_downtown_v4c_v3_chirax'][3] = "-3269.968750;-1680.834229;-195.968750;0.000;-0.966;0.000;http://www.classicgamesarcade.com/games/pacman.swf;Pacman;50;3;"
    locations['rp_downtown_v4c_v3_chirax'][4] = "-3269.968750;-1566.871582;-195.968750;0.000;-0.520;0.000;http://www.classicgamesarcade.com/games/doom.swf;DOOM;50;0;models/greenblack/arcade_doom"
    locations['rp_downtown_v4c_v3_chirax'][5] = "-3266.324219;-1435.928833;-195.968750;0.000;2.741;0.000;http://www.pizn.com/swf/1-space-invaders.swf;Space Invaders;50;1;"

    locations['rp_downtown_v4c_v2'] = {}
    locations['rp_downtown_v4c_v2'][1] = "-2320;600;-185;0;0;0.000;http://www.classicgamesarcade.com/games/defender.swf;Cosmic Defender;50;0;"
    locations['rp_downtown_v4c_v2'][2] = "-2320;550;-185;0;0;0;http://www.ponger.org/fullscreen/galaxians.swf;Galaxian;50;2;"
    locations['rp_downtown_v4c_v2'][3] = "-2320;500;-185;0;0;0;http://www.classicgamesarcade.com/games/pacman.swf;Pacman;50;3;"
    locations['rp_downtown_v4c_v2'][4] = "-2320;450;-185;0;0;0;http://www.classicgamesarcade.com/games/doom.swf;DOOM;50;0;models/greenblack/arcade_doom"
    locations['rp_downtown_v4c_v2'][5] = "-2320;650;-185;0;0;0;http://www.pizn.com/swf/1-space-invaders.swf;Space Invaders;50;1;"


    if not locations[string.lower(game.GetMap())] then return end

    for k, v in pairs(locations[string.lower(game.GetMap())]) do
		local ArcadeFile = v
	  
		local TheArcade = string.Explode( ";", ArcadeFile )
		
		local TheVector = Vector(TheArcade[1], TheArcade[2], TheArcade[3])
		local TheAngle = Angle(tonumber(TheArcade[4]), TheArcade[5], TheArcade[6])
		
		local Arcade = ents.Create("arcade_machine")
		Arcade:SetModel("models/johny-srka/arcade_machine.mdl")
		Arcade:SetPos(TheVector)
		Arcade:SetAngles(TheAngle)
		
		Arcade.Game = TheArcade[7]
		Arcade.Name = TheArcade[8]
		Arcade.Price = TheArcade[9]
        Arcade:SetSkin(TheArcade[10] or 1)
        Arcade:SetMaterial(TheArcade[11] or "")

        Arcade:SetNWInt("ArcadePrice", Arcade.Price)
        Arcade:SetNWString("ArcadeName", Arcade.Name)
		
		Arcade:Spawn()
		Arcade:SetMoveType(MOVETYPE_NONE)
		Arcade:SetSolid( SOLID_BBOX )
		Arcade:SetCollisionGroup(COLLISION_GROUP_PLAYER)

	end
end
timer.Simple(1, SpawnArcadeMachine)

function CH_Arcade_Position( ply, cmd, args )
	if ply:IsSuperAdmin() then
		local FileName = args[1]
		local HTML = args[2]
		local Gamename = args[3]
		local Price = args[4] or 0
        local HisSkin = args[5] or 0
        local HisMat = args[6] or ""
		
		if not FileName then
			ply:ChatPrint("Please choose a UNIQUE name for the arcade!") 
			return
		end
		if not HTML then
			ply:ChatPrint("Please type in the HTML code for the arcade!") 
			return
		end
		if not Gamename then
			ply:ChatPrint("Please choose a name for the arcade game!") 
			return
		end
		
		print(FileName)
		print(HTML)
		print(Gamename)
		print(Price)
		
		if file.Exists( "craphead_scripts/arcade_machine/".. string.lower(game.GetMap()) .."/arcade_location_".. FileName ..".txt", "DATA" ) then 
			ply:ChatPrint("This file name is already in use. Please choose another one or remove this one by typing 'arcade_remove "..FileName.."' in console.")
			return
		end
		
		local HisVector = string.Explode(" ", tostring(ply:GetPos()))
		local HisAngles = string.Explode(" ", tostring(ply:GetAngles()))
		
		file.Write("craphead_scripts/arcade_machine/".. string.lower(game.GetMap()) .."/arcade_location_".. FileName ..".txt", ""..(HisVector[1])..";"..(HisVector[2])..";"..(HisVector[3])..";"..(HisAngles[1])..";"..(HisAngles[2])..";"..(HisAngles[3])..";"..HTML..";"..Gamename..";"..Price..";"..HisSkin..";"..HisMat, "DATA")
		ply:ChatPrint("New position for the arcade has been succesfully set. Please restart your server!")
	else
		ply:ChatPrint("Only administrators can perform this action")
	end
end
--concommand.Add("arcade_setpos", CH_Arcade_Position)

function CH_Arcade_Position_Remove( ply, cmd, args )
	if ply:IsAdmin() then
		local FileName = args[1]
		
		if not FileName then
			ply:ChatPrint("Please enter a filename!") 
			return
		end
		
		if file.Exists( "craphead_scripts/arcade_machine/".. string.lower(game.GetMap()) .."/arcade_location_".. FileName ..".txt", "DATA" ) then
			file.Delete( "craphead_scripts/arcade_machine/".. string.lower(game.GetMap()) .."/arcade_location_".. FileName ..".txt" )
			ply:ChatPrint("The selected arcade has been removed. Please restart your server in order for the updates to take place!")
			return
		end
		
	else
		ply:ChatPrint("Only administrators can perform this action")
	end
end
--concommand.Add("arcade_remove", CH_Arcade_Position_Remove)

function ENT:AcceptInput(ply, caller)
	if caller:IsPlayer() && !caller.CantUse then--&& !caller.MenuOpen then
	
		if caller:getDarkRPVar("money") < tonumber(self.Price) then
            DarkRP.notify(caller, 1, 5, "It costs $"..self.Price.." to play on this arcade game!")
			return
		end
		
		if tonumber(self.Price) > 0 then
			caller:addMoney( self.Price * -1 )
            DarkRP.notify(caller, 1, 5, "You've paid $"..self.Price.." to play on this arcade game!")
            hook.Call("arcadegamer",GAMEMODE,caller)
		end
		
		caller.CantUse = true
		caller.MenuOpen = true
		timer.Simple(3, function()  caller.CantUse = false end)
		
		if caller:IsValid() then
			umsg.Start("CH_ArcadeMenu", caller)
				umsg.String(self.Name)
				umsg.String(self.Game)
			umsg.End()
		end
	end
end

util.AddNetworkString("CH_CloseArcadeMenu")
net.Receive("CH_CloseArcadeMenu", function(length, ply)
	ply.MenuOpen = false
end)
--[[
-- Resources
resource.AddFile("models/Arcade-Nacgubes/Mortal-Kombat-2.dx80")
resource.AddFile("models/Arcade-Nacgubes/Mortal-Kombat-2.dx90")
resource.AddFile("models/Arcade-Nacgubes/Mortal-Kombat-2.mdl")
resource.AddFile("models/Arcade-Nacgubes/Mortal-Kombat-2.phy")
resource.AddFile("models/Arcade-Nacgubes/Mortal-Kombat-2.sw")
resource.AddFile("models/Arcade-Nacgubes/Mortal-Kombat-2.vvd")

resource.AddFile("materials/Arcade Machines/Shared/Special/Screen-Reflect.vtf")
resource.AddFile("materials/Arcade Machines/Shared/Special/Screen-Reflect.vmf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/Special/BumpyPlastic-NormalMap+Phong.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/Special/CoinSlots-NormalMap+Reflect.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/Special/ControlPad-Reflect.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/Special/SpeakerScrewHead-NormalMap.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/Artwork-Marquee-Lower.vmf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/Artwork-Marquee-Lower.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/Artwork-Marquee-Upper.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/Artwork-Marquee-Upper.vmf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/ArtWork-Side.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/ArtWork-Side.vmf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/BlackWood.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/BlackWood.vmf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/BumpyPlastic.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/BumpyPlastic.vmf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/CoinSlots.vmf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/CoinSlots+SelfIllum.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/ControlPad.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/ControlPad.vmf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/Joystick&Buttons+Phong.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/Joystick&Buttons+Phong.vmf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/MidwayLogo.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/MidwayLogo.vmf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/ScreenShots-Animated.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/ScreenShots-Animated.vmf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/SpeakerGrate.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/SpeakerGrate.vmf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/SpeakerScrewHead+Translucent.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/SpeakerScrewHead+Translucent.vmf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/SpeakerShadow+Translucent.vtf")
resource.AddFile("materials/Arcade Machines/Mortal Kombat 2/SpeakerShadow+Translucent.vmf")

]]--