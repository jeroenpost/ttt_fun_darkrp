include("shared.lua")

surface.CreateFont("ArcadeDefault", {
    font = "Tahoma",
    size = 14,
    weight = 600
})

surface.CreateFont("ArcadeSmall", {
    font = "Tahoma",
    size = 13,
    weight = 600
})

surface.CreateFont("Arcade20", {
    font = "Trebuchet MS",
    size = 20,
    weight = 900
})





function ENT:DrawTranslucent()
	self:Draw()
end

function ArcadeMenu( um )
local Gamename = um:ReadString()
local Game = um:ReadString()

local ArcadeFrame = vgui.Create( "DFrame" )
ArcadeFrame:SetSize( 825, 645 )
ArcadeFrame:Center()
ArcadeFrame:SetTitle( "" ) 
ArcadeFrame:SetDraggable( true ) 
ArcadeFrame:ShowCloseButton( false )
ArcadeFrame:MakePopup()
ArcadeFrame.Paint = function(CHPaint)
	-- Draw the menu background color.		
	draw.RoundedBox( 0, 0, 25, CHPaint:GetWide(), CHPaint:GetTall(), Color( 255, 255, 255, 150 ) )

	-- Draw the outline of the menu.
	surface.SetDrawColor(0,0,0,255)
	surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), CHPaint:GetTall())
	 
	draw.RoundedBox( 0, 0, 0, CHPaint:GetWide(), 25, Color( 255, 255, 255, 200 ) )
		
	surface.SetDrawColor(0,0,0,255)
	surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), 25)
end

local MenuTitle = vgui.Create( "DLabel", ArcadeFrame )
MenuTitle:SetText( "Arcade Game: "..Gamename )
MenuTitle:SetPos( 10, 5  )
MenuTitle:SetFont( "ArcadeDefault" )
MenuTitle:SetTextColor( Color( 70,70,70,255 ) )
MenuTitle:SizeToContents()

local GUI_Main_Exit = vgui.Create("DButton", ArcadeFrame)
GUI_Main_Exit:SetSize(16,16)
GUI_Main_Exit:SetPos(805,5)
GUI_Main_Exit:SetText("")
GUI_Main_Exit.Paint = function()
	surface.SetMaterial(Material("icon16/cross.png"))
	surface.SetDrawColor(200,200,0,200)
	surface.DrawTexturedRect(0,0,GUI_Main_Exit:GetWide(),GUI_Main_Exit:GetTall())
end
GUI_Main_Exit.DoClick = function()
	ArcadeFrame:Remove()
	net.Start("CH_CloseArcadeMenu")
	net.SendToServer()
end

local TheGame = vgui.Create( "HTML", ArcadeFrame )
TheGame:SetPos( 5, 25 )
TheGame:SetSize( 820, 650 )
TheGame:SetHTML([[<embed src=']].. Game ..[[' type='application/x-shockwave-flash' width='800' height='600'></embed>]])
end
usermessage.Hook("CH_ArcadeMenu", ArcadeMenu)


function ENT:Draw()
    self.Entity:DrawModel()

end

hook.Add("PostDrawOpaqueRenderables", "Arcade 2d3d", function()
    for _, ent in pairs (ents.FindByClass("arcade_machine")) do
        if ent:GetPos():Distance(LocalPlayer():GetPos()) < 400 then
            local pos = ent:GetPos()+ent:GetUp()*90
            local ang = ent:GetAngles()

            ang:RotateAroundAxis( ang:Forward(), 90)
            ang:RotateAroundAxis( ang:Right(), -90)

            cam.Start3D2D(pos, ang, 0.35)

                draw.SimpleTextOutlined(ent:GetNWString("ArcadeName"), "Arcade20", 0, 0, Color(60, 60, 60, 255), 1, 1, 1.5, Color(0, 0, 0, 255))
            --draw.SimpleTextOutlined( 'Car Dealer', "HUDNumber5", 0, 0, Color( 0, 150, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP, 1, Color(0, 0, 0, 255))

            local price = ent:GetNWInt("ArcadePrice")

               -- if price > 0 then
                    draw.SimpleTextOutlined("$"..price, "ArcadeSmall", 0, 25, Color(60, 100, 0, 200), 1, 1, 1.5, Color(0, 0, 0, 255))
             --   else
               --     draw.SimpleTextOutlined("Free", "ArcadeSmall", 0, -90, Color(60, 100, 0, 200), 1, 1, 1.5, Color(0, 0, 0, 255))
              --  end


            cam.End3D2D()
        end
    end
end)