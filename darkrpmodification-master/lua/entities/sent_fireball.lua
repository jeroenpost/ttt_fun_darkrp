
ENT.Type 			= "anim"
ENT.PrintName			= "Fireball"
ENT.Author			= "buu342"

ENT.Spawnable			= false
ENT.AdminSpawnable		= false


function ENT:SpawnFunction( ply, tr )

    if ( !tr.Hit ) then return end

    local SpawnPos = tr.HitPos + tr.HitNormal * 16

    local ent = ents.Create( "sent_fireball" )
    ent:SetPos( SpawnPos )
    ent:SetOwner( ply )
    ent:Spawn()
    self.Entity:Ignite(100, 100)
    ent:Activate()
    self.Entity:SetMaterial( "models/props_lab/Tank_Glass001" )
    ent:SetColor(100, 100, 100, 255)
    return ent

end


function ENT:Initialize()
    self.Entity:Ignite(100, 100)
    self.Entity:SetModel( "models/dav0r/hoverball.mdl" )
    self.Entity:SetMaterial( "models/props_lab/Tank_Glass001" )

    //self.Entity:PhysicsInitSphere( 16, "boulder" )
    self:PhysicsInitSphere( 16, "metal_bouncy")


    local phys = self.Entity:GetPhysicsObject()
    self.Counter = 0
            // Set collision bounds exactly
    self.Entity:SetCollisionBounds( Vector( -16, -16, -16), Vector( 16, 16, 16 ) )
    self.Entity:SetColor(100, 100, 100, 255)
    local phys = self:GetPhysicsObject()
    if (phys:IsValid()) then
        phys:Wake()
    end
    //self.Activated = false
    self.Act = false
end



function ENT:PhysicsCollide( data, physobj )
    if data.HitEntity:IsWorld() then
        // Play sound on bounce
        if (data.Speed > 80 && data.DeltaTime > 0.2 ) then
        self.Entity:EmitSound( "Boulder.ImpactHard" )
        end

        -- Bounce like a crazy bitch
        local LastSpeed = math.max( data.OurOldVelocity:Length(), data.Speed )
        local NewVelocity = physobj:GetVelocity()
        NewVelocity:Normalize()
        self.Counter = self.Counter + 1
        LastSpeed = math.max( NewVelocity:Length(), LastSpeed )

        local TargetVelocity = NewVelocity * LastSpeed * 0.9

        physobj:SetVelocity( TargetVelocity )

    elseif data.HitEntity:IsPlayer() or data.HitEntity:IsNPC() then
        for k, v in pairs(ents.FindInSphere(self.Entity:GetPos(), 50)) do
            if v:IsPlayer() or v:GetClass() == "prop_physics" or v:GetClass() == "prop_ragdoll" or
                    v:IsNPC() then


                if IsValid(self.Owner) and self.Owner != v then
                v:TakeDamage(30, self.Owner, self.Owner:GetActiveWeapon())
                v:Ignite(2, 10)
                else
                    v:TakeDamage(20, self, self)
                    v:Ignite(2, 10)
                end
                self.Activated = true
            end
        end
        if self.Activated then
            self.Entity:Remove()
        end
    end
end

function ENT:OnTakeDamage( dmginfo )


    self.Entity:TakePhysicsDamage( dmginfo )

end


function ENT:Think()

    if self.Counter >= 3 then
        self.Entity:Remove()
        for k, v in pairs(ents.FindInSphere(self.Entity:GetPos(), 50)) do
            if v:IsPlayer() or v:GetClass() == "prop_physics" or v:GetClass() == "prop_ragdoll" or v:IsNPC() then
                --v:Ignite(10, 100)
                --if IsValid(self.Owner) then
                --v:TakeDamage(30, self.Owner, self.Owner:GetActiveWeapon())
                --end

            end
        end


    end
end



