AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

--[[------------------------------------------------------------------------------------

	THINGS YOU CAN EDIT

--]]------------------------------------------------------------------------------------
local recharge_time = 300 //Time between each recharge after using the dumpster
local prop_delete_time = 25 //Time of how long the props that shoot out will last
local minimum_amount_items = 2 //Least amount of items that can come from dumpster
local maximum_amount_items = 4 //Max amount of items that can come from dumpster
local open_sound = "physics/metal/metal_solid_strain5.wav" //Sound played when the dumpster is used

--Keep all of these numbers whole numbers between 1-100
local prop_percentage = 100 //When set to 100, if no entities or weapons were created, then a prop will be 
local ent_percentage = 25
local weapon_percentage = 10
--Keep all of these numbers whole numbers between 1-100



local Dumpster_Items = {
	Props = { --Add/Change models of props
		"models/props_c17/FurnitureShelf001b.mdl",
		"models/props_c17/FurnitureDrawer001a_Chunk02.mdl",
		"models/props_interiors/refrigeratorDoor02a.mdl",
		"models/props_lab/lockerdoorleft.mdl",
		"models/props_wasteland/prison_lamp001c.mdl",
		"models/props_wasteland/prison_shelf002a.mdl",
		"models/props_vehicles/tire001c_car.mdl",
		"models/props_trainstation/traincar_rack001.mdl",
		"models/props_interiors/SinkKitchen01a.mdl",
		"models/props_c17/lampShade001a.mdl", 
		"models/props_junk/PlasticCrate01a.mdl",
		"models/props_c17/metalladder002b.mdl",
		"models/Gibs/HGIBS.mdl",
		"models/props_c17/metalPot001a.mdl",
		"models/props_c17/streetsign002b.mdl",
		"models/props_c17/pottery06a.mdl",
		"models/props_combine/breenbust.mdl",
		"models/props_lab/partsbin01.mdl",
		"models/props_trainstation/payphone_reciever001a.mdl",
		"models/props_vehicles/carparts_door01a.mdl",
		"models/props_vehicles/carparts_axel01a.mdl"
	},
	
	Ents =  { --Change these to entites you want
		"lockpick",
		"keypad_cracker",
        "spring",
        "wood",
        "wrench",
        "ironbar",
        "dnascanner",
        "durgz_lsd",
        "durgz_weed",
        "durgz_water",
        "durgz_cigarette",
        "durgz_alcohol",
        "durgz_aspirin",
	},
	
	Weapons = {
		"weapon_real_cs_knife",
		"weapon_real_cs_p228",
		"weapon_real_cs_five-seven",
		"weapon_real_cs_flash",
		"weapon_real_cs_smoke",
	}
}

local Spawn_Positions = { //The locations/angles of each dumpster
	["rp_downtown_evilmelon_v1"] = {
		{Vector(2922.516846, -2890.843994, 97.614372), Angle(0,0,0)},
		{Vector(5541.050293, -1697.577881, 97.615143), Angle(0,180,0)},
		{Vector(4571.008789, -3546.913818, 97.582504), Angle(0,-90,0)},
		{Vector(3685.531738, -2714.948730, -638.518066), Angle(0,180,0)},
		{Vector(1413.322876, 1508.890015, 89.552284), Angle(0,-90,0)},
		{Vector(1370.521851, -3125.158691, 90.464058), Angle(0,90,0)}
	},
	["rp_bangclaw"] = {
		{Vector(2922.516846, -2890.843994, 97.614372), Angle(0,0,0)},
		{Vector(5541.050293, -1697.577881, 97.615143), Angle(0,180,0)},
		{Vector(4571.008789, -3546.913818, 97.582504), Angle(0,-90,0)},
		{Vector(3685.531738, -2714.948730, -638.518066), Angle(0,180,0)},
		{Vector(1413.322876, 1508.890015, 89.552284), Angle(0,-90,0)},
		{Vector(1370.521851, -3125.158691, 90.464058), Angle(0,90,0)}
	}
}
--[[------------------------------------------------------------------------------------

	THINGS YOU CAN EDIT

--]]------------------------------------------------------------------------------------

function ENT:Initialize()
	self:SetModel("models/props_junk/TrashDumpster01a.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_NONE)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
    self:SetFunVar("timer"..self:EntIndex(),0)
    timer.Create("dumpster"..self:EntIndex(),3,0,function()
        if not IsValid(self) then return end
        local t =  self:GetFunVar("timer"..self:EntIndex())
        if t > 0 then
            self:SetFunVar("timer"..self:EntIndex(),t-1)
        end
    end)
end

function ENT:OnTakeDamage(dmginfo)
	return
end

function ENT:EmitItems(searcher)
	self:EmitSound(open_sound, 300, 100)
	local pos = self:GetPos() + Vector(0, 0, 50)
	
	for i = 1, math.random(minimum_amount_items,maximum_amount_items) do
		if math.random(1, 100) <= weapon_percentage then
			local ent = ents.Create(table.Random(Dumpster_Items["Weapons"]))
			ent:SetPos(pos)
			ent:Spawn()
		elseif math.random(1, 100) <= ent_percentage then
            local em = table.Random(Dumpster_Items["Ents"])
			local ent = ents.Create(em)
			ent:SetPos(pos)
			ent:Spawn()
		elseif math.random(1, 100) <= prop_percentage then
			local prop = ents.Create("prop_physics_multiplayer")
			prop:SetModel(table.Random(Dumpster_Items["Props"]))
			prop:SetPos(pos)
			prop:Spawn()
			
			timer.Simple(prop_delete_time, function() -- Remove the prop after x seconds
				if prop:IsValid() then
					prop:Remove()
				end
			end)
		end
	end
end



function ENT:Use(activator)

	if self:GetFunVar("timer"..self:EntIndex()) > 0 then
        activator:SetFunVar("timer"..self:EntIndex(), self:GetFunVar("timer"..self:EntIndex()))
        timer.Create("dumpster_player"..activator:EntIndex(),1,300,function()
            if IsValid(self) and IsValid(activator) then
                local t =  self:GetFunVar("timer"..self:EntIndex())
                if t > 0 then
                    activator:SetFunVar("timer"..self:EntIndex(),t-1)
                end
            end
        end)
        return
    end
	
	if table.HasValue({TEAM_HOBO,TEAM_HOBOKING,TEAM_SKELETON}, activator:Team()) then
		self:SetFunVar("timer"..self:EntIndex(), 300)
		self:EmitItems(activator)

	end
end

function ENT:Think()

end

