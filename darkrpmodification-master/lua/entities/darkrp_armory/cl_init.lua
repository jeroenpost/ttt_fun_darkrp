include("shared.lua")

surface.CreateFont("Arial Black45", {font="Arial Black", size=45, antialias = true})
surface.CreateFont("Arial Black45_shady", {font="Arial Black", blursize = 30, scanlines = 15, size=45, antialias = true})

function ENT:Draw()
	self.Entity:DrawModel()
    if self.Entity:GetPos():Distance(LocalPlayer():GetPos()) < 800 then
        self.Entity:SetColor(Color(25, 25, 25, 255))

        local angle = self:GetAngles()
        angle:RotateAroundAxis(angle:Forward(), 90)
        angle:RotateAroundAxis(angle:Right(), 270)
        local pos = self:LocalToWorld(Vector(9, 0, 20))
        cam.Start3D2D(pos, angle, 0.2)
            draw.DrawText(
                "S.W.A.T Armory",
                "Arial Black45_shady",
                0,
                7,
                (DelMods.armory.IsLockDownActive() or not GetGlobalBool("DelMods.armory.requiresLockdown")) and Color(0, 255, 0, 255) or Color(255, 0, 0, 200),
                TEXT_ALIGN_CENTER
            )
            draw.DrawText(
                "S.W.A.T Armory",
                "Arial Black45",
                0,
                0,
                (DelMods.armory.IsLockDownActive() or not GetGlobalBool("DelMods.armory.requiresLockdown")) and Color(255, 255, 0, 255) or Color(255, 0, 0, 150),
                TEXT_ALIGN_CENTER
            )
        cam.End3D2D()
    end
end