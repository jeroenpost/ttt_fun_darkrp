ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.PrintName = "SWAT Armory"
ENT.Author = "Dellkan"
ENT.Spawnable = false
ENT.AdminSpawnable = false

DelMods = DelMods or {}
DelMods.armory = DelMods.armory or {}
function DelMods.armory.IsLockDownActive()
	if ConVarExists("DarkRP_LockDown") then
		return tobool(GetConVarNumber("DarkRP_LockDown"))
	else
		return GetGlobalBool("DarkRP_LockDown")
	end
end