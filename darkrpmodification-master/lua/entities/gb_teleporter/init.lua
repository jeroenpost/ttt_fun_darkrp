AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include( "shared.lua" )

function ENT:Initialize( )
	self:SetModel( "models/props_phx/construct/wood/wood_panel1x2.mdl" )
   -- self:SetMaterial("models/effects/vol_light001" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
		
    local phys = self:GetPhysicsObject()
	if ( phys:IsValid() ) then
		phys:Wake()
	end

end

function ENT:OnTakeDamage( dmg ) 
	return false
end

function ENT:Touch( activator )
    if not activator:IsPlayer() then return end
    if not activator.nextgbTPtouch then activator.nextgbTPtouch = 0 end
    if  activator:isWanted() or activator.IsSlowedStick then
        activator:PrintMessage( HUD_PRINTCENTER,  "You are wanted or frozen, cant use teleporter" )
        return
    end
    if activator.nextgbTPtouch > CurTime() then return end
    activator.nextgbTPtouch = CurTime() + 3000
    umsg.Start("gb_vip_tp_menu", activator)
    umsg.End()

end


function ENT:Use( activator, caller )
    if not activator.nexttepeportuse then activator.nexttepeportuse = 0 end
    if activator.nexttepeportuse > CurTime() then return end
    activator.nexttepeportuse = CurTime() + 2
    if  activator:isWanted() or activator.IsSlowedStick then
        activator:PrintMessage( HUD_PRINTCENTER,  "You are wanted or frozen, cant use teleporter" )
        return
    end

    umsg.Start("gb_vip_tp_menu", activator)
    umsg.End()

end

if SERVER then
    util.AddNetworkString("gb_teleport")
    net.Receive("gb_teleport", function(length, ply)
        local place = net.ReadString()
        local location = {-1970,-1620,350 }

        if  ply:isWanted() or ply.IsSlowedStick then
            ply:PrintMessage( HUD_PRINTCENTER, "You are wanted or frozen, cant use teleporter" )
            return
        end

        if place == "close" then
            ply.nextgbTPtouch = CurTime() + 5
            return
        end

        if not gb.compare_rank(ply, {"vip","vipp","vippp"}) and not gb.is_mod(ply) then
            darkrp.notify(ply,"You need to be VIP to use this")
            return
        end

        local map = string.lower(game.GetMap())

        if map == "rp_downtown_v4c_v3_chirax" then
            if place == "hangout" then
                location = {2968,-471,8.5}
            elseif place == "casino" then
                location = {-2867,-1670,51}
            elseif place == "cardealer" then
               location = {300,-6602,-10}
            elseif place == "heaven" then
                location = {2968,-471,8.5}
            elseif place == "park" then
                location = {1541,-6668,212}
            end
        else
            if place == "hangout" then
                location = {1505,-6326,160}
            elseif place == "casino" then
                location = {-2282,-713,115}
            elseif place == "cardealer" then
                location = {-1325,-6390,150}
            elseif place == "heaven" then
                location = {-1428,-3559,1800}
            elseif place == "park" then
                location = {2600,1160,90}
            end
        end

        ply.nodamagetill = CurTime() + 5
        ply.nextgbTPtouch = 0
        ply:SetPos(Vector(location[1],location[2],location[3]))

    end)

    -- No damage for 5 seconds
    hook.Add("EntityTakeDamage","gb_tp_nodamage",function( target, dmginfo )
        if target.nodamagetill and target.nodamagetill > CurTime() then
            dmginfo:SetDamage( 0 )
        end
    end)

end

