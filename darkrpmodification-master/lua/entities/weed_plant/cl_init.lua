include("shared.lua")

function ENT:Initialize()
end

function ENT:Draw()
	self:DrawModel()
end

surface.CreateFont( "Header", {
 font = "HUDNumber5",
 size = 42,
 weight = 2500,
})

surface.CreateFont( "BasicFont", {
 font = "HUDNumber5",
 size = 22,
 weight = 2500,
})

function ENT:Think()

	if self.dt.BadLevel == 0 then
		lepl = "HYDRATED" 
		lpcol = Color(0,255,0)
	end
	if self.dt.BadLevel == 1 then
		lepl = "QUITE THIRSTY"
		lpcol = Color(0,180,0)
	end
	if self.dt.BadLevel == 2 then
		lepl = "THIRSTY"
		lpcol = Color(180,150,0)
	end
	if self.dt.BadLevel == 3 then
		lepl = "VERY THIRSTY"
		lpcol = Color(180,75,75)
	end
	if self.dt.BadLevel == 4 then
		lepl = "DEHYDRATING"
		lpcol = Color(220,0,0)
	end
	if self.dt.BadLevel == 5 then
		lepl = "DEAD"
		lpcol = Color(255,0,0)
	end

end


usermessage.Hook("leweedmenu", function( um )
local self = um:ReadEntity()
	local sFrame = vgui.Create("DFrame")
	sFrame:SetSize(450,250)
	sFrame:SetPos(ScrW()/2 - 270, ScrH()/2 + 200)
	sFrame:MoveTo(ScrW()/2-270,ScrH()- 650, 0.4,0,5)	
	sFrame:SetVisible(true)
	sFrame:MakePopup()
	sFrame:ShowCloseButton(false)
	sFrame:SetTitle("")
	sFrame.Paint = function()
		draw.RoundedBox( 0, 20, 0, 410, ScrH(), Color( 34, 34, 34, 210))
		draw.RoundedBox( 0, 0, 0, 800, 85, Color( 75, 139, 191, 255))
		draw.RoundedBox( 0, 8, 25, 60, 50, Color( 224, 224, 224, 255))
		draw.RoundedBox( 0, 8, 10, 60, 8, Color( 224, 224, 224, 255))
		
		texture = surface.GetTextureID("vgui/white");
		
		surface.SetTexture(texture)
		surface.SetDrawColor(75,139,191,255)
		surface.DrawTexturedRectRotated(65,10,10,25,-130)
//

		draw.RoundedBox( 0, 20, 45, 35, 20, Color( 75, 139, 191, 255))
		draw.RoundedBox( 0, 15, 60, 45, 5, Color( 75, 139, 191, 255))
		draw.RoundedBox( 0, 37, 30, 3, 15, Color( 75, 139, 191, 255))


		draw.SimpleTextOutlined("THE WEED PLANT", "HUDNumber5", 77, 18,Color(224,224,224), TEXT_ALIGN_LEFT,0,0,Color(0,0,0))
		draw.SimpleTextOutlined("Grow your own plants.", "Trebuchet24", 100, 55,Color(224,224,224), TEXT_ALIGN_LEFT,0,0,Color(0,0,0))
		draw.RoundedBox( 0, 0, 95, ScrW(), 32, Color( 224, 224, 224))
		end
		local EXIT = vgui.Create("DButton",sFrame)
		EXIT:SetPos(350, 0)
		EXIT:SetSize(100,25)
		EXIT:SetText("")
		EXIT.Paint = function()
		--draw.RoundedBox( 0, 0, 2,ScrW(), 110, Color( 30, 30, 40, 255))
		draw.RoundedBox( 0, 0, 2, ScrW(), 110, Color( 75, 139, 191, 255))
		--draw.RoundedBox( 0, 1, 1,288, 23, Color( 0, 73, 153, 255))
		draw.SimpleTextOutlined("X", "Trebuchet24", 80, 1,Color(255,255,255), TEXT_ALIGN_CENTER,0,0,Color(0,0,0))
		end
		EXIT.DoClick = function()
		sFrame:Remove()
		end
		BTStats = vgui.Create( "DButton", sFrame )
		BTStats:SetSize(150, 32.5)
		BTStats:SetPos(0,95)
		BTStats:SetText("")
		BTStats.Paint = function()
			draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( 34, 34, 34))
			draw.RoundedBox( 0, 147, 0, 3, ScrH(), Color( 224, 224, 224))
			draw.SimpleTextOutlined( "STATS", "Trebuchet24",75, 3, Color( 224, 224, 224, 255), 1, 0, 0, Color( 0, 0, 0 ) )
		end
		BTStats.DoClick = function()
			if BTWeed2 then
				BTWeed2:Remove()
				WeedFrame:Remove()
				BTWeedWater:Remove()
				BTWeedSell:Remove()
			end
			BTStats2 = vgui.Create( "DButton", sFrame )
			BTStats2:SetSize(150, 32.5)
			BTStats2:SetPos(0,95)
			BTStats2:SetText("")
			BTStats2.Paint = function()
				draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( 75, 139, 191, 255))
				draw.RoundedBox( 0, 147, 0, 3, ScrH(), Color( 224, 224, 224))
				draw.SimpleTextOutlined( "STATS", "Trebuchet24",75, 3, Color( 224, 224, 224, 255), 1, 0, 0, Color( 0, 0, 0 ) )
			end
			StatsFrame = vgui.Create("DFrame", sFrame)
			StatsFrame:SetPos(35,135)
			StatsFrame:SetSize(450-35*2,105)
			StatsFrame:SetTitle("")
			StatsFrame:ShowCloseButton(false)
			StatsFrame.Paint = function()
                if not IsValid(self) or not self.dt then return end
				draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( 34, 34, 34, 240))
				draw.SimpleTextOutlined("STATS", "Trebuchet22",25, 5, Color(224,224,224,20), TEXT_ALIGN_LEFT, 0, 0, Color( 0, 0, 0 ) )
				draw.SimpleTextOutlined("LEVEL: "..self.dt.PlantLevel, "Trebuchet24",350, 30, Color(224,224,224), TEXT_ALIGN_RIGHT, 0, 0, Color( 0, 0, 0 ) )
				draw.SimpleTextOutlined("WORTH: $"..self.dt.Price, "Trebuchet24",25, 30, Color(224,224,224), TEXT_ALIGN_LEFT, 0, 0, Color( 0, 0, 0 ) )
				draw.SimpleTextOutlined(lepl, "Trebuchet24",25, 60, lpcol, TEXT_ALIGN_LEFT, 0, 0, Color( 0, 0, 0 ) )
				
			end
		end	
		BTWeed = vgui.Create( "DButton", sFrame )
		BTWeed:SetSize(150, 32.5)
		BTWeed:SetPos(150,95)
		BTWeed:SetText("")
		BTWeed.Paint = function()
			draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( 34, 34, 34))
			draw.RoundedBox( 0, 147, 0, 3, ScrH(), Color( 224, 224, 224))
			draw.SimpleTextOutlined( "OTHER", "Trebuchet24",75, 3, Color( 224, 224, 224, 255), 1, 0, 0, Color( 0, 0, 0 ) )
		end
		BTWeed.DoClick = function()
			if BTStats2 then
				BTStats2:Remove()
				StatsFrame:Remove()
			end
		
			BTWeed2 = vgui.Create( "DButton", sFrame )
			BTWeed2:SetSize(150, 32.5)
			BTWeed2:SetPos(150,95)
			BTWeed2:SetText("")
			BTWeed2.Paint = function()
				draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( 75, 139, 191, 255))
				draw.RoundedBox( 0, 147, 0, 3, ScrH(), Color( 224, 224, 224))
				draw.SimpleTextOutlined( "OTHER", "Trebuchet24",75, 3, Color( 224, 224, 224, 255), 1, 0, 0, Color( 0, 0, 0 ) )
			end
			WeedFrame = vgui.Create("DFrame", sFrame)
			WeedFrame:SetPos(35,135)
			WeedFrame:SetSize(450-35*2,105)
			WeedFrame:SetTitle("")
			WeedFrame:ShowCloseButton(false)
			WeedFrame.Paint = function()
				draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( 34, 34, 34, 240))
			end
		BTWeedWater = vgui.Create( "DButton", sFrame )
		BTWeedWater:SetSize(150, 32.5)
		BTWeedWater:SetPos(45,150)
		BTWeedWater:SetText("")
		BTWeedWater.Paint = function()
			draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color(224, 224, 224))
			draw.SimpleTextOutlined( "WATER PLANT", "Trebuchet24",75, 3.5, Color( 34, 34, 34, 255), 1, 0, 0, Color( 0, 0, 0 ) )
		end
		BTWeedWater.DoClick = function()
			BTWeedWaterCol = vgui.Create( "DButton", sFrame )
			BTWeedWaterCol:SetSize(150, 32.5)
			BTWeedWaterCol:SetPos(45,150)
			BTWeedWaterCol:SetText("")
			BTWeedWaterCol.Paint = function()
				draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( 180, 75, 75))
				draw.SimpleTextOutlined( "WATER PLANT", "Trebuchet24",75, 3.5, Color( 224, 224, 224, 255), 1, 0, 0, Color( 0, 0, 0 ) )
			end
			timer.Simple(0.1,function()
				BTWeedWaterCol:Remove()
			end)
			timer.Simple(0.2,function()
				net.Start("WaterPlant")
				net.WriteEntity(self)
				net.SendToServer()
			end)
		end
		BTWeedSell = vgui.Create( "DButton", sFrame )
		BTWeedSell:SetSize(150, 32.5)
		BTWeedSell:SetPos(255,150)
		BTWeedSell:SetText("")
		BTWeedSell.Paint = function()
			draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color(224, 224, 224))
			draw.SimpleTextOutlined( "SELL PLANT", "Trebuchet24",75, 3.5, Color( 34, 34, 34, 255), 1, 0, 0, Color( 0, 0, 0 ) )
		end
		BTWeedSell.DoClick = function()
			BTWeedSellCol = vgui.Create( "DButton", sFrame )
			BTWeedSellCol:SetSize(150, 32.5)
			BTWeedSellCol:SetPos(255,150)
			BTWeedSellCol:SetText("")
			BTWeedSellCol.Paint = function()
				draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( 180, 75, 75))
				draw.SimpleTextOutlined( "SELL PLANT", "Trebuchet24",75, 3.5, Color( 224, 224, 224, 255), 1, 0, 0, Color( 0, 0, 0 ) )
			end
			timer.Simple(0.1,function()
				BTWeedSellCol:Remove()
			end)
			timer.Simple(0.2,function()

                local id = self:EntIndex()
                for k,v in pairs(ents.FindByClass("weed_plant")) do
                    if v:EntIndex() == id then
                        Plant2 = v
                    end
                end
                if not IsValid(Plant2) then return end
				sFrame:Remove()
				net.Start("SellPlant")
				net.WriteEntity(Plant2)
				net.SendToServer()
			end)
		end
		
	end
end)
