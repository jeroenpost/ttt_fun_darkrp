AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")
/*_________________________________________________
Configs!
_________________________________________________*/
WBPlant = {}
WBPlant.DarkRP25 = true -- Wethever you are using DarkRP 2.5 or 2.4..
// Prices
WPlantLevel0Price = 100 -- The price you can sell it for at level 0
WPlantLevel1Price = 500 -- The price you can sell it for at level 1
WPlantLevel2Price = 1000 -- The price you can sell it for at level 2
WPlantLevel3Price = 1500 -- The price you can sell it for at level 3
WPlantLevel4Price = 2000 -- The price you can sell it for at level 4
WPlantLevel5Price = 3000 -- The price you can sell it for at level 5

// Rot time..
WPlantRotTimeMin = 80 -- The minimum amount of seconds before the plants goes a rotting level up.
WPlantRotTimeMax = 180 -- The maximum amount of seconds before the plants goes a rotting level up.
//NOTE AT ROTTING LEVEL 5 THE PLANT REMOVES IT SELF.

// Level up
WPlantLevelTimeMin = 60 -- The minimum amount of seconds before the plant levels up.
WPlantLevelTimeMax = 900 -- The maximum amount of seconds before the plant levels up.

ENT.SeizeReward = 0 -- The reward police will get for destroying this weed.
function ENT:Initialize()
	self:SetModel("models/props/cs_office/plant01.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
	local phys = self:GetPhysicsObject()
	phys:Wake()	
	self.damage = 350
	self:MakeItBad()
	self:LevelThePlant()
	self.dt.BadLevel = 0
	self.dt.PlantLevel = 0
end

function ENT:OnTakeDamage(dmg)

	self.damage = (self.damage) - dmg:GetDamage()
	if self.damage < 1 then
	self:Remove()
	end
end

function ENT:Use(ply)
	umsg.Start("leweedmenu", ply)
		umsg.Entity(self)
	umsg.End()
end

util.AddNetworkString("WaterPlant")
net.Receive("WaterPlant", function(len, player)
local plant = net.ReadEntity()
	if plant:IsValid() and plant:GetClass() == "weed_plant" then

		if plant.dt.BadLevel == 0 then
			player:SendLua(
			[[
			chat.AddText( Color(0,255,0), "[Weed Plant]", Color(255,255,255), " Your weed plant does not need water")]])
			return
		end
	
		player:SendLua(
		[[
		chat.AddText( Color(0,255,0), "[Weed Plant]", Color(255,255,255), " You gave your weed plant some water.")]])
		plant.dt.BadLevel = plant.dt.BadLevel -1 
	
		if plant.dt.BadLevel == 0 then 
			plant:SetColor(Color(255,255,255))
		end
	
		if plant.dt.BadLevel == 1 then 
			plant:SetColor(Color(250,200,200))
		end
	
		if plant.dt.BadLevel == 2 then 
			plant:SetColor(Color(250,160,160))
		end
	
		if plant.dt.BadLevel == 3 then 
			plant:SetColor(Color(250,120,120))
		end
	
		if plant.dt.BadLevel == 4 then 
			plant:SetColor(Color(250,80,80))
		end
	
		if plant.dt.BadLevel == 5 then 
			plant:SetColor(Color(250,40,40))
		end
	
	end
end)

function ENT:MakeItBad()
if self.Removed then return end
	timer.Create("GetBad"..self:EntIndex(), math.random(WPlantRotTimeMin,WPlantRotTimeMax), 0, function()
	self.dt.BadLevel = self.dt.BadLevel + 1
	
	if self.dt.BadLevel == 0 then 
	self:SetColor(Color(255,255,255))
	end
	
	if self.dt.BadLevel == 1 then 
	self:SetColor(Color(250,200,200))
	end
	
	if self.dt.BadLevel == 2 then 
	self:SetColor(Color(250,160,160))
	end
	
	if self.dt.BadLevel == 3 then 
	self:SetColor(Color(250,120,120))
	end
	
	if self.dt.BadLevel == 4 then 
	self:SetColor(Color(250,80,80))
	end
	
	if self.dt.BadLevel == 5 then 
	self:SetColor(Color(250,40,40))
	end
	
			if self.dt.BadLevel >= 5 then
			self:Remove()
		end
	end)
end

function ENT:LevelThePlant()
if self.Removed then return end
	local owner = self:Getowning_ent()
	timer.Create("LevelUp"..self:EntIndex(), math.random(WPlantLevelTimeMin,WPlantLevelTimeMax), 0, function() -- repeats le forever
	self.dt.PlantLevel = self.dt.PlantLevel + 1
	owner:SendLua(
	[[
	chat.AddText( Color(0,255,0), "[Weed Plant]", Color(255,255,255), " Your weed plant is now : Level ]] .. self.dt.PlantLevel .. [[.")]])
	if self.dt.PlantLevel == 0 then
	self.dt.Price = WPlantLevel0Price
	end
	if self.dt.PlantLevel == 1 then
	self.dt.Price = WPlantLevel1Price
	end
	
	if self.dt.PlantLevel == 2 then
	self.dt.Price = WPlantLevel2Price
	end
	
	if self.dt.PlantLevel == 3 then
	self.dt.Price = WPlantLevel3Price
	end

	if self.dt.PlantLevel == 4 then
	self.dt.Price = WPlantLevel4Price
	end
	
	if self.dt.PlantLevel == 5 then
	self.dt.Price = WPlantLevel5Price
	timer.Stop("LevelUp"..self:EntIndex())
	end

	end)
end

util.AddNetworkString("SellPlant")
net.Receive("SellPlant", function(len, player)
	local plant = net.ReadEntity()
    local ply = player

    if not plant:EntIndex() then return end
    local Plant2 = false
    for k,v in pairs(ents.FindByClass("weed_plant")) do
        if v:EntIndex() == plant:EntIndex() then
            Plant2 = v
        end
    end

    plant = Plant2

    if not IsValid(Plant2) then
        DarkRP.notify(ply, 1, 5,  "Dont try hacking please.")
        DarkRP.printMessageAll(HUD_PRINTCENTER, ply:Nick().." is hacking" )
        DarkRP.notifyAll(HUD_PRINTCENTER, 10, ply:Nick().." is hacking. Weedplant hack" )
        ply:Kick()
        return
    end

	if plant:IsValid() and plant:GetClass()  == "weed_plant" then
		if WBPlant.DarkRP25 then
			player:addMoney(plant.dt.Price)
		else
			player:AddMoney(plant.dt.Price)
		end
		plant:Remove()
		player:SendLua(
		[[
		chat.AddText( Color(0,255,0), "[Weed Plant]", Color(255,255,255), " You have sold your weed plant for $]] .. tonumber(plant.dt.Price) .. [[.")]])
	else
        print("Hmm, plant broken")
    end
end)

function ENT:OnRemove( )
self.Removed = true
timer.Stop("LevelUp"..self:EntIndex())
timer.Stop("GetBad"..self:EntIndex())
self.dt.BadLevel = 0
self.dt.PlantLevel = 0
end

