ENT.Type = "anim"
ENT.Base = "food_base"

ENT.Category = "GreenBlack"
ENT.PrintName = "Brocolli Pizza"
ENT.Author = "GreenBlack"
ENT.Purpose = "Eating"
ENT.Instructions = "Delicious pie of Brocolli Pizza!"

ENT.Spawnable			= true
ENT.AdminSpawnable		= true