ENT.Type = "anim"
ENT.Base = "base_gmodentity"

ENT.PrintName = "food_base"
ENT.Category = "Food Mod"
ENT.Author = "KoZ"
ENT.Contact = "http://steamcommunity.com/id/drunkenkoz"
ENT.Purpose = "Food Mod Base Entity"
ENT.Information = "Base for food mod."

ENT.Spawnable			= false
ENT.AdminSpawnable		= false

foodmod = {} -- Do not edit this line

-- Changes the mode of the food mod ( 1 = health, 2 = hungermod )
foodmod.mode = 1

-- Disables the default foodmod sounds.
foodmod.enablesounds = true

-- Mode 1 configuration
foodmod.healthgain = 15

-- Mode 2 configuration
foodmod.energygain = 20