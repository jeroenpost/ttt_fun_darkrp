if (SERVER) then
	AddCSLuaFile();
end;

DEFINE_BASECLASS("base_entity");

ENT.PrintName		= "Ladder (Small - Type 2)"
ENT.Category		= "Ladders"
ENT.Spawnable		= true
ENT.AdminOnly		= false
ENT.Model			= Model("models/props_c17/metalladder002.mdl");
ENT.RenderGroup 	= RENDERGROUP_BOTH;

if (SERVER) then

	function ENT:Initialize()
		self:SetModel(self.Model);
		self:SetSolid(SOLID_VPHYSICS);
		self:PhysicsInit(SOLID_VPHYSICS);
		self:SetUseType(SIMPLE_USE);

		local phys = self:GetPhysicsObject();

		if (IsValid(phys)) then
			phys:EnableMotion(false);
		end;

		self:UpdateLadder(true);
	end;

	function ENT:UpdateLadder(bCreate)
		if (bCreate) then
			local oldAngs = self:GetAngles();

			self:SetAngles(Angle(0, 0, 0));

			SafeRemoveEntity(self.ladder);
			SafeRemoveEntity(self.bottomDismount);
			SafeRemoveEntity(self.topDismount);
			self.ladder = ents.Create("func_useableladder");
			self.ladder:SetPos(self:GetPos() + self:GetForward() * 30);
			self.ladder:SetKeyValue("point0", tostring(self:GetPos() + self:GetForward() * 30));
			self.ladder:SetKeyValue("point1", tostring(self:GetPos() + self:GetForward() * 30 + self:GetUp() * 128));
			self.ladder:SetKeyValue("targetname", "zladder_" .. self:EntIndex());
			self.ladder:SetParent(self);
			self.ladder:Spawn();

			self.bottomDismount = ents.Create("info_ladder_dismount");
			self.bottomDismount:SetPos(self:GetPos() + self:GetForward() * 62);
			self.bottomDismount:SetKeyValue("laddername", "zladder_" .. self:EntIndex());
			self.bottomDismount:SetParent(self);
			self.bottomDismount:Spawn();

			self.topDismount = ents.Create("info_ladder_dismount");
			self.topDismount:SetPos(self:GetPos() - self:GetForward() * 2 + self:GetUp() * 128);
			self.topDismount:SetKeyValue("laddername", "zladder_" .. self:EntIndex());
			self.topDismount:SetParent(self);
			self.topDismount:Spawn();

			self.ladder:Activate();

			self:SetAngles(oldAngs);
		else
			self.ladder:Activate();
		end;
	end;

	function ENT:Think()
		if (IsValid(self.ladder)) then
			self:UpdateLadder();
			self:NextThink(CurTime() + 1);
			return true;
		end;
	end;

	function ENT:PostEntityPaste()
		self:UpdateLadder(true);
	end;

elseif (CLIENT) then

	function ENT:Initialize()
		self:SetSolid(SOLID_VPHYSICS);
	end;

	function ENT:Draw()
		self:DrawModel();
	end;
end;