ENT.Type = "anim"
ENT.Base = "food_base"

ENT.Category = "GreenBlack"
ENT.PrintName = "Veggie Pizza"
ENT.Author = "GreenBlack"
ENT.Purpose = "Eating"
ENT.Instructions = "Delicious pie of Veggie Pizza!"

ENT.Spawnable			= true
ENT.AdminSpawnable		= true