ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.PrintName = "Crafting Table"
ENT.Author = "Fat Jesus / Trickster"



function ENT:SetupDataTables()
	self:NetworkVar("Entity",1,"owning_ent")
end

-- MATERIAL OPTIONS!
-- wood
-- spring
-- ironbar
-- wrench
-- MATERIAL OPTIONS!

Crafting_Recipes = {} 
/*
I'm gonna explain how to add new items, and it's actualy really easy, that is what i spent most time on doing, SO IT'S EASY TO ADD NEW ITEMS! :D

Crafting_Recipes["AK-47"] = { -- The name inside the [" "] HAS TO BE THE SAME AS TheName, else this wil NOT work!
	TheName = "AK-47", The name that will be displayed.
	HowTo = "To create a AK-47 you will need wood x 1 and spring x 2", The information that wil be shown in the chatbox when you press on it.
	Materials = { This is where you write the materials needed to craft your entity!
		wood = 1, Do remember there has to be a comma after every material
		spring = 2,
	},
	Create = "weapon_ak47custom", -- The entity it creates, this is the folder name of the entity, just copy the name and place it inside the " "
} -- Remember no comma after this!

*/




Crafting_Recipes["EOtech Sight"] = {
    TheName = "EOtech Sight",
    HowTo = "You will need 1 wood, 1 wrench 5 iron and 5 springs",
    Materials = {
        wood = 1,
        wrench = 1,
        ironbar = 2,
        spring = 2,
    },
    Create = "fas2_att_eotech",
}

Crafting_Recipes["Acog Sight"] = {
    TheName = "Acog Sight",
    HowTo = "You will need 1 wood, 1 wrench 5 iron and 3 springs",
    Materials = {
        wood = 1,
        wrench = 1,
        ironbar = 4,
        spring = 3,
    },
    Create = "fas2_att_acog",
}

Crafting_Recipes["Foregrip"] = {
    TheName = "Foregrip",
    HowTo = "You will need 5 wood and 1 wrench",
    Materials = {
        wood = 3,
        wrench = 1,
        ironbar = 0,
        spring = 0,
    },
    Create = "fas2_att_foregrip",
}

Crafting_Recipes["Silencer"] = {
    TheName = "Silencer",
    HowTo = "You will need 1 wrench 1 iron and 6 springs",
    Materials = {
        wood = 0,
        wrench = 1,
        ironbar = 1,
        spring = 3,
    },
    Create = "fas2_att_suppressor",
}

Crafting_Recipes["Knife"] = {
    TheName = "Knife",
    HowTo = "You will need 5 ironbars an 1 wood",
    Materials = {
        wood = 1,
        wrench = 0,
        ironbar = 4,
        spring = 0,
    },
    Create = "weapon_real_cs_knife",
}

Crafting_Recipes["Glock 20"] = {
    TheName = "Glock 20",
    HowTo = "You will need 1 wood, 6 iron and 10 spring",
    Materials = {
        wood = 1,
        wrench = 0,
        ironbar = 2,
        spring = 3,
    },
    Create = "fas2_glock20",
}

Crafting_Recipes["AK47"] = {
    TheName = "AK47",
    HowTo = "You will need 3 wood, 5 wrenches, 15 iron and 14 springs",
    Materials = {
        wood = 1,
        wrench = 1,
        ironbar = 2,
        spring = 2,
    },
    Create = "fas2_ak47",
}

Crafting_Recipes["RK 95"] = {
    TheName = "RK 95",
    HowTo = "You will need 3 wood, 5 wrenches, 15 iron 10 springs",
    Materials = {
        wood = 1,
        wrench = 2,
        ironbar = 2,
        spring = 2,
    },
    Create = "fas2_rk95",
}

Crafting_Recipes["M24"] = {
    TheName = "M24",
    HowTo = "You will need 5 wood, 8 wrenches, 18 iron 17 springs",
    Materials = {
        wood = 1,
        wrench = 1,
        ironbar = 2,
        spring = 4,
    },
    Create = "fas2_m24",
}


Crafting_Recipes["Pizza"] = {
    TheName = "Pizza",
    HowTo = "You need 1 wood for your pizza",
    Materials = {
        wood = 1,
        wrench = 0,
        ironbar = 0,
        spring = 0,
    },
    Create = "food_veggie_pizza",
}

