

EFFECT.Mat = Material( "effects/tool_tracer" )
local matLight 	= Material( "effects/yellowflare" )

function EFFECT:Init( data )

	self.Position = data:GetStart()
	self.WeaponEnt = data:GetEntity()
	self.Attachment = data:GetAttachment()
    self.Alpha = 255

	self.StartPos = self:GetTracerShootPos( self.Position, self.WeaponEnt, self.Attachment )
	self.EndPos = data:GetOrigin()
	
	local Weapon = data:GetEntity()
	if ( IsValid( Weapon ) ) then
        if Weapon.LaserColor then
            self.Color =  Weapon.LaserColor
         else
            self.Color =  Color(255,255,255,255)
         end


	
	end
	


end

function EFFECT:Think( )

    if not self.Alpha then self.Alpha = 255 end
	self.Alpha = self.Alpha - FrameTime() * 500
	
	self.Entity:SetRenderBoundsWS( self.StartPos, self.EndPos )
	
	if (self.Alpha < 0) then return false end
	return true

end

function EFFECT:Render( )

	if ( self.Alpha < 1 or not self.Color) then return end
	

	self.Length = (self.StartPos - self.EndPos):Length()
		
	
	local texcoord = CurTime() * -0.2
	
	
	for i = 1, 10 do
			render.SetMaterial( self.Mat )
		texcoord = texcoord + i * 0.05 * texcoord
		render.DrawBeam( self.StartPos,
						self.EndPos,
						i * self.Alpha * 0.025,
						texcoord,
						texcoord + (self.Length / (128 + self.Alpha)),
						self.Color )
						
		render.SetMaterial( matLight )

		render.DrawSprite( self.StartPos, i * 15, i * 15, Color( self.Color.r, self.Color.g, self.Color.b, self.Alpha ) )
		render.DrawSprite( self.EndPos, i * 15, i * 15, Color( self.Color.r, self.Color.g, self.Color.b, self.Alpha ) )
	
	end
	


end
