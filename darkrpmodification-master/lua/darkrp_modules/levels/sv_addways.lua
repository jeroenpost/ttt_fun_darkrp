
function PlayerDeath(victim, weapon, killer)
	if(LevelSystemConfiguration.KillModule) then
		if(victim ~= killer) then
			if(killer:IsPlayer()) then
					if(LevelSystemConfiguration.Friendly) then
						if((killer:getDarkRPVar('level') or 1)<=(victim:getDarkRPVar('level') or 1)) then
								killer:addXP(5*(victim:getDarkRPVar('level') or 1), true)
								killer:addMoney(LevelSystemConfiguration.TakeAwayMoneyAmount)
								local xpgot = 5*(victim:getDarkRPVar('level') or 1)
								DarkRP.notify(killer, 0,4,'You got '..xpgot..' XP for killing '..victim:Nick())
								victim:addMoney(-100)
								DarkRP.notify(victim, 0,4,'You died and lost $'..LevelSystemConfiguration.TakeAwayMoneyAmount..'!')					
						else 
							DarkRP.notify(killer,0,4,'You killed '..victim:Nick())
						end
					else
						killer:addXP(5*(victim:getDarkRPVar('level') or 1),true)
						killer:addMoney(LevelSystemConfiguration.TakeAwayMoneyAmount)
						local xpgot = 5*(victim:getDarkRPVar('level') or 1)
						DarkRP.notify(killer, 0,4,'You got '..xpgot..' XP for killing '..victim:Nick())
					end
						
	        end
		end
	end
end

hook.Add( "PlayerDeath", "plyded123", PlayerDeath )


function NPCDeath(npc, killer,weapon)
	if(LevelSystemConfiguration.NPCXP) then
		if(npc != killer) then -- Not a suicide? Somehow.
			if(killer:IsPlayer()) then
				killer:addXP(LevelSystemConfiguration.NPCXPAmount, true)
				DarkRP.notify(killer, 0,4,'You got '..LevelSystemConfiguration.NPCXPAmount..' XP for killing an NPC.')
			end
		end
	end
end

hook.Add( "OnNPCKilled", "enpcded123", NPCDeath )

hook.Add( "PlayerInitialSpawn", "playerXPevery5minutes", function(ply)
    local time = LevelSystemConfiguration.Timertime
    local timername = "PlayXP_"..ply:SteamID()
    timer.Create( timername, time,0,function()
        if not IsValid(ply) then
            timer.Destroy(timername)
            return
        end
        if IsValid(ply) and not ply.isafk and (LevelSystemConfiguration.TimerModule) then
            --for k,v in pairs(player.GetAll()) do

                local amount = 500

                DarkRP.notify(ply,0,4,"You got "..amount.." XP for playing on TTT-FUN")
                ply:addXP(amount,true)
            --end
        end
    end)
end)