

LevelSystemConfiguration = {} -- Leave this :)
local Printers = {} -- This too
local XPBooks = {} -- And this



-- Okay so, clientside settings first, such as the HUD.							

LevelSystemConfiguration.EnableHUD = false -- Is the HUD enabled?
LevelSystemConfiguration.LevelColor = Color(255,255,255,255) -- The color of the "Level: 1" HUD element. White looks best. (This setting is nullified if you have the prestige system)
LevelSystemConfiguration.XPTextColor = Color(255,255,255,255) -- The color of the XP percentage HUD element.
LevelSystemConfiguration.LevelBarColor = {6,116,255} -- The color of the XP bar. (Sorry this one is different. It is still RGB)
LevelSystemConfiguration.LevelTextPos = {1.5, 170} -- The position of the LevelText. Y starts from bottom. Fiddle with it

LevelSystemConfiguration.GreenJobBars = true -- Are the green bars at the bottom of jobs enabled? KEEP THIS TRUE! 
LevelSystemConfiguration.GreenAllBars = true -- Are the green bars at the bottom of everything but jobs enabled? I WOULD VERY MUCH RECCOMEND THIS TO BE TRUE.

--Now, let us configure the ways (other than printers) that players get XP
LevelSystemConfiguration.KillModule = true -- Give XP + Money for kills! -- Next 2 settings control this.
LevelSystemConfiguration.Friendly = true -- Only take away money / give XP if the killer is a lower level/same level than the victim. KEEP THIS TO TRUE UNLESS YOU WANT A STUPID RDM MINGEFEST SERVER.
LevelSystemConfiguration.TakeAwayMoneyAmount = 100 -- How much money to take away from players when they are killed and add to the killer. You can change this to 0 if none. The XP amount is dynamic.

LevelSystemConfiguration.NPCXP = true -- Give XP when an NPC is killed?
LevelSystemConfiguration.NPCXPAmount = 10 -- Amount of XP to give when an NPC is killed

LevelSystemConfiguration.TimerModule = true -- Give XP to everybody every howeverlong
LevelSystemConfiguration.Timertime = 300 -- How much time (in seconds) until everybody gets given XP
LevelSystemConfiguration.TimerXPAmount = 5 -- How much XP to give each time it goes off
LevelSystemConfiguration.YourServerName = "TTT-FUN" -- The notifcation text ish. "You got 100XP for playing on this sexy server"

--Next, general settings
LevelSystemConfiguration.XPMult = 2 -- How hard it is to level up. 2 would require twice as much XP, ect.
LevelSystemConfiguration.MaxLevel = 99999 -- The max level
LevelSystemConfiguration.ContinueXP = false -- If remaining XP continues over to next levels. I recommend this to be false. Seriously. What if a level 1 gets 99999999 XP somehow? He is level 99 so quickly.

--Printer settings
LevelSystemConfiguration.PrinterSound = true -- Give the printers sounds?
LevelSystemConfiguration.PrinterMaxP = 4 -- How many times a printer can print before stopping. Change this to 0 if you want infine.
LevelSystemConfiguration.PrinterMax = 3 -- How many printers of a certain type a player can own at any one time
LevelSystemConfiguration.PrinterOverheat = true -- Can printers overheat?
LevelSystemConfiguration.PrinterTime = 120 -- How long it takes printers to print
LevelSystemConfiguration.KeepThisToTrue = true -- Can players collect from printers that are 5 levels above their level? PLEASE KEEP THIS TRUE FOR GOD SAKE
LevelSystemConfiguration.Epilepsy = false -- If printers flash different colors when they have money in them. (This is only added because somebody will complain about the removal of the green printers.)

--Let us add/change printers! Super easily!

DarkRP.registerDarkRPVar("xp", fn.Curry(fn.Flip(net.WriteInt), 2)(32), fn.Partial(net.ReadInt, 32))
DarkRP.registerDarkRPVar("level", fn.Curry(fn.Flip(net.WriteInt), 2)(32), fn.Partial(net.ReadInt, 32))
DarkRP.registerDarkRPVar("maxxp", fn.Curry(fn.Flip(net.WriteInt), 2)(32), fn.Partial(net.ReadInt, 32))




-- Here are the default printers:

local Printer={}
Printer.Name = 'Regular Printer'
Printer.Type = 'regularprinter'
Printer.XPPerPrint = 50
Printer.MoneyPerPrint = 50
Printer.Color = Color(255,255,255,255)
Printer.Model = 'models/props_lab/reciever01b.mdl'
Printer.Price = 500
Printer.Level = 1
Printer.Prestige = 0

table.insert(Printers,Printer)

local Printer={}
Printer.Name = 'Golden Money Printer'
Printer.Type = 'goldenprinter'
Printer.XPPerPrint = 100
Printer.MoneyPerPrint = 100
Printer.Color = Color(255,215,0)
Printer.Model = 'models/props_lab/reciever01b.mdl'
Printer.Price = 1200
Printer.Level = 6
Printer.Prestige = 0
table.insert(Printers,Printer)

local Printer={}
Printer.Name = 'Ruby Money Printer'
Printer.Type = 'rubyprinter'
Printer.XPPerPrint = 150
Printer.MoneyPerPrint = 200
Printer.Color = Color(255,0,0)
Printer.Model = 'models/props_lab/reciever01a.mdl'
Printer.Price = 2400
Printer.Level = 12
Printer.Prestige = 0
table.insert(Printers,Printer)

local Printer={}
Printer.Name = 'Platinum Money Printer'
Printer.Type = 'platprinter'
Printer.XPPerPrint = 250
Printer.MoneyPerPrint = 300
Printer.Color = Color(255,255,255)
Printer.Model = 'models/props_c17/consolebox03a.mdl'
Printer.Price = 4500
Printer.Level = 25
Printer.Prestige = 0
table.insert(Printers,Printer)

local Printer={}
Printer.Name = 'Diamond Money Printer'
Printer.Type = 'diamondprinter'
Printer.XPPerPrint = 450
Printer.MoneyPerPrint = 450
Printer.Color = Color(135,200,250)
Printer.Model = 'models/props_c17/consolebox01a.mdl'
Printer.Price = 10000
Printer.Level = 35
Printer.Prestige = 0
table.insert(Printers,Printer)


local Printer={}
Printer.Name = 'Emerald Money Printer'
Printer.Type = 'emeraldprinter'
Printer.XPPerPrint = 800
Printer.MoneyPerPrint = 650
Printer.Color = Color(0,100,0)
Printer.Model = 'models/props_c17/consolebox01a.mdl'
Printer.Price = 18000
Printer.Level = 50
Printer.Prestige = 0
table.insert(Printers,Printer)

local Printer={}
Printer.Name = 'Unubtainium Money Printer'
Printer.Type = 'unubprinter'
Printer.XPPerPrint = 1000
Printer.MoneyPerPrint = 750
Printer.Color = Color(0,0,0)
Printer.Model = 'models/props_c17/consolebox01a.mdl'
Printer.Price = 36000
Printer.Level = 75
Printer.Prestige = 0
table.insert(Printers,Printer)

local Printer={}
Printer.Name = 'Master Printer'
Printer.Type = 'levelprinter'
Printer.XPPerPrint = 1750
Printer.MoneyPerPrint = 800
Printer.Color = Color(0,50,0)
Printer.Model =  'models/props_c17/consolebox01a.mdl'
Printer.Price = 54000
Printer.Level = 90
Printer.Prestige = 0
table.insert(Printers,Printer)

local Printer={}
Printer.Name = "Crazy Petes Printer"
Printer.Type = 'crazypeteprinter'
Printer.XPPerPrint = 2500
Printer.MoneyPerPrint = 950
Printer.Color = Color(0,150,50)
Printer.Model =  'models/props_c17/consolebox01a.mdl'
Printer.Price = 75000
Printer.Level = 100
Printer.Prestige = 0
table.insert(Printers,Printer)

local Printer={}
Printer.Name = "Crazy Jacks Printer"
Printer.Type = 'crazyjcksprinter'
Printer.XPPerPrint = 2750
Printer.MoneyPerPrint = 1000
Printer.Color = Color(150,150,50)
Printer.Model =  'models/props_c17/consolebox01a.mdl'
Printer.Price = 100000
Printer.Level = 110
Printer.Prestige = 0
table.insert(Printers,Printer)

local Printer={}
Printer.Name = "The HAX Printerz"
Printer.Type = 'haxprinter'
Printer.XPPerPrint = 3500
Printer.MoneyPerPrint = 1200
Printer.Color = Color(160,160,160)
Printer.Model =  'models/props_c17/consolebox01a.mdl'
Printer.Price = 250000
Printer.Level = 101
Printer.Prestige = 0
table.insert(Printers,Printer)

local Printer={}
Printer.Name = "The HAXest Printerz"
Printer.Type = 'haxestprinter'
Printer.XPPerPrint = 5000
Printer.MoneyPerPrint = 1350
Printer.Color = Color(120,160,120)
Printer.Model =  'models/props_c17/consolebox01a.mdl'
Printer.Price = 350000
Printer.Level = 140
Printer.Prestige = 0
table.insert(Printers,Printer)

local Printer={}
Printer.Name = 'Ruby VIP Money Printer'
Printer.Type = 'rubyvipprinter'
Printer.XPPerPrint = 350
Printer.MoneyPerPrint = 210
Printer.Color = Color(255,0,0)
Printer.Model = 'models/props_c17/consolebox05a.mdl'
Printer.Price = 2500
Printer.Level = 12
Printer.Prestige = 0
Printer.Vip = {"vip","vipp","vippp" }
Printer.vipName = "VIP"
table.insert(Printers,Printer)

local Printer={}
Printer.Name = 'Platinum VIP Money Printer'
Printer.Type = 'platvipprinter'
Printer.XPPerPrint = 550
Printer.MoneyPerPrint = 325
Printer.Color = Color(255,0,0)
Printer.Model = 'models/props_c17/consolebox05a.mdl'
Printer.Price = 5000
Printer.Level = 25
Printer.Prestige = 0
Printer.Vip = {"vip","vipp","vippp" }
Printer.vipName = "VIP"
table.insert(Printers,Printer)

local Printer={}
Printer.Name = 'Diamond VIP+ Money Printer'
Printer.Type = 'diamondvippprinter'
Printer.XPPerPrint = 750
Printer.MoneyPerPrint = 480
Printer.Color = Color(135,200,250)
Printer.Model = 'models/props_c17/consolebox05a.mdl'
Printer.Price = 12500
Printer.Level = 35
Printer.Prestige = 0
Printer.Vip = {"vipp","vippp" }
Printer.vipName = "VIP+"
table.insert(Printers,Printer)

local Printer={}
Printer.Name = 'Emerald VIP+ Money Printer'
Printer.Type = 'emeraldvippprinter'
Printer.XPPerPrint = 1200
Printer.MoneyPerPrint = 700
Printer.Color = Color(0,100,0)
Printer.Model = 'models/props_c17/consolebox05a.mdl'
Printer.Price = 19000
Printer.Level = 50
Printer.Prestige = 0
Printer.Vip = {"vipp","vippp" }
Printer.vipName = "VIP+"
table.insert(Printers,Printer)

local Printer={}
Printer.Name = 'Unubtainium VIP+ Money Printer'
Printer.Type = 'unubvipprinter'
Printer.XPPerPrint = 1800
Printer.MoneyPerPrint = 850
Printer.Color = Color(255,255,255)
Printer.Model = 'models/props_c17/consolebox05a.mdl'
Printer.Price = 37000
Printer.Level = 75
Printer.Prestige = 0
Printer.Vip = {"vipp","vippp" }
Printer.vipName = "VIP+"
table.insert(Printers,Printer)

local Printer={}
Printer.Name = 'Master VIP++ Printer'
Printer.Type = 'mastervippp'
Printer.XPPerPrint = 2500
Printer.MoneyPerPrint = 850
Printer.Color = Color(55,255,255)
Printer.Model = 'models/props_c17/consolebox05a.mdl'
Printer.Price = 54000
Printer.Level = 90
Printer.Prestige = 0
Printer.Vip = {"vippp" }
Printer.vipName = "VIP++"
table.insert(Printers,Printer)







	for k,v in pairs(Printers) do
		local Errors = {}
		if not type(v.Name) == 'string' then table.insert(Errors, 'The name of a printer is INVALID!') end
		if not type(v.Type) == 'string' then table.insert(Errors, 'The type of a printer is INVALID!') end
		if not type(v.XPPerPrint) == 'number' then table.insert(Errors, 'The XP of a printer is INVALID!') end
		if not type(v.MoneyPerPrint) == 'number' then table.insert(Errors, 'The money of a printer is INVALID!') end
		if not type(v.Color) == 'table' then table.insert(Errors, 'The color of a printer is INVALID!') end
		if not type(v.Model) == 'string' then table.insert(Errors, 'The model of a printer is INVALID!') end
		if not type(v.Price) == 'number' then table.insert(Errors, 'The price of a printer is INVALID!') end
		if not type(v.Level) == 'number' then table.insert(Errors, 'The level of a printer is INVALID!') end
		local ErrorCount = 0
		for k,v in pairs(Errors) do
			error(v)
			ErrorCount = ErrorCount + 1
		end

		if not(ErrorCount==0) then return false end
	    if not v.Vip then 
		DarkRP.createEntity(v.Name,{
			ent = "vrondakis_printer",
			model = v.Model,
			price = v.Price,
			prestige = (v.Prestige or 0),
			printer = true,
			level = v.Level,
            SeizeReward = (v.Price/2),
			max = LevelSystemConfiguration.PrinterMax,
			cmd = 'buyvrondakis'..v.Type..'printer',
            customCheck  = function(ply) if (SERVER and ply:get_number_of_printers() > 4) then
                        DarkRP.notify(ply, 1, 4, "You can only have 5 printers spawned at the same time")
                        return false
                        end return true end,
			vrondakisName = v.Name,
			vrondakisType = v.Type,
			vrondakisXPPerPrint = v.XPPerPrint,
			vrondakisMoneyPerPrint = v.MoneyPerPrint,
			vrondakisColor = v.Color,
			vrondakisModel = v.Model,
			vrondakisOverheat = LevelSystemConfiguration.PrinterOverheat,
			PrinterMaxP = LevelSystemConfiguration.PrinterMaxP,
			vrondakisPrinterTime = LevelSystemConfiguration.PrinterTime,
			vrondakisIsBuyerRetarded = LevelSystemConfiguration.KeepThisToTrue,
			vrondakisEpileptic = LevelSystemConfiguration.Epilepsy,
            shop_printers = true,
            shop = true
		})
        else
            DarkRP.createEntity(v.Name,{
                ent = "vrondakis_printer",
                model = v.Model,
                price = v.Price,
                SeizeReward = (v.Price/2),
                prestige = (v.Prestige or 0),
                printer = true,
                level = v.Level,
                max = LevelSystemConfiguration.PrinterMax,
                cmd = 'buyvrondakis'..v.Type..'printer',
                customCheck  = function(ply) if (SERVER and ply:get_number_of_printers() > 4) then
                    DarkRP.notify(ply, 1, 4, "You can only have 5 printers spawned at the same time")
                    return false
                end return true end,
                vrondakisName = v.Name,
                vrondakisType = v.Type,
                vrondakisXPPerPrint = v.XPPerPrint,
                vrondakisMoneyPerPrint = v.MoneyPerPrint,
                vrondakisColor = v.Color,
                vrondakisModel = v.Model,
                vrondakisOverheat = LevelSystemConfiguration.PrinterOverheat,
                PrinterMaxP = LevelSystemConfiguration.PrinterMaxP,
                vrondakisPrinterTime = LevelSystemConfiguration.PrinterTime,
                vrondakisIsBuyerRetarded = LevelSystemConfiguration.KeepThisToTrue,
                vrondakisEpileptic = LevelSystemConfiguration.Epilepsy,
                shop_vipprinters = true,
                VIPOnly = v.Vip,
                vip = v.Vip,
                vipName = v.vipName,
                shop = true
            })

        end
	end


hook.Add('canBuyCustomEntity', 'canbuyprintervip', function(ply, entity)
    if (entity.vip) then
        if not gb.compare_rank(ply, entity.vip) and entity.vipName then
            DarkRP.notify(ply, 1, 4, 'You need to be '..entity.vipName..' to buy this')
            return false, true
        end
    end
end)

