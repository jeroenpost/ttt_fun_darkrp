if true then return end
jobqueue={}

net.Receive("OpenQueueWindow", function() 
	local t = tonumber(net.ReadString());

	jobqueue:OpenQueueWindow(t);
end)

function jobqueue:OpenQueueWindow(t)

	local teamName=RPExtraTeams[t].name
	local frame=vgui.Create("DFrame")
	frame:SetSize(300,110)
	frame:SetPos(ScrW()/2-frame:GetWide()/2,ScrH()/2-frame:GetTall()/2)
	frame:SetVisible(true)
	frame:SetTitle("QUEUE")
	frame:SetDraggable(false)
	frame:MakePopup()
	frame:DoModal(true)
	
	local label=vgui.Create("DLabel",frame)
	label:SetPos(10,22)
	label:SetSize(280,30)
	label:SetText("Would you like to queue to be "..teamName.."?")

	local yesbutton=vgui.Create("DButton",frame)
	yesbutton:SetPos(50,50)
	yesbutton:SetSize(70,40)
	yesbutton:SetText("Yes")
	yesbutton.DoClick=function()
		net.Start("AddToQueue")
			net.WriteDouble(t)
		net.SendToServer()

		frame:Close()
	end
	
	local cancelbutton=vgui.Create("DButton",frame)
	cancelbutton:SetPos(170,50)
	cancelbutton:SetSize(70,40)
	cancelbutton:SetText("Cancel")
	cancelbutton.DoClick=function()
		frame:Close()
	end
	
	frame:SetSkin(GAMEMODE.Config.DarkRPSkin)
end

net.Start("ChangeQueue", function()
local ot = tonumber(net.Receive(pq));
local nt = tonumber(net.Receive(t));
jobqueue:ChangeQueue(ot,nt);
end)

function jobqueue:ChangeQueue(ot,nt) --OldTeam,NewTeam

	local teamNameOld=RPExtraTeams[ot].name
	local teamNameNew=RPExtraTeams[nt].name
	local frame=vgui.Create("DFrame")
	frame:SetSize(300,150)
	frame:SetPos(ScrW()/2-frame:GetWide()/2,ScrH()/2-frame:GetTall()/2)
	frame:SetVisible(true)
	frame:SetTitle("Change queue")
	frame:SetDraggable(false)
	frame:MakePopup()
	frame:DoModal(true)
	
	local label=vgui.Create("DLabel",frame)
	label:SetPos(10,32)
	label:SetText("You are already in the queue to be "..teamNameOld.."\nWould you like to move to the new queue to be "..teamNameNew.."?\nWARING: you will lose place in old queue!")
	label:SizeToContents()
	
	local yesbutton=vgui.Create("DButton",frame)
	yesbutton:SetPos(50,90)
	yesbutton:SetSize(70,40)
	yesbutton:SetText("Yes")
	yesbutton.DoClick=function()
		net.Start("RemoveFromQueue")
			net.WriteDouble(ot)
		net.SendToServer()
		net.Start("AddToQueue")
			net.WriteDouble(nt)
		net.SendToServer()

		frame:Close()
	end
	
	local cancelbutton=vgui.Create("DButton",frame)
	cancelbutton:SetPos(170,90)
	cancelbutton:SetSize(70,40)
	cancelbutton:SetText("Cancel")
	cancelbutton.DoClick=function()
		frame:Close()
	end
	
	frame:SetSkin(GAMEMODE.Config.DarkRPSkin)
end

net.Receive("jobqueue_BecomeJob", function()
    local i = net.ReadUInt(32)

    jobqueue:BecomeJob(i)
end)

function jobqueue:BecomeJob(t)

	local agreement=false
	local time=CurTime()+20
	local teamName=RPExtraTeams[t].name
	local frame=vgui.Create("DFrame")
	frame:SetSize(230,110)
	frame:SetPos(ScrW()/2-frame:GetWide()/2,ScrH()/2-frame:GetTall()/2)
	frame:SetVisible(true)
	frame:SetTitle("Become job")
	frame:SetDraggable(false)
	frame:MakePopup()
	frame:DoModal(true)
	function frame:OnClose()
		if not agreement then
			net.Start("RemoveFromQueue")
				net.WriteDouble(t)
			net.SendToServer()
		end
	end

	local label=vgui.Create("DLabel",frame)
	label:SetPos(15,22)
	label:SetText("You are top of the queue of "..teamName..",\nwould you become this job")
	label:SizeToContents()
	
	local yesbutton=vgui.Create("DButton",frame)
	yesbutton:SetPos(20,60)
	yesbutton:SetSize(100,40)
	yesbutton:SetText(RPExtraTeams[t]["vote"] and DarkRP.getPhrase("create_vote_for_job") or DarkRP.getPhrase("become_job"))
	yesbutton.DoClick=function()
		agreement=true
		net.Start("BecomeJob")
			net.WriteDouble(t)
		net.SendToServer()

		frame:Close()
	end
	function yesbutton:Think()
		yesbutton:SetText(RPExtraTeams[t]["vote"] and DarkRP.getPhrase("create_vote_for_job") .. "("..math.floor(time-CurTime())..")" or DarkRP.getPhrase("become_job") .. "("..math.floor(time-CurTime())..")")
		if time-CurTime()<0 then frame:Close() end
	end
	
	local cancelbutton=vgui.Create("DButton",frame)
	cancelbutton:SetPos(140,60)
	cancelbutton:SetSize(70,40)
	cancelbutton:SetText("Cancel")
	cancelbutton.DoClick=function()
		frame:Close()
	end
		
	frame:SetSkin(GAMEMODE.Config.DarkRPSkin)
end