if true then return end

jobqueue={}
util.AddNetworkString("AddToQueue")
util.AddNetworkString("RemoveFromQueue")
util.AddNetworkString("BecomeJob")
util.AddNetworkString("OpenQueueWindow")
util.AddNetworkString("ChangeQueue")
util.AddNetworkString("jobqueue_BecomeJob")
local function hookedChangeTeam(p,t,f)
	
	if f then return true end
	local TEAM = RPExtraTeams[t]
	if not TEAM then return false end
	if not jobqueue.status[t] then jobqueue.status[t] = 0 end
	
	local max = TEAM.max
	if (max ~= 0 and -- No limit
	(max >= 1 and team.NumPlayers(t) >= max or -- absolute maximum
	max < 1 and (team.NumPlayers(t) + 1) / #player.GetAll() > max) and p:Team()~=t) or jobqueue.status[t]>1 then -- fractional limit (in percentages
		if jobqueue.status[t]<2 or (jobqueue.status[t]>1 and jobqueue.queue[t][1]~=p) then
			net.Start("OpenQueueWindow");
			net.WriteString(t);
			net.Send(p);
			return false
		end
	end
	
	return true
end

jobqueue.queue={}
jobqueue.status={}

local function Init_Gamemode()

	--creating callback to function changeTeam
	local meta=FindMetaTable("Player")
	meta.changeTeamOld=meta.changeTeam
	meta.changeTeam=function(p,t,f)
		if hookedChangeTeam(p,t,f) then
			p:changeTeamOld(t,f)
			if jobqueue.status[t]>1 and  jobqueue.queue[t][1]==p then
				local qplace=table.RemoveByValue(jobqueue.queue[t],p)
				if #jobqueue.queue[t]>0 then jobqueue.status[t]=1 else jobqueue.status[t]=0 end
				if #jobqueue.queue[t]>0 then
					for i=qplace,#jobqueue.queue[t] do
						DarkRP.notify(jobqueue.queue[t][i], 2, 4, "New position in queue ("..i..") "..RPExtraTeams[t].name)
					end
				end
			end
		end
	end

	--creating tables for each job
	for i=1,#RPExtraTeams do
		jobqueue.queue[i]={} --queue will be here
		jobqueue.status[i]=0 --this is status of queue 0 - free to join, 1 - queue existed, 2 - waiting for decision
		if RPExtraTeams[i].vote or RPExtraTeams[i].RequiresVote then --hook for jobs, required vote
		local cmd="vote"..RPExtraTeams[i].command
			DarkRP.getChatCommands()[cmd].callback2=DarkRP.getChatCommands()[cmd].callback
			DarkRP.getChatCommands()[cmd].callback=function(ply)
				--print(ply:GetName().." goint to be a "..RPExtraTeams[i].name)
				if hookedChangeTeam(ply,i,f) then
					DarkRP.getChatCommands()[cmd].callback2(ply)
				end
			end
		end
	end	
end
timer.Simple(1,function() Init_Gamemode() end) --loads after all


local function PlayerInQueue(ply) --check queue where player is
	for i=1,#jobqueue.queue do
		if table.HasValue(jobqueue.queue[i],ply) then return i end
	end
	return 0
end


net.Receive("AddToQueue",function(len,ply)
	local t=net.ReadDouble()
	local pq=PlayerInQueue(ply)
	if pq~=0 then
		--already in queue
		if pq==t then
			DarkRP.notify(ply,1,4,"You are in "..table.KeyFromValue(jobqueue.queue[t],ply).." place. Please wait!")
		else
			DarkRP.notify(ply,1,4,"You are in different job queue "..RPExtraTeams[pq].name.."("..table.KeyFromValue(jobqueue.queue[pq],ply)..")")
			net.Start("ChangeQueue");
			net.WriteString(pq);
			net.WriteString(t);
			net.Send(p);

		end
	else
		jobqueue.queue[t][#jobqueue.queue[t]+1]=ply
		if jobqueue.status[t]==0 then jobqueue.status[t]=1 end
		DarkRP.notify(ply,1,4,"You have added to queue, your position is "..#jobqueue.queue[t])
	end
end)
net.Receive("RemoveFromQueue",function(len,ply)
	local t=net.ReadDouble()
	if jobqueue.status[t]>1 and jobqueue.queue[t][1]==ply then jobqueue.status[t]=1 end
	local qplace=table.RemoveByValue(jobqueue.queue[t],ply)
	if #jobqueue.queue[t]==0 and jobqueue.status[t]==1 then jobqueue.status[t]=0 end
	if #jobqueue.queue[t]>0 then
		for i=qplace,#jobqueue.queue[t] do
			DarkRP.notify(jobqueue.queue[t][i], 2, 4, "New position in queue ("..i..") "..RPExtraTeams[t].name)
		end
	end
end)
net.Receive("BecomeJob",function(len,ply)
	local t=net.ReadDouble()
	
	if RPExtraTeams[t]["vote"] then
		jobqueue.status[t]=3
		ply:SendLua("RunConsoleCommand(\"darkrp\",\"vote"..RPExtraTeams[t].command.."\")")
	else
		ply:SendLua("RunConsoleCommand(\"darkrp\",\""..RPExtraTeams[t].command.."\")")
	end
end)

local function UpdateQueue() --watch for existed players. if player leave the server it will clear the place in queue also main function to submit player if he done to join team
	for i=1,#jobqueue.queue do
		for j=1,#jobqueue.queue[i] do
			if not IsValid(jobqueue.queue[i][j]) then
				local qplace=table.RemoveByValue(jobqueue.queue[i],ply)
				if #jobqueue.queue[i]==0 and jobqueue.status[i]==1 then jobqueue.status[i]=0 end
				if #jobqueue.queue[i]>0 then
					for k=qplace,#jobqueue.queue[i] do
						DarkRP.notify(jobqueue.queue[i][k], 2, 4, "New position in queue ("..k..") "..RPExtraTeams[t].name)
					end
				end
			end
		end
		local TEAM = RPExtraTeams[i]
		if not TEAM then return false end
		local max = TEAM.max
		if not (max ~= 0 and -- No limit
		(max >= 1 and team.NumPlayers(i) >= max or -- absolute maximum
		max < 1 and (team.NumPlayers(i) + 1) / #player.GetAll() > max)) and jobqueue.status[i]==1 then
			jobqueue.status[i]=2
			local ply=jobqueue.queue[i][1]
			--ply:SendLua("jobqueue:BecomeJob("..i..")")
			net.Start("jobqueue_BecomeJob")
            net.WriteUInt(i, 32)
            net.Send(ply)
			timer.Simple(15,function() 
				if IsValid(jobqueue.queue[i][1]) and jobqueue.queue[i][1]==ply and ply:Team()~=i and jobqueue.status[i]==2 then --player lagging or timeout or something like that
					local qplace=table.RemoveByValue(jobqueue.queue[i],ply)
					if #jobqueue.queue[i]==0 then jobqueue.status[i]=0 else jobqueue.status[i]=1 end
					if #jobqueue.queue[i]>0 then
						for j=qplace,#jobqueue.queue[i] do
							DarkRP.notify(jobqueue.queue[i][j], 2, 4, "New position in queue ("..j..") "..RPExtraTeams[i].name)
						end
					end
				end
			end)
		end
	end
end
hook.Add("Think","UpdateQueue",UpdateQueue)