if IsValid( GAMEMODE.LawyerHud ) then
	GAMEMODE.LawyerHud:Remove( )
end

net.Receive( "PlayerArrested", function( len )
	local criminal = net.ReadEntity( )
	criminal.arrestEnd = net.ReadUInt( 32 )
	criminal.arrestedBy = net.ReadEntity( )
end )

local function createPlyPanel( ply, parent )
	local panel = vgui.Create( "DPanel", parent )
	panel:SetTall( 75 )
	function panel:Paint( w, h )
		surface.SetDrawColor( 100, 100, 100, 200 )
		surface.DrawRect( 0, 0, w, h )
	end
	panel.ply = ply
	
	local modelPanel = vgui.Create( "DModelPanel", panel )
	modelPanel:DockMargin( 5, 5, 0, 5 )
	local op = modelPanel.Paint
	
	local oSm = modelPanel.SetModel
	function modelPanel:SetModel( str ) 
		oSm( self, str )
		local iSeq = self.Entity:LookupSequence( "idle_all_01" );
		self.Entity:ResetSequence( iSeq )
		self.bAnimated = true
	end
	function modelPanel:LayoutEntity( ent )
		self:RunAnimation()
		ent:SetAngles( Angle( 0, 45,  0) )
	end
	modelPanel:SetModel( LocalPlayer( ):GetModel( ) )
	
	function modelPanel:Paint( w, h )
		draw.RoundedBox( 6, 0, 0, w, h, Color( 50, 50, 50, 150 ) )
		op( self, w, h )
	end
	function modelPanel:Think( )
        if not IsValid(ply) then return end
		modelPanel:SetModel( ply:GetModel( ) )
		local origbone = self.Entity:LookupBone( "ValveBiped.Bip01_Spine" )
		if origbone then
			local pos, ang = self.Entity:GetBonePosition( origbone )
			self.vLookatPos = pos + Vector( 0, 0, 15 )
		end
	end
	modelPanel:SetFOV( 25 )
	modelPanel:SetCamPos( Vector( 50, 50, 50 ) )
	modelPanel:SetSize( 64, 64 )
	modelPanel:Dock( LEFT )
	
	panel.descPanel = vgui.Create( "DPanel", panel )
	panel.descPanel:Dock( FILL )
	panel.descPanel:DockMargin( 5, 0, 5, 0 )
	panel.descPanel.Paint = function( ) end
	
	panel.name = vgui.Create( "DLabel", panel.descPanel )
	panel.name:SetFont( "DermaDefault" )
	panel.name:SetText( ply:Name( ) )
	panel.name:DockPadding( 5, 5, 5, 5 )
	panel.name:Dock( TOP )
	
	panel.timeleftLabel = vgui.Create( "DLabel", panel.descPanel )
	panel.timeleftLabel:Dock( TOP )
	
	panel.arrestingOffLabel = vgui.Create( "DLabel", panel.descPanel )
	panel.arrestingOffLabel:Dock( TOP )
    if IsValid(ply.arrestedBy) then
	panel.arrestingOffLabel:SetText( "Officer: " .. ply.arrestedBy:Nick( ) )
	end

	function panel:Think( )
		if not IsValid( self.ply ) or not self.ply:isArrested( ) then
			self:Remove( )
			return
		end
		panel.name:SetText( self.ply:Nick( ) )
		
		local timeleft = self.ply.arrestEnd - CurTime( )
		timeleft = timeleft > 0 and timeleft or 0
		panel.timeleftLabel:SetText( Format( "%is left", timeleft ) )
	end
	
	return panel
end

local function createLawyerHud( )
	local panel = vgui.Create( "DPanel" )
	panel:DockMargin( 10, 10, 10, ScrH( ) / 3 )
	panel:SetWide( ScrW( ) / 6 )
	panel:Dock( LEFT )
	panel:SetSkin( GAMEMODE.Config.DarkRPSkin )
	panel:ParentToHUD( )
	panel:MoveToBack( )
	panel:SetPaintedManually( true )

	panel.txt = panel.txt or vgui.Create( "DLabel", panel )
	panel.txt:Dock( TOP )
	panel.txt:DockMargin( 5, 5, 5, 0 )
	panel.txt:SetContentAlignment( 5 )
	panel.txt:SetFont( "DermaDefault" )
	panel.txt:SetText( "Currently Arrested Players" )
	
	panel.scroll = vgui.Create( "DScrollPanel", panel )
	panel.scroll:Dock( FILL )
	panel.scroll:DockMargin( 5, 0, 5, 5 )
	panel.scroll.Paint = function( p , w, h )
	end

	panel.arrestedPlayers = vgui.Create( "DIconLayout", panel.scroll )
	function panel.arrestedPlayers:Think( )
		self:SetWide( self:GetParent( ):GetWide( ) )
		self:SizeToChildren( false, true )
	end
	panel.arrestedPlayers:SetSpaceY( 5 )
	panel.arrestedPlayers:SetBorder( 5 )
	panel.arrestedPlayers.Paint = function( p , w, h )
		---surface.SetDrawColor( Color( 2555,255,0 ) )
		--surface.DrawRect( 0, 0, w ,h )
	end

	function panel:Paint( w, h )
		surface.SetDrawColor( 50, 50, 50, 200 )
		surface.DrawRect( 0, 0, w, h )
		surface.SetDrawColor( 10, 10, 50, 220 )
		surface.DrawRect( 0, 0, w, 25 )
	end

	function panel:Think( )
		for k, v in pairs( player.GetAll( ) ) do
			if v:isArrested( ) then
				local found = false
				for _, child in pairs( self.arrestedPlayers:GetChildren( ) ) do
					if child.ply == v then
						found = true
					end	
				end 
				
				if not found then
					local plyPanel = createPlyPanel( v, self.arrestedPlayers )
					function plyPanel.PerformLayout( pnl )
						pnl:SetWide( self.arrestedPlayers:GetWide( ) - 10 )
					end
					self.arrestedPlayers:OnChildAdded( plyPanel )
				end
			end
		end
	end
	
	GAMEMODE.LawyerHud = panel
end
if IsValid( LocalPlayer( ) ) and LocalPlayer():Team( ) == TEAM_LAWYER then
	createLawyerHud( )
end

hook.Add( "PreDrawHUD", "ads", function( )
	if IsValid( GAMEMODE.LawyerHud ) then
		cam.Start2D()
		GAMEMODE.LawyerHud:SetPaintedManually( false )
		GAMEMODE.LawyerHud:PaintManual( )
		GAMEMODE.LawyerHud:SetPaintedManually( true )
		cam.End2D( )
	end
end )

hook.Add( "teamChanged", "ToggleHUD", function( before, after )
	if before == TEAM_LAYWER and after == TEAM_LAWYER then
		--nothing really happened
		return
	end
	
	if before == TEAM_LAWYER then
		if IsValid( GAMEMODE.LawyerHud ) then
			GAMEMODE.LawyerHud:Remove( )
		end
		return
	end
	
	if after == TEAM_LAWYER then
		if not IsValid( GAMEMODE.LaywerHud ) then
			createLawyerHud( )
		end
	end
end )