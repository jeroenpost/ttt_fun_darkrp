util.AddNetworkString( "LawyerOpenBailMenu" )
util.AddNetworkString( "LawyerOfferBail" )
util.AddNetworkString( "AcceptBailOffer" )

util.AddNetworkString( "PlayerBailOffer" )

resource.AddFile( "materials/gui/attorney-legal.png" )

util.AddNetworkString( "PlayerArrested" )
--Network the hook to make it available on cl as well
hook.Add( "playerArrested", "LawyerHUD", function( criminal, time, actor )
	net.Start( "PlayerArrested" )
		net.WriteEntity( criminal )
		net.WriteUInt( CurTime( ) + time, 32 )
		net.WriteEntity( actor )
	net.Broadcast( )
end )

net.Receive( "LawyerOfferBail", function( len, ply )
	if not table.HasValue( BAIL.AllowedTeams, ply:Team( ) ) then
		print( "wrongteam" )
		return
	end
	
	local plyToBail = net.ReadEntity( )
	local bailAmount = 1000 --net.ReadUInt( 16 )
	if not IsValid( plyToBail ) or not plyToBail:isArrested( ) then
		print( "notarrested" )
		return
	end
	
	--Transmit offer to player
	net.Start( "PlayerBailOffer" )
		net.WriteEntity( ply )
		net.WriteUInt( bailAmount, 16 )
	net.Send( plyToBail )
end )

net.Receive( "AcceptBailOffer", function( len, ply )
	local lawyer, amount = net.ReadEntity( ), net.ReadUInt( 16 )
	
	if not IsValid( ply ) or not ply:isArrested( ) or not ply:canAfford( amount ) then
		return
	end
	if ply:getDarkRPVar( "money" ) > 1000 then
	    ply:addMoney( -1000 )
    end
	ply:unArrest( )
	lawyer:addMoney( 1000 * BAIL.LawyerFraction )
    lawyer:addXP( 2500 )
end )

local function RequestBail( ply )
	if not table.HasValue( BAIL.AllowedTeams, ply:Team( ) ) then
		DarkRP.notify(ply, 1, 4, string.format( "You don't have the right job to use /bail"))
		return ""
	end
	
	local LookingAt = ply:GetEyeTrace().Entity

	if not IsValid(LookingAt) or not LookingAt:isArrested( ) then
		DarkRP.notify(ply, 1, 4, string.format( "You must be looking at an arrested player to use this command"))
		return ""
	end

	net.Start( "LawyerOpenBailMenu" )
		net.WriteEntity( LookingAt )
	net.Send( ply )
end
DarkRP.defineChatCommand( "bail", RequestBail )

hook.Add( "EntityTakeDamage", "asdfniggre", function( target, dmginfo )
	if target:IsPlayer( ) and IsValid( dmginfo:GetAttacker( ) ) and dmginfo:GetAttacker( ):IsPlayer( ) then
		if table.HasValue( BAIL.DisableTeamDamage, dmginfo:GetAttacker( ):Team( ) ) and table.HasValue( BAIL.DisableTeamDamage, target:Team( ) ) then
			dmginfo:SetDamage( 0 )
		end
	end
end )