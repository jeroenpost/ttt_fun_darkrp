function openLawyerBailMenu( plyTarget )
	local frame = vgui.Create( "DFrame" )
	frame:SetSize( 450, 150 )
	frame:MakePopup( )
	frame:SetTitle( "Bail a player out of jail" )
	frame:Center( )
	function frame:Paint( w, h )
		surface.SetDrawColor( 110, 110, 110, 255 )
		surface.DrawRect( 0, 0, w, h )
	end
	
	local p = vgui.Create( "DPanel", frame )
	p:Dock( LEFT )
	p:SetContentAlignment( 5 )
	p:SetWide( 110 )
	function p:Paint() end 
	local img = vgui.Create( "DImage", p )
	img:SetImage( "gui/attorney-legal.png" )
	img:SetSize( 110, 110 )
	
	local right = vgui.Create( "DPanel", frame )
	right:DockMargin( 5, 5, 5, 5 )
	right:Dock( FILL )
	function right:Paint( )
	end
	
	local title = vgui.Create( "DLabel", right )
	title:SetText( "Set admission to bail for " .. plyTarget:Nick( ) )
	title:Dock( TOP )
	title:SizeToContents( )
	
	local number = vgui.Create( "DNumSlider", right )
	number:DockMargin( 5, 5, 5, 5 )
	number:SetText( "Bail Amount: " )
	number:Dock( TOP )
	number:SetMin( BAIL.MinimumAmount )
	number:SetMax( BAIL.MaximumAmount )
	number:SetValue( BAIL.MinimumAmount )
	number:SetDecimals( 0 )
	
	local button = vgui.Create( "DButton", right )
	button:DockMargin( 5, 5, 5, 5 )
	button:SetText( "Make Offer" )
	button:Dock( TOP )
	function button:DoClick( )
		frame:Close( )
		net.Start( "LawyerOfferBail" )
			net.WriteEntity( plyTarget )
			net.WriteUInt( number:GetValue( ), 16 )
		net.SendToServer( )
		Derma_Message( plyTarget:Nick( ) .. " has received your offer. Should he accept you will receive " .. math.Round( number:GetValue( ) * BAIL.LawyerFraction ) .. "$ for your work.", "Your offer has been sent" )
	end
end
net.Receive( "LawyerOpenBailMenu", function( ) openLawyerBailMenu( net.ReadEntity( ) ) end )

function openPlayerBailMenu( lawyer, amount )
	local frame = vgui.Create( "DFrame" )
	frame:SetSize( 450, 150 )
	frame:MakePopup( )
	frame:SetTitle( "Pay Bail" )
	frame:Center( )
	function frame:Paint( w, h )
		surface.SetDrawColor( 110, 110, 110, 255 )
		surface.DrawRect( 0, 0, w, h )
	end
	
	local p = vgui.Create( "DPanel", frame )
	p:Dock( LEFT )
	p:SetContentAlignment( 5 )
	p:SetWide( 110 )
	function p:Paint() end 
	local img = vgui.Create( "DImage", p )
	img:SetImage( "gui/attorney-legal.png" )
	img:SetSize( 110, 110 )
	
	local right = vgui.Create( "DPanel", frame )
	right:DockMargin( 5, 5, 5, 5 )
	right:Dock( FILL )
	function right:Paint( )
	end
	
	local title = vgui.Create( "DLabel", right )
	title:SetText( "You have been offered to be released on bail by " .. lawyer:Nick( ) )
	title:DockMargin( 5, 5, 5, 5 )
	title:Dock( TOP )
	title:SizeToContents( )
	
	local number = vgui.Create( "DLabel", right )
	number:SetText( "Bail Amount: $" .. amount )
	number:DockMargin( 5, 5, 5, 5 )
	number:Dock( TOP )
	number:SizeToContents( )
	
	
	local button = vgui.Create( "DButton", right )
	button:DockMargin( 5, 5, 5, 5 )
	button:SetText( "Admit to bail" )
	button:Dock( TOP )
	function button:DoClick( )
		if LocalPlayer( ):canAfford( amount ) then
			frame:Close( )
			net.Start( "AcceptBailOffer" )
				net.WriteEntity( lawyer )
				net.WriteUInt( amount, 16 )
			net.SendToServer( )
			Derma_Message( "You have been released on bail", "Unarrested" )
		else
			Derma_Message( "You don't have enough money to pay the bail", "Not enough money" )
		end
	end
	if not LocalPlayer( ):canAfford( amount ) then
		button:SetColor( Color( 255, 0, 0, 255 ) )
	end
	
	local button2 = vgui.Create( "DButton", right )
	button2:SetText( "Decline" )
	button2:DockMargin( 5, 5, 5, 5 )
	button2:Dock( TOP )
	function button2:DoClick( )
		frame:Close( )
	end
end

net.Receive( "PlayerBailOffer", function( )
	local lawyer = net.ReadEntity( )
	local amount = net.ReadUInt( 16 )
	openPlayerBailMenu( lawyer, amount )
end )