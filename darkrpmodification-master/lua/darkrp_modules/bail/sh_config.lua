BAIL = {}

--This is what adds the lawyer job. 
--You can customize everything in here, Refer to
--http://wiki.darkrp.com/index.php/Functions/darkrp/shared/createjob 
--for the parameters


BAIL.AllowedTeams = {TEAM_LAWYER} --Teams that are allowed to use /bail on arrested players
BAIL.MinimumAmount = 50			--Minimum amount that the lawyer can set
BAIL.MaximumAmount = 1000		--Minimum amount that the lawyer can set
BAIL.LawyerFraction = 4/5		--Fraction of the bail money that the lawyer will receive

--Team damage is disabled for the teams in this list
--Remvoe the dashes to enable.
BAIL.DisableTeamDamage = {
	--TEAM_MAYOR,
	--TEAM_CHIEF,
	--TEAM_POLICE
}

DarkRP.declareChatCommand{
        command = "bail",
        description = "Bail a player out of jail (as a lawyer)",
        delay = 1.5
}

--Set this again
timer.Simple(5,function()
    BAIL.AllowedTeams = {TEAM_LAWYER}
end)