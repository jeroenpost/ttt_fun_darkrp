if SERVER then
	AddCSLuaFile()
else
	surface.CreateFont( "SpeedometerNumberFont", {
		font = "Trebuchet24",
		size = 19,
		weight = 500, 
		blursize = 0, 
		scanlines = 0, 
		antialias = true, 
		underline = false, 
		italic = false, 
		strikeout = false, 
		symbol = false, 
		rotary = false, 
		shadow = false, 
		additive = false, 
		outline = false,
	} )

	local speed_units = "MPH" --or km/h
	local dock_position = "br" --top left = tl, top right = tr, bottom left = br, bottom right = br

	local size_factor = 1 / 4 --size of speedometer
	local padding = 100 		  --space between speedometer and edge of screen
	local speed_max_default = 120	  --default maximum speed
	local speed_interval_default = 10 --speed interval (what intervals it puts the numbers)

	local color_bg = Color( 0, 0, 0, 240 ) 	 --background color
	local color_numbers = Color( 20, 150, 255, 255 )  --numbers color
	local color_needle = Color( 175, 75, 0, 255 )  --needle color
	local color_needle_base = Color( 0, 0, 0, 255 ) --color of the needle's base
	local color_interval = Color( 175, 175, 175, 255 ) --interval color (color of the small lines)
	local color_text = Color( 200, 200, 200, 255 )	 --units color

	local vehicle_speed_max = {  --set maximum speed and unit intervals for different vehicle models
	--[=[ Example
		["models/sentry/ferrarif1.mdl"] = { 
			speed = 90,  --maximum speed to display
			interval = 5  --speed interval
		},
	]=]
	}



	local size = math.min( ScrW(), ScrH() ) * size_factor
	local xl, yt = ScrW() - padding - size, ScrH() - padding - size
	local vel_factor = 0.75 * ((string.lower( speed_units )[1]== 'k') and 3600 * 0.0000254 or 3600 / 63360 )

	if dock_position == "tl" then
		xl, yt = padding, padding
	elseif dock_position == "tr" then
		xl, yt = ScrW() - padding - size, padding
	elseif dock_position == "bl" then
		xl, yt = padding, ScrH() - padding - size
	end

	local pi = math.pi

	local function drawCircle( x, y, rad, quality, col )
		surface.SetDrawColor( col )

		local step = (2 * pi) / quality
		local verts = {}
		local ang

		for i=1,quality do
			ang = i * step

			table.insert( verts, {
				x = x + math.cos( ang ) * rad,
				y = y + math.sin( ang ) * rad
			} )
		end

		surface.DrawPoly( verts )
	end

	hook.Add( "HUDPaint", "rp_speedometer", function()
		local veh = LocalPlayer():GetVehicle()

		if !IsValid( veh ) or string.find( veh:GetClass(), "pod" ) then return end

		local customrange = vehicle_speed_max[veh:GetModel()] or {}
		local maxspeed = customrange.speed or speed_max_default
		local interval = customrange.interval or speed_interval_default

		surface.SetTexture( 0 )
		
		local rad = size / 2

		drawCircle( xl + rad, yt + rad, rad - 2, 48, color_bg )

		local numcnt = math.ceil( maxspeed / interval )
		local step = (3 * (pi / 2)) / numcnt
		local offset = 3 * (pi / 4)
		local ang, x, y, ismajor

		for i=0,numcnt do
			ang = (i * step) + offset

			x = xl + rad + (math.cos( ang ) * (rad - 30))
			y = yt + rad + (math.sin( ang ) * (rad - 30))

			draw.SimpleText(
				tostring( i * interval ),
				"SpeedometerNumberFont",
				x, y,
				color_numbers,
				TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER
			)
		end

		local barcnt = numcnt * 10
		step = (3 * (pi / 2)) / barcnt

		surface.SetDrawColor( color_interval )

		for i=0,barcnt do
			ismajor = (i % 5) == 0
			ang = (i * step) + offset

			x = xl + rad + (math.cos( ang ) * (rad - 5))
			y = yt + rad + (math.sin( ang ) * (rad - 5))

			surface.DrawTexturedRectRotated( 
				x, y, 
				ismajor and 14 or 8, 
				ismajor and 2 or 1, 
				math.deg( -ang )
			)
		end

		local vel = veh:GetVelocity():Length()
		local speed = math.min( vel * vel_factor, maxspeed )

		drawCircle( xl + rad, yt + rad, rad / 10, 32, color_needle_base )

		local ang = offset + (speed / maxspeed) * (6 * (pi / 4))

		surface.SetDrawColor( color_needle )
		surface.DrawPoly(
		{
			{
				x = xl + rad + (math.cos( ang ) * (rad / 1.5)),
				y = yt + rad + (math.sin( ang ) * (rad / 1.5))
			},
			{
			 	x = xl + rad + (math.cos( ang + pi - pi / 30) * (rad / 3)),
				y = yt + rad + (math.sin( ang + pi - pi / 30) * (rad / 3))
			},
			{
			 	x = xl + rad + (math.cos( ang + pi ) * ((rad / 3) - 3)),
				y = yt + rad + (math.sin( ang + pi ) * ((rad / 3) - 3))
			},
			{
				x = xl + rad + (math.cos( ang + pi + pi / 30) * (rad / 3)),
				y = yt + rad + (math.sin( ang + pi + pi / 30) * (rad / 3))
			}
		} )

		draw.SimpleText(
			speed_units,
			"Trebuchet24",
			xl + rad, yt + rad * 1.5,
			color_text,
			TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER
		)
	end )
end
