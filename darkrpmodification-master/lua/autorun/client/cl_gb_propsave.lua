local gb_propsave = {}


function gb_propsave.PaintJoinWindow()
    local ply =  LocalPlayer()
    if not IsValid( ply ) then return end

    local itemnumber = ply:GetNWInt("gb_lost_items_number",0)
    local open_slots = ply:GetNWInt("gb_open_slots_number",0)

    local text = "We found "..itemnumber.." lost items!"


        surface.SetDrawColor(Color(255,90,90,240))
        surface.DrawRect(0, 0, 500, 350)

        surface.SetDrawColor(Color(150,150,150,240))
        surface.DrawRect(5, 5, 490, 340)
        local slotsmessage = "You have enough open inventory slots\nClick the button to get your items back"
        if itemnumber > open_slots then
            slotsmessage = "YOU DON'T HAVE ENOUGH OPEN SLOTS\nEmpty some slots from your inventory first!\nUse consolecommand gb_getlostitems when ready"
        end



        draw.DrawText(text, "Bebas32Font", 20, 40, Color(0,0,0), 0)
        local text2 = "You might have left the server without picking up your items,\n"
                .."or the server crashed or something\n \n"
                ..slotsmessage
        draw.DrawText(text2, "Bebas24Font", 20, 100, Color(0,0,0), 0)




end

function gb_propsave.OpenBarAlertJoin( settings )
    if not LocalPlayer() then return end

    if not gb_propsave.joinWindow then
        gb_propsave.joinWindow = vgui.Create( "DFrame" )
        gb_propsave.joinWindow:SetSize( 500,  350)
        gb_propsave.joinWindow:SetPos((ScrW()/2)-250,(ScrH()/2)-175)
        gb_propsave.joinWindow:SetDraggable( false )
        gb_propsave.joinWindow:ShowCloseButton( true )
        gb_propsave.joinWindow:SetTitle( "" )
        gb_propsave.joinWindow.Paint = gb_propsave.PaintJoinWindow
        gb_propsave.joinWindow:MakePopup();

        local itemnumber =  LocalPlayer():GetNWInt("gb_lost_items_number",0)
        local open_slots =  LocalPlayer():GetNWInt("gb_open_slots_number",0)

        if  itemnumber > open_slots then

            local DButton = vgui.Create( "DButton" )
            DButton:SetPos( 250, 250 )
            DButton:SetParent( gb_propsave.joinWindow )
            DButton:SetText( "Fill whatever possible, I accept the loss" )
            DButton:SetSize( 200, 60 )
            DButton.DoClick = function()
                LocalPlayer():ConCommand("gb_getlostitems")
                gb_propsave.CloseJoin()
            end

            local DButton = vgui.Create( "DButton" )
            DButton:SetPos( 25, 250 )
            DButton:SetParent( gb_propsave.joinWindow )
            DButton:SetText( "Close this, I make some space" )
            DButton:SetSize( 200, 60 )
            DButton.DoClick = function()
                LocalPlayer():ConCommand("gb_getlostitems")
                gb_propsave.CloseJoin()
            end

        else
            local DButton = vgui.Create( "DButton" )
            DButton:SetPos( 200, 250 )
            DButton:SetParent( gb_propsave.joinWindow )
            DButton:SetText( "Get my items back!" )
            DButton:SetSize( 120, 60 )
            DButton.DoClick = function()
               LocalPlayer():ConCommand("gb_getlostitems")
               gb_propsave.CloseJoin()
            end
        end
    else
      --  gb_propsave.CloseJoin()
    end
end

function gb_propsave.CloseJoin()
    if gb_propsave.joinWindow then
        gb_propsave.joinWindow:Remove()
        gb_propsave.joinWindow = nil
    end
end
usermessage.Hook("gb_propsave__Close_Join", gb_propsave.CloseJoin)
concommand.Add("gb_propsave_closejoin", gb_propsave.CloseJoin)

concommand.Add("gb_propsave_openwindow", gb_propsave.OpenBarAlertJoin)
