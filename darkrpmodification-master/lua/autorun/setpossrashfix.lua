--// This script prevents crashes that happen when objects move to far from the origin.
--// For updates check: http://douglashuck.com
--// Version 1.4

local Ent = FindMetaTable("Entity")
local Phys = FindMetaTable("PhysObj")

Ent.SetRealPos = Ent.SetRealPos or Ent.SetPos
Phys.SetRealPos = Phys.SetRealPos or Phys.SetPos

local clamp = math.Clamp
function Ent.SetPos(ent, pos)
    pos.x = clamp(pos.x, -500000, 500000)
    pos.y = clamp(pos.y, -500000, 500000)
    pos.z = clamp(pos.z, -500000, 500000)
    Ent.SetRealPos(ent, pos)
end
function Phys.SetPos(phys, pos)
    pos.x = clamp(pos.x, -500000, 500000)
    pos.y = clamp(pos.y, -500000, 500000)
    pos.z = clamp(pos.z, -500000, 500000)
    Phys.SetRealPos(phys, pos)
end