local convars = {}
local condefs = {
    whk_mediaenabled      = 1,
    whk_mediavolume      = 0.7,
    whk_mediadistance = 500,
    wyozicr_stereoenabled = 1,
    wyozicr_stereovolume = 50,
    wyozicr_reqcarstereodist = 1024,
    whk_volume = 1

}
for key,value in pairs(condefs) do
    convars[#convars + 1] = key
end

local function OnPopulateToolPanel(panel)
    panel:AddControl("ComboBox", {
        Options = { ["default"] = condefs },
        CVars = convars,
        Label = "",
        MenuButton = "1",
        Folder = "tttfun"
    })

    panel:AddControl("CheckBox", {
        Label = "Enable TV's, radios and the Cinema",
        Command = "whk_mediaenabled",
    })
    panel:AddControl("Slider", {
        Label = "TV and Radio",
        Command = "whk_mediavolume",
        Type = "Float",
        Min = "0.001",
        Max = "1.000",
    })
    panel:AddControl("Slider", {
        Label = "Cinema Volume",
        Command = "whk_volume",
        Type = "Float",
        Min = "0.001",
        Max = "1.000",
    })
    panel:AddControl("Slider", {
        Label = "Tv/Radio Distance",
        Command = "whk_mediadistance",
        Type = "Int",
        Min = "1",
        Max = "2048",
    })
    panel:AddControl( "Label", { Text = "", Description	= "" }  )

    panel:AddControl("CheckBox", {
        Label = "Enable Car Radio's",
        Command = "wyozicr_stereoenabled",
    })
    panel:AddControl("Slider", {
        Label = "Car Radio Volume",
        Command = "wyozicr_stereovolume",
        Type = "Int",
        Min = "1",
        Max = "100",
    })
    panel:AddControl("Slider", {
        Label = "Car Radio Distance",
        Command = "wyozicr_reqcarstereodist",
        Type = "Int",
        Min = "1",
        Max = "2048",
    })



end
local function OnPopulateToolMenu()
    spawnmenu.AddToolMenuOption("Options", "TTT-FUN Settings", "tttfunsettings", "Tv's and Media", "", "", OnPopulateToolPanel, {SwitchConVar = 'snap_enabled'})
end

hook.Add("PopulateToolMenu", "TTT-Fun Menu", OnPopulateToolMenu)