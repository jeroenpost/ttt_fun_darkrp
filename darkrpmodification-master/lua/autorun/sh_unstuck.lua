
if SERVER then 
	util.AddNetworkString( "StuckMessage" )
	AddCSLuaFile()
end

if CLIENT then

	local m = {}
		m[1] = "Trying to free you!"
		m[2] = "Couldn't find a place to put you! Make sure you're aiming at away from a wall, and try again in 10 seconds!"
		m[3] = "You should be unstuck! You can try again in 10 seconds!"
		m[4] = "You must be alive to use this command!"
		m[5] = "Aim away from any walls, at a spot which is clear!\nProcess begins in 3 seconds!"
		m[6] = "Cooldown period still active! Wait a bit!"
		m[7] = { "Player '", "' used the unstuck command! Investigate possible misuse." }
		m[8] = "Make sure you're aiming away from all other entities!\nie: Not trying to go through walls"
		
	net.Receive( "StuckMessage", function()
	
		local fl = net.ReadFloat()
		local ply = net.ReadEntity()

		if not ply:IsPlayer() then
			chat.AddText( Color( 200, 100, 100 ), "[UnStuck_stuck] ", Color( 255, 255, 255 ), m[fl] )
		else
			chat.AddText( Color( 255, 100, 100 ), "[UnStuck_stuck ADMIN] ", Color( 255, 255, 255 ), m[fl][1], ply:Nick(), m[fl][2] )
		end
		
	end)

end

if SERVER then

	 function SendMessage_stuck( ply, num, pent )
		net.Start( "StuckMessage" )
			net.WriteFloat( num )
			if pent then
				net.WriteEntity( pent )
			end
		net.Send( ply )
	end

	function FindNewPos_stuck( ply )
		
		local pos = ply:GetShootPos()
		local ang = ply:GetAimVector()
		local forward = ply:GetForward()
		local center = Vector( 0, 0, 30 )
		local realpos = ( (pos + center ) + (forward * 75) )
		
		local chprop = ents.Create( "prop_physics" )
		
			chprop:SetModel( "models/props_c17/oildrum001.mdl" )
			chprop:SetPos( realpos )
			chprop:SetCollisionGroup( COLLISION_GROUP_WEAPON )
			chprop:SetOwner( ply )
			chprop:SetNoDraw( true )
			chprop:DrawShadow( false )
			chprop:DropToFloor()
			chprop:Spawn()
			local p = chprop:GetPhysicsObject()
				if IsValid( p ) then
					p:EnableMotion( false )
				end
		
		local tracedata = {}
				tracedata.start = pos
				tracedata.endpos = chprop:GetPos()
				tracedata.filter = ply	
		local trace = util.TraceLine(tracedata)
			
			timer.Create( "CheckUseAblePos", 0.01, 1, function()
		
				ply:Freeze( false )
			
				if IsValid( chprop:GetGroundEntity() ) then
					local gent = chprop:GetGroundEntity()
					gent:SetCollisionGroup( COLLISION_GROUP_WEAPON )
						timer.Simple( 0.01, function()
							gent:SetCollisionGroup( COLLISION_GROUP_NONE )
						end)
				end
		
				if chprop:IsInWorld() then
					
					if trace.Entity == chprop then
						ply:SetPos( chprop:GetPos() )
						SendMessage_stuck( ply, 3 )
					else
						SendMessage_stuck( ply, 8 )
					end
				else
				
					SendMessage_stuck( ply, 2 )
				end
			
				chprop:Remove()
			
			end)
			
	end

	function UnStuck_stuck( ply )

		if ply:GetMoveType() == MOVETYPE_OBSERVER or not ply:Alive() then return end
	
		ply:Freeze( true )

		FindNewPos_stuck( ply )
	
        hook.Call("addToDamageLog", GAMEMODE, "UNSTUCK: "..ply:Nick().." Used unstuck")

        end

	hook.Add("PlayerSay", "playersaystuck", function(ply, text)

		if ( text == "!unstuck" or text == "!stuck" ) then
		
			if ply.UnStuck_stuckCooldown == nil then
				ply.UnStuck_stuckCooldown = CurTime() - 1
			end
			
			if (ply.UnStuck_stuckCooldown < CurTime()) then
		
				if ply:Alive() then 
					ply.UnStuck_stuckCooldown = CurTime() + 14
					--SendMessage_stuck( ply, 5 )
					UnStuck_stuck( ply )
				else
					SendMessage_stuck( ply, 4 )
				end
			
			else
				SendMessage_stuck( ply, 6 )
			end
			
		return ""
		end
	end)
 
end