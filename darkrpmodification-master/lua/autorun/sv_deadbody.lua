if CLIENT then return end

local noweps = {
	["arrest_stick"] = true,
	["door_ram"] = true,
	["gmod_camera"] = true,
	["gmod_tool"] = true,
	["keys"] = true,
	["lockpick"] = true,
	["med_kit"] = true,
	["pocket"] = true,
	["stunstick"] = true,
	["unarrest_stick"] = true,
	["weapon_keypadchecker"] = true,
	["weapon_physcannon"] = true,
	["weapon_physgun"] = true,
	["weaponchecker"] = true,
	["weapon_fists"] = true,
	["itemstore_pickup"] = true,
}

local bodies = {}
hook.Add( "PlayerDeath", "db.ragdoll", function( ply,int, killer )

    if IsValid(ply.hat) then
        ply.hat:Drop()
    end

    if IsValid(ply.DeadBody) then
        ply.DeadBody.DecayTime = CurTime() + 5
    end

    if killer:IsPlayer()  then

        local ent = ents.Create( "prop_physics" )
        ent:SetPos( ply:GetPos() + Vector(0,0,100) )
        ent:SetAngles( Angle( 0, ply:GetAngles().y, 0 ) )
        ent:SetModel( "models/props_c17/gravestone004a.mdl"   )

        ent:Spawn()
        ent:SetCollisionGroup(COLLISION_GROUP_DEBRIS_TRIGGER)
        ent.CollisionGroup = COLLISION_GROUP_DEBRIS_TRIGGER

        ent:SetNWBool("DeadBody", true)
        ent:SetNWString("Name",ply:Nick())
        ent:SetNWString("Killer", killer:Nick())
        ent.KillerEnt = killer
        ent.DecayTime = CurTime() + 120
        if  ply == killer then
            ent.DecayTime = CurTime() + 8
        end
        ent:SetRenderMode( 1 )
        ply.DeadBody = ent
        table.insert( bodies, ent )

        if IsValid( ply:GetRagdollEntity() ) then
            ply:GetRagdollEntity():Remove()
        end
    end
	
end )

hook.Add( "Think", "db.ragdoll", function( ply )

    if table.Count(bodies) < 1 then return end

	for k, ent in pairs( bodies ) do
		
		if not IsValid( ent ) then table.remove( bodies, k ) continue end
		
		if ent.DecayTime - CurTime() < 10 then
			ent:SetColor( Color( 255, 255, 255, ( ent.DecayTime - CurTime() ) / 10 * 255 ) )
		end
		
		if CurTime() > ent.DecayTime then
			ent:Remove()
			table.remove( bodies, k )
		end
		
	end
	
end )
