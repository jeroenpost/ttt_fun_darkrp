gb_zones = {
    ["rp_downtown_v4c_v2"]= {
        [1] = {['save_min'] = Vector(-1424,-2055,-190),['save_max'] = Vector(-2412,-1080,190)},
        [2] = {['save_min'] = Vector(1324,-380,-190),['save_max'] = Vector(690,-620,190) }
        },
    ["rp_downtown_v4c_v3_chirax"]= {
        [1] = {['save_min'] = Vector(-1424,-2055,-190),['save_max'] = Vector(-2412,-1080,190)},
        [2] = {['save_min'] = Vector(1324,-380,-190),['save_max'] = Vector(690,-620,190) }
    },
    ["rp_downtown_v4c_v2_drp_ext"]= {[1] = {['save_min'] = Vector(-1424,-2055,-190),['save_max'] = Vector(-2412,-1080,190)},[2] = {['save_min'] = Vector(1324,-380,-190),['save_max'] = Vector(690,-620,190)} }
}





local blacklist = {["swep_pickpocket"] = true}
local ents_FindInBox = ents.FindInBox
local table_HasValue = table.HasValue

local function gb_savezone(entities, zone)
    for k, v in pairs(player.GetAll()) do
        if not  v.in_savezone then  v.in_savezone = {} end
        local inside = table_HasValue(entities, v)
        if (inside and not v.in_savezone[zone]) then
            v.in_savezone[zone] = true
            v.in_the_savezone = true
            v:ChatPrint("You are in the safezone.")
            v.str_Weapons = {}
            for k2, v2 in pairs(v:GetWeapons()) do
                if (blacklist[v2:GetClass()]) then
                    table.insert(v.str_Weapons, v2:GetClass())
                    v:StripWeapon(v2:GetClass())
                end
            end
        elseif (not inside and v.in_savezone[zone]) then
            v.in_savezone[zone] = false
            v.in_the_savezone = false
            v:ChatPrint("You are out of the safezone.")
            if (v.str_Weapons) then
                for k2, v2 in pairs(v.str_Weapons) do
                    v:Give(v2)
                end
            end
        end
    end
end

timer.Create("gb_checksavezone", 2, 0, function()
    local gb_zones = gb_zones[string.lower(game.GetMap())]

    if (gb_zones) then
        for i,_ in pairs(gb_zones) do
         local entities = ents_FindInBox(gb_zones[i].save_min, gb_zones[i].save_max)
            gb_savezone(entities, i)
        end
    end
end)
