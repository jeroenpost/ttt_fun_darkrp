
AddCSLuaFile("autorun/client/cl_gb_propsave.lua")

local function saveplayeritems(ply,items)
    if table.Count(items) > 0 then
        items.lastsave = os.time()
    end
    ply:SetPData("gb_lostitems", util.TableToJSON(items))
  --  print("Saving items for "..ply:Nick())
  --  PrintTable(items)
end

local function getDarkRPCommand(entTable)
    local class = entTable.ClassName
    if entTable.DarkRPItem and entTable.DarkRPItem.name then
        for k, v in pairs(DarkRPEntities) do
            if v.ent == class and v.name == entTable.DarkRPItem.name then
                return v.cmd
            end
        end
    else
        for k, v in pairs(DarkRPEntities) do
            if v.ent == class then
                return v.cmd
            end
        end
    end
end

local function savepersonprop(ply,ent,entitytable)
    -- Check if it can be stored
    if gb_inventory.isItem( ent ) then
        local entTable = ent:GetTable()
        local items = util.JSONToTable(ply:GetPData( "gb_lostitems", "[]" ))
        local cmd = getDarkRPCommand(entTable)
        if not cmd then return end
        if items[cmd] then
            items[cmd] = items[cmd] + 1
        else
            items[cmd] = 1
        end
        saveplayeritems(ply,items)
    end
end
hook.Add("OnEntityCreated","gb_propsave_created",function(ent)
    timer.Simple(0.1,function()
        if not IsValid(ent) or (ent.propsave_claim and ent.propsave_claim > CurTime()) then return end
            if ent.FPPOwner and IsValid(ent.FPPOwner) then
                if ent.FPPOwner.canusepropsave then return end
             savepersonprop(ent.FPPOwner,ent)
            end
    end)

end)

local function onentityremoved(ent)
    if ent.FPPOwner and IsValid(ent.FPPOwner) then
        if gb_inventory.isItem( ent ) then
            local ply = ent.FPPOwner
            local items = util.JSONToTable(ply:GetPData( "gb_lostitems", "[]" ))
            local entTable = ent:GetTable()
            local cmd = getDarkRPCommand(entTable)
            if items[cmd] then
                items[cmd] = items[cmd] - 1
                if items[cmd] == 0 then
                    items[cmd] = nil
                end
            end
            saveplayeritems(ply,items)
        end
    end
end
hook.Add("EntityRemoved","gb_propsave_entremoved",function(ent)
    onentityremoved(ent)
end)

-- Copy from original darkrp
local function defaultSpawn(ply, tr, tblE)
    local ent = ents.Create(tblE.ent)
    if not ent:IsValid() then error("Entity '" .. tblE.ent .. "' does not exist or is not valid.") end
    ent.dt = ent.dt or {}
    ent.dt.owning_ent = ply
    if ent.Setowning_ent then ent:Setowning_ent(ply) end
    ent:SetPos(tr.HitPos)
    -- These must be set before :Spawn()
    ent.SID = ply.SID
    ent.allowed = tblE.allowed
    ent.DarkRPItem = tblE
    ent:Spawn()

    local phys = ent:GetPhysicsObject()
    if phys:IsValid() then phys:Wake() end

    return ent
end

concommand.Add("gb_getlostitems",function(ply)
    if not ply.canusepropsave then
        ply:ChatPrint("Sorry, you cannot use this function right now")
        return
    end

    local items = util.JSONToTable(ply:GetPData( "gb_lostitems", "[]" ))

    if table.Count(items) > 0 then
        for item,number in pairs(items) do
            for k, v in pairs(DarkRPEntities) do
                if v.cmd == item then
                    local i = 0
                    while i < number do
                        i = i + 1
                        local tblEnt = v
                        local trace = {}
                        trace.start = ply:EyePos()
                        trace.endpos = trace.start + ply:GetAimVector() * 85
                        trace.filter = ply

                        local tr = util.TraceLine(trace)

                        local ent = (tblEnt.spawn or defaultSpawn)(ply, tr, tblEnt)
                        ent.onlyremover = true
                        -- Repeat these properties to alleviate work in tblEnt.spawn:
                        ent.SID = ply.SID
                        ent.allowed = tblEnt.allowed
                        ent.DarkRPItem = tblEnt
                        ent.propsave_claim = CurTime() + 10

                        gb_inventory.storeItem( ply, ent, 1 )
                        ply:ChatPrint("Retrieved "..tblEnt.name)

                    end


                end
            end
        end
        saveplayeritems(ply,{})
        ply.canusepropsave = nil
    end
end)


local function playerspawnmessage(ply)
    if ply.gb_propsave_checked then return end
    timer.Simple(6,function()
        if not IsValid(ply) then return end
        ply.gb_propsave_checked = true
        local items = util.JSONToTable(ply:GetPData( "gb_lostitems", "[]" ))
        if table.Count(items) > 1 then
           ply:ChatPrint("You have "..(table.Count(items)-1).." lost items. Use command gb_getitems in console. Make sure you have enough space in your inventory!")
            ply.canusepropsave = true
            ply:SetNWInt("gb_lost_items_number",(table.Count(items)-1))
           ply:SetNWInt("gb_open_slots_number",gb_inventory.getNumberEmptySlots(ply))
            ply:ConCommand("gb_propsave_openwindow")
        end
    end)
end

hook.Add("PlayerInitialSpawn","gb_propsave_PlayerInitialSpawn",function(ent)
    playerspawnmessage(ent)
    ent.gb_propsave_checked = true
end)
hook.Add("PlayerSpawn","gb_propsave_PlayerSpawn",function(ent)
    if ent.gb_propsave_checked then return end
    playerspawnmessage(ent)
end)