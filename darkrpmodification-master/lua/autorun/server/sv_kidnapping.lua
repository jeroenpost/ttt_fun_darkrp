if SERVER or CLIENT then return end
--[[
util.AddNetworkString("Kidnapped")

kidnapper = {}
kidnapper.AntiSpam = CreateConVar( "kidnapmod_antispam_timer", "300", {FCVAR_SERVER_CAN_EXECUTE, FCVAR_NOTIFY, FCVAR_ARCHIVE}, "How long must you wait before you can knock the same person out again" )
kidnapper.KidnapLength = CreateConVar( "kidnapmod_knockout_time", "20", {FCVAR_SERVER_CAN_EXECUTE, FCVAR_NOTIFY, FCVAR_ARCHIVE}, "How long a KO'd person remains unconscious" )
kidnapper.WeaponCooldown = 300 -- how long to make the bludgeon unusable for after ko'ing somebody
-- if your model doesn't have a ragdoll then add it to this list
kidnapper.BrokenModels = {
	"models/player/lordvipes/bl_clance/crimsonlanceplayer.mdl",
   "models/player_eevee.mdl" ,
"models/player_espeon.mdl" ,
"models/player_umbreon.mdl" ,
"models/player_flareon.mdl" ,
"models/player_glaceon.mdl" ,
"models/player_leafeon.mdl" ,
"models/player_jolteon.mdl" ,
"models/player_vaporeon.mdl" ,
"models/player_sylveon.mdl" ,
 "models/player/pokemon/buizel.mdl",
 "models/player/blockdude.mdl",
    "models/player/eggman_npc.mdl",
    "models/conex/stickguy/stickguy.mdl",
    "models/player/bobert/joker.mdl",
    "models/humans/mafia/male_02.mdl","models/humans/mafia/male_03.mdl"
    ,"models/humans/mafia/male_04.mdl","models/humans/mafia/male_05.mdl","models/humans/mafia/male_06.mdl",
    "models/humans/mafia/male_07.mdl"
    ,"models/humans/mafia/male_08.mdl","models/humans/mafia/male_09.mdl",
    "models/player/sanic.mdl"
    ,




}

function kidnapper:CanKnockout( ply )

if ply:IsValid() and ply:Alive() and !ply:InVehicle() and ply:GetMoveType() != MOVETYPE_NOCLIP and (ply.LastKidnapped and ply.LastKidnapped < CurTime()) or not ply.LastKidnapped then return true end
return false
end

function kidnapper:IsKnockedOut( ply )
return ply.KnockedOut
end

function kidnapper:IsAdmin( ply )
if !ply:IsValid() then return end
return (ply:IsUserGroup("superadmin") or ply:IsSuperAdmin() or ply:IsUserGroup("admin") or ply:IsAdmin() or gb.is_admin(ply) )

end

function kidnapper:SaveShit()

end

function kidnapper:SavePlayerGear( ply )
	if !ply:IsValid() then return end
	local plyinfo = {}

	plyinfo.health = ply:Health()
	plyinfo.armor = ply:Armor()
	if ply:GetActiveWeapon():IsValid() then
		plyinfo.curweapon = ply:GetActiveWeapon():GetClass()
	end

	local gunz = ply:GetWeapons()
	plyinfo.gunz = {}
	for _, weapon in ipairs( gunz ) do
		local printname = weapon:GetClass()
		plyinfo.gunz[ printname ] = {}
		plyinfo.gunz[ printname ].clip1 = weapon:Clip1()
		plyinfo.gunz[ printname ].clip2 = weapon:Clip2()
		plyinfo.gunz[ printname ].ammo1 = ply:GetAmmoCount( weapon:GetPrimaryAmmoType() )
		plyinfo.gunz[ printname ].ammo2 = ply:GetAmmoCount( weapon:GetSecondaryAmmoType() )
	end
	ply.KSpawnInfo = plyinfo
end

function kidnapper:RestorePlayerGear( ply )
	if !ply:IsValid() or !ply.KSpawnInfo then return end

	ply:StripAmmo()
	ply:StripWeapons()

	ply:SetHealth( ply.KSpawnInfo.health )
	ply:SetArmor( ply.KSpawnInfo.armor )

    ply:Give("weapon_physgun")

	for swep, data in pairs( ply.KSpawnInfo.gunz ) do
		ply:Give( swep )
		local weapon = ply:GetWeapon( swep )
		weapon:SetClip1( data.clip1 )
		weapon:SetClip2( data.clip2 )
		ply:SetAmmo( data.ammo1, weapon:GetPrimaryAmmoType() )
		ply:SetAmmo( data.ammo2, weapon:GetSecondaryAmmoType() )
	end

	if ply.KSpawnInfo.curweapon then
		ply:SelectWeapon( ply.KSpawnInfo.curweapon )
	end

	ply.KSpawnInfo = nil

end

function kidnapper:DeleteGlitchedRagdolls()
	for k, v in pairs(ents.GetAll()) do
		if  v.KOedplayer then
			if !v.Ownedby:IsValid() then v:Remove() end
			if v.Ownedby:GetObserverTarget() != v then v:Remove() end
		end
	end

end


function kidnapper:Knockout( ply, attacker )
if !ply:IsValid() or !ply:Alive() then return end
local attacker = attacker or game.GetWorld()
local kotime = kidnapper.KidnapLength:GetInt()
kidnapper:DeleteGlitchedRagdolls()
attacker.lastkidnap = CurTime() + kidnapper.WeaponCooldown
ply.KnockedOut = true

kidnapper:SavePlayerGear( ply )

local ragdoll = ents.Create( "prop_physics" )
ragdoll.Ownedby = ply

ragdoll.KOedplayer = true
ragdoll:SetPos( ply:GetPos() )
local velocity = ply:GetVelocity()
ragdoll:SetAngles( ply:GetAngles() )
--if table.HasValue( kidnapper.BrokenModels, ply:GetModel() ) then
	ragdoll:SetModel("models/maxofs2d/companion_doll.mdl")
--else
--ragdoll:SetModel( ply:GetModel() )
--end
ragdoll.GetPlayerColor = function() return ragdoll.Ownedby:GetPlayerColor() end
ragdoll:Spawn()
ragdoll:Activate()
local mats = ply:GetMaterials()
if #mats == 1 then
    ragdoll:SetMaterial(mats[1])
else
    for k,v in pairs(mats) do
        if string.find(v,"body",1,true) or string.find(v,"sheet",1,true) then
            ragdoll:SetMaterial(mats[k])
        end
    end
end

local limb = 1


ply:Lock()

net.Start("Kidnapped")
net.WriteUInt( kotime, 8 )
if attacker:IsValid() then
	net.WriteEntity( attacker )
else
	net.WriteEntity( game.GetWorld() )
end
net.Send( ply )

ply.LastKidnapped = CurTime() + kidnapper.AntiSpam:GetInt()

ragdoll.PhysgunPickup = true
ragdoll.CanTool = false
ragdoll:SetCollisionGroup(COLLISION_GROUP_WEAPON)

ply:Spectate( OBS_MODE_CHASE )
ply:SpectateEntity( ragdoll )
ply:StripWeapons()

timer.Simple(kotime, function()
	if !ply:IsValid() then return end
	if !ragdoll:IsValid() then ply:UnSpectate() ply:UnLock() ply:Spawn() return end -- something fucked the ragdoll, gotta put them back at spawn

--	ply:SetParent()
	ply:UnSpectate()
	ply:Spawn()
	ply:SetPos(ragdoll:GetPos())
	ragdoll:Remove()
	ply:UnLock()
	kidnapper:RestorePlayerGear( ply )


end)

end

-- wat
/*
concommand.Add( "kidnapmod_antispam_timer", function( ply, cmd, args )
	local len = tonumber(args[1]) or 120
	if not kidnapper:IsAdmin( ply ) then return end
	if not isnumber( len ) then return end
	print("Kidnapmod: changed antispam time to "..len)
	kidnapper.AntiSpam = len
end )

concommand.Add( "kidnapmod_knockout_time", function( ply, cmd, args )
	local len = tonumber(args[1]) or 20
	if not kidnapper:IsAdmin( ply ) then return end
	if not isnumber( len ) then return end
	print("Kidnapmod: changed knockout time to "..len)
	kidnapper.KidnapLength:GetInt() = len
end )
*/


]]--