

if SERVER then
    local mc, mp = math.Clamp, math.pow
    local bn, ba, bo = bit.bnot, bit.band, bit.bor
    local sl, ls = string.lower, {}
    local lp, ft, ct, gf = LocalPlayer, FrameTime, CurTime, {}

    print("Loading Bhop script");

    hook.Remove("Move","gb_bhopmove")





    local function gb_bunnyhop_move( ply, data )

        if not IsValid( ply )  then return end
        local hop = ply:GetFunVar("bunnyhop")
        if  hop and hop > 0 then
            if lp and ply != lp() then return end

            local st, og = 1, ply:IsOnGround()
            if og and not gf[ ply ] then
                gf[ ply ] = 0
            elseif og and gf[ ply ] then
                gf[ ply ] = gf[ ply ] + 1
                if gf[ ply ] > 12 then
                    ply:SetDuckSpeed( 0.4 )
                    ply:SetUnDuckSpeed( 0.2 )
                end
            elseif not og then
                gf[ ply ] = 0
                ply:SetDuckSpeed( 0 )
                ply:SetUnDuckSpeed( 0 )
            end


            --  if og or not ply:Alive() then return end

            local aa, mv = 500, 100
            local aim = data:GetMoveAngles()
            local forward, right = aim:Forward(), aim:Right()
            local fmove = data:GetForwardSpeed()
            local smove = data:GetSideSpeed()


            if data:KeyDown( IN_MOVERIGHT ) then smove = smove + 500 end
            if data:KeyDown( IN_MOVELEFT ) then smove = smove - 500 end


            forward.z, right.z = 0,0
            forward:Normalize()
            right:Normalize()

            local wishvel = forward * fmove + right * smove
            wishvel.z = 0

            local wishspeed = wishvel:Length()
            if wishspeed > data:GetMaxSpeed() then
                wishvel = wishvel * (data:GetMaxSpeed() / wishspeed)
                wishspeed = data:GetMaxSpeed()
            end

            local wishspd = wishspeed
            wishspd = mc( wishspd, 0, mv )

            local wishdir = wishvel:GetNormal()
            local current = data:GetVelocity():Dot( wishdir )

            local addspeed = wishspd - current
            if addspeed <= 0 then return end

            local accelspeed = aa * ft() * wishspeed
            if accelspeed > addspeed then
                accelspeed = addspeed
            end

            local vel = data:GetVelocity()
            vel = vel + (wishdir * accelspeed)



            data:SetVelocity( vel )

            return false
        end
    end
    hook.Add("Move", "gb_bhopmove", gb_bunnyhop_move)



    local function AutoHop( ply, data )
        if lp and ply != lp() then return end

        if ply.bunnyhop then

            local ButtonData = data:GetButtons()
            if ba( ButtonData, IN_JUMP ) > 0 then
                if ply:WaterLevel() < 2 and ply:GetMoveType() != MOVETYPE_LADDER and not ply:IsOnGround() then
                data:SetButtons( ba( ButtonData, bn( IN_JUMP ) ) )
                end

            end
        end
    end
    hook.Add( "SetupMove", "gb_AutoHop", AutoHop )

    local PlayerJumps = {}
    local P1, P2 = 220, 270

    local function PlayerGround( ply, bWater )
        if lp and ply != lp() then return end
        if ply.bunnyhop then
            ply:SetJumpPower( P1  )
            timer.Simple( 0.3, function() if not IsValid( ply ) or not ply.SetJumpPower then return end ply:SetJumpPower( P2  ) end )


            if PlayerJumps[ ply ] then
                PlayerJumps[ ply ] = PlayerJumps[ ply ] + 1
            end
        end
    end
    hook.Add( "OnPlayerHitGround", "gb_HitGround_bhop", PlayerGround )
end

if CLIENT then
    surface.CreateFont( "MyFont", {
        font 		= "Arial",
        size 		= 30,
        weight 		= 500,
        blursize 	= 0,
        scanlines 	= 0,
        antialias 	= true,
        underline 	= false,
        italic 		= false,
        strikeout 	= false,
        symbol 		= false,
        rotary 		= false,
        shadow 		= false,
        additive 	= false,
        outline 	= false
    })
    hook.Add("HUDPaint","gb_hubpaint_bunnyhop",function()

        local ply = LocalPlayer()
        if not ply then return end
        local VEL = math.Round(LocalPlayer():GetVelocity():Length())
        local hop =  ply:GetFunVar("bunnyhop")
        if hop and hop > 0 then

            surface.SetTextColor( 255, 255, 255, 255)
            surface.SetTextPos( 20, 20 )
            surface.SetFont( "MyFont" )
            surface.DrawText( "Velocity: ".. VEL)
            surface.SetTextPos( 20, 50 )
            surface.DrawText( "Time left: ".. hop)
        end
    end)
end