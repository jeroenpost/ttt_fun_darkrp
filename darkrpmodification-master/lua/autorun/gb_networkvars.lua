
if SERVER then
    AddCSLuaFile()
    util.AddNetworkString( "gb_fun_networked_vars" )

        -- Networked Vars
        local meta = FindMetaTable("Player")
        function meta:GetFunVar(var)
            if not self.funvars then self.funvars = {} end
            if not self.funvars[var] then return 0 end
            return self.funvars[var]
        end


        function meta:SetFunVar(name,vari)
            if not self.funvars then self.funvars = {} end
            if not vari or not name then return end
            net.Start( "gb_fun_networked_vars" )
                net.WriteString( "ply" )
                net.WriteString( name )
                net.WriteFloat( vari )
            net.Send( self )
            self.funvars[name] = vari
        end

    local meta2 = FindMetaTable("Entity")
    function meta2:GetFunVar(var)
        if not self.funvars then self.funvars = {} end
        if not self.funvars[var] then return 0 end
        return self.funvars[var]
    end


    function meta2:SetFunVar(name,vari)
        if not self.funvars then self.funvars = {} end
        if not vari or not name then return end
        net.Start( "gb_fun_networked_vars" )
        net.WriteString( "ent" )
        net.WriteString( name )
        net.WriteFloat( vari )
        net.Send( self )
        self.funvars[name] = vari
    end
end


if CLIENT then
    local gb_localstorage = {}
    local gb_localstorage_ent = {}
    net.Receive( "gb_fun_networked_vars", function( len )
        local type net.ReadString( )
        local name = net.ReadString()
        local var = net.ReadFloat( )
        if type == "ent" then
            gb_localstorage_ent[name] = var
        else
            gb_localstorage[name] = var
        end
    end)

    local meta = FindMetaTable("Player")
    function meta:GetFunVar(var)
        if not gb_localstorage[var] then return 0 end
        return gb_localstorage[var]
    end

    local meta2 = FindMetaTable("Entity")
    function meta2:GetFunVar(var)
        if not gb_localstorage_ent[var] then return 0 end
        return gb_localstorage_ent[var]
    end
end