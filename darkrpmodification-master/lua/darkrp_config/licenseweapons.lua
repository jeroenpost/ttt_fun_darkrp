/*---------------------------------------------------------------------------
License weapons
Add weapons that do NOT require a special license here
ALL other weapons will require a license

Note: this only works if the license setting is enabled
---------------------------------------------------------------------------*/
GM.NoLicense["weapon_physcannon"] = true
GM.NoLicense["weapon_physgun"]    = true
GM.NoLicense["weapon_bugbait"]    = true
GM.NoLicense["gmod_tool"]         = true
GM.NoLicense["gmod_camera"]       = true
GM.NoLicense["weapon_popcorn"]       = true
GM.NoLicense["diamond_pickaxe"]       = true
GM.NoLicense["ruby_pickaxe"]       = true
GM.NoLicense["emerald_pickaxe"]       = true
GM.NoLicense["sapphire_pickaxe"]       = true
GM.NoLicense["gold_pickaxe"]       = true
GM.NoLicense["iron_pickaxe"]       = true
GM.NoLicense["keys"]       = true
GM.NoLicense["hunter_knife"] = true
GM.NoLicense["skeleton_swep"] = true
GM.NoLicense["superman_fly"] = true
GM.NoLicense["superman_lasereyes"] = true
GM.NoLicense["swep_pickpocket"] = true
GM.NoLicense["unarrest_stick"] = true
GM.NoLicense["weapon_keypadchecker"] = true
GM.NoLicense["weapon_physcannon"] = true
GM.NoLicense["weapon_physgun"] = true
GM.NoLicense["weaponchecker"] = true
GM.NoLicense["angry_hobo"] = true
GM.NoLicense["dnascanner"] = true
GM.NoLicense["dog_bite"] = true
GM.NoLicense["fists"] = true
GM.NoLicense["fists_strong"] = true
GM.NoLicense["hunter_knife"] = true
GM.NoLicense["skeleton_swep"] = true
GM.NoLicense["superman_fly"] = true
GM.NoLicense["superman_lasereyes"] = true
GM.NoLicense["swep_pickpocket"] = true
GM.NoLicense["tail_whip"] = true
GM.NoLicense["vaporeon_water"] = true
GM.NoLicense["climb_swep2"] = true
GM.NoLicense["handcuffs"] = true
GM.NoLicense["stungun"] = true
GM.NoLicense["realistic_hook"] = true
GM.NoLicense["carjack"] = true
GM.NoLicense["mod_stick"] = true
GM.NoLicense["weapon_hack_phone"] = true
GM.NoLicense["flight"] = true
GM.NoLicense["riot_shield"] = true
GM.NoLicense["itemstore_pickup"] = true