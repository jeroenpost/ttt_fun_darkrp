AddCSLuaFile()

if CLIENT then
    SWEP.PrintName = "Inventory Checker"
    SWEP.Slot = 1
    SWEP.SlotPos = 10
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false
end

SWEP.Author = "DarkRP Developers"
SWEP.Instructions = "Leftclick to see the stufzies"
SWEP.Contact = ""
SWEP.Purpose = ""

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false
SWEP.AnimPrefix  = "rpg"

SWEP.Spawnable = true
SWEP.AdminOnly = true
SWEP.Category = "DarkRP (Utility)"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""

function SWEP:Initialize()
    self:SetHoldType("normal")
end

function SWEP:Deploy()
    return true
end

function SWEP:DrawWorldModel() end

function SWEP:PreDrawViewModel(vm)
    return true
end

function SWEP:PrimaryAttack()

    self:SetNextPrimaryFire(CurTime() + 0.3)

    self:GetOwner():LagCompensation(true)
    local trace = self:GetOwner():GetEyeTrace()
    self:GetOwner():LagCompensation(false)

    if not IsValid(trace.Entity) or not trace.Entity:IsPlayer() or trace.Entity:GetPos():Distance(self:GetOwner():GetPos()) > 100 then
        return
    end

    self:EmitSound("npc/combine_soldier/gear5.wav", 50, 100)


    if CLIENT then return end
   -- trace.Entity = self.Owner

    local result = {}
    local inventory = trace.Entity:getInventory()
    for k, items in pairs( inventory ) do
        if not istable(items) then continue end
        for v, item in pairs( items ) do
            if not item then continue end
                if item.data and item.data.WeaponClass then
                    table.insert(result, item.data.WeaponClass)
                elseif item.data and item.data.vrondakisName then
                    table.insert(result, item.data.vrondakisName)
                elseif item.data and item.data.ammotype then
                    table.insert(result, item.data.ammotype.." ammo")
                else
                    table.insert(result, item.class)
                end
            end
        end




    result = table.concat(result, ", ")

    if result == "" then
        self:GetOwner():ChatPrint("Inventory for "..trace.Entity:Nick().." is empty")
    else
        self:GetOwner():ChatPrint("Inventory for "..trace.Entity:Nick()..":")
        if string.len(result) >= 126 then
            local amount = math.ceil(string.len(result) / 126)
            for i = 1, amount, 1 do
                self:GetOwner():ChatPrint(string.sub(result, (i-1) * 126, i * 126 - 1))
            end
        else
            self:GetOwner():ChatPrint(result)
        end
    end
end
