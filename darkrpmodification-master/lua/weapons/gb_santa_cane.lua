SWEP.PrintName = "Santa's Present Stick"

SWEP.Purpose = "Giving presents to players"
SWEP.Instructions = "Give it"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.UseHands = true
SWEP.ViewModel			= "models/weapons/c_crowbar.mdl"
SWEP.WorldModel			= "models/weapons/w_crowbar.mdl"
SWEP.CanDrop = false

SWEP.Primary.Clipsize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"
SWEP.Primary.Automatic = false

SWEP.Secondary.Clipsize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.Slot               = 1
SWEP.SlotPos 			= 10


function SWEP:Initalize()
    self:SetNWString("presenttype","xp")
    self.Weapon:SetMaterial("camos/crowbar_christmas")
    self.Weapon:SetSubMaterial(1,"camos/crowbar_christmas")
    self.Weapon:SetSubMaterial(2,"camos/crowbar_christmas_head")
end

function SWEP:canAffort(ply,money)
    if not ply:canAfford(money) then
    self:printmessage(self.Owner, "You cant affort this")
    return false
    end
    return true
end

SWEP.nextpresent = 0
function SWEP:PrimaryAttack()
    if  not SERVER then return end
    if self.nextpresent > CurTime() then
        self:printmessage(self.Owner, math.floor(self.nextpresent - CurTime()).." seconds left")
        return end
    self.nextpresent = CurTime() + 5
    local tr = self.Owner:GetEyeTrace()



    local mode = self:GetNWString("presenttype","xp")
    local ply = self.Owner
    local affort = false

    if mode == "xp"             and self:canAffort(ply,2500) then ply:addMoney(-2500) self:printmessage(ply,"You bought XP for $2500") affort=true  end
    if mode == "money"          and self:canAffort(ply,5500) then ply:addMoney(-5500) self:printmessage(ply,"You bought Money for $5000") affort=true end
    if mode == "weapon_ak"      and self:canAffort(ply,2000) then ply:addMoney(-2000) self:printmessage(ply,"You bought an AK for $2000") affort=true end
    if mode == "weapon_elites"  and self:canAffort(ply,2000) then ply:addMoney(-2000) self:printmessage(ply,"You bought Elites for $1250") affort=true end
    if mode == "armor"          and self:canAffort(ply,2000) then ply:addMoney(-2000) self:printmessage(ply,"You bought armor for $2000") affort=true end
    if mode == "boom"           and self:canAffort(ply,15000) then ply:addMoney(-15000) self:printmessage(ply,"You bought a BOOM for $15000" ) affort=true end
    if mode == "mushroom"       and self:canAffort(ply,3000) then ply:addMoney(-3000) self:printmessage(ply,"You bought a mushroom for $3000") affort=true end
   -- if mode == "vip"            and self:canAffort(ply,100000) then  ply:addMoney(-100000) self:printmessage(ply,"You bought Temparaty VIP++ for $100000") affort=true end

    if affort then
        local spos = tr.HitPos + self.Owner:GetAimVector() * -46

        local present = ents.Create("gb_christmas_present")
        present:SetPos( spos )
        present:SetAngles(Angle(0,-90,0))
        present:Spawn()
        present.Owner = self.Owner
        present.presenttype = mode
    end

end

SWEP.nextswitch = 0
function SWEP:SecondaryAttack()
    if self.nextswitch > CurTime() then return end
    self.nextswitch = CurTime() + 0.3

    local mode = self:GetNWString("presenttype","xp")

    if mode == "xp" then self:SetNWString("presenttype","money") end
    if mode == "money" then self:SetNWString("presenttype","weapon_ak") end
    if mode == "weapon_ak" then self:SetNWString("presenttype","weapon_elites") end
    if mode == "weapon_elites" then self:SetNWString("presenttype","armor") end
    if mode == "armor" then self:SetNWString("presenttype","boom") end
    if mode == "boom" then self:SetNWString("presenttype","mushroom") end
    if mode == "mushroom" then self:SetNWString("presenttype","xp") end
   -- if mode == "vip" then self:SetNWString("presenttype","xp") end

end

SWEP.NextThink2 = 0
SWEP.thinks = 0
function SWEP:Think()

    if CLIENT and  IsValid(self) and self.NextThink2 < CurTime() and IsValid(self.Owner) and IsValid(self.Owner:GetViewModel()) then
        if self.thinks > 100 then
            self.NextThink2 = CurTime() + 3
        end
        self.thinks = self.thinks + 1
        self.Owner:GetViewModel():SetMaterial("camos/crowbar_christmas")
        self.Owner:GetViewModel():SetSubMaterial(2,"camos/crowbar_christmas")
        self.Owner:GetViewModel():SetSubMaterial(3,"camos/crowbar_christmas_head")
    end
end

function SWEP:Deploy()
    self:SetMaterial("camos/crowbar_christmas")
end

function SWEP:printmessage(ply,message)
    ply:PrintMessage( HUD_PRINTCENTER, message )
    ply:ChatPrint(message)
end


function SWEP:Holster() self:EndSound() return true end
function SWEP:OwnerChanged() self:EndSound() end

function SWEP:EndSound()
    if not IsValid(self.Owner) then return end
    self.Weapon:SetNWBool ("on", false)
    self.on = false
    timer.Destroy("sanicPlay"..self.EntIndex())
    gb_StopSoundFromServer("sanic_theme"..self.Owner:SteamID())
end

SWEP.SoundObject = false
SWEP.LastFrame = false
SWEP.RestartDelay = 0
SWEP.SoundPlaying = false
SWEP.LastSoundRelease = 0
SWEP.on = false

SWEP.nextreload = 0
function SWEP:Reload()
    if self.nextreload > CurTime() then return end
    self.nextreload = CurTime() + 0.5
    if self.on then
        self:EndSound()
    else
        self:CreateSound()
    end
end


SWEP.NextSoundCheck = 0
function SWEP:CreateSound()
    if not self.on and not self.Weapon:GetNWBool("on") and ( not self.NextSoundCheck2 or self.NextSoundCheck2 < CurTime()) then
        self.NextSoundCheck2 = CurTime() + 0.5
        self.on = true

        gb_PlaySoundFromServer(gb_config.websounds.."santa_sponge.mp3", self.Owner,false,"sanic_theme"..self.Owner:SteamID());
        self.Weapon:SetNWBool ("on", true)
        timer.Simple(26,function()
            if IsValid(self) then self:EndSound() end
        end)
    end
end



if CLIENT then

    function SWEP:DrawHUD()
        local mode = self:GetNWString("presenttype", "xp")

        local message = ""
        if mode == "xp" then message = "25*level XP - $2500" end
        if mode == "money" then  message = "$5000 money - $5500"  end
        if mode == "weapon_ak" then message = "Weapon AK47 - $2000" end
        if mode == "weapon_elites" then message = "Weapon Elites - $1250" end
        if mode == "armor" then message = "50 Armor - $2000" end
        if mode == "boom" then message = "Explosion - $15000" end
        if mode == "mushroom" then message = "Mushroom - $3000" end
       -- if mode == "vip" then message = "Temporary VIP++ - $100000" end

        local shotttext = "Present: "..message.."\nRMB to change - R to laugh"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 255,30,30, 200 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER );

        self.BaseClass.DrawHUD(self)
    end
end
