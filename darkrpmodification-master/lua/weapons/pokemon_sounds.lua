
if CLIENT then
    SWEP.PrintName = "Pokemon Sound"
    SWEP.Slot = 1
    SWEP.SlotPos = 3
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false
    SWEP.DisplayAlpha = 0
SWEP.BobScale = 0
SWEP.SwayScale = 0
end

SWEP.Base = "weapon_cs_base2"

SWEP.Author = "GreenBlack"

SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.IconLetter = ""

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false

SWEP.Spawnable = true
SWEP.AdminOnly = true
SWEP.Category = "GreenBlack"

SWEP.NextStrike = 0

SWEP.ViewModel			= "models/weapons/c_arms_citizen.mdl"
SWEP.WorldModel			= 'models/props_wasteland/panel_leverHandle001a.mdl'

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""

SWEP.ArrestSoundTime = 0
SWEP.ArrestTargetTime = 0

SWEP.ArrestTime = 3
SWEP.CanArrestFromAnyAngle = false
SWEP.RightClickSwitchToUnarrestBaton = true

SWEP.Handcuffs = true -- identifier, instead of calling GetClass (cheaper this way)
SWEP.UseHands = true
SWEP.ArrestCooldown = 10 -- change this value to add a cooldown for arresting after a successful arrest
SWEP.ArrestWait = 0 -- don't change this
SWEP.AllowDrop = false
SWEP.CanDrop = true


function SWEP:SecondaryAttack()
    self:SetNextSecondaryFire(CurTime() + 3)
    --if SERVER then
        local soundy = self.Owner:GetModel()

        if soundy == "models/player_eevee.mdl" then
           self:EmitSound("pokemon/Eevee.mp3")
        end
        if soundy == "models/player_espeon.mdl" then
            self:EmitSound("pokemon/Espeon.mp3")
        end
        if soundy == "models/player_flareon.mdl" then
            self:EmitSound("pokemon/Flareon.mp3")
        end
        if soundy == "models/player_glaceon.mdl" then
            self:EmitSound("pokemon/Glaceon.mp3")
        end
        if soundy == "models/player_jolteon.mdl" then
            self:EmitSound("pokemon/Jolteon.mp3")
        end
        if soundy == "models/player_leafeon.mdl" then
            self:EmitSound("pokemon/leafeon.mp3")
        end
        if soundy == "models/player_sylveon.mdl" then
            self:EmitSound("pokemon/Sylveon.mp3")
        end
        if soundy == "models/player_umbreon.mdl" then
            self:EmitSound("pokemon/Umbreon.mp3")
        end
        if soundy == "models/player_vaporeon.mdl" then
            self:EmitSound("pokemon/Vaporeon.mp3")
        end

    if soundy == "models/player/pokemon/buizel.mdl" then
        self:EmitSound("pokemon/Buizel.mp3")
    end
    if soundy == "models/player/slow_alien.mdl" then
        self:EmitSound(table.Random({"npc/antlion/attack_double1.wav",
            "npc/antlion/attack_double2.wav",
            "npc/antlion/attack_double3.wav",
            "npc/antlion/attack_single1.wav",
            "npc/antlion/attack_single2.wav",
            "npc/antlion/attack_single3.wav"}),100,100)
    end

    -- end
end
