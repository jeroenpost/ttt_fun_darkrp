SWEP.doesdamage= true
SWEP.PrintName			= "Alien beam"

SWEP.SlotPos = 6 //			= 1
SWEP.Slot = 2

SWEP.Base				= "pokemon_sounds"

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType = "normal"
SWEP.ViewModelFOV = 80
SWEP.ViewModelFlip = true
SWEP.ViewModel			= "models/weapons/c_arms_citizen.mdl"
SWEP.WorldModel			= 'models/props_wasteland/panel_leverHandle001a.mdl'
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBonescales = {}


SWEP.Primary.Sound			= Sound("default.zoom")
SWEP.Primary.SoundLevel			= 40
SWEP.Primary.Recoil			= 2
SWEP.Primary.Damage			= 15
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.015
SWEP.Primary.ClipSize		= 50
SWEP.Primary.Delay			= 1.2
SWEP.Primary.DefaultClip	= 50
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "battery"
SWEP.InitialHoldtype = "pistol"
SWEP.InHoldtype = "pistol"
SWEP.CantSilence = true
SWEP.NoBoltAnim = true
SWEP.ChamberAmount = 2
SWEP.SprintAndShoot = true
SWEP.HeadshotMultiplier = 1.2
SWEP.Category = "GreenBlack"
-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.AnimPrefix = ""

SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0.5
SWEP.IncAmmoPerc = 0.75 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.FOVZoom = 85

SWEP.Primary.Sound2 = { "ambient/levels/citadel/portal_beam_shoot2.wav","ambient/levels/citadel/portal_beam_shoot3.wav","ambient/levels/citadel/portal_beam_shoot6.wav"}



SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)


SWEP.HoldType =  "normal"
SWEP.HeadshotMultiplier = 1

function SWEP:NormalPrimaryAttack(worldsnd)

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage,  self.Primary.NumShots, self.Primary.Cone )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:Reload()
    self.Weapon:SetNextPrimaryFire( CurTime() + 3)

end

function SWEP:PrimaryAttack()

    if not self:CanPrimaryAttack() then return end
    if self.Owner:Team() != TEAM_POKEMON && self.Owner:Team() != TEAM_ALIEN && self.Owner:Team() != TEAM_ALIENQUEEN then
    self.Owner:ChatPrint("You cannot use alien powers as a non-aliejn")
        return false
    end

    self:NormalPrimaryAttack()

    if self:Clip1() > 0 and SERVER then
        self.Owner:EmitSound( table.Random( self.Primary.Sound2 ),100,100)
    end

end



SWEP.LaserColor = Color(80,80,80,255)
function SWEP:ShootBullet( damage, num_bullets, aimcone )

    local bullet = {}
    bullet.Num 		= num_bullets
    bullet.Src 		= self.Owner:GetShootPos()
    bullet.Dir 		= self.Owner:GetAimVector()
    bullet.Spread 	= Vector( 0.001, 0.001, 0 )
    bullet.Tracer	= 1
    bullet.Force	= 10
    bullet.Damage	= damage
    bullet.AmmoType = "Pistol"
    bullet.HullSize = 2
    bullet.TracerName = "LaserTracer_thick"

    self.Owner:FireBullets( bullet )
    self:ShootEffects()

end

function SWEP:SecondaryAttack()
end



function SWEP:DrawWorldModel( )

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end
    return false
end