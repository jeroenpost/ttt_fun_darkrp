
if ( SERVER ) then

    AddCSLuaFile(  )


end

if ( CLIENT ) then

    SWEP.PrintName			= "FireBall"

    SWEP.DrawCrosshair 		= true
    SWEP.DrawAmmo			= false
    SWEP.Slot				= 1
    SWEP.SlotPos			= 7
    SWEP.ViewModelFOV		= 80
    SWEP.IconLetter			= "x"

end


SWEP.IgniteProps = {}

SWEP.MakeFireBall = 0
SWEP.MakeFireBallDel = CurTime()

SWEP.PhysBall = NULL

SWEP.BallPos   = 0
SWEP.SecDelay  = CurTime()
SWEP.PrimDelay = CurTime()
SWEP.ReloadDelay = CurTime()

SWEP.FireEffect = CurTime()
SWEP.FireDie    = 0

SWEP.ShootFireBall = 0

SWEP.Ball = NULL

SWEP.ColorR = 255
SWEP.ColorG = 255
SWEP.ColorB = 5

SWEP.PlyHealth = NULL
SWEP.WepUse = 1

SWEP.SecAttack = 0

SWEP.FartTrail = CurTime()

-----------------------Main functions----------------------------
function SWEP:Precache()
    util.PrecacheSound("ambient/fire/ignite.wav")
    util.PrecacheSound("ambient/fire/mtov_flame2.wav")
    util.PrecacheSound("fireball/fireball.wav")
end
--------------------------
function SWEP:Initialize()

    if (SERVER) then
        self:SetWeaponHoldType( "melee" )
    end

end
-----------------------
function SWEP:Think()


    if self.WepUse == 1 then

        self.Weapon:EmitSound("ambient/fire/ignite.wav")


        self.WepUse = 0
    end

    ------------GUIDING FIREBALL
    if self.Owner:KeyDown(IN_USE) == true and  self.MakeFireBall == 1 then

        if (SERVER) then

            local trace = {}
            trace.start = self.Owner:GetShootPos()
            trace.endpos = trace.start + (self.Owner:GetAimVector() * 99999)
            trace.filter = { self.Owner, self.Weaponm,}
            local tr = util.TraceLine( trace )
            local NotFireball = tr.Entity

            if self.Ball ~= NotFireball then


                local tr = util.GetPlayerTrace( self.Owner )
                local trace = util.TraceLine( tr )
                if (!trace.Hit) then return end


                local Vec = trace.HitPos - self.Ball:GetPos()
                Vec:Normalize()

                local speed = self.Ball:GetPhysicsObject():GetVelocity()
                self.Ball:GetPhysicsObject():SetVelocity( (Vec *20) + speed )
            end
        end
    end
    ------------------------GUIDING FIRE BALL END

    if self.ShootFireBall == 1 and self.MakeFireBallDel < CurTime() then

        self.ShootFireBall = 0
        self.MakeFireBall = 1
        local fball = ents.Create("prop_physics")
        self.Ball = fball
        self.Ball:SetModel("models/dav0r/hoverball.mdl")
        self.Ball:SetAngles(self.Owner:EyeAngles())
        self.Ball:SetPos(self.Owner:GetShootPos()+(self.Owner:GetAimVector()*20))
        self.Ball:SetOwner(self.Owner)
        self.Ball:SetPhysicsAttacker(self.Owner)
        self.Ball:SetMaterial("camos/camo24")
        self.Ball:Spawn()
        self.PhysBall = self.Ball:GetPhysicsObject()
        self.Ball:EmitSound("fireball/fireball.wav")
        self.PhysBall:SetMass(10)
        self.PhysBall:ApplyForceCenter(self.Owner:GetAimVector() * 10000)
        self.Ball:Fire("kill", "", 2)
        self.FireEffect  = CurTime()+2
    end
    ---------
    if (self.FireEffect - 1.8) < CurTime() and self.Ball ~= nil and self.Ball ~= NULL then self.Ball:Ignite( 20, 100 ) end

    if self.FireEffect > CurTime() then
        if not self.PhysBall then return end
        self.BallPos = self.PhysBall:GetPos()
        local effectdata = EffectData()
        effectdata:SetOrigin( self.BallPos )
        util.Effect( "FireBall", effectdata )

        --self.Ball:SetColor(Color(255, 153, 0, 0))
        IgniteProps = {} --ents.FindInSphere( self.Ball:GetPos(), 50)

    end
    -------------
    if CurTime() > (self.FireEffect-0.3) and self.MakeFireBall==1 and self.Ball:IsValid() then self.Ball:Ignite( 10, 100 ) end
    if CurTime() > (self.FireEffect-0.2) and self.MakeFireBall==1 and self.Ball:IsValid() then self.FireDie = self.FireDie+1 end

    if self.FireDie == 1  and self.Ball ~= nil and self.Ball ~= NULL then

        self.MakeFireBall = 0
        self.FireDie = 20
        self.Ball:EmitSound("ambient/fire/ignite.wav")

        local effectdata = EffectData()
        effectdata:SetOrigin( self.BallPos )
        util.Effect( "FireDie", effectdata )


        local FireExp = ents.Create("env_physexplosion")
        FireExp:SetPos(self.Ball:GetPos())
        FireExp:SetKeyValue("magnitude", 200)
        FireExp:SetKeyValue("radius", 200)
        FireExp:SetKeyValue("spawnflags", "1")
        FireExp:Spawn()
        FireExp:Fire("Explode", "", 0)
        FireExp:Fire("kill", "", 1)
    end
    --------------------
    if self.SecAttack == 1 then

        self.SecAttack = 0

        self.Weapon:EmitSound("ambient/fire/ignite.wav")
        local effectdata = EffectData()
        effectdata:SetOrigin( self.Owner:GetPos() )
        util.Effect( "FireExplosion", effectdata )

        IgniteProps = {} --ents.FindInSphere( self.Owner:GetPos(), 200)

        if SERVER then
            local FireExp = ents.Create("env_physexplosion")
            FireExp:SetPos(self.Owner:GetPos())
            FireExp:SetParent(self.Owner)
            FireExp:SetKeyValue("magnitude", 200)
            FireExp:SetKeyValue("radius", 500)
            FireExp:SetKeyValue("spawnflags", "1")
            FireExp:Spawn()
            FireExp:Fire("Explode", "", 0)
            FireExp:Fire("kill", "", 2)

            for i=1,100 do

                if IgniteProps[i] ~= nil and IgniteProps[i] ~= NULL then
                    if  string.find(IgniteProps[i]:GetClass(), "prop_vehicle_*") or string.find(IgniteProps[i]:GetClass(), "prop_ragdoll") or string.find(IgniteProps[i]:GetClass(), "npc_*")then
                        IgniteProps[i]:Ignite( 20, 10)
                    end
                end
            end
        end
    end
    -----------------
    if self.FartTrail > CurTime() then

        local effectdata = EffectData()
        effectdata:SetOrigin( self.Owner:GetPos() )
        util.Effect( "FireTrail", effectdata )
    end
    -----------------
end
-------------------------
function SWEP:PrimaryAttack()

    if self.PrimDelay < CurTime() then
        self.PrimDelay = CurTime()+2.5




        if SERVER then

            if  self.Owner:Team() ~= TEAM_DRAGON then
                self.Owner:Ignite(2,10)
                self.Owner:ChatPrint("Don't use dragon stuff, you'll burn!!")
                return
            end

            self.Weapon:EmitSound("ambient/fire/mtov_flame2.wav")
            self.Owner:SetAnimation( PLAYER_ATTACK1 );
            self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )

            self.ShootFireBall = 1
            self.FireDie = 0
            self.MakeFireBallDel = CurTime()+0.3

        end
    end
end
-------------------
function SWEP:SecondaryAttack()

    if self.SecDelay < CurTime() then
        self.SecDelay = CurTime() + 4

        self.Owner:SetAnimation( PLAYER_ATTACK1 )
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        self.SecAttack = 1
    end
end
------------------
function SWEP:Reload()
    if self.ReloadDelay < CurTime() then
        self.ReloadDelay = CurTime()+2
        self.Weapon:EmitSound("ambient/fire/mtov_flame2.wav")
        self.FartTrail = CurTime() + 1
        self.Owner:SetVelocity(Vector( 0, 0, 400))
    end
end
--------------------
function SWEP:Holster()

    self.WepUse = 1
    self.FireDie = 1
    return true
end
------------General Swep Info---------------
SWEP.Author   			= "Sakarias - Fixed by KoZ"
SWEP.Contact        	= "sakariasjohansson@hotmail.com"
SWEP.Purpose        	= ""
SWEP.Instructions   	= "Throw fireballs"
SWEP.Category			= "Magic"
SWEP.Spawnable      	= true
SWEP.AdminSpawnable 	= true
--------------------------------------------

------------Models---------------------------
SWEP.ViewModel = Model("models/weapons/v_stunbaton.mdl")
SWEP.WorldModel = Model("models/weapons/w_stunbaton.mdl")
SWEP.ViewModelFOV = 10
---------------------------------------------

-------------Primary Fire Attributes----------------------------------------
SWEP.Primary.Delay				= 0.8
SWEP.Primary.Recoil				= 0
SWEP.Primary.Damage				= 0
SWEP.Primary.NumShots			= 0
SWEP.Primary.Cone				= 0
SWEP.Primary.ClipSize			= -1
SWEP.Primary.DefaultClip		= -1
SWEP.Primary.Automatic   		= true
SWEP.Primary.Ammo         		= "none"
-------------------------------------------------------------------------------------

-------------Secondary Fire Attributes---------------------------------------

SWEP.Secondary.Delay			= 0.8
SWEP.Secondary.Recoil			= 0
SWEP.Secondary.Damage			= 0
SWEP.Secondary.NumShots			= 0
SWEP.Secondary.Cone		  		= 0
SWEP.Secondary.ClipSize			= -1
SWEP.Secondary.DefaultClip		= -1
SWEP.Secondary.Automatic   		= true
SWEP.Secondary.Ammo         	= "none"
-------------------------------------------------------------------------------------
