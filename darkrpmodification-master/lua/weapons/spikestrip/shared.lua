if SERVER then
	AddCSLuaFile("shared.lua")
end

Kun_RepairCost = 50

SWEP.ViewModel = Model("models/weapons/c_pistol.mdl")
SWEP.WorldModel = Model("models/weapons/w_pistol.mdl")
SWEP.UseHands = true
SWEP.Base = "weapon_base"

	SWEP.PrintName = "Spike Strip"
	SWEP.Slot = 1
	SWEP.SlotPos = 3
	SWEP.DrawAmmo = true
	SWEP.DrawCrosshair = true


SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false

SWEP.Spawnable = false
SWEP.AdminSpawnable = true
SWEP.Sound = ""
SWEP.Primary.ClipSize = 1
SWEP.Primary.DefaultClip = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo 		= "XBowBolt"
SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "spikestrips"

function SWEP:Initialize()
	self:SetHoldType("normal")
end

function SWEP:Deploy()
	if SERVER then
		self.Owner:DrawWorldModel(false)
	end
end

function SWEP:PrimaryAttack()
    self:SetNextPrimaryFire(CurTime() + 1)
    if self:Clip1() < 1 and SERVER then
        DarkRP.notify(self.Owner, 0, 4, "You are out of ammo. Look at your spikestrip to pick it up")
        self:SecondaryAttack()
        return
    end
	if SERVER then
		local ply = self.Owner
		self:DarkRP_SpikeStrip(ply,ent)
	end
end

function SWEP:SecondaryAttack()
    self:SetNextSecondaryFire(CurTime() + 1)
	if SERVER then
		local ply = self.Owner
		local ent = ply:GetEyeTrace().Entity
		if(ent:GetClass() == "ent_spikes" and ent.TrapOwner == ply) then
			ent:Remove()
			self:SetClip1(self:Clip1() + 1)
		end
	end
end

function SWEP:DarkRP_SpikeStrip(ply,ent)
	if(ply.FireTime != nil and (CurTime() - ply.FireTime) < 2) then return end
	ply.FireTime = CurTime()
	local pos = ply:GetEyeTrace().HitPos
	local lookent = ply:GetEyeTrace().Entity
	if(pos:Distance(ply:GetPos()) > 200) then return end
	if(lookent != nil and lookent:IsValid() and lookent:IsPlayer()) then return false end
	local entsz = ents.FindInSphere(pos,100)
	for k,v in pairs(entsz) do
		if(string.find(v:GetClass(),"_door") != nil) then
			DarkRP.notify(ply, 0, 4, "Do not place this near a door.")
			return
		end
	end
	
	if(ply.HasSpikes == nil or ply.HasSpikes == 0 or not IsValid(ply.HasSpikes)) then
		local angs = ply:GetAngles()
		local ent = ents.Create("ent_spikes")
		ent:SetPos(pos)
		ent:SetAngles(Angle(0,angs.y,0)) 
		ent:Spawn()
		ent.TrapOwner = ply
		ply.HasSpikes = ent
		ent:GetPhysicsObject():EnableMotion(false)
        self:TakePrimaryAmmo(1)
	else
		if(ply.HasSpikes != 0 and ply.HasSpikes:IsValid()) then
			DarkRP.notify(ply, 0, 4, "You already have a trap out somewhere.")
		end
	end
end

function SWEP:Reload()

    self.Weapon:DefaultReload(ACT_VM_RELOAD)
    -- Animation when you're reloading

    if ( self.Weapon:Clip1() < self.Primary.ClipSize ) and self.Owner:GetAmmoCount(self.Primary.Ammo) > 0 then
        -- When the current clip < full clip and the rest of your ammo > 0, then

        self.Owner:SetFOV( 0, 0.15 )
        -- Zoom = 0

    end
end
