
SWEP.PrintName			= "Fire Breath"
SWEP.Category = "GreenBlack"
SWEP.SlotPos = 6 //			= 1
SWEP.Slot = 2

SWEP.Base				= "pokemon_sounds"

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType = "normal"
SWEP.ViewModelFOV = 80
SWEP.ViewModelFlip = true
SWEP.ViewModel			= "models/weapons/c_arms_citizen.mdl"
SWEP.WorldModel			= 'models/props_wasteland/panel_leverHandle001a.mdl'
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("default.zoom")
SWEP.Primary.SoundLevel			= 40
SWEP.Primary.Recoil			= 2
SWEP.Primary.Damage			= 0
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.015
SWEP.Primary.ClipSize		= 900
SWEP.Primary.Delay			= 10
SWEP.Primary.DefaultClip	= 900
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= ".50AE"
SWEP.InitialHoldtype = "pistol"
SWEP.InHoldtype = "pistol"
SWEP.CantSilence = true
SWEP.NoBoltAnim = true
SWEP.ChamberAmount = 2
SWEP.SprintAndShoot = true
SWEP.HeadshotMultiplier = 1.2

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.75 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.FOVZoom = 85

SWEP.Primary.Sound2 = { "weapons/warden/warden1.mp3","weapons/warden/warden2.mp3","weapons/warden/warden3.mp3"}

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 3
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone 		= 0.02
SWEP.HipCone 				= 0.046
SWEP.ConeInaccuracyAff1 = 0.5

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.AnimCyc = 1

SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

function SWEP:Reload()
end

SWEP.HoldType =  "normal"
SWEP.HeadshotMultiplier = 1

function SWEP:NormalPrimaryAttack(worldsnd)

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self.Primary.Cone )

    --self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:PrimaryAttack()

    if self.Owner:Team() != TEAM_POKEMON and  self.Owner:Team() != TEAM_DRAGON then
    self.Owner:ChatPrint("You cannot use pokemon powers as a non-pokemon")
    return false
    end

    if self:Clip1() > 0 and SERVER then
        self.Owner:EmitSound( table.Random(  { "npc/strider/fire.wav"} ),100,100)
    end
    self:NormalPrimaryAttack()

end



SWEP.LaserColor = Color(255,100,100,255)
function SWEP:ShootBullet( damage, num_bullets, aimcone )

    local bullet = {}
    bullet.Num 		= num_bullets
    bullet.Src 		= self.Owner:GetShootPos()
    bullet.Dir 		= self.Owner:GetAimVector()
    bullet.Spread 	= Vector( 0.001, 0.001, 0 )
    bullet.Tracer	= 1
    bullet.Force	= 10
    bullet.Damage	= 5
    bullet.AmmoType = "Pistol"
    bullet.HullSize = 2
    bullet.TracerName = "LaserTracer_thick"

    self.Owner:FireBullets( bullet )
    self:ShootEffects()

    local tr = self.Owner:GetEyeTrace()
    local effectdata = EffectData()
    effectdata:SetOrigin(tr.HitPos)
    effectdata:SetNormal(tr.HitNormal)
    effectdata:SetScale(1)
    util.Effect("effect_mad_ignition", effectdata)
    util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

    local tracedata = {}
    tracedata.start = tr.HitPos
    tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
    tracedata.filter = tr.HitPos
    local tracedata = util.TraceLine(tracedata)
    if SERVER then
        if tracedata.HitWorld then
            local flame = ents.Create("env_fire");
            flame:SetPos(tr.HitPos + Vector(0, 0, 1));
            flame:SetKeyValue("firesize", "10");
            flame:SetKeyValue("fireattack", "10");
            flame:SetKeyValue("StartDisabled", "0");
            flame:SetKeyValue("health", "10");
            flame:SetKeyValue("firetype", "0");
            flame:SetKeyValue("damagescale", "5");
            flame:SetKeyValue("spawnflags", "128");
            flame:SetPhysicsAttacker(self.Owner)
            flame:SetOwner(self.Owner)
            flame:Spawn();
            flame:Fire("StartFire", 0);
        end
    end

end



function SWEP:DrawWorldModel( )

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end
    return false
end