
-- Variables that are used on both client and server
SWEP.Gun = ("m9k_thompson") -- must be the name of your swep but NO CAPITALS!
SWEP.Category				= "GreenBlack"
SWEP.Author				= ""
SWEP.Contact				= ""
SWEP.Purpose				= ""
SWEP.Instructions				= ""
SWEP.MuzzleAttachment			= "1" 	-- Should be "1" for CSS models or "muzzle" for hl2 models
SWEP.ShellEjectAttachment			= "2" 	-- Should be "2" for CSS models or "1" for hl2 models
SWEP.PrintName				= "Tommy Gun"		-- Weapon name (Shown on HUD)	
SWEP.Slot				= 2				-- Slot in the weapon selection menu
SWEP.SlotPos				= 3			-- Position in the slot
SWEP.DrawAmmo				= true		-- Should draw the default HL2 ammo counter
SWEP.DrawWeaponInfoBox			= false		-- Should draw the weapon info box
SWEP.BounceWeaponIcon   		= 	false	-- Should the weapon icon bounce?
SWEP.DrawCrosshair			= true		-- set false if you want no crosshair
SWEP.Weight				= 30			-- rank relative ot other weapons. bigger is better
SWEP.AutoSwitchTo			= true		-- Auto switch to if we pick it up
SWEP.AutoSwitchFrom			= true		-- Auto switch from if you pick up a better weapon
SWEP.HoldType 				= "smg"		-- how others view you carrying the weapon
-- normal melee melee2 fist knife smg ar2 pistol rpg physgun grenade shotgun crossbow slam passive 
-- you're mostly going to use ar2, smg, shotgun or pistol. rpg and crossbow make for good sniper rifles

SWEP.Slot = 2
SWEP.ViewModelFOV			= 65
SWEP.ViewModelFlip			= true
SWEP.ViewModel				= "models/weapons/v_smg_p90_tommy.mdl"	-- Weapon view model
SWEP.WorldModel				= "models/weapons/w_smg_p90_tommy.mdl"	-- Weapon world model
SWEP.Base				= "weapon_real_base_smg"
SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true
SWEP.FiresUnderwater = false

SWEP.Primary.Sound			= Sound("tommygun_shoot.mp3")		-- Script that calls the primary fire sound
SWEP.Primary.RPM			= 575			-- This is in Rounds Per Minute
SWEP.Primary.ClipSize			= 75		-- Size of a clip
SWEP.Primary.DefaultClip		= 75		-- Bullets you start with
SWEP.Primary.KickUp				= 0.2		-- Maximum up recoil (rise)
SWEP.Primary.KickDown			= 0.6		-- Maximum down recoil (skeet)
SWEP.Primary.KickHorizontal		= 0.2		-- Maximum up recoil (stock)
SWEP.Primary.Automatic			= true		-- Automatic = true; Semi Auto = false
SWEP.Primary.Ammo			= "smg1"			-- pistol, 357, smg1, ar2, buckshot, slam, SniperPenetratedRound, AirboatGun
-- Pistol, buckshot, and slam always ricochet. Use AirboatGun for a light metal peircing shotgun pellets

SWEP.SelectiveFire		= true

SWEP.Secondary.IronFOV			= 60		-- How much you 'zoom' in. Less is more! 	

SWEP.data 				= {}				--The starting firemode
SWEP.data.ironsights			= 1

SWEP.HoldType = "smg"
SWEP.Primary.NumShots	= 1		-- How many bullets to shoot per trigger pull
SWEP.Primary.Damage		= 12	-- Base damage per bullet
SWEP.Primary.Cone = 0.015
SWEP.Primary.Recoil = 0.5
SWEP.Primary.Spread		= .15	-- Define from-the-hip accuracy 1 is terrible, .0001 is exact)
SWEP.Primary.IronAccuracy = .09 -- Ironsight accuracy, should be the same for shotguns
SWEP.Primary.Delay = 0.1
SWEP.IronSightsPos = Vector(3.369, 0, 1.7)
SWEP.IronSightsAng = Vector(-1.407, -3.981, 0)

function SWEP:Initialize()
    util.PrecacheSound(self.Primary.Sound)
    self:SetHoldType( self.HoldType or "pistol" )
end

function SWEP:SecondaryAttack()
end

function SWEP:Reload()

    self.Weapon:DefaultReload( ACT_VM_RELOAD );
end

function SWEP:DrawWorldModel()

    local hand, offset, rotate

    if not IsValid(self.Owner) then
        self:DrawModel()
        return
    end

    hand = self.Owner:GetAttachment(self.Owner:LookupAttachment("anim_attachment_rh"))
    if not hand then return end
    offset = hand.Ang:Right() * 1 + hand.Ang:Forward() * -6 + hand.Ang:Up() * -2

    hand.Ang:RotateAroundAxis(hand.Ang:Right(), 12)
    hand.Ang:RotateAroundAxis(hand.Ang:Forward(), 0)
    hand.Ang:RotateAroundAxis(hand.Ang:Up(), 360)

    self:SetRenderOrigin(hand.Pos + offset)
    self:SetRenderAngles(hand.Ang)

    self:DrawModel()

    if (CLIENT) then
        self:SetModelScale(1,0.9,1)
    end
end



