if SERVER then
    AddCSLuaFile()
end

if CLIENT then
    SWEP.PrintName = "Mod / Admin Stick"
    SWEP.Slot = 4
    SWEP.SlotPos = 5
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = true
end

SWEP.Base = "arrest_stick"

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false
SWEP.AnimPrefix = "stunstick"

SWEP.Spawnable = true
SWEP.AdminOnly = true
SWEP.Category = "GreenBlack"

SWEP.NextStrike = 0

SWEP.ViewModel = Model("models/weapons/v_stunbaton.mdl")
SWEP.WorldModel = Model("models/weapons/w_stunbaton.mdl")

SWEP.Sound = Sound("weapons/stunstick/stunstick_swing1.wav")

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""
SWEP.AllowDrop = false
SWEP.CanDrop = false


function SWEP:OnDrop()
    self:Remove()
end

function SWEP:Initialize()
    self:SetNWInt("ArrestTime",60)
end

function SWEP:Deploy()
    self.disabledandsuch = true
    self.BaseClass.Deploy(self)
end

function SWEP:PrimaryAttack()
    if self.disabledandsuch then
        self.Owner:PrintMessage( HUD_PRINTCENTER, "Disabled. Press R to enable" )
        return
    end
    if CurTime() < self.NextStrike then return end

    self:SetHoldType("melee")
    timer.Simple(0.3, function() if self:IsValid() then self:SetHoldType("normal") end end)

    self.NextStrike = CurTime() + 0.2 -- Actual delay is set later.

    if CLIENT then return end

    timer.Stop(self:GetClass() .. "_idle" .. self:EntIndex())
    local vm = self.Owner:GetViewModel()
    if IsValid(vm) then
        vm:ResetSequence(vm:LookupSequence("idle01"))
        timer.Simple(0, function()
            if not IsValid(self) or not IsValid(self.Owner) or not IsValid(self.Owner:GetActiveWeapon()) or self.Owner:GetActiveWeapon() ~= self then return end
            self.Owner:SetAnimation(PLAYER_ATTACK1)

            if IsValid(self.Weapon) then
                self.Weapon:EmitSound(self.Sound)
            end

            local vm = self.Owner:GetViewModel()
            if not IsValid(vm) then return end
            vm:ResetSequence(vm:LookupSequence("attackch"))
            vm:SetPlaybackRate(1 + 1/3)
            local duration = vm:SequenceDuration() / vm:GetPlaybackRate()
            timer.Create(self:GetClass() .. "_idle" .. self:EntIndex(), duration, 1, function()
                if not IsValid(self) or not IsValid(self.Owner) then return end
                local vm = self.Owner:GetViewModel()
                if not IsValid(vm) then return end
                vm:ResetSequence(vm:LookupSequence("idle01"))
            end)
            self.NextStrike = CurTime() + 0.2
        end)
    end

    self.Owner:LagCompensation(true)
    local trace = util.QuickTrace(self.Owner:EyePos(), self.Owner:GetAimVector() * 250, {self.Owner})
    self.Owner:LagCompensation(false)
    if IsValid(trace.Entity) and trace.Entity.onArrestStickUsed then
        trace.Entity:onArrestStickUsed(self.Owner)
        return
    end

    local ent = self.Owner:getEyeSightHitEntity(nil, nil, function(p) return p ~= self.Owner and p:IsPlayer() and p:Alive() end)

    if not IsValid(ent) or (self.Owner:EyePos():Distance(ent:GetPos()) > 250) or (not ent:IsPlayer() and not ent:IsNPC()) then
        return
    end


    local jpc = DarkRP.jailPosCount()

    if not jpc or jpc == 0 then
        DarkRP.notify(self.Owner, 1, 4, DarkRP.getPhrase("cant_arrest_no_jail_pos"))
    else
        -- Send NPCs to Jail
        if ent:IsNPC() then
            ent:SetPos(DarkRP.retrieveJailPos())
        else
            if not ent.Babygod then
                ent:arrest(self:GetNWInt("ArrestTime"), self.Owner)
                DarkRP.notify(ent, 0, 20, "Moderator/Admin Arrest by: "..self.Owner:Nick())

                if self.Owner.SteamName then
                    DarkRP.log(self.Owner:Nick().." ("..self.Owner:SteamID()..") arrested "..ent:Nick().." (Mod/Admin arrest)", Color(0, 255, 255))
                end
            else
                DarkRP.notify(self.Owner, 1, 4, DarkRP.getPhrase("cant_arrest_spawning_players"))
            end
        end
    end
end

function SWEP:PreDrawViewModel()
    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,0,255,255))
    self.Owner:GetViewModel():SetMaterial("models/shiny")
end

SWEP.nextreload=0
function SWEP:Reload()
    if CLIENT or self.nextreload > CurTime() then return end
    self.nextreload = CurTime() + 0.3
    if self.disabledandsuch then
        self.Owner:PrintMessage( HUD_PRINTCENTER, "Enabled" )
        self.disabledandsuch = false
        return
    end

    local t = self:GetNWInt("ArrestTime");
    if t == 30 then
        self:SetNWInt("ArrestTime", 60);
    elseif t == 60 then
        self:SetNWInt("ArrestTime", 90);
    elseif t == 90 then
        self:SetNWInt("ArrestTime", 120);
    elseif t == 120 then
        self:SetNWInt("ArrestTime", 240);
    else
        self:SetNWInt("ArrestTime", 30);
    end
end


    if CLIENT then
        function SWEP:DrawHUD( )
            local shotttext = "Jail Time: "..self:GetNWInt("ArrestTime").." seconds\nR to change JailTime | RMB to freeze"
            surface.SetFont( "ChatFont" );
            local size_x, size_y = surface.GetTextSize( shotttext );

            draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
            draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        end
    end;

function SWEP:SecondaryAttack()
    if CurTime() < self.NextStrike then return end

    self:SetHoldType("melee")
    timer.Simple(0.3, function() if self:IsValid() then self:SetHoldType("normal") end end)

    self.NextStrike = CurTime() + 0.2 -- Actual delay is set later.



    timer.Stop(self:GetClass() .. "_idle" .. self:EntIndex())
    local vm = self.Owner:GetViewModel()
    if IsValid(vm) then
        vm:ResetSequence(vm:LookupSequence("idle01"))
        timer.Simple(0, function()
            if not IsValid(self) or not IsValid(self.Owner) or not IsValid(self.Owner:GetActiveWeapon()) or self.Owner:GetActiveWeapon() ~= self then return end
            self.Owner:SetAnimation(PLAYER_ATTACK1)

            if IsValid(self.Weapon) then
                self.Weapon:EmitSound(self.Sound)
            end

            local vm = self.Owner:GetViewModel()
            if not IsValid(vm) then return end
            vm:ResetSequence(vm:LookupSequence("attackch"))
            vm:SetPlaybackRate(1 + 1/3)
            local duration = vm:SequenceDuration() / vm:GetPlaybackRate()
            timer.Create(self:GetClass() .. "_idle" .. self:EntIndex(), duration, 1, function()
                if not IsValid(self) or not IsValid(self.Owner) then return end
                local vm = self.Owner:GetViewModel()
                if not IsValid(vm) then return end
                vm:ResetSequence(vm:LookupSequence("idle01"))
            end)
            self.NextStrike = CurTime() + 0.2
        end)
    end

    self.Owner:LagCompensation(true)
    local trace = util.QuickTrace(self.Owner:EyePos(), self.Owner:GetAimVector() * 250, {self.Owner})
    self.Owner:LagCompensation(false)
    if IsValid(trace.Entity) and trace.Entity.onArrestStickUsed then
        trace.Entity:onArrestStickUsed(self.Owner)
        return
    end

    local ent = self.Owner:getEyeSightHitEntity(nil, nil, function(p) return p ~= self.Owner and p:IsPlayer() and p:Alive() end)



    if CLIENT then
        if not IsValid(ent) then return end
        ent.nextwantedcheck = CurTime() + 0.25
        return
    end



    if not IsValid(ent) or (self.Owner:EyePos():Distance(ent:GetPos()) > 250) or (not ent:IsPlayer() and not ent:IsNPC()) then
        return
    end


    local jpc = DarkRP.jailPosCount()

    if not jpc or jpc == 0 then
        DarkRP.notify(self.Owner, 1, 4, DarkRP.getPhrase("cant_arrest_no_jail_pos"))
    else
        if ent.IsSlowedStick  then
            ent:SetWalkSpeed( ent.oldWalkSpeed )
            ent:SetRunSpeed( ent.oldRunSpeed )
            ent.IsSlowedStick = false
            ent.slowedstick = false
            ent:SetNWBool("slowedstick",false)
           ent:Freeze(false)
        else
            ent.oldWalkSpeed = ent:GetWalkSpeed()
            ent.oldRunSpeed = ent:GetRunSpeed()
            ent:SetWalkSpeed( 1 )
            ent:SetRunSpeed( 1 )
            ent.IsSlowedStick = true
            ent.slowedstick = true
            ent:SetNWBool("slowedstick",true)
            ent:Freeze(true)
        end
    end
end