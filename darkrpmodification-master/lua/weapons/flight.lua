if ( SERVER ) then

	AddCSLuaFile()
	
	SWEP.HoldType			= "ar2"
	
end

if ( CLIENT ) then

	SWEP.PrintName			= "Flight"
	SWEP.Author				= "Rejax" 
	SWEP.Purpose		= "flight and entertainment and learning lua"
	SWEP.Instructions	= "Primary = Flight mode | Secondary = end"
	SWEP.Slot				= 3
	SWEP.SlotPos			= 3
	

	SWEP.WepSelectIcon		= surface.GetTextureID( "weapons/swep" )
	
end


SWEP.DrawCrosshair      = false
--SWEP.Base				= "weapon_cs_base"

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.Category = "Flight"

SWEP.ViewModel			= "models/weapons/v_hands.mdl" --view model
SWEP.WorldModel			= "" --world model
SWEP.ViewModelFlip		= true
SWEP.DrawAmmo = false
SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Delay			= 0.9 	--In seconds
SWEP.Primary.Recoil			= 0		--Gun Kick
SWEP.Primary.NumShots		= 1		--Number of shots per one fire
SWEP.Primary.ClipSize		= 1000 --Use "-1 if there are no clips"
SWEP.Primary.DefaultClip	= 1000	--Number of shots in next clip
SWEP.Primary.Automatic   	= false	--Pistol fire (false) or SMG fire (true)
SWEP.Primary.Ammo         	= "pistol"	--Ammo Type

SWEP.Secondary.Delay		= 0.9
SWEP.Secondary.NumShots		= 1
SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic   	= false
SWEP.Secondary.Ammo         = "pistol"

function SWEP:Initialize()

self.IsJumping = 0
self.NextJumpTime = 0
util.PrecacheSound("physics/flesh/flesh_impact_bullet" .. math.random( 3, 5 ) .. ".wav")
util.PrecacheSound("weapons/iceaxe/iceaxe_swing1.wav")

end

function SWEP:Deploy()
    self.Owner:DrawViewModel(false)
    --self.Owner:DrawWorldModel(false)
    return true
end


function SWEP:PrimaryAttack()

    if self.Owner:Team() != TEAM_BIRD and self.Owner:Team() != TEAM_DRAGON then
        return
    end

	if ( !self:CanPrimaryAttack() or self.IsJumping == 1 ) then
		return
	end
	
	if self.IsJumping == 0 then
	
		self.IsJumping = 1
		
		self.Owner:SetMoveType( 4 )
			
		self.NextJumpTime = CurTime() + 0.1
		self.Owner:SetMaxSpeed( 1500 )

		self.Owner:ConCommand("pp_dof_initlength 9")
		self.Owner:ConCommand("pp_dof_spacing 8")
		

	end
	
	// Play shoot sound
--	self.Owner:EmitSound(Sound("player/suit_sprint.wav"))
	self.Weapon:EmitSound("ambient/wind/wind_hit2.wav", 100, 60)
	
end



function SWEP:SecondaryAttack()

local trace = self.Owner:GetEyeTrace()

	if self.IsJumping == 1 then

		self.IsJumping = 0 

		self.Owner:ConCommand("pp_dof 0")

		self.Owner:SetVelocity(self.Owner:GetForward() * 200 + Vector(0,0,200))
		self.Owner:SetMoveType( 2 )
		self.Owner:SetMaxSpeed( 200 )
		self.NextJumpTime = CurTime()
		
	end
	
end

function SWEP:Think()
	
	if self.NextJumpTime == CurTime() then

		self.IsJumping = 0
		
		self.Owner:ConCommand("pp_dof 0")
		self.Owner:SetVelocity(self.Owner:GetForward() * 50 + Vector(0,0,200))
		self.Owner:SetMoveType( 2 )
		self.Owner:SetMaxSpeed( 200 )
		
	end
	
end

