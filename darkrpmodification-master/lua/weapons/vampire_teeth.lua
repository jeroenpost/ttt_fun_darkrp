
SWEP.AllowDrop = false
-------------------------------------------------------------------
SWEP.Author   = "GreenBlack"
SWEP.Contact        = ""
SWEP.Purpose        = "Vampire"
SWEP.Instructions   = ""
SWEP.Spawnable      = true
SWEP.AdminSpawnable  = true
SWEP.Category = "GreenBlack"
-----------------------------------------------
SWEP.FiresUnderwater = true
SWEP.HoldType		= "ar2"
SWEP.ViewModel = Model("models/weapons/v_hands.mdl")
SWEP.WorldModel   	= ""--"w_hands.mdl"
-----------------------------------------------
SWEP.Primary.Delay			= 0.5
SWEP.Primary.Sound			= Sound( "weapons/bite.mp3" )
SWEP.Primary.Recoil			= 0
SWEP.Primary.Damage			= 5
SWEP.Primary.NumShots		= 1		
SWEP.Primary.Cone			= 0
SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic   	= true
SWEP.Primary.Ammo         	= "none" 
-------------------------------------------------



SWEP.HoldType			= "fist"
	SWEP.Weight				= 5
	SWEP.AutoSwitchTo 		= false;
	SWEP.AutoSwitchFrom 	= false;
SWEP.PrintName			= "Vampire Teeth"
SWEP.Base = "weapon_base"
SWEP.Kind = WEAPON_PISTOL
SWEP.Category = "GreenBlack"
	SWEP.Author				= "GreenBlack"


	SWEP.Slot				= 1
	SWEP.SlotPos			= 1
	SWEP.IconLetter			= "C" 
	SWEP.ViewModelFOV		= 50
	SWEP.DrawCrosshair		= false
SWEP.IsUsed = false


x = 0

function SWEP:Reload()
end

function SWEP:OnDrop()
    return false
end


function SWEP:SecondaryAttack()
    self.Weapon:SetNextSecondaryFire(CurTime() + .75)
    local trace = self.Owner:GetEyeTrace()
    local hitEnt = trace.Entity
    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 70)


    if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 150 and IsValid( hitEnt ) and hitEnt:IsPlayer() then
       if hitEnt:Team() != TEAM_VAMPIRE then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "You can only give blood to other vampires" )
           return
       end
       self.Owner:SetNWInt("vampireHealth", self.Owner:GetNWInt("vampireHealth") - 10 )
       hitEnt:SetNWInt("vampireHealth", hitEnt:GetNWInt("vampireHealth") + 10 )
       if hitEnt:GetNWInt("vampireHealth")> 100 then
           hitEnt:SetNWInt("vampireHealth", 100)
       end
       self:EmitSound("npc/headcrab_poison/ph_rattle3.wav")
    end
end

function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire(CurTime() + .75)
    if self.Owner:Team() != TEAM_VAMPIRE then
    self.Owner:ChatPrint("You cannot use vampire stuff as non-vampire")

    return false
    end

    local ply = self.Owner

    GAMEMODE:SetPlayerSpeed(ply, 350,380) ply:SetJumpPower(380)


	local trace = self.Owner:GetEyeTrace()
       local hitEnt = trace.Entity
          local spos = self.Owner:GetShootPos()
   local sdest = spos + (self.Owner:GetAimVector() * 150)


    if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 170 and IsValid( hitEnt ) then

		local dmg = DamageInfo()
            if self.Owner:GetNWInt("vampireHealth") < 90 then
                dmg:SetDamage(6)
            else
                dmg:SetDamage(0)
            end
            if hitEnt:IsPlayer() and hitEnt:Team() == TEAM_VAMPIREHUNTER then
                dmg:SetDamage(51)
            end

                dmg:SetAttacker(self.Owner)
                dmg:SetInflictor(self.Owner)
                dmg:SetDamageForce(self.Owner:GetAimVector() * 1500)
                dmg:SetDamagePosition(self.Owner:GetPos())
                dmg:SetDamageType(DMG_CLUB)

                hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

                local edata = EffectData()
                    edata:SetStart(spos)
                    edata:SetOrigin(trace.HitPos)
                    edata:SetNormal(trace.Normal)
                    edata:SetEntity(hitEnt)

                    if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
                        if hitEnt:IsPlayer() and hitEnt:Team()== TEAM_VAMPIRE then
                            self.Owner:PrintMessage( HUD_PRINTCENTER, "You cannot get blood from other vampires" )
                            return
                         end
                       util.Effect("BloodImpact", edata)
                       self.Weapon:EmitSound("npc/headcrab_poison/ph_poisonbite"..math.random(1,3)..".wav")

                        if hitEnt:IsPlayer() and hitEnt:Team() == TEAM_VAMPIREHUNTER then
                            self.Owner:SetNWInt("vampireHealth", self.Owner:GetNWInt("vampireHealth") + 40 )
                        else
                            self.Owner:SetNWInt("vampireHealth", self.Owner:GetNWInt("vampireHealth") + 10 )
                        end


                       if self.Owner:GetNWInt("vampireHealth")> 100 then
                           self.Owner:SetNWInt("vampireHealth", 100)
                       end
                        if hitEnt:GetClass() == "prop_ragdoll"  and SERVER  then
                            if not hitEnt.vampirebites then hitEnt.vampirebites = 0 end
                            hitEnt.vampirebites = hitEnt.vampirebites + 1
                            if hitEnt.vampirebites > 9 then
                                hitEnt:Remove()
                            end
                        end
                    end
    end



end

function SWEP:Deploy()
	self.Owner:DrawViewModel(false)
	--self.Owner:DrawWorldModel(false)
	return true
end

function SWEP:Holster()
	return true
end

function SWEP:DoImpactEffect( tr, dmgtype )
	return true
end



