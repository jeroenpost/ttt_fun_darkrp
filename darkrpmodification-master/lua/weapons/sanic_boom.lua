if (SERVER) then --the init.lua stuff goes in here
 
 
  -- AddCSLuaFile ("shared.lua");
 
 
   SWEP.Weight = 5;
   SWEP.AutoSwitchTo = false;
   SWEP.AutoSwitchFrom = false;
 
end
 
if (CLIENT) then --the cl_init.lua stuff goes in here
 
 
   SWEP.PrintName = "Sanic BOOM";
   SWEP.Slot = 0;
   SWEP.SlotPos = 4;
   SWEP.DrawAmmo = false;
   SWEP.DrawCrosshair = false;
 
end
 
 
SWEP.Author = "GreenBlack";
SWEP.Contact = "";
SWEP.Purpose = "SPEED";
SWEP.Instructions = "PLAI MAI TJIEM SJONK!";
SWEP.Category = "GreenBlack"
 
SWEP.Spawnable = true;
SWEP.AdminSpawnable = true;

SWEP.ViewModel          = "models/weapons/cstrike/c_knife_t.mdl"
SWEP.WorldModel         = 'models/weapons/w_knife_t.mdl'
SWEP.UseHands = true
SWEP.ViewModelFOV			= 60
SWEP.HoldType				= "grenade"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false


SWEP.Primary.ClipSize = -1;
SWEP.Primary.DefaultClip = -1;
SWEP.Primary.Automatic = false;
SWEP.Primary.Ammo = "none";
SWEP.Primary.Delay		= 1.5


SWEP.Secondary.ClipSize = -1;
SWEP.Secondary.DefaultClip = -1;
SWEP.Secondary.Automatic = false;
SWEP.Secondary.Ammo = "none";
SWEP.Secondary.Delay		= 3

SWEP.RandomEffects = {

}

function SWEP:PrimaryAttack()
    if self.Owner:Team() != TEAM_SANIC then return end
    self:CreateSound ()
    self.Owner:SetRunSpeed(2500)
end

SWEP.SoundObject = false
SWEP.LastFrame = false
SWEP.RestartDelay = 0
SWEP.SoundPlaying = false
SWEP.LastSoundRelease = 0
SWEP.on = false
function SWEP:Think ()
    if !self.Owner:KeyDown( IN_ATTACK ) and self.on then
         self:EndSound ()
    end
    if self.on then
         self:SetColor( Color(math.random(0,255),math.random(0,255),math.random(0,255)) )
    end
end

SWEP.NextSoundCheck = 0
function SWEP:CreateSound ()
    if not self.on and not self.Weapon:GetNWBool("on") and ( not self.NextSoundCheck2 or self.NextSoundCheck2 < CurTime()) then
        self.NextSoundCheck2 = CurTime() + 0.5
        self.on = true

        gb_PlaySoundFromServer(gb_config.websounds.."sanic_theme.mp3", self.Owner,false,"sanic_theme"..self.Owner:SteamID());
        self.Weapon:SetNWBool ("on", true)
        timer.Create("sanicPlay"..self.EntIndex(),49,10,function()
            if not IsValid(self) then return end
            self.Weapon:SetNWBool ("on", true)
            gb_PlaySoundFromServer(gb_config.websounds.."sanic_theme.mp3", self.Owner,false,"sanic_theme"..self.Owner:SteamID());
        end)


    end

end

function SWEP:SecondaryAttack()
    self:EndSound()
    self.on = false
end

function SWEP:Holster() self:EndSound() return true end
function SWEP:OwnerChanged() self:EndSound() end

function SWEP:EndSound()
    self.Weapon:SetNWBool ("on", false)
    self.on = false
    timer.Destroy("sanicPlay"..self.EntIndex())
    gb_StopSoundFromServer("sanic_theme"..self.EntIndex())
end



function SWEP:Reload()
		self.Weapon:SendWeaponAnim( ACT_VM_DRAW ) 
end

function SWEP:CamoPreDrawViewModel()
    if not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) or not self.Weapon:GetNWBool("on")  then return end
    self.Owner:GetViewModel():SetColor( Color(math.random(0,255),math.random(0,255),math.random(0,255)) )

end

function SWEP:PreDrawViewModel()
    self:CamoPreDrawViewModel()
end
