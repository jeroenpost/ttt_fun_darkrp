
if CLIENT then
	SWEP.PrintName = "Breathalizer"
	SWEP.Slot = 1
	SWEP.SlotPos = 3
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
	SWEP.DisplayAlpha = 0
	SWEP.BobScale = 0
	SWEP.SwayScale = 0
end

SWEP.Base = "weapon_cs_base2"

SWEP.Author = "GreenBlack"
SWEP.Instructions = "Check if comeone is drunk"
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.IconLetter = ""

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false

SWEP.Spawnable = true
SWEP.AdminOnly = true
SWEP.Category = "reenBlack"

SWEP.NextStrike = 0

SWEP.ViewModel = Model("models/weapons/v_c4.mdl")
SWEP.WorldModel = Model("models/weapons/w_c4.mdl")

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""

SWEP.ArrestSoundTime = 0
SWEP.ArrestTargetTime = 0

SWEP.ArrestTime = 3
SWEP.CanArrestFromAnyAngle = false
SWEP.RightClickSwitchToUnarrestBaton = true

SWEP.Handcuffs = true -- identifier, instead of calling GetClass (cheaper this way)
SWEP.UseHands = true
SWEP.ArrestCooldown = 10 -- change this value to add a cooldown for arresting after a successful arrest
SWEP.ArrestWait = 0 -- don't change this
SWEP.AllowDrop = false
SWEP.CanDrop = false


function SWEP:PrimaryAttack()
    if SERVER then

        local trace = self.Owner:GetEyeTrace( )

        if trace.StartPos:Distance( trace.HitPos ) < 150 and trace.Entity:IsPlayer()then
            self:SetNextPrimaryFire(CurTime() + 5)

            local ply = trace.Entity
            local owner = self.Owner
            timer.Create(ply:SteamID().."beep",0.5,5,function()
                sound.Play("weapons/c4/c4_beep1.wav", self:GetPos())

            end)
            timer.Create(ply:SteamID().."beep2",2.5,1,function()
                if IsValid(owner) and IsValid(ply) then

                    local drugs = {
                        "weed",
                        "cocaine",
                        "cigarette",
                        "alcohol",
                        "mushroom",
                        "meth",
                        "ecstasy",
                        "caffeine",
                        "pcp",
                        "lsd",
                        "opium"
                    }
                    for i = 1, #drugs do
                      if  ply:GetNWFloat("durgz_"..drugs[i].."_high_end") > CurTime() then
                          owner:PrintMessage( HUD_PRINTCENTER, ply:Nick().." is under influence of "..drugs[i] )
                          self.Owner:addXP( 500 )
                          return
                      end
                    end


                    owner:PrintMessage( HUD_PRINTCENTER, ply:Nick().." is sober" )
                    self.Owner:addXP( 250 )

                end
            end)
        end

    end
end

function SWEP:SecondaryAttack()

end