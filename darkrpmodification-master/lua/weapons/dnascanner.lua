if (SERVER) then
    AddCSLuaFile()
end

if (CLIENT) then
    SWEP.PrintName 		= "DNA Scanner"
    SWEP.Slot 			= 5
    SWEP.SlotPos 		= 1
    SWEP.IconLetter 		= "b"
end

SWEP.EjectDelay			= 0.05

SWEP.Instructions 		= "Scan a body with Primary"

SWEP.Category			= "GreenBlack"		-- Swep Categorie (You can type what your want)

SWEP.Base 				= "weapon_base"

SWEP.HoldType 		= "ar2"

SWEP.Spawnable 			= true
SWEP.AdminSpawnable 		= true

SWEP.ViewModel  = "models/weapons/v_crowbar.mdl"
SWEP.WorldModel = "models/props_lab/huladoll.mdl"

SWEP.Primary.Sound 		= Sound("Weapon_AK47.Single")
SWEP.Primary.Recoil 		= 1
SWEP.Primary.Damage 		= 33
SWEP.Primary.NumShots 		= 1
SWEP.Primary.Cone 		= 0.001
SWEP.Primary.ClipSize 		= -1
SWEP.Primary.Delay 		= 0.1
SWEP.Primary.DefaultClip 	= 30
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo 		= "smg1"

SWEP.Secondary.ClipSize 	= -1
SWEP.Secondary.DefaultClip 	= -1
SWEP.Secondary.Automatic 	= false
SWEP.Secondary.Ammo 		= "none"

SWEP.IronSightsPos = Vector(-6.56, -16.24, 2.799)
SWEP.IronSightsAng = Vector(2.299, 0.1, -0.5)


function SWEP:Reload()
    return false
end
function SWEP:Deploy()
    if SERVER and IsValid(self.Owner) then
        self.Owner:DrawViewModel(false)
    end
    return true
end

if CLIENT then
    function SWEP:DrawWorldModel()
        if not IsValid(self.Owner) then
            self:DrawModel()
        end
    end
end

if CLIENT then


    function SWEP:PrimaryAttack()
        local ent = self.Owner:GetEyeTrace().Entity
        if not self.Started and IsValid( ent ) and ent:GetNWBool("DeadBody",false)  then
            self.StartTime = CurTime()
            self.EndTime = CurTime() + 8
            self.Started = true
        end
    end

    function SWEP:DrawHUD()
        if self.Started then
            --print("1")
            if self.Owner:KeyDown(IN_ATTACK) then
                --print("2")
                local ent = self.Owner:GetEyeTrace().Entity
                if  IsValid( ent ) and  ent:GetNWBool("DeadBody",false)   then
                    local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
                   local fract = math.TimeFraction(self.StartTime, self.EndTime, CurTime())
                        surface.SetDrawColor(Color(255, 0, 0, 255))
                        surface.SetDrawColor(Color(120, 120, 120, 130))
                        surface.DrawRect(ScrW() / 2 - (160 / 2), ScrH() / 2 + 10, 160, 10)
                        surface.SetFont("TabLarge")
                        local w, h = surface.GetTextSize("Scanning body...")
                        surface.SetTextPos(ScrW() / 2 - (w / 2), ScrH() / 2 - h)
                        surface.SetTextColor(Color(255, 0, 0, 255))
                        surface.DrawText("Scanning body...")
                        surface.SetDrawColor(Color(255, 0, 0, 255))
                        surface.DrawRect(ScrW() / 2 - (160 / 2), ScrH() / 2 + 10, math.Clamp(160 * fract, 0, 160), 10)

                else
                    self.StartTime = CurTime()
                    self.EndTime = CurTime() + 8
                end
            else
                self.Started = false
            end
        end
    end
end
SWEP.StartTime = 0
SWEP.EndTime = 0
SWEP.NextBeep = 0
if SERVER then
    function SWEP:PrimaryAttack()
        local ent = self.Owner:GetEyeTrace().Entity
        if  not self.Started and IsValid( ent ) and ent.DeadBody then
            self.StartTime = CurTime()
            self.Started = true
        end
    end

    function SWEP:Think()
        if self.Owner:KeyDown(IN_ATTACK) then

            if not self.Started then
                self.StartTime = CurTime()
            end
            self.Started = true
        else
            self.Started = false

        end
        if self.Started then

            if self.Owner:KeyDown(IN_ATTACK) then
                --print("2")
                local ent = self.Owner:GetEyeTrace().Entity
                if  IsValid( ent ) and  ent:GetNWBool("DeadBody",false)  and (ent.DecayTime - CurTime()) > 10 then
                    if self.NextBeep < CurTime() then
                        self.NextBeep = CurTime() + 1
                        self.Owner:EmitSound("buttons/blip2.wav")
                    end

                    local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()

                        if (CurTime() - self.StartTime) >= 8 then
                            self.StartTime = CurTime()
                            self.Owner:PrintMessage( HUD_PRINTCENTER, ent:GetNWString("Name").." Killed by "..ent:GetNWString("Killer") )
                            self.Owner:PrintMessage( HUD_PRINTTALK, ent:GetNWString("Name").." Killed by "..ent:GetNWString("Killer") )
                            if IsValid(ent.KillerEnt) then
                             --   ent.KillerEnt:wanted(self.Owner, "Killing "..ent:GetNWString("Name"));

                                if ent.KillerEnt != self.Owner then self.Owner:addMoney(50) end
                                ent.DecayTime = CurTime() + 9
                            end
                        end
                else
                        self.StartTime = CurTime()
                        if self.NextBeep < CurTime() then
                            self.NextBeep = CurTime() + 1
                            self.Owner:EmitSound("player/suit_denydevice.wav")
                       end
                end
            else
                self.Started = false
            end
        end
    end

end