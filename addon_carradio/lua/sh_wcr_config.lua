wyozicr.Stations = {
	{
		Name = "Classic Rap",
		Link = "http://listen.radionomy.com/a-better-old-school-classic-rap-station.m3u"
	},
	{
		Name = "DUBTHUGZ",
		Link = "https://www.internet-radio.com/servers/tools/playlistgenerator/?u=http://uk1.internet-radio.com:15634/listen.pls&t=.pls"
	},
	{
		Name = "HouseTime.FM",
		Link = "http://listen.housetime.fm/dsl.pls"
	},
	{
		Name = "ChartHits.FM",
		Link = "http://yp.shoutcast.com/sbin/tunein-station.pls?id=31645"
	},
	{
		Name = "French Kiss FM",
		Link = "http://stream.frenchkissfm.com/listen.pls"
	},
	{
		Name = "Soma.fm Groove Salad",
		Link = "http://somafm.com/groovesalad.pls"
	},
	{
		Name = "Soma.fm Beat Blender",
		Link = "http://somafm.com/beatblender.pls"
    },
    {
        Name = "Rock Radio",
        Link = "http://77.74.192.50:8000/listen.pls"
    },
    {
        Name = "DEATH.fm",
        Link = "http://hi.death.fm/listen.pls"
    },


}

-- Uncomment the following line if you wish to impose limits on usage of the car radio
--wyozicr.AllowedUsergroups = {"superadmin", "admin", "donator"}