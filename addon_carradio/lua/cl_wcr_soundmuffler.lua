if true then return end
local wcr_mufflesounds = CreateConVar("wyozicr_mufflesounds", "1", FCVAR_ARCHIVE)

hook.Add("Think", "WCR_SoundMuffler", function()
	local muffled = wyozicr.SoundMuffled
	local shouldbemuffled = IsValid(LocalPlayer():GetVehicle()) and wcr_mufflesounds:GetBool()

	if muffled ~= shouldbemuffled then
		LocalPlayer():SetDSP(shouldbemuffled and 31 or 0, false)
		wyozicr.SoundMuffled = shouldbemuffled
	end
end)