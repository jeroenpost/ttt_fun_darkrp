CreateClientConVar("wyozicr_reqcarstereodist", "1024", true, false)
CreateClientConVar("wyozicr_stereoenabled", "1", true, false)
CreateClientConVar("wyozicr_stereovolume", "50", true, false)

local function GetDriver(car)
	if not IsValid(car) then return end

	local plys = player.GetAll()
	for i=1,#plys do
		if plys[i]:GetVehicle() == car then
			return plys[i]
		end
	end
end

wyozicr.StartedRadios = wyozicr.StartedRadios or {}

hook.Add("Think", "WCR_RadioUpdater", function()
	local possible_ents = {}
	table.Add(possible_ents, ents.FindByClass("prop_vehicle_jeep"))
	--table.Add(possible_ents, ents.FindByClass("blackjack_table"))
    --table.Add(possible_ents, ents.FindByClass("prop_vehicle_prisoner_pod"))
	for _,car in pairs(possible_ents) do
		local carpos = car:GetPos()
		local cardriver = GetDriver(car)

		if car.WCR_Player then

			local localveh = wyozicr.GetCarEntity(LocalPlayer():GetVehicle())

			local volumemul = 0.5

			if IsValid(localveh) and localveh == car then
				car.WCR_Player:SetPos(LocalPlayer():EyePos())
				volumemul = 1
			else
				car.WCR_Player:SetPos(carpos)
			end

			local vol = GetConVarNumber("wyozicr_stereovolume") or 50
			vol = vol / 100
			vol = vol * volumemul
            if not IsValid(car.WCR_Player) or not type(car.WCR_Player) == "IGModAudioChannel" or not car.WCR_Player.SetVolume or not vol or vol < 0.01 or vol > 1 then return end
			car.WCR_Player:SetVolume(vol)

		end

		local pos = LocalPlayer():GetPos()

		local cardist = carpos:Distance(pos)

		local station = car:GetNWInt("wcr_station")
		local stereourl = car:GetNWString("wcr_stationlink")
		local isvalidsurl = stereourl and stereourl ~= ""
		if not isvalidsurl then
			stereourl = wyozicr.Stations[station] and wyozicr.Stations[station].Link
			isvalidsurl = stereourl and stereourl ~= ""
		end

		local stereoenabled = GetConVarNumber("wyozicr_stereoenabled") == 1

		local playerstatus = car.WCR_Player ~= nil
		local shouldbeplaying = isvalidsurl and cardist < GetConVarNumber("wyozicr_reqcarstereodist") and not car.WCR_StartingStereo and stereoenabled

		if playerstatus and ( not shouldbeplaying or (isvalidsurl and car.WCR_LastValidSurl and car.WCR_LastValidSurl ~= stereourl) ) then
			car.WCR_Player:Stop()
			car.WCR_Player = nil
			--MsgN("Stopping car stereo")
		elseif not playerstatus and shouldbeplaying then
			car.WCR_StartingStereo = true
			car.WCR_LastValidSurl = stereourl
			--MsgN("Starting car stereo: ", stereourl)
			sound.PlayURL(stereourl, "3d", function(chan)
				car.WCR_StartingStereo = false
				car.WCR_Player = chan
				wyozicr.StartedRadios[car] = chan
			end)
		end
	end

	for car,radio in pairs(wyozicr.StartedRadios) do
		if not IsValid(car) and IsValid(radio) then
			radio:Stop()
			wyozicr.StartedRadios[car] = nil
		end
	end
end)