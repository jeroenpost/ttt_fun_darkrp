Wyozi Car Radio adds a new in-car HUD, that allows players to browse through a selection of radio channels.

##Features

---

**Extremely easy installation** Wow your playerbase and give players a reason to use cars.

**Gamemode agnostic** While the script was developed in DarkRP, it should require only a plug-n-play installation for all other gamemodes as well.

**Synchronized** The radio station changes are automatically broadcasted to all players.

**Usergroup restriction support**

**Easy to modify** Adding a new radio station requires only a simple file edit.

**Supports TDMCars and SCars!**

## Installation

---

Just unzip the addon to your addons folder and you're good to go!
