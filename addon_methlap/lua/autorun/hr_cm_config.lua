-- Heisenberg Recipes: Crystal Meth
-- Console command to spawn buyer 'hr_cm_spawnnpc name'.
-- Console command to remove buyer 'hr_cm_removenpc name'.
-- Steam Community Guide: http://steamcommunity.com/sharedfiles/filedetails/?id=443724752&tscn=1431885930 by Heisenberg.

HR_CrystalMeth = {}

HR_CrystalMeth.DrawDistance = 256; -- Change panels draw distance here.

HR_CrystalMeth.UseNPC = true; -- If you are using NPC you will be required to pick meth by pressing E on it and then contact NPC to sell it, press E on it.
HR_CrystalMeth.NameNPC = "Amphetamine Buyer"; -- If you are using NPC you will be required to pick meth by pressing E on it and then contact NPC to sell it, press E on it.
HR_CrystalMeth.DamageDrop = true; -- If you are are carrying meth and you got damaged you'll drop meth.
HR_CrystalMeth.DisableCough = false; -- Setting it to true will disable coughing at all.
HR_CrystalMeth.DisableViewScreenPunch = false; -- Setting it to true will disable view screen punching.
HR_CrystalMeth.MakeWantedOnSell = true; -- If true - makes player wanted when he tries to sell meth to buyer.
HR_CrystalMeth.PerfectCookReward_Mod = 3; -- Perfect cook reward modifier (If player made 100% amphetamine, it's price will be tripled.)
										  -- Type number 1 instead of 3 if you don't want to have modifier.
										  -- 1 - NO MODIFIER
										  -- 2 - DOUBLES PRICE
										  -- 3 - TRIPLES PRICE
										  -- etc.

HR_CrystalMeth.CanOpenBoxes = true; -- Can you open boxes(only aluminum affected)?
HR_CrystalMeth.HintsEnabled = true; -- Are hints on Alt+E enabled(for pots and tank)?
HR_CrystalMeth.HintTime = 3600; -- Hint time(when you choose job you'll receive a formula hint).
HR_CrystalMeth.CopSellEnabled = true; -- If true - cops can't pick meth and bring it to buyer, instead they automaticly sell it for price_of_meth*modifier_below.
HR_CrystalMeth.CopJobs = {"Civil Protection", "Mayor"} -- Jobs which can pick meth with 'E' (automaticly sells it for price_of_meth*modifier_below)
HR_CrystalMeth.CopSellMod = 0.25; -- Modifier for cops to sell meth. 

HR_CrystalMeth.Health_Pots = 50; -- Health for pots.
HR_CrystalMeth.Damagable_Pots = true; -- Can you damage and destroy pots?

-- Aluminum Item Table
HR_CrystalMeth.AluminumOne_Amount = 1; -- Amount of aluminum in 1 bar.
HR_CrystalMeth.AluminumOne_TextColor = Color(150, 150, 150, 255); -- Text color of panel.

HR_CrystalMeth.AluminumBox_Amount = 10; -- Amount of aluminum in box.
HR_CrystalMeth.AluminumBox_Text = "Aluminum"; -- Aluminum draw name.
HR_CrystalMeth.AluminumBox_TextCopyright = "Put it to Chemical Tank"; -- Some sort of hint below on box.

-- Cyfluthrin Item Table
HR_CrystalMeth.CyfluthrinCan_Amount = 20; -- Amount of cyfluthrin in gas can.
HR_CrystalMeth.CyfluthrinCan_Text = "Cyfluthrin"; -- Cyfluthrin draw name. 
HR_CrystalMeth.CyfluthrinCan_TextColor = Color(140, 140, 255, 255); -- Text color of panel.
HR_CrystalMeth.CyfluthrinCan_TextCopyright = "Put it to Chemical Tank"; -- Some sort of hint below on box.

-- Chemical Tank Table
HR_CrystalMeth.ChemicalTank_Al_Hold = 100; -- Do not change it.
HR_CrystalMeth.ChemicalTank_Cl_Hold = 100; -- Do not change it.
HR_CrystalMeth.ChemicalTank_Mt_Hold = 100; -- Do not change it.
HR_CrystalMeth.ChemicalTank_Ia_Hold = 10; -- Do not change it.

HR_CrystalMeth.ChemicalTank_Health = 500; -- Amount of health for Chemical Tank.
HR_CrystalMeth.ChemicalTank_Destroyable = true; -- Can you destroy Chemical Tank?

-- If pressure more than max then tank explodes.
-- If pressure less than min then progress regresses.

HR_CrystalMeth.ChemicalTank_S1_MaxPressure = 100; -- Do not change it. If pressure>100 on 1st stage. It's gonna explode.
HR_CrystalMeth.ChemicalTank_S1_MinPressure = 50; -- Do not change it.

HR_CrystalMeth.ChemicalTank_S2_MaxPressure = 50; -- Do not change it. If pressure>100 on 1st stage. It's gonna explode.
HR_CrystalMeth.ChemicalTank_S2_MinPressure = 25; -- Do not change it.

HR_CrystalMeth.ChemicalTank_S3_MaxPressure = 25; -- Do not change it. If pressure>100 on 1st stage. It's gonna explode.
HR_CrystalMeth.ChemicalTank_S3_MinPressure = 5; -- Do not change it.

HR_CrystalMeth.Pressure_S1_CD = 1; -- Pressure CD for stage 1.
HR_CrystalMeth.Pressure_S1_Amount = 1; -- Amount of pressure which goes for stage 1.

HR_CrystalMeth.Pressure_S2_CD = 1; -- Pressure CD for stage 3.
HR_CrystalMeth.Pressure_S2_Amount = 1; -- Amount of pressure which goes for stage 2.

HR_CrystalMeth.Pressure_S3_CD = 1; -- Pressure CD for stage 2.
HR_CrystalMeth.Pressure_S3_Amount = 1; -- Amount of pressure which goes up for stage 3.

HR_CrystalMeth.ProgressCD = 1; -- Progress cooldown.
HR_CrystalMeth.ProgressAmount = 5; -- Amount of progress for each cooldown.

-- Meth Item Table
HR_CrystalMeth.MethName = "Amphetamine"; -- Meth draw name.
HR_CrystalMeth.MethName_Color = Color(0, 161, 255, 255); -- Meth name draw color.
HR_CrystalMeth.MethPrice_Mod = 1150; -- 1000$ for each quality % percent.
HR_CrystalMeth.MethQualityFormula = {55, 64, 80, 5} -- Random formula for each restart generates here. Don't touch it.
HR_CrystalMeth.MethQualityFormula_WrongInfo = {0,0,0,0} -- You can change these values, but these ones are optimal. Player will receive 60-70% meth quality with these errors.

-- Water Item Table
HR_CrystalMeth.WaterOne_Amount = 8; -- Amount of water in jug.
HR_CrystalMeth.WaterOne_Text = "Water"; -- Water draw name.
HR_CrystalMeth.WaterOne_TextColor = Color(100, 100, 255, 255); -- Text color of panel.
HR_CrystalMeth.WaterOne_TextCopyright = "Put it to Pot"; -- Some sort of hint below on box.

-- Methylamine Item Table
HR_CrystalMeth.Methylamine_Amount = 80; -- Amount of methylamine in barrel.
HR_CrystalMeth.Methylamine_Text = "Methylamine"; -- Methylamine draw name.
HR_CrystalMeth.Methylamine_TextColor = Color(255, 255, 255, 255); -- Text color of panel.
HR_CrystalMeth.Methylamine_TextCopyright = "For Chemical Tank"; -- Some sort of hint below on box.

-- Red Phosphorus
HR_CrystalMeth.RedPOne_Amount = 1; -- Amount of red phosphorus in 1 piece.
HR_CrystalMeth.RedPOne_TextColor = Color(125, 25, 25, 255); -- Text color of panel.

HR_CrystalMeth.RedPBox_Amount = 10; -- Amount of Red Phosphorus in box.
HR_CrystalMeth.RedPBox_Text = "Red Phosphorus"; -- Red Phosphorus draw name.
HR_CrystalMeth.RedPBox_TextCopyright = "Put it to Pot"; -- Some sort of hint below on box.


-- Pot (Water/RedP)
HR_CrystalMeth.PotWR_WaterMax = 4; -- Max amount of Water in WR pot.
HR_CrystalMeth.PotWR_RedpMax = 4; -- Max amount of Red Phosphorus in WR pot.

HR_CrystalMeth.PotWR_SpoilDamage = true; -- Can you get damage if product in spoiled state?
HR_CrystalMeth.PotWR_SpoilDamageCD = 2; -- Spoil damage cooldown.
HR_CrystalMeth.PotWR_SpoilDamageRAD = 128; -- Spoil damage radius.
HR_CrystalMeth.PotWR_SpoilDamage = 5; -- Spoil damage amount.

HR_CrystalMeth.PotWR_StartTemp = 20; -- Start temperature for pot.
HR_CrystalMeth.PotWR_FireTemp = 200; -- Temperature when pot ignites.
HR_CrystalMeth.PotWR_CritTemp = 150; -- Critical temperature when product spoils.
HR_CrystalMeth.PotWR_BoilTemp = 100; -- Temperature when cook time starts counting.
HR_CrystalMeth.PotWR_CookTime = 60; -- Cook time in seconds.
HR_CrystalMeth.PotWR_TextColor = Color(255, 25, 0, 50); -- Draw color.
HR_CrystalMeth.PotWR_TextColorReady = Color(255, 25, 0, 255); -- Draw color when ready.
HR_CrystalMeth.PotWR_Text = "Cooking Pot"; -- Draw name.


-- Stove
HR_CrystalMeth.Stove_Health = 500; -- Amount of health for stove.
HR_CrystalMeth.Stove_Destroyable = true; -- Can you destroy stove?
HR_CrystalMeth.StoveGas = 12000; -- Amount of gasoline in stove.
HR_CrystalMeth.StoveGasColor = Color(150, 75, 0, 100); -- Gasoline bar color.
HR_CrystalMeth.StoveGas_ConsLow = 1; -- Consumption for small burner.
HR_CrystalMeth.StoveGas_TempLow = 1; -- Temperature gain for small burner.
HR_CrystalMeth.StoveGas_ConsBig = 2; -- Consumption for big burner.
HR_CrystalMeth.StoveGas_TempBig = 4; -- Temperature gain for big burner.
HR_CrystalMeth.StoveGas_TempLim = 1200; -- The highest temperature stove could heat pots to.


-- Sulfur Dioxide
HR_CrystalMeth.SulfurD_Amount = 1; -- Amount of Sulfur Trioxide in 1 piece.
HR_CrystalMeth.SulfurD_TextColor = Color(230, 210, 40, 255); -- Text color of panel.

HR_CrystalMeth.SulfurDBox_Amount = 10; -- Amount of Sulfur Trioxide in box.
HR_CrystalMeth.SulfurDBox_Text = "Sulfur Trioxide"; -- Sulfur Trioxide draw name.
HR_CrystalMeth.SulfurDBox_TextCopyright = "Put it to Pot"; -- Some sort of hint below on box.


-- Pot (Water/SulfurD)
HR_CrystalMeth.PotWD_WaterMax = 8; -- Max amount of Water in WD pot.
HR_CrystalMeth.PotWD_SulfurMax = 4; -- Max amount of Sulfur Trioxide in WD pot.

HR_CrystalMeth.PotWD_SpoilDamage = true;  -- Can you get damage if product in spoiled state?
HR_CrystalMeth.PotWD_SpoilDamageCD = 4; -- Spoil damage cooldown.
HR_CrystalMeth.PotWD_SpoilDamageRAD = 128; -- Spoil damage radius.
HR_CrystalMeth.PotWD_SpoilDamage = 0; -- Not harmful. But player coughs.

HR_CrystalMeth.PotWD_StartTemp = 20; -- Start temperature for pot.
HR_CrystalMeth.PotWD_FireTemp = 500; -- Temperature when pot ignites.
HR_CrystalMeth.PotWD_CritTemp = 400; -- Critical temperature when cook timer starts.
HR_CrystalMeth.PotWD_BoilTemp = 100; -- Doesn't affect on anything for this pot.
HR_CrystalMeth.PotWD_CookTime = 30; -- Cook time in seconds.
HR_CrystalMeth.PotWD_TextColor = Color(255, 190, 25, 50); -- Draw color.
HR_CrystalMeth.PotWD_TextColorReady = Color(255, 190, 25, 255); -- Draw color when ready.
HR_CrystalMeth.PotWD_Text = "Chemical Pot"; -- Draw name.


-- Big Pot (S-Acid/P-Acid)
HR_CrystalMeth.PotSA_SulfurMax = 5; -- Max amount of Sulfuric Acid in WD pot.
HR_CrystalMeth.PotSA_PhosphorMax = 5; -- Max amount of Phosphoric Acid in WD pot.

HR_CrystalMeth.PotSA_SpoilDamage = true;  -- Can you get damage if product in spoiled state?
HR_CrystalMeth.PotSA_SpoilDamageCD = 4; -- Spoil damage cooldown.
HR_CrystalMeth.PotSA_SpoilDamageRAD = 128; -- Spoil damage radius.
HR_CrystalMeth.PotSA_SpoilDamage = 20; -- Spoil damage amount.

HR_CrystalMeth.PotSA_StartTemp = 20; -- Start temperature for pot.
HR_CrystalMeth.PotSA_FireTemp = 250; -- Temperature when pot ignites.
HR_CrystalMeth.PotSA_CritTemp = 200; -- Critical temperature when product spoils.
HR_CrystalMeth.PotSA_BoilTemp = 100; -- Temperature when cook time starts counting.
HR_CrystalMeth.PotSA_CookTime = 60; -- Cook time in seconds.
HR_CrystalMeth.PotSA_TextColor = Color(100, 175, 155, 50); -- Draw color.
HR_CrystalMeth.PotSA_TextColorReady = Color(127, 255, 159, 255); -- Draw color when ready.
HR_CrystalMeth.PotSA_Text = "Cooking Pan"; -- Draw name.


-- Chemical Glass
HR_CrystalMeth.Glass_MaxAcid = 10; -- Max amount of Acid you can collect.

HR_CrystalMeth.Glass_Phosphoric_Acid = "Phosphoric Acid"; -- Name for PA.
HR_CrystalMeth.Glass_Phosphoric_Acid_Color = Color(255, 25, 25); -- Color for PA.
HR_CrystalMeth.Glass_Sulfuric_Acid = "Sulfuric Acid"; -- Name for SA.
HR_CrystalMeth.Glass_Sulfuric_Acid_Color = Color(255, 190, 25, 255); -- Color for SA.
HR_CrystalMeth.Glass_Intermediate_Acid = "Intermediate Acid"; -- Name or IA.
HR_CrystalMeth.Glass_Intermediate_Acid_Color = Color(127, 255, 159, 255); -- Color for IA.
HR_CrystalMeth.Glass_CrackOnShoot = true; -- Can you destroy glass?