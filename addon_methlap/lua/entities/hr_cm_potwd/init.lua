AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");

function ENT:Initialize()
	self:SetModel("models/props_interiors/pot02a.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);
	
	self:SetColor(Color(127, 159, 255, 255));
	
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);
	
	self:SetNWInt("water", 0);
	self:SetNWInt("sulfurd", 0);
	
	self:SetNWBool("1state_ready", false);

	self:SetNWInt("temp", HR_CrystalMeth.PotWD_StartTemp);
	self:SetNWInt("cook_time", HR_CrystalMeth.PotWD_CookTime);
	--self:SetNWInt("temp", 190);
	
	self:SetNWInt("fire_temp", HR_CrystalMeth.PotWD_FireTemp);
	self:SetNWBool("onfire", false);
	
	self:SetNWInt("crit_temp", HR_CrystalMeth.PotWD_CritTemp);
	self:SetNWInt("boil_temp", HR_CrystalMeth.PotWD_BoilTemp);

	self:SetPos(self:GetPos()+Vector(0, 0, 8));
	self:GetPhysicsObject():SetVelocity(self:GetUp()*2);
	
	self:SetCollisionGroup(COLLISION_GROUP_WEAPON);
	self.waterSound = CreateSound(self, Sound("ambient/levels/canals/toxic_slime_loop1.wav"));
	self.waterSound:SetSoundLevel(1);
	self.waterSound:PlayEx(1, 120);
	
	self:SetHealth(HR_CrystalMeth.Health_Pots);
	self:GetPhysicsObject():SetMass(105);
end;
 
function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("hr_cm_potwd");
	ent:SetPos(trace.HitPos + trace.HitNormal * 16);
	ent:Spawn();
	ent:Activate();
     
	return ent;
end;

function ENT:OnTakeDamage(dmginfo)
	if (HR_CrystalMeth.Damagable_Pots) then
		self:SetHealth(math.Clamp(self:Health()-dmginfo:GetDamage(), 0, HR_CrystalMeth.Health_Pots));		
		self:EmitSound("weapons/fx/rics/ric"..table.Random({1, 2, 4, 5})..".wav", 70, 100);
		
		local effectdata = EffectData();
		effectdata:SetStart(dmginfo:GetDamagePosition());
		effectdata:SetOrigin(dmginfo:GetDamagePosition());
		effectdata:SetAngles(dmginfo:GetAttacker():GetAngles());
		effectdata:SetScale(1);
		util.Effect("StunstickImpact", effectdata);		
		
		if (self:Health() == 0) then
			self:Remove();
		end;
	end;
end;

function ENT:Use(activator, caller)
local curTime = CurTime();
	if (activator:GetEyeTrace().Entity == self) then
		if (!self.nextUse or curTime >= self.nextUse) then		
			if (activator:KeyDown(IN_SPEED)) then
				self:SetPos(self:GetPos()+Vector(0, 0, 1));
				self:SetAngles(activator:GetAngles()+Angle(0, 90, 0));
				self:GetPhysicsObject():SetVelocity(self:GetUp()*2);
			end;
			if (HR_CrystalMeth.HintsEnabled) then
				if (activator:KeyDown(IN_WALK)) then
					activator:SendLua("LocalPlayer():EmitSound('ui/buttonclick.wav', 70, 100)");
					activator:SendLua("local tab = {Color(255,255,255),[[How to cook ]], HR_CrystalMeth.Glass_Sulfuric_Acid_Color,[["..HR_CrystalMeth.Glass_Sulfuric_Acid.."]],Color(255,255,255),[[:]]}chat.AddText(unpack(tab))");
					activator:SendLua("local tab = {Color(255,255,255),[[1) Fill ]], HR_CrystalMeth.PotWD_TextColor,[["..HR_CrystalMeth.PotWD_Text.."]],Color(255,255,255),[[ with ]], HR_CrystalMeth.WaterOne_TextColor,[["..HR_CrystalMeth.WaterOne_Text.."]], Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");
					activator:SendLua("local tab = {Color(255,255,255),[[2) Boil ]], HR_CrystalMeth.PotWD_TextColor,[["..HR_CrystalMeth.PotWD_Text.."]], Color(255,255,255),[[ with]], HR_CrystalMeth.WaterOne_TextColor,[["..HR_CrystalMeth.WaterOne_Text.."]], Color(255,255,255),[[ on stove.]]}chat.AddText(unpack(tab))");
					activator:SendLua("local tab = {Color(255,255,255),[[3) When temperature reaches ]], Color(255,150,0),[["..HR_CrystalMeth.PotWD_CritTemp.."C]], Color(255,255,255),[[ add ]], HR_CrystalMeth.SulfurD_TextColor,[["..HR_CrystalMeth.SulfurDBox_Text.."]], Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");
					activator:SendLua("local tab = {Color(255,255,255),[[4) Keep temperature between ]], Color(255,150,0),[["..HR_CrystalMeth.PotWD_CritTemp.."C]], Color(255,255,255),[[ and ]], Color(255,150,0),[["..HR_CrystalMeth.PotWD_FireTemp.."C]], Color(255,255,255),[[ for ]],Color(255,255,0),[["..HR_CrystalMeth.PotWD_CookTime.."s]],Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");
					activator:SendLua("local tab = {Color(255,255,255),[[5) Collect ]], HR_CrystalMeth.Glass_Sulfuric_Acid_Color,[["..HR_CrystalMeth.Glass_Sulfuric_Acid.."]], Color(255,255,255),[[ with ]], Color(90,90,90),[[Chemical Glass]], Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");
				end;
			end;			
			self.nextUse = curTime + 0.5;
		end;
	end;
end;

local boilTable = {"ambient/levels/canals/toxic_slime_gurgle2.wav", "ambient/levels/canals/toxic_slime_gurgle4.wav", "ambient/levels/canals/toxic_slime_gurgle7.wav"}
local critTable = {"ambient/levels/canals/toxic_slime_sizzle2.wav", "ambient/levels/canals/toxic_slime_sizzle3.wav", "ambient/levels/canals/toxic_slime_sizzle4.wav"}
function ENT:Think()
	if (self:GetNWInt("temp")>=self:GetNWInt("fire_temp")) then
		if (!self:GetNWBool("onfire")) then
			self:GetPhysicsObject():SetVelocity((self:GetForward()*math.random(-48, 48))+(self:GetRight()*math.random(-48, 48))+(self:GetUp()*150));	
			self:SetNWBool("onfire", true);
			self:EmitSound("ambient/fire/ignite.wav", 70, 100);
			self:Ignite(5);
			timer.Simple(5, function()
				self:Remove();
			end);			
		end;
	elseif (self:GetNWInt("temp")>=self:GetNWInt("crit_temp") and (self:GetNWInt("cook_time")!=0)) then
		self:GetPhysicsObject():SetVelocity((self:GetForward()*math.random(-2, 2))+(self:GetRight()*math.random(-2, 2))+(self:GetUp()*math.random(-4, 4)));	
		if (!self.nextSoundCrit or CurTime() >= self.nextSoundCrit) then
			self:EmitSound(table.Random(critTable), 70, 95);
			if (self:GetNWInt("water")>0) then
				local removeChance = math.random(1, 10);
				if (removeChance == 10) then
					self:SetNWInt("water", self:GetNWInt("water")-1);
				end;
				if (self:GetNWInt("cook_time")!=0) then
					if ((self:GetNWInt("water")>0) and (self:GetNWInt("sulfurd")>0)) then
						self:SetNWInt("cook_time", self:GetNWInt("cook_time")-1);
						if (self:GetNWInt("cook_time")==0) and (!self:GetNWBool("1state_ready")) then
							self:SetNWBool("1state_ready", true);
							self:EmitSound("ambient/levels/canals/toxic_slime_gurgle5.wav", 70, 95);
						end;
						--print("cook time water/sulfur-- "..self:GetNWInt("cook_time"))
					end;
				end;
			else
				self:GetPhysicsObject():SetVelocity((self:GetForward()*math.random(-48, 48))+(self:GetRight()*math.random(-48, 48))+(self:GetUp()*150));	
				self:SetNWBool("onfire", true);
				self:EmitSound("ambient/fire/ignite.wav", 70, 100);
				self:Ignite(5);
				timer.Simple(5, function()
					self:Remove();
				end);
			end;
			self.nextSoundCrit = CurTime() + 2.25;
		end;
		if (HR_CrystalMeth.PotWD_SpoilDamage) then
			if (self:GetNWInt("sulfurd")>0) then
				if (!self.nextCough or CurTime() >= self.nextCough) then
					for k, v in pairs(ents.FindInSphere(self:GetPos(), HR_CrystalMeth.PotWD_SpoilDamageRAD)) do
						if IsValid(v) then
							if (v:IsPlayer() and v:Alive()) then
								v:TakeDamage(HR_CrystalMeth.PotWD_SpoilDamage, v, v);
								if (!HR_CrystalMeth.DisableCough) then
									v:EmitSound("ambient/voices/cough"..math.random(1, 4)..".wav", 70, 105);
								end;
								if (!HR_CrystalMeth.DisableViewScreenPunch) then
									v:ViewPunch(Angle(math.random(8, 12), 0, 0 ));
								end;
							end;
						end;				
					end;
					self.nextCough = CurTime()+HR_CrystalMeth.PotWD_SpoilDamageCD;
				end;
			end;
		end;
	elseif (self:GetNWInt("temp")>=self:GetNWInt("boil_temp") and (self:GetNWInt("cook_time")!=0)) then	
		self:GetPhysicsObject():SetVelocity((self:GetForward()*math.random(-2, 2))+(self:GetRight()*math.random(-2, 2)));
		if (!self.nextSound or CurTime() >= self.nextSound) then	
			self:EmitSound(table.Random(boilTable), 50, 90);
			self.nextSound = CurTime() + 1.25;
		end;
	end;
	--print("temp - "..self:GetNWInt("temp"))
	if (!self.nextTempDown or CurTime() >= self.nextTempDown) then	
		self:SetNWInt("temp", math.Clamp(self:GetNWInt("temp")-1, HR_CrystalMeth.PotWD_StartTemp, HR_CrystalMeth.PotWD_FireTemp))
		self.nextTempDown = CurTime() + 2;
	end;	
end;
