include("shared.lua");

surface.CreateFont("HR_CR_FlyFont", {
	font = "Arial",
	size = 32,
	weight = 600,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
});

function ENT:Initialize()	
	self.smokeTime = CurTime();
	self.smokePos = ParticleEmitter(self:GetPos()+(self:GetUp()));
end;

local matTable = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16"}
function ENT:Think()
	if (self.smokeTime < CurTime()) then
		if (self:GetNWInt("temp") > HR_CrystalMeth.PotWR_StartTemp) then
			if (self:GetNWInt("temp") >= self:GetNWInt("crit_temp")) then
				if (self:GetNWInt("cook_time")!=0) then
					if (self:GetNWInt("sulfurd")>0) then
						local smoke = self.smokePos:Add("particle/smokesprites_00"..table.Random(matTable), self:GetPos()+(self:GetRight()*-5));
						smoke:SetVelocity(Vector(math.random(-24, 24), math.random(-24, 24), Vector(0, 0, 512)));
						smoke:SetDieTime(1);
						smoke:SetStartAlpha(100);
						smoke:SetEndAlpha(200);
						smoke:SetStartSize(4);
						smoke:SetEndSize(12);
						smoke:SetRoll(math.random(180, 480));
						smoke:SetRollDelta(math.random(-3, 3));
						smoke:SetColor(255, 190, 25);
						smoke:SetGravity(Vector(0, 0, 128));
						smoke:SetAirResistance(256);	
					else
						local smoke = self.smokePos:Add("particle/smokesprites_00"..table.Random(matTable), self:GetPos()+(self:GetRight()*-5));
						smoke:SetVelocity(Vector(math.random(-24, 24), math.random(-24, 24), Vector(0, 0, 512)));
						smoke:SetDieTime(1);
						smoke:SetStartAlpha(100);
						smoke:SetEndAlpha(200);
						smoke:SetStartSize(4);
						smoke:SetEndSize(12);
						smoke:SetRoll(math.random(180, 480));
						smoke:SetRollDelta(math.random(-3, 3));
						smoke:SetColor(225, 225, 225);
						smoke:SetGravity(Vector(0, 0, 128));
						smoke:SetAirResistance(256);				
					end;
				elseif (self:GetNWInt("cook_time")==0) then
					if (self:GetNWInt("sulfurd")>0) then
						local smoke = self.smokePos:Add("particle/smokesprites_00"..table.Random(matTable), self:GetPos()+(self:GetRight()*-5));
						smoke:SetVelocity(Vector(math.random(-24, 24), math.random(-24, 24), Vector(0, 0, 512)));
						smoke:SetDieTime(1);
						smoke:SetStartAlpha(100);
						smoke:SetEndAlpha(200);
						smoke:SetStartSize(4);
						smoke:SetEndSize(12);
						smoke:SetRoll(math.random(180, 480));
						smoke:SetRollDelta(math.random(-3, 3));
						smoke:SetColor(225, 255, 25);
						smoke:SetGravity(Vector(0, 0, 128));
						smoke:SetAirResistance(256);						
					end;				
				end;
			elseif (self:GetNWInt("temp") >= self:GetNWInt("boil_temp")) then
				if (self:GetNWInt("cook_time")!=0) then
					if ((self:GetNWInt("water")>0) and (self:GetNWInt("sulfurd")>0)) then
						local smoke = self.smokePos:Add("particle/smokesprites_00"..table.Random(matTable), self:GetPos()+(self:GetRight()*-5));
						smoke:SetVelocity(Vector(math.random(-16, 16), math.random(-16, 16), Vector(0, 0, 512)));
						smoke:SetDieTime(1);
						smoke:SetStartAlpha(100);
						smoke:SetEndAlpha(0);
						smoke:SetStartSize(6);
						smoke:SetEndSize(0.1);
						smoke:SetRoll(math.random(180, 480));
						smoke:SetRollDelta(math.random(-3, 3));
						smoke:SetColor(255, 190, 25);
						smoke:SetGravity(Vector(0, 0, 128));
						smoke:SetAirResistance(256);
					else
						local smoke = self.smokePos:Add("particle/smokesprites_00"..table.Random(matTable), self:GetPos()+(self:GetRight()*-5));
						smoke:SetVelocity(Vector(math.random(-16, 16), math.random(-16, 16), Vector(0, 0, 512)));
						smoke:SetDieTime(1);
						smoke:SetStartAlpha(100);
						smoke:SetEndAlpha(0);
						smoke:SetStartSize(6);
						smoke:SetEndSize(0.1);
						smoke:SetRoll(math.random(180, 480));
						smoke:SetRollDelta(math.random(-3, 3));
						smoke:SetColor(225, 225, 255);
						smoke:SetGravity(Vector(0, 0, 128));
						smoke:SetAirResistance(256);					
					end;
				elseif (self:GetNWInt("cook_time")==0) then
						local smoke = self.smokePos:Add("particle/smokesprites_00"..table.Random(matTable), self:GetPos()+(self:GetRight()*-5));
						smoke:SetVelocity(Vector(math.random(-16, 16), math.random(-16, 16), Vector(0, 0, 512)));
						smoke:SetDieTime(1);
						smoke:SetStartAlpha(100);
						smoke:SetEndAlpha(0);
						smoke:SetStartSize(6);
						smoke:SetEndSize(0.1);
						smoke:SetRoll(math.random(180, 480));
						smoke:SetRollDelta(math.random(-3, 3));
						smoke:SetColor(225, 255, 25);
						smoke:SetGravity(Vector(0, 0, 128));
						smoke:SetAirResistance(256);				
				end;
			else
				local smoke = self.smokePos:Add("particle/smokesprites_00"..table.Random(matTable), self:GetPos()+(self:GetRight()*-5));
				smoke:SetVelocity(Vector(math.random(-16, 16), math.random(-16, 16), Vector(0, 0, 512)));
				smoke:SetDieTime(1);
				smoke:SetStartAlpha(100);
				smoke:SetEndAlpha(0);
				smoke:SetStartSize(2);
				smoke:SetEndSize(6);
				smoke:SetRoll(math.random(180, 480));
				smoke:SetRollDelta(math.random(-3, 3));
				smoke:SetColor(225, 225, 225);
				smoke:SetGravity(Vector(0, 0, 32));
				smoke:SetAirResistance(256);			
			end;
		end;
		self.smokeTime = CurTime() + 0.1;				
	end;
end;

function ENT:Draw()
	self:DrawModel();
	
	local camPos = self:GetPos();
	local camAng = self:GetAngles();

	camAng:RotateAroundAxis(camAng:Up(), 90);
	camAng:RotateAroundAxis(camAng:Forward(), 90);	
	camAng:RotateAroundAxis(camAng:Right(), -90);	
	
	if (LocalPlayer():GetPos():Distance(self:GetPos()) < HR_CrystalMeth.DrawDistance) then
		cam.Start3D2D(camPos+(camAng:Right()*-0.5)+(camAng:Up()*10), camAng, 0.035)
			if (self:GetNWInt("cook_time")!=0) then
				draw.SimpleTextOutlined(HR_CrystalMeth.PotWD_Text.." (In Progress)", "HR_CR_FlyFont", 0, -4, HR_CrystalMeth.PotWD_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			elseif (self:GetNWInt("cook_time")==0) then
				draw.SimpleTextOutlined(HR_CrystalMeth.PotWD_Text.." (Ready)", "HR_CR_FlyFont", 0, -4, HR_CrystalMeth.PotWD_TextColorReady, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			end;
		cam.End3D2D();
		
		cam.Start3D2D(camPos+(camAng:Right()*-0.5)+(camAng:Up()*10), camAng, 0.030)
			draw.SimpleTextOutlined("Water: "..(self:GetNWInt("water")*1).."/"..HR_CrystalMeth.PotWD_WaterMax.." L", "HR_CR_FlyFont", -84, 12, HR_CrystalMeth.WaterOne_TextColor, TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT, 1, Color(25, 25, 25, 100));
			draw.SimpleTextOutlined("Sulfur: "..(self:GetNWInt("sulfurd")*100).."/"..(HR_CrystalMeth.PotWD_SulfurMax*100).."g", "HR_CR_FlyFont", -84, 40, HR_CrystalMeth.SulfurD_TextColor, TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT, 1, Color(25, 25, 25, 100));
			draw.SimpleTextOutlined("Temp: "..(self:GetNWInt("temp")).."C", "HR_CR_FlyFont", -84, 68, Color(255, 150, 0, 255), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT, 1, Color(25, 25, 25, 100));
		cam.End3D2D();		
	end;
end;
