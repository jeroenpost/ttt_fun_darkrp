AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");

function ENT:Initialize()
	self:SetModel("models/gibs/shield_scanner_gib1.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);
	
	self:SetMaterial("models/props_c17/FurnitureMetal001a");
	self:SetColor(Color(230, 210, 40));
	
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);
	
	self:SetNWInt("amount", HR_CrystalMeth.SulfurD_Amount);
	self:SetPos(self:GetPos()+Vector(0, 0, 8));
	self:GetPhysicsObject():SetVelocity(self:GetUp()*2);
	
	self:SetCollisionGroup(COLLISION_GROUP_WEAPON);
end;
 
function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("hr_cm_sulfurd");
	ent:SetPos(trace.HitPos + trace.HitNormal * 16);
	ent:Spawn();
	ent:Activate();
     
	return ent;
end;

function ENT:PhysicsCollide(data, phys)
	local dEnt = data.HitEntity;
	if (data.DeltaTime > 0) and (dEnt:GetClass() == "hr_cm_potwd") then
		if (dEnt:GetNWInt("temp")>=dEnt:GetNWInt("crit_temp")) then
			if (dEnt:GetNWInt("sulfurd")<HR_CrystalMeth.PotWD_SulfurMax) then
				dEnt:SetNWInt("sulfurd", math.Clamp(dEnt:GetNWInt("sulfurd") + 1, 0, HR_CrystalMeth.PotWD_SulfurMax));
				dEnt:GetPhysicsObject():SetVelocity((dEnt:GetForward()*math.random(-2, 2))+(dEnt:GetRight()*math.random(-2, 2)));	
				dEnt:EmitSound("physics/metal/metal_solid_impact_soft"..math.random(1, 3)..".wav", 70 , 95);
				self:EmitSound("physics/cardboard/cardboard_box_impact_soft"..math.random(1, 7)..".wav", 70, 100);	
				self:VisualEffect();
			end;
		end;
	end;
end;

function ENT:OnTakeDamage(dmginfo)
	self:VisualEffect();
end;

function ENT:VisualEffect()
	local effectData = EffectData();	
	effectData:SetStart(self:GetPos());
	effectData:SetOrigin(self:GetPos());
	effectData:SetScale(8);	
	util.Effect("GlassImpact", effectData, true, true);
	self:Remove();
end;

