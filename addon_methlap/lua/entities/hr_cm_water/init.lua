AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");

function ENT:Initialize()
	self:SetModel("models/props_junk/garbage_milkcarton001a.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);
	
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);
	
	self:SetNWInt("amount", HR_CrystalMeth.WaterOne_Amount);
	self:SetNWBool("locked", true);
	self:SetPos(self:GetPos()+Vector(0, 0, 8));
	self:GetPhysicsObject():SetVelocity(self:GetUp()*2);
	
	self:SetCollisionGroup(COLLISION_GROUP_WEAPON);
	self:GetPhysicsObject():SetMass(105);
end;
 
function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("hr_cm_water");
	ent:SetPos(trace.HitPos + trace.HitNormal * 16);
	ent:Spawn();
	ent:Activate();
     
	return ent;
end;

function ENT:OnTakeDamage(dmginfo)

end;

function ENT:Use(activator, caller)
local curTime = CurTime();
	if (activator:GetEyeTrace().Entity == self) then
		if (!self.nextUse or curTime >= self.nextUse) then		
			if (self:GetNWBool("locked")) then
				if (activator:GetActiveWeapon():GetClass() == "weapon_physcannon") then
					self:SetAngles(Angle(180, 0, 0));
					activator:ConCommand("+attack2");
					timer.Simple(0.1, function() activator:ConCommand("-attack2") end);
				end;
			else
				self:SetAngles(Angle(0, 0, 0));
			end;
			--self:EmitSound("buttons/lever3.wav", 70, 110);
			self.nextUse = curTime + 0.5;
		end;
	end;
end;

function ENT:Think()
	local upPos = self:GetPos()+(self:GetRight()*0.35)+(self:GetUp()*8.5);
	local downPos = self:GetPos()+(self:GetRight()*0.35)+(self:GetUp()*-8.5);
	if (downPos.z > upPos.z) then
		--self:EmitSound("npc/barnacle/barnacle_gulp1.wav", 70, 110);
		if (!self.nextFuel or CurTime() >= self.nextFuel) then	
			if (!self:GetNWBool("locked") and self:GetNWInt("amount")>0) then
				local trace = {}	
				trace.start = upPos;
				trace.endpos = upPos+Vector(0, 0, -4096);
				trace.filter = self;
				
				local traceRes = util.TraceLine(trace);			
				
				self:SetNWInt("amount", self:GetNWInt("amount")-1);
				if (self:GetNWInt("amount")==0) then
					self:EmitSound("ambient/water/water_splash3.wav", 70, 110);
					timer.Simple(5, function()
						self:VisualEffect();
					end);
				end;
				local trEnt = traceRes.Entity;
				if IsValid(trEnt) then
					if ((trEnt:GetClass() == "hr_cm_potwr") and (trEnt:GetNWInt("cook_time")!=0)) then
						if (trEnt:GetNWInt("water")<HR_CrystalMeth.PotWR_WaterMax) then
							local eData = EffectData();
							eData:SetOrigin(traceRes.HitPos);
							util.Effect("BloodImpact", eData);
							
							trEnt:EmitSound("ambient/water/water_splash"..math.random(1, 3)..".wav", 70, 120);
							trEnt:SetNWInt("water", math.Clamp(trEnt:GetNWInt("water")+1, 0, HR_CrystalMeth.PotWR_WaterMax));
						end;
					elseif ((trEnt:GetClass() == "hr_cm_potwd") and (trEnt:GetNWInt("cook_time")!=0)) then
						if (trEnt:GetNWInt("water")<HR_CrystalMeth.PotWD_WaterMax) then
							local eData = EffectData();
							eData:SetOrigin(traceRes.HitPos);
							util.Effect("BloodImpact", eData);
							
							trEnt:EmitSound("ambient/water/water_splash"..math.random(1, 3)..".wav", 70, 120);
							trEnt:SetNWInt("water", math.Clamp(trEnt:GetNWInt("water")+1, 0, HR_CrystalMeth.PotWD_WaterMax));
						end;
					end;
				end;
			end;
			self.nextFuel = CurTime() + 0.45;
		end;
	end;
end;


function ENT:VisualEffect()
	local effectData = EffectData();	
	effectData:SetStart(self:GetPos());
	effectData:SetOrigin(self:GetPos());
	effectData:SetScale(8);	
	util.Effect("GlassImpact", effectData, true, true);
	self:Remove();
end;

hook.Add("KeyPress", "HR_KeyPress_Water", function(player, key)
	local lookEnt = player:GetEyeTrace().Entity;
	if IsValid(lookEnt) then 
		if (lookEnt:GetClass() == "hr_cm_water") then
			if (lookEnt:GetPos():Distance(player:GetPos())<128) then
				if (key == IN_SPEED) then
					if (lookEnt:GetNWBool("locked")) then
						lookEnt:SetNWBool("locked", false);
					else
						lookEnt:SetNWBool("locked", true);
					end;
					lookEnt:EmitSound("buttons/lever3.wav", 70, 110);
				end;
			end;
		end;
	end;
end);