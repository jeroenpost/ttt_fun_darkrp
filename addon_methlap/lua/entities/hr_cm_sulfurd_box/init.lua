AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");

function ENT:Initialize()
	self:SetModel("models/props/cs_office/Cardboard_box01.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);
	
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);
	
	self:SetNWInt("amount", HR_CrystalMeth.SulfurDBox_Amount);
	self:SetPos(self:GetPos()+Vector(0, 0, 8));
	self:GetPhysicsObject():SetVelocity(self:GetUp()*2);
	
	for i1=1, 5 do
		for i=1, 2 do
			local fakeRp = ents.Create("prop_dynamic");
			fakeRp:SetModel("models/gibs/shield_scanner_gib1.mdl");
			fakeRp:SetMaterial("models/props_pipes/Pipesystem01a_skin3");
			fakeRp:SetMaterial("models/props_c17/FurnitureMetal001a");
			fakeRp:SetColor(Color(230, 210, 40));
			fakeRp:SetPos(self:GetPos()+(self:GetUp()*1.5)+(self:GetForward()*-12)+(self:GetRight()*i*-6.2)+(fakeRp:GetForward()*i1*4)+(self:GetRight()*10));		
			fakeRp:SetAngles(self:GetAngles());
			fakeRp:SetParent(self);
			fakeRp:Spawn();
		end;
	end;
	
	self:SetCollisionGroup(COLLISION_GROUP_WEAPON);
	self:GetPhysicsObject():SetMass(105);
end;

function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("hr_cm_sulfurd_box");
	ent:SetPos(trace.HitPos + trace.HitNormal * 16);
	ent:Spawn();
	ent:Activate();
     
	return ent;
end;

function ENT:Use(activator, caller)
local curTime = CurTime();
	if (activator:GetEyeTrace().Entity == self) then
		if (!self.nextUse or curTime >= self.nextUse) then
			if (HR_CrystalMeth.CanOpenBoxes) then
				if (activator:KeyDown(IN_SPEED)) then
					self:EmitSound("physics/cardboard/cardboard_box_break2.wav", 70, 100);
					for i=1, self:GetNWInt("amount") do
						local redp = ents.Create("hr_cm_sulfurd");
							local di = i%2;
							if (di == 0) then
								redp:SetPos(self:GetPos()+(self:GetUp()*2*i)+(self:GetForward()*i*1));		
							else
								redp:SetPos(self:GetPos()+(self:GetUp()*2*i)+(self:GetForward()*-i*1)+(self:GetRight()*3));
							end
						redp:SetAngles(self:GetAngles());							
						redp:Spawn();
                        redp:CPPISetOwner(activator)
					end;
					self:VisualEffect();					
				elseif (!activator:KeyDown(IN_SPEED)) then
					if (self:GetNWInt("amount")>0) then
						self:SetNWInt("amount", self:GetNWInt("amount")-1);
						self:EmitSound("physics/cardboard/cardboard_box_break3.wav", 70, 100);
						
						local redp = ents.Create("hr_cm_sulfurd");
						redp:SetPos(self:GetPos()+(self:GetUp()*14));
						redp:SetAngles(activator:GetAngles());
						redp:Spawn();
                        redp:CPPISetOwner(activator)
						if (self:GetNWInt("amount")==0) then
							self:EmitSound("physics/cardboard/cardboard_box_break2.wav", 70, 100);
							self:VisualEffect();
						end;
					end;		
				end;
			end;
			self.nextUse = curTime + 0.5;
		end;
	end;
end;

function ENT:OnTakeDamage(dmginfo)
	self:EmitSound("physics/cardboard/cardboard_box_break2.wav", 70, 100);
	for i=1, self:GetNWInt("amount") do
		local redp = ents.Create("hr_cm_sulfurd");
		local di = i%2;
		if (di == 0) then
			redp:SetPos(self:GetPos()+(self:GetUp()*1)+(redp:GetForward()*i*2));		
		else
			redp:SetPos(self:GetPos()+(self:GetUp()*1)+(redp:GetForward()*-i*2));
		end
		redp:SetAngles(self:GetAngles());
		redp:Spawn();
	end;
	self:VisualEffect();
end;

function ENT:VisualEffect()
	local effectData = EffectData();	
	effectData:SetStart(self:GetPos());
	effectData:SetOrigin(self:GetPos());
	effectData:SetScale(8);	
	util.Effect("GlassImpact", effectData, true, true);
	self:Remove();
end;

