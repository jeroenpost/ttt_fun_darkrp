include("shared.lua");

surface.CreateFont("HR_CR_BoxFontBig", {
	font = "Arial",
	size = 48,
	weight = 600,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
});

surface.CreateFont("HR_CR_BoxFontSmall", {
	font = "Arial",
	size = 32,
	weight = 600,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
});

function ENT:Initialize()	

end;

function ENT:Draw()
	self:DrawModel();
	
	local camPos = self:GetPos();
	local camAng = self:GetAngles();

	camAng:RotateAroundAxis(camAng:Up(), 90);
	camAng:RotateAroundAxis(camAng:Forward(), 90);	
	camAng:RotateAroundAxis(camAng:Right(), 180);
	
	if (LocalPlayer():GetPos():Distance(self:GetPos()) < HR_CrystalMeth.DrawDistance) then
		cam.Start3D2D(camPos+camAng:Up()*12.75, camAng, 0.065)
			draw.SimpleTextOutlined(HR_CrystalMeth.SulfurDBox_Text, "HR_CR_BoxFontBig", 0, -202, HR_CrystalMeth.SulfurD_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			draw.SimpleTextOutlined(string.Comma(self:GetNWInt("amount")*100).."g", "HR_CR_BoxFontBig", 0, -162, HR_CrystalMeth.SulfurD_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));			
		cam.End3D2D();
		cam.Start3D2D(camPos+camAng:Up()*12.75, camAng, 0.055)
			draw.SimpleTextOutlined("___________________", "HR_CR_BoxFontSmall", 0, -64, HR_CrystalMeth.SulfurD_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			draw.SimpleTextOutlined(HR_CrystalMeth.SulfurDBox_TextCopyright, "HR_CR_BoxFontSmall", 0, -32, HR_CrystalMeth.SulfurD_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));			
		cam.End3D2D();			
	end;
end;
