include("shared.lua");

surface.CreateFont("HR_CR_FlyFont", {
	font = "Arial",
	size = 32,
	weight = 600,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
});

function ENT:Initialize()	

end;

function ENT:Draw()
	self:DrawModel();
	
	local camPos = self:GetPos();
	local camAng = self:GetAngles();

	if (LocalPlayer():GetPos():Distance(self:GetPos()) < HR_CrystalMeth.DrawDistance) then
		cam.Start3D2D(camPos+Vector(0, 0, 3.5), Angle(0, LocalPlayer():EyeAngles().y-90, 90), 0.075)
			draw.SimpleTextOutlined("Aluminum", "HR_CR_FlyFont", 0, -4, HR_CrystalMeth.AluminumOne_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			draw.SimpleTextOutlined((self:GetNWInt("amount")*250).."g", "HR_CR_FlyFont", 0, 20, HR_CrystalMeth.AluminumOne_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
		cam.End3D2D();
	end;
end;
