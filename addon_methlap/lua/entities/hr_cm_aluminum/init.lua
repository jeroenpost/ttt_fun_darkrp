AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");

function ENT:Initialize()
	self:SetModel("models/gibs/metal_gib2.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);
	
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);
	
	self:SetNWInt("amount", HR_CrystalMeth.AluminumOne_Amount);
	self:SetPos(self:GetPos()+Vector(0, 0, 8));
	self:GetPhysicsObject():SetVelocity(self:GetUp()*2);
	
	self:SetCollisionGroup(COLLISION_GROUP_WEAPON);
end;
 
function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("hr_cm_aluminum");
	ent:SetPos(trace.HitPos + trace.HitNormal * 16);
	ent:Spawn();
	ent:Activate();
     
	return ent;
end;

function ENT:PhysicsCollide(data, phys) 
	local dEnt = data.HitEntity;
	if ((data.DeltaTime > 0) and (dEnt:GetClass() == "hr_cm_tank") and (dEnt:GetNWInt("stage") == 1)) then
		if (dEnt:GetNWInt("aluminum")<HR_CrystalMeth.ChemicalTank_Al_Hold) then
			dEnt:SetNWInt("aluminum", math.Clamp(dEnt:GetNWInt("aluminum") + 1, 0, HR_CrystalMeth.ChemicalTank_Al_Hold));
			self:EmitSound("physics/metal/metal_barrel_impact_soft"..math.random(1, 4)..".wav", 70, 100);			
			self:VisualEffect();
		end;
	end;
end;

function ENT:OnTakeDamage(dmginfo)
	self:VisualEffect();
end;

function ENT:VisualEffect()
	local effectData = EffectData();	
	effectData:SetStart(self:GetPos());
	effectData:SetOrigin(self:GetPos());
	effectData:SetScale(8);	
	util.Effect("GlassImpact", effectData, true, true);
	self:Remove();
end;

