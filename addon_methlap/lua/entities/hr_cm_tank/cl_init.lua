include("shared.lua");

surface.CreateFont("HR_CR_TankFontSmall", {
	font = "Arial",
	size = 24,
	weight = 600,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
});

function ENT:Initialize()	
	self.gasTime = CurTime();
	self.gasPos = ParticleEmitter(self:GetPos()+(self:GetRight()*7)+(self:GetUp()*11));
end;

function ENT:Think()
	if (self:GetNWBool("pressure_leak")) then
		if (self.gasTime < CurTime()) then
			local blood = self.gasPos:Add("particle/smokesprites_000"..math.random(1,9), self:GetPos()+(self:GetRight()*7)+(self:GetUp()*32));
			blood:SetVelocity(Vector(math.random(-10, 10), math.random(-10, 10), self:GetUp()*512));
			blood:SetDieTime(1);
			blood:SetStartAlpha(150);
			blood:SetEndAlpha(0);
			blood:SetStartSize(2);
			blood:SetEndSize(36);
			blood:SetRoll(math.random(180, 480));
			blood:SetRollDelta(math.random(-3, 3));
			blood:SetColor(255, 255, 255);
			blood:SetGravity(Vector(0, 0, 512));
			blood:SetAirResistance(256);
			blood:SetCollide(true);
			self.gasTime = CurTime() + 0.05;				
		end;
	end;
end;

function ENT:Draw()
	self:DrawModel();
	
	local camPos = self:GetPos();
	local camAng = self:GetAngles();

	camAng:RotateAroundAxis(camAng:Up(), 90);
	camAng:RotateAroundAxis(camAng:Forward(), 90);	
	camAng:RotateAroundAxis(camAng:Right(), -100);
	
	local butPos1 = self:GetPos()+(self:GetForward()*-12.25)+(self:GetRight()*-33.4)+(self:GetUp()*16.0);
	local butPos2 = self:GetPos()+(self:GetForward()*-12.25)+(self:GetRight()*-33.4)+(self:GetUp()*14.8);
	local butPos3 = self:GetPos()+(self:GetForward()*-12.25)+(self:GetRight()*-33.4)+(self:GetUp()*13.6);
	
	-- phase 1 (aluminum + cyfluthrin)
	-- phase 2 - when phase 1 finished +metylamine
	-- phase 3 - when phase 2 finished +intermediate acid
	-- phase 4 - when phase 3 finished can leak liquid meth
	
	if (LocalPlayer():GetPos():Distance(self:GetPos()) < HR_CrystalMeth.DrawDistance) then	
		cam.Start3D2D(camPos+camAng:Up()*35, camAng, 0.1)
			-- Base
			draw.RoundedBox(0, -60, -210, 140, 110, Color(25, 25, 30, 255));	

			-- Upper text looks cool.
			surface.SetDrawColor(Color(0, 0, 25, 255));
			surface.DrawRect(-60, -210, 140, 16);			
			
			if (self:GetNWInt("stage") == 1) then			
				-- Aluminum Bar
				surface.SetDrawColor(Color(0, 0, 25, 255));
				surface.DrawRect(-55, -182.5, 100, 14);			

				surface.SetDrawColor(HR_CrystalMeth.AluminumOne_TextColor);
				surface.DrawRect(-55, -182.5, math.Clamp(self:GetNWInt("aluminum"), 0, 100), 14);	
				
				surface.SetDrawColor(Color(45, 45, 45, 255));
				surface.DrawOutlinedRect(-55, -182.5, 100, 14);			

				-- Cyfluthrin Bar
				surface.SetDrawColor(Color(0, 0, 25, 255));
				surface.DrawRect(-55, -155, 100, 14);			

				surface.SetDrawColor(HR_CrystalMeth.CyfluthrinCan_TextColor);
				surface.DrawRect(-55, -155, math.Clamp(self:GetNWInt("cyfluthrin"), 0, 100), 14);	
				
				surface.SetDrawColor(Color(45, 45, 45, 255));
				surface.DrawOutlinedRect(-55, -155, 100, 14);					

				-- Progress Bar
				surface.SetDrawColor(Color(0, 0, 25, 255));
				surface.DrawRect(-55, -127, 100, 14);			

				surface.SetDrawColor(Color(200, 255, 200, 255));
				surface.DrawRect(-55, -127, math.Clamp(self:GetNWInt("progress"), 0, 100), 14);	
				
				surface.SetDrawColor(Color(45, 45, 45, 255));
				surface.DrawOutlinedRect(-55, -127, 100, 14);	
			elseif (self:GetNWInt("stage") == 2) then
				-- Methylamine Bar
				surface.SetDrawColor(Color(0, 0, 25, 255));
				surface.DrawRect(-55, -182.5, 100, 14);			

				surface.SetDrawColor(HR_CrystalMeth.Methylamine_TextColor);
				surface.DrawRect(-55, -182.5, math.Clamp(self:GetNWInt("methylamine"), 0, 100), 14);	
				
				surface.SetDrawColor(Color(45, 45, 45, 255));
				surface.DrawOutlinedRect(-55, -182.5, 100, 14);			

				-- Progress Bar
				surface.SetDrawColor(Color(0, 0, 25, 255));
				surface.DrawRect(-55, -155, 100, 14);			

				surface.SetDrawColor(Color(200, 255, 200, 255));
				surface.DrawRect(-55, -155, math.Clamp(self:GetNWInt("progress"), 0, 100), 14);	
				
				surface.SetDrawColor(Color(45, 45, 45, 255));
				surface.DrawOutlinedRect(-55, -155, 100, 14);				

			elseif (self:GetNWInt("stage") == 3) then
				-- Intermediate Bar
				surface.SetDrawColor(Color(0, 0, 25, 255));
				surface.DrawRect(-55, -182.5, 100, 14);			

				surface.SetDrawColor(HR_CrystalMeth.Glass_Intermediate_Acid_Color);
				surface.DrawRect(-55, -182.5, math.Clamp(self:GetNWInt("inter_acid")*10, 0, 100), 14);	
				
				surface.SetDrawColor(Color(45, 45, 45, 255));
				surface.DrawOutlinedRect(-55, -182.5, 100, 14);			

				-- Progress Bar
				surface.SetDrawColor(Color(0, 0, 25, 255));
				surface.DrawRect(-55, -155, 100, 14);			

				surface.SetDrawColor(Color(200, 255, 200, 255));
				surface.DrawRect(-55, -155, math.Clamp(self:GetNWInt("progress"), 0, 100), 14);	
				
				surface.SetDrawColor(Color(45, 45, 45, 255));
				surface.DrawOutlinedRect(-55, -155, 100, 14);				
							
				
			end;
			
			-- Pressure Digits
			surface.SetDrawColor(Color(15, 0, 0, 255));
			surface.DrawRect(49, -182.5, 27, 14);			
			
			surface.SetDrawColor(Color(45, 45, 45, 255));
			surface.DrawOutlinedRect(49, -182.5, 27, 14);	

			
			-- Button 1
			if (!self:GetNWBool("working")) then
				if (LocalPlayer():GetEyeTrace().HitPos:Distance(butPos1)<0.90) then
					surface.SetDrawColor(Color(0, 100, 0, 255));
				else
					surface.SetDrawColor(Color(0, 75, 0, 255));
				end;			
			else
				if (LocalPlayer():GetEyeTrace().HitPos:Distance(butPos1)<0.90) then
					surface.SetDrawColor(Color(0, 100, 0, 255));
				else
					surface.SetDrawColor(Color(0, 75, 0, 255));
				end;			
			end;
			surface.DrawRect(49, -166, 27, 11);			
			
			
			surface.SetDrawColor(Color(45, 45, 45, 255));
			surface.DrawOutlinedRect(49, -166, 27, 11);

			-- Button 2
			if (!self:GetNWBool("pressure_leak")) then
				if (LocalPlayer():GetEyeTrace().HitPos:Distance(butPos2)<0.90) then
					surface.SetDrawColor(Color(100, 0, 0, 255));
				else
					surface.SetDrawColor(Color(75, 0, 0, 255));
				end;			
			else
				if (LocalPlayer():GetEyeTrace().HitPos:Distance(butPos2)<0.90) then
					surface.SetDrawColor(Color(100, 0, 0, 255));
				else
					surface.SetDrawColor(Color(75, 0, 0, 255));
				end;			
			end;
			surface.DrawRect(49, -154, 27, 11);			
			
			surface.SetDrawColor(Color(45, 45, 45, 255));
			surface.DrawOutlinedRect(49, -154, 27, 11);	
			
			-- Button 3
			if (!self:GetNWBool("pressure_gain")) then
				if (LocalPlayer():GetEyeTrace().HitPos:Distance(butPos3)<0.90) then
					surface.SetDrawColor(Color(100, 75, 0, 255));
				else
					surface.SetDrawColor(Color(75, 50, 0, 255));
				end;			
			else
				if (LocalPlayer():GetEyeTrace().HitPos:Distance(butPos3)<0.90) then
					surface.SetDrawColor(Color(100, 75, 0, 255));
				else
					surface.SetDrawColor(Color(75, 50, 0, 255));
				end;			
			end;			
			surface.DrawRect(49, -142, 27, 11);			
			
			surface.SetDrawColor(Color(45, 45, 45, 255));
			surface.DrawOutlinedRect(49, -142, 27, 11);			
		cam.End3D2D();	

		cam.Start3D2D(camPos+camAng:Up()*35, camAng, 0.035)
			if (self:GetNWInt("stage") == 1) then
				draw.SimpleTextOutlined(HR_CrystalMeth.AluminumBox_Text.." ("..string.Comma(self:GetNWInt("aluminum")*250).."/"..string.Comma(HR_CrystalMeth.ChemicalTank_Al_Hold*250).."g)", "HR_CR_TankFontSmall", -150, -545, HR_CrystalMeth.AluminumOne_TextColor, TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT, 1, Color(25, 25, 25, 100));
				draw.SimpleTextOutlined(HR_CrystalMeth.CyfluthrinCan_Text.." ("..string.Comma(self:GetNWInt("cyfluthrin")).."/"..string.Comma(HR_CrystalMeth.ChemicalTank_Cl_Hold).." L)", "HR_CR_TankFontSmall", -150, -470, HR_CrystalMeth.CyfluthrinCan_TextColor, TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT, 1, Color(25, 25, 25, 100));
				draw.SimpleTextOutlined("Progress: "..self:GetNWInt("progress").."%", "HR_CR_TankFontSmall", -150, -390, Color(200, 255, 200, 255), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT, 1, Color(25, 25, 25, 100));
				draw.SimpleTextOutlined("Keep pressure between  "..HR_CrystalMeth.ChemicalTank_S1_MinPressure.."-"..HR_CrystalMeth.ChemicalTank_S1_MaxPressure.."P", "HR_CR_TankFontSmall", 25, -580, Color(125, 0, 0, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			elseif (self:GetNWInt("stage") == 2) then
				draw.SimpleTextOutlined(HR_CrystalMeth.Methylamine_Text.." ("..string.Comma(self:GetNWInt("methylamine")).."/"..string.Comma(HR_CrystalMeth.ChemicalTank_Mt_Hold).." L)", "HR_CR_TankFontSmall", -150, -545, HR_CrystalMeth.Methylamine_TextColor, TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT, 1, Color(25, 25, 25, 100));
				draw.SimpleTextOutlined("Progress: "..self:GetNWInt("progress").."%", "HR_CR_TankFontSmall", -150, -470, Color(200, 255, 200, 255), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT, 1, Color(25, 25, 25, 100));
				draw.SimpleTextOutlined("Keep pressure between  "..HR_CrystalMeth.ChemicalTank_S2_MinPressure.."-"..HR_CrystalMeth.ChemicalTank_S2_MaxPressure.."P", "HR_CR_TankFontSmall", 25, -580, Color(125, 0, 0, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			elseif (self:GetNWInt("stage") == 3) then
				draw.SimpleTextOutlined(HR_CrystalMeth.Glass_Intermediate_Acid.." ("..string.Comma(self:GetNWInt("inter_acid")).."/"..string.Comma(HR_CrystalMeth.ChemicalTank_Ia_Hold).." L)", "HR_CR_TankFontSmall", -150, -545, HR_CrystalMeth.Glass_Intermediate_Acid_Color, TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT, 1, Color(25, 25, 25, 100));
				draw.SimpleTextOutlined("Progress: "..self:GetNWInt("progress").."%", "HR_CR_TankFontSmall", -150, -470, Color(200, 255, 200, 255), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT, 1, Color(25, 25, 25, 100));
				draw.SimpleTextOutlined("Keep pressure between  "..HR_CrystalMeth.ChemicalTank_S3_MinPressure.."-"..HR_CrystalMeth.ChemicalTank_S3_MaxPressure.."P", "HR_CR_TankFontSmall", 25, -580, Color(125, 0, 0, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			end;
			
			draw.SimpleTextOutlined(self:GetNWInt("pressure").."P", "HR_CR_TankFontSmall", 180, -500, Color(125, 0, 0, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			if (!self:GetNWBool("working")) then
				draw.SimpleTextOutlined("Start", "HR_CR_TankFontSmall", 180, -459, Color(45, 45, 45, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 0, Color(25, 25, 25, 100));
			else
				draw.SimpleTextOutlined("Stop", "HR_CR_TankFontSmall", 180, -459, Color(45, 45, 45, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 0, Color(25, 25, 25, 100));
			end;

			if (!self:GetNWBool("pressure_leak")) then
				draw.SimpleTextOutlined("Open", "HR_CR_TankFontSmall", 180, -424, Color(45, 45, 45, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 0, Color(25, 25, 25, 100));
			else
				draw.SimpleTextOutlined("Close", "HR_CR_TankFontSmall", 180, -424, Color(45, 45, 45, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 0, Color(25, 25, 25, 100));
			end;
			
			if (!self:GetNWBool("pressure_gain")) then
				draw.SimpleTextOutlined("Gain", "HR_CR_TankFontSmall", 180, -389, Color(45, 45, 45, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 0, Color(25, 25, 25, 100));
			else
				draw.SimpleTextOutlined("Stop", "HR_CR_TankFontSmall", 180, -389, Color(45, 45, 45, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 0, Color(25, 25, 25, 100));
			end;			
		cam.End3D2D();			
		
		--render.SetMaterial(Material("sprites/glow04_noz"));
		--render.DrawSprite(butPos1, 1, 1, Color(255, 100, 100, 255));
		--render.DrawSprite(butPos2, 1, 1, Color(100, 255, 100, 255));					
	end;
end;
