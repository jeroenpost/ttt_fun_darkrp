AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");

function ENT:Initialize()
	self:SetModel("models/props_wasteland/laundry_washer001a.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);
	
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);

	self:SetNWInt("stage", 1);
	
	self:SetNWInt("aluminum", 0);
	self:SetNWInt("cyfluthrin", 0);
	self:SetNWInt("methylamine", 0);
	self:SetNWInt("inter_acid", 0);
	self:SetNWInt("progress", 0);
	self:SetNWInt("pressure", 75);
	
	self:SetNWBool("pressure_leak", false);
	self:SetNWBool("pressure_gain", false);
	self:SetNWBool("working", false);
	
	self:SetPos(self:GetPos()+Vector(0, 0, 48));
	self:GetPhysicsObject():SetVelocity(self:GetUp()*2);	

	self:SetHealth(HR_CrystalMeth.ChemicalTank_Health);
	self:GetPhysicsObject():SetMass(200);
end;
 
function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("hr_cm_tank");
	ent:SetPos(trace.HitPos + trace.HitNormal * 16);
	ent:Spawn();
	ent:Activate();
     
	return ent;
end;


function ENT:OnTakeDamage(dmginfo)
	if (HR_CrystalMeth.ChemicalTank_Destroyable) then
		self:SetHealth(math.Clamp(self:Health()-dmginfo:GetDamage(), 0, HR_CrystalMeth.ChemicalTank_Health));		
		self:EmitSound("weapons/fx/rics/ric"..table.Random({1, 2, 4, 5})..".wav", 70, 100);
		
		local effectdata = EffectData();
		effectdata:SetStart(dmginfo:GetDamagePosition());
		effectdata:SetOrigin(dmginfo:GetDamagePosition());
		effectdata:SetAngles(dmginfo:GetAttacker():GetAngles());
		effectdata:SetScale(1);
		util.Effect("StunstickImpact", effectdata);		
		
		if (self:Health() == 0) then
			self:Explode();	
		end;
	end;
end;

function ENT:Use(activator, caller)
local curTime = CurTime();
	if (activator:GetEyeTrace().Entity == self) then
		if (!self.nextUse or curTime >= self.nextUse) then		
			local butPos1 = self:GetPos()+(self:GetForward()*-12.25)+(self:GetRight()*-33.4)+(self:GetUp()*16.0);
			local butPos2 = self:GetPos()+(self:GetForward()*-12.25)+(self:GetRight()*-33.4)+(self:GetUp()*14.8);
			local butPos3 = self:GetPos()+(self:GetForward()*-12.25)+(self:GetRight()*-33.4)+(self:GetUp()*13.6);
	
			if (HR_CrystalMeth.HintsEnabled) then
				if (activator:KeyDown(IN_WALK)) then
					activator:SendLua("LocalPlayer():EmitSound('ui/buttonclick.wav', 70, 100)");
					if (self:GetNWInt("stage") == 1) then
						activator:SendLua("local tab = {Color(255,255,255),[[How to cook ]], HR_CrystalMeth.MethName_Color,[[Amphetamine - Stage I:]]}chat.AddText(unpack(tab))");
						activator:SendLua("local tab = {Color(255,255,255),[[1) Add ]], HR_CrystalMeth.AluminumOne_TextColor,[["..HR_CrystalMeth.AluminumBox_Text.."]], Color(255,255,255),[[ in tank.]]}chat.AddText(unpack(tab))");
						activator:SendLua("local tab = {Color(255,255,255),[[2) Add ]], HR_CrystalMeth.CyfluthrinCan_TextColor,[["..HR_CrystalMeth.CyfluthrinCan_Text.."]], Color(255,255,255),[[ in tank.]]}chat.AddText(unpack(tab))");
						activator:SendLua("local tab = {Color(255,255,255),[[3) Enable ]], Color(200,200,200),[[Chemical Tank]], Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");
						activator:SendLua("local tab = {Color(255,255,255),[[4) Regulate ]], Color(200,0,0),[[pressure]], Color(255,255,255),[[ according information on screen.]]}chat.AddText(unpack(tab))");
						activator:SendLua("local tab = {Color(255,255,255),[[5) Wait until ]], Color(200,255,200),[[finish]], Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");
					elseif (self:GetNWInt("stage") == 2) then
						activator:SendLua("local tab = {Color(255,255,255),[[How to cook ]], HR_CrystalMeth.MethName_Color,[[Amphetamine - Stage II:]]}chat.AddText(unpack(tab))");
						activator:SendLua("local tab = {Color(255,255,255),[[1) Add ]], HR_CrystalMeth.Methylamine_TextColor,[["..HR_CrystalMeth.Methylamine_Text.."]], Color(255,255,255),[[ in tank.]]}chat.AddText(unpack(tab))");
						activator:SendLua("local tab = {Color(255,255,255),[[2) Enable ]], Color(200,200,200),[[Chemical Tank]], Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");
						activator:SendLua("local tab = {Color(255,255,255),[[3) Regulate ]], Color(200,0,0),[[pressure]], Color(255,255,255),[[ according information on screen.]]}chat.AddText(unpack(tab))");
						activator:SendLua("local tab = {Color(255,255,255),[[4) Wait until ]], Color(200,255,200),[[finish]], Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");
					elseif (self:GetNWInt("stage") == 3) then
						activator:SendLua("local tab = {Color(255,255,255),[[How to cook ]], HR_CrystalMeth.MethName_Color,[[Amphetamine - Stage III:]]}chat.AddText(unpack(tab))");
						activator:SendLua("local tab = {Color(255,255,255),[[1) Add ]], HR_CrystalMeth.Glass_Intermediate_Acid_Color,[["..HR_CrystalMeth.Glass_Intermediate_Acid.."]], Color(255,255,255),[[ in tank.]]}chat.AddText(unpack(tab))");
						activator:SendLua("local tab = {Color(255,255,255),[[2) Enable ]], Color(200,200,200),[[Chemical Tank]], Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");
						activator:SendLua("local tab = {Color(255,255,255),[[3) Regulate ]], Color(200,0,0),[[pressure]], Color(255,255,255),[[ according information on screen.]]}chat.AddText(unpack(tab))");
						activator:SendLua("local tab = {Color(255,255,255),[[4) Wait until ]], Color(200,255,200),[[finish]], Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");						
					end;
				end;
			end;		
			
			if (activator:GetEyeTrace().HitPos:Distance(butPos1)<0.90) then
				self:EmitSound("buttons/blip1.wav", 55, 120);
				if (self:GetNWInt("stage") < 4) then
					if (!self:GetNWBool("working")) then
						self:SetNWBool("working", true);
						timer.Simple(0.25, function()
							self:EmitSound("ambient/machines/spinup.wav", 70, 90);
						end);
						timer.Simple(0.75, function()
							self.workSound = CreateSound(self, Sound("ambient/machines/spin_loop.wav"));
							self.workSound:SetSoundLevel(60);
							self.workSound:PlayEx(1, 100);			
						end);
					else
						self:SetNWBool("working", false);					
						timer.Simple(0.25, function()
							self:EmitSound("ambient/machines/spindown.wav", 70, 90);
						end);
						timer.Simple(0.75, function()
							self.workSound:Stop();		
						end);
					end;
				end;
			end;

			if (activator:GetEyeTrace().HitPos:Distance(butPos2)<0.90) then
				self:EmitSound("buttons/blip1.wav", 55, 120);
				if (self:GetNWInt("stage") < 4) then
					if (!self:GetNWBool("pressure_leak")) then
						self:SetNWBool("pressure_leak", true);
						timer.Simple(0.25, function()
							self:EmitSound("buttons/lever2.wav", 70, 100);
						end);
						timer.Simple(0.75, function()
							self.leakSound = CreateSound(self, Sound("ambient/gas/steam2.wav"));
							self.leakSound:SetSoundLevel(60);
							self.leakSound:PlayEx(1, 120);			
						end);
					else
						self:SetNWBool("pressure_leak", false);					
						timer.Simple(0.25, function()
							self:EmitSound("buttons/lever2.wav", 70, 100);
						end);
						timer.Simple(0.75, function()
							self.leakSound:Stop();		
						end);
					end;
				end;
			end;

			if (activator:GetEyeTrace().HitPos:Distance(butPos3)<0.90) then
				self:EmitSound("buttons/blip1.wav", 55, 120);
				if (self:GetNWInt("stage") < 4) then
					if (!self:GetNWBool("pressure_gain")) then
						self:SetNWBool("pressure_gain", true);
						timer.Simple(0.25, function()
							self:EmitSound("buttons/lever2.wav", 70, 100);
						end);
						timer.Simple(0.75, function()
							self.gainSound = CreateSound(self, Sound("ambient/gas/steam_loop1.wav"));
							self.gainSound:SetSoundLevel(60);
							self.gainSound:PlayEx(1, 120);			
						end);
					else
						self:SetNWBool("pressure_gain", false);					
						timer.Simple(0.25, function()
							self:EmitSound("buttons/lever2.wav", 70, 100);
						end);
						timer.Simple(0.75, function()
							self.gainSound:Stop();		
						end);
					end;
				end;
			end;	
			
			self.nextUse = curTime + 0.50;
		end;
	end;
end;

function ENT:Think()
	if (!self.nextProgress or CurTime() >= self.nextProgress) then	
		if (self:GetNWBool("working")) then
			-- aluminum + cyfluthrin
			if (self:GetNWInt("stage") == 1) then
				if ((self:GetNWInt("aluminum")>0) and (self:GetNWInt("cyfluthrin")>0)) then
					if (self:GetNWInt("progress")<100) then
						self:SetNWInt("progress", self:GetNWInt("progress")+HR_CrystalMeth.ProgressAmount);
					end;
					if (self:GetNWInt("progress")==100) then
						self:SetNWInt("stage", 2);
						self:SetNWInt("progress", 0);

						-- Disable leak.
						if (self:GetNWBool("pressure_leak")) then
							self:SetNWBool("pressure_leak", false);
							timer.Simple(0.25, function()
								self:EmitSound("buttons/lever2.wav", 70, 100);
							end);
							timer.Simple(0.75, function()
								self.leakSound:Stop();		
							end);							
						end;
						
						-- DIsable working.
						self:SetNWBool("working", false);
						timer.Simple(0.25, function()
							self:EmitSound("ambient/machines/spindown.wav", 70, 90);
						end);
						timer.Simple(0.75, function()
							self.workSound:Stop();		
						end);
					end;					
				end;
			end;

			-- aluminum + cyfluthrin + methylamine
			if (self:GetNWInt("stage") == 2) then
				if ((self:GetNWInt("aluminum")>0) and (self:GetNWInt("cyfluthrin")>0) and (self:GetNWInt("methylamine")>0)) then
					if (self:GetNWInt("progress")<100) then
						self:SetNWInt("progress", self:GetNWInt("progress")+HR_CrystalMeth.ProgressAmount);
					end;
					if (self:GetNWInt("progress")==100) then
						self:SetNWInt("stage", 3);
						self:SetNWInt("progress", 0);
						
						-- Disable leak.
						if (self:GetNWBool("pressure_leak")) then
							self:SetNWBool("pressure_leak", false);
							timer.Simple(0.25, function()
								self:EmitSound("buttons/lever2.wav", 70, 100);
							end);
							timer.Simple(0.75, function()
								self.leakSound:Stop();		
							end);							
						end;
						
						-- DIsable working.
						self:SetNWBool("working", false);
						timer.Simple(0.25, function()
							self:EmitSound("ambient/machines/spindown.wav", 70, 90);
						end);
						timer.Simple(0.75, function()
							self.workSound:Stop();		
						end);						
					end;					
				end;
			end;

			-- aluminum + cyfluthrin + methylamine + inter_acid
			if (self:GetNWInt("stage") == 3) then
				if ((self:GetNWInt("aluminum")>0) and (self:GetNWInt("cyfluthrin")>0) and (self:GetNWInt("methylamine")>0) and (self:GetNWInt("inter_acid")>0)) then
					if (self:GetNWInt("progress")<100) then
						self:SetNWInt("progress", self:GetNWInt("progress")+HR_CrystalMeth.ProgressAmount);
					end;
					if (self:GetNWInt("progress")==100) then
						self:SetNWInt("stage", 1);
						self:SetNWInt("progress", 0);

						-- Disable leak.
						if (self:GetNWBool("pressure_leak")) then
							self:SetNWBool("pressure_leak", false);
							timer.Simple(0.25, function()
								self:EmitSound("buttons/lever2.wav", 70, 100);
							end);
							timer.Simple(0.75, function()
								self.leakSound:Stop();		
							end);							
						end;
						
						-- DIsable working.
						self:SetNWBool("working", false);
						timer.Simple(0.25, function()
							self:EmitSound("ambient/machines/spindown.wav", 70, 90);
						end);
						timer.Simple(0.75, function()
							self.workSound:Stop();		
						end);
						
						local amph = ents.Create("hr_cm_meth");
						amph:SetPos(self:GetPos()+(self:GetUp()*32)+(self:GetRight()*7));		
						amph:SetAngles(self:GetAngles());
						amph:Spawn();
						amph:SetNWInt("aluminum", self:GetNWInt("aluminum"));
						amph:SetNWInt("cyfluthrin", self:GetNWInt("cyfluthrin"));
						amph:SetNWInt("methylamine", self:GetNWInt("methylamine"));
						amph:SetNWInt("inter_acid", self:GetNWInt("inter_acid"));

						local value1 = math.abs(HR_CrystalMeth.MethQualityFormula[1]-amph:GetNWInt("aluminum"));
						local value2 = math.abs(HR_CrystalMeth.MethQualityFormula[2]-amph:GetNWInt("cyfluthrin"));
						local value3 = math.abs(HR_CrystalMeth.MethQualityFormula[3]-amph:GetNWInt("methylamine"));
						local value4 = math.abs(HR_CrystalMeth.MethQualityFormula[4]-amph:GetNWInt("inter_acid"));
						local sum = math.Clamp(100-(value1+value2+value3+value4), 0, 100);
						amph:SetNWInt("quality", sum);
						
						self:EmitSound("physics/metal/metal_sheet_impact_hard"..table.Random({2, 6})..".wav", 70, 100);
						self:SetNWInt("aluminum", 0);
						self:SetNWInt("cyfluthrin", 0);
						self:SetNWInt("methylamine", 0);
						self:SetNWInt("inter_acid", 0);						
					end;					
				end;
			end;			
		
		end;
	self.nextProgress = CurTime() + HR_CrystalMeth.ProgressCD;
	end;
	

	if (self:GetNWBool("pressure_gain")) then
		if (self:GetNWInt("stage") == 1) then
			if (!self.nextPressure or CurTime() >= self.nextPressure) then		
				self:SetNWInt("pressure", self:GetNWInt("pressure")+(3*HR_CrystalMeth.Pressure_S1_Amount));
			self.nextPressure = CurTime() + HR_CrystalMeth.Pressure_S1_CD;
			end;
		elseif (self:GetNWInt("stage") == 2) then
			if (!self.nextPressure or CurTime() >= self.nextPressure) then	
				self:SetNWInt("pressure", self:GetNWInt("pressure")+(3*HR_CrystalMeth.Pressure_S2_Amount));
			self.nextPressure = CurTime() + HR_CrystalMeth.Pressure_S2_CD;
			end;
		elseif (self:GetNWInt("stage") == 3) then
			if (!self.nextPressure or CurTime() >= self.nextPressure) then	
				self:SetNWInt("pressure", self:GetNWInt("pressure")+(3*HR_CrystalMeth.Pressure_S3_Amount));
			self.nextPressure = CurTime() + HR_CrystalMeth.Pressure_S3_CD;
			end;		
		end;
	end;
	
	if (self:GetNWBool("pressure_leak")) then
		if (self:GetNWInt("stage") == 1) then
			if (!self.nextPressure or CurTime() >= self.nextPressure) then		
				self:SetNWInt("pressure", self:GetNWInt("pressure")-(2*HR_CrystalMeth.Pressure_S1_Amount));
			self.nextPressure = CurTime() + HR_CrystalMeth.Pressure_S1_CD;
			end;
		elseif (self:GetNWInt("stage") == 2) then
			if (!self.nextPressure or CurTime() >= self.nextPressure) then	
				self:SetNWInt("pressure", self:GetNWInt("pressure")-(2*HR_CrystalMeth.Pressure_S2_Amount));
			self.nextPressure = CurTime() + HR_CrystalMeth.Pressure_S2_CD;
			end;
		elseif (self:GetNWInt("stage") == 3) then
			if (!self.nextPressure or CurTime() >= self.nextPressure) then	
				self:SetNWInt("pressure", self:GetNWInt("pressure")-(2*HR_CrystalMeth.Pressure_S3_Amount));
			self.nextPressure = CurTime() + HR_CrystalMeth.Pressure_S3_CD;
			end;		
		end;
	elseif (!self:GetNWBool("pressure_leak")) then
		if (self:GetNWInt("stage") == 1) then
			if (!self.nextPressure or CurTime() >= self.nextPressure) then	
				if ((self:GetNWInt("aluminum")>0) and (self:GetNWInt("cyfluthrin")>0)) then	
					if (self:GetNWBool("working")) then
						self:SetNWInt("pressure", self:GetNWInt("pressure")+HR_CrystalMeth.Pressure_S1_Amount);
					end;
				end;
			self.nextPressure = CurTime() + HR_CrystalMeth.Pressure_S1_CD;
			end;
		elseif (self:GetNWInt("stage") == 2) then
			if (!self.nextPressure or CurTime() >= self.nextPressure) then	
				if ((self:GetNWInt("aluminum")>0) and (self:GetNWInt("cyfluthrin")>0) and (self:GetNWInt("methylamine")>0)) then
					if (self:GetNWBool("working")) then
						self:SetNWInt("pressure", self:GetNWInt("pressure")+HR_CrystalMeth.Pressure_S2_Amount);
					end;					
				end;
			self.nextPressure = CurTime() + HR_CrystalMeth.Pressure_S2_CD;
			end;
		elseif (self:GetNWInt("stage") == 3) then
			if (!self.nextPressure or CurTime() >= self.nextPressure) then	
				if ((self:GetNWInt("aluminum")>0) and (self:GetNWInt("cyfluthrin")>0) and (self:GetNWInt("methylamine")>0) and (self:GetNWInt("inter_acid")>0)) then
					if (self:GetNWBool("working")) then
						self:SetNWInt("pressure", self:GetNWInt("pressure")+HR_CrystalMeth.Pressure_S3_Amount);
					end;
				end;
			self.nextPressure = CurTime() + HR_CrystalMeth.Pressure_S3_CD;
			end;		
		end;		
	end;
	
	if (!self.nextLoss or CurTime() >= self.nextLoss) then
		if (self:GetNWInt("stage") == 1) then			
			if ((self:GetNWInt("aluminum")>0) and (self:GetNWInt("cyfluthrin")>0)) then	
				if (self:GetNWInt("pressure")>HR_CrystalMeth.ChemicalTank_S1_MaxPressure) then
					self:Explode();
				elseif (self:GetNWInt("pressure")<HR_CrystalMeth.ChemicalTank_S1_MinPressure) then					
					self:SetNWInt("aluminum", math.Clamp(self:GetNWInt("aluminum")-math.random(1, 2), 0, HR_CrystalMeth.ChemicalTank_Al_Hold));
				end;
			end;
		elseif (self:GetNWInt("stage") == 2) then
			if ((self:GetNWInt("aluminum")>0) and (self:GetNWInt("cyfluthrin")>0) and (self:GetNWInt("methylamine")>0)) then
				if (self:GetNWInt("pressure")>HR_CrystalMeth.ChemicalTank_S2_MaxPressure) then
					self:Explode();
				elseif (self:GetNWInt("pressure")<HR_CrystalMeth.ChemicalTank_S2_MinPressure) then					
					self:SetNWInt("methylamine", math.Clamp(self:GetNWInt("methylamine")-math.random(1, 4), 0, HR_CrystalMeth.ChemicalTank_Mt_Hold));
				end;					
			end;
		elseif (self:GetNWInt("stage") == 3) then
			if ((self:GetNWInt("aluminum")>0) and (self:GetNWInt("cyfluthrin")>0) and (self:GetNWInt("methylamine")>0) and (self:GetNWInt("inter_acid")>0)) then
				if (self:GetNWInt("pressure")>HR_CrystalMeth.ChemicalTank_S3_MaxPressure) then
					self:Explode();
				elseif (self:GetNWInt("pressure")<HR_CrystalMeth.ChemicalTank_S3_MinPressure) then					
					self:SetNWInt("progress", math.Clamp(self:GetNWInt("progress")-2, 0, 100));
				end;
			end;
		end;
		self.nextLoss = CurTime() + 1;
	end;
	
end;

function ENT:Explode()
	local effectData = EffectData();	
	effectData:SetStart(self:GetPos());
	effectData:SetOrigin(self:GetPos());
	effectData:SetScale(8);	
	util.Effect("Explosion", effectData, true, true);
	
	if (self.workSound) then
		self.workSound:Stop();
	end;
	if (self.leakSound) then
		self.leakSound:Stop();
	end;
	self:Remove();
end;

function ENT:VisualEffect()
	local effectData = EffectData();	
	effectData:SetStart(self:GetPos());
	effectData:SetOrigin(self:GetPos());
	effectData:SetScale(8);	
	util.Effect("GlassImpact", effectData, true, true);
	self:Remove();
end;

