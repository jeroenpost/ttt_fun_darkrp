AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");

function ENT:Initialize()
	self:SetModel("models/props_wasteland/kitchen_stove001a.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);
	
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);


	self:SetNWBool("working1", false);
	self:SetNWBool("working2", false);
	self:SetNWBool("working3", false);
	self:SetNWBool("working4", false);
	self:SetNWBool("working5", false);
	
	self:SetNWInt("gas", HR_CrystalMeth.StoveGas);
	
	self:GetPhysicsObject():SetVelocity(self:GetUp()*2);
	
	local backBlock = ents.Create("prop_dynamic");
	backBlock:SetModel("models/props_debris/metal_panel02a.mdl");
	backBlock:SetPos(self:GetPos()+(self:GetUp()*28)+(self:GetForward()*-18)+(self:GetRight()*-1));		
	backBlock:SetAngles(self:GetAngles()+Angle(0, 0, 90));
	backBlock:SetParent(self);
	backBlock:Spawn();
	
	self:SetHealth(HR_CrystalMeth.Stove_Health);
	self:GetPhysicsObject():SetMass(200);
end;
 
function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("hr_cm_stove");
	ent:SetPos(trace.HitPos + trace.HitNormal * 16);
	ent:Spawn();
	ent:Activate();
     
	return ent;
end;

function ENT:OnTakeDamage(dmginfo)
	if (HR_CrystalMeth.Stove_Destroyable) then
		self:SetHealth(math.Clamp(self:Health()-dmginfo:GetDamage(), 0, HR_CrystalMeth.Stove_Health));		
		self:EmitSound("weapons/fx/rics/ric"..table.Random({1, 2, 4, 5})..".wav", 70, 100);
		
		local effectdata = EffectData();
		effectdata:SetStart(dmginfo:GetDamagePosition());
		effectdata:SetOrigin(dmginfo:GetDamagePosition());
		effectdata:SetAngles(dmginfo:GetAttacker():GetAngles());
		effectdata:SetScale(1);
		util.Effect("StunstickImpact", effectdata);		
		
		if (self:Health() == 0) then
			local effectData = EffectData();	
			effectData:SetStart(self:GetPos());
			effectData:SetOrigin(self:GetPos());
			effectData:SetScale(8);	
			util.Effect("Explosion", effectData, true, true);

			self:Remove();
		end;
	end;
end;

function ENT:Use(activator, caller)
local curTime = CurTime();
	if (activator:GetEyeTrace().Entity == self) then
		if (!self.nextUse or curTime >= self.nextUse) then		
			local B_F1_Pos = self:GetPos()+(self:GetRight()*-16.25)+(self:GetUp()*35.25)+(self:GetForward()*15.5);
			local B_F2_Pos = self:GetPos()+(self:GetRight()*-20.75)+(self:GetUp()*35.25)+(self:GetForward()*15.5);
			local B_F3_Pos = self:GetPos()+(self:GetRight()*-25.25)+(self:GetUp()*35.25)+(self:GetForward()*15.5);
			local B_F4_Pos = self:GetPos()+(self:GetRight()*-29.75)+(self:GetUp()*35.25)+(self:GetForward()*15.5);
			local BU_F1_Pos = self:GetPos()+(self:GetRight()*6.50)+(self:GetUp()*41.25)+(self:GetForward()*16.5);
			
			if (activator:GetEyeTrace().HitPos:Distance(B_F1_Pos)<2) then
				self:EmitSound("buttons/lever2.wav", 55, 120);				
				if (!self:GetNWBool("working1")) then
					self:SetNWBool("working1", true);
					self:EmitSound("ambient/fire/mtov_flame2.wav", 50, 90);
					self.fireSound1 = CreateSound(self, Sound("ambient/fire/fire_small_loop1.wav"));
					self.fireSound1:SetSoundLevel(45);
					self.fireSound1:PlayEx(1, 120);
				else
					self:SetNWBool("working1", false);	
					self.fireSound1:Stop();	
				end;
			end;
	
			if (activator:GetEyeTrace().HitPos:Distance(B_F2_Pos)<2) then
				self:EmitSound("buttons/lever2.wav", 55, 120);
				if (!self:GetNWBool("working2")) then
					self:SetNWBool("working2", true);
					self:EmitSound("ambient/fire/mtov_flame2.wav", 50, 90);
					self.fireSound2 = CreateSound(self, Sound("ambient/fire/fire_small_loop1.wav"));
					self.fireSound2:SetSoundLevel(45);
					self.fireSound2:PlayEx(1, 120);					
				else
					self:SetNWBool("working2", false);
					self.fireSound2:Stop();
				end;
			end;

			if (activator:GetEyeTrace().HitPos:Distance(B_F3_Pos)<2) then
				self:EmitSound("buttons/lever2.wav", 55, 120);
				if (!self:GetNWBool("working3")) then
					self:SetNWBool("working3", true);
					self:EmitSound("ambient/fire/mtov_flame2.wav", 50, 90);
					self.fireSound3 = CreateSound(self, Sound("ambient/fire/fire_small_loop1.wav"));
					self.fireSound3:SetSoundLevel(45);
					self.fireSound3:PlayEx(1, 120);						
				else
					self:SetNWBool("working3", false);
					self.fireSound3:Stop();
				end;
			end;

			if (activator:GetEyeTrace().HitPos:Distance(B_F4_Pos)<2) then
				self:EmitSound("buttons/lever2.wav", 55, 120);
				if (!self:GetNWBool("working4")) then
					self:SetNWBool("working4", true);
					self:EmitSound("ambient/fire/mtov_flame2.wav", 50, 90);
					self.fireSound4 = CreateSound(self, Sound("ambient/fire/fire_small_loop1.wav"));
					self.fireSound4:SetSoundLevel(45);
					self.fireSound4:PlayEx(1, 120);						
				else
					self:SetNWBool("working4", false);
					self.fireSound4:Stop();
				end;
			end;
			
			if (activator:GetEyeTrace().HitPos:Distance(BU_F1_Pos)<2) then
				self:EmitSound("buttons/lever2.wav", 55, 120);
				if (!self:GetNWBool("working5")) then
					self:SetNWBool("working5", true);
					self:EmitSound("ambient/fire/mtov_flame2.wav", 50, 90);
					self.fireSound5 = CreateSound(self, Sound("ambient/fire/fire_small_loop2.wav"));
					self.fireSound5:SetSoundLevel(50);
					self.fireSound5:PlayEx(1, 120);						
				else
					self:SetNWBool("working5", false);
					self.fireSound5:Stop();
				end;
			end;			
			self.nextUse = curTime + 0.50;
		end;
	end;
end;

function ENT:Think()
	if (!self.nextHeat or CurTime() >= self.nextHeat) then	
		local traceF1 = {}	
		traceF1.start = self:GetPos()+(self:GetRight()*-11.75)+(self:GetUp()*40.5)+(self:GetForward()*6.5);
		traceF1.endpos = self:GetPos()+(self:GetRight()*-11.75)+(self:GetUp()*45.5)+(self:GetForward()*6.5);
		traceF1.filter = self;

		local traceF2 = {}	
		traceF2.start = self:GetPos()+(self:GetRight()*-25.00)+(self:GetUp()*40.5)+(self:GetForward()*6.5);
		traceF2.endpos = self:GetPos()+(self:GetRight()*-25.00)+(self:GetUp()*45.5)+(self:GetForward()*6.5);
		traceF2.filter = self;

		local traceF3 = {}	
		traceF3.start = self:GetPos()+(self:GetRight()*-11.75)+(self:GetUp()*40.5)+(self:GetForward()*-6.25);
		traceF3.endpos = self:GetPos()+(self:GetRight()*-11.75)+(self:GetUp()*45.5)+(self:GetForward()*-6.25);
		traceF3.filter = self;

		local traceF4 = {}	
		traceF4.start = self:GetPos()+(self:GetRight()*-25.00)+(self:GetUp()*40.5)+(self:GetForward()*-6.25);
		traceF4.endpos = self:GetPos()+(self:GetRight()*-25.00)+(self:GetUp()*45.5)+(self:GetForward()*-6.25);
		traceF4.filter = self;

		local traceF5 = {}	
		traceF5.start = self:GetPos()+(self:GetRight()*13.50)+(self:GetUp()*45.5)+(self:GetForward()*-1.25);
		traceF5.endpos = self:GetPos()+(self:GetRight()*13.50)+(self:GetUp()*50.5)+(self:GetForward()*-1.25);
		traceF5.filter = self;		
		
		local traceFire1 = util.TraceLine(traceF1);
		local traceFire2 = util.TraceLine(traceF2);
		local traceFire3 = util.TraceLine(traceF3);
		local traceFire4 = util.TraceLine(traceF4);
		local traceFire5 = util.TraceLine(traceF5);	
		
		if (self:GetNWBool("working1") and (self:GetNWInt("gas")>HR_CrystalMeth.StoveGas_ConsLow)) then
			self:SetNWInt("gas", self:GetNWInt("gas")-HR_CrystalMeth.StoveGas_ConsLow);
			if IsValid(traceFire1.Entity) then	
				if ((traceFire1.Entity:GetClass() == "hr_cm_potwr") or (traceFire1.Entity:GetClass() == "hr_cm_potwd") or (traceFire1.Entity:GetClass() == "hr_cm_potia")) then
					traceFire1.Entity:SetNWInt("temp", math.Clamp(traceFire1.Entity:GetNWInt("temp")+HR_CrystalMeth.StoveGas_TempLow, 0, HR_CrystalMeth.StoveGas_TempLim));
				end;
			end;			
		end;
		
		if (self:GetNWBool("working2") and (self:GetNWInt("gas")>HR_CrystalMeth.StoveGas_ConsLow)) then
			self:SetNWInt("gas", self:GetNWInt("gas")-HR_CrystalMeth.StoveGas_ConsLow);
			if IsValid(traceFire2.Entity) then	
				if ((traceFire2.Entity:GetClass() == "hr_cm_potwr") or (traceFire2.Entity:GetClass() == "hr_cm_potwd") or (traceFire2.Entity:GetClass() == "hr_cm_potia")) then
					traceFire2.Entity:SetNWInt("temp", math.Clamp(traceFire2.Entity:GetNWInt("temp")+HR_CrystalMeth.StoveGas_TempLow, 0, HR_CrystalMeth.StoveGas_TempLim));
				end;
			end;			
		end;
		
		if (self:GetNWBool("working3") and (self:GetNWInt("gas")>HR_CrystalMeth.StoveGas_ConsLow)) then
			self:SetNWInt("gas", self:GetNWInt("gas")-HR_CrystalMeth.StoveGas_ConsLow);
			if IsValid(traceFire3.Entity) then	
				if ((traceFire3.Entity:GetClass() == "hr_cm_potwr") or (traceFire3.Entity:GetClass() == "hr_cm_potwd") or (traceFire3.Entity:GetClass() == "hr_cm_potia")) then
					traceFire3.Entity:SetNWInt("temp", math.Clamp(traceFire3.Entity:GetNWInt("temp")+HR_CrystalMeth.StoveGas_TempLow, 0, HR_CrystalMeth.StoveGas_TempLim));
				end;
			end;			
		end;
		
		if (self:GetNWBool("working4") and (self:GetNWInt("gas")>HR_CrystalMeth.StoveGas_ConsLow)) then
			self:SetNWInt("gas", self:GetNWInt("gas")-HR_CrystalMeth.StoveGas_ConsLow);
			if IsValid(traceFire4.Entity) then	
				if ((traceFire4.Entity:GetClass() == "hr_cm_potwr") or (traceFire4.Entity:GetClass() == "hr_cm_potwd") or (traceFire4.Entity:GetClass() == "hr_cm_potia")) then
					traceFire4.Entity:SetNWInt("temp", math.Clamp(traceFire4.Entity:GetNWInt("temp")+HR_CrystalMeth.StoveGas_TempLow, 0, HR_CrystalMeth.StoveGas_TempLim));
				end;
			end;
		end;
		
		if (self:GetNWBool("working5") and (self:GetNWInt("gas")>HR_CrystalMeth.StoveGas_ConsBig)) then
			self:SetNWInt("gas", self:GetNWInt("gas")-HR_CrystalMeth.StoveGas_ConsBig);
			if IsValid(traceFire5.Entity) then	
				if ((traceFire5.Entity:GetClass() == "hr_cm_potwr") or (traceFire5.Entity:GetClass() == "hr_cm_potwd") or (traceFire5.Entity:GetClass() == "hr_cm_potia")) then
					traceFire5.Entity:SetNWInt("temp", math.Clamp(traceFire5.Entity:GetNWInt("temp")+HR_CrystalMeth.StoveGas_TempBig, 0, HR_CrystalMeth.StoveGas_TempLim));
				end;
			end;
		end;
	self.nextHeat = CurTime() + 0.10;
	end;
end;

function ENT:VisualEffect()
	local effectData = EffectData();	
	effectData:SetStart(self:GetPos());
	effectData:SetOrigin(self:GetPos());
	effectData:SetScale(8);	
	util.Effect("GlassImpact", effectData, true, true);
	self:Remove();
end;

