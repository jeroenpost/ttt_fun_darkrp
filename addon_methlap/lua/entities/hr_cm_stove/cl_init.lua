include("shared.lua");

surface.CreateFont("HR_CR_StoveFontSmall", {
	font = "Arial",
	size = 24,
	weight = 600,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
});

function ENT:Initialize()	
	self.fireTime = CurTime();
	self.firePos1 = ParticleEmitter(self:GetPos()+(self:GetRight()*-11.75)+(self:GetUp()*41.5)+(self:GetForward()*6.5));
	self.firePos2 = ParticleEmitter(self:GetPos()+(self:GetRight()*-25.00)+(self:GetUp()*41.5)+(self:GetForward()*6.5));
	self.firePos3 = ParticleEmitter(self:GetPos()+(self:GetRight()*-11.75)+(self:GetUp()*41.5)+(self:GetForward()*-6.25));
	self.firePos4 = ParticleEmitter(self:GetPos()+(self:GetRight()*-25.00)+(self:GetUp()*41.5)+(self:GetForward()*-6.25));
	self.firePos5 = ParticleEmitter(self:GetPos()+(self:GetRight()*13.50)+(self:GetUp()*47.5)+(self:GetForward()*-1.25));
end;

function ENT:Think()
	--if (!self:GetNWBool("locked") and self:GetNWInt("amount")>0) then
		if (self.fireTime < CurTime()) then
			if (self:GetNWBool("working1")) then
				local fire = self.firePos1:Add("sprites/flamelet"..math.random(1, 5), self:GetPos()+(self:GetRight()*-11.75)+(self:GetUp()*41.5)+(self:GetForward()*6.5));
				fire:SetVelocity(Vector(math.random(-10, 10), math.random(-10, 10), Vector(0, 0, 512)));
				fire:SetDieTime(1);
				fire:SetStartAlpha(255);
				fire:SetEndAlpha(0);
				fire:SetStartSize(1);
				fire:SetEndSize(0.2);
				fire:SetRoll(math.random(180, 480));
				fire:SetRollDelta(math.random(-3, 3));
				fire:SetColor(255, 150, 0);
				fire:SetGravity(Vector(0, 0, 64));
				fire:SetAirResistance(256);
			end;
			if (self:GetNWBool("working2")) then
				local fire = self.firePos2:Add("sprites/flamelet"..math.random(1, 5), self:GetPos()+(self:GetRight()*-25.00)+(self:GetUp()*41.5)+(self:GetForward()*6.5));
				fire:SetVelocity(Vector(math.random(-10, 10), math.random(-10, 10), Vector(0, 0, 512)));
				fire:SetDieTime(1);
				fire:SetStartAlpha(255);
				fire:SetEndAlpha(0);
				fire:SetStartSize(1);
				fire:SetEndSize(0.2);
				fire:SetRoll(math.random(180, 480));
				fire:SetRollDelta(math.random(-3, 3));
				fire:SetColor(255, 150, 0);
				fire:SetGravity(Vector(0, 0, 64));
				fire:SetAirResistance(256);
			end;
			if (self:GetNWBool("working3")) then
				local fire = self.firePos3:Add("sprites/flamelet"..math.random(1, 5), self:GetPos()+(self:GetRight()*-11.75)+(self:GetUp()*41.5)+(self:GetForward()*-6.25));
				fire:SetVelocity(Vector(math.random(-10, 10), math.random(-10, 10), Vector(0, 0, 512)));
				fire:SetDieTime(1);
				fire:SetStartAlpha(255);
				fire:SetEndAlpha(0);
				fire:SetStartSize(1);
				fire:SetEndSize(0.2);
				fire:SetRoll(math.random(180, 480));
				fire:SetRollDelta(math.random(-3, 3));
				fire:SetColor(255, 150, 0);
				fire:SetGravity(Vector(0, 0, 64));
				fire:SetAirResistance(256);
			end;
			if (self:GetNWBool("working4")) then
				local fire = self.firePos4:Add("sprites/flamelet"..math.random(1, 5), self:GetPos()+(self:GetRight()*-25.00)+(self:GetUp()*41.5)+(self:GetForward()*-6.25));
				fire:SetVelocity(Vector(math.random(-10, 10), math.random(-10, 10), Vector(0, 0, 512)));
				fire:SetDieTime(1);
				fire:SetStartAlpha(255);
				fire:SetEndAlpha(0);
				fire:SetStartSize(1);
				fire:SetEndSize(0.2);
				fire:SetRoll(math.random(180, 480));
				fire:SetRollDelta(math.random(-3, 3));
				fire:SetColor(255, 150, 0);
				fire:SetGravity(Vector(0, 0, 64));
				fire:SetAirResistance(256);
			end;
			if (self:GetNWBool("working5")) then
				local fire = self.firePos5:Add("sprites/flamelet"..math.random(1, 5), self:GetPos()+(self:GetRight()*13.50)+(self:GetUp()*47.5)+(self:GetForward()*-1.25));
				fire:SetVelocity(Vector(math.random(-10, 10), math.random(-10, 10), Vector(0, 0, 512)));
				fire:SetDieTime(1);
				fire:SetStartAlpha(255);
				fire:SetEndAlpha(0);
				fire:SetStartSize(1.6);
				fire:SetEndSize(0.2);
				fire:SetRoll(math.random(180, 480));
				fire:SetRollDelta(math.random(-3, 3));
				fire:SetColor(150, 255, 150);
				fire:SetGravity(Vector(0, 0, 72));
				fire:SetAirResistance(256);
			end;			
			self.fireTime = CurTime() + 0.1;				
		end;
	--end;
end;

function ENT:Draw()
	self:DrawModel();
	
	local camPos = self:GetPos();
	local camAng = self:GetAngles();
	
	local L_F1_Pos = self:GetPos()+(self:GetRight()*-11.75)+(self:GetUp()*41.5)+(self:GetForward()*6.5);
	local L_F2_Pos = self:GetPos()+(self:GetRight()*-25.00)+(self:GetUp()*41.5)+(self:GetForward()*6.5);
	local L_F3_Pos = self:GetPos()+(self:GetRight()*-11.75)+(self:GetUp()*41.5)+(self:GetForward()*-6.25);
	local L_F4_Pos = self:GetPos()+(self:GetRight()*-25.00)+(self:GetUp()*41.5)+(self:GetForward()*-6.25);

	local B_F1_Pos = self:GetPos()+(self:GetRight()*-16.25)+(self:GetUp()*36.10)+(self:GetForward()*16.10);
	local B_F2_Pos = self:GetPos()+(self:GetRight()*-20.75)+(self:GetUp()*36.10)+(self:GetForward()*16.10);
	local B_F3_Pos = self:GetPos()+(self:GetRight()*-25.25)+(self:GetUp()*36.10)+(self:GetForward()*16.10);
	local B_F4_Pos = self:GetPos()+(self:GetRight()*-29.75)+(self:GetUp()*36.10)+(self:GetForward()*16.10);
	
	local U_F1_Pos = self:GetPos()+(self:GetRight()*13.50)+(self:GetUp()*47.5)+(self:GetForward()*-1.25);
	local BU_F1_Pos = self:GetPos()+(self:GetRight()*6.50)+(self:GetUp()*41.25)+(self:GetForward()*16.5);
	
	camAng:RotateAroundAxis(camAng:Up(), 90);
	camAng:RotateAroundAxis(camAng:Forward(), 90);	
	--camAng:RotateAroundAxis(camAng:Right(), 180);
	
	if (LocalPlayer():GetPos():Distance(self:GetPos()) < HR_CrystalMeth.DrawDistance) then	
		render.SetMaterial(Material("sprites/glow04_noz"));
		render.DrawSprite(L_F1_Pos, 4, 4, Color(165, 95, 0, 255));
		render.DrawSprite(L_F2_Pos, 4, 4, Color(165, 95, 0, 255));
		render.DrawSprite(L_F3_Pos, 4, 4, Color(165, 95, 0, 255));
		render.DrawSprite(L_F4_Pos, 4, 4, Color(165, 95, 0, 255));
		
		render.DrawSprite(U_F1_Pos, 4, 4, Color(100, 255, 100, 255));
	
		render.DrawSprite(B_F1_Pos, 4, 4, Color(255, 255, 100, 255));
		render.DrawSprite(B_F2_Pos, 4, 4, Color(255, 255, 100, 255));
		render.DrawSprite(B_F3_Pos, 4, 4, Color(255, 255, 100, 255));
		render.DrawSprite(B_F4_Pos, 4, 4, Color(255, 255, 100, 255));
		
		render.DrawSprite(BU_F1_Pos, 4, 4, Color(255, 0, 0, 255));
				
		cam.Start3D2D(camPos+camAng:Up()*15.50, camAng, 0.075)
			surface.SetDrawColor(Color(0, 0, 0, 200));
			surface.DrawRect(66, -436, 363, 16)	

			surface.SetDrawColor(HR_CrystalMeth.StoveGasColor);
			surface.DrawRect(68, -434, math.Clamp((self:GetNWInt("gas")*359)/HR_CrystalMeth.StoveGas, 0, 359), 12)		
		cam.End3D2D();
		
		cam.Start3D2D(camPos+camAng:Up()*15.50, camAng, 0.0375)
			draw.SimpleTextOutlined("Gas ("..self:GetNWInt("gas").."/"..HR_CrystalMeth.StoveGas.." u)", "HR_CR_StoveFontSmall", 256, -856, Color(200, 200, 200, 200), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));			
		cam.End3D2D();		
	end;
end;