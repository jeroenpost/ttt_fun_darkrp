AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");

function ENT:Initialize()
	self:SetModel("models/props/cs_office/Cardboard_box01.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);
	
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);
	
	self:SetNWInt("amount", HR_CrystalMeth.AluminumBox_Amount);
	self:SetPos(self:GetPos()+Vector(0, 0, 8));
	self:GetPhysicsObject():SetVelocity(self:GetUp()*2);
	
	for i1=1, 5 do
		for i=1, 2 do
			local fakeAg = ents.Create("prop_dynamic");
			fakeAg:SetModel("models/gibs/metal_gib2.mdl");
			fakeAg:SetPos(self:GetPos()+(self:GetUp()*0.2)+(self:GetForward()*-12)+(self:GetUp()*i*6.2)+(fakeAg:GetForward()*i1*4));		
			fakeAg:SetAngles(self:GetAngles());
			fakeAg:SetParent(self);
			fakeAg:Spawn();
		end;
	end;
	
	self:SetCollisionGroup(COLLISION_GROUP_WEAPON);
	self:GetPhysicsObject():SetMass(105);
end;

function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("hr_cm_aluminum_box");
	ent:SetPos(trace.HitPos + trace.HitNormal * 16);
	ent:Spawn();
	ent:Activate();
    ent:CPPISetOwner(ply)
     
	return ent;
end;

function ENT:PhysicsCollide(data, phys)
	local dEnt = data.HitEntity;
	if ((data.DeltaTime > 0) and (dEnt:GetClass() == "hr_cm_tank") and (dEnt:GetNWInt("stage") == 1)) then
		if (dEnt:GetNWInt("aluminum")<HR_CrystalMeth.ChemicalTank_Al_Hold) then
			for i=1, self:GetNWInt("amount") do
				dEnt:SetNWInt("aluminum", math.Clamp(dEnt:GetNWInt("aluminum") + 1, 0, HR_CrystalMeth.ChemicalTank_Al_Hold));
			end;
			self:EmitSound("physics/metal/metal_barrel_impact_soft"..math.random(1, 4)..".wav", 70, 100);			
			self:VisualEffect();
		end;
	end;
end;

function ENT:Use(activator, caller)
local curTime = CurTime();
	if (activator:GetEyeTrace().Entity == self) then
		if (!self.nextUse or curTime >= self.nextUse) then
			if (HR_CrystalMeth.CanOpenBoxes) then
				if (activator:KeyDown(IN_SPEED)) then
					self:EmitSound("physics/cardboard/cardboard_box_break2.wav", 70, 100);
					for i=1, self:GetNWInt("amount") do
						local aluminum = ents.Create("hr_cm_aluminum");
							local di = i%2;
							if (di == 0) then
								aluminum:SetPos(self:GetPos()+(self:GetUp()*1)+(self:GetForward()*i*2));		
							else
								aluminum:SetPos(self:GetPos()+(self:GetUp()*1)+(self:GetForward()*-i*2));
							end
						aluminum:SetAngles(self:GetAngles());							
						aluminum:Spawn();
                        aluminum:CPPISetOwner(activator)
					end;
					self:VisualEffect();					
				elseif (!activator:KeyDown(IN_SPEED)) then
					if (self:GetNWInt("amount")>0) then
						self:SetNWInt("amount", self:GetNWInt("amount")-1);
						self:EmitSound("physics/cardboard/cardboard_box_break3.wav", 70, 100);
						
						local aluminum = ents.Create("hr_cm_aluminum");
						aluminum:SetPos(self:GetPos()+(self:GetUp()*14));
						aluminum:SetAngles(activator:GetAngles());
						aluminum:Spawn();
                        aluminum:CPPISetOwner(activator)
						if (self:GetNWInt("amount")==0) then
							self:EmitSound("physics/cardboard/cardboard_box_break2.wav", 70, 100);
							self:VisualEffect();
						end;
					end;		
				end;
			end;
			self.nextUse = curTime + 0.5;
		end;
	end;
end;

function ENT:OnTakeDamage(dmginfo)
	self:EmitSound("physics/cardboard/cardboard_box_break2.wav", 70, 100);
	for i=1, self:GetNWInt("amount") do
		local aluminum = ents.Create("hr_cm_aluminum");
		local di = i%2;
		if (di == 0) then
			aluminum:SetPos(self:GetPos()+(self:GetUp()*1)+(aluminum:GetForward()*i*2));		
		else
			aluminum:SetPos(self:GetPos()+(self:GetUp()*1)+(aluminum:GetForward()*-i*2));
		end
		aluminum:SetAngles(self:GetAngles());
		aluminum:Spawn();
	end;
	self:VisualEffect();
end;

function ENT:VisualEffect()
	local effectData = EffectData();	
	effectData:SetStart(self:GetPos());
	effectData:SetOrigin(self:GetPos());
	effectData:SetScale(8);	
	util.Effect("GlassImpact", effectData, true, true);
	self:Remove();
end;

