include("shared.lua");

surface.CreateFont("HR_CR_BoxFontBig", {
	font = "Arial",
	size = 48,
	weight = 600,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
});

surface.CreateFont("HR_CR_BoxFontSmall", {
	font = "Arial",
	size = 32,
	weight = 600,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
});

function ENT:Initialize()	
	self.leakTime = CurTime();
	self.leakPos = ParticleEmitter(self:GetPos()+(self:GetRight()*7)+(self:GetUp()*11));
end;

function ENT:Think()
	local upPos = self:GetPos()+(self:GetForward()*10)+(self:GetUp()*45.5);
	local downPos = self:GetPos()+(self:GetForward()*10)+(self:GetUp()*5.5);
	if (downPos.z > upPos.z) then
		if (!self:GetNWBool("locked") and self:GetNWInt("amount")>0) then
			if (self.leakTime < CurTime()) then
				local blood = self.leakPos:Add("particle/rain", upPos);
				blood:SetVelocity(Vector(math.random(-10, 10), math.random(-10, 10), self:GetForward()*512));
				blood:SetDieTime(1);
				blood:SetStartAlpha(255);
				blood:SetEndAlpha(0);
				blood:SetStartSize(1);
				blood:SetEndSize(math.random(4, 6));
				blood:SetRoll(math.random(180, 480));
				blood:SetRollDelta(math.random(-3, 3));
				blood:SetColor(255, 255, 255);
				blood:SetGravity(Vector(0, 0, -1200));
				blood:SetAirResistance(256);
				blood:SetCollide(true);
				blood:SetCollideCallback( function(part, hitPos, hitNormal)
						local eData = EffectData();
						eData:SetOrigin(hitPos);
						util.Effect("GlassImpact", eData);
						self:EmitSound("ambient/water/rain_drip"..math.random(1, 4)..".wav", 50, 70);
					end);
				self.leakTime = CurTime() + 0.05;				
			end;
		end;
	end;
end;

function ENT:Draw()
	self:DrawModel();
	
	local camPos = self:GetPos();
	local camAng = self:GetAngles();

	camAng:RotateAroundAxis(camAng:Up(), 90);
	camAng:RotateAroundAxis(camAng:Forward(), 90);	
	camAng:RotateAroundAxis(camAng:Right(), 180);
	
	if (LocalPlayer():GetPos():Distance(self:GetPos()) < HR_CrystalMeth.DrawDistance) then	
		render.SetMaterial(Material("sprites/glow04_noz"));
		if (!self:GetNWBool("locked")) then
			render.DrawSprite(self:GetPos()+(self:GetForward()*10)+(self:GetUp()*45.5), 8, 8, Color(100, 255, 100, 255));
		else
			render.DrawSprite(self:GetPos()+(self:GetForward()*10)+(self:GetUp()*45.5), 8, 8, Color(255, 100, 100, 255));
		end;
		cam.Start3D2D(camPos+camAng:Up()*14, camAng, 0.075)
			draw.SimpleTextOutlined(HR_CrystalMeth.Methylamine_Text, "HR_CR_BoxFontBig", 0, -400, HR_CrystalMeth.Methylamine_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			draw.SimpleTextOutlined(self:GetNWInt("amount").." L", "HR_CR_BoxFontBig", 0, -356, HR_CrystalMeth.Methylamine_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));			
		cam.End3D2D();
		cam.Start3D2D(camPos+camAng:Up()*14, camAng, 0.055)
			draw.SimpleTextOutlined("___________________", "HR_CR_BoxFontSmall", 0, -324, HR_CrystalMeth.Methylamine_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			draw.SimpleTextOutlined(HR_CrystalMeth.Methylamine_TextCopyright, "HR_CR_BoxFontSmall", 0, -296, HR_CrystalMeth.Methylamine_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));			

			if (IsValid(LocalPlayer():GetActiveWeapon())) then	
				if (LocalPlayer():GetActiveWeapon():GetClass() != "weapon_physcannon") then
					draw.SimpleTextOutlined("Choose PhysGun first", "HR_CR_BoxFontSmall", 0, -326, Color(255, 255, 255, math.Clamp(math.cos(CurTime())*35, 0, 35)), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 0, Color(25, 25, 25, 0));
				end;
				if (LocalPlayer():GetActiveWeapon():GetClass() == "weapon_physcannon") then
					draw.SimpleTextOutlined("USE to take", "HR_CR_BoxFontSmall", 0, -354, Color(255, 255, 255, math.Clamp(math.cos(CurTime())*35, 0, 35)), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 0, Color(25, 25, 25, 0));
					draw.SimpleTextOutlined("SHIFT to open/close", "HR_CR_BoxFontSmall", 0, -326, Color(255, 255, 255, math.Clamp(math.cos(CurTime())*35, 0, 35)), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 0, Color(25, 25, 25, 0));
				end;
			end;
		cam.End3D2D();		
	camAng:RotateAroundAxis(camAng:Right(), 180);

		cam.Start3D2D(camPos+camAng:Up()*14, camAng, 0.075)
			draw.SimpleTextOutlined(HR_CrystalMeth.Methylamine_Text, "HR_CR_BoxFontBig", 0, -400, HR_CrystalMeth.Methylamine_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			draw.SimpleTextOutlined(self:GetNWInt("amount").." L", "HR_CR_BoxFontBig", 0, -356, HR_CrystalMeth.Methylamine_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));			
		cam.End3D2D();
		cam.Start3D2D(camPos+camAng:Up()*14, camAng, 0.055)
			draw.SimpleTextOutlined("___________________", "HR_CR_BoxFontSmall", 0, -324, HR_CrystalMeth.Methylamine_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			draw.SimpleTextOutlined(HR_CrystalMeth.Methylamine_TextCopyright, "HR_CR_BoxFontSmall", 0, -296, HR_CrystalMeth.Methylamine_TextColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));			

			if (IsValid(LocalPlayer():GetActiveWeapon())) then		
				if (LocalPlayer():GetActiveWeapon():GetClass() != "weapon_physcannon") then
					draw.SimpleTextOutlined("Choose PhysGun first", "HR_CR_BoxFontSmall", 0, -326, Color(255, 255, 255, math.Clamp(math.cos(CurTime())*35, 0, 35)), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 0, Color(25, 25, 25, 0));
				end;
				if (LocalPlayer():GetActiveWeapon():GetClass() == "weapon_physcannon") then
					draw.SimpleTextOutlined("USE to take", "HR_CR_BoxFontSmall", 0, -354, Color(255, 255, 255, math.Clamp(math.cos(CurTime())*35, 0, 35)), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 0, Color(25, 25, 25, 0));
					draw.SimpleTextOutlined("SHIFT to open/close", "HR_CR_BoxFontSmall", 0, -326, Color(255, 255, 255, math.Clamp(math.cos(CurTime())*35, 0, 35)), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 0, Color(25, 25, 25, 0));
				end;
			end;
		cam.End3D2D();
	
	end;
end;