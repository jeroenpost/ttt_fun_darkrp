include("shared.lua");

surface.CreateFont("HR_CR_FlyFont", {
	font = "Arial",
	size = 32,
	weight = 600,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
});

surface.CreateFont("HR_CR_PriceFont", {
	font = "Arial",
	size = 32,
	weight = 800,
	blursize = 0,
	scanlines = 0,
	antialias = true,
	underline = false,
	italic = false,
	strikeout = false,
	symbol = false,
	rotary = false,
	shadow = false,
	additive = false,
	outline = false,
});

function ENT:Initialize()	

end;

function ENT:Draw()
	self:DrawModel();
	
	local camPos = self:GetPos();
	local camAng = self:GetAngles();

	if (LocalPlayer():GetPos():Distance(self:GetPos()) < HR_CrystalMeth.DrawDistance) then
		cam.Start3D2D(camPos+Vector(0, 0, 3.5), Angle(0, LocalPlayer():EyeAngles().y-90, 90), 0.075)
			draw.SimpleTextOutlined((self:GetNWInt("quality")).."%", "HR_CR_FlyFont", 0, 18, HR_CrystalMeth.MethName_Color, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			draw.SimpleTextOutlined(HR_CrystalMeth.MethName, "HR_CR_FlyFont", 0, -6, HR_CrystalMeth.MethName_Color, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			draw.SimpleTextOutlined(string.Comma(self:GetNWInt("quality")*HR_CrystalMeth.MethPrice_Mod).."$", "HR_CR_PriceFont", 0, -32, Color(100, 255, 100, math.Clamp(math.cos(CurTime())*255, 0, 255)), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, math.Clamp(math.cos(CurTime())*255, 0, 255)));
		cam.End3D2D();
	end;
end;
