AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");

function ENT:Initialize()
	self:SetModel("models/gibs/shield_scanner_gib1.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);
	
	self:SetMaterial("models/shiny");
	self:SetColor(HR_CrystalMeth.MethName_Color);
	
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);
	
	self:SetNWInt("aluminum", 50);
	self:SetNWInt("cyfluthrin", 50);
	self:SetNWInt("methylamine", 50);
	self:SetNWInt("inter_acid", 0);
	
	local value1 = math.abs(HR_CrystalMeth.MethQualityFormula[1]-self:GetNWInt("aluminum"));
	local value2 = math.abs(HR_CrystalMeth.MethQualityFormula[2]-self:GetNWInt("cyfluthrin"));
	local value3 = math.abs(HR_CrystalMeth.MethQualityFormula[3]-self:GetNWInt("methylamine"));
	local value4 = math.abs(HR_CrystalMeth.MethQualityFormula[4]-self:GetNWInt("inter_acid"));
	local sum = math.Clamp(100-(value1+value2+value3+value4), 0, 100);
	self:SetNWInt("quality", sum);
	
	self:SetPos(self:GetPos()+Vector(0, 0, 8));
	self:GetPhysicsObject():SetVelocity(self:GetUp()*2);
	
	self:SetCollisionGroup(COLLISION_GROUP_WEAPON);
	self:GetPhysicsObject():SetMass(105);
end;
 
function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("hr_cm_meth");
	ent:SetPos(trace.HitPos + trace.HitNormal * 16);
	ent:Spawn();
	ent:Activate();
     
	return ent;
end;

function ENT:Use(activator, caller)
local curTime = CurTime();
	if (activator:GetEyeTrace().Entity == self) then
		if (!self.nextUse or curTime >= self.nextUse) then	
			if (!table.HasValue(HR_CrystalMeth.CopJobs, team.GetName(activator:Team()))) then
				if (!HR_CrystalMeth.UseNPC) then
					if (self:GetNWInt("quality")==100) then
						activator:SendLua("local tab = {Color(255,255,255),[[You sold ]], HR_CrystalMeth.MethName_Color,[["..self:GetNWInt("quality").."%]],Color(255,255,255),[[ Amphetamine and got ]],Color(100,255,100),[["..self:GetNWInt("quality")*HR_CrystalMeth.MethPrice_Mod*HR_CrystalMeth.PerfectCookReward_Mod.."$]],Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");
					else
						activator:SendLua("local tab = {Color(255,255,255),[[You sold ]], HR_CrystalMeth.MethName_Color,[["..self:GetNWInt("quality").."%]],Color(255,255,255),[[ Amphetamine and got ]],Color(100,255,100),[["..self:GetNWInt("quality")*HR_CrystalMeth.MethPrice_Mod.."$]],Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");
					end;
					if (self:GetNWInt("quality")>100) then
						activator:EmitSound("vo/k_lab2/kl_greatscott.wav", 70, 100);
					elseif (self:GetNWInt("quality")>75) then
						activator:EmitSound("vo/k_lab/kl_mygoodness01.wav", 70, 100);
					elseif (self:GetNWInt("quality")>50) then
						activator:EmitSound("vo/k_lab/kl_diditwork.wav", 70, 100);			
					else
						activator:EmitSound("vo/k_lab/kl_hedyno03.wav", 70, 100);	
					end;
					if (self:GetNWInt("quality")==100) then
						activator:addMoney(self:GetNWInt("quality")*HR_CrystalMeth.MethPrice_Mod*HR_CrystalMeth.PerfectCookReward_Mod);
					else
						activator:addMoney(self:GetNWInt("quality")*HR_CrystalMeth.MethPrice_Mod);
					end;
					self:Remove();
				elseif (HR_CrystalMeth.UseNPC) then
					if ((activator:GetNWInt("amphetamine") == 0) or (activator:GetNWInt("amphetamine") == nil)) then					
						if (self:GetNWInt("quality")>100) then
							activator:EmitSound("vo/k_lab2/kl_greatscott.wav", 70, 100);
						elseif (self:GetNWInt("quality")>75) then
							activator:EmitSound("vo/k_lab/kl_mygoodness01.wav", 70, 100);
						elseif (self:GetNWInt("quality")>50) then
							activator:EmitSound("vo/k_lab/kl_diditwork.wav", 70, 100);			
						else
							activator:EmitSound("vo/k_lab/kl_hedyno03.wav", 70, 100);	
						end;	
						activator:SetNWInt("amphetamine", self:GetNWInt("quality"));
						activator:SendLua("local tab = {Color(255,255,255),[[You picked up ]], HR_CrystalMeth.MethName_Color,[["..self:GetNWInt("quality").."%]],Color(255,255,255),[[ Amphetamine. Selling price: ]],Color(100,255,100),[["..self:GetNWInt("quality")*HR_CrystalMeth.MethPrice_Mod.."$]],Color(255,255,255),[[. Bring it to buyer.]]}chat.AddText(unpack(tab))");
						self:Remove();
					else
						activator:SendLua("local tab = {Color(255,255,255),[[You alredy have ]], HR_CrystalMeth.MethName_Color,[[Amphetamine]],Color(255,255,255),[[ Sell old one first.]]}chat.AddText(unpack(tab))");
					end;
				end;
			elseif (table.HasValue(HR_CrystalMeth.CopJobs, team.GetName(activator:Team()))) then
				activator:EmitSound("npc/metropolice/vo/ihave10-30my10-20responding.wav", 70, 95);
				activator:SendLua("local tab = {Color(255,255,255),[[You confiscated ]], HR_CrystalMeth.MethName_Color,[["..self:GetNWInt("quality").."%]],Color(255,255,255),[[ Amphetamine and got ]],Color(100,255,100),[["..self:GetNWInt("quality")*HR_CrystalMeth.MethPrice_Mod*HR_CrystalMeth.CopSellMod.."$]],Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");
				activator:addMoney(math.Round(self:GetNWInt("quality")*HR_CrystalMeth.MethPrice_Mod*HR_CrystalMeth.CopSellMod));
				
				local amph_conf = ents.Create("prop_physics");
				amph_conf:SetModel("models/props_lab/box01a.mdl");
				amph_conf:SetPos(self:GetPos()+Vector(0, 0, 8));		
				amph_conf:SetAngles(activator:GetAngles()+Angle(0, 180, 0));
				amph_conf:SetCollisionGroup(COLLISION_GROUP_WEAPON);				
				amph_conf:Spawn();
				amph_conf:GetPhysicsObject():SetMass(105);
				
				local amph_in = ents.Create("prop_physics");
				amph_in:SetModel("models/gibs/shield_scanner_gib1.mdl");				
				amph_in:SetMaterial("models/shiny");
				amph_in:SetColor(HR_CrystalMeth.MethName_Color);
				amph_in:SetParent(amph_conf);
				amph_in:SetPos(amph_conf:GetPos()+(amph_conf:GetRight()*1));		
				amph_in:SetAngles(amph_conf:GetAngles()+Angle(0, 30, 0));
				amph_in:SetCollisionGroup(COLLISION_GROUP_WEAPON);	
				amph_in:Spawn();
				amph_in:GetPhysicsObject():SetMass(105);
				
				timer.Simple(10, function()
					SafeRemoveEntity(amph_conf);
					SafeRemoveEntity(amph_in);
				end);
				
				amph_conf:EmitSound("physics/cardboard/cardboard_box_break2.wav", 70, 100);
				
				self:Remove();
			end;
			self.nextUse = curTime + 0.5;
		end;
	end;
end;

function ENT:PhysicsCollide(data, phys)
	local dEnt = data.HitEntity;
	if (data.DeltaTime > 0) and (dEnt:GetClass() == "hr_cm_potwd") then
		if (dEnt:GetNWInt("temp")>=dEnt:GetNWInt("crit_temp")) then
			if (dEnt:GetNWInt("sulfurd")<HR_CrystalMeth.PotWD_SulfurMax) then
				dEnt:SetNWInt("sulfurd", math.Clamp(dEnt:GetNWInt("sulfurd") + 1, 0, HR_CrystalMeth.PotWD_SulfurMax));
				dEnt:GetPhysicsObject():SetVelocity((dEnt:GetForward()*math.random(-2, 2))+(dEnt:GetRight()*math.random(-2, 2)));	
				dEnt:EmitSound("physics/metal/metal_solid_impact_soft"..math.random(1, 3)..".wav", 70 , 95);
				self:VisualEffect();
			end;
		end;
	end;
end;

function ENT:OnTakeDamage(dmginfo)
	self:VisualEffect();
end;

function ENT:VisualEffect()
	local effectData = EffectData();	
	effectData:SetStart(self:GetPos());
	effectData:SetOrigin(self:GetPos());
	effectData:SetScale(8);	
	util.Effect("GlassImpact", effectData, true, true);
	self:Remove();
end;

