include("shared.lua");

function ENT:Initialize()	
	self.leakTime = CurTime();
	self.leakPos = ParticleEmitter(self:GetPos()+(self:GetRight()*0.35)+(self:GetUp()*8.5));
end;

function ENT:Think()
	local upPos = self:GetPos()+(self:GetUp()*9);
	local downPos = self:GetPos()+(self:GetUp()*2);
	if (downPos.z > upPos.z) then
		if (!self:GetNWBool("locked") and self:GetNWInt("amount")>0) then
			if (self.leakTime < CurTime()) then
				local liquid = self.leakPos:Add("particle/rain", upPos);
				liquid:SetVelocity(Vector(math.random(-10, 10), math.random(-10, 10), self:GetForward()*512));
				liquid:SetDieTime(1);
				liquid:SetStartAlpha(255);
				liquid:SetEndAlpha(0);
				liquid:SetStartSize(1);
				liquid:SetEndSize(2);
				liquid:SetRoll(math.random(180, 480));
				liquid:SetRollDelta(math.random(-3, 3));
				if (self:GetNWString("type") == "redp") then
					liquid:SetColor(255, 25, 25);
				elseif (self:GetNWString("type") == "sulfur") then
					liquid:SetColor(255, 190, 25);	
				end;
				liquid:SetGravity(Vector(0, 0, -800));
				liquid:SetAirResistance(256);
				liquid:SetCollide(true);
				liquid:SetCollideCallback( function(part, hitPos, hitNormal)
						local eData = EffectData();
						eData:SetOrigin(hitPos);
						util.Effect("GlassImpact", eData);
						self:EmitSound("ambient/water/rain_drip"..math.random(1, 4)..".wav", 50, 30);
					end);
				self.leakTime = CurTime() + 0.1;				
			end;
		end;
	end;
end;

function ENT:Draw()
	self:DrawModel();
	
	local camPos = self:GetPos();
	local camAng = self:GetAngles();

	camAng:RotateAroundAxis(camAng:Up(), 90);
	camAng:RotateAroundAxis(camAng:Forward(), 90);	
	camAng:RotateAroundAxis(camAng:Right(), 72.5);
	
	if (LocalPlayer():GetPos():Distance(self:GetPos()) < HR_CrystalMeth.DrawDistance) then	
		render.SetMaterial(Material("sprites/glow04_noz"));
		if (!self:GetNWBool("locked")) then
			render.DrawSprite(self:GetPos()+(self:GetUp()*9), 4, 4, Color(100, 255, 100, 255));
		else
			render.DrawSprite(self:GetPos()+(self:GetUp()*9), 4, 4, Color(255, 0, 0, 255));
		end;	

		cam.Start3D2D(camPos+(camAng:Up()*4.2), camAng, 0.030)
			draw.SimpleTextOutlined("Chemical Glass", "HR_CR_FlyFont", 0, -40, Color(255, 255, 225, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			draw.SimpleTextOutlined("_____________", "HR_CR_FlyFont", 0, -40, Color(255, 255, 225, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));		
			
			if (self:GetNWString("type") == "none") then
				draw.SimpleTextOutlined("Empty", "HR_CR_FlyFont", 0, 0, Color(90, 90, 90, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
				draw.SimpleTextOutlined("Fill with Acid", "HR_CR_FlyFont", 0, 40, Color(90, 90, 90, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			elseif (self:GetNWString("type") == "redp") then
				draw.SimpleTextOutlined(HR_CrystalMeth.Glass_Phosphoric_Acid, "HR_CR_FlyFont", 0, 0, HR_CrystalMeth.Glass_Phosphoric_Acid_Color, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
				draw.SimpleTextOutlined(self:GetNWInt("amount").." L", "HR_CR_FlyFont", 0, 30, HR_CrystalMeth.Glass_Phosphoric_Acid_Color, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			elseif (self:GetNWString("type") == "sulfur") then
				draw.SimpleTextOutlined(HR_CrystalMeth.Glass_Sulfuric_Acid, "HR_CR_FlyFont", 0, 0, HR_CrystalMeth.Glass_Sulfuric_Acid_Color, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
				draw.SimpleTextOutlined(self:GetNWInt("amount").." L", "HR_CR_FlyFont", 0, 30, HR_CrystalMeth.Glass_Sulfuric_Acid_Color, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
			elseif (self:GetNWString("type") == "i-acid") then
				draw.SimpleTextOutlined(HR_CrystalMeth.Glass_Intermediate_Acid, "HR_CR_FlyFont", 0, 0, HR_CrystalMeth.Glass_Intermediate_Acid_Color, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));
				draw.SimpleTextOutlined(self:GetNWInt("amount").." L", "HR_CR_FlyFont", 0, 30, HR_CrystalMeth.Glass_Intermediate_Acid_Color, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color(25, 25, 25, 100));			
			end;
		cam.End3D2D();		
	end;
end;