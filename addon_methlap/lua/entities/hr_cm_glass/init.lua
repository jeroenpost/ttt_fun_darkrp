AddCSLuaFile("cl_init.lua");
AddCSLuaFile("shared.lua");
include("shared.lua");

function ENT:Initialize()
	self:SetModel("models/props_junk/garbage_glassbottle002a.mdl");
	self:PhysicsInit(SOLID_VPHYSICS);
	
	self:SetMoveType(MOVETYPE_VPHYSICS);
	self:SetSolid(SOLID_VPHYSICS);
	

	self:SetNWInt("amount", 0);
	self:SetNWString("type", "none");
	
	self:SetNWBool("locked", true);
	self:SetPos(self:GetPos()+Vector(0, 0, 12));
	self:GetPhysicsObject():SetVelocity(self:GetUp()*2);
	
	self:SetCollisionGroup(COLLISION_GROUP_WEAPON);
	self:GetPhysicsObject():SetMass(105);
end;
 
function ENT:SpawnFunction(ply, trace)
	local ent = ents.Create("hr_cm_glass");
	ent:SetPos(trace.HitPos);
	ent:Spawn();
	ent:Activate();
     
	return ent;
end;

function ENT:OnTakeDamage(dmginfo)
	if (HR_CrystalMeth.Glass_CrackOnShoot) then
		self:GlassCrack();
	end;
end;

function ENT:PhysicsCollide(data, phys)
	local dEnt = data.HitEntity;
	if (self:GetNWString("type") == "none") then
		-- If Phosphoric Acid
		if (data.DeltaTime > 0) and (dEnt:GetClass() == "hr_cm_potwr") then			
			if (dEnt:GetNWInt("temp")>=dEnt:GetNWInt("boil_temp") and (dEnt:GetNWInt("cook_time") == 0)) then
				if (dEnt:GetNWInt("redp")>0) then
					self:SetNWString("type", "redp");
					self:SetNWInt("amount", dEnt:GetNWInt("redp")+math.Round(dEnt:GetNWInt("water")/1.5));
					dEnt:SetNWInt("redp", 0);
					dEnt:SetNWInt("water", 0);
					dEnt:SetNWInt("temp", math.Round(self:GetNWInt("temp")/2));
					
					dEnt:SetNWInt("cook_time", HR_CrystalMeth.PotWR_CookTime);
					dEnt:EmitSound("ambient/water/water_splash1.wav", 70 , 95);
					self:SetColor(Color(125, 0, 0, 255));
				end;
			end;
		end;
		
		-- If Sulfuric Acid
		if (data.DeltaTime > 0) and (dEnt:GetClass() == "hr_cm_potwd") then			
			if (dEnt:GetNWInt("temp")>=dEnt:GetNWInt("boil_temp") and (dEnt:GetNWInt("cook_time") == 0)) then
				if (dEnt:GetNWInt("sulfurd")>0) then
					self:SetNWString("type", "sulfur");
					self:SetNWInt("amount", dEnt:GetNWInt("sulfurd")+math.Round(dEnt:GetNWInt("water")/2));
					dEnt:SetNWInt("sulfurd", 0);
					dEnt:SetNWInt("water", 0);
					dEnt:SetNWInt("temp", math.Round(self:GetNWInt("temp")/2))
					
					dEnt:SetNWInt("cook_time", HR_CrystalMeth.PotWD_CookTime);
					dEnt:EmitSound("ambient/water/water_splash1.wav", 70 , 95);
					self:SetColor(Color(255, 240, 0, 255));
				end;
			end;
		end;

		-- If Inter Acid
		if (data.DeltaTime > 0) and (dEnt:GetClass() == "hr_cm_potia") then			
			if (dEnt:GetNWInt("temp")>=dEnt:GetNWInt("boil_temp") and (dEnt:GetNWInt("cook_time") == 0)) then
				if ((dEnt:GetNWInt("s-acid")>0) and (dEnt:GetNWInt("p-acid")>0)) then
					self:SetNWString("type", "i-acid");
					self:SetNWInt("amount", dEnt:GetNWInt("s-acid")+dEnt:GetNWInt("p-acid"));
					dEnt:SetNWInt("s-acid", 0);
					dEnt:SetNWInt("p-acid", 0);
					dEnt:SetNWInt("temp", math.Round(self:GetNWInt("temp")/2))
					
					dEnt:SetNWInt("cook_time", HR_CrystalMeth.PotSA_CookTime);
					dEnt:EmitSound("ambient/water/water_splash1.wav", 70 , 95);
					self:SetColor(Color(255, 255, 255, 255));
				end;
			end;
		end;		
	end;
end;

function ENT:Use(activator, caller)
local curTime = CurTime();
	if (activator:GetEyeTrace().Entity == self) then
		if (!self.nextUse or curTime >= self.nextUse) then		
			if (self:GetNWBool("locked")) then
				if (activator:GetActiveWeapon():GetClass() == "weapon_physcannon") then
					self:SetPos(self:GetPos()+Vector(0, 0, 1));
					self:GetPhysicsObject():SetVelocity(self:GetUp()*2);
					self:SetAngles(Angle(180, 0, 0));
					activator:ConCommand("+attack2");
					timer.Simple(0.1, function() activator:ConCommand("-attack2") end);
				end;
			else
				self:SetAngles(Angle(0, 0, 0));
			end;
			self.nextUse = curTime + 0.5;
		end;
	end;
end;

function ENT:Think()
	local upPos = self:GetPos()+(self:GetRight()*0.35)+(self:GetUp()*9);
	local downPos = self:GetPos()+(self:GetRight()*0.35)+(self:GetUp()*-2);
	if (downPos.z > upPos.z) then
		--self:EmitSound("npc/barnacle/barnacle_gulp1.wav", 70, 110);
		if (!self.nextFuel or CurTime() >= self.nextFuel) then	
			if (!self:GetNWBool("locked") and self:GetNWInt("amount")>0) then
				local trace = {}	
				trace.start = upPos;
				trace.endpos = upPos+Vector(0, 0, -4096);
				trace.filter = self;
				
				local traceRes = util.TraceLine(trace);			
				
				self:SetNWInt("amount", self:GetNWInt("amount")-1);
				if (self:GetNWInt("amount")==0) then
					self:EmitSound("ambient/water/water_splash3.wav", 70, 110);
					self:SetNWString("type", "none");
				end;
				local trEnt = traceRes.Entity;
				if (self:GetNWString("type") == "redp") then
					if IsValid(trEnt) then
						if ((trEnt:GetClass() == "hr_cm_potia") and (trEnt:GetNWInt("cook_time")!=0)) then
							if (trEnt:GetNWInt("p-acid")<HR_CrystalMeth.PotSA_PhosphorMax) then
								local eData = EffectData();
								eData:SetOrigin(traceRes.HitPos);
								util.Effect("BloodImpact", eData);
								
								trEnt:EmitSound("ambient/water/water_splash"..math.random(1, 3)..".wav", 70, 120);
								trEnt:SetNWInt("p-acid", math.Clamp(trEnt:GetNWInt("p-acid")+1, 0, HR_CrystalMeth.PotSA_PhosphorMax));
							end;
						end;
					end;
				elseif (self:GetNWString("type") == "sulfur") then
					if IsValid(trEnt) then
						if ((trEnt:GetClass() == "hr_cm_potia") and (trEnt:GetNWInt("cook_time")!=0)) then
							if (trEnt:GetNWInt("s-acid")<HR_CrystalMeth.PotSA_SulfurMax) then
								local eData = EffectData();
								eData:SetOrigin(traceRes.HitPos);
								util.Effect("BloodImpact", eData);
								
								trEnt:EmitSound("ambient/water/water_splash"..math.random(1, 3)..".wav", 70, 120);
								trEnt:SetNWInt("s-acid", math.Clamp(trEnt:GetNWInt("s-acid")+1, 0, HR_CrystalMeth.PotSA_SulfurMax));
							end;
						end;
					end;				
				elseif (self:GetNWString("type") == "i-acid") then
					if IsValid(trEnt) then
						if ((trEnt:GetClass() == "hr_cm_tank") and (trEnt:GetNWInt("stage")==3)) then
							if (trEnt:GetNWInt("inter_acid")<HR_CrystalMeth.ChemicalTank_Ia_Hold) then
								local eData = EffectData();
								eData:SetOrigin(traceRes.HitPos);
								util.Effect("BloodImpact", eData);
								
								trEnt:EmitSound("ambient/water/water_splash"..math.random(1, 3)..".wav", 70, 120);
								trEnt:SetNWInt("inter_acid", math.Clamp(trEnt:GetNWInt("inter_acid")+1, 0, HR_CrystalMeth.ChemicalTank_Ia_Hold+1));
							end;
						end;
					end;
				end;
			end;
			self.nextFuel = CurTime() + 0.45;
		end;
	end;
end;


function ENT:GlassCrack()
	local effectData = EffectData();	
	effectData:SetStart(self:GetPos());
	effectData:SetOrigin(self:GetPos());
	effectData:SetScale(8);	
	util.Effect("GlassImpact", effectData, true, true);
	self:Remove();
end;

hook.Add("KeyPress", "HR_KeyPress_Glass", function(player, key)
	local lookEnt = player:GetEyeTrace().Entity;
	if IsValid(lookEnt) then
		if (lookEnt:GetClass() == "hr_cm_glass") then
			if (lookEnt:GetPos():Distance(player:GetPos())<96) then
				if (key == IN_SPEED) then
					if (lookEnt:GetNWBool("locked")) then
						lookEnt:SetNWBool("locked", false);
					else
						lookEnt:SetNWBool("locked", true);
					end;
					lookEnt:EmitSound("buttons/lever7.wav", 70, 130);
				end;
			end;
		end;
	end;
end);