AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

function HR_CM_SpawnBuyer()	
	if not file.IsDir("hr_cm", "DATA") then
		file.CreateDir("hr_cm", "DATA");
	end;
	
	if not file.IsDir("hr_cm/"..string.lower(game.GetMap()).."", "DATA") then
		file.CreateDir("hr_cm/"..string.lower(game.GetMap()).."", "DATA");
	end;

	for k, v in pairs(file.Find("hr_cm/"..string.lower(game.GetMap()).."/*.txt", "DATA")) do
		local methPosFile = file.Read("hr_cm/"..string.lower(game.GetMap()).."/".. v, "DATA");
	 
		local spawnNumber = string.Explode(";", methPosFile);
		
		local methPos = Vector(spawnNumber[1], spawnNumber[2], spawnNumber[3]);
		local methAngles = Angle(tonumber(spawnNumber[4]), spawnNumber[5], spawnNumber[6]);
		
		local methBuyer = ents.Create("hr_cm_buyer");
		methBuyer:SetPos(methPos);
		methBuyer:SetAngles(methAngles);
		methBuyer:Spawn();
	end;
end;
timer.Simple(1, HR_CM_SpawnBuyer);

function HR_CM_SpawnBuyerPos(ply, cmd, args)
	if (ply:IsAdmin() or ply:IsSuperAdmin()) then
		local fileMethName = args[1];
		
		if not fileMethName then
			ply:SendLua("local tab = {HR_CrystalMeth.MethName_Color, [[HR: Crystal Meth - ]], Color(255,255,255), [[Choose a name for your buyer NPC!]] } chat.AddText(unpack(tab))");
			return;
		end;
		
		if file.Exists( "hr_cm/"..string.lower(game.GetMap()).."/amph_".. fileMethName ..".txt", "DATA") then 
			ply:SendLua("local tab = {HR_CrystalMeth.MethName_Color, [[HR: Crystal Meth - ]], [[This name is alredy in use, choose another one or remove this one by typing 'hr_cm_removenpc "..fileMethName.."' in console.]] } chat.AddText(unpack(tab))");
			return;
		end;
		
		local methVector = string.Explode(" ", tostring(ply:GetEyeTrace().HitPos));
		local methAngles = string.Explode(" ", tostring(ply:GetAngles()+Angle(0, -180, 0)));

		local methBuyer = ents.Create("hr_cm_buyer");
		methBuyer:SetPos(ply:GetEyeTrace().HitPos);
		methBuyer:SetAngles(ply:GetAngles()+Angle(0, -180, 0));
		methBuyer:Spawn();
		
		file.Write("hr_cm/".. string.lower(game.GetMap()) .."/amph_".. fileMethName ..".txt", ""..(methVector[1])..";"..(methVector[2])..";"..(methVector[3])..";"..(methAngles[1])..";"..(methAngles[2])..";"..(methAngles[3]).."", "DATA");
		ply:SendLua("local tab = {HR_CrystalMeth.MethName_Color, [[HR: Crystal Meth - ]], Color(255,255,255),[[New pos for the buyer NPC has been set.]] } chat.AddText(unpack(tab))");
	else
		ply:SendLua("local tab = {HR_CrystalMeth.MethName_Color, [[HR: Crystal Meth - ]], Color(255,255,255),[[Only admins and superadmins can perform this action.]] } chat.AddText(unpack(tab))");
	end;
end;
concommand.Add("hr_cm_spawnnpc", HR_CM_SpawnBuyerPos);

function HR_CM_RemoveBuyer(ply, cmd, args)
	if (ply:IsAdmin() or ply:IsSuperAdmin()) then
		local fileMethName = args[1];
		
		if not fileMethName then
			ply:SendLua("local tab = {HR_CrystalMeth.MethName_Color, [[HR: Crystal Meth - ]], Color(255,255,255), [[Please enter a name of file!]] } chat.AddText(unpack(tab))");
			return;
		end;
		
		if file.Exists("hr_cm/".. string.lower(game.GetMap()) .."/amph_"..fileMethName..".txt", "DATA") then
			file.Delete("hr_cm/".. string.lower(game.GetMap()) .."/amph_"..fileMethName..".txt");
			ply:SendLua("local tab = {HR_CrystalMeth.MethName_Color, [[HR: Crystal Meth - ]], Color(255,255,255), [[This buyer NPC has been removed. Restart your server!]] } chat.AddText(unpack(tab))");
			return;
		end;
		
	else
		ply:SendLua("local tab = {HR_CrystalMeth.MethName_Color, [[HR: Crystal Meth - ]], Color(255,255,255), [[Only admins and superadmins can perform this action.]] } chat.AddText(unpack(tab))");					
	end;
end;
concommand.Add("hr_cm_removenpc", HR_CM_RemoveBuyer);

function ENT:Initialize()
	self:SetModel("models/Humans/Group02/Male_05.mdl");
	self:SetHullType(HULL_HUMAN);
	self:SetHullSizeNormal();
	self:SetNPCState(NPC_STATE_SCRIPT);
	self:SetSolid(SOLID_BBOX);
	self:SetUseType(SIMPLE_USE);
	self:SetBloodColor(BLOOD_COLOR_RED);
end;

function ENT:AcceptInput(name, activator, caller)	
	if (!self.nextUse or CurTime() >= self.nextUse) then
		if (name == "Use" and caller:IsPlayer() and (caller:GetNWInt("amphetamine") == 0)) then			
			caller:SendLua("local tab = {HR_CrystalMeth.MethName_Color, [["..HR_CrystalMeth.NameNPC..": ]], Color(255,255,255), [[Go away!]] } chat.AddText(unpack(tab))");
		elseif (name == "Use") and (caller:IsPlayer()) and (caller:GetNWInt("amphetamine") > 0) then
			caller:addMoney(caller:GetNWInt("amphetamine")*HR_CrystalMeth.MethPrice_Mod);
			caller:SendLua("local tab = {HR_CrystalMeth.MethName_Color, [["..HR_CrystalMeth.NameNPC..": ]], Color(255,255,255), [[Nice, here is your ]], Color(100,255,100), [["..caller:GetNWInt("amphetamine")*HR_CrystalMeth.MethPrice_Mod.."$.]] } chat.AddText(unpack(tab))");
			caller:SetNWInt("amphetamine", 0);
			if (HR_CrystalMeth.MakeWantedOnSell) then
				caller:wanted(nil, "Selling Amphetamine");
			end;
			timer.Simple(0.25, function() self:EmitSound("vo/npc/male01/moan0"..math.random(1, 5)..".wav") end);
		end;
		self.nextUse = CurTime()+3;
	end;
end;

function ENT:OnTakeDamage(dmginfo)
	return false;
end;

function ENT:OnTakeDamage(dmginfo)
	return false;
end;