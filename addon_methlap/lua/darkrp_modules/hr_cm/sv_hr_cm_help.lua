local HR_CM_ThinkCD = HR_CrystalMeth.HintTime;
hook.Add("PlayerTick", "HR_CM_Help", function(player)
	if (player:Team() == TEAM_HR_CM) then
		if (!player.HR_CM_Help or CurTime() >= player.HR_CM_Help) then
			player:SendLua("LocalPlayer():EmitSound('vo/k_lab/kl_almostforgot.wav', 70, 100)");
			player:SendLua("local tab = {Color(255,255,255),[[Hey doc, here is ]],HR_CrystalMeth.MethName_Color,[[Amphetamine ]], Color(255,255,255),[[rough formula information:]]}chat.AddText(unpack(tab))");
			player:SendLua("local tab = {HR_CrystalMeth.AluminumBox_TextColor,[["..HR_CrystalMeth.AluminumBox_Text.."]], Color(255,255,255),[[ - "..(math.abs(HR_CrystalMeth.MethQualityFormula[1]-HR_CrystalMeth.MethQualityFormula_WrongInfo[1])*250).."g]]}chat.AddText(unpack(tab))");
			player:SendLua("local tab = {HR_CrystalMeth.CyfluthrinCan_TextColor,[["..HR_CrystalMeth.CyfluthrinCan_Text.."]], Color(255,255,255),[[ - "..math.abs(HR_CrystalMeth.MethQualityFormula[2]-HR_CrystalMeth.MethQualityFormula_WrongInfo[2]).." L]]}chat.AddText(unpack(tab))");
			player:SendLua("local tab = {HR_CrystalMeth.Methylamine_TextColor,[["..HR_CrystalMeth.Methylamine_Text.."]], Color(255,255,255),[[ - "..math.abs(HR_CrystalMeth.MethQualityFormula[3]-HR_CrystalMeth.MethQualityFormula_WrongInfo[3]).." L]]}chat.AddText(unpack(tab))");
			player:SendLua("local tab = {HR_CrystalMeth.Glass_Intermediate_Acid_Color,[["..HR_CrystalMeth.Glass_Intermediate_Acid.."]], Color(255,255,255),[[ - "..math.abs(HR_CrystalMeth.MethQualityFormula[4]-HR_CrystalMeth.MethQualityFormula_WrongInfo[4]).." L]]}chat.AddText(unpack(tab))");					
			player.HR_CM_Help = CurTime() + HR_CrystalMeth.HintTime;
		end;
	end;
end);
local function showhelp(player, text, teamonly)
    if text == "!showrecipe" then
        player:SendLua("LocalPlayer():EmitSound('vo/k_lab/kl_almostforgot.wav', 70, 100)");
        player:SendLua("local tab = {Color(255,255,255),[[Hey doc, here is ]],HR_CrystalMeth.MethName_Color,[[Amphetamine ]], Color(255,255,255),[[rough formula information:]]}chat.AddText(unpack(tab))");
        player:SendLua("local tab = {HR_CrystalMeth.AluminumBox_TextColor,[["..HR_CrystalMeth.AluminumBox_Text.."]], Color(255,255,255),[[ - "..(math.abs(HR_CrystalMeth.MethQualityFormula[1]-HR_CrystalMeth.MethQualityFormula_WrongInfo[1])*250).."g]]}chat.AddText(unpack(tab))");
        player:SendLua("local tab = {HR_CrystalMeth.CyfluthrinCan_TextColor,[["..HR_CrystalMeth.CyfluthrinCan_Text.."]], Color(255,255,255),[[ - "..math.abs(HR_CrystalMeth.MethQualityFormula[2]-HR_CrystalMeth.MethQualityFormula_WrongInfo[2]).." L]]}chat.AddText(unpack(tab))");
        player:SendLua("local tab = {HR_CrystalMeth.Methylamine_TextColor,[["..HR_CrystalMeth.Methylamine_Text.."]], Color(255,255,255),[[ - "..math.abs(HR_CrystalMeth.MethQualityFormula[3]-HR_CrystalMeth.MethQualityFormula_WrongInfo[3]).." L]]}chat.AddText(unpack(tab))");
        player:SendLua("local tab = {HR_CrystalMeth.Glass_Intermediate_Acid_Color,[["..HR_CrystalMeth.Glass_Intermediate_Acid.."]], Color(255,255,255),[[ - "..math.abs(HR_CrystalMeth.MethQualityFormula[4]-HR_CrystalMeth.MethQualityFormula_WrongInfo[4]).." L]]}chat.AddText(unpack(tab))");

    end
end
hook.Add( "PlayerSay", "help_HM_CR", showhelp )

hook.Add("PlayerHurt", "HR_CM_PlayerHurt", function(player, attacker)
	if (IsValid(player) and player:Alive()) then
		if (player:GetNWInt("amphetamine") > 0) then
			local amph = ents.Create("hr_cm_meth");
			amph:SetPos(player:GetPos()+(player:GetUp()*48));		
			amph:SetAngles(player:GetAngles());
			amph:Spawn();
			amph:SetNWInt("quality", player:GetNWInt("amphetamine"));
			amph:GetPhysicsObject():SetVelocity((amph:GetUp()*128)+(amph:GetForward()*math.random(-256, 256))+(amph:GetRight()*math.random(-128, 128)));
			player:SetNWInt("amphetamine", 0);
			player:EmitSound("physics/flesh/flesh_impact_hard3.wav", 70, 100);
			player:SendLua("local tab = {Color(255,255,255),[[You got hurt and dropped ]], HR_CrystalMeth.MethName_Color,[[Amphetamine "..amph:GetNWInt("quality").."%]],Color(255,255,255),[[.]]}chat.AddText(unpack(tab))");
		end;
	end;
end);