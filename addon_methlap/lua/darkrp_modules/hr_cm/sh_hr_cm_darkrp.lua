TEAM_HR_CM = DarkRP.createJob("Amphetamine Cook", {
	color = Color(170, 190, 255, 255),
	model = {
		"models/player/hostage/hostage_04.mdl",
		"models/player/hostage/hostage_01.mdl"},
	description = [[The lowest person of crime.
		A amphetamine cook generally works for the Mobboss who runs the crime family.
		The Mob boss sets your agenda and you follow it or you might be punished.]],
	weapons = {},
	command = "amphcook",
	max = 5,
	salary = 0,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Other",
    level = 20,
    hasLicense = false,
    VIPOnly = {"vipp","vippp"},
    vipName = "VIP+",
    jobs_special = true,
    jobs_vipp = true
})

DarkRP.createEntity(HR_CrystalMeth.AluminumBox_Text, {
	ent = "hr_cm_aluminum_box",
	model = "models/props/cs_office/Cardboard_box01.mdl",
	price = 500,
	max = 20,
	cmd = "buyaluminum",
	allowed = TEAM_HR_CM,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity(HR_CrystalMeth.CyfluthrinCan_Text, {
	ent = "hr_cm_cyfluthrin",
	model = "models/props_junk/metalgascan.mdl",
	price = 250,
	max = 20,
	cmd = "buycyfluthrin",
	allowed = TEAM_HR_CM,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Chemical Glass", {
	ent = "hr_cm_glass",
	model = "models/props_junk/glassjug01.mdl",
	price = 100,
	max = 20,
	cmd = "buyglass",
	allowed = TEAM_HR_CM,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity(HR_CrystalMeth.Methylamine_Text, {
	ent = "hr_cm_methylamine",
	model = "models/props_c17/oildrum001.mdl",
	price = 1000,
	max = 5,
	cmd = "buymethylamine",
	allowed = TEAM_HR_CM,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity(HR_CrystalMeth.PotSA_Text, {
	ent = "hr_cm_potia",
	model = "models/props_c17/metalPot001a.mdl",
	price = 1000,
	max = 5,
	cmd = "buypotia",
	allowed = TEAM_HR_CM,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity(HR_CrystalMeth.PotWR_Text, {
	ent = "hr_cm_potwr",
	model = "models/props_interiors/pot02a.mdl",
	price = 500,
	max = 5,
	cmd = "buypotwr",
	allowed = TEAM_HR_CM,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity(HR_CrystalMeth.PotWD_Text, {
	ent = "hr_cm_potwd",
	model = "models/props_interiors/pot02a.mdl",
	price = 500,
	max = 5,
	cmd = "buypotwd",
	allowed = TEAM_HR_CM,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity(HR_CrystalMeth.RedPBox_Text, {
	ent = "hr_cm_redp_box",
	model = "models/props/cs_office/Cardboard_box01.mdl",
	price = 1000,
	max = 5,
	cmd = "buyredp",
	allowed = TEAM_HR_CM,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Stove", {
	ent = "hr_cm_stove",
	model = "models/props_wasteland/kitchen_stove001a.mdl",
	price = 2000,
	max = 3,
	cmd = "buycmstove",
	allowed = TEAM_HR_CM,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Chemical Tank", {
	ent = "hr_cm_tank",
	model = "models/props_wasteland/laundry_washer001a.mdl",
	price = 4000,
	max = 3,
	cmd = "buychemtank",
	allowed = TEAM_HR_CM,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity(HR_CrystalMeth.WaterOne_Text, {
	ent = "hr_cm_water",
	model = "models/props_junk/garbage_milkcarton001a.mdl",
	price = 50,
	max = 20,
	cmd = "buycmwater",
	allowed = TEAM_HR_CM,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity(HR_CrystalMeth.SulfurDBox_Text, {
	ent = "hr_cm_sulfurd_box",
	model = "models/props/cs_office/Cardboard_box01.mdl",
	price = 1000,
	max = 20,
	cmd = "buysulfurd",
	allowed = TEAM_HR_CM,
    shop_drugs = true,
    shop = true
})




TEAM_HR_METH = DarkRP.createJob("Meth Cook", {
    color = Color(170, 190, 255, 255),
    model = {
        "models/player/hostage/hostage_04.mdl",
        "models/player/hostage/hostage_01.mdl"},
    description = [[The lowest person of crime.
		A amphetamine cook generally works for the Mobboss who runs the crime family.
		The Mob boss sets your agenda and you follow it or you might be punished.]],
    weapons = {},
    command = "methcook",
    max = 5,
    salary = 0,
    admin = 0,
    vote = false,
    hasLicense = false,
    category = "Other",
    level = 20,
    hasLicense = false,
    VIPOnly = {"vip","vipp","vippp"},
    vipName = "VIP",
    jobs_special = true,
    jobs_vip = true
})

DarkRP.createEntity("Gas Canister", {
    ent = "eml_gas",
    model = "models/props_c17/canister01a.mdl",
    price = 50,
    max = 20,
    cmd = "buygascanister",
    allowed = TEAM_HR_METH,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Liquid Iodine", {
    ent = "eml_iodine",
    model = "models/props_lab/jar01b.mdl",
    price = 120,
    max = 20,
    cmd = "buyiodine",
    allowed = TEAM_HR_METH,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Jar", {
    ent = "eml_jar",
    model = "models/props_lab/jar01a.mdl",
    price = 50,
    max = 20,
    cmd = "buyjar",
    allowed = TEAM_HR_METH,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Muriatic Acid", {
    ent = "eml_macid",
    model = "models/props_junk/garbage_plasticbottle001a.mdl",
    price = 60,
    max = 20,
    cmd = "buymacid",
    allowed = TEAM_HR_METH,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Pot", {
    ent = "eml_pot",
    model = "models/props_c17/metalPot001a.mdl",
    price = 150,
    max = 20,
    cmd = "buypot",
    allowed = TEAM_HR_METH,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Special Pot", {
    ent = "eml_spot",
    model = "models/props_c17/metalPot001a.mdl",
    price = 250,
    max = 20,
    cmd = "buyspot",
    allowed = TEAM_HR_METH,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Stove", {
    ent = "eml_stove",
    model = "models/props_c17/furnitureStove001a.mdl",
    price = 3000,
    max = 5,
    cmd = "buystove",
    allowed = TEAM_HR_METH,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Liquid Sulfur", {
    ent = "eml_sulfur",
    model = "models/props_lab/jar01b.mdl",
    price = 120,
    max = 20,
    cmd = "buysulfur",
    allowed = TEAM_HR_METH,
    shop_drugs = true,
    shop = true
})

DarkRP.createEntity("Water", {
    ent = "eml_water",
    model = "models/props_junk/garbage_plasticbottle003a.mdl",
    price = 50,
    max = 20,
    cmd = "buywater",
    allowed = TEAM_HR_METH,
    shop_drugs = true,
    shop = true
})