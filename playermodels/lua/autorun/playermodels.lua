local function AddPlayerModel( name, model )
    list.Set( "PlayerOptionsModel", name, model )
    player_manager.AddValidModel( name, model )
end

AddPlayerModel( "Batman", 		"models/player/superheroes/batman.mdl" );
AddPlayerModel( "Superman", 		"models/player/superheroes/superman.mdl" );
AddPlayerModel( "Jesus", 		"models/player/jesus/jesus.mdl" );
AddPlayerModel( "DanBoard", 		"models/player/danboard.mdl" );
AddPlayerModel( "Stick Man", "models/conex/stickguy/stickguy.mdl" )
AddPlayerModel( "Fireman", "models/fearless/fireman2.mdl" )
AddPlayerModel( "Jeffry the Hunter", "models/newinfec/Newhun.mdl" )
AddPlayerModel( "BlockDude", "models/player/blockdude.mdl" )
AddPlayerModel(  "dogggy", "models/glayer_dog.mdl" )

AddPlayerModel( "Eevee","models/player_eevee.mdl" )
AddPlayerModel( "Espeon","models/player_espeon.mdl" )
AddPlayerModel( "Umbreon","models/player_umbreon.mdl" )
AddPlayerModel( "Flareon","models/player_flareon.mdl" )
AddPlayerModel( "Glaceon","models/player_glaceon.mdl" )
AddPlayerModel( "Leafeon","models/player_leafeon.mdl" )
AddPlayerModel( "Jolteon","models/player_jolteon.mdl" )
AddPlayerModel( "Vaporeon","models/player_vaporeon.mdl" )
AddPlayerModel( "Sylveon","models/player_sylveon.mdl" )
AddPlayerModel( "Buizel", "models/player/pokemon/buizel.mdl" )

AddPlayerModel( "Agent Smith", "models/player/smith.mdl" )
AddPlayerModel( "Joker", "models/player/bobert/joker.mdl" )

AddPlayerModel( "Alien", "models/player/slow_alien.mdl" )

player_manager.AddValidModel( "Thief", "models/player/group01/cookies114.mdl" )

AddPlayerModel( "Mobster1", "models/humans/mafia/male_02.mdl" )
AddPlayerModel( "Mobster2", "models/humans/mafia/male_04.mdl" )
AddPlayerModel( "Mobster3", "models/humans/mafia/male_06.mdl" )
AddPlayerModel( "Mobster4", "models/humans/mafia/male_07.mdl" )
AddPlayerModel( "Mobster5", "models/humans/mafia/male_08.mdl" )
AddPlayerModel( "Mobster6", "models/humans/mafia/male_09.mdl" )

AddPlayerModel(	"Soldier1", "models/player/soldier/soldier_1.mdl" )
player_manager.AddValidHands(	"Soldier1", "models/player/soldier/c_arms_soldier.mdl", 0, "00000000" )
AddPlayerModel(	"Soldier2", "models/player/soldier/soldier_2.mdl" )
player_manager.AddValidHands(	"Soldier2", "models/player/soldier/c_arms_soldier.mdl", 0, "00000000" )
AddPlayerModel(	"Soldier3", "models/player/soldier/soldier_3.mdl" )
player_manager.AddValidHands(	"Soldier3", "models/player/soldier/c_arms_soldier.mdl", 0, "00000000" )
AddPlayerModel(	"Soldier g1", "models/player/soldier/soldier_g1.mdl" )
player_manager.AddValidHands(	"Soldier g1", "models/player/soldier/c_arms_soldier.mdl", 0, "00000000" )
AddPlayerModel(	"Soldier g2", "models/player/soldier/soldier_g2.mdl" )
player_manager.AddValidHands(	"Soldier g2", "models/player/soldier/c_arms_soldier.mdl", 0, "00000000" )

AddPlayerModel( "Sanic", "models/player/sanic.mdl" )
player_manager.AddValidHands( "Sanic", "models/weapons/c_arms_sanic.mdl", 0, "11" )

AddPlayerModel( "Eggman Player Model", 				"models/player/eggman_npc.mdl" )

player_manager.AddValidModel( "shrek", "models/player/pyroteknik/shrek.mdl" )
player_manager.AddValidHands( "shrek", "models/player/hands/shrek.mdl", 0, "00000000" )


AddPlayerModel( "Spider-Man", "models/player/tasm2spider.mdl" )
player_manager.AddValidHands( "Spider-Man", "models/weapons/c_arms_hev.mdl", 0, "00000000" )

player_manager.AddValidModel( "Demon Violinist", 		"models/player/demon_violinist/demon_violinist.mdl" );
list.Set( "PlayerOptionsModel", "Demon Violinist", 	"models/player/demon_violinist/demon_violinist.mdl" );
player_manager.AddValidHands( "Demon Violinist", "models/weapons/arms/demon_violinist_arms.mdl", 0, "00000000" )

player_manager.AddValidModel( "WOS Venom", "models/player/venom.mdl" )
player_manager.AddValidHands( "WOS Venom", "models/weapons/c_arms_hev.mdl", 0, "00000000" )
list.Set( "PlayerOptionsModel", "WOS Venom", "models/player/venom.mdl" )

player_manager.AddValidModel( "Zombie Jester", "models/zombie_jester/zombie_jester.mdl" )
list.Set( "PlayerOptionsModel", "Zombie Jester", "models/zombie_jester/zombie_jester.mdl" )
player_manager.AddValidHands( "Zombie Jester", "models/weapons/c_arms_combine.mdl", 0, "10000000" )






