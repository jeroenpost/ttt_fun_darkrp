D3DCarConfig_refund = D3DCarConfig_refund or {}
D3DCarConfig_refund.Car = {}

timer.Simple(1,function()
/* ──────────────────────────────────────────────────────────────────────────────────
	Adding New CAR < If you dont know how to add, Goto 3d_car_dealer/How to add more cars.txt
────────────────────────────────────────────────────────────────────────────────── */


        local TB2Insert = {}
    TB2Insert.VehicleName = "Jeep"
    TB2Insert.CarName = "Jeep"
    TB2Insert.CarPrice = 15000
    TB2Insert.CarRefund = 10000
    TB2Insert.Description = ""
    table.insert(D3DCarConfig_refund.Car,TB2Insert)


        local TB2Insert = {}
        TB2Insert.VehicleName = "Airboat"
        TB2Insert.CarName = "Hovercraft"
        TB2Insert.CarPrice = 17500
        TB2Insert.CarRefund = 12500
        TB2Insert.Description = "Go everywhere with this vehicle!"
        table.insert(D3DCarConfig_refund.Car,TB2Insert)


	local TB2Insert = {}
		TB2Insert.VehicleName = "reventonrtdm_withseats"
		TB2Insert.CarName = "TDM - Reventon RoadSter"
		TB2Insert.CarPrice = 100
		TB2Insert.CarRefund = 100
		TB2Insert.MaxAmount = 1
		TB2Insert.AvaliableTeam = {TEAM_POLICE}
		TB2Insert.Description = "Basic Car."
	--table.insert(D3DCarConfig_refund.Car,TB2Insert)

local TB2Insert = {}
TB2Insert.VehicleName = "airtugtdm_withseats"
TB2Insert.CarName = "Airtug"
TB2Insert.CarPrice = 180000
TB2Insert.CarRefund = 150000
TB2Insert.MaxAmount = 1
TB2Insert.Model = "models/tdmcars/gtaiv_airtug.mdl"
TB2Insert.Description = "A golf cart? Airplane carrier? Or not? It's fast, thats for sure."
TB2Insert.allowed = {TEAM_CARDEALER}
table.insert(D3DCarConfig_refund.Car,TB2Insert)


local TB2Insert = {}
TB2Insert.VehicleName = "st1tdm_withseats"
TB2Insert.CarName = "Zenvo ST1"
TB2Insert.CarPrice = 425000
TB2Insert.CarRefund = 400000
TB2Insert.MaxAmount = 1
TB2Insert.Model = "models/tdmcars/zen_st1.mdl"
TB2Insert.Description = "This car means business, tons or RARRWRRR for you cash"
TB2Insert.allowed = {TEAM_CARDEALER}
table.insert(D3DCarConfig_refund.Car,TB2Insert)


local TB2Insert = {}
TB2Insert.VehicleName = "bowlexrstdm_withseats"
TB2Insert.CarName = "Bowler EXR-S"
TB2Insert.CarPrice = 225000
TB2Insert.CarRefund = 175000
TB2Insert.MaxAmount = 1
TB2Insert.Model = "models/tdmcars/bowler_exrs.mdl"
TB2Insert.Description = "All terrain powercar, 4 seats"
TB2Insert.allowed = {TEAM_CARDEALER}
table.insert(D3DCarConfig_refund.Car,TB2Insert)


local TB2Insert = {}
TB2Insert.VehicleName = "deloreantdm_withseats"
TB2Insert.CarName = "Delorean DMC-12"
TB2Insert.CarPrice = 150000
TB2Insert.CarRefund = 125000
TB2Insert.MaxAmount = 1
TB2Insert.Model = "models/tdmcars/del_dmc.mdl"
TB2Insert.Description = "Go back in time with this baby"
TB2Insert.allowed = {TEAM_CARDEALER}
table.insert(D3DCarConfig_refund.Car,TB2Insert)

local TB2Insert = {}
TB2Insert.VehicleName = "xbowtdm_withseats"
TB2Insert.CarName = "KTM X-BOW"
TB2Insert.CarPrice = 190000
TB2Insert.CarRefund = 170000
TB2Insert.MaxAmount = 1
TB2Insert.Model = "models/tdmcars/ktm_xbow.mdl"
TB2Insert.Description = "Fast, agile, like a go-kart"
TB2Insert.allowed = {TEAM_CARDEALER}
table.insert(D3DCarConfig_refund.Car,TB2Insert)

local TB2Insert = {}
TB2Insert.VehicleName = "morgaerosstdm_withseats"
TB2Insert.CarName = "Morgan Aero SS"
TB2Insert.CarPrice = 2950000
TB2Insert.CarRefund = 270000
TB2Insert.MaxAmount = 1
TB2Insert.Model = "models/tdmcars/morg_aeross.mdl"
TB2Insert.Description = "Stylish sportscar"
TB2Insert.allowed = {TEAM_CARDEALER}
table.insert(D3DCarConfig_refund.Car,TB2Insert)



local TB2Insert = {}
TB2Insert.VehicleName = "noblem600tdm_withseats"
TB2Insert.CarName ="Noble M600"
TB2Insert.CarPrice = 385000
TB2Insert.CarRefund = 280000
TB2Insert.MaxAmount = 1
TB2Insert.Model = "models/tdmcars/noblem600.mdl"
TB2Insert.Description = "Wanna be cool? You NEED this car!"
TB2Insert.allowed = {TEAM_CARDEALER}
table.insert(D3DCarConfig_refund.Car,TB2Insert)



local TB2Insert = {}
TB2Insert.VehicleName = "tesmodelstdm_withseats"
TB2Insert.CarName = "Tesla Model S"
TB2Insert.CarPrice = 280000
TB2Insert.CarRefund = 230000
TB2Insert.MaxAmount = 1
TB2Insert.Model = "models/tdmcars/tesla_models.mdl"
TB2Insert.Description = "Concerned about nature? This car is the most high tech fully electric vehicle available!"
TB2Insert.allowed = {TEAM_CARDEALER}
table.insert(D3DCarConfig_refund.Car,TB2Insert)


hook.Add("PlayerInitialSpawn","refundthecars_gb",function(ply)
    timer.Simple(8,function()
        if not IsValid(ply) then return end
        local SteamIDG = string.gsub(ply:SteamID(),":","_")

        -- CrashSaver Check

        local Data = nil
        if file.Exists( "rm_car_dealer/inventory/" .. SteamIDG .. ".txt" ,"DATA") then
            Data = util.JSONToTable(file.Read( "rm_car_dealer/inventory/" .. SteamIDG .. ".txt" ))

            for _,DB in pairs(Data or {}) do
                if DB.VehicleName ~= "Jeep" and DB.VehicleName ~= "Airboat" and not string.find(DB.VehicleName,"withseat") then
                    DB.VehicleName = DB.VehicleName.."_withseats"
                end

                for k,v in pairs( D3DCarConfig_refund.Car ) do
                    if  DB.VehicleName == v.VehicleName and isnumber(v.CarPrice) then
                        ply:addMoney( v.CarPrice )
                        ply:ChatPrint("Refunded your "..DB.VehicleName.." for ".. v.CarPrice)
                        print("Refunded money")
                    end
                end

            end
            file.Delete("rm_car_dealer/inventory/" .. SteamIDG .. ".txt" )
        end

        if file.Exists( "vc_npc_cars/"..ply:UniqueID()..".txt" ,"DATA") then
            for k,v in pairs( string.Explode( ";", file.Read( "vc_npc_cars/"..ply:UniqueID()..".txt", "DATA" ) ) ) do
                if istable(VC_NPC_Cars[ k ]) and VC_NPC_Cars[ k ].price then
                    ply:addMoney( tonumber(VC_NPC_Cars[ k ].price) )
                    ply:ChatPrint("Refunded a car for ".. VC_NPC_Cars[ k ].price)
                end
            end
            file.Delete("vc_npc_cars/"..ply:UniqueID()..".txt" )
        end

    end)
end)



/* ──────────────────────────────────────────────────────────────────────────────────
	Adding New CAR :: END
────────────────────────────────────────────────────────────────────────────────── */
end)
	