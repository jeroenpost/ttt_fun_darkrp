local V = {
			Name = "Lamborghini Murcielago LP 670-4 SV", 
			Class = "prop_vehicle_jeep",
			Category = "TDM Cars",
			Author = "TheDanishMaster, Turn 10",
			Information = "A drivable Lamborghini Murcielago LP 670-4 SV by TheDanishMaster",
				Model = "models/tdmcars/lambo_murcielagosv.mdl",
							KeyValues = {
							vehiclescript	=	"scripts/vehicles/tdmcars/murcielagosv.txt"
							}
			}
list.Set("Vehicles", "murcielagosvtdm", V)