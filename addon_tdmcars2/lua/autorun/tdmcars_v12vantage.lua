local V = {
			Name = "Aston Martin V12 Vantage 2010", 
			Class = "prop_vehicle_jeep",
			Category = "TDM Cars",
			Author = "TheDanishMaster, Turn 10",
			Information = "A drivable Aston Martin V12 Vantage by TheDanishMaster",
				Model = "models/tdmcars/aston_v12vantage.mdl",
							KeyValues = {
							vehiclescript	=	"scripts/vehicles/TDMCars/v12vantage.txt"
							}
			}
list.Set("Vehicles", "v12vantagetdm", V)