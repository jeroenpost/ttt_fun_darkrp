sound.Add( 
{
    name = "tdm69camaro_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/69camaro/idle.wav"
} )

sound.Add( 
{
    name = "tdm69camaro_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/69camaro/start.wav"
} )

sound.Add( 
{
    name = "tdm69camaro_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/69camaro/rev.wav"
} )

sound.Add( 
{
    name = "tdm69camaro_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = {"vehicles/tdmcars/69camaro/first_2.mp3",
	         "vehicles/tdmcars/69camaro/first.mp3"}
} )

sound.Add( 
{
    name = "tdm69camaro_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/69camaro/second.mp3"
} )

sound.Add( 
{
    name = "tdm69camaro_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/69camaro/third.mp3"
} )

sound.Add( 
{
    name = "tdm69camaro_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/69camaro/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdm69camaro_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/69camaro/second.mp3"
} )

sound.Add( 
{
    name = "tdm69camaro_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/69camaro/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmc12_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/c12/idle.wav"
} )

sound.Add( 
{
    name = "tdmc12_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/c12/start.wav"
} )

sound.Add( 
{
    name = "tdmc12_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/c12/rev.wav"
} )

sound.Add( 
{
    name = "tdmc12_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = {"vehicles/tdmcars/c12/first_2.mp3",
	         "vehicles/tdmcars/c12/first.mp3"}
} )

sound.Add( 
{
    name = "tdmc12_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/c12/second.mp3"
} )

sound.Add( 
{
    name = "tdmc12_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/c12/third.mp3"
} )

sound.Add( 
{
    name = "tdmc12_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/c12/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmc12_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/c12/second.mp3"
} )

sound.Add( 
{
    name = "tdmc12_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/c12/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmcivictyper_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/civictyper/idle.wav"
} )

sound.Add( 
{
    name = "tdmcivictyper_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/civictyper/start.mp3"
} )

sound.Add( 
{
    name = "tdmcivictyper_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/civictyper/rev.wav"
} )

sound.Add( 
{
    name = "tdmcivictyper_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/civictyper/first.mp3"
} )

sound.Add( 
{
    name = "tdmcivictyper_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/civictyper/second.mp3"
} )

sound.Add( 
{
    name = "tdmcivictyper_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/civictyper/third.mp3"
} )

sound.Add( 
{
    name = "tdmcivictyper_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/civictyper/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmcivictyper_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/civictyper/second.mp3"
} )

sound.Add( 
{
    name = "tdmcivictyper_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/civictyper/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmceed_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/ceed/idle.wav"
} )

sound.Add( 
{
    name = "tdmceed_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/ceed/start.mp3"
} )

sound.Add( 
{
    name = "tdmceed_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/ceed/rev.wav"
} )

sound.Add( 
{
    name = "tdmceed_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/ceed/first.mp3"
} )

sound.Add( 
{
    name = "tdmceed_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/ceed/second.mp3"
} )

sound.Add( 
{
    name = "tdmceed_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/ceed/third.mp3"
} )

sound.Add( 
{
    name = "tdmceed_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/ceed/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmceed_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/ceed/second.mp3"
} )

sound.Add( 
{
    name = "tdmceed_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/ceed/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmcoupe40_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/coupe40/idle.wav"
} )

sound.Add( 
{
    name = "tdmcoupe40_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/coupe40/start.mp3"
} )

sound.Add( 
{
    name = "tdmcoupe40_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/coupe40/rev.wav"
} )

sound.Add( 
{
    name = "tdmcoupe40_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/coupe40/first.mp3"
} )

sound.Add( 
{
    name = "tdmcoupe40_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/coupe40/second.mp3"
} )

sound.Add( 
{
    name = "tdmcoupe40_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/coupe40/third.mp3"
} )

sound.Add( 
{
    name = "tdmcoupe40_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/coupe40/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmcoupe40_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/coupe40/second.mp3"
} )

sound.Add( 
{
    name = "tdmcoupe40_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/coupe40/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmgt05_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/gt05/idle.wav"
} )

sound.Add( 
{
    name = "tdmgt05_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/gt05/start.mp3"
} )

sound.Add( 
{
    name = "tdmgt05_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/gt05/rev.wav"
} )

sound.Add( 
{
    name = "tdmgt05_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/gt05/first.mp3"
} )

sound.Add( 
{
    name = "tdmgt05_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/gt05/second.mp3"
} )

sound.Add( 
{
    name = "tdmgt05_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/gt05/third.mp3"
} )

sound.Add( 
{
    name = "tdmgt05_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/gt05/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmgt05_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/gt05/second.mp3"
} )

sound.Add( 
{
    name = "tdmgt05_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/gt05/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmgallardo_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/gallardo/idle.wav"
} )

sound.Add( 
{
    name = "tdmgallardo_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/gallardo/start.wav"
} )

sound.Add( 
{
    name = "tdmgallardo_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/gallardo/rev.wav"
} )

sound.Add( 
{
    name = "tdmgallardo_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/gallardo/first.mp3"
} )

sound.Add( 
{
    name = "tdmgallardo_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/gallardo/second.mp3"
} )

sound.Add( 
{
    name = "tdmgallardo_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/gallardo/third.mp3"
} )

sound.Add( 
{
    name = "tdmgallardo_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/gallardo/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmgallardo_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/gallardo/second.mp3"
} )

sound.Add( 
{
    name = "tdmgallardo_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/gallardo/throttle_off.wav"
} )
sound.Add( 
{
    name = "tdmcrownvic_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/crownvic/idle.wav"
} )

sound.Add( 
{
    name = "tdmcrownvic_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/crownvic/start.mp3"
} )

sound.Add( 
{
    name = "tdmcrownvic_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/crownvic/rev.wav"
} )

sound.Add( 
{
    name = "tdmcrownvic_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/crownvic/first.mp3"
} )

sound.Add( 
{
    name = "tdmcrownvic_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/crownvic/second.mp3"
} )

sound.Add( 
{
    name = "tdmcrownvic_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/crownvic/third.mp3"
} )

sound.Add( 
{
    name = "tdmcrownvic_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/crownvic/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmcrownvic_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/crownvic/second.mp3"
} )

sound.Add( 
{
    name = "tdmcrownvic_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/crownvic/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmm3e92_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/m3e92/idle.wav"
} )

sound.Add( 
{
    name = "tdmm3e92_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/m3e92/start.mp3"
} )

sound.Add( 
{
    name = "tdmm3e92_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/m3e92/rev.wav"
} )

sound.Add( 
{
    name = "tdmm3e92_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/m3e92/first.mp3"
} )

sound.Add( 
{
    name = "tdmm3e92_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/m3e92/second.mp3"
} )

sound.Add( 
{
    name = "tdmm3e92_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/m3e92/third.mp3"
} )

sound.Add( 
{
    name = "tdmm3e92_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/m3e92/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmm3e92_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/m3e92/second.mp3"
} )

sound.Add( 
{
    name = "tdmm3e92_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/m3e92/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmh1_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/h1/idle.wav"
} )

sound.Add( 
{
    name = "tdmh1_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/h1/start.mp3"
} )

sound.Add( 
{
    name = "tdmh1_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/h1/rev.wav"
} )

sound.Add( 
{
    name = "tdmh1_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/h1/first.mp3"
} )

sound.Add( 
{
    name = "tdmh1_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/h1/second.mp3"
} )

sound.Add( 
{
    name = "tdmh1_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/h1/third.mp3"
} )

sound.Add( 
{
    name = "tdmh1_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/h1/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmh1_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/h1/second.mp3"
} )

sound.Add( 
{
    name = "tdmh1_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/h1/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmmk2_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/mk2/idle.wav"
} )

sound.Add( 
{
    name = "tdmmk2_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/mk2/start.mp3"
} )

sound.Add( 
{
    name = "tdmmk2_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/mk2/rev.wav"
} )

sound.Add( 
{
    name = "tdmmk2_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/mk2/first.mp3"
} )

sound.Add( 
{
    name = "tdmmk2_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/mk2/second.mp3"
} )

sound.Add( 
{
    name = "tdmmk2_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/mk2/third.mp3"
} )

sound.Add( 
{
    name = "tdmmk2_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/mk2/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmmk2_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/mk2/second.mp3"
} )

sound.Add( 
{
    name = "tdmmk2_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/mk2/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmprius_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = ""
} )

sound.Add( 
{
    name = "tdmprius_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = ""
} )

sound.Add( 
{
    name = "tdmprius_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/prius/rev.wav"
} )

sound.Add( 
{
    name = "tdmprius_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/prius/first.mp3"
} )

sound.Add( 
{
    name = "tdmprius_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/prius/second.mp3"
} )

sound.Add( 
{
    name = "tdmprius_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/prius/third.mp3"
} )

sound.Add( 
{
    name = "tdmprius_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/prius/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmprius_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/prius/second.mp3"
} )

sound.Add( 
{
    name = "tdmprius_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/prius/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmspark_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/spark/idle.wav"
} )

sound.Add( 
{
    name = "tdmspark_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/spark/start.mp3"
} )

sound.Add( 
{
    name = "tdmspark_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/spark/rev.wav"
} )

sound.Add( 
{
    name = "tdmspark_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/spark/first.mp3"
} )

sound.Add( 
{
    name = "tdmspark_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/spark/second.mp3"
} )

sound.Add( 
{
    name = "tdmspark_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/spark/third.mp3"
} )

sound.Add( 
{
    name = "tdmspark_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/spark/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmspark_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/spark/second.mp3"
} )

sound.Add( 
{
    name = "tdmspark_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/spark/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmtransit_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/transit/idle.wav"
} )

sound.Add( 
{
    name = "tdmtransit_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/transit/start.mp3"
} )

sound.Add( 
{
    name = "tdmtransit_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/transit/rev.wav"
} )

sound.Add( 
{
    name = "tdmtransit_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/transit/first.mp3"
} )

sound.Add( 
{
    name = "tdmtransit_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/transit/second.mp3"
} )

sound.Add( 
{
    name = "tdmtransit_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/transit/third.mp3"
} )

sound.Add( 
{
    name = "tdmtransit_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/transit/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmtransit_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/transit/second.mp3"
} )

sound.Add( 
{
    name = "tdmtransit_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/transit/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmtruck_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 100,
    sound = "vehicles/tdmcars/truck/idle.wav"
} )

sound.Add( 
{
    name = "tdmtruck_start",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 100,
    sound = "vehicles/tdmcars/truck/start.wav"
} )

sound.Add( 
{
    name = "tdmtruck_stop",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    sound = "common/null.wav"
} )

sound.Add( 
{
    name = "tdmtruck_rev",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/truck/rev.wav"
} )

sound.Add( 
{
    name = "tdmtruck_first",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
	pitchend = 95,
    sound = "vehicles/tdmcars/truck/first.wav"
} )

sound.Add( 
{
    name = "tdmtruck_second",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    pitchstart = 85,
    pitchend = 95,
    sound = "vehicles/tdmcars/truck/second.wav"
} )

sound.Add( 
{
    name = "tdmtruck_third",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    pitchstart = 85,
    pitchend = 95,
    sound = "vehicles/tdmcars/truck/third.wav"
} )

sound.Add( 
{
    name = "tdmtruck_fourth",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    pitchstart = 85,
    pitchend = 95,
    sound = "vehicles/tdmcars/truck/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmtruck_first_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    pitchstart = 85,
    pitchend = 95,
    sound = "vehicles/tdmcars/truck/first.wav"
} )

sound.Add( 
{
    name = "tdmtruck_second_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    pitchstart = 85,
    pitchend = 95,
    sound = "vehicles/tdmcars/truck/second.wav"
} )

sound.Add( 
{
    name = "tdmtruck_third_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    pitchstart = 85,
    pitchend = 95,
    sound = "vehicles/tdmcars/truck/third.wav"
} )

sound.Add( 
{
    name = "tdmtruck_fourth_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    pitchstart = 85,
    pitchend = 95,
    sound = "vehicles/tdmcars/truck/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmtruck_throttleoff",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    pitchstart = 85,
    pitchend = 95,
    sound = "vehicles/tdmcars/truck/throttle_off.wav"
} )

sound.Add( 
{
    name = "tdmtruck_skid_highfriction",
    channel = CHAN_BODY,
    volume = 1.0,
    soundlevel = 40,
	pitchstart = 75,
	pitchend = 75,
    sound = "vehicles/v8/skid_highfriction.wav"
} )


sound.Add( 
{
    name = "tdmtruck_skid_highfriction",
    channel = CHAN_BODY,
    volume = 1.0,
    soundlevel = 90,
	pitchstart = 40,
	pitchend = 75,
    sound = "vehicles/v8/skid_highfriction.wav"
} )

sound.Add( 
{
    name = "tdmcars_skid",
    channel = CHAN_BODY,
    volume = 1.5,
    soundlevel = 120,
	pitchstart = 80,
	pitchend = 110,
    sound = {"vehicles/tdmcars/tires/1.mp3",
	         "vehicles/tdmcars/tires/2.mp3",
			 "vehicles/tdmcars/tires/3.mp3"}
} )
sound.Add( 
{
    name = "tdmwrangler_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/wrangler/idle.wav"
} )
sound.Add( 
{
    name = "tdmwrangler_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/wrangler/rev.wav"
} )

sound.Add( 
{
    name = "tdmwrangler_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/wrangler/first.mp3"
} )

sound.Add( 
{
    name = "tdmwrangler_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/wrangler/second.mp3"
} )

sound.Add( 
{
    name = "tdmwrangler_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/wrangler/third.mp3"
} )

sound.Add( 
{
    name = "tdmwrangler_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/wrangler/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmwrangler_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/wrangler/second.mp3"
} )

sound.Add( 
{
    name = "tdmwrangler_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/wrangler/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdm507_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/507/idle.wav"
} )
sound.Add( 
{
    name = "tdm507_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/507/rev.wav"
} )

sound.Add( 
{
    name = "tdm507_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/507/first.mp3"
} )

sound.Add( 
{
    name = "tdm507_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/507/second.mp3"
} )

sound.Add( 
{
    name = "tdm507_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/507/third.mp3"
} )

sound.Add( 
{
    name = "tdm507_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/507/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdm507_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/507/second.mp3"
} )

sound.Add( 
{
    name = "tdm507_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/507/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmmx5_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/mx5/idle.wav"
} )
sound.Add( 
{
    name = "tdmmx5_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/mx5/rev.wav"
} )

sound.Add( 
{
    name = "tdmmx5_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/mx5/first.mp3"
} )

sound.Add( 
{
    name = "tdmmx5_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/mx5/second.mp3"
} )

sound.Add( 
{
    name = "tdmmx5_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/mx5/third.mp3"
} )

sound.Add( 
{
    name = "tdmmx5_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/mx5/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmmx5_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/mx5/second.mp3"
} )

sound.Add( 
{
    name = "tdmmx5_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/mx5/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmreventon_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/reventon/idle.wav"
} )
sound.Add( 
{
    name = "tdmreventon_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/reventon/rev.wav"
} )

sound.Add( 
{
    name = "tdmreventon_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/reventon/first.mp3"
} )

sound.Add( 
{
    name = "tdmreventon_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/reventon/second.mp3"
} )

sound.Add( 
{
    name = "tdmreventon_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/reventon/third.mp3"
} )

sound.Add( 
{
    name = "tdmreventon_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/reventon/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmreventon_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/reventon/second.mp3"
} )

sound.Add( 
{
    name = "tdmreventon_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/reventon/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdmgolf3_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/golf3/idle.wav"
} )
sound.Add( 
{
    name = "tdmgolf3_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/golf3/rev.wav"
} )

sound.Add( 
{
    name = "tdmgolf3_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/golf3/first.mp3"
} )

sound.Add( 
{
    name = "tdmgolf3_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/golf3/second.mp3"
} )

sound.Add( 
{
    name = "tdmgolf3_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/golf3/third.mp3"
} )

sound.Add( 
{
    name = "tdmgolf3_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/golf3/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdmgolf3_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/golf3/second.mp3"
} )

sound.Add( 
{
    name = "tdmgolf3_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/golf3/throttle_off.mp3"
} )
sound.Add( 
{
    name = "tdms5_idle",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = "vehicles/tdmcars/s5/idle.wav"
} )
sound.Add( 
{
    name = "tdms5_reverse",
    channel = CHAN_STATIC,
    volume = 0.9,
    soundlevel = 90,
    pitchstart = 98,
	pitchend = 105,
    sound = "vehicles/tdmcars/s5/rev.wav"
} )

sound.Add( 
{
    name = "tdms5_firstgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 95,
	pitchend = 104,
    sound = "vehicles/tdmcars/s5/first.mp3"
} )

sound.Add( 
{
    name = "tdms5_secondgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/s5/second.mp3"
} )

sound.Add( 
{
    name = "tdms5_thirdgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/s5/third.mp3"
} )

sound.Add( 
{
    name = "tdms5_fourthgear",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 90,
    pitchend = 105,
    sound = "vehicles/tdmcars/s5/fourth_cruise.wav"
} )

sound.Add( 
{
    name = "tdms5_noshift",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/s5/second.mp3"
} )

sound.Add( 
{
    name = "tdms5_slowdown",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    pitchstart = 85,
    pitchend = 110,
    sound = "vehicles/tdmcars/s5/throttle_off.mp3"
} )