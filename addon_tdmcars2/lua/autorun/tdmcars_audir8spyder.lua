local V = {
			Name = "Audi R8 GT Spyder", 
			Class = "prop_vehicle_jeep",
			Category = "TDM Cars",
			Author = "TheDanishMaster, Turn 10",
			Information = "A drivable Audi R8 GT Spyder by TheDanishMaster",
				Model = "models/tdmcars/audi_r8_spyder.mdl",
				KeyValues = {
							vehiclescript	=	"scripts/vehicles/TDMCars/audir8spyd.txt"
							}
			}
list.Set("Vehicles", "audir8spydtdm", V)