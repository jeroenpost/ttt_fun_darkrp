local V = {
			Name = "Ferrari 458 Spider", 
			Class = "prop_vehicle_jeep",
			Category = "TDM Cars",
			Author = "TheDanishMaster, Turn 10",
			Information = "A drivable Ferrari 458 Spider by TheDanishMaster",
			Model = "models/tdmcars/fer_458spid.mdl",
						KeyValues = {
							vehiclescript	=	"scripts/vehicles/TDMCars/fer458spid.txt"
							}
			}
list.Set("Vehicles", "458spidtdm", V)
