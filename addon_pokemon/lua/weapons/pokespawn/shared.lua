if (SERVER) then
    AddCSLuaFile ("shared.lua")
end

if (CLIENT) then
    SWEP.PrintName 	= "PokeSpawn"
    SWEP.Slot 		= 2
    SWEP.DrawAmmo 			= false
    SWEP.DrawCrosshair 		= false
    SWEP.ViewModelFOV		= 65
    SWEP.ViewModelFlip		= true
    SWEP.CSMuzzleFlashes	= false
    SWEP.BounceWeaponIcon   = false
    SWEP.Name = "Empty Pokeball"
end

SWEP.Entity = "pokeball_spawn"
SWEP.Category		= "GreenBlack"
SWEP.Spawnable = true
SWEP.AdminSpawnable = false
SWEP.DrawCrosshair = true

SWEP.ent = "pokeball_spawn"

SWEP.Spawnable 		= true
SWEP.AdminSpawnable = false

SWEP.ViewModel 		= "models/weapons/v_pokeball.mdl"
SWEP.WorldModel 	= "models/weapons/w_pokeball.mdl"

SWEP.Primary.ClipSize	 = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic	 = true
SWEP.Primary.Ammo 		 = "none"

SWEP.Secondary.ClipSize 	= 1
SWEP.Secondary.DefaultClip 	= 1
SWEP.Secondary.Automatic 	= true
SWEP.Secondary.Ammo 		= "none"

function SWEP:Initialize()
    self:SetHoldType("grenade")
end

function SWEP:Deploy()
    self.Weapon:SetNextPrimaryFire(CurTime()+1)
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    self.pokemon = nil
    return true
end

function SWEP:Holster()
    self.Proned = false
    self.Throwing = false
    return true
end

function SWEP:Reload()

end

function SWEP:Equip(newowner)

end

function SWEP:Think()
    if self.Proned and not self.Owner:KeyDown ( IN_ATTACK) and self.Owner:KeyReleased(IN_ATTACK) then
        self.Proned = false
        self.Throwing = true
        self.Weapon:SendWeaponAnim(ACT_VM_THROW)
        if self:IsValid() then
            timer.Simple(0.35, function()
                self:Throw()
            end)
        end
    end
end

function SWEP:Throw()
    if !self.Throwing then return end
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    local tr = self.Owner:GetEyeTrace()

    if (not SERVER) or not IsValid(self.pokemon) then return end

    local ent = ents.Create (self.ent)
    ent.type = self.type
    ent.thrownby = self.Owner
    ent.playerid = self.pokemon
    local v = self.Owner:GetShootPos()
    v = v + self.Owner:GetForward() * 1
    v = v + self.Owner:GetRight() * 3
    v = v + self.Owner:GetUp() * 1
    ent:SetPos( v )
    ent:SetAngles (Angle(math.random(1,100),math.random(1,100),math.random(1,100)))
    --	ent.GrenadeOwner = self.Owner
    --	ent:SetOwner(self.Owner)
    ent:Spawn()
    local phys = ent:GetPhysicsObject()
    local shot_length = tr.HitPos:Length()

    phys:ApplyForceCenter(self.Owner:GetAimVector() *2500 *1.2 + Vector(0,0,200) )
    phys:AddAngleVelocity(Vector(math.random(-500,500),math.random(-500,500),math.random(-500,500)))
    self.Weapon:SetNextPrimaryFire( CurTime() + 1.6 )


end

SWEP.NextShoot = 0
function SWEP:PrimaryAttack()
    if self.NextShoot > CurTime() then return end

    if not self.Owner:Team() == TEAM_POKETRAINER then
        self.Owner:PrintMessage( HUD_PRINTCENTER, "You are not a poketrainer" )
        return
    end
    if not istable(self.Owner.ownedpokemon) or #self.Owner.ownedpokemon < 1 then
        self.Owner:PrintMessage( HUD_PRINTCENTER, "You don't have any pokemon. Catch one first" )
        return
    end
    for k,v in pairs(self.Owner.ownedpokemon) do
        if not IsValid(v) or v.pokeowner != self.Owner then
            self.Owner.ownedpokemon[k] = nil
        end
    end
    if not IsValid(self.pokemon) then
        self.Owner:PrintMessage( HUD_PRINTCENTER, "Pick a pokemon with rightclick" )
        return
    end

    if self.pokemon.lastpokespawn and self.pokemon.lastpokespawn +30 > CurTime() then
        self.Owner:PrintMessage( HUD_PRINTCENTER, "You spawned this pokemon less then 30 seconds ago" )
        return
    end

    self.NextChange = CurTime() + 1
    if self.Throwing then return end
    if !self.Proned then
    self.Weapon:SendWeaponAnim(ACT_VM_PULLPIN)
    self.Proned = true
    end
    timer.Simple(3,function()
        if IsValid(self) then
        self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
        self.Proned = false
        self.Throwing = false
        end
    end)
end

SWEP.NextChange = 0
function SWEP:SecondaryAttack()
    if self.NextChange > CurTime() then return end
    self.NextChange = CurTime() + 0.5

    if CLIENT then return end

    for k,v in pairs(self.Owner.ownedpokemon) do
        if not IsValid(v) or v.pokeowner != self.Owner then
            self.Owner.ownedpokemon[k] = nil
        end
    end

    if not istable(self.Owner.ownedpokemon) or #self.Owner.ownedpokemon < 1  then
        PrintTable( self.Owner.ownedpokemon)

        self.Owner:PrintMessage( HUD_PRINTCENTER, "You don't have any pokemon. Catch one first" )
        return
    end

    local p = table.Random( self.Owner.ownedpokemon  )
    if #self.Owner.ownedpokemon > 1 and p == self.pokemon then
        while p == self.pokemon do
        p = table.Random( self.Owner.ownedpokemon  )
        end
    end

    if IsValid( p) then
       self.pokemon = p
       self:SetNWString("pokemon", p:Nick())
       self.Owner:PrintMessage( HUD_PRINTCENTER, "Pokemon: "..p:Nick() )
    else
        self.Owner:PrintMessage( HUD_PRINTCENTER, "Pokemon left." )
     end
end


if CLIENT then
    function SWEP:DrawHUD( )
        local shotttext = "Pokemon: "..self:GetNWString("pokemon").."\nRightClick to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
    end
end;