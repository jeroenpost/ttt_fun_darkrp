AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

util.AddNetworkString("SendSelf")

function ENT:PhysicsCollide(data, phys)
	self:EmitSound(self.BounceSnd)
	
	local impulse = -data.Speed * data.HitNormal * .4 + (data.OurOldVelocity * -.6)
	phys:ApplyForceCenter(impulse)
    self.bounces = self.bounces + 1
    if self.bounces > 3 or  self:GetNWBool("HasHitNpc") then
        local pball = "pokeball"
        local ent1 = ents.Create(pball)
        --ent1.spawn = ent:GetClass()
        ent1.model = self:GetModel()
        ent1:SetPos(self:GetPos())
        ent1:Spawn()
        self:Remove()
    end
end

function ENT:Initialize()
	self:SetModel("models/weapons/w_pokeball_thrown.mdl")
	self:PhysicsInit(SOLID_CUSTOM)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:DrawShadow(false)
	self.timer = CurTime() + 20
    self.bounces = 0
end

function ENT:Think()
	if self.Entity:GetNWBool("HasHitNpc", true) then
		self:DoEffects(self.ent)
	end
end

function ENT:DoEffects(ent)
	if type(self.ent) ~= "string" then
		--elf.Alpha = self.Alpha - 800*FrameTime()
		--ent:SetRenderMode(RENDERMODE_TRANSALPHA)
		--ent:SetColor(Color(255, 0, 0, self.Alpha))
		--ent:SetModelScale(ent:GetModelScale()*0.5, 1)
	end
end
			
function ENT:Settype(type)
	self.type = type
end
	
function ENT:SetEnt(ent)
	if IsValid(ent) and ent:IsNPC() then
		self.ent = ent
	end
end
	
function ENT:StartTouch(ent)
	if self:GetNWBool("HasHitNpc") then
		return false 
    end

	if IsValid(ent) and ent:IsPlayer() and ent:Team() == TEAM_POKEMON and not ent.in_the_savezone   then
        if IsValid( ent.pokeowner) and ent.pokeowner:Team() == TEAM_POKEMONTRAINER then return end

        self:SetNWBool("HasHitNpc", true)
		self.ent = ent
		self:DoEffects(ent)
		self:DoColorstuff(ent)
	end
end

function ENT:DoColorstuff(ent) --Hacky way of doing this, but it looks cool so what the hell
	if IsValid(ent) and ent:IsPlayer() and IsValid(self.thrownby) then
		ent:SetNWInt("pokeowner",self.thrownby:UniqueID())
        ent.pokeisowned = true
        ent.pokeowner = self.thrownby
        ent:PrintMessage( HUD_PRINTCENTER, "You are now owned by "..self.thrownby:Nick() )
        self.thrownby:PrintMessage( HUD_PRINTCENTER, "You caught "..ent:Nick().."!!" )
        if not self.thrownby.ownedpokemon then
            self.thrownby.ownedpokemon = {}
        end
        table.insert(self.thrownby.ownedpokemon, ent)
        for k,v in pairs(self.thrownby.ownedpokemon) do
            if not IsValid(v) then
                self.thrownby.ownedpokemon[k] = nil
                print("Pokemon removed")
            end
        end


        self:Remove()
	end
end