include('shared.lua')

function ENT:Draw()
	self:DrawModel()
end

function ENT:IsTranslucent()
	return true
end

local pokeball, entity

net.Receive("SendSelf", function()
	pokeball = net.ReadEntity()
	entity = net.ReadEntity()
end)

local Traceline = Material("models/debug/debugwhite")

hook.Add("PostDrawOpaqueRenderables", "DatBeam", function()
	if IsValid(pokeball) and IsValid(entity) then
		render.SetMaterial(Traceline)
		render.DrawBeam(pokeball:GetPos(), entity:GetPos(), 6, 0, 1, Color(255, 0, 0))
	end
end)