AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

util.AddNetworkString("SendSelf")

function ENT:PhysicsCollide(data, phys)
	self:EmitSound(self.ReleaseSnd)
	local p = self.playerid
    if IsValid( p) then
        p:SetPos( self:GetPos() + Vector(0,0,75))
        p:SetModelScale(p:GetModelScale()*0.01, 0.01)
        timer.Simple(0.02,function()
            if not IsValid(p) then return end
            p:SetModelScale(1, 2)
            p:SetHealth(100)
            p:DropToFloor()
            p.lastpokespawn = CurTime()
        end)
        p:PrintMessage( HUD_PRINTCENTER, "You were spawned by your owner" )
    end
    self:Remove()
end

function ENT:Initialize()
	self:SetModel("models/weapons/w_pokeball_thrown.mdl")
	self:PhysicsInit(SOLID_CUSTOM)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:DrawShadow(false)
	self.timer = CurTime() + 20
    self.bounces = 0
end

