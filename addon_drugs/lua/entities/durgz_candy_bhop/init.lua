AddCSLuaFile("shared.lua")
include("shared.lua")

ENT.MODEL = "models/noesis/donut.mdl"
ENT.MATERIAL = "camos/camo57"

ENT.MASS = 2; --the model is too heavy so we have to override it with THIS

ENT.LASTINGEFFECT = 45; --how long the high lasts in seconds



function ENT:High(activator,caller)
    activator.bunnyhop = true
	--cut health in half and double the speed
    activator:SetFunVar("bunnyhop",activator:GetFunVar("bunnyhop")+60)

    timer.Create("candy_bhop"..activator:SteamID(),1,0,function()
        if not IsValid(activator) then return end

        activator:SetFunVar("bunnyhop",activator:GetFunVar("bunnyhop")-1)
        if( activator:GetFunVar("bunnyhop") < 1) then
            activator.bunnyhop = false
            activator:GetFunVar("bunnyhop", 0)
            timer.Destroy("candy_bhop"..activator:SteamID())
        end
    end)


end

function ENT:AfterHigh(activator, caller)

end

