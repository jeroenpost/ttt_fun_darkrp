ENT.Type = "anim"
ENT.Base = "durgz_base"
ENT.PrintName = "LSD"
ENT.Nicknames = {"LSD", "acid", "a tab", "a few tabs", "an entire vial of LSD", "a glass of LSD"}
ENT.OverdosePhrase = {"saw God whilst on", "peed in their mouth after taking", "drank"}
ENT.Author = "Jenna Huxley"
ENT.Spawnable = true
ENT.AdminSpawnable = true 
ENT.Information	 = " lol high scientists" 
ENT.Category = "Drugs"
ENT.TRANSITION_TIME = 6

--function for high visuals

if(CLIENT)then

	killicon.Add("durgz_lsd","killicons/durgz_lsd_killicon",Color( 255, 80, 0, 255 ))

	local TRANSITION_TIME = ENT.TRANSITION_TIME; --transition effect from sober to high, high to sober, in seconds how long it will take etc.
	local HIGH_INTENSITY = 0.77; --1 is max, 0 is nothing at all


	local function DoLSD()
		if(!DURGZ_LOST_VIRGINITY)then return; end
		--self:SetNWInt( "SprintSpeed"
		local pl = LocalPlayer();
        local curtime = CurTime()
        local mul = 0.5


            start = pl:GetFunVar("durgz_lsd_high_start")
            theend = pl:GetFunVar("durgz_lsd_high_end")
            pl.durgz_lsd_high_start = start
            pl.durgz_lsd_high_end = theend

        if start == 0 or theend < curtime then return end
		

		if( pl:GetFunVar("durgz_lsd_high_start") && pl:GetFunVar("durgz_lsd_high_end") > CurTime() )then

        local tab = {}
        tab[ "$pp_colour_addr" ] = 0
        tab[ "$pp_colour_addg" ] = 0
        tab[ "$pp_colour_addb" ] = 0
                //tab[ "$pp_colour_brightness" ] = 0
                //tab[ "$pp_colour_contrast" ] = 1
        tab[ "$pp_colour_mulr" ] = 0
        tab[ "$pp_colour_mulg" ] = 0
        tab[ "$pp_colour_mulb" ] = 0
		
			if( pl:GetFunVar("durgz_lsd_high_start") + TRANSITION_TIME > CurTime() )then
			
				local s = pl:GetFunVar("durgz_lsd_high_start");
				local e = s + TRANSITION_TIME;
				local c = CurTime();
				local pf = (c-s) / (e-s);
				
				tab[ "$pp_colour_colour" ] =   1 + pf*3
				tab[ "$pp_colour_brightness" ] = -pf*0.19
				tab[ "$pp_colour_contrast" ] = 1 + pf*5.31
				--DrawBloom(0.65, (pf^2)*0.1, 9, 9, 4, 7.7,255,255,255)
				--DrawColorModify( tab )

                DrawMotionBlur( 0.2, 1 * mul, 0 );
                DrawSharpen( ( 1.39 + 0.5 * math.sin( CurTime() * 5 ) ) * mul, 1 + 0.62 * mul );
                DrawMaterialOverlay( "effects/strider_pinch_dudv", 0.1 * mul, mul );
				
			elseif( pl:GetFunVar("durgz_lsd_high_end") - TRANSITION_TIME < CurTime() )then
			
				local e = pl:GetFunVar("durgz_lsd_high_end");
				local s = e - TRANSITION_TIME;
				local c = CurTime();
				local pf = 1 - (c-s) / (e-s);
				
				tab[ "$pp_colour_colour" ] =   1 + pf*3
				tab[ "$pp_colour_brightness" ] = -pf*0.19
				tab[ "$pp_colour_contrast" ] = 1 + pf*5.31
                DrawMotionBlur( 0.2, 1 * mul, 0 );
                DrawSharpen( ( 1.39 + 0.5 * math.sin( CurTime() * 5 ) ) * mul, 1 + 0.62 * mul );
                DrawMaterialOverlay( "effects/strider_pinch_dudv", 0.1 * mul, mul );
				
			else
				
				
				tab[ "$pp_colour_colour" ] =   1 + 3
				tab[ "$pp_colour_brightness" ] = -0.19
				tab[ "$pp_colour_contrast" ] = 1 + 5.31
                DrawMotionBlur( 0.2, 1 * mul, 0 );
                DrawSharpen( ( 1.39 + 0.5 * math.sin( CurTime() * 5 ) ) * mul, 1 + 0.62 * mul );
                DrawMaterialOverlay( "effects/strider_pinch_dudv", 0.1 * mul, mul );
				
			end
			
			
		end
	end
	
	

	
	hook.Add("RenderScreenspaceEffects", "durgz_lsd_high", DoLSD)

    local lp = LocalPlayer()
    if not lp.lsdhookcreated then
        local bhstop = 0xFFFF - IN_JUMP
        local band = bit.band

        hook.Add("CreateMove",lp:EntIndex().."bhop",function( uc )
                if not IsValid(lp) then  lp = LocalPlayer() return end

             	if( lp:GetFunVar("durgz_lsd_high_start") && (lp:GetFunVar("durgz_lsd_high_end") + 500) > CurTime() ) and  lp:GetMoveType() == MOVETYPE_WALK then
                if not lp:InVehicle() and ( band(uc:GetButtons(), IN_JUMP) ) > 0 then
                    if lp:IsOnGround() then
                        uc:SetButtons( uc:GetButtons() or IN_JUMP )
                    else
                        uc:SetButtons( band(uc:GetButtons(), bhstop) )
                    end
                end
            end
        end)
        lp.lsdhookcreated = true
    end
end
