AddCSLuaFile("shared.lua")
include("shared.lua")

ENT.MODEL = "models/props_phx/misc/egg.mdl"
ENT.MATERIAL = "camos/camo57"

ENT.MASS = 2; --the model is too heavy so we have to override it with THIS

ENT.LASTINGEFFECT = 45; --how long the high lasts in seconds

function ENT:Initialize()

    self:SetModel( self.MODEL )

    self:SetMaterial( self.MATERIAL )

    self:PhysicsInit( SOLID_VPHYSICS )

    local phys = self:GetPhysicsObject()
    if phys:IsValid() then
        phys:Wake()
    end

    self.LACED = {};

    if( self.MASS )then
        self.Entity:GetPhysicsObject():SetMass( self.MASS );
    end

end

function ENT:High(activator,caller)
    activator.ishigh = true
	--cut health in half and double the speed


	self.MakeHigh = false;

    if not self:Realistic() then
        if activator:GetFunVar("durgz_cocaine1_high_end") < CurTime() then
            self.MakeHigh = true;
        end
    end

    timer.Create("cocaine1_high"..activator:SteamID(),5,50,function()
        if not IsValid(activator) then return end
        if  activator.durgz_cocaine_fast and activator:GetFunVar("durgz_cocaine1_high_end") < CurTime() then
            gb_updateplayerspeed(activator)
            activator.durgz_cocaine_fast = false
            activator.ishigh = false
        end
    end)


end

function ENT:AfterHigh(activator, caller)
	
	--kill them if they're weak
	if( activator:Health() <=1 )then
		activator.DURGZ_MOD_DEATH = "durgz_candy1";
		activator.DURGZ_MOD_OVERRIDE = activator:Nick().." died of a heart attack (too much candy).";

	return
	end
	
	if( self.MakeHigh )then
        activator.durgz_cocaine_fast = true
		activator:SetRunSpeed(750)
		activator:SetWalkSpeed(750)
	end
end

function ENT:Use(activator,caller)
    if caller.usedcandy and caller.usedcandy > CurTime() then return end
    caller.usedcandy = CurTime() + 45
    self.BaseClass.Use(self,activator,caller)
end
