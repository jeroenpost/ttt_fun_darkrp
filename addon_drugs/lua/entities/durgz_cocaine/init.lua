AddCSLuaFile("shared.lua")
include("shared.lua")

ENT.MODEL = "models/cocn.mdl"

ENT.MASS = 2; --the model is too heavy so we have to override it with THIS

ENT.LASTINGEFFECT = 45; --how long the high lasts in seconds


function ENT:High(activator,caller)
    activator.ishigh = true
	--cut health in half and double the speed
    if not self:Realistic() then
        activator:SetHealth(activator:Health()/2)
    end

	self.MakeHigh = false;

    if not self:Realistic() then
        if activator:GetFunVar("durgz_cocaine_high_end") < CurTime() then
            self.MakeHigh = true;
        end
    end

    timer.Create("cocaine_high"..activator:SteamID(),5,50,function()
        if not IsValid(activator) then return end
        if  activator.durgz_cocaine_fast and activator:GetFunVar("durgz_cocaine_high_end") < CurTime() then
            gb_updateplayerspeed(activator)
            activator.durgz_cocaine_fast = false
            activator.ishigh = false
        end
    end)


end

function ENT:AfterHigh(activator, caller)
	
	--kill them if they're weak
	if( activator:Health() <=1 )then
		activator.DURGZ_MOD_DEATH = "durgz_cocaine";
		activator.DURGZ_MOD_OVERRIDE = activator:Nick().." died of a heart attack (too much cocaine).";
		activator:Kill()
	return
	end
	
	if( self.MakeHigh )then
        activator.durgz_cocaine_fast = true
		activator:SetRunSpeed(750)
		activator:SetWalkSpeed(750)
	end
end

