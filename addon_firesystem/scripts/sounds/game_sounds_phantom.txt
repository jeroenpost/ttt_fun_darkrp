// phantom


"phantom_engine_idle"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"

	//"wave"		"vehicles/atv_idle_loop1.wav"
	"wave"		"vehicles/phantom/idle.wav"
}

"phantom_engine_null"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"

	"wave"		"common/null.wav"
}

"phantom_engine_start"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	//"wave"		"vehicles/atv_start_loop1.wav"
	"wave"		"vehicles/phantom/start.wav"
}

"phantom_engine_stop"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"

	//"wave"		"common/null.wav"
	"wave"		"vehicles/phantom/stop.wav"
}

"phantom_rev"
{
	"channel"		"CHAN_STATIC"
	"volume"		"0.9"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"98,105"
	"wave"		"vehicles/phantom/rev_short_loop.wav"

//	"rndwave"
//	{
//		"wave"		"vehicles/phantom/rev_short_loop.wav"
//		"wave"		"vehicles/phantom/rev_short_loop.wav"
//	}
}

"phantom_reverse"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"			"100"
	"wave"			"vehicles/phantom/firstgear_reverse.wav"
}


"phantom_firstgear"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"100"
	//"wave"		"vehicles/v8/v8_firstgear_rev_loop1.wav"
	"wave"		"vehicles/phantom/first.wav"
}

"phantom_secondgear"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"90,105"
	//"wave"		"vehicles/atv_secondgear_to_fullthrottle_loop1.wav"
	//"wave"		"vehicles/v8/v8_secondgear_loop1.wav"
	"wave"		"vehicles/phantom/second.wav"
}

"phantom_thirdgear"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"90,105"
	//"wave"		"vehicles/atv_thirdgear_to_fullthrottle_loop1.wav"
	//"wave"		"vehicles/v8/v8_secondgear_loop1.wav"
	"wave"		"vehicles/phantom/third.wav"
}

"phantom_fourthgear"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"105,105"
	"wave"		"vehicles/phantom/fourth_cruise.wav"
}

"phantom_firstgear_noshift"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"105,105"
	// NOTE: This needs to be a looping sound
	"wave"		"vehicles/phantom/first.wav"
}
"phantom_secondgear_noshift"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"105,105"
	// NOTE: This needs to be a looping sound
	"wave"		"vehicles/phantom/second.wav"
}
"phantom_thirdgear_noshift"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"105,105"
	// NOTE: This needs to be a looping sound
	"wave"		"vehicles/phantom/third.wav"
}

"phantom_fourthgear_noshift"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"105,105"
	// NOTE: This needs to be a looping sound
	"wave"		"vehicles/phantom/fourth_cruise.wav"
}

"phantom_downshift_to_2nd"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"90,105"
	//"wave"		"vehicles/atv_downshift_to_2nd_loop1.wav"
	"wave"		"vehicles/phantom/firstgear_rev_loop.wav"
	//"wave"		"vehicles/phantom/idle.wav"
	//"wave"		"common/null.wav"
}

"phantom_downshift_to_1st"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"90,105"
	//"wave"		"vehicles/atv_downshift_to_1st_loop1.wav"
	"wave"		"vehicles/phantom/throttle_off_fast.wav"
	//"wave"		"vehicles/phantom/idle.wav"
	//"wave"		"common/null.wav"
}

"phantom_throttleoff_slowspeed"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"90,105"
	//"wave"		"vehicles/phantom/throttleofffast.wav"
	"wave"		"vehicles/phantom/Throttle_off_slow.wav"
}

"phantom_throttleoff_fastspeed"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"90,105"
	//"wave"		"vehicles/atv_throttleoff_loop1.wav"
	"wave"		"vehicles/phantom/throttle_off_fast.wav"
	//"wave"		"common/null.wav"
}

"phantom_skid_lowfriction"
{
	"channel"		"CHAN_BODY"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"90,110"
	//"wave"		"vehicles/atv_skid_lowfriction.wav"
	"wave"		"vehicles/v8/skid_lowfriction.wav"
}

"phantom_skid_normalfriction"
{
	"channel"		"CHAN_BODY"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"90,110"
	//"wave"		"vehicles/atv_skid_normalfriction.wav"
	"wave"		"vehicles/v8/skid_normalfriction.wav"
}

"phantom_skid_highfriction"
{
	"channel"		"CHAN_BODY"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"90,110"
	//"wave"		"vehicles/atv_skid_highfriction.wav"
	"wave"		"vehicles/v8/skid_highfriction.wav"
}

"phantom_impact_heavy"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"95,110"
	"rndwave"
	{
		"wave"	"vehicles/v8/vehicle_impact_heavy1.wav"
		"wave"	"vehicles/v8/vehicle_impact_heavy2.wav"
		"wave"	"vehicles/v8/vehicle_impact_heavy3.wav"
		"wave"	"vehicles/v8/vehicle_impact_heavy4.wav"
	}
}

"phantom_impact_medium"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"95,110"
	"rndwave"
	{
		"wave"	"vehicles/v8/vehicle_impact_medium1.wav"
		"wave"	"vehicles/v8/vehicle_impact_medium2.wav"
		"wave"	"vehicles/v8/vehicle_impact_medium3.wav"
		"wave"	"vehicles/v8/vehicle_impact_medium4.wav"
	}
}

"phantom_rollover"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"95,110"
	"rndwave"
	{
		"wave"	"vehicles/v8/vehicle_rollover1.wav"
		"wave"	"vehicles/v8/vehicle_rollover2.wav"
	}
}


"phantom_turbo_on"
{
	"channel"		"CHAN_ITEM"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"90,110"
	//"wave"		"vehicles/vehicle_turbo_loop3.wav"
	"wave"		"vehicles/phantom/turbo.wav"
}

"phantom_turbo_off"
{
	"channel"		"CHAN_ITEM"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"		"90,110"
	//"wave"		"vehicles/vehicle_turbo_off1.wav"
	"wave"		"vehicles/v8/v8_turbo_off.wav"
}

"phantom_start_in_water"
{
	"channel"		"CHAN_VOICE"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"			"100"
	"wave"			"vehicles/jetski/jetski_no_gas_start.wav"
}

"phantom_stall_in_water"
{
	"channel"		"CHAN_VOICE"
	"volume"		"1.0"
	"soundlevel"	"SNDLVL_80dB"
	"pitch"			"100"
	"wave"			"vehicles/jetski/jetski_off.wav"
}