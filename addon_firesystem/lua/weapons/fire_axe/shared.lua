if SERVER then
	AddCSLuaFile("shared.lua")
end

if CLIENT then
	SWEP.PrintName = "Fire Axe"
	SWEP.Slot = 2
	SWEP.SlotPos = 1
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
end

SWEP.Author = "Crap-Head" 
SWEP.Instructions = "Use the axe to open doors in case of fire."

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false

SWEP.Spawnable = false
SWEP.AdminSpawnable = true
SWEP.UseHands = true
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""

SWEP.ViewModel = "models/weapons/v_fireaxe.mdl"
SWEP.WorldModel = "models/weapons/w_fireaxe.mdl"

function SWEP:Initialize()
	self:SetHoldType("melee")
end

function SWEP:CanPrimaryAttack() return true end

function SWEP:PrimaryAttack()	
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	self.Weapon:EmitSound( "npc/vort/claw_swing" .. math.random( 1, 2 ) .. ".wav" )
	self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
	
	self.Weapon:SetNextPrimaryFire( CurTime() + 0.5 )
	
	local tr = self.Owner:GetEyeTrace()
	
	if self.Owner:EyePos():Distance( tr.HitPos ) > 75 then 
		return 
	end
	
	if tr.MatType == MAT_GLASS then
		self.Weapon:EmitSound("physics/glass/glass_cup_break" .. math.random( 1, 2 ) .. ".wav")
		return
	elseif tr.HitWorld then
		self.Weapon:EmitSound("physics/metal/metal_canister_impact_hard" .. math.random( 1, 3 ) .. ".wav")
		return
	end
	
	if tr.Entity:IsPlayer() or tr.Entity:IsNPC() then
		self.Weapon:EmitSound("physics/flesh/flesh_impact_hard" .. math.random( 1, 6 ) .. ".wav")
		
		if SERVER then
			--tr.Entity:TakeDamage(FIREAXE_PlayerDamage,self.Owner,self)
		end
	elseif tr.Entity:GetClass() == "func_door" or tr.Entity:GetClass() == "func_door_rotating" or tr.Entity:GetClass() == "prop_door_rotating" then
		self.Weapon:EmitSound("physics/wood/wood_box_impact_hard" .. math.random( 1, 3 ) .. ".wav")
		
		local NearFire = false
		for k, v in pairs(ents.FindInSphere(tr.Entity:GetPos(), FIREAXE_FireRange)) do
			if v:GetClass() == "fire" then
				NearFire = true
			end
		end
		
		if NearFire then
			tr.Entity.DoorHealth = tr.Entity.DoorHealth or math.random( 4, 6 )
			tr.Entity.DoorHealth = tr.Entity.DoorHealth - 1
			
			if tr.Entity.DoorHealth <= 0 then
				tr.Entity.DoorHealth = nil
				
				tr.Entity:Fire("unlock", "", 0)
				tr.Entity:Fire("open", "", 0.5)
			end
		else
			if SERVER then
				DarkRP.notify(self.Owner, 1, 5,  "There is no fire nearby.")
			end
		end
	end
end

function SWEP:SecondaryAttack()
end