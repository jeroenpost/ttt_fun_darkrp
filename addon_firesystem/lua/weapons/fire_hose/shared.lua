if SERVER then
	AddCSLuaFile("shared.lua")
end

if CLIENT then
	SWEP.PrintName 			= "Fire Hose"
	SWEP.Slot				= 2
	SWEP.SlotPos 			= 1
	SWEP.DrawAmmo 			= false
	SWEP.DrawCrosshair 		= false
end

SWEP.Author 				= "Crap-Head"
SWEP.Instructions 			= "Left Click: Extinguish Fires\nCan only be used near a firetruck!"
SWEP.Category				= "DarkRP Fire System"

SWEP.UseHands				= true
SWEP.ViewModelFOV 			= 62
SWEP.ViewModelFlip 			= false

SWEP.Spawnable 				= true
SWEP.AdminSpawnable 		= true

SWEP.Primary.ClipSize 		= -1
SWEP.Primary.DefaultClip 	= 0
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo 			= ""

SWEP.Secondary.ClipSize 	= -1
SWEP.Secondary.DefaultClip 	= 0
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo 		= ""

SWEP.ViewModel 				= "models/weapons/v_superphyscannon.mdl"
SWEP.WorldModel = "models/weapons/w_fire_extinguisher.mdl"


function SWEP:Initialize()
end

function SWEP:PrimaryAttack()	
	local TruckClose = false
	
	for k, v in pairs( ents.FindByClass("prop_vehicle_jeep") ) do
		if v:GetPos():Distance( self.Owner:GetPos() ) < 1000 and v:GetModel() == FIRETRUCK_VehicleModel then
			TruckClose = true
			break
		end
	end
	
	if not TruckClose then		
		if SERVER then
			self.Owner:ChatPrint("You must be near a firetruck to use this!")
			return
		end
	end
	
	self.Weapon:EmitSound( "ambient/wind/wind_hit2.wav" )
	self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
	self.Weapon:SetNextPrimaryFire(CurTime() + .12)
	
	local ExtinguishEffect = EffectData()
	ExtinguishEffect:SetAttachment( 1 )
	ExtinguishEffect:SetEntity( self.Owner )
	ExtinguishEffect:SetOrigin( self.Owner:GetShootPos() )
	ExtinguishEffect:SetNormal( self.Owner:GetAimVector() )
	util.Effect( "hose_extinguish", ExtinguishEffect )
	
	if SERVER then
		local trace = {}
		trace.start = self.Owner:GetShootPos()
		trace.endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 150
		trace.filter = self.Owner

		local tr = util.TraceLine( trace )
		
		for k, v in pairs( ents.FindInSphere( tr.HitPos, 50 ) ) do
			if v:GetClass() == "fire" then
				v:ExtinguishAttack( self.Owner, true )
			end
			
			if v:IsOnFire() then 
				v:Extinguish()
				v:SetColor( Color(255,255,255,255) )
			end
		end
	end
end

function SWEP:SecondaryAttack()
end
