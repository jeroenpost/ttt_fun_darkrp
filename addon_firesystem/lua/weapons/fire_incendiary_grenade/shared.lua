if (SERVER) then

	AddCSLuaFile( "shared.lua" )
	SWEP.Weight				= 5
	SWEP.AutoSwitchTo		= false
	SWEP.AutoSwitchFrom		= false

end

if ( CLIENT ) then
	SWEP.PrintName			= "Incendiary Grenade"	
	SWEP.Author				= "Crap-Head"
	SWEP.DrawAmmo 			= false
	SWEP.DrawCrosshair 		= false
	
	SWEP.Slot				= 2
	SWEP.SlotPos			= 1
	SWEP.IconLetter			= "G"
end

SWEP.HoldType				= "melee"
SWEP.AnimPrefix  			= "stunstick"
 
SWEP.Instructions 			= "Left click to throw the grenade."
SWEP.Category				= "DarkRP Fire System"

SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true

SWEP.UseHands				= true
SWEP.ViewModelFlip			= true
SWEP.ViewModelFOV			= 54
SWEP.ViewModel 				= "models/weapons/v_eq_flashbang.mdl"
SWEP.WorldModel 			= "models/weapons/w_eq_flashbang.mdl" 

SWEP.Weight					= 0.1
SWEP.AutoSwitchTo			= false
SWEP.AutoSwitchFrom			= false

SWEP.Primary.ClipSize		= 1
SWEP.Primary.DefaultClip	= 1
SWEP.Primary.Automatic		= false
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.12
SWEP.Primary.Ammo			= "none"
SWEP.Primary.Delay			= 1

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Damage		= 0
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

function SWEP:Think()
end

function SWEP:Initialize() 
	self:SetHoldType( "melee" )
end 

function SWEP:Deploy()
	return true
end

function SWEP:SecondaryAttack()
end

function SWEP:PrimaryAttack()
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	
	local trace = {}
	trace.start = self.Owner:GetShootPos()
	trace.endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 10 ^ 14
	trace.filter = self.Owner
	local tr = util.TraceLine(trace)

	if SERVER then
		local grenade = ents.Create("ent_incendiary_grenade")
		grenade:SetPos(self.Owner:GetShootPos() + self.Owner:GetAimVector() * 10 + self.Owner:EyeAngles():Up() * 10 * -1)
		grenade:SetAngles(self.Owner:GetAimVector():Angle())
		grenade:Spawn()
		grenade:Activate()
		grenade:GetPhysicsObject():ApplyForceCenter(self.Owner:GetAimVector():Angle():Forward() * 900)
	end

	self.Weapon:SetNextPrimaryFire(CurTime() + 1)
    self.Weapon:SetNextSecondaryFire(CurTime() + 1)
	if SERVER then
		self.Owner:StripWeapon("fire_incendiary_grenade")
	end
end