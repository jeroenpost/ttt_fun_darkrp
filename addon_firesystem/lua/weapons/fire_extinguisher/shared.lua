if SERVER then
	AddCSLuaFile("shared.lua")
end

if CLIENT then
	SWEP.PrintName = "Fire Extinguisher"
	SWEP.Slot = 2
	SWEP.SlotPos = 1
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
end

SWEP.Author = "Crap-Head"
SWEP.Instructions = "Left Click: Extinguish Fires"

SWEP.ViewModelFOV = 55
SWEP.ViewModelFlip = false

SWEP.Spawnable = false
SWEP.AdminSpawnable = true

SWEP.UseHands = true
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Ammo = ""

SWEP.ViewModel = "models/weapons/c_fire_extinguisher.mdl"
SWEP.WorldModel = "models/weapons/w_fire_extinguisher.mdl"

function SWEP:Initialize()
	self:SetHoldType("slam")
end

function SWEP:PrimaryAttack()
	if SERVER then
		if not table.HasValue( FIRE_AllowedTeams, team.GetName(self.Owner:Team()) ) then
			if not self.HasBeenActivated then
				timer.Simple(FIREEXTCITZ_RemoveTimer, function()
					if IsValid(self.Owner) then
						if self.Owner:HasWeapon("fire_extinguisher") then
							self.Owner:StripWeapon("fire_extinguisher")
							DarkRP.notify(self.Owner, 1, 5,  "Your citizen fire extinguisher has expired.")
						end
					end
				end)
				DarkRP.notify(self.Owner, 1, 5,  "Your citizen fire extinguisher has been activated. You can use it for "..FIREEXTCITZ_RemoveTimer.." seconds.")
			end
			self.HasBeenActivated = true
		end
	end
	self.Weapon:EmitSound( Sound("ambient/wind/wind_hit2.wav") )
	
	self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
	self.Weapon:SetNextPrimaryFire( CurTime() + .1 )
	
	local ExtinguishEffect = EffectData()
	ExtinguishEffect:SetAttachment( 1 )
	ExtinguishEffect:SetEntity( self.Owner )
	ExtinguishEffect:SetOrigin( self.Owner:GetShootPos() )
	ExtinguishEffect:SetNormal( self.Owner:GetAimVector() )
	util.Effect( "extinguish", ExtinguishEffect )
	
	if SERVER then
		local trace = {}
		trace.start = self.Owner:GetShootPos()
		trace.endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 150
		trace.filter = self.Owner

		local tr = util.TraceLine( trace )
		
		for k, v in pairs( ents.FindInSphere( tr.HitPos, 50 ) ) do
			if v:GetClass() == "fire" then
				v:ExtinguishAttack( self.Owner )
			end
			
			if v:IsOnFire() then 
				v:Extinguish()
				v:SetColor( Color(255,255,255,255) )
			end
		end
	end
end

function SWEP:SecondaryAttack()
end