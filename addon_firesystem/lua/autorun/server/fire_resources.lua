--resource.AddWorkshop( "104607228" ) -- Extinguisher by Robotboy655. Used for the model and materials only!

resource.AddFile("materials/effects/extinguisher.vmt")
resource.AddFile("materials/effects/flame.vtf")
resource.AddFile("materials/effects/flame.vmt")

resource.AddFile("materials/particles/smokey.vtf")
resource.AddFile("materials/particles/smokey.vmt")

resource.AddFile("materials/models/gtaiv/vehicles/truckfire/firetruck_badges.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/truckfire/firetruck_badges.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/truckfire/firetruck_badges_normal.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/truckfire/firetruck_detail.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/truckfire/firetruck_detail.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/truckfire/firetruck_detail_normal.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/truckfire/firetruck_lights_glass.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/truckfire/firetruck_lights_glass.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/truckfire/vehicle_generic_smallspecmap.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/truckfire/vehicle_generic_smallspecmap.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/truckfire/vehicle_generic_smallspecmap2.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/truckfire/vehicle_generic_smallspecmap2.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/truckfire/vehicle_generic_tyrewallblack.vmt")

resource.AddFile("materials/models/gtaiv/vehicles/ecpd/norm.vtf")

resource.AddFile("materials/models/gtaiv/vehicles/black.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/blcak.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/blue.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/blue.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/color_normal.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/green.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/green.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/red.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/red.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/schafter_interior.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/schafter_interior.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_carbon.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_carbon.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_carbon_normal.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_detail2.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_detail2.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_detail2_normal.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_detail22.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_detail24.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_doorshut.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_doorshut.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_doorshut_normal.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_glasswindows2.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_glasswindows2.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_glasswindows2_normal.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_glasswindows2_normal.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_interior.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_interior.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_interiorn.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_mesh1.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_mesh1.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_mesh1_normal.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_mesh1_normal.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_metalpaint_normal.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_truck_interior.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_truck_interior.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_truck_interior_normal.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_truck_interior_normal.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_tyrewall_normal.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_tyrewall_spec.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_tyrewallblack.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_tyrewallblack.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_tyrewallblack_normal.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_tyrewallblack_normal.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_tyrewallblack2.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_tyrewallred.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/white.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/white.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/yellow.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/yellow.vtf")

resource.AddFile("materials/models/props_junk/garbage001z_01.vtf")
resource.AddFile("materials/models/props_junk/garbage001z_01.vmt")

resource.AddFile("materials/models/weapons/v_crowbar/axe.vmt")
resource.AddFile("materials/models/weapons/v_crowbar/axe.vtf")
resource.AddFile("materials/models/weapons/v_crowbar/axe_normal.vtf")

resource.AddFile("models/sickness/truckfire.dx80")
resource.AddFile("models/sickness/truckfire.dx90")
resource.AddFile("models/sickness/truckfire.mdl")
resource.AddFile("models/sickness/truckfire.phy")
resource.AddFile("models/sickness/truckfire.sw")
resource.AddFile("models/sickness/truckfire.vvd")

resource.AddFile("models/weapons/v_beerbo2.dx80")
resource.AddFile("models/weapons/v_beerbo2.dx90")
resource.AddFile("models/weapons/v_beerbo2.mdl")
resource.AddFile("models/weapons/v_beerbo2.phy")
resource.AddFile("models/weapons/v_beerbo2.sw")
resource.AddFile("models/weapons/v_beerbo2.vvd")

resource.AddFile("models/weapons/W_beerbot.dx80")
resource.AddFile("models/weapons/W_beerbot.dx90")
resource.AddFile("models/weapons/W_beerbot.mdl")
resource.AddFile("models/weapons/W_beerbot.phy")
resource.AddFile("models/weapons/W_beerbot.sw")
resource.AddFile("models/weapons/W_beerbot.vvd")

resource.AddFile("models/weapons/v_fireaxe.dx80")
resource.AddFile("models/weapons/v_fireaxe.dx90")
resource.AddFile("models/weapons/v_fireaxe.mdl")
resource.AddFile("models/weapons/v_fireaxe.phy")
resource.AddFile("models/weapons/v_fireaxe.sw")
resource.AddFile("models/weapons/v_fireaxe.vvd")

resource.AddFile("models/weapons/w_fireaxe.dx80")
resource.AddFile("models/weapons/w_fireaxe.dx90")
resource.AddFile("models/weapons/w_fireaxe.mdl")
resource.AddFile("models/weapons/w_fireaxe.phy")
resource.AddFile("models/weapons/w_fireaxe.sw")
resource.AddFile("models/weapons/w_fireaxe.vvd")


resource.AddFile("sound/vehicles/phantom/first.wav")
resource.AddFile("sound/vehicles/phantom/firstgear_reverse.wav")
resource.AddFile("sound/vehicles/phantom/fourth.wav")
resource.AddFile("sound/vehicles/phantom/fourth_cruise.wav")
resource.AddFile("sound/vehicles/phantom/idle.wav")
resource.AddFile("sound/vehicles/phantom/rev_short_loop.wav")
resource.AddFile("sound/vehicles/phantom/second.wav")
resource.AddFile("sound/vehicles/phantom/start.wav")
resource.AddFile("sound/vehicles/phantom/stop.wav")
resource.AddFile("sound/vehicles/phantom/third.wav")
resource.AddFile("sound/vehicles/phantom/throttle_off_fast.wav")
resource.AddFile("sound/vehicles/phantom/throttle_off_slow.wav")

resource.AddFile("sound/vehicles/v8/skid_highfriction.wav")
resource.AddFile("sound/vehicles/v8/skid_lowfriction.wav")
resource.AddFile("sound/vehicles/v8/skid_normalfriction.wav")
resource.AddFile("sound/vehicles/v8/vehicle_impact_heavy1.wav")
resource.AddFile("sound/vehicles/v8/vehicle_impact_heavy2.wav")
resource.AddFile("sound/vehicles/v8/vehicle_impact_heavy3.wav")
resource.AddFile("sound/vehicles/v8/vehicle_impact_heavy4.wav")
resource.AddFile("sound/vehicles/v8/vehicle_impact_medium1.wav")
resource.AddFile("sound/vehicles/v8/vehicle_impact_medium2.wav")
resource.AddFile("sound/vehicles/v8/vehicle_impact_medium3.wav")
resource.AddFile("sound/vehicles/v8/vehicle_impact_medium4.wav")
resource.AddFile("sound/vehicles/v8/vehicle_rollover1.wav")
resource.AddFile("sound/vehicles/v8/vehicle_rollover2.wav")

resource.AddFile("models/weapons/c_fire_extinguisher.dx80")
resource.AddFile("models/weapons/c_fire_extinguisher.dx90")
resource.AddFile("models/weapons/c_fire_extinguisher.mdl")
resource.AddFile("models/weapons/c_fire_extinguisher.phy")
resource.AddFile("models/weapons/c_fire_extinguisher.sw")
resource.AddFile("models/weapons/c_fire_extinguisher.vvd")

resource.AddFile("models/weapons/w_fire_extinguisher.dx80")
resource.AddFile("models/weapons/w_fire_extinguisher.dx90")
resource.AddFile("models/weapons/w_fire_extinguisher.mdl")
resource.AddFile("models/weapons/w_fire_extinguisher.phy")
resource.AddFile("models/weapons/w_fire_extinguisher.sw")
resource.AddFile("models/weapons/w_fire_extinguisher.vvd")

resource.AddFile("materials/models/weapons/v_fire_extinguisher/clamp.vtf")
resource.AddFile("materials/models/weapons/v_fire_extinguisher/clamp.vmt")
resource.AddFile("materials/models/weapons/v_fire_extinguisher/cone.vtf")
resource.AddFile("materials/models/weapons/v_fire_extinguisher/cone.vmt")
resource.AddFile("materials/models/weapons/v_fire_extinguisher/guage.vtf")
resource.AddFile("materials/models/weapons/v_fire_extinguisher/guage.vmt")
resource.AddFile("materials/models/weapons/v_fire_extinguisher/handle_noozle.vtf")
resource.AddFile("materials/models/weapons/v_fire_extinguisher/handle_noozle.vmt")
resource.AddFile("materials/models/weapons/v_fire_extinguisher/tank.vmt")

resource.AddFile("materials/models/weapons/w_fire_extinguisher/w_fire_extinguisher.vtf")
resource.AddFile("materials/models/weapons/w_fire_extinguisher/w_fire_extinguisher.vmt")
resource.AddFile("materials/models/weapons/w_fire_extinguisher/w_fire_extinguisher_hose.vtf")
resource.AddFile("materials/models/weapons/w_fire_extinguisher/w_fire_extinguisher_hose.vmt")
resource.AddFile("materials/models/weapons/w_fire_extinguisher/w_fire_extinguisher_mask.vtf")
resource.AddFile("materials/models/weapons/w_fire_extinguisher/w_fire_extinguisher_valve.vtf")
resource.AddFile("materials/models/weapons/w_fire_extinguisher/w_fire_extinguisher_valve.vmt")


function SOUND_AddDir(dir) // recursively adds everything in a directory to be downloaded by client
    local list = file.Find(dir.."/*", "DATA")
    for _, fdir in pairs(list) do
        if fdir != ".svn" then // don't spam people with useless .svn folders
            SOUND_AddDir(dir.."/"..fdir)
        end
    end
  
    for k,v in pairs(file.Find(dir.."/*", "GAME")) do
        resource.AddFile(dir.."/"..v)
    end
end

SOUND_AddDir("sound/vehicles/v8") 
SOUND_AddDir("sound/vehicles/phantom") 