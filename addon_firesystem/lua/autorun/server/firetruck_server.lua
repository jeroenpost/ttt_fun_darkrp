if not file.Exists( "craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/firetruck_location.txt", "DATA" ) then
	file.Write("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/firetruck_location.txt", "0;-0;-0;0;0;0", "DATA")
end

function RP_FireTruck_Position( ply )
	if ply:IsAdmin() then
		local HisVector = string.Explode(" ", tostring(ply:GetPos()))
		local HisAngles = string.Explode(" ", tostring(ply:GetAngles()))
		
		file.Write("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/firetruck_location.txt", ""..(HisVector[1])..";"..(HisVector[2])..";"..(HisVector[3])..";"..(HisAngles[1])..";"..(HisAngles[2])..";"..(HisAngles[3]).."", "DATA")
		ply:ChatPrint("New position for the fire truck has been succesfully set. The new position is now in effect!")
	else
		ply:ChatPrint("Only administrators can perform this action")
	end
end
concommand.Add("firetruck_setpos", RP_FireTruck_Position)

local CurFireTrucks = 0

util.AddNetworkString("FIRE_CreateFireTruck")
net.Receive("FIRE_CreateFireTruck", function(length, ply)
	
	if ply.HasFireTruck then
		DarkRP.notify(ply, 1, 5,  "You already own a firetruck!")
		return
	end
	
	if CurFireTrucks == FIRETRUCK_MaxTrucks then
		DarkRP.notify(ply, 1, 5,  "The limitation of maximum firetrucks has been reached!")
		return
	end
	
	DarkRP.notify(ply, 1, 5,  "You have succesfully retrieved a firetruck!")
	
	local PositionFile = file.Read("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/firetruck_location.txt", "DATA")
	local ThePosition = string.Explode( ";", PositionFile )
	local TheVector = Vector(ThePosition[1], ThePosition[2], ThePosition[3])
	local TheAngle = Angle(tonumber(ThePosition[4]), ThePosition[5], ThePosition[6])
	
	if AdvancedFiretruck then
		local AdvFireTruck = ents.Create( "prop_vehicle_jeep" )
		AdvFireTruck:SetKeyValue( "vehiclescript", FIRETRUCK_AdvVehicleScript )
		AdvFireTruck:SetPos( TheVector )
		AdvFireTruck:SetAngles( TheAngle )
		AdvFireTruck:SetRenderMode( RENDERMODE_TRANSADDFRAMEBLEND )
		AdvFireTruck:SetModel( FIRETRUCK_AdvVehicleModel )
		AdvFireTruck:Spawn()
		AdvFireTruck:Activate()
		AdvFireTruck:SetHealth( FIRETRUCK_AdvHealth ) 
		AdvFireTruck:SetNWInt( "Owner", ply:EntIndex() )
		AdvFireTruck:keysOwn( ply )
		
		AdvFireTruck.Lights = {}
		local TruckLight = ents.Create("advfire_siren")
		TruckLight:SetPos( AdvFireTruck:GetPos() + Vector( 0, 50, 120 ) )
		TruckLight:SetParent( AdvFireTruck )
		TruckLight:Spawn()
		table.insert( AdvFireTruck.Lights, TruckLight )
	else
		local FireTruck = ents.Create( "prop_vehicle_jeep" )
		FireTruck:SetKeyValue( "vehiclescript", FIRETRUCK_VehicleScript )
		FireTruck:SetPos( TheVector )
		FireTruck:SetAngles( TheAngle )
		FireTruck:SetRenderMode( RENDERMODE_TRANSADDFRAMEBLEND )
		FireTruck:SetModel( FIRETRUCK_VehicleModel )
		FireTruck:Spawn()
		FireTruck:Activate()
		FireTruck:SetHealth( FIRETRUCK_Health ) 
		FireTruck:SetNWInt( "Owner", ply:EntIndex() )
		FireTruck:keysOwn( ply )
	end
	
	ply.HasFireTruck = true
	CurFireTrucks = CurFireTrucks + 1
end)

util.AddNetworkString("FIRE_RemoveFireTruck")
net.Receive("FIRE_RemoveFireTruck", function(length, ply)
	
	if ply.HasFireTruck then
		for _, ent in pairs(ents.GetAll()) do
			if ent:GetModel() == FIRETRUCK_VehicleModel then
				if ent:GetNWInt("Owner") == ply:EntIndex() then
					ent:Remove()
					DarkRP.notify(ply, 1, 5, "Your firetruck has been removed!")
				end
			end
		end
	else
		DarkRP.notify(ply, 1, 5, "You don't have a firetruck!")
	end

end)

function FIRETRUCK_Removal( ent )
	if ent:GetModel() == FIRETRUCK_VehicleModel then
		player.GetByID(ent:GetNWInt("Owner")).HasFireTruck = false
		CurFireTrucks = CurFireTrucks - 1
	end
end
hook.Add("EntityRemoved", "FIRETRUCK_Removal", FIRETRUCK_Removal)

function FIRETRUCK_Disconnect( ply )
	for _, ent in pairs(ents.GetAll()) do
		if ent:GetModel() == FIRETRUCK_VehicleModel then
			if ent:GetNWInt("Owner") == ply:EntIndex() then
				ent:Remove()
			end
		end
	end
end
hook.Add("PlayerDisconnected", "FIRETRUCK_Disconnect", FIRETRUCK_Disconnect)

function FIRETRUCK_JobChange( ply )
	for _, ent in pairs(ents.FindByClass("prop_vehicle_jeep")) do
		if not table.HasValue( FIRE_AllowedTeams, team.GetName(ply:Team()) ) then
			if ply.HasFireTruck then
				if ent:GetNWInt("Owner") == ply:EntIndex() then
					if ent:IsValid() then
						ent:Remove()
					end
				end
			end
		end
	end
end


function FIRETRUCK_CustomExit(ply, vehicle)
	if vehicle:GetModel() == FIRETRUCK_VehicleModel then
		ply:SetPos( vehicle:GetPos() + Vector(-90,125,20) )
	end
end
hook.Add("PlayerLeaveVehicle", "FIRETRUCK_CustomExit", FIRETRUCK_CustomExit)