function FIRE_MapInit()
	timer.Create("FIRE_CreateTimer", FIRE_RandomFireInterval, 0, function()
		if team.NumPlayers( TEAM_FIREFIGHTER ) >= 1 then
			if FIRE_CurrentFires <= 0 then
				if not file.IsDir("craphead_scripts", "DATA") then
					file.CreateDir("craphead_scripts", "DATA")
				end
			
				if not file.IsDir("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."", "DATA") then
					file.CreateDir("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."", "DATA")
				end
				
				for k, v in pairs(file.Find("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/fire_location_*.txt", "DATA")) do
					if FIRE_CurrentFires >= FIRE_MaxFires then 
						return 
					end
					
					local Randomize = 2
					if FIRE_RandomizeFireSpawn then
						Randomize = math.random(1,2)
					end
					
					if tonumber(Randomize) == 2 then
						local PositionFile = file.Read("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/".. v, "DATA")
				
						local ThePosition = string.Explode( ";", PositionFile )
					
						local TheVector = Vector(ThePosition[1], ThePosition[2], ThePosition[3])
						local TheAngle = Angle(tonumber(ThePosition[4]), ThePosition[5], ThePosition[6])

						local Fire = ents.Create("fire")
						Fire:SetPos(TheVector)
						Fire:SetAngles(TheAngle)
						Fire:Spawn()
					end
				end
			end
		end
	end)
end
hook.Add( "Initialize", "FIRE_MapInit", FIRE_MapInit )

function FIRE_CreateFire( ply, cmd, args )
	if ply:IsAdmin() then
		local FileName = args[1]
		
		if not FileName then
			ply:ChatPrint("Please choose a UNIQUE name for the fire!") 
			return
		end
		
		if file.Exists( "craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/fire_location_".. FileName ..".txt", "DATA" ) then
			ply:ChatPrint("This file name is already in use. Please choose another name for this location of fire.")
			return
		end
		
		local HisVector = string.Explode(" ", tostring(ply:GetPos()))
		local HisAngles = string.Explode(" ", tostring(ply:GetAngles()))
		
		file.Write("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/fire_location_".. FileName ..".txt", ""..(HisVector[1])..";"..(HisVector[2])..";"..(HisVector[3])..";"..(HisAngles[1])..";"..(HisAngles[2])..";"..(HisAngles[3]).."", "DATA")
		ply:ChatPrint("New fire location created!")
	else
		ply:ChatPrint("Only administrators can perform this action")
	end
end
concommand.Add("ch_create_fire", FIRE_CreateFire)

function FIRE_RemoveFire( ply, cmd, args )
	if ply:IsAdmin() then
		local FileName = args[1]
		
		if not FileName then
			ply:ChatPrint("Please enter a filename!") 
			return
		end
		
		if file.Exists( "craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/fire_location_".. FileName ..".txt", "DATA" ) then
			file.Delete( "craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/fire_location_".. FileName ..".txt" )
			ply:ChatPrint("The selected fire has been removed!")
		else
			ply:ChatPrint("The selected fire does not exist!")
		end
	else
		ply:ChatPrint("Only administrators can perform this action")
	end
end
concommand.Add("ch_remove_fire", FIRE_RemoveFire)

function FIRE_ValidFiresList( ply, cmd, args )
	if ply:IsAdmin() then
		for k, v in pairs(file.Find("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/fire_location_*.txt", "DATA")) do
			local PosFile = file.Read("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/".. v, "DATA")
			local ThePos = string.Explode( ";", PosFile )
			local ThePrintPos = ThePos[1], ThePos[2], ThePos[3]
					
			print("NAMES OF VALID FIRES:")
			print("File Name: ".. v .." - Fire Position: "..ThePrintPos)
			print("REMEMBER: YOU ONLY USE THE LAST OF THE FILE NAME. SO IF A FILE NAME IS CALLED 'FIRE_LOCATION_TRAIN', THEN YOU WOULD USE 'CH_REMOVE_FIRE TRAIN' TO REMOVE IT!")
		end 
	else
		ply:ChatPrint("Only administrators can perform this action")
	end
end
concommand.Add("ch_valid_fires", FIRE_ValidFiresList)

function FIRE_Disconnect()
	timer.Simple(2, function()
		if FIRE_RemoveAllOnLastDC then
			if #player.GetAll() == 0 then
				for k, v in pairs(ents.FindByClass("fire")) do
					v:KillFire()
				end
			end
		end
	end)
end
hook.Add( "PlayerDisconnected", "FIRE_Disconnect", FIRE_Disconnect )

function FIRE_PlayerCanPickupMolotov( ply, wep )
	if ply:HasWeapon("fire_molotov") then
		if wep:GetClass() == "fire_molotov" then 
			return false 
		end
	end

	return true
end
hook.Add( "PlayerCanPickupWeapon", "FIRE_PlayerCanPickupMolotov", FIRE_PlayerCanPickupMolotov )

-- Extra Administration Commands
function FIRE_ADMIN_KillFires( ply )
	if ply:IsAdmin() then
		for k, v in pairs(ents.FindByClass("fire")) do
			v:KillFire()
		end
	else
		ply:ChatPrint("Only administrators can perform this action")
	end
end
concommand.Add("fire_admin_killfires", FIRE_ADMIN_KillFires)

function FIRE_ADMIN_CurrentFires( ply )
	if ply:IsAdmin() then
		ply:ChatPrint("There are currently ".. FIRE_CurrentFires .." active fires.")
	else
		ply:ChatPrint("Only administrators can perform this action")
	end
end
concommand.Add("fire_admin_curfires", FIRE_ADMIN_CurrentFires)

function FIRE_ADMIN_SpawnFire( ply )
	if ply:IsAdmin() then
		local trace = ply:GetEyeTrace()
		
		local Fire = ents.Create("fire")
		Fire:SetPos(trace.HitPos)
		Fire:Spawn()
	else
		ply:ChatPrint("Only administrators can perform this action")
	end
end
concommand.Add("fire_admin_spawnfire", FIRE_ADMIN_SpawnFire)