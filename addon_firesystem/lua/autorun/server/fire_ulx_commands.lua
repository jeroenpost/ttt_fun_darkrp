function ulx.molotov( calling_ply, target_ply )
	target_ply:Give( "fire_molotov" )
	
	ulx.fancyLogAdmin( calling_ply, true, "#A gave a molotov to #T", target_ply )
end
local molotov = ulx.command( "Fire System", "ulx molotov", ulx.molotov, "!molotov", true )
molotov:addParam{ type=ULib.cmds.PlayerArg }
molotov:defaultAccess( ULib.ACCESS_ADMIN )
molotov:help( "Give player a molotov." )

function ulx.extinguisher( calling_ply, target_ply )
	target_ply:Give( "fire_extinguisher" )
	
	ulx.fancyLogAdmin( calling_ply, true, "#A gave an extinguisher to #T", target_ply )	
end
local extinguisher = ulx.command( "Fire System", "ulx extinguisher", ulx.extinguisher, "!ext", true )
extinguisher:addParam{ type=ULib.cmds.PlayerArg }
extinguisher:defaultAccess( ULib.ACCESS_ADMIN )
extinguisher:help( "Give player an extinguisher." )

function ulx.forcefirefighter( calling_ply, target_ply )
	target_ply:changeTeam( TEAM_FIREFIGHTER )
	
	ulx.fancyLogAdmin( calling_ply, true, "#A forced #T to become a fire fighter", target_ply ) 
end
local forcefirefighter = ulx.command( "Fire System", "ulx forcefirefighter", ulx.forcefirefighter, "!forcefirefighter", true )
forcefirefighter:addParam{ type=ULib.cmds.PlayerArg }
forcefirefighter:defaultAccess( ULib.ACCESS_ADMIN )
forcefirefighter:help( "Make player fire fighter." )

function ulx.fireoff( calling_ply )
	for k, v in pairs(ents.FindByClass("fire")) do
		v:KillFire()
	end
	
	ulx.fancyLogAdmin( calling_ply, true, "#A turned off all fires" ) 
end
local fireoff = ulx.command( "Fire System", "ulx fireoff", ulx.fireoff, "!fireoff", true )
fireoff:defaultAccess( ULib.ACCESS_ADMIN )

function ulx.startfire( calling_ply )
	local trace = calling_ply:GetEyeTrace()
		
	local Fire = ents.Create("fire")
	Fire:SetPos(trace.HitPos)
	Fire:Spawn()
	
	ulx.fancyLogAdmin( calling_ply, true, "#A started a fire" ) 
end
local startfire = ulx.command( "Fire System", "ulx startfire", ulx.startfire, "!startfire", true )
startfire:defaultAccess( ULib.ACCESS_ADMIN )

function ulx.validfires( calling_ply )
	for k, v in pairs(file.Find("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/fire_location_*.txt", "DATA")) do
		local PosFile = file.Read("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/".. v, "DATA")
		local ThePos = string.Explode( ";", PosFile )
		local ThePrintPos = ThePos[1], ThePos[2], ThePos[3]
					
		print("NAMES OF VALID FIRES:")
		print("File Name: ".. v .." - Fire Position: "..ThePrintPos)
		print("REMEMBER: YOU ONLY USE THE LAST OF THE FILE NAME. SO IF A FILE NAME IS CALLED 'FIRE_LOCATION_TRAIN', THEN YOU WOULD USE 'CH_REMOVE_FIRE TRAIN' TO REMOVE IT!")
	end
	
	ulx.fancyLogAdmin( calling_ply, true, "#A looked up valid fires" )
	calling_ply:ChatPrint("A list of valid fires has been printed to your console!")
end
local validfires = ulx.command( "Fire System", "ulx validfires", ulx.validfires, "!validfires", true )
validfires:defaultAccess( ULib.ACCESS_ADMIN )

function ulx.allfires( calling_ply )
	for k, v in pairs(file.Find("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/fire_location_*.txt", "DATA")) do
		if FIRE_CurrentFires >= FIRE_MaxFires then 
			return 
		end
		
		local Randomize = 2
		if FIRE_RandomizeFireSpawn then
			Randomize = math.random(1,2)
		end
		
		if tonumber(Randomize) == 2 then
			local PositionFile = file.Read("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/".. v, "DATA")
				
			local ThePosition = string.Explode( ";", PositionFile )
				
			local TheVector = Vector(ThePosition[1], ThePosition[2], ThePosition[3])
			local TheAngle = Angle(tonumber(ThePosition[4]), ThePosition[5], ThePosition[6])

			local Fire = ents.Create("fire")
			Fire:SetPos(TheVector)
			Fire:SetAngles(TheAngle)
			Fire:Spawn()
		end
	end
	
	ulx.fancyLogAdmin( calling_ply, true, "#A started all fires" ) 
end
local allfires = ulx.command( "Fire System", "ulx allfires", ulx.allfires, "!allfires", true )
allfires:defaultAccess( ULib.ACCESS_SUPERADMIN )