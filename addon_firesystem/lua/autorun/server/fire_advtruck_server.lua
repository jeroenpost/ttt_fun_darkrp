timer.Simple(3, function()
	AdvancedFiretruck = true
end)

resource.AddFile("sound/craphead_scripts/fire_system/firetruck_siren.wav")
resource.AddFile("sound/craphead_scripts/fire_system/firetruck_horn.mp3")
resource.AddFile("sound/craphead_scripts/fire_system/firetruck_shortsiren.mp3")

FIRE_ADVTRUCK_HornKey = IN_WALK
FIRE_ADVTRUCK_ShortSirenKey = IN_ATTACK
FIRE_ADVTRUCK_ToogleSirenKey = IN_ATTACK2
FIRE_ADVTRUCK_ToogleLightKey = IN_SPEED

FIRETRUCK_AdvVehicleScript = "scripts/vehicles/firetruck.txt"
FIRETRUCK_AdvVehicleModel = "models/sickness/truckfire.mdl"
FIRETRUCK_AdvHealth = 500

function FIRE_ADVTRUCK_Siren( ply, key )
	if ply:InVehicle() and ply:GetVehicle():GetClass() == "prop_vehicle_jeep" then
		local Vehicle = ply:GetVehicle()
		
		if Vehicle:GetModel() == FIRETRUCK_VehicleModel then
			if key == FIRE_ADVTRUCK_HornKey then
				ply.HornDelay = ply.HornDelay or 0
				if ply.HornDelay + 1 > CurTime() then
					return
				end
				ply.HornDelay = CurTime()
					
				if Vehicle:GetModel() == "models/sickness/truckfire.mdl" then
					Vehicle:EmitSound( "craphead_scripts/fire_system/firetruck_horn.mp3" )
				end
			elseif key == FIRE_ADVTRUCK_ShortSirenKey then
				ply.ShortSirenDelay = ply.ShortSirenDelay or 0
				if ply.ShortSirenDelay + 1 > CurTime() then
					return
				end
				ply.ShortSirenDelay = CurTime()
					
				if Vehicle:GetModel() == "models/sickness/truckfire.mdl" then
					Vehicle:EmitSound( "craphead_scripts/fire_system/firetruck_shortsiren.mp3" )
				end
			elseif key == FIRE_ADVTRUCK_ToogleSirenKey then
				if Vehicle.Lights != nil then
					ply.ToogleSirenDelay = ply.ToogleSirenDelay or 0
					if ply.ToogleSirenDelay + 3 > CurTime() then
						return false
					end
					ply.ToogleSirenDelay = CurTime()
					
					for _, light in pairs( Vehicle.Lights ) do
						if not light:GetNWBool( "SirenOn" ) then
							light:SetNWBool( "SirenOn", true )
						else
							light:SetNWBool( "SirenOn", false )
						end
					end
				end
			elseif key == FIRE_ADVTRUCK_ToogleLightKey then
				ply.ToogleLightDelay = ply.ToogleLightDelay or 0
				if ply.ToogleLightDelay + 3 > CurTime() then
					return false
				end
				ply.ToogleLightDelay = CurTime()
				
				if ply:GetVehicle().Lights != nil then
					for _,light in pairs( Vehicle.Lights ) do
						if not light:GetNWBool( "LightOn" ) then
							light:SetNWBool( "LightOn", true )
						else 
							light:SetNWBool( "LightOn", false )
						end
					end
				end
			end
		end
	end
end
hook.Add("KeyPress", "FIRE_ADVTRUCK_Siren", FIRE_ADVTRUCK_Siren)

function FIRE_ADVTRUCK_SirensOffOnOut( ply, Vehicle )
	if Vehicle.Lights != nil then
		for _, light in pairs( Vehicle.Lights ) do
			light:SetNWBool( "siren", false )
		end
	end
end
hook.Add("CanExitVehicle", "FIRE_ADVTRUCK_SirensOffOnOut", FIRE_ADVTRUCK_SirensOffOnOut)