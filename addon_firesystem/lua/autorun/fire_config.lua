

-- General Config
FIRE_MaterialTypes = { -- Full list of material types (ground types), that fire can spread on. For example: wood, grass, carpet.
	MAT_DIRT,
	MAT_WOOD,
	MAT_COMPUTER,
	MAT_FOLIAGE,
	MAT_PLASTIC,
	MAT_SAND,
	MAT_SLOSH,
	MAT_TILE,
	MAT_VENT -- THE LAST LINE SHOULD NOT HAVE A COMMA AT THE END. BE AWARE OF THIS WHEN EDITING THIS!
}

FIRE_AllowedTeams = { -- This is a list of fire fighter teams. You can add fire chief and stuff like that, and they can do the things fire fighters can.
	-- Please note that this table has to be set up as the one above. If you add more teams, you must add a comma to the team at the top. Only the last team should not have a comma. By default there is only one team, so no comma is needed.
	"Fire Fighter"
}

FIRE_MaxFires = 25 -- Maximum amount of fires that can spawn/spread. [Default = 250]
FIRE_RandomFireInterval = 900 -- Interval between random fires are generated around the map. [Default = 300 (5 minutes)]
FIRE_RemoveAllOnLastDC = true -- Remove all fires when there are no more players on the server (to prevent lag?). [Default = false]
FIRE_SpreadInterval = 180 -- Time between fire spreads (in seconds). [Default = 120 (2 minutes)]
FIRE_ExtinguishPay = 10 -- Payment for turning off a fire. [Default = 10]
FIRE_NotifyOnExtinguish = true -- Send the player a notification that they've received money for extinguishing fire. [Default = true]
FIRE_AutoTurnOff = 300 -- Fire will automatically turn off after x seconds. 0 and it will burn until put out with extinguisher. [Default = 0]
FIRE_RandomizeFireSpawn = true -- Will randomize if a fire spawns or not when FIRE_RandomFireInterval hits 0. true is enabled, false is disabled. [Default = false]

-- Fire Damage Config
FIRE_FireFighterDamage = 2 -- The amount of damage fire fighters should take from standing in fire. [Default = 2]
FIRE_FireDamage = 4 -- The amount of damage everyone else should take from standing in fire. [Default = 4]
FIRE_VehicleDamage = 1 -- The amount of damage vehicles should take from being in fire. [Default = 6]
FIRE_DamageInterval = 1 -- The amount of time between taking damage when standing in fire (in seconds). [Default = 0.5]
FIRE_RemovePropTimer = 1200 -- Time amount of seconds before a prop is removed after it ignites (if it is not extinguished). [Default = 10]

FIRE_BurntPropColor = Color(120, 120, 120, 255) -- If a prop hits the fire, it will set on fire and turn into this color (burnt color). [Default = Color(120, 120, 120, 255)]

-- Firetruck Config
FIRETRUCK_VehicleScript = "scripts/vehicles/firetruck.txt" -- This is the vehicle script for the vehicle. The default vehicle script is from Sickness Models.
FIRETRUCK_VehicleModel = "models/sickness/truckfire.mdl" -- This is the model for the firetruck. The default model is from Sickness Models.
FIRETRUCK_NPCModel = "models/odessa.mdl" -- This is the model of the NPC to get a firetruck from.
FIRETRUCK_Health = 5000 -- The amount of health the fire truck has.
FIRETRUCK_MaxTrucks = 1 -- The maximum amount of firetrucks allowed.

-- Fire Axe Config
FIREAXE_FireRange = 450 -- If there is fire within this range of the door, firefighters can open it. [Default = 450]
FIREAXE_PlayerDamage = 1 -- How much damage should the fire axe do to players. [Default = 5 (0 for disabled)]

-- Fire Pyro Job
FIREPYRO_EnablePyroJob = true -- If you want to enable the pyro job and the cheap molotov cocktail, set this to true. If not, set it to false. [Default = true]

-- Fire Extinguisher Citizen
FIREEXTCITZ_RemoveTimer = 250 -- The amount of seconds before the fire fighter is removed from the citizen after he start using it. [Default = 20 (Recommended)
FIREEXTCITZ_ExtinguishPay = 100 -- Payment for turning off a fire as a non-firefighter. [Default = 4]