-- Firetruck sound script.
sound.Add({
	name = "phantom_engine_idle",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 95,
	pitchend = 110,
	sound = "vehicles/phantom/idle.wav"
})

sound.Add({
	name = "phantom_engine_null",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 95,
	pitchend = 110,
	sound = "common/null.wav"
})

sound.Add({
	name = "phantom_engine_start",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 95,
	pitchend = 110,
	sound = "vehicles/phantom/start.wav"
})

sound.Add({
	name = "phantom_engine_stop",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 95,
	pitchend = 110,
	sound = "vehicles/phantom/stop.wav"
})

sound.Add({
	name = "phantom_rev",
	volume = 0.9,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 98,
	pitchend = 105,
	sound = "vehicles/phantom/rev_short_loop.wav"
})

sound.Add({
	name = "phantom_reverse",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 95,
	pitchend = 110,
	sound = "vehicles/phantom/firstgear_reverse.wav"
})

sound.Add({
	name = "phantom_firstgear",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 95,
	pitchend = 110,
	sound = "vehicles/phantom/first.wav"
})

sound.Add({
	name = "phantom_secondgear",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 90,
	pitchend = 105,
	sound = "vehicles/phantom/second.wav"
})

sound.Add({
	name = "phantom_thirdgear",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 90,
	pitchend = 105,
	sound = "vehicles/phantom/third.wav"
})

sound.Add({
	name = "phantom_fourthgear",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 105,
	pitchend = 105,
	sound = "vehicles/phantom/fourth_cruise.wav"
})

sound.Add({
	name = "phantom_firstgear_noshift",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 105,
	pitchend = 105,
	sound = "vehicles/phantom/first.wav"
})

sound.Add({
	name = "phantom_secondgear_noshift",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 105,
	pitchend = 105,
	sound = "vehicles/phantom/second.wav"
})

sound.Add({
	name = "phantom_thirdgear_noshift",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 105,
	pitchend = 105,
	sound = "vehicles/phantom/third.wav"
})

sound.Add({
	name = "phantom_fourthgear_noshift",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 105,
	pitchend = 105,
	sound = "vehicles/phantom/fourth_cruise.wav"
})

sound.Add({
	name = "phantom_downshift_to_2nd",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 90,
	pitchend = 105,
	sound = "vehicles/phantom/firstgear_rev_loop.wav"
})

sound.Add({
	name = "phantom_downshift_to_1st",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 90,
	pitchend = 105,
	sound = "vehicles/phantom/throttle_off_fast.wav"
})

sound.Add({
	name = "phantom_throttleoff_slowspeed",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 90,
	pitchend = 105,
	sound = "vehicles/phantom/Throttle_off_slow.wav"
})

sound.Add({
	name = "phantom_throttleoff_fastspeed",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 90,
	pitchend = 105,
	sound = "vehicles/phantom/throttle_off_fast.wav"
})

sound.Add({
	name = "phantom_skid_lowfriction",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_BODY,
	pitchstart = 90,
	pitchend = 110,
	sound = "vehicles/v8/skid_lowfriction.wav"
})

sound.Add({
	name = "phantom_skid_normalfriction",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_BODY,
	pitchstart = 90,
	pitchend = 110,
	sound = "vehicles/v8/skid_normalfriction.wav"
})

sound.Add({
	name = "phantom_skid_highfriction",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_BODY,
	pitchstart = 90,
	pitchend = 110,
	sound = "vehicles/v8/skid_highfriction.wav"
})

sound.Add({
	name = "phantom_impact_heavy",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 95,
	pitchend = 110,
	sound = {
		"vehicles/v8/vehicle_impact_heavy1.wav",
		"vehicles/v8/vehicle_impact_heavy2.wav",
		"vehicles/v8/vehicle_impact_heavy3.wav",
		"vehicles/v8/vehicle_impact_heavy4.wav"
	}
})

sound.Add({
	name = "phantom_impact_medium",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 95,
	pitchend = 110,
	sound = {
		"vehicles/v8/vehicle_impact_medium1.wav",
		"vehicles/v8/vehicle_impact_medium2.wav",
		"vehicles/v8/vehicle_impact_medium3.wav",
		"vehicles/v8/vehicle_impact_medium4.wav"
	}
})

sound.Add({
	name = "phantom_rollover",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_STATIC,
	pitchstart = 95,
	pitchend = 110,
	sound = {
		"vehicles/v8/vehicle_rollover1.wav",
		"vehicles/v8/vehicle_rollover2.wav"
	}
})

sound.Add({
	name = "phantom_turbo_on",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_ITEM,
	pitchstart = 90,
	pitchend = 110,
	sound = "vehicles/phantom/turbo.wav"
})

sound.Add({
	name = "phantom_turbo_off",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_ITEM,
	pitchstart = 90,
	pitchend = 110,
	sound = "vehicles/v8/v8_turbo_off.wav"
})

sound.Add({
	name = "phantom_start_in_water",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_VOICE,
	pitchstart = 95,
	pitchend = 110,
	sound = "vehicles/jetski/jetski_no_gas_start.wav"
})


sound.Add({
	name = "phantom_stall_in_water",
	volume = 1.0,
	soundlevel = 80,
	channel = CHAN_VOICE,
	pitchstart = 95,
	pitchend = 110,
	sound = "vehicles/jetski/jetski_off.wav"
})