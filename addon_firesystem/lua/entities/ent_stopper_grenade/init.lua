AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self.Entity:SetModel("models/weapons/w_eq_smokegrenade.mdl")
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Entity:SetCollisionGroup( COLLISION_GROUP_PROJECTILE )
	
	local phys = self:GetPhysicsObject()
	
	if phys:IsValid() then
		phys:Wake()
		phys:EnableGravity( true )
	end
end

function ENT:PhysicsCollide( data )
	local pos = self.Entity:GetPos()
	
	-- Remove nearby fires.
	for k, v in pairs(ents.FindInSphere( pos, 160 )) do
		if v:GetClass() == "fire" then
			local effectdata = EffectData()
			effectdata:SetStart( v:GetPos() )
			effectdata:SetOrigin( v:GetPos() )
			effectdata:SetScale( 1 )
			
			util.Effect( "WaterSurfaceExplosion", effectdata )
			
			v:KillFire()
		end
	end
	
	self.Entity:Remove()
end 