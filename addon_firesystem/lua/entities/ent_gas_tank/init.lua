AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include( "shared.lua" )

function ENT:Initialize()
	self:SetModel("models/props_junk/gascan001a.mdl")
	self:SetHealth( 50 )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetCollisionGroup( COLLISION_GROUP_DEBRIS )
end

function ENT:OnTakeDamage( dmginfo )
	self:SetHealth( self:Health() - dmginfo:GetDamage() )
	
	if self:Health() <= 0 then
	
		-- Creating 4-6 fires.
		for i = 1, math.random(4,6) do
			local pos = self:GetPos()
	
			local effectdata = EffectData()
			effectdata:SetStart( pos + Vector(math.random(-200, 200), math.random(-200, 200), 0) )
			effectdata:SetOrigin( pos + Vector(math.random(-200, 200), math.random(-200, 200), 0) )
			effectdata:SetScale( 1 )
			util.Effect( "HelicopterMegaBomb", effectdata )
			
			timer.Simple(0.3, function()
				local IncendiaryFire = ents.Create("fire")
				IncendiaryFire:SetPos(pos + Vector(math.random(-200, 200), math.random(-200, 200), 0))
				IncendiaryFire:Spawn()
			end)
		end
		
		self:Remove()
	end
end