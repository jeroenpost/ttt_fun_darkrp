AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self.Entity:SetModel("models/weapons/w_eq_flashbang.mdl")
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Entity:SetCollisionGroup( COLLISION_GROUP_PROJECTILE )
	
	local phys = self:GetPhysicsObject()
	
	if phys:IsValid() then
		phys:Wake()
		phys:EnableGravity( true )
	end
end

function ENT:PhysicsCollide( data )
	local pos = self.Entity:GetPos()
	
	local effectdata = EffectData()
	effectdata:SetStart( pos )
	effectdata:SetOrigin( pos )
	effectdata:SetScale( 1 )
	util.Effect( "HelicopterMegaBomb", effectdata )
	
	-- Creating 4-6 fires.
	for i = 1, math.random(4,6) do
		local IncendiaryFire = ents.Create("fire")
		IncendiaryFire:SetPos(pos + Vector(math.random(-200, 200), math.random(-200, 200), 0))
		IncendiaryFire:Spawn()
	end
	
	self.Entity:Remove()
end 