AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function FIRE_TruckNPC_Spawn()	
	if not file.IsDir("craphead_scripts", "DATA") then
		file.CreateDir("craphead_scripts", "DATA")
	end
	
	if not file.IsDir("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."", "DATA") then
		file.CreateDir("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."", "DATA")
	end
	
	if not file.Exists( "craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/firetrucknpc_location.txt", "DATA" ) then
		file.Write("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/firetrucknpc_location.txt", "0;-0;-0;0;0;0", "DATA")
	end
	
	local PositionFile = file.Read("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/firetrucknpc_location.txt", "DATA")
	 
	local ThePosition = string.Explode( ";", PositionFile )
		
	local TheVector = Vector(ThePosition[1], ThePosition[2], ThePosition[3])
	local TheAngle = Angle(tonumber(ThePosition[4]), ThePosition[5], ThePosition[6])
	
	local FiretruckNPC = ents.Create("npc_firetruck")
	FiretruckNPC:SetModel(FIRETRUCK_NPCModel)
	FiretruckNPC:SetPos(TheVector)
	FiretruckNPC:SetAngles(TheAngle)
	FiretruckNPC:Spawn()
	FiretruckNPC:SetMoveType(MOVETYPE_NONE)
	FiretruckNPC:SetSolid( SOLID_BBOX )
	FiretruckNPC:SetCollisionGroup(COLLISION_GROUP_PLAYER)

end
--timer.Simple(1, FIRE_TruckNPC_Spawn)

function FIRE_TruckNPC_Position( ply )
	if ply:IsAdmin() then
		local HisVector = string.Explode(" ", tostring(ply:GetPos()))
		local HisAngles = string.Explode(" ", tostring(ply:GetAngles()))
		
		file.Write("craphead_scripts/fire_system/".. string.lower(string.lower(game.GetMap())) .."/firetrucknpc_location.txt", ""..(HisVector[1])..";"..(HisVector[2])..";"..(HisVector[3])..";"..(HisAngles[1])..";"..(HisAngles[2])..";"..(HisAngles[3]).."", "DATA")
		ply:ChatPrint("New position for the firetruck NPC has been succesfully set. Please restart your server!")
	else
		ply:ChatPrint("Only administrators can perform this action")
	end
end
concommand.Add("firetrucknpc_setpos", FIRE_TruckNPC_Position)

function ENT:AcceptInput(ply, caller)
	if caller:IsPlayer() && !caller.CantUse then
		caller.CantUse = true
		timer.Simple(3, function()  caller.CantUse = false end)

		if caller:IsValid() and table.HasValue( FIRE_AllowedTeams, team.GetName(caller:Team()) ) then
			umsg.Start("FIRE_FiretruckMenu", caller)
			umsg.End()
		else
			DarkRP.notify(caller, 2, 5,  "Only firefighters can access this NPC!")
		end
	end
end