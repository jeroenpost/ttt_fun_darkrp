AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include("shared.lua")

FIRE_CurrentFires = 0

function ENT:Initialize()
	self:SetMoveType( MOVETYPE_NONE )
	self:SetSolid( SOLID_NONE )
	self:SetAngles( Angle(0, 0, 0) )
	self:SetPos( self:GetPos() + Vector(0, 0, 10) )
	
	FIRE_CurrentFires = FIRE_CurrentFires + 1
	
	self.ExtinguisherLeft = 25
	self.LastDamage = CurTime()
	self.FireHealth = CurTime()
	self.LastSpread = CurTime()
	
	self:DrawShadow( false )
	
	if FIRE_AutoTurnOff > 0 then
		timer.Simple( FIRE_AutoTurnOff, function()
			if self:IsValid() then
				self:KillFire()
			end
		end)
	end
end

function ENT:KillFire() 
	FIRE_CurrentFires = FIRE_CurrentFires - 1
	self:Remove()
end

function ENT:SpreadFire()
	for i = 1, 20 do
		local trace = {}
		trace.start = self:GetPos() + Vector( 0, 0, 10 )
		trace.endpos = self:GetPos() + Vector( math.Rand( -1, 1 ) * math.Rand( 50, 100 ), math.Rand( -1, 1 ) * math.Rand( 50, 100 ), 50 )
		trace.filter = self.Entity
		trace.mask = MASK_OPAQUE
		
		local traceline1 = util.TraceLine( trace )
	
		local trace2 = {}
		trace2.start = self:GetPos() + Vector( math.Rand( -1, 1 ) * math.Rand( 50, 100 ), math.Rand( -1, 1 ) * math.Rand( 50, 100 ), 50 )
		trace2.endpos = self:GetPos() + Vector( 0, 0, 10 )
		trace2.filter = self.Entity
		trace2.mask = MASK_OPAQUE
			
		local traceline2 = util.TraceLine( trace2 )
		
		if ( traceline1.Hit and traceline2.Hit ) or ( not traceline1.Hit and not traceline2.Hit ) then
		
			local trstart = self:GetPos() + Vector( math.Rand( -1, 1 ) * math.Rand( 50, 100 ), math.Rand( -1, 1 ) * math.Rand( 50, 100 ), 50)
			local trend = trstart - Vector( 0, 0, 100 )
			
			local trace = {}
			trace.start = trstart
			trace.endpos = trend
			trace.filter = self.Entity
				
			local TraceResults = util.TraceLine( trace )
				
			if TraceResults.HitWorld then
				if util.IsInWorld( TraceResults.HitPos ) then
					if table.HasValue( FIRE_MaterialTypes, TraceResults.MatType ) then
									
						local NearOtherFire = false
						
						for k, v in pairs( ents.FindInSphere( TraceResults.HitPos, 50) ) do
							if v:GetClass() == "fire" then
								NearOtherFire = true
							end
						end
						
						if !NearOtherFire then
							if FIRE_CurrentFires >= FIRE_MaxFires then return false end
							local NewFire = ents.Create( "fire" )
							NewFire:SetPos( TraceResults.HitPos )
							NewFire:Spawn()
							return
						end
					end
				end
			end
		end
	end
end

function ENT:ExtinguishAttack( ply, hose )
	if hose then
		self.ExtinguisherLeft = self.ExtinguisherLeft - math.random(3, 5)
	else
		self.ExtinguisherLeft = self.ExtinguisherLeft - math.random(1, 3)
	end
	
	if self.ExtinguisherLeft <= 0 then
		if ply:IsValid() then
			if table.HasValue( FIRE_AllowedTeams, team.GetName(ply:Team()) ) then
				ply:addMoney( FIRE_ExtinguishPay )
                ply:addXP( 250 )
				if FIRE_NotifyOnExtinguish then
					DarkRP.notify(ply, 2, 5,  "You've been given $".. FIRE_ExtinguishPay .." and 250XP for extinguishing fire.")
				end
			else
				ply:addMoney( FIREEXTCITZ_ExtinguishPay )
				if FIRE_NotifyOnExtinguish then
					DarkRP.notify(ply, 2, 5,  "You've been given $".. FIREEXTCITZ_ExtinguishPay .." for extinguishing fire as a non-firefighter.")
				end
			end
		end
		
		self:KillFire()
	end
end

function ENT:Think()
	if self:WaterLevel() > 0 then 
		self:KillFire() 
		return 
	end
	
	local trace = {}
	trace.start = self:GetPos()
	trace.endpos = self:GetPos() + Vector(0, 0, 500)
	trace.mask = MASK_VISIBLE
	trace.filter = self
	
	local tr = util.TraceLine(trace)
	
	if self.FireHealth + 10 < CurTime() then 
		self.FireHealth = CurTime() 
		self.ExtinguisherLeft = math.Clamp(self.ExtinguisherLeft + 1, 0, 120) 
	end

	if self.LastSpread + FIRE_SpreadInterval < CurTime() then
		self:SpreadFire()
			
		self.LastSpread = CurTime()
	end
	
	if self.LastDamage + FIRE_DamageInterval < CurTime() then
		self.LastDamage = CurTime()
		
		for k , v in pairs(ents.FindInSphere(self:GetPos(), math.random(35, 100))) do
			if v:GetClass() == "prop_physics" then
				if not v:IsOnFire() then
					--[[v:Ignite(60, 100)
					timer.Simple(FIRE_RemovePropTimer, function()
						if IsValid(v) then
							if v:IsOnFire() then
								local effectdata = EffectData()
								effectdata:SetOrigin( v:GetPos() )
								effectdata:SetMagnitude( 2 )
								effectdata:SetScale( 2 )
								effectdata:SetRadius( 3 )
								util.Effect( "Sparks", effectdata )
								v:Remove()
							end
						end
					end)]]--
				end
				
				v:SetColor( FIRE_BurntPropColor )
			elseif v:IsPlayer() and v:Alive() and v:GetPos():Distance(self:GetPos()) < 70 then
				if table.HasValue( FIRE_AllowedTeams, team.GetName(v:Team()) ) then
					v:TakeDamage( FIRE_FireFighterDamage, v )
				else
					v:TakeDamage( FIRE_FireDamage, v )
				end
			elseif v:IsVehicle() and v:GetPos():Distance(self:GetPos()) < 70 then
				v:TakeDamage( FIRE_VehicleDamage )
			end
		end
	end
end