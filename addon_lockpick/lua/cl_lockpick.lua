AddCSLuaFile()
function Lockpick.DrawLockpickPreview()
	txt=(LocalPlayer():GetEyeTrace().Entity:GetPos():Distance(LocalPlayer():GetPos())>100 and Lockpick.Config.TooFarMessage) or (LocalPlayer():Crouching() and Lockpick.Config.PressEMessage) or Lockpick.Config.CrouchMessage
	txt=txt.." ("..LocalPlayer():GetLockpickCount().." picks remaining.)"
	surface.SetTextColor(255, 255, 255, Lockpick.DrawOpacity)
	surface.SetFont("Lockpick")
	local x, y=surface.GetTextSize(txt)
	surface.SetTextPos(ScrW()/2-x/2, ScrH()-ScrH()/4-y/2)
	surface.DrawText(txt)
end

Lockpick.Bottom = Material ("lockpick/lock_bottom.png", "nocull")
Lockpick.Top = Material ("lockpick/lock_top.png", "nocull")
Lockpick.PickMat = Material ("lockpick/pick_1.png", "nocull")
function Lockpick.DrawLockpickMinigame()
	if !LocalPlayer():Alive() or (LocalPlayer():GetPos():Distance(LocalPlayer().LockpickEnt:GetPos())>100) then
		LocalPlayer().LockpickEnt = nil
	end
	local mX, mY=gui.MousePos()
	local cX, cY=ScrW()/2, ScrH()/1.3
	local r=ScrW()/4
	Lockpick.CompPerc=Lockpick.CompPerc or 0
	Lockpick.Health=(Lockpick.Health or Lockpick.Config.BaseHealth)-FrameTime()/2
	if Lockpick.Health<=0 then
		RunConsoleCommand("__playLockpickSound", 5)
		RunConsoleCommand('__pickbroken')
		Lockpick.Picking=false
		Lockpick.PickPos=nil
		Lockpick.Health=Lockpick.Config.BaseHealth
	end
	Lockpick.PickAngle=math.deg(math.atan2(mX-cX, cY-mY))
	if Lockpick.PickAngle<0 then
		Lockpick.PickAngle=360+Lockpick.PickAngle
	end

	Lockpick.NextSoundPlay=Lockpick.NextSoundPlay or CurTime()
	if !Lockpick.HasSoundPlayed and Lockpick.Health<40 then
		RunConsoleCommand("__playLockpickSound", 6)
		Lockpick.HasSoundPlayed=true
	elseif Lockpick.Health>40 then
		Lockpick.HasSoundPlayed=false
	end
	local offset=0
	if !Lockpick.Picking then
		if input.IsMouseDown(MOUSE_LEFT) then
			Lockpick.Picking=true
			Lockpick.PickPos=Lockpick.PickAngle
			RunConsoleCommand("__playLockpickSound", 1)
		end
	else
		Lockpick.OldAng=Lockpick.OldAng or Lockpick.PickAngle
		Lockpick.Health=Lockpick.Health-((math.abs(Lockpick.PickAngle-Lockpick.OldAng))*(Lockpick.CompPerc or 0)*Lockpick.Config.TensionDamage)
		offset=(math.Rand(-20,20)*(math.cos(math.Rand(-6,6))*((((Lockpick.PickAngle-Lockpick.PickPos)/360))*((Lockpick.PickPos-Lockpick.TargetAng)/360))))*Lockpick.Config.ShakeScale
		surface.SetDrawColor(255, 255, 255, 128)
		//surface.DrawLine(cX, cY, cX+r/2*math.cos(math.rad(Lockpick.PickPos-90)), cY+r/2*math.sin(math.rad(Lockpick.PickPos-90)))
		Lockpick.Health=Lockpick.Health-(1-Lockpick.CompPerc)
		if (Lockpick.PickAngle-Lockpick.PickPos)>0 and math.abs(Lockpick.PickAngle-Lockpick.PickPos)>=Lockpick.Config.AngleToComplete then
			if math.abs(Lockpick.PickPos-Lockpick.TargetAng)<Lockpick.Config.AngleTolerance then
				RunConsoleCommand('_lockpickSuccess', math.floor(Lockpick.TargetAng))
			end
		else
			Lockpick.PickAngle=math.Clamp(Lockpick.PickAngle, Lockpick.PickPos-((90)-((Lockpick.PickPos or 0)-(Lockpick.PickPos or 0)+(math.abs((Lockpick.PickPos or 0)-(Lockpick.TargetAng))/1.9))/6)*Lockpick.Config.AngleToComplete/90, Lockpick.PickPos+((90)-((Lockpick.PickPos or 0)-(Lockpick.PickPos or 0)+(math.abs((Lockpick.PickPos or 0)-(Lockpick.TargetAng))/1.9))/6)*Lockpick.Config.AngleToComplete/90)
			Lockpick.DrawAngle=Lockpick.PickAngle-90
		end
		if !input.IsMouseDown(MOUSE_LEFT) then
			Lockpick.Picking=false
			Lockpick.PickPos=nil
		end
	end
	Lockpick.DrawAngle=Lockpick.PickAngle-90+(offset)
	surface.SetDrawColor(255, 255, 255, 255)
	surface.SetMaterial(Lockpick.Bottom)
	surface.DrawTexturedRectRotated(cX, cY, r, r, 360)
	
	Lockpick.CompPerc=math.pow((math.abs(((1)-math.abs((Lockpick.PickPos or Lockpick.PickAngle)-Lockpick.TargetAng)/180)*1)), Lockpick.Config.LockExponent)
	surface.SetMaterial(Lockpick.Top)
	local ang=math.Clamp((math.max(360-(math.Clamp(Lockpick.PickAngle-(Lockpick.PickPos or Lockpick.PickAngle), 0, 90))*2+offset, 360-90)-360)*(45/Lockpick.Config.AngleToComplete)*Lockpick.CompPerc, -90, 90)
	if Lockpick.PickPos and math.abs(Lockpick.PickPos-Lockpick.TargetAng)>Lockpick.Config.AngleTolerance then
		ang=math.Clamp(ang, -85*Lockpick.CompPerc, 85*Lockpick.CompPerc)
		Lockpick.Health=Lockpick.Health-FrameTime()*2
	end
	
	surface.DrawTexturedRectRotated(cX, cY, r, r, ang)
	
	if Lockpick.Config.DebugMode then
		surface.DrawLine(cX, cY, cX+r/2*math.cos(math.rad(Lockpick.DrawAngle)), cY+r/2*math.sin(math.rad(Lockpick.DrawAngle)))
		if Lockpick.PickPos then
			surface.SetDrawColor(255, 255, 255, 128)
			surface.DrawLine(cX, cY, cX+r/2*math.cos(math.rad(Lockpick.PickPos-90)), cY+r/2*math.sin(math.rad(Lockpick.PickPos-90)))
		end
	end
	surface.SetDrawColor(255, 255, 255, 255)
	surface.SetMaterial(Lockpick.PickMat)
	surface.DrawTexturedRectRotated(cX, cY, r*1.5, r*1.5, ang*0.9)
	surface.DrawTexturedRectRotated(cX, cY, r*1.5, r*1.5, 360-Lockpick.DrawAngle+45)
	
	if Lockpick.Config.DebugMode then
		surface.SetDrawColor(255, 0, 0, 255)
		surface.DrawLine(cX, cY, cX+r/2*math.cos(math.rad(Lockpick.TargetAng-90)), cY+r/2*math.sin(math.rad(Lockpick.TargetAng-90)))
		txt="DEBUG: "
		local pa=Lockpick.PickAngle
		txt=txt.."Angle="..tostring(math.floor(pa))
		txt=txt.."; Pos="..tostring(math.floor(Lockpick.PickPos or pa))
		txt=txt.."; Target Angle="..Lockpick.TargetAng
		local lOpp=Lockpick.PickPos
		Lockpick.PickPos=Lockpick.PickPos or Lockpick.PickAngle
		txt=txt.."; TensionDamage="..((math.abs(Lockpick.PickAngle-Lockpick.OldAng))*(Lockpick.CompPerc or 0)*Lockpick.Config.TensionDamage)
		Lockpick.PickPos=lOpp
		local x, y=surface.GetTextSize(txt)
		surface.SetTextPos(200, ScrH()-ScrH()/4-y/2-r/2)
	else
		txt="Press E to stop lockpicking"
		txt=txt.." ("..LocalPlayer():GetLockpickCount().." picks remaining.)"
		local x, y=surface.GetTextSize(txt)
		surface.SetTextPos(ScrW()/2-x/2, ScrH()-ScrH()/4-y/2-r/2)
	end
	if Lockpick.PickAngle<Lockpick.TargetAng+Lockpick.Config.AngleTolerance*2 and Lockpick.PickAngle>Lockpick.TargetAng-Lockpick.Config.AngleTolerance*2 then
		if math.Rand(-200, 200)>=199 then
			RunConsoleCommand("__playLockpickSound", 3)
		end
	end
	surface.SetTextColor(255, 255, 255, Lockpick.DrawOpacity)
	surface.SetFont("Lockpick")
	surface.DrawText(txt)
	Lockpick.OldAng=Lockpick.PickAngle
end

function DrawBreakNotification()
	Lockpick.BreakTime=Lockpick.BreakTime or CurTime()
	local del=CurTime()-Lockpick.BreakTime
	local txt="You have broken your last lockpick."
	if del<Lockpick.Config.BreakNotificationLength then
		surface.SetTextColor(255, 255, 255, math.min((del/(Lockpick.Config.BreakNotificationLength/4)), 1)*255)
	else
		surface.SetTextColor(255, 255, 255, 255-math.min(((del-Lockpick.Config.BreakNotificationLength)/(Lockpick.Config.BreakNotificationLength/4)), 1)*255)
	end
	surface.SetFont("Lockpick")
	local x, y=surface.GetTextSize(txt)
	surface.SetTextPos(ScrW()/2-x/2, ScrH()-ScrH()/4-y/2)
	surface.DrawText(txt)
	if del>10 then
		Lockpick.DrawBreakNotif=nil
		Lockpick.BreakTime=nil
	end
end