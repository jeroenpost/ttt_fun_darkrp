local mEnt=FindMetaTable('Entity')
mEnt.IsDoor=mEnt.isDoor or mEnt.IsDoor or function(self)
	if !IsValid(self) then return false end
	return string.find(self:GetClass(), "door")
end

mEnt.oldFire=mEnt.oldFire or mEnt.Fire

function mEnt:Fire(inp, param, delay, ...)
	if inp=="lock" then
		hook.Call('EntityLocked', GM, self)
	elseif inp=="unlock" then
		hook.Call('EntityUnlocked', GM, self)
	end
	self:oldFire(inp, param, delay, ...)
end

function mEnt:IsPickableVehicle()
	return self:IsVehicle()
end

function mEnt:IsLocked()
	if !IsValid(self) then return false end
	if self:IsDoor() or self:IsPickableVehicle() then
		if self.Locked==false then return false end
		self.Locked=self.Locked or (self:IsDoor() and self:HasSpawnFlags(2048)) or false
		return self.Locked
	elseif Lockpick.Config.CustomEnts[self:GetClass()] then
		return Lockpick.Config.CustomEnts[self:GetClass()]:IsLocked(self)
	end
	return false
end

function mEnt:Lock(b)
	if !IsValid(self) then return false end
	if !b then
		if self:IsDoor() then
			self:Fire("lock")
		elseif Lockpick.Config.CustomEnts[self:GetClass()] then
			Lockpick.Config.CustomEnts[self:GetClass()]:Lock(self)
		else
			return
		end
	end
	self.Locked=true
	SendUserMessage('doorLock', nil, self, self.Locked)
end

function mEnt:Unlock(b)
	if !IsValid(self) then return false end
	if !b then
		if self:IsDoor() or self:IsPickableVehicle() then
			self:Fire("unlock")
		elseif Lockpick.Config.CustomEnts[self:GetClass()] then
			Lockpick.Config.CustomEnts[self:GetClass()]:Unlock(self)
		else
			return
		end
	end
	self.Locked=false
	SendUserMessage('doorLock', nil, self, self.Locked)
end

function mEnt:IsPickable(ply)
	if !IsValid(self) then return false end
	if self:IsDoor() or self:IsPickableVehicle() then
		self.PickLevel=self.PickLevel or 1
		if self:IsLocked() and !(DarkRP and (((Lockpick.Config.BlockUnownableDoors and self.getKeysNonOwnable and self:getKeysNonOwnable())) or (!self.DoorData or self.DoorData.NonOwnable))) then
			return self.PickLevel<5
		end
	elseif Lockpick.Config.CustomEnts[self:GetClass()] then
		return Lockpick.Config.CustomEnts[self:GetClass()]:IsPickable(self, ply) and self:IsLocked()
	end
	return false
end

function mEnt:SetPickLevel(i)
	self.PickLevel=i
	if SERVER then
		SendUserMessage('setDoorPickLevel', nil, self, i)
	end
end

hook.Add('RenderScreenspaceEffects', 'doSSAO', function()
	local txt="asdf"
	surface.SetDrawColor(255, 255, 255, 255)
	//surface.DrawRect(0, 0, ScrW(), ScrH())
	surface.SetTextColor(255, 255, 255, 0)
	surface.SetFont("Lockpick")
	local x, y=surface.GetTextSize(txt)
	surface.SetTextPos(ScrW()/2, ScrW()/2)
	surface.DrawText(txt)
end)

hook.Add('EntityTakeDamage', 'UnlockFromHandle', function(ent, dmginfo)
	if Lockpick.Config.EnableHandleShooting and ent:IsDoor() and ent:LookupBone('handle') then
		local pos, ang=ent:GetBonePosition(ent:LookupBone('handle'))
		if pos:Distance(dmginfo:GetDamagePosition())<5 then
			if ent:IsLocked() then
				ent.HandleHealth=(ent.HandleHealth or Lockpick.Config.MaxHandleHealth)-dmginfo:GetDamage()
				if ent.HandleHealth<=0 then
					ent.HandleHealth=nil
					ent:Fire("unlock")
					ent:Fire('open')
				end
			else
				ent:Fire('open')
			end
		end
	end
end)

hook.Add('EntityLocked', 'resetLockDamage', function(ent)
	ent.HandleHealth=nil
end)