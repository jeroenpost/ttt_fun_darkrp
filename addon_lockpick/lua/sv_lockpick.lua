// Concommands
concommand.Add('__pickbroken', function(ply)
	ply:SetLockpickCount(math.floor(math.max(ply:GetLockpickCount()-1, 0)))
	hook.Call('OnPickBroken', GM, ply)
	if ply:GetLockpickCount()<=0 then
		hook.Call('OnLastPickBroken', GM, ply)
		ply:EndLockpick()
		if !Lockpick.Config.NotificationFunction(ply, "You have broken your last lockpick.") then
			if DarkRP and Lockpick.Config.UseDarkRPNotifications  then
				DarkRP.notify(ply, 1, Lockpick.Config.BreakNotificationLength, "You have broken your last lockpick.")
			else
				SendUserMessage("drawBreakNotif", ply)
			end
		end
	end
end)
concommand.Add('beginlockpick', function(ply, cmd, args)
	if !ply:IsLockpicking() and ply:Crouching() and IsValid(ply:GetEyeTrace().Entity) and ply:GetEyeTrace().Entity:GetPos():Distance(ply:GetPos())<100  then
		for f, v in pairs(player.GetAll()) do
			if v.LockpickEnt then
				if ply:GetEyeTrace().Entity==v.LockpickEnt then return end
			end
		end
		ply:BeginLockpick(ply:GetEyeTrace().Entity, math.random(Lockpick.Config.AngleToComplete, 360-Lockpick.Config.AngleToComplete))
		ply.oWalkSpeed=ply:GetWalkSpeed()
		ply.oRunSpeed=ply:GetRunSpeed()
		ply:SetWalkSpeed(1)
		ply:SetRunSpeed(1)
	elseif ply:IsLockpicking() then
		ply:EndLockpick()
	end
end)
concommand.Add('endlockpick', function(ply, cmd, args)
	hook.Call('OnEndLockpick', GM, ply)
	if ply.oWalkSpeed then
		ply:EndLockpick()
	end
end)
concommand.Add('__playLockpickSound', function(ply, cmd, args)
	if ply:IsLockpicking() then
		local snd=tonumber(args[1])
		ply:PlayLockpickSound(snd)
	end
end)
concommand.Add('_lockpickSuccess', function(ply, cmd, args)
	local ang=math.floor(tonumber(args[1]))
	if ang==math.floor(ply.LockpickTargetAng) then
		local ent=ply.LockpickEnt
		if IsValid(ent) then
			if ent:IsDoor() or ent:IsPickableVehicle() then
                ent.VC_Locked = false
				ent:Unlock()
				ent:Fire("open")
			elseif Lockpick.Config.CustomEnts[ent:GetClass()] then
				Lockpick.Config.CustomEnts[ent:GetClass()]:OnPicked(ply, ent)
			end
			hook.Call("OnLockpickSuccess", GAMEMODE or GM, ply, ent)
			ply:PlayLockpickSound(4)
			ply:EndLockpick()
		end
	end
end)
// DATA
DATAMODE_TEXT=true
Lockpick.Database={}

print('Loading data extension...')
function Lockpick.Database.Prepare()
	if DATAMODE_TEXT then
		if !von then
			von={}
			von.serialize=glon.encode
			von.deserialize=glon.decode
		end
	else
		print('Only text mode ready')
		DATAMODE_MYSQL = false
		DATAMODE_SQLITE = false
		DATAMODE_TEXT = false
	end
end

function Lockpick.Database.GenPlayerData(ply)
	local playerData={
		Lockpicks=ply:GetLockpickCount()
	}
	playerData.Nick=ply:Nick()
	playerData.SteamID=ply:SteamID()
	return playerData
end

function Lockpick.Database.SetPlayerData(str, ply)
	local dat=von.deserialize(str)
	ply:SetLockpickCount(dat.Lockpicks)
	for f, v in pairs(player.GetAll()) do
		v:SetLockpickCount(v:GetLockpickCount(), ply)
	end
end

function Lockpick.Database.SavePlayerData(ply)
	Lockpick.Database.Prepare()
	if DATAMODE_TEXT then
		local playerData=Lockpick.Database.GenPlayerData(ply)
		if !file.IsDir(Lockpick.Config.SaveDirectory, "DATA") then
			file.CreateDir(Lockpick.Config.SaveDirectory)
		end
		if !file.IsDir(Lockpick.Config.SaveDirectory.."/", "DATA") then
			file.CreateDir(Lockpick.Config.SaveDirectory.."/")
		end
		local str=von.serialize(playerData)
		file.Write(Lockpick.Config.SaveDirectory.."/".."/"..string.gsub(ply:SteamID(), ":", "_")..".txt", str)
	end
end

function Lockpick.Database.LoadPlayerData(ply)
	Lockpick.Database.Prepare()
	if DATAMODE_TEXT then
		if !file.IsDir(Lockpick.Config.SaveDirectory, "DATA") then
			file.CreateDir(Lockpick.Config.SaveDirectory)
		end
		if !file.IsDir(Lockpick.Config.SaveDirectory.."/", "DATA") then
			file.CreateDir(Lockpick.Config.SaveDirectory.."/")
		end
		local str=file.Read(Lockpick.Config.SaveDirectory.."/".."/"..string.gsub(ply:SteamID(), ":", "_")..".txt", "DATA")
		if str then
			Lockpick.Database.SetPlayerData(str, ply)
		end
	end
	ply.EventLevel=ply.EventLevel or 0
end

hook.Add('ShutDown', 'savelockpickies', function()
	for f, v in pairs(player.GetAll()) do
		Lockpick.Database.SavePlayerData(v)
		//print("Saving", v)
	end
end)

hook.Add('PlayerDisconnected', 'printInvAndStuff', function(ply)
	/*print(ply, ply:GetPos(), ply:EyeAngles())
	PrintTable(ply.lockpicky)*/
	if Lockpick.Database then
		Lockpick.Database.SavePlayerData(ply)
	end
end)

hook.Add('PlayerInitialSpawn', 'loadlockpickyAndStuff', function(ply)
	Lockpick.Database.LoadPlayerData(ply)
end)