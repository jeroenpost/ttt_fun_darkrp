local mPly=FindMetaTable('Player')

function mPly:CanLockpick()
	return (self:GetLockpickCount()>0 or Lockpick.Config.DebugMode) and !table.HasValue(Lockpick.Config.RestrictedClasses, self:Team()) and !(Lockpick.Config.DisableJailPicks and self.isArrested and self:isArrested())
end

function mPly:PlayLockpickSound(n)
	if IsValid(self) and self:IsLockpicking() and Lockpick.PickingSounds[n] then
		self:EmitSound(Lockpick.PickingSounds[n],  75, 100)
	end
end

function mPly:SetLockpickCount(n)
	if SERVER then Lockpick.Database.SavePlayerData(self) end
	local b = hook.Call("PlayerSetLockpickCount", GM, self, n)
	if b then return b end
	return self:SetNWInt("lockpicks", math.max(math.floor(n)), 0)
end

function mPly:GetLockpickCount()
	local c = self:GetNWInt("lockpicks") or 0;
	local v = Lockpick.Config.GetLockpickCountOverride(self, c) or c
	
	local b = hook.Call("PlayerGetLockpickCount", GM, self, v)
	if b then return b end
	
	return v;
end

function mPly:BeginLockpick(ent, int)
	self.LockpickEnt=ent
	if SERVER then
		SendUserMessage('playerLockpicking', nil, self, self:GetEyeTrace().Entity, int)
		self.LockpickTargetAng=int
		    local owner = self:GetEyeTrace().Entity:isKeysOwnedBy(self)
		     if not owner then
                 hook.Call("addToDamageLog", GAMEMODE, "LOCKPICK: "..self:Nick().." ("..self:SteamID()..") started lockpicking")
            end
	elseif CLIENT then
		if self==LocalPlayer() then
			RunConsoleCommand('+duck')
			gui.EnableScreenClicker(true)
			Lockpick.TargetAng=int
		end
	end
end

function mPly:EndLockpick()
	self.LockpickEnt=nil
	if SERVER then
		self:SetWalkSpeed(self.oWalkSpeed or 220)
		self:SetRunSpeed(self.oRunSpeed or 300)
		SendUserMessage('endPlayerLockpicking', nil, self)
	end
	if CLIENT then
		Lockpick.Health=Lockpick.Config.BaseHealth
		Lockpick.PickPos=nil
		Lockpick.Picking=false
		if self==LocalPlayer() then
			RunConsoleCommand('-duck')
			gui.EnableScreenClicker(false)
		end
	end
end

function mPly:IsLockpicking()
	return (IsValid(self.LockpickEnt))
end

hook.Add("PlayerSpawn", 'removeLockpicks', function(ply)
	if Lockpick.Config.RemoveOnDeath then
		ply:SetLockpickCount(0)
	end
end)