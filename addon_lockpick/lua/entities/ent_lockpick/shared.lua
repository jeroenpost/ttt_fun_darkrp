ENT.Base = "base_entity"
ENT.Type = "anim"
AddCSLuaFile()

ENT.PrintName		= "Lockpick"
ENT.Author			= "shadowndacorner"
ENT.Contact			= "shadowndacorner"
ENT.Purpose			= "To pick locks"
ENT.Instructions	= "Press E to pick up, then crouch and press E to pick locks."
ENT.RenderGroup		= RENDERGROUP_OPAQUE

--[[---------------------------------------------------------
   Name: Initialize
   Desc: First function called. Use to set up your entity
-----------------------------------------------------------]]
function ENT:SetupDataTables()
	self:DTVar("Int", 0, "ItemID")
end

if CLIENT then
	function ENT:Draw()
		self:DrawModel()
	end
end

function ENT:Think()
end

function ENT:Initialize()
	if SERVER then
		self:SetModel("models/props_c17/TrapPropeller_Lever.mdl")
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		self:SetUseType(SIMPLE_USE)
		local phys = self:GetPhysicsObject()
		phys:Wake()
	end
end

function ENT:Use(ply, call)
	if IsValid(self) then
		ply:SetLockpickCount(ply:GetLockpickCount()+1)
		if DarkRP and !Lockpick.Config.NotificationFunction(ply, "You have picked up a lockpick ("..ply:GetLockpickCount().." picks total).") then
			if GAMEMODE.Notify then
				GAMEMODE:Notify(ply, 4, Lockpick.Config.BreakNotificationLength, "You have picked up a lockpick ("..ply:GetLockpickCount().." picks total).")
			else
				DarkRP.notify(ply, 4, Lockpick.Config.BreakNotificationLength, "You have picked up a lockpick ("..ply:GetLockpickCount().." picks total).")
			end
		end
		self:Remove()
	end
end

--[[---------------------------------------------------------
   Name: KeyValue
   Desc: Called when a keyvalue is added to us
-----------------------------------------------------------]]
function ENT:KeyValue( key, value )
end

--[[---------------------------------------------------------
   Name: Think
   Desc: Entity's think function. 
-----------------------------------------------------------]]
function ENT:Think()
	
end

--
--   Name: OnRemove
--   Desc: Called just before entity is deleted
--
function ENT:OnRemove()
end

--
--	UpdateTransmitState
--
function ENT:UpdateTransmitState()
	return TRANSMIT_PVS
end