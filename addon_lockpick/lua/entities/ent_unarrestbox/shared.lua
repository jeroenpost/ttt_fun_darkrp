AddCSLuaFile()
ENT.Type = "anim"  
ENT.Base = "base_gmodentity"     
ENT.PrintName		= "Unarrest Box" 
ENT.Author			= "shadowndacorner"
ENT.Contact			= ""  
ENT.Purpose			= ""  
ENT.Instructions	= "Unarrest people."

ENT.Spawnable			= false
ENT.AdminSpawnable		= true

function ENT:Initialize()     
	self:SetModel( "models/items/ammocrate_smg1.mdl" )  
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	if SERVER then
		self:SetUseType(SIMPLE_USE)
	end
	local phys = self.Entity:GetPhysicsObject()  
	phys:Wake()  
	self:SetMoveType( MOVETYPE_NONE )
	phys:SetMass( 200 )
	if !DarkRP then self:Remove() end
end

function ENT:Unarrest(ply)
	local ang=ply:EyeAngles()
	local pos=ply:GetPos()
	local mdl=ply:GetModel()
	local paw=ply.PreArrestWeapons
	local paa=ply.PreArrestAmmo
	ply:unArrest()
	ply:wanted(NULL, "Escaping from jail!")
	timer.Simple(0.1+FrameTime(), function()
		ply:SetPos(pos)
		ply:SetEyeAngles(ang)
		ply:SetModel(mdl)
		ply:StripWeapons()
		for f, v in pairs(paw) do
			ply:Give(v)
		end
		for f, v in pairs(paa) do
			ply:SetAmmo(v, f)
		end
		ply.oWalkSpeed = ply.PreArrestWS
		ply.oRunSpeed = ply.PrearrestRS
		timer.Simple(0.5, function()
			ply:SetWalkSpeed(ply.PreArrestWS)
			ply:SetRunSpeed(ply.PreArrestRS or ply:GetWalkSpeed()*2)
			ply:SprintEnable(true)
			ply:SetLockpickCount(ply.PreArrestLockpicks + ply:GetLockpickCount())
			
			ply.PreArrestWS=nil
			ply.PreArrestRS=nil
			ply.PreArrestLockpicks=nil
		end)
	end)
end

hook.Add('playerArrested', 'GetAllWeapons', function(ply, time, enf)
	if Lockpick.Config.EnableBreakout then
		ply.PreArrestWS=ply:GetWalkSpeed()
		ply.PreArrestRS=ply:GetRunSpeed()
		ply.PreArrestWeapons={}
		ply.PreArrestAmmo={}
		local picksToKeep = math.min(ply:GetLockpickCount(), Lockpick.Config.JailPicks)
		ply.PreArrestLockpicks = ply:GetLockpickCount() - picksToKeep
		ply:SetLockpickCount(picksToKeep)
		for f, v in pairs(ply:GetWeapons()) do
			ply.PreArrestWeapons[f]=v:GetClass()
			ply.PreArrestAmmo[v:GetPrimaryAmmoType()]=ply:GetAmmoCount(v:GetPrimaryAmmoType())
			ply.PreArrestAmmo[v:GetSecondaryAmmoType()]=ply:GetAmmoCount(v:GetPrimaryAmmoType())
		end
	end
end)

hook.Add('playerUnArrested', 'DeleteWeapons', function(ply, time, enf)
	ply.PreArrestWeapons=nil
	ply.PreArrestAmmo=nil
end)

function ENT:Use(ply)
end

function ENT:UpdateTransmitState()
	return TRANSMIT_PVS
end

function ENT:Draw()
	self.Entity:DrawModel()
	cam.Start3D2D(self:GetPos()+self:GetAngles():Up()*self:OBBMaxs().z/2-self:GetAngles():Forward()*16-self:GetAngles():Right()*12, self:GetAngles()+Angle(180, 90, -90), 0.12)
		surface.SetDrawColor(80, 80, 80, 200)
		surface.DrawRect(11, 0, (23/0.12), 10/0.12)
		
		surface.SetTextColor(255, 255, 255, 255)
		surface.SetFont('Lockpick')
		local txt="Evidence"
		if LocalPlayer():isArrested() then
			txt=txt.." (Lockpick to Unarrest)"
		end
		local x, y=surface.GetTextSize(txt)
		surface.SetTextPos((25/0.12/2)-x/2, 12.5-y/2)
		surface.DrawText(txt)
	cam.End3D2D()
end 

if Lockpick and Lockpick.Config then
	function Lockpick:CreateUnarrestBox()
		if SERVER and !self.Config.DisableJailPicks and self.Config.EnableBreakout and Lockpick.Config.UnarrestBoxPositions[string.lower(game.GetMap())] then
			local ent=ents.Create('ent_unarrestbox')
			local pt=Lockpick.Config.UnarrestBoxPositions[string.lower(game.GetMap())]
			//ent:SetPos(pt.CratePos)
			//ent:SetAngles(pt.CrateAng)
			ent:Spawn()
			ent:Activate()
			
			ent:SetPos(pt.CratePos)
			ent:SetAngles(pt.CrateAng)
		end
	end
	
	game.lpoCleanUpMap=game.lpoCleanUpMap or game.CleanUpMap
	function game.CleanUpMap(...)
		game.lpoCleanUpMap(...)
		timer.Simple(0, function()
			Lockpick:CreateUnarrestBox()
		end)
	end
	
	hook.Add('InitPostEntity', 'CreateUnarrestBox', function()
		Lockpick:CreateUnarrestBox()
	end)
end