// Read the documentation file for help regarding this config.
// Seriously.  Read it.
// If you ask me a question that could be answered by reading the documentation, I will simply tell you to read the documentation.

Lockpick.Config={
	// Vehicles
	EnableVehiclePicking = true;
	RestrictedVehicles = {
		--"enter_a_non_usable_vehicle_class_here_as_shown_below";
		--"prop_vehicle_prisoner_pod";
	};
	
	// Shootable Door Handles
	EnableHandleShooting=true;
	MaxHandleHealth=150;
	
	// Entities
	CustomEnts={
		ent_unarrestbox=
		{
			IsPickable=function(self, ent, ply)
				return ply:isArrested()
			end;
			OnPicked=function(self, ply, ent)
				ent:Unarrest(ply)				// CODERS: This function is set up for DarkRP.  If you're using your own gamemode, you will almost definitely need to modify this function in addons/lockpick/lua/entities/ent_unarrestbox.lua (function ENT:Unarrest(ply))
			end;
			IsLocked=function()
				return true
			end;
			Lock=function()
				
			end;
			Unlock=function()
				
			end
		};
		
		prop_quest_target=						// Compatibility for my (upcoming?) NPC/questing system
		{
			IsPickable=function(self, ent, ply)
				return ent:CanLockpick(ply)
			end;
			OnPicked=function(self, ply, ent)
				ent:OnLockpick(ply)
			end;
			IsLocked=function()
				return true
			end;
			Lock=function()
				
			end;
			Unlock=function()
				
			end
		};


		
	};
	
	// Game/Interface
	
	DefaultFont="Orator Std";
	
	TooFarMessage = "Come closer to begin lockpicking";
	
	PressEMessage = "Press E to begin lockpicking";
	
	CrouchMessage = "Crouch to lockpick";
	
	UseDarkRPNotifications = true;
	
	NotificationFunction=function(ply, text)
		//return true 							// Coders, return true here if you want to override the notifications with your own.
	end;
	
	BreakNotificationLength = 5;
	
	RestrictedClasses={
		//TEAM_RESTRICTME,
		//TEAM_RESTRICTMETOO,
		////etc
       ////TEAM_THIEF, TEAM_MOB, TEAM_BANKROBBER
	};
	
	// Jailbreak system
	
	DisableJailPicks = true;
	
	EnableBreakout=false;
	
	EnableJailPickOverride=true;
	
	JailPicks = 0;
	
	UnarrestBoxPositions={
	
		rp_downtown_v4c_v2=
		{
			CratePos=Vector(-1500, 0, -145);
			CrateAng=Angle(0, 0, 0);
		};
	};
	
	// Lockpicks
	
	RemoveOnDeath=false;
	
	BlockUnownableDoors=true;
	
	BaseHealth=500;
	
	AngleToComplete=30;
	
	AngleTolerance=9;
	
	AngleToleranceMultiplier=
	function(ply, tol, cfg)
		if ply:IsAdmin() then					// Coders, the arguments are (Player, Default tolerance, Config table)
			//return tol*2						// Return what you want the tolerance to be
		end
	end;
	
	GetLockpickCountOverride=
	function(ply, cnt)							//The arguments are (Entity) player, (number) lockpickCount
		//return ply:GetItemCount(�Lockpick�)	// Inventory system example
	end;
	
	LockExponent=1.25;
	
	TensionDamage=1;
	
	ShakeScale=10;
	
	// Data
	
	SaveLockpickData=true;
	SaveDirectory="lockpick_pdata";
}

if Lockpick.Config.DisableJailPicks then Lockpick.Config.EnableBreakout = false end

if CLIENT then
	surface.CreateFont("Lockpick", {			// This way, the font reloads if you modify the config file.
		font=Lockpick.Config.DefaultFont,
		size=(30*ScrW()/1920)
	})
end