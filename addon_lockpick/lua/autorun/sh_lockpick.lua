require('von')
Lockpick=Lockpick or {}
Lockpick.PickingSounds={
	[1]=Sound("physics/metal/weapon_footstep1.wav"),
	[2]=Sound("weapons/357/357_reload3.wav"),
	[3]=Sound("weapons/357/357_reload4.wav"),
	[4]=Sound("weapons/357/357_spin1.wav"),
	[5]=Sound("weapons/slam/mine_mode.wav"),
	[6]=Sound("weapons/357/357_reload1.wav"),
}

Lockpick.Config={	// Config defaults - in case someone messes up, or someone chooses not to update the config file
	EnableVehiclePicking = true;
	RestrictedVehicles = {};
	EnableHandleShooting=true;
	MaxHandleHealth=150;
	CustomEnts={};
	DefaultFont="Orator Std";
	TooFarMessage = "Come closer to begin lockpicking";
	PressEMessage = "Press E to begin lockpicking";
	CrouchMessage = "Crouch to lockpick";
	UseDarkRPNotifications = true;
	NotificationFunction=function(ply, text) end;
	BreakNotificationLength = 5;
	RestrictedClasses={};
	DisableJailPicks = false;
	EnableBreakout=true;
	EnableJailPickOverride=true;
	JailPicks = 15;
	UnarrestBoxPositions={};
	RemoveOnDeath=false;
	BlockUnownableDoors=true;
	BaseHealth=300;
	AngleToComplete=30;
	AngleTolerance=9;
	AngleToleranceMultiplier=
	function(ply, tol, cfg)end;
	GetLockpickCountOverride=
	function(ply, cnt)end;
	LockExponent=1.25;
	TensionDamage=1;
	ShakeScale=10;
	SaveLockpickData=true;
	SaveDirectory="lockpick_pdata";
}

AddCSLuaFile('sh_lockpick_config.lua')
include('sh_lockpick_config.lua')
hook.Add("InitPostEntity", "getConfigTeams", function()
	include('sh_lockpick_config.lua')
end)

AddCSLuaFile()
if SERVER then
	AddCSLuaFile('cl_lockpick.lua')
	AddCSLuaFile('sh_lockpick_player.lua')
	AddCSLuaFile('sh_lockpick_entity.lua')
	include('sv_lockpick.lua')
else
	include('cl_lockpick.lua')
end

include('sh_lockpick_player.lua')
include('sh_lockpick_entity.lua')

print('============Lockpick Mod loaded============')

// Hooks n shit

usermessage.Hook('setDoorPickLevel', function(d)
	local ent=d:ReadEntity()
	local i=d:ReadLong()
	ent.PickLevel=i
end)

hook.Add('EntityLocked', 'SetLockVar', function(ent)
	ent:Lock(true)
end)

hook.Add('EntityUnlocked', 'SetLockVar', function(ent)
	ent.Locked=false
	for f, v in pairs(player.GetAll()) do
		if v.LockpickEnt then
			if ent==v.LockpickEnt then v:EndLockpick() end
		end
	end
	ent:Unlock(true)
end)

usermessage.Hook('doorLock', function(d)
	local ent=d:ReadEntity()
	local b=d:ReadBool()
	ent.Locked=b
end)

if SERVER then
	concommand.Add('___askDoor', function(ply, cmd, args)
		local ent=Entity(tonumber(args[1]))
		if IsValid(ent) then
			SendUserMessage('doorLock', nil, ent, ent:IsLocked())
		end
	end)
end

if CLIENT then
	hook.Add('OnEntityCreated', 'detectLockedeDoors', function(ent)
		if !IsValid(ent) then return end
		if ent:IsDoor() or Lockpick.Config.CustomEnts[ent:GetClass()] then
			RunConsoleCommand('___askDoor', ent:EntIndex())
		end
	end)
end

usermessage.Hook('playerLockpicking', function(d)
	local ent=d:ReadEntity()
	local ent2=d:ReadEntity()
	local int=d:ReadLong()
	if IsValid(ent) and IsValid(ent2) and ent.BeginLockpick then
		ent:BeginLockpick(ent2, int)
	end
end)

usermessage.Hook('endPlayerLockpicking', function(d)
	local ent=d:ReadEntity()
	if IsValid(ent) and ent.EndLockpick then
		ent:EndLockpick()
	end
end)

hook.Add('PreDrawHalos', 'drawLockpickOutline', function()
	if Lockpick.DrawingPreview or Lockpick.DrawingMinigame then
		local ent=LocalPlayer().LockpickEnt
		if !IsValid(ent) then ent=LocalPlayer():GetEyeTrace().Entity end
		local dist=ent:GetPos():Distance(LocalPlayer():EyePos())
		if dist<200 and not ent.IsCar then
			Lockpick.DrawOpacity=255*((200-dist)/200)
			halo.Add({ent}, Color(255, 255, 255, Lockpick.DrawOpacity), 5, 5, 2, true, false)
		else
			Lockpick.DrawOpacity=0
		end
	end
end)

hook.Add('PlayerUse', 'cancelDoors', function(ply, ent)
	if ply:Crouching() and ent:IsDoor() and ent:IsPickable(ply) and ply:CanLockpick() then
		return false
	end
end)

hook.Add('PlayerBindPress', 'detectStartLockpick', function(ply, cmd, bUse)
	local ent=LocalPlayer():GetEyeTrace().Entity
	if cmd=="+use" and ((!Lockpick.UseEnabled and ent:IsPickable(ply)) or IsValid(LocalPlayer().LockpickEnt)) and LocalPlayer():GetEyeTrace().Entity:GetPos():Distance(LocalPlayer():GetPos())<100 and ply:GetLockpickCount()>0 then
		Lockpick.UseEnabled=true
		RunConsoleCommand('beginlockpick')
	end
end)

usermessage.Hook('drawBreakNotif', function()
	Lockpick.DrawBreakNotif=true
end)

hook.Add('HUDPaint', '111aaadrawLockpickOptions', function()	
	if Lockpick.DrawingMinigame and !IsValid(LocalPlayer().LockpickEnt) then
		RunConsoleCommand('endlockpick')
	end
	if LocalPlayer():CanLockpick() then
		Lockpick.DrawingPreview=false
		Lockpick.DrawingMinigame=false
		if !LocalPlayer():KeyDown(IN_USE) then
			Lockpick.UseEnabled=false
		else
			Lockpick.UseEnabled=true
		end
		if IsValid(LocalPlayer().LockpickEnt) or LocalPlayer():GetEyeTrace().Entity:IsPickable(LocalPlayer()) or Lockpick.DrawingMinigame then
			Lockpick.DrawEnt=LocalPlayer():GetEyeTrace().Entity
			if LocalPlayer():IsLockpicking() then
				Lockpick.DrawingMinigame=true
				Lockpick.DrawLockpickMinigame()
				return true
			elseif Lockpick.DrawEnt:IsPickable(LocalPlayer()) then
				Lockpick.DrawingPreview=true
				Lockpick.DrawLockpickPreview()
			end
		end
	else
		Lockpick.DrawingMinigame=false
		Lockpick.DrawingPreview=false
	end
	if Lockpick.DrawBreakNotif then
		DrawBreakNotification()
	end
end)

if SERVER then
	resource.AddFile("materials/lockpick/lock_bottom.png")
	resource.AddFile("materials/lockpick/lock_top.png")
	resource.AddFile("materials/lockpick/pick_1.png")
end

hook.Add("CalcView", '111aaalockpickCalcView', function(ply, pos, ang, fov, zn, zf)
	if Lockpick.DrawingMinigame then
		Lockpick.View=Lockpick.View or {}
		Lockpick.View.origin=Lockpick.View.origin or pos
		Lockpick.View.angles=Lockpick.View.angles or ang
		ang.p=0
		ang.r=0
		local target
		if IsValid(LocalPlayer().LockpickEnt) then
			local pos2, ang2
			if LocalPlayer().LockpickEnt:LookupBone('handle') then
				pos2, ang2=LocalPlayer().LockpickEnt:GetBonePosition(LocalPlayer().LockpickEnt:LookupBone('handle'))
			end
			pos=(pos2 and pos2-ang:Forward()*40) or pos-ang:Forward()*20+Vector(0, 0, 10)-ang:Right()*10
			target=pos
			local tr=util.TraceLine({start=pos, endpos=target, filter=player.GetAll()})
			Lockpick.View.origin=LerpVector(FrameTime()*5, Lockpick.View.origin, tr.HitPos+ang:Forward()*10)
			Lockpick.View.drawviewer=true
			Lockpick.View.angles=ang//Lerp(FrameTime()*5, Lockpick.View.angles, ang)
			return Lockpick.View
		end
	elseif Lockpick.View then
		if Lockpick.View.origin:Distance(pos)>10 then
			Lockpick.View=Lockpick.View or {}
			Lockpick.View.origin=Lockpick.View.origin or pos
			Lockpick.View.angles=Lockpick.View.angles or ang
			ang.p=0
			ang.r=0
			Lockpick.View.origin=LerpVector(FrameTime()*10, Lockpick.View.origin, pos)
			Lockpick.View.drawviewer=true
			Lockpick.View.angles=Lerp(FrameTime()*10, Lockpick.View.angles, ang)
			return Lockpick.View
		else
			Lockpick.View=nil
		end
	end
end)

hook.Add('EntityTakeDamage', 'getOutOfLockpick', function(ply)
	if ply:IsPlayer() and ply:IsLockpicking() then
		ply:EndLockpick()
	end
end)