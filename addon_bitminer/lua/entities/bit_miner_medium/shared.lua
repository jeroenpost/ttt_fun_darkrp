ENT.Type = "anim"
ENT.Base = "base_gmodentity" --We should not need a base so I will just remove this for now
 
ENT.PrintName		= "BITMINER STANDARD"
ENT.Author			= "{CODE BLUE}"
ENT.Contact			= "Via steam(Diamondjack7777 or {CODE BLUE} or 0V3RR1D3)"
ENT.Purpose			= "To be used with {CODE BLUE}'s BITMINER addon"
ENT.Instructions	= "Just purchase it from the F4 menu :D"

ENT.Category = "BITMINER"
ENT.Spawnable = true
ENT.AdminSpawnable = true

ENT.AutomaticFrameAdvance = true

function ENT:Initialize()

	self:SetModel("models/BITMINER/BitMinerMedium.mdl")

	self.health = 75

	self.timer = 0
	self.CanDraw = false
    self.vip = {"vip","vipp","vippp"}


	self:SetMoveType( MOVETYPE_NONE )
	self:SetSolid( SOLID_BBOX )
    self:SetCollisionGroup(COLLISION_GROUP_PLAYER)

    local phys = self:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
	end

	--Used to play the animation and force it to be client side for network performance;
	self:UseClientSideAnimation()

	local sequence = self:LookupSequence( "idle" )
	self:ResetSequence( sequence )

	if(SERVER) then
		
		sound.Add( {
			name = "bitminer_sound_medium",
			channel = CHAN_STATIC,
			volume = 0.8,
			level =65,
			pitch = { 95, 110 },
			sound = "ambient/machines/machine3.wav"
		} )

		self:EmitSound("bitminer_sound_medium", 100, 100, 1, CHAN_AUTO )

	end

end

function ENT:OnRemove()



end

function ENT:SetupDataTables()

	self:NetworkVar("Int" , 0 , "MinedCoins")
    self:NetworkVar("Int" , 4 , "MinedXP")
	self:NetworkVar("Float" , 1 , "MinedProgress")
	self:NetworkVar("String" , 3 , "OwnerName")
    self:NetworkVar("Entity", 0, "owning_ent")
end