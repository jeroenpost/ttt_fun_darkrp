AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include("shared.lua")
include("bitminer_config.lua")

local explodePos

local interval = 1 --Seconds before adding the value

function ENT:Think()

	if self.timer < CurTime() then
		
		self.timer = CurTime() + interval

		self:SetMinedProgress(self:GetMinedProgress(0) +  bitconfig.mediumProccessSpeed)

	end

	if self:GetMinedProgress(0) > 100 then

			self:SetMinedProgress(0)
			--self:SetMinedCoins(self:GetMinedCoins(0) + 1)
            local coins = self:GetMinedCoins(0)

            local owner = self:Getowning_ent()
            if IsValid(owner) then
                local level = owner:getDarkRPVar('level')
                local xp = (3*(level) ) + 750
                local mined = self:GetMinedXP(0)
                if coins < 3 then
                    self:SetMinedXP(mined + xp)
                end
                if coins < 10 then
                    self:SetMinedCoins(self:GetMinedCoins(0) + 1)
                end
            end


	end 

	if self:WaterLevel() > 0 then

		self:Remove()

		local explode = ents.Create( "env_explosion" )
		explode:SetPos( self:GetPos())
		explode:Spawn() 
		explode:SetKeyValue( "iMagnitude", tostring(bitconfig.exploisionSize)  )
		explode:Fire( "Explode", 1, 0 )
		explode:EmitSound( "weapon_AWP.Single", 400, 400 ) 



	end

end
 
function ENT:OnTakeDamage(damage )
    if true then return end
	
	self.health = self.health - damage:GetDamage()

	if self.health <= 0 then
		
		self:Remove()
		explodePos = self:GetPos()

		timer.Simple(0.1 , function()

			local explode = ents.Create( "env_explosion" )
			explode:SetPos( explodePos)
			explode:Spawn() 
			explode:SetKeyValue( "iMagnitude", tostring(bitconfig.exploisionSize) )
			explode:Fire( "Explode", 1, 0 )
			explode:EmitSound( "weapon_AWP.Single", 400, 400 ) 

			end)

	end

end

function ENT:Use(act , caller)

    if not gb.compare_rank(caller, {"vip","vipp","vippp"}) then
        caller:ChatPrint("You need to be VIP to use this")
        return
    end

	caller:CollectBitCoins(self)

end