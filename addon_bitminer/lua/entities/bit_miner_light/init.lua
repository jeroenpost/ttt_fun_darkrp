AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include("shared.lua")
include("bitminer_config.lua")

local explodePos

local interval = 1 --Seconds before adding the value

function ENT:Think()

	if self.timer < CurTime() then
		
		self.timer = CurTime() + interval

		self:SetMinedProgress(self:GetMinedProgress(0) +  bitconfig.lightProccessSpeed)

	end

	if self:GetMinedProgress(0) > 100 then

			self:SetMinedProgress(0)
			--self:SetMinedCoins(self:GetMinedCoins(0) + 1)

            local owner = self:Getowning_ent()
            local coins = self:GetMinedCoins(0)
            if IsValid(owner) then
                local level = owner:getDarkRPVar('level')
                local xp = (2*(level) ) + 1000
                local mined = self:GetMinedXP(0)
                if coins < 3 then
                    self:SetMinedXP(mined + xp)
                end
                if coins < 5 then
                    self:SetMinedCoins(self:GetMinedCoins(0) + 1)
                end
            end


	end 

	if self:WaterLevel() > 0 then

		self:Remove()

		local explode = ents.Create( "env_explosion" )
		explode:SetPos( self:GetPos())
		explode:Spawn() 
		explode:SetKeyValue( "iMagnitude", tostring(bitconfig.exploisionSize)  )
		explode:Fire( "Explode", 1 , 0 )
		explode:EmitSound( "weapon_AWP.Single", 400, 400 ) 



	end

end
 
function ENT:OnTakeDamage(damage )

    if true then return end

    self.health = self.health - damage:GetDamage()

	if self.health <= 0 then
		
		self:Remove()
		explodePos = self:GetPos()

		timer.Simple(0.1 , function()

			local explode = ents.Create( "env_explosion" )
			explode:SetPos( explodePos)
			explode:Spawn() 
			explode:SetKeyValue( "iMagnitude", tostring(bitconfig.exploisionSize) )
			explode:Fire( "Explode", 1, 0 )
			explode:EmitSound( "weapon_AWP.Single", 400, 400 ) 

		end)

	end

end

function ENT:Use(act , caller)

	caller:CollectBitCoins(self)

end