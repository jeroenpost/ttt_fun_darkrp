--Here is the config for the addon, You should not need to modify
--Any of the other scripts for this to work as you want, If you need support then be 
--sure to open a ticket on ScriptFodder and I will get back to you as soon as possible

bitconfig = {}

bitconfig.bitvalue = 750 --This is the value of one bit coin in DarkRP money

bitconfig.lightProccessSpeed = 0.5 --This is the speed that the light bit coin miner will mine the coins at (THIS SHOULD BE A SMALL VALUE!)
bitconfig.mediumProccessSpeed = 1 --Same as above but for the medium miner
bitconfig.heavyProccessSpeed = 3 --Same as above but for the heavy printer

bitconfig.exploisionSize = 0 --The size of the explosion , Change to 0 to remove all damage