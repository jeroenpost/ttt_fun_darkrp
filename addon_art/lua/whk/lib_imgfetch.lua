local folder = "whomekit/res"

local loaded = {}
local loading = {}
function whk.fetchImage(url, cb)
	if loaded[url] then
		if cb then cb(loaded[url]) end
		return loaded[url]
	end

	local ext = string.find(url, "%.vtf") and "vtf" or "png"

	local path = folder .. "/" .. string.format("%08x", util.CRC(url)) .. "." .. ext
	file.CreateDir(folder)

	local function LoadMat()
		if ext == "vtf" then
			return CreateMaterial("WHK_ImgFetcher_" .. CurTime(), "VertexLitGeneric", {
				["$basetexture"] = "../data/" .. path,
				-- ["$selfillum"] = "1",
				["Proxies"] = {
					["AnimatedTexture"] = {
						["animatedTextureVar"] = "$basetexture",
						["animatedTextureFrameNumVar"] = "$frame",
						["animatedTextureFrameRate"] = "10"
					}
				}
			})
		else
			return Material("../data/" .. path, "noclamp")
		end
	end

	if file.Exists(path, "DATA") then
		loaded[url] = LoadMat()
		if cb then cb(loaded[url]) end
		return loaded[url]
	end

	if loading[url] then
		return
	end

	loading[url] = true
	http.Fetch(url, function(data)
		file.Write(path, data)
		loaded[url] = LoadMat()
		if cb then cb(loaded[url]) end
	end)
end
