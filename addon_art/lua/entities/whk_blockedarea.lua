AddCSLuaFile()

ENT.Base = "whk_ent"

ENT.PrintName		= "Blocked Area"
ENT.Author			= "Wyozi"
ENT.Category		= "Wyozi Home Kit"

ENT.Spawnable		= true
ENT.AdminOnly		= true

ENT.Model = "models/mechanics/solid_steel/box_beam_16.mdl"

ENT.CustomBuyCheck = function(ply)
	return false
end

function ENT:Initialize()
	if SERVER then
		self:SetModel("models/hunter/blocks/cube075x075x075.mdl")
		self:DrawShadow(false)
		self:PhysicsInit(SOLID_BBOX)

		local mins, maxs = self:ComputeMinsMaxs()
		maxs = maxs + self:GetUp() * 52 -- lel. Dont want people jumping over
		self:SetCollisionBounds(self:WorldToLocal(mins), self:WorldToLocal(maxs))
	end
end

function ENT:GetAreaWidth()
	return 13
end
function ENT:GetAreaHeight()
	return 7
end

local stripeMat = Material("phoenix_storms/stripes")
local stripeUnlitMat

function ENT:Draw()
	if not IsValid(self.EdgeBar) then
		self.EdgeBar = ClientsideModel("models/mechanics/solid_steel/box_beam_16.mdl", RENDERGROUP_OPAQUE)
	end
	if not IsValid(self.DescrFrame) then
		self.DescrFrame = ClientsideModel("models/props_c17/Frame002a.mdl", RENDERGROUP_OPAQUE)
	end

	self.EdgeBar:SetRenderAngles(Angle(0, 0, 90))
	self.EdgeBar:SetModelScale(0.2, 0)
	-- Draw bars
	local nwpos, nepos, swpos, sepos = self:ComputeBarPositions()
	do -- Northwest
		self.EdgeBar:SetRenderOrigin(nwpos)
		self.EdgeBar:SetupBones()
		self.EdgeBar:DrawModel()
	end
	do -- Northeast
		self.EdgeBar:SetRenderOrigin(nepos)
		self.EdgeBar:SetupBones()
		self.EdgeBar:DrawModel()
	end
	do -- Southwest
		self.EdgeBar:SetRenderOrigin(swpos)
		self.EdgeBar:SetupBones()
		self.EdgeBar:DrawModel()
	end
	do -- Southeast
		self.EdgeBar:SetRenderOrigin(sepos)
		self.EdgeBar:SetupBones()
		self.EdgeBar:DrawModel()
	end

	-- Draw ropes

	if not stripeUnlitMat then
		stripeUnlitMat = CreateMaterial("Blocker_UnlitStripes", "UnlitGeneric", {})
		stripeUnlitMat:SetTexture("$basetexture", stripeMat:GetTexture("$basetexture"))
	end
	render.SetMaterial(stripeUnlitMat)

	local function DrawStripeRope(StartPos, EndPos)
		local segments = 6
		local maxdrop = 12
		local distpersegment = StartPos:Distance(EndPos) / segments
		local normal = (EndPos - StartPos):GetNormalized()

		render.StartBeam( segments + 1 )
		for i=0, segments do
			local distfrac = i / (segments*0.5)
			if distfrac > 1 then distfrac = 2 - distfrac end

			local pos = StartPos + normal * (distpersegment*i) + Vector(0, 0, -maxdrop * distfrac^0.5)
			render.AddBeam(pos, 1, i, Color(255, 255, 255))
		end
		render.EndBeam()
	end

	render.SuppressEngineLighting(true)

	DrawStripeRope(nwpos + Vector(0, 0, 18), nepos + Vector(0, 0, 18))
	DrawStripeRope(nepos + Vector(0, 0, 18), sepos + Vector(0, 0, 18))
	DrawStripeRope(sepos + Vector(0, 0, 18), swpos + Vector(0, 0, 18))
	DrawStripeRope(swpos + Vector(0, 0, 18), nwpos + Vector(0, 0, 18))

	render.SuppressEngineLighting(false)
end

function ENT:OnRemove()
	if IsValid(self.EdgeBar) then
		self.EdgeBar:Remove()
	end
	if IsValid(self.DescrFrame) then
		self.DescrFrame:Remove()
	end
end

function ENT:ComputeBarPositions()
	local midpos = self:GetPos()
	local xradius, yradius = self:GetAreaWidth(), self:GetAreaHeight()
	xradius, yradius = xradius * 4, yradius * 4

	local nwpos, nepos, swpos, sepos
	do -- Northwest
		nwpos = midpos - self:GetForward() * xradius - self:GetRight() * yradius
		nepos = midpos + self:GetForward() * xradius - self:GetRight() * yradius
		swpos = midpos - self:GetForward() * xradius + self:GetRight() * yradius
		sepos = midpos + self:GetForward() * xradius + self:GetRight() * yradius
	end

	return nwpos, nepos, swpos, sepos
end
function ENT:ComputeMinsMaxs()
	local _, nepos, swpos, _ = self:ComputeBarPositions()
	swpos = swpos + self:GetUp() * 9
	nepos = nepos - self:GetUp() * 9
	return swpos, nepos
end
