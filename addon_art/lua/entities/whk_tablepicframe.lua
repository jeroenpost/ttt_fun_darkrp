AddCSLuaFile()

ENT.Base = "whk_art"

ENT.PrintName		= "Table Picture Frame"
ENT.Author			= "Wyozi"
ENT.Category		= "Wyozi Home Kit"

ENT.Spawnable		= true
ENT.AdminOnly		= true

ENT.Model = "models/props_lab/frame002a.mdl"
ENT.BaseMaterial = Material("models/props_lab/photo_group002a")

ENT.Multipliers = {0.085, 0.207, 0.465, 0.734}

ENT.OverrideMatScale = 1

ENT.Price = 75
