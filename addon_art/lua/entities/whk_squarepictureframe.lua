AddCSLuaFile()

ENT.Base = "whk_art"

ENT.PrintName		= "Square Picture Frame"
ENT.Author			= "Wyozi"
ENT.Category		= "Wyozi Home Kit"

ENT.Spawnable		= true
ENT.AdminOnly		= true

ENT.Model = "models/props/de_inferno/picture1.mdl"

ENT.BaseMaterial = Material("models/props/de_inferno/picture1")
ENT.Multipliers = {0.16, 0.19, 0.68, 0.68}

ENT.MaterialIndex = 0

ENT.OverrideMatScale = 2

ENT.Price = 100
