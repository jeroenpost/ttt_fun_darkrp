AddCSLuaFile()

ENT.Base = "whk_art"

ENT.PrintName		= "Picture Frame"
ENT.Author			= "Wyozi"
ENT.Category		= "Wyozi Home Kit"

ENT.Spawnable		= true
ENT.AdminOnly		= true

ENT.Model = "models/props_c17/frame002a.mdl"
ENT.BaseMaterial = Material("models/props_c17/frame002a")
ENT.Multipliers = {0.672, 0.505, 0.308, 0.48}

ENT.Price = 100
