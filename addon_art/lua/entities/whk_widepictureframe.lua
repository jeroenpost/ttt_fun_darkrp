AddCSLuaFile()

ENT.Base = "whk_art"

ENT.PrintName		= "Wide Picture Frame"
ENT.Author			= "Wyozi"
ENT.Category		= "Wyozi Home Kit"

ENT.Spawnable		= true
ENT.AdminOnly		= true

ENT.Model = "models/maxofs2d/gm_painting.mdl"

ENT.BaseMaterial = Material("maxofs2d/models/gm_painting")
ENT.Multipliers = {0.06, 0.06, 0.885, 0.47}

ENT.OverrideMatScale = 0.25

ENT.Price = 150
