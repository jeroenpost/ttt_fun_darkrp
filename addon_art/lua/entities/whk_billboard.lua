AddCSLuaFile()

ENT.Base = "whk_art"
DEFINE_BASECLASS("whk_art")

ENT.PrintName		= "Billboard"
ENT.Author			= "Wyozi"
ENT.Category		= "Wyozi Home Kit"

ENT.Spawnable		= true
ENT.AdminOnly		= true

ENT.Model = "models/props/cs_assault/billboard.mdl"

ENT.BaseMaterial = Material("models/props/cs_assault/billboard")
ENT.Multipliers = {0.032, 0.032, 0.8, 0.43}

ENT.OverrideMatScale = 1
ENT.MaterialIndex = 0

if SERVER then
	function ENT:Initialize()
		BaseClass.Initialize(self)

		-- TODO use clientside ProjectedTexture()
		--[[
		local l = ents.Create("env_projectedtexture")
		l:SetParent(self)

		l:SetLocalPos(Vector(130, 0, 0))
		l:SetLocalAngles(Angle(0, -180, -180))
		l:SetKeyValue( "farz", 170 )
		l:SetKeyValue( "nearz", 12 )
		l:SetKeyValue( "lightfov", 100 )

		local b = 0.4
		l:SetKeyValue("lightcolor", Format("%i %i %i 255", 255 * b, 255 * b, 255 * b ))

		l:Spawn()
		]]
	end
end

if CLIENT then
	local coverMesh = Mesh()
	coverMesh:BuildFromTriangles {
		{ pos = Vector(1, -111, -42), normal = Vector(0, -1, 0), u = 0, v = 0 },
		{ pos = Vector(1, 111, -158), normal = Vector(0, -1, 0), u =1, v = 1 },
		{ pos = Vector(1, -111, -158), normal = Vector(0, -1, 0), u = 0, v = 1 },

		{ pos = Vector(1, -111, -42), normal = Vector(0, -1, 0), u = 0, v = 0 },
		{ pos = Vector(1, 111, -42), normal = Vector(0, -1, 0), u = 1, v = 0 },
		{ pos = Vector(1, 111, -158), normal = Vector(0, -1, 0), u = 1, v = 1 },
	}

	local origMat = Material("models/props/cs_assault/billboard")
	local matBeam = Material("effects/lamp_beam")

	local lightOffsets = {0, 65.8, 132.8, 199}
	function ENT:Draw()
		self:DrawModel()

		local coverMat = self:GetArtMaterial()
		if coverMat then
			local mat = Matrix()
			mat:Translate(self:LocalToWorld(Vector(0, 0, 100)))
			mat:Rotate(self:GetAngles())

			render.SetMaterial(coverMat)
			cam.PushModelMatrix(mat)
			coverMesh:Draw()
			cam.PopModelMatrix()
		end

		render.SetMaterial(matBeam)
		for i=1, 4 do

			local Pos = self:LocalToWorld(Vector(50, 99.2, 65)) + self:GetRight() * lightOffsets[i]
			local Norm = self:LocalToWorldAngles(Angle(-45, 0, 0)):Forward()

			render.StartBeam( 2 )
				render.AddBeam( Pos + Norm * 1, 40, 0.0, Color( 255, 255, 255, 50) )
				render.AddBeam( Pos - Norm * 50, 40, 1, Color( 255, 255, 255, 8) )
			render.EndBeam()
		end
	end
end
