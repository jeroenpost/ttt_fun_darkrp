ENT.Base = "whk_ent"
DEFINE_BASECLASS("whk_ent")

ENT.WHK_Art = true

ENT.Editable = true

function ENT:SetupDataTables()
	if BaseClass.SetupDataTables then BaseClass.SetupDataTables(self) end

	self:NetworkVar("String", 0, "Url", {KeyName = "url", Edit = {type = "String", order = 2}})
end
