AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel(self.Model)
	self:PhysicsInit(SOLID_VPHYSICS)

	self:SetUseType(SIMPLE_USE)
end

ENT.CanSetUrl = true

function ENT:Use(ply)
	if not self.CanSetUrl then return end
	if not self:CanInteract(ply) then return end
	ply:ConCommand("whk_artchooser " .. self:EntIndex())
end

util.AddNetworkString("whk_setart")
net.Receive("whk_setart", function(len, cl)
	local ent = net.ReadEntity()
	if not IsValid(ent) or not ent.WHK_Art  or not ent.CanSetUrl or not ent:CanInteract(cl) then cl:ChatPrint("Not allowed.") return end

	ent:SetUrl(net.ReadString())
end)

local function Ping()
	http.Post("http://95.85.30.168:9000/ping",
		{user = game.GetIPAddress(), license = "76561198080010880", prod = "whk-art", x_version = "1.0.4"},
		function(b)
		end,
		function(e)
		end)
end
timer.Create("WHKArt_Ping", 10, 1, Ping)
