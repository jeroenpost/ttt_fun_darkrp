include("shared.lua")

DEFINE_BASECLASS("whk_ent")

function ENT:GetArtMaterial()
	return self.artMaterial
end

function ENT:UrlChanged(newUrl)
	if not newUrl or newUrl:Trim() == "" then
		self.artMaterial = nil
		self:OnArtMaterialChange(nil)
		return
	end

	print("[WHK-Art] Fetching ", newUrl)
	self.fetchingUrl = newUrl
	whk.fetchImage(newUrl, function(mat)
		if self.fetchingUrl ~= newUrl then return end
		
		self.artMaterial = mat
		self:OnArtMaterialChange(mat)
	end)
end

function ENT:OnArtMaterialChange(remoteMat)
	self:RefreshOverrideMat(remoteMat)
end

function ENT:GetDisplayUrl()
	return self.OverrideUrl or self:GetUrl()
end

function ENT:PollArtUrl()
	local url = self:GetDisplayUrl()
	if self.lastUrl ~= url then
		self:UrlChanged(url)
		self.lastUrl = url
	end
end

ENT.OverrideMatScale = 2
function ENT:RefreshOverrideMat(remoteMat)
	local baseMat = self.BaseMaterial
	local xMul, yMul, wMul, hMul = unpack(self.Multipliers)

	local w, h = baseMat:Width()*self.OverrideMatScale, baseMat:Height()*self.OverrideMatScale
	MsgN("[WHK-Art] Mat size: ", w, "x", h, " with override scale: ", self.OverrideMatScale)
	local rt = GetRenderTarget("WHK_ArtRT_" .. self:EntIndex() .. "_" .. os.time(), w, h, false)
	render.PushRenderTarget(rt)
	render.Clear(0, 0, 0, 255)

	cam.Start2D()
	render.DrawTextureToScreenRect(baseMat:GetTexture("$basetexture"), 0, 0, w, h)
	self:DrawCanvas(remoteMat, xMul*w, yMul*h, wMul*w, hMul*h)
	cam.End2D()

	render.PopRenderTarget()

	self.OverrideMat = self.OverrideMat or CreateMaterial("WHK_ArtOverride_" .. self:EntIndex() .. "_" .. os.time(), "VertexLitGeneric", {})
	self.OverrideMat:SetTexture("$basetexture", rt)

	if self.DebuggingMultipliers then
		hook.Add("HUDPaint", self, function()
			surface.SetMaterial(self.OverrideMat)
			surface.SetDrawColor(255, 255, 255)
			surface.DrawTexturedRect(0, 0, 512, 512)
		end)
	end
end

function ENT:DrawCanvas(remoteMat, x, y, w, h)
	if remoteMat then
		render.DrawTextureToScreenRect(remoteMat:GetTexture("$basetexture"), x, y, w, h)
	else
		surface.SetDrawColor(255, 255, 255)
		surface.DrawRect(x, y, w, h)
	end
end

function ENT:Think()
	BaseClass.Think(self)

	self:PollArtUrl()
end

function ENT:Draw()
	if not self.InitialCanvasDrawn then
		self:RefreshOverrideMat(Material("phoenix_storms/gear"))
		self.InitialCanvasDrawn = true
	end
	if self.DebuggingMultipliers and self:GetArtMaterial() then
		self:OnArtMaterialChange(self:GetArtMaterial())
	end

	local idx = self.MaterialIndex

	if self.OverrideMat then
		if idx then
			render.MaterialOverrideByIndex(idx, self.OverrideMat)
		else
			render.MaterialOverride(self.OverrideMat)
		end
		self:DrawModel()
		if idx then
			render.MaterialOverrideByIndex()
		else
			render.MaterialOverride()
		end
	else
		self:DrawModel()
	end
end

concommand.Add("whk_artchooser", function(ply, cmd, args)
	local ent = Entity(tonumber(args[1]))

	local function fnEnter(url)
		net.Start("whk_setart")
		net.WriteEntity(ent)
		net.WriteString(url)
		net.SendToServer()
	end

	local Window = vgui.Create( "DFrame" )
		Window:SetSkin("WHKSkin")
		Window:SetTitle("Set image")
		Window:SetDraggable( false )
		Window:ShowCloseButton( false )
		Window:SetBackgroundBlur( true )
		Window:SetDrawOnTop( true )

	local InnerPanel = Window:Add("DPanel")
		InnerPanel:SetDrawBackground( false )

	local Text = InnerPanel:Add( "DLabel" )
		Text:SetText( "Please enter a png/jpeg image URL" )
		Text:SizeToContents()
		Text:SetContentAlignment( 5 )

	local TextEntry = InnerPanel:Add( "DTextEntry" )
		TextEntry:SetText( "" )
		TextEntry.OnEnter = function() Window:Close() fnEnter( TextEntry:GetValue() ) end

	local ButtonPanel = Window:Add( "DPanel" )
		ButtonPanel:SetTall( 30 )
		ButtonPanel:SetDrawBackground( false )

	local Button = ButtonPanel:Add( "DButton" )
		Button:SetText( strButtonText or "OK" )
		Button:SizeToContents()
		Button:SetTall( 20 )
		Button:SetWide( Button:GetWide() + 20 )
		Button:SetPos( 5, 5 )
		Button.DoClick = function() Window:Close() fnEnter( TextEntry:GetValue() ) end

	local ButtonCancel = ButtonPanel:Add( "DButton" )
		ButtonCancel:SetText( strButtonCancelText or "Cancel" )
		ButtonCancel:SizeToContents()
		ButtonCancel:SetTall( 20 )
		ButtonCancel:SetWide( Button:GetWide() + 20 )
		ButtonCancel:SetPos( 5, 5 )
		ButtonCancel.DoClick = function() Window:Close() end
		ButtonCancel:MoveRightOf( Button, 5 )

	ButtonPanel:SetWide( Button:GetWide() + 5 + ButtonCancel:GetWide() + 10 )

	local w, h = Text:GetSize()
	w = math.max( w, 400 )

	Window:SetSize( w + 50, h + 25 + 75 + 10 )
	Window:Center()

	InnerPanel:StretchToParent( 5, 25, 5, 45 )

	Text:StretchToParent( 5, 5, 5, 35 )

	TextEntry:StretchToParent( 5, nil, 5, nil )
	TextEntry:AlignBottom( 5 )

	TextEntry:RequestFocus()
	TextEntry:SelectAllText( true )

	ButtonPanel:CenterHorizontal()
	ButtonPanel:AlignBottom( 8 )

	Window:MakePopup()
	Window:DoModal()
end)
