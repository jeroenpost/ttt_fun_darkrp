
SWEP.Author			= "GreenBlack"
SWEP.Category 		= "GreenBlack"
SWEP.Contact		= ""
SWEP.Purpose		= "Allows you to be Spider-Man"
SWEP.Instructions	= "Left click to shot web"

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType 			= "normal"
SWEP.PrintName			= "SpiderMan Web"
SWEP.Slot				= 2
SWEP.SlotPos			= 0
SWEP.DrawAmmo			= true
SWEP.DrawCrosshair		= true
SWEP.ViewModel			= "models/weapons/v_pistol.mdl"
SWEP.WorldModel			= ""
SWEP.Primary.Damage         = 0
SWEP.Primary.ClipSize       = 3
SWEP.Primary.DefaultClip    = 3
SWEP.Primary.Automatic      = false
SWEP.Primary.Delay = 3
SWEP.Primary.Ammo       = "AR2AltFire"
SWEP.ViewModelFOV = 40
SWEP.UseHands = false

function SWEP:Initialize()

    self:SetHoldType( self.HoldType or "pistol" )
	
end

function SWEP:Think()

	if (!self.Owner || !IsValid(self.Owner)) then return end
	
	if ( self.Owner:KeyPressed( IN_ATTACK ) ) then
	
		self:StartAttack()
		
	elseif ( self.Owner:KeyDown( IN_ATTACK ) && inRange ) then
	
		self:UpdateAttack()
		
	elseif ( self.Owner:KeyReleased( IN_ATTACK ) && inRange ) then
	
		self:EndAttack( true )
	
	end


end

function SWEP:DoTrace( endpos )
	local trace = {}
		trace.start = self.Owner:GetShootPos()
		trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096)
		if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
		trace.filter = { self.Owner, self.Weapon }
		
	self.Tr = nil
	self.Tr = util.TraceLine( trace )
end



function SWEP:StartAttack()
	local gunPos = self.Owner:GetShootPos()
	local disTrace = self.Owner:GetEyeTrace()
	local hitPos = disTrace.HitPos
	
	local x = (gunPos.x - hitPos.x)^2;
	local y = (gunPos.y - hitPos.y)^2;
	local z = (gunPos.z - hitPos.z)^2;
	local distance = math.sqrt(x + y + z);
	
	local distanceCvar = GetConVarNumber("rope_distance")
	inRange = false
	if distance <= distanceCvar then
		inRange = true
	end
	
	if inRange then
		if (SERVER) then
			
			if (!self.Beam) then
				self.Beam = ents.Create( "spider_rope" )
					self.Beam:SetPos( self.Owner:GetShootPos() )
				self.Beam:Spawn()
			end
			
			self.Beam:SetParent( self.Owner )
			self.Beam:SetOwner( self.Owner )
		
		end
		
		self:DoTrace()
		self.speed = 55000
		self.startTime = CurTime()
		self.endTime = CurTime() + self.speed
		self.dt = -1
		
		if (SERVER && self.Beam) then
			self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
		end
		
		self:UpdateAttack()
		
	else
	end
end

function SWEP:UpdateAttack()

	self.Owner:LagCompensation( true )
	
	if (!endpos) then endpos = self.Tr.HitPos end
	
	if (SERVER && self.Beam) then
		self.Beam:GetTable():SetEndPos( endpos )
	end

	lastpos = endpos
	
	
			if ( self.Tr.Entity:IsValid() ) then
			
					endpos = self.Tr.Entity:GetPos()
					if ( SERVER ) then
					self.Beam:GetTable():SetEndPos( endpos )
					end
			
			end
			
			local vVel = (endpos - self.Owner:GetPos())
			local Distance = endpos:Distance(self.Owner:GetPos())
			
			local et = (self.startTime + (Distance/self.speed))
			if(self.dt != 0) then
				self.dt = (et - CurTime()) / (et - self.startTime)
			end
			if(self.dt < 0) then
				self.dt = 0
			end
			
			if(self.dt == 0) then
			zVel = self.Owner:GetVelocity().z
			vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
				if( SERVER ) then
				local gravity = GetConVarNumber("sv_Gravity")
				vVel:Add(Vector(0,0,(gravity/100)*5.5))
				if(zVel < 0) then
					vVel:Sub(Vector(0,0,zVel/300))
				end
				self.Owner:SetVelocity(vVel)
				end
			end
	
	endpos = nil
	
	self.Owner:LagCompensation( false )
	
end

function SWEP:EndAttack( shutdownsound )
	
	if ( CLIENT ) then return end
	if ( !self.Beam ) then return end
	
	self.Beam:Remove()
	self.Beam = nil
	
end



function SWEP:Holster()
	self:EndAttack( false )
	return true
end

function SWEP:OnRemove()
	self:EndAttack( false )
	return true
end


function SWEP:PrimaryAttack()
	self.Weapon:EmitSound("web/webfire.mp3")
end

SWEP.NextFreeze = 0
function SWEP:SecondaryAttack()
    if self:Clip1() < 1 then return end
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:EmitSound( Sound( "weapons/grenade_launcher1.wav" ) )

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
        self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
        local ply = self.Owner
        if not IsValid(ply) then return end

        local ang = ply:EyeAngles()

        if ang.p < 90 then
            ang.p = -10 + ang.p * ((90 + 10) / 90)
        else
            ang.p = 360 - ang.p
            ang.p = -10 + ang.p * -((90 + 10) / 90)
        end

        local vel = math.Clamp((90 - ang.p) * 13, 1000, 1400)

        local vfw = ang:Forward()
        local vrt = ang:Right()

        local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())

        src = src + (vfw * 1) + (vrt * 3)

        local thr = vfw * vel + ply:GetVelocity()

        local rc_ang = Angle(-10,0,0) + ang
        rc_ang:RotateAroundAxis(rc_ang:Right(), -90)

        local recombob = ents.Create("spider_freeze")
        if not IsValid(recombob) then return end
        recombob:SetPos(src)
        recombob:SetAngles(rc_ang)
        recombob:SetOwner(ply)
        recombob:Spawn()

        recombob.Damage = self.Primary.Damage
        recombob.Owner = ply
        recombob:SetOwner(ply)

        local phys = recombob:GetPhysicsObject()
        if IsValid(phys) then
            phys:SetVelocity(thr)
            phys:Wake()
        end
        timer.Simple(30,function() if(IsValid(self)) then self:SetClip1(self:Clip1()+1) end end)

        self:TakePrimaryAmmo( 1 )
        self:Reload()
    end
end
