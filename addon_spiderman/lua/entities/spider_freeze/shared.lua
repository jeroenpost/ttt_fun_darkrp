-- thrown knife

if SERVER then
   AddCSLuaFile("shared.lua")
else
   ENT.PrintName = "spider_freezer"
end


ENT.Type = "anim"
ENT.Model = Model("models/items/combine_rifle_ammo01.mdl")

function ENT:Initialize()
   self.Entity:SetModel(self.Model)

   self.Entity:PhysicsInit(SOLID_VPHYSICS)

   if SERVER then
      self:SetGravity(0.4)
      self:SetFriction(1.0)
      self:SetElasticity(0.45)

      self.StartPos = self:GetPos()

      self.Entity:NextThink(CurTime())
   end
end


if SERVER then
	function ENT:Explode()
		self.Entity:EmitSound( Sound("npc/assassin/ball_zap1.wav") )
	   local radius = 200
	   local phys_force = 1500
	   local push_force = 512
	   local pos = self.Entity:GetPos()
	   -- pull physics objects and push players
	   for k, target in pairs(ents.FindInSphere(pos, radius)) do
		  if IsValid(target) then

			 if target:IsPlayer() and (not target:IsFrozen()) and not target == self.Owner and (not table.HasValue( GovernmentTeams, team.GetName(target:Team()) )) then

                     local ply = target
                     if not ply.OldRunSpeed and not ply.OldWalkSpeed then
                         ply.OldRunSpeed = ply:GetNWInt("runspeed")
                         ply.OldWalkSpeed =  ply:GetNWInt("walkspeed")
                     end

                     ply:SetNWInt("runspeed", 25)
                     ply:SetNWInt("walkspeed", 25)
                     ply:SetRunSpeed(25)
                     ply:SetWalkSpeed(25)

                     timer.Create("NormalizeRunSpeedFreeze"..ply:UniqueID() ,6,1,function()
                         if IsValid(ply) and  ply.OldRunSpeed and ply.OldWalkSpeed then
                             if tonumber(ply.OldRunSpeed) < 220 then ply.OldRunSpeed = 260 end
                             if tonumber(ply.OldWalkSpeed) < 220 then ply.OldWalkSpeed = 240 end
                             if tonumber(ply.OldRunSpeed) > 400 then ply.OldRunSpeed = 400 end
                             if tonumber(ply.OldWalkSpeed) >400 then ply.OldWalkSpeed = 400 end
                             ply:SetNWInt("runspeed",  ply.OldRunSpeed)
                             ply:SetNWInt("walkspeed",ply.OldWalkSpeed)
                             ply:SetRunSpeed( ply.OldRunSpeed)
                             ply:SetWalkSpeed(ply.OldWalkSpeed)
                             ply.OldRunSpeed = false
                             ply.OldWalkSpeed = false
                         end
                     end)
            end
		  end
	   end

	   local phexp = ents.Create("env_physexplosion")
	   if IsValid(phexp) then
		  phexp:SetPos(pos)
		  phexp:SetKeyValue("magnitude", 100) --max
		  phexp:SetKeyValue("radius", radius)
		  -- 1 = no dmg, 2 = push ply, 4 = push radial, 8 = los, 16 = viewpunch
		  phexp:SetKeyValue("spawnflags", 1 + 16)
		  phexp:Spawn()
		  phexp:Fire("Explode", "", 0.2)
		end
	end
	
	
   function ENT:PhysicsCollide(data, phys)
	  self:Explode()
	  self.Entity:Remove()
	end
end
