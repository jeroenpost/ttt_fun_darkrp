/*-----------------------------------------------------------
	Racing
	
	Copyright © 2015 Szymon (Szymekk) Jankowski
	All Rights Reserved
	Steam: https://steamcommunity.com/id/szymski
-------------------------------------------------------------*/
if true then return end
include("shared.lua")

AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_init.lua")

function ENT:Initialize ()
	self:SetModel("models/humans/group01/male_02.mdl")
	self:SetHullType(HULL_HUMAN)
	self:SetHullSizeNormal()
	self:SetNPCState(NPC_STATE_SCRIPT)
	self:SetSolid(SOLID_BBOX)
	self:CapabilitiesAdd(CAP_ANIMATEDFACE)
	self:SetUseType(SIMPLE_USE)
	self:DropToFloor()
 
	self:SetMaxYawSpeed(90)

	self.nextSound = 0
end

function ENT:Think()
	if self.nextSound < CurTime() then
		self.nextSound = CurTime() + 2
		for k, v in pairs(player.GetAll()) do
			if v:GetPos():Distance(self:GetPos()) > 500 && v:GetPos():Distance(self:GetPos()) < 1000 then
				self:PlayScene("scenes/npc/male01/overhere01.vcd")
				self.nextSound = CurTime() + 30
				return
			end
		end
	end

	if !IsValid(self.player1) || self.player1:GetPos():Distance(self:GetPos()) > 500 then
		if IsValid(self.player1) then
			self.player1:SendLua("chat.AddText(Color(55,155,255),'[Racing]',Color(255,55,55),' Waiting cancelled!')")
		end
		self.player1 = nil
	end

	self:DropToFloor()
end

function ENT:AcceptInput(act, ply)
	if (ply:GetNWInt("next_race") or 0 ) > CurTime() then
		ply:SendLua("chat.AddText(Color(55,155,255),'[Racing]',Color(255,255,255),' You have to wait "..string.FormattedTime(ply:GetNWInt("next_race")-CurTime(), "%02i:%02i").."')")
		self:PlayScene("scenes/npc/male01/sorry01.vcd")
		--return
	end
	if self.player1 == ply then
		ply:SendLua("chat.AddText(Color(55,155,255),'[Racing]',Color(255,255,255),' You have to wait for second player')")
		return
	end
	net.Start("racing_menu")
	net.WriteInt(self:GetNWInt("track"), 16)
	net.Send(ply)
end
