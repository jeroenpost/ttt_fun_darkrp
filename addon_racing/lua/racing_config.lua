/*-----------------------------------------------------------
	MaterialEsc
	
	Copyright © 2015 Szymon (Szymekk) Jankowski
	All Rights Reserved
	Steam: https://steamcommunity.com/id/szymski
-------------------------------------------------------------*/

RACINGConfig = { }
local Config = RACINGConfig 

/*--------------------------------
	Racing configuration
----------------------------------*/

Config.DelayTime 						= 120			// Time the player has to wait after taking a part in a race (in seconds)
Config.TakeMoney						= false			// Takes money when a player looses
Config.NpcText 							= "Race NPC"	// The text above NPC
