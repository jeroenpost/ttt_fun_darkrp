/*-----------------------------------------------------------
	Racing
	
	Copyright © 2015 Szymon (Szymekk) Jankowski
	All Rights Reserved
	Steam: https://steamcommunity.com/id/szymski
-------------------------------------------------------------*/
if true then return end
SWEP.PrintName			= "Track Maker"
SWEP.Author				= "Szymekk"
SWEP.Instructions		= ""
SWEP.Spawnable = true

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo		= "none"

SWEP.Weight			    = 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Slot				= 0
SWEP.SlotPos			= 20
SWEP.DrawAmmo			= true
SWEP.DrawCrosshair		= true

SWEP.UseHands 			= false

if CLIENT then
	local function DrawTextS(text, font, posx, posy, color, align)
		draw.DrawText(text, font, posx+1, posy+1, Color(0,0,0), align)
		draw.DrawText(text, font, posx, posy, color, align)
	end	

	function SWEP:DrawHUD()
		local track = LocalPlayer():GetNWInt("seltrack")
		local info = LocalPlayer():GetNWString("trackinfo")

		if track > 0 then
			DrawTextS("Editing track " .. track .. (info != "" and (" - " .. info) or ""), "DermaLarge", ScrW()/2, 30, Color(255,255,255), 1)
		else
			DrawTextS("Click on the ground to place an NPC", "DermaLarge", ScrW()/2, 30, Color(255,255,255), 1)
			DrawTextS("Click on an existing NPC to edit", "DermaLarge", ScrW()/2, 60, Color(255,255,255), 1)
		end
	end
end

if CLIENT then return end

function SWEP:Deploy()
	local ply = self.Owner
	local track = self.Owner.sel_track
	if !track || !table.HasValue(RACING.Tracks, track) then
		ply:SetNWInt("seltrack", -1)
	end
end

function SWEP:PrimaryAttack()
	local tr = self.Owner:GetEyeTrace()
	local ply = self.Owner
	local track = self.Owner.sel_track

	// Continue editing track
	if tr.Entity:GetClass() == "race_npc" then
		local id = tr.Entity:GetNWInt("track")
		ply.sel_track = RACING.Tracks[id]
		ply:SetNWInt("seltrack", id)
		ply:SetNWString("trackinfo", "Place checkpoints")
		return
	end

	// Creating NPC
	if !track || !table.HasValue(RACING.Tracks, track) then
		RACING.Tracks[#RACING.Tracks+1] = { }
		ply.sel_track = RACING.Tracks[#RACING.Tracks]
		track = ply.sel_track
		ply:ChatPrint("Created new track id " .. #RACING.Tracks)
		local npc = ents.Create("race_npc")
		npc:SetPos(tr.HitPos + Vector(0,0,30))
		npc:Spawn()
		npc:SetAngles(Angle(0,ply:EyeAngles().y-180,0))
		npc:SetNWInt("track", #RACING.Tracks)
		track.Npc = npc
		track.Vehicle = { }
		track.Vehicle.Name = "Jeep"
		track.Vehicle.Class = "prop_vehicle_jeep_old"
		track.Vehicle.Model = "models/buggy.mdl"
		track.Vehicle.KeyValues = {
			vehiclescript = "scripts/vehicles/jeep_test.txt"
		}
		track.Prize = 500
		track.Time = 30
		ply:SetNWInt("seltrack", #RACING.Tracks)
		ply:SetNWString("trackinfo", "Place checkpoints")
		RACING.UpdateTracks()
		return
	end

	// Creating first point
	if !track.Points then		
		track.Points = { }
		track.Points[#track.Points+1] = tr.HitPos
		RACING.UpdateTracks()
		return
	end

	track.Points[#track.Points+1] = tr.HitPos
	RACING.UpdateTracks()
end

function SWEP:SecondaryAttack()
	local track = self.Owner.sel_track

	if !track || !table.HasValue(RACING.Tracks, track) then return end

	RACING.UpdateTracks()

	net.Start("race_track_menu")
	net.Send(self.Owner)
end
