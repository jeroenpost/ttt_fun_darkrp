/*-----------------------------------------------------------
	Racing
	
	Copyright © 2015 Szymon (Szymekk) Jankowski
	All Rights Reserved
	Steam: https://steamcommunity.com/id/szymski
-------------------------------------------------------------*/
if true then return end
local DEBUG = false

if SERVER then
	AddCSLuaFile()
	AddCSLuaFile("racing_config.lua")
	AddCSLuaFile("matcore.lua")

	include("racing_config.lua")
	local Config = RACINGConfig
	RACING = RACING or { }
	RACING.Tracks = RACING.Tracks or { }
	RACING.Races = { }

	CreateConVar("rac_ver", 1, FCVAR_NOTIFY)

	resource.AddFile("materials/racing/menu.png")
	resource.AddFile("materials/racing/close.png")
	resource.AddFile("materials/racing/way.png")

	resource.AddFile("materials/shadow/u.png")
	resource.AddFile("materials/shadow/l.png")
	resource.AddFile("materials/shadow/r.png")
	resource.AddFile("materials/shadow/d.png")

	resource.AddFile("materials/shadow/lu.png")
	resource.AddFile("materials/shadow/ru.png")
	resource.AddFile("materials/shadow/ld.png")
	resource.AddFile("materials/shadow/rd.png")

	resource.AddFile("models/racing/arrow.mdl")

	util.AddNetworkString("racing_menu")
	util.AddNetworkString("tracks_update")
	util.AddNetworkString("track_update")
	util.AddNetworkString("track_remove")
	util.AddNetworkString("track_removel")
	util.AddNetworkString("track_done")
	util.AddNetworkString("race_track_menu")
	util.AddNetworkString("race_accept")
	util.AddNetworkString("race_done")

	// Accepting race
	net.Receive("race_accept", function(len, ply)
		local id = net.ReadInt(16)
		local track = RACING.Tracks[id]
		if !track then return end
		if track.Type == 1 then
			if IsValid(track.Npc.player1) && track.Npc.player1:GetPos():Distance(track.Npc:GetPos()) < 500 then
				RACING.StartRace(track.Npc.player1, id, ply)
				track.Npc.player1 = nil
			else
				track.Npc.player1 = ply
				ply:SendLua("chat.AddText(Color(55,155,255),'[Racing]',Color(255,255,255),' Wait for second player and stay close to the NPC!')")
				if DEBUG then
					RACING.StartRace(track.Npc.player1, id, player.GetAll()[2])
					track.Npc.player1 = nil
				end
			end
			return
		end
		RACING.StartRace(ply, id)
	end)

	// Removing entire track
	net.Receive("track_remove", function(len, ply)
		local id = net.ReadInt(16)
		if IsValid(RACING.Tracks[id].Npc) then
			RACING.Tracks[id].Npc:Remove()
		end
		RACING.Tracks[id] = nil
		RACING.UpdateTracks()
	end)

	// Removing last point
	net.Receive("track_removel", function(len, ply)
		local track = RACING.Tracks[net.ReadInt(16)]
		if track && track.Points && #track.Points > 0 then
			table.remove(track.Points, #track.Points)
			RACING.UpdateTracks()
		end
	end)

	// Updating track
	net.Receive("track_update", function(len, ply)
		local track = RACING.Tracks[net.ReadInt(16)]
		if track then
			track.Prize = net.ReadInt(32)
			track.Time = net.ReadInt(32)
			track.Type = net.ReadInt(8)

			track.Vehicle.Name = net.ReadString()
			track.Vehicle.Class = net.ReadString()
			track.Vehicle.Model = net.ReadString()
			track.Vehicle.KeyValues = net.ReadTable()

			RACING.UpdateTracks()
			RACING.Save()
		end
	end)

	// Editing done
	net.Receive("track_done", function(len, ply)
		ply:SetNWInt("seltrack", -1)
		ply.sel_track = nil
	end)

	function RACING.UpdateTracks()
		net.Start("tracks_update")
		net.WriteTable(RACING.Tracks)
		net.Broadcast()
	end

	function RACING.StartRace(ply, id, ply2)
		net.Start("tracks_update")
		net.WriteTable(RACING.Tracks)
		net.Send(ply)

		ply:SetNWInt("next_race", math.ceil(CurTime()+Config.DelayTime))

		local track = RACING.Tracks[id]
		if track.Type == 0 then
			ply:SetNWInt("curtrack", id)
			ply:SetNWInt("curpoint", 1)
			ply:SetNWFloat("racestart", CurTime() + 3)
			local veh = ents.Create(track.Vehicle.Class)
			veh:SetModel(track.Vehicle.Model)
			for k, v in pairs(track.Vehicle.KeyValues) do
				veh:SetKeyValue(k, v)
			end
			veh:SetPos(track.Points[1]+Vector(0,0,10))
			veh:SetAngles(Angle(0,(track.Points[1]-track.Points[2]):Angle().y+90,0))
			veh:Spawn()
			ply:EnterVehicle(veh)
			RACING.Races[#RACING.Races+1] = {
				Player = ply,
				TrackID = id,
				Track = track,
				Vehicle = veh,
				Type = track.Type,
				StartTime = CurTime() + 3,
				StartPos = veh:GetPos(),
				CurPoint = 1
			}
		else
			ply:SetNWInt("curtrack", id)
			ply:SetNWInt("curpoint", 1)
			ply:SetNWFloat("racestart", CurTime() + 3)
			local veh = ents.Create(track.Vehicle.Class)
			veh:SetModel(track.Vehicle.Model)
			for k, v in pairs(track.Vehicle.KeyValues) do
				veh:SetKeyValue(k, v)
			end
			veh:SetPos(track.Points[1]+Vector(0,0,10)-(track.Points[1]-track.Points[2]):Angle():Right()*90)
			veh:SetAngles(Angle(0,(track.Points[1]-track.Points[2]):Angle().y+90,0))
			veh:Spawn()
			ply:EnterVehicle(veh)

			ply2:SetNWInt("curtrack", id)
			ply2:SetNWInt("curpoint", 1)
			ply2:SetNWFloat("racestart", CurTime() + 3)
			local veh2 = ents.Create(track.Vehicle.Class)
			veh2:SetModel(track.Vehicle.Model)
			for k, v in pairs(track.Vehicle.KeyValues) do
				veh2:SetKeyValue(k, v)
			end
			veh2:SetPos(track.Points[1]+Vector(0,0,10)+(track.Points[1]-track.Points[2]):Angle():Right()*90)
			veh2:SetAngles(Angle(0,(track.Points[1]-track.Points[2]):Angle().y+90,0))
			veh2:Spawn()
			ply2:EnterVehicle(veh2)

			RACING.Races[#RACING.Races+1] = {
				Player = ply,
				Player2 = ply2,
				TrackID = id,
				Track = track,
				Vehicle = veh,
				Vehicle2 = veh2,
				Type = track.Type,
				StartTime = CurTime() + 3,
				StartPos = track.Points[1],
				CurPoint = 1,
				CurPoint2 = 1,
			}
		end
	end

	function RACING.EndRace(raceid, won, failmsg, plyWon)
		local race = RACING.Races[raceid]
		race.Player:SetNWInt("curtrack", -1)
		timer.Simple(2, function()
			race.Vehicle:Remove()
		end)
		RACING.Races[raceid] = nil

		if race.Type == 0 then
			net.Start("race_done")
			net.WriteBit(won and 1 or 0)
			net.WriteString(won and "Won!" or "Failed!")
			net.WriteString(won and ("You got $" .. race.Track.Prize) or failmsg)
			net.Send(race.Player)

			if won && race.Player.addMoney then
				race.Player:addMoney(race.Track.Prize)
			end

			if !won && Config.TakeMoney && race.Player.addMoney then
				race.Player:addMoney(-race.Track.Prize)
			end		
		else
			race.Player2:SetNWInt("curtrack", -1)

			timer.Simple(2, function()
				race.Vehicle2:Remove()
			end)

			local plyFail = (race.Player == plyWon) and race.Player2 or race.Player

			net.Start("race_done")
			net.WriteBit(1)
			net.WriteString("Won!")
			net.WriteString("You got $" .. race.Track.Prize)
			net.Send(plyWon)

			net.Start("race_done")
			net.WriteBit(1)
			net.WriteString("Failed!")
			net.WriteString(failmsg)
			net.Send(plyFail)

			if plyWon.addMoney then
				plyWon:addMoney(race.Track.Prize)
			end

			if Config.TakeMoney && plyFail.addMoney then
				plyFail.addMoney:addMoney(-race.Track.Prize)
			end
		end
	end

	// Checking
	hook.Add("Tick", "RacingTick", function()
		for k, race in pairs(RACING.Races) do
			local track = race.Track

            if not IsValid(race.Player) then return end

			if race.Type == 0 then
				if !race.Player:InVehicle() then
					RACING.EndRace(k, false, "You got out of the vehicle")
                race.Vehicle:Remove()
				end

				if race.StartTime > CurTime() then
					race.Vehicle:GetPhysicsObject():SetVelocity(Vector())
					return
				end

				local temp = (track.Points[race.CurPoint+1] and (track.Points[race.CurPoint]-track.Points[race.CurPoint+1]) or (track.Points[race.CurPoint-1]-track.Points[race.CurPoint]))
				local dot = temp:Dot(track.Points[race.CurPoint+1]-race.Vehicle:GetPos())

               

                if dot > -200000 then
					race.CurPoint = race.CurPoint + 1
					race.Player:SetNWInt("curpoint", race.CurPoint)
					if !track.Points[race.CurPoint+1] then
						if CurTime()-race.StartTime < race.Track.Time then
							RACING.EndRace(k, true)
						else
							RACING.EndRace(k, false, "You were too slow")
						end
						return
					end
				end
			else
				if !race.Player:InVehicle() then
					RACING.EndRace(k, false, "You got out of the vehicle", race.Player2)
                    race.Vehicle:Remove()
				end

				if !race.Player2:InVehicle() then
					RACING.EndRace(k, false, "You got out of the vehicle", race.Player)
                    race.Vehicle:Remove()
				end

				if race.StartTime > CurTime() then
					race.Vehicle:GetPhysicsObject():SetVelocity(Vector())
					race.Vehicle2:GetPhysicsObject():SetVelocity(Vector())
					return
				end

				local temp = (track.Points[race.CurPoint+1] and (track.Points[race.CurPoint]-track.Points[race.CurPoint+1]) or (track.Points[race.CurPoint-1]-track.Points[race.CurPoint]))
				local dot = temp:Dot(track.Points[race.CurPoint+1]-race.Vehicle:GetPos())



                if dot > -200000 then
					race.CurPoint = race.CurPoint + 1
					race.Player:SetNWInt("curpoint", race.CurPoint)
					if !track.Points[race.CurPoint+1] then
						RACING.EndRace(k, true, "You were too slow", race.Player)
						return
					end
				end

				local temp = (track.Points[race.CurPoint+1] and (track.Points[race.CurPoint]-track.Points[race.CurPoint+1]) or (track.Points[race.CurPoint-1]-track.Points[race.CurPoint]))
				local dot = temp:Dot(track.Points[race.CurPoint+1]-race.Vehicle2:GetPos())

                if dot > -200000 then
					race.CurPoint = race.CurPoint + 1
					race.Player2:SetNWInt("curpoint", race.CurPoint)
					if !track.Points[race.CurPoint+1] then
						RACING.EndRace(k, true, "You were too slow", race.Player2)
						return
					end
				end
			end
		end
	end)

	// Track saving
	function RACING.Save() 
		local data = { }

		for k, v in pairs(RACING.Tracks) do
			local track = { }
			data[#data+1] = track
			track.Vehicle = v.Vehicle
			track.Prize = v.Prize
			track.Time = v.Time
			track.Type = v.Type
			track.Points = v.Points
			track.NpcPos = v.Npc:GetPos()
			track.NpcRot = v.Npc:GetAngles()
		end

		file.Write("racing_tracks_" .. game.GetMap() .. ".txt", util.TableToJSON(data))
	end

	// Track loading
	function RACING.Load()
		RACING.Tracks = { }

		for k, v in pairs(ents.FindByClass("race_npc")) do
			v:Remove()
		end

		if !file.Exists("racing_tracks_" .. game.GetMap() .. ".txt", "DATA") then return end

		local data = util.JSONToTable(file.Read("racing_tracks_" .. game.GetMap() .. ".txt", "DATA"))

		for k, v in pairs(data) do
			local track = { }
			RACING.Tracks[#RACING.Tracks+1] = track
			local npc = ents.Create("race_npc")
			npc:SetPos(v.NpcPos)
			npc:Spawn()
			npc:SetAngles(v.NpcRot)
			npc:SetNWInt("track", #RACING.Tracks)
			track.Npc = npc
			track.Vehicle = v.Vehicle
			track.Prize = v.Prize
			track.Time = v.Time
			track.Points = v.Points
			track.Type = v.Type
		end 

		RACING.UpdateTracks()
	end

	hook.Add("InitPostEntity", "RacingLoad", function() 
		timer.Simple(10, function()
			RACING.Load()
		end)
	end)

	// Delay time reset
	for k, v in pairs(player.GetAll()) do
		v:SetNWInt("next_race", 0)
	end

	// Duel reset
	for k, v in pairs(ents.FindByClass("race_npc")) do
		v.player1 = nil
	end

	hook.Add("PlayerSpawn", "RacingUpdate", function(ply)
		net.Start("tracks_update")
		net.WriteTable(RACING.Tracks)
		net.Send(ply)
	end)

	return
end

include("matcore.lua")

include("racing_config.lua")
local Config = RACINGConfig
RACING = RACING or { }
RACING.Tracks = RACING.Tracks or { }

/*--------------------------------
	Useful
----------------------------------*/

local MatMenu = Material("racing/menu.png", "unlitgeneric")
local MatClose = Material("racing/close.png", "unlitgeneric")
local MatWay = Material("racing/way.png", "unlitgeneric")

net.Receive("tracks_update", function()
	RACING.Tracks = net.ReadTable()
	if IsValid(LocalPlayer()) then
		if (LocalPlayer():GetNWInt("seltrack") or 0) > 0 then
			RACING.CurTrack = RACING.Tracks[LocalPlayer():GetNWInt("seltrack")]
		end
	end
end)

/*--------------------------------
	Drawing track
----------------------------------*/

if IsValid(LocalPlayer()) then
	LocalPlayer():SetNWInt("curtrack", -1)
end

hook.Add("PostDrawTranslucentRenderables", "RacingDrawTrack", function()
	RACING.CurTrack = RACING.Tracks[LocalPlayer():GetNWInt("seltrack")] or RACING.Tracks[LocalPlayer():GetNWInt("curtrack")]

	if !RACING.CurTrack || !RACING.CurTrack.Points then return end

	/*for k, v in pairs(RACING.CurTrack.Points) do
		local nextTrack = RACING.CurTrack.Points[k+1] or v
		cam.Start3D2D(v+Vector(0,0,2), (nextTrack - v):Angle(), 1)
			surface.SetDrawColor(255,255,255)
			surface.SetMaterial(MatWay)
			for i=1, (nextTrack - v):Length()/80 do
				local norm = (nextTrack - v)
				norm:Normalize()
				if (v+norm*80*i):Distance(LocalPlayer():GetPos()) > 500 then break end
				pos = Vector(i*80,0,0)
				surface.DrawTexturedRectRotated(pos.x,pos.y,64,64,-90)
			end
		cam.End3D2D()

		render.DrawLine(v, nextTrack, Color(0,255,0), true)
	end*/

	local curPoint = 1
	local offset = 350
	local curOff = 0

	local a = 1
	while RACING.CurTrack.Points[curPoint] do
		local curTrack = RACING.CurTrack.Points[curPoint]
		local nextTrack = RACING.CurTrack.Points[curPoint+1] or RACING.CurTrack.Points[curPoint]
		cam.Start3D2D(curTrack+Vector(0,0,2), (nextTrack - curTrack):Angle(), 1)
			surface.SetDrawColor(255,255,255)
			surface.SetMaterial(MatWay)
			local i = 1
			while i*offset < curTrack:Distance(nextTrack)+curOff do
				local norm = (nextTrack - curTrack)
				norm:Normalize()
				--if (curTrack+norm*offset*i):Distance(LocalPlayer():GetPos()) > 5000 then break end
				pos = Vector(i*offset-curOff,0,0)
				if EyePos():Distance(curTrack+norm*(i*offset-curOff)) < 1800 then
					surface.SetDrawColor(255,255,255,255*(1-math.Clamp(EyePos():Distance(curTrack+norm*(i*offset-curOff))/1800,0,1)))
					if math.ceil((CurTime()+a)*30)%31==0 && !RACING.Tracks[LocalPlayer():GetNWInt("seltrack")] then
						surface.SetDrawColor(255,255,0)
						surface.DrawTexturedRectRotated(pos.x,pos.y,96,96,-90)
					end
					surface.DrawTexturedRectRotated(pos.x,pos.y,64,64,-90)
				end
				i = i + 1
				a = a + 1
				//curOff = 0
			end

			curOff = (curTrack:Distance(nextTrack)+curOff)%offset
		cam.End3D2D()

		if RACING.Tracks[LocalPlayer():GetNWInt("seltrack")] then
			render.DrawLine(curTrack, nextTrack, Color(0,255,0), true)
		end

		curPoint = curPoint + 1
	end
end)

/*--------------------------------
	HUD
----------------------------------*/

surface.CreateFont("racing_time", {
	font = "Roboto",
	size = 72,
	weight = 5000,
	blursize = 0,
	scanlines = 0,
	antialias = true,
})

surface.CreateFont("racing_number", {
	font = "Roboto",
	size = 128,
	weight = 5000,
	blursize = 0,
	scanlines = 0,
	antialias = true,
}) 

local function DrawTextS(text, font, posx, posy, color, alignx, alingy)
	draw.SimpleText(text, font, posx+1, posy+1, Color(0,0,0,color.a), alignx, aligny)
	draw.SimpleText(text, font, posx, posy, color, alignx, aligny)
end	

local lastNumber = 0

local endTextTime = 1
local endText1 = ""
local endText2 = ""

local won = false

local arrowAng = 0

surface.CreateFont("racing_npcfont", {
	font = "Roboto",
	size = 55,
	weight = 5000,
	blursize = 0,
	scanlines = 0,
	antialias = true,
}) 

hook.Add("HUDPaint", "RacingHUD", function()
	// Above head text
	for k, v in pairs(ents.FindByClass("race_npc")) do
		if v:GetPos():Distance(EyePos()) > 500 then continue end
		local pos = (v:GetPos()+Vector(0,0,85)):ToScreen()
		draw.SimpleText(Config.NpcText, "racing_npcfont", pos.x+1, pos.y+1, Color(0,0,0), 1, 1)
		draw.SimpleText(Config.NpcText, "racing_npcfont", pos.x, pos.y, Color(255,255,255), 1, 1)
	end 

	if endTextTime > 0.02 then
		endTextTime = math.Max(endTextTime-FrameTime()*0.2, 0)
		DrawTextS(endText1, "racing_number", ScrW()/2, ScrH()/2-100, Color(255,255,255, math.sin((1-endTextTime)*3.14)*512), 1, 1)
		DrawTextS(endText2, "racing_time", ScrW()/2, ScrH()/2, Color(255,255,255, math.sin((1-endTextTime)*3.14)*512), 1, 1)
	end

	if !RACING.CurTrack || !RACING.CurTrack.Points || RACING.Tracks[LocalPlayer():GetNWInt("seltrack")] then return end

	local startTime = LocalPlayer():GetNWFloat("racestart")

	if startTime > CurTime() then
			if lastNumber != math.ceil(startTime-CurTime()) then
				surface.PlaySound("buttons/button16.wav")
			end
			DrawTextS("" .. math.ceil(startTime-CurTime()), "racing_number", ScrW()/2, ScrH()/2-64, Color(255,255,255), 1, 1)
			lastNumber = math.ceil(startTime-CurTime())
		return
	end

	if lastNumber != 0 then
		surface.PlaySound("weapons/357/357_fire2.wav")
		lastNumber = 0
	end

	DrawTextS(string.FormattedTime(CurTime()-LocalPlayer():GetNWFloat("racestart"), "%02i:%02i:%02i"), "racing_time", ScrW()/2, 80, Color(255,255,255), 1, 1)

	local curPoint = LocalPlayer():GetNWInt("curpoint")

	local ang = RACING.CurTrack.Points[curPoint+1] and (RACING.CurTrack.Points[curPoint+1]+(RACING.CurTrack.Points[curPoint+1]-RACING.CurTrack.Points[curPoint])*0.5-LocalPlayer():GetPos()):Angle().y or
	(RACING.CurTrack.Points[curPoint]-LocalPlayer():GetPos()):Angle().y

	ang = ang - EyeAngles().y

	ang = (RACING.CurTrack.Points[curPoint]-RACING.CurTrack.Points[curPoint+1]):Angle().y - EyeAngles().y - 180

	arrowAng = LerpAngle(FrameTime()*5, Angle(0,arrowAng,0), Angle(0,ang,0)).y

	// Arrow drawing
	cam.Start3D(Vector(0,0,0), Angle(0,0,0))

	render.SetColorModulation(3, 3, 3)
	render.SetLightingMode(1)
	render.Model({
		model = "models/racing/arrow.mdl",
		pos = Vector(120,0,-60),
		angle = Angle(0,arrowAng,0)
	})
	render.SetLightingMode(0)
	render.SetColorModulation(1, 1, 1)

	cam.End3D()
end)

/*--------------------------------
	Net messages
----------------------------------*/

net.Receive("race_done", function()
	endTextTime = 1
	won = net.ReadBit() == 1
	endText1 = net.ReadString()
	endText2 = net.ReadString()
end)

/*--------------------------------
	Menu
----------------------------------*/

if IsValid(RACEM) then
	RACEM:Remove()
end

local function OpenRaceMenu()
	local id = net.ReadInt(16)
	local track = RACING.Tracks[id]

	RACEM = matcore.MakeDialog(Color(255,255,255),
		track.Type == 0 and "Take a part in a race?" or "Take a part in a duel race?",
		"You're going to take a part in a race.\nMoney to win: $" .. track.Prize .. "\nTime: " .. track.Time .. " seconds", 
		Color(0,0,0), Color(120,120,120), "YES", "NO")
	RACEM:SetAnimSize(500,300)
	function RACEM.Agree:DoClick()
		net.Start("race_accept")
		net.WriteInt(id, 16)
		net.SendToServer()
		RACEM:Remove()
	end
	function RACEM.Disagree:DoClick()
		RACEM:Remove()
	end
end

net.Receive("racing_menu", OpenRaceMenu)

/*--------------------------------
	Track edit menu
----------------------------------*/

if IsValid(TRACKM) then
	TRACKM:Remove()
end

local function OpenTrackEdit()
	local track = RACING.Tracks[LocalPlayer():GetNWInt("seltrack")]

	TRACKM = vgui.Create("DFrame")
	TRACKM:SetSize(400, 400)
	TRACKM:SetPos(ScrW()-TRACKM:GetWide()-10, ScrH()-TRACKM:GetTall()-10)
	TRACKM:MakePopup()
	function TRACKM:OnClose()
		TRACKM:Update()
	end

	// Remove Last Button
	local rmLast = TRACKM:Add("DButton")
	rmLast:Dock(TOP)
	rmLast:SetText("Remove Last Checkpoint")
	function rmLast:DoClick()
		net.Start("track_removel")
		net.WriteInt(LocalPlayer():GetNWInt("seltrack"), 16)
		net.SendToServer()
	end

	// Track type
	local typeBtn = TRACKM:Add("DButton")
	typeBtn:Dock(TOP)
	typeBtn:SetText((track.Type or 0) == 0 and "Single Player" or "Duel")
	typeBtn.type = track.Type
	function typeBtn:DoClick()
		track.Type = track.Type == 1 and 0 or 1
		typeBtn.type = track.Type
		typeBtn:SetText((track.Type or 0) == 0 and "Single Player" or "Duel")
	end

	// Vehicle
	local veh = TRACKM:Add("DButton")
	veh:Dock(TOP)
	veh:SetText(track.Vehicle.Name)
	function veh:DoClick()
		local lst = list.Get("Vehicles")
		local menu = DermaMenu()
		for k, v in pairs(lst) do
			menu:AddOption(v.Name, function()
				veh:SetText(v.Name)
				veh.Name = v.Name
				veh.Class = v.Class
				veh.Model = v.Model
				veh.KeyValues = v.KeyValues
			end)
		end
		local lst = list.Get("SCars")
		for k, v in pairs(lst) do
			menu:AddOption(v.Name, function()
				veh:SetText(v.Name)
				veh.Name = v.Name
				veh.Class = v.Class
				veh.Model = v.Model
				veh.KeyValues = v.KeyValues
			end)
		end
		menu:Open()
	end
	veh.Name = track.Vehicle.Name
	veh.Class = track.Vehicle.Class
	veh.Model = track.Vehicle.Model
	veh.KeyValues = track.Vehicle.KeyValues

	// Remove Button
	local rmBtn = TRACKM:Add("DButton")
	rmBtn:SetText("Remove")
	rmBtn:Dock(BOTTOM)
	function rmBtn:DoClick()
		net.Start("track_remove")
		net.WriteInt(LocalPlayer():GetNWInt("seltrack"), 16)
		net.SendToServer()
		TRACKM:Remove()
	end

	// Finish editing Button
	local finishBtn = TRACKM:Add("DButton")
	finishBtn:Dock(BOTTOM)
	finishBtn:SetText("Finish editing")
	function finishBtn:DoClick()
		net.Start("track_done")
		net.SendToServer()
		TRACKM:Update()
		TRACKM:Remove()
	end

	// Prize
	local prize = TRACKM:Add("DNumSlider")
	prize:Dock(TOP)
	prize:SetText("Money")
	prize:SetMin(1)
	prize:SetMax(5000)
	prize:SetDecimals(0)
	prize:SetValue(track.Prize)

	// Time
	local time = TRACKM:Add("DNumSlider")
	time:Dock(TOP)
	time:SetText("Time (in seconds)")
	time:SetMin(5)
	time:SetMax(300)
	time:SetDecimals(0)
	time:SetValue(track.Time)

	function TRACKM:Update()
		net.Start("track_update")

		net.WriteInt(LocalPlayer():GetNWInt("seltrack"), 16)

		net.WriteInt(prize:GetValue(), 32)
		net.WriteInt(time:GetValue(), 32)
		net.WriteInt(typeBtn.type or 0, 8)

		net.WriteString(veh.Name)
		net.WriteString(veh.Class)
		net.WriteString(veh.Model)
		net.WriteTable(veh.KeyValues)

		net.SendToServer()
	end
end

net.Receive("race_track_menu", OpenTrackEdit)
