include("autorun/config.lua")
local Fonts = {}
	Fonts["William_Bebas"] = "BebasNeue"
	
	
for a,b in pairs(Fonts) do
	for k=10,70 do
		surface.CreateFont( a .. "_S"..k,{font = b,size = k,weight = 300,antialias = true})
		surface.CreateFont( a .. "Out_S"..k,{font = b,size = k,weight = 300,antialias = true,outline = true})
	end
end


function GetIsPurge(um)
	IsPurge = um:ReadBool() or false
end
usermessage.Hook("IsPurge", GetIsPurge)

function GetPURGEMODE(um)
    PURGEMODE = um:ReadBool() or false
end
usermessage.Hook("PURGEMODE", GetPURGEMODE)

function PurgeClient()
	if PURGEMODE then

		net.Receive("PurgeCountdown", function()
        PURGEtime = net.ReadFloat() or 1
    	end)

    	PURGEtime = PURGEtime or 1


		--local time = SetPurgeTimer
		local minutes = math.floor(PURGEtime / 60)
		local sec = PURGEtime - (minutes * 60)
		local dots = ":"
		if sec < 10 then dots = ":0"
		end
		local actualtime = tonumber(minutes)..dots..tonumber(sec)

		local cin = (math.sin(CurTime()) + 1) / 2

		if IsPurge then
			draw.SimpleTextOutlined("The Purge Ends in", "William_Bebas_S27", ScrW()/2, 5, Color(255,255,255,255), TEXT_ALIGN_CENTER,0,1, Color( 100,0,0,255))
			if PURGEtime == PurgePlayPurgeSoundEnd then
				timer.Create("PurgeSoundDelay", 0.2, 1, function()	
					surface.PlaySound( PurgeEndSound )
				end)
			end
		else
			draw.SimpleTextOutlined("The Purge Starts in", "William_Bebas_S27", ScrW()/2, 5, Color(255,255,255,255), TEXT_ALIGN_CENTER,0,1, Color( 100,0,0,255))
			if PURGEtime == PurgePlayPurgeSoundStart then
				timer.Create("PurgeSoundDelay", 0.2, 1, function()	
					surface.PlaySound( PurgeStartSound )
				end)
			end


		end	

		draw.SimpleTextOutlined(actualtime, "William_Bebas_S27", ScrW()/2, 30, Color(255,255,255,255), TEXT_ALIGN_CENTER,0,1, Color( 100,0,0,255))

	end

end
hook.Add("HUDPaint", "PurgeTimerStuff", PurgeClient)

timer.Simple(10, PurgeClient)

hook.Add("PlayerInitialSpawn", "ShowPurgeOnJoin", PurgeClient)

