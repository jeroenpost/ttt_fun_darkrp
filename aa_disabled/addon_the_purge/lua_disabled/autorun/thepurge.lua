PURGEMODE = false


PurgeCountdownTimer = 60 * 90 -- Time until The Purge will start [Default: 60 * 60] 60 Minutes
PurgeEndCountdownTimer = 60 * 10 -- Time until The Purge will end [Default: 60 * 12] 12 minutes

PurgePlayPurgeSoundStart = 52 -- Seconds before The Purge starts, when it should play the starting sound [Default: 52] 52 Seconds
PurgePlayPurgeSoundEnd = 1 -- Seconds before The Purge ends, when it should play the end sound [Default: 1] 1 second

PurgeStartSound = "purge/thepurge.mp3" -- The Sound that should play before The Purge starts [Default: "purge/thepurge.mp3"]
PurgeEndSound = "purge/thepurgeend.mp3" -- The Sound that should play when The Purge ends [Default: "purge/thepurge.mp3"]

AddCSLuaFile("client/cl_thepurge.lua")

if SERVER then

    if (server_id == 361 or server_id == 99) then
        PURGEMODE = true
    else
        PURGEMODE = false
    end

    timer.Simple(5,function()
        if (server_id == 361 or server_id == 99) then
            PURGEMODE = true
        else
            PURGEMODE = false
        end

    end)

resource.AddFile( "sound/purge/thepurge.mp3" )
resource.AddFile( "sound/purge/thepurgeend.mp3" )	
resource.AddFile( "resource/fonts/bebasneue.ttf" )		

util.AddNetworkString("PurgeCountdown")

IsPurge = false

 SetPurgeTimer = PurgeCountdownTimer

function StartPurgeTimer()

	if PURGEMODE then

	
		timer.Create("countdownpurge", 1, 1, function()
			if IsPurge == false then
				SetPurgeTimer = SetPurgeTimer - 1
				StartPurgeTimer()


				if SetPurgeTimer < 1 then
					IsPurge = true
					SetPurgeTimer = PurgeEndCountdownTimer
					for k, v in pairs(player.GetAll()) do
					   v:PrintMessage( HUD_PRINTTALK, "The Purge Has Started!" )
					end
				end
			else
				SetPurgeTimer = SetPurgeTimer - 1
				StartPurgeTimer()

				if SetPurgeTimer < 1 then
					IsPurge = false
					SetPurgeTimer = PurgeCountdownTimer
					for k, v in pairs(player.GetAll()) do
					   v:PrintMessage( HUD_PRINTTALK, "The Purge Has Ended!" )
					end
				end

			end
		end)

		umsg.Start("IsPurge")
			umsg.Bool(IsPurge)
		umsg.End()

		net.Start("PurgeCountdown")
				net.WriteFloat( SetPurgeTimer )
		net.Broadcast()
	end

end

    hook.Add("PlayerInitialSpawn", "SendPurgeTimer", function( ply )
        net.Start("PurgeCountdown")
            net.WriteFloat( SetPurgeTimer )
        net.Send( ply )

        umsg.Start("IsPurge", ply)
			umsg.Bool(IsPurge)
		umsg.End()

        umsg.Start("PURGEMODE", ply)
             umsg.Bool(PURGEMODE)
        umsg.End()
    end)

timer.Simple(10, StartPurgeTimer)
end
