----------------------------------------------/
----/ Created by Temparh
----/ 16th November 2013
----/ ghost_configuration.lua
----/ As a part of the Ghost Mode addon
GhostMode = {} ---- DONT TOUCH

 




--[[________________________________________________________________

                         Ghost Settings
__________________________________________________________________]]--


--[[----------------------------------------------------
    ghosttime: How many seconds it takes to respawn
------------------------------------------------------]]-- 
GhostMode.ghosttime = 120


--[[------------------------------------------------------------------
    Performance: What should the balance between performance
    and effeciency be? 
    default is 100.
    Lower (e.g. 50) is better performance but slower at recognizing NLR break.
    Higher (e.g. 200) is higher efficiency and faster at recognizing NLR break
    but take up more server performance.

    Recommended to not go below 50.
-------------------------------------------------------------------]]-- 
GhostMode.Performance = 12.5


--[[-------------------------------------------------------------------------
    ghostinteract: Can a ghosted player interact with stuff? true/false
    should remain false
--------------------------------------------------------------------------]]-- 
GhostMode.ghostinteract = false


--[[------------------------------------------------------------------------------------------
    completeblock: Can a ghosted player open the F4 menu, changing jobs etc?
 ----------------------------------------------------------------------------------------]]-- 
GhostMode.completeblock = true


--[[-------------------------------------------------------------------------
    ghostspawnitems: Can a ghosted player spawn props? true/false
--------------------------------------------------------------------------]]-- 
GhostMode.ghostspawnitems = false


--[[--------------------------------------------------------
    ghostmaterial: How does a ghosted player look like?
    These are some of the material you can use:
    "models/wireframe" 
    "models/shadertest/shader3" 
    "models/effects/splodearc_sheet" 
    "models/effects/vol_light002"

    usematerial: If you want a custom material to a ghost
    set usermaterial to true, if not set it to false
---------------------------------------------------------]]-- 
GhostMode.ghostmaterial = "models/shadertest/shader3" 
GhostMode.usematerial = false  


--[[----------------------------------------------------
    ghostcolor: What color should ghosted players be
    Color( red, green, blue, transparency)
    Transparency is what you want to edit mostly
------------------------------------------------------]]--
GhostMode.ghostcolor = Color(0,0,0,180)


--[[----------------------------------------------------------
    ghostonneardeath: Should the player only become a ghost 
    if he nears his scene of death? true/false

    Do not set to false if banPlayer or kickPlayer is true.

    Recommended: true
------------------------------------------------------------]]--
GhostMode.ghostneardeath = true


--[[----------------------------------------------------------
    distancetoscene: How close to the players death scene
    does he have to be to become ghosted? default: 800
------------------------------------------------------------]]-- 
GhostMode.distancetoscene = 800


--[[----------------------------------------------------------
    noghostgroups: What players groups should not become a ghost
    when they die?
    Example: GhostMode.noghostgroups = { "superadmin", "admin", "moderator" }
------------------------------------------------------------]]-- 
GhostMode.noghostgroups = {}


--[[----------------------------------------------------------
    noghostjob: What job should not become a ghost when they die?
    Example: GhostMode.noghostjob = { TEAM_POLICE }

    IMPROVED WAY TO AVOID GHOST FOR A CERTAIN JOB

    Add "PlayerLoadout = function(ply) ply:GhostModeException(true) return false end,"
    in the job setup in jobrelated.lua

------------------------------------------------------------]]-- 
GhostMode.noghostjob = {}


--[[----------------------------------------------------------
    dostriction: Should this addon restrict you from doing stuff
    whenever you enter the area? or should it just make the ring for layout?
    true/false
    default: true
------------------------------------------------------------]]-- 
GhostMode.dostriction = true

 
--[[----------------------------------------------------------
    nlrrespawn: Should a player whos standing inside the NLR 
    area completely respawn when the zone he's standing in's NLR timer
    runs out?
    true/false
-----------------------------------------------------------]]--
GhostMode.nlrrespawn = false


--[[----------------------------------------------------------
    invincibleGhost: Should the ghosted player become
    invincible and unable to take damage when becoming a ghost?
    Can prevent players from seeking shelter in their NLR zone.
    true/false
-----------------------------------------------------------]]-- 
GhostMode.invincibleGhost = true 


--[[----------------------------------------------------------
    kickPlayer: Should a player be kicked from the server 
    if they try to enter their NLR zone?
    true / false

    kickMessage: What should the kick reason be.
-----------------------------------------------------------]]--
GhostMode.kickPlayer = false
GhostMode.kickMessage = "You are not allowed to return to your NLR zone"


--[[----------------------------------------------------------
    banPlayer: Should a player be banned from the server 
    if they try to enter their NLR zone? 
    true / false

    banTime: Bantime set in minutes.

    banMessage: What should the ban reason be.

    ulxBan: Should players be banned using the ULX system?
    Requires ULX installed.
    True/false 
-----------------------------------------------------------]]--
GhostMode.banPlayer = false 
GhostMode.banLength = 1 -- minutes
GhostMode.banReason = "You have been temporarily banned for breaking NLR"
GhostMode.ulxBan = true


--[[________________________________________________________________

                        Ghost Communication
__________________________________________________________________]]--

--[[--------------------------------------------------------------------------
    ghostshowchat: Can a player see the chat while being dead? true/false
---------------------------------------------------------------------------]]-- 
GhostMode.ghostshowchat = false
    

--[[--------------------------------------------------------------
    ghostmute: Can a player chat while being dead? true/false
---------------------------------------------------------------]]-- 
GhostMode.ghostchatmute = false 


--[[-----------------------------------------------------------------------
    ghostoocmute: Can a player chat in OOC while being dead? true/false
    This overrides ghostchatmute.
-----------------------------------------------------------------------]]-- 
GhostMode.ghostoocmute = true


--[[-------------------------------------------------------------------------
    ghostmutewhitelist: A list of words a player can say while a ghost
    format: GhostMode.ghostmutewhitelist = { "!noclip", "!menu", "hello" }
--------------------------------------------------------------------------]]-- 
GhostMode.ghostmutewhitelist = { "!noclip", "!menu", "hello"} 


--[[--------------------------------------------------------------
    ghosttalkmute: Can a player talk while being dead? true/false
---------------------------------------------------------------]]-- 
GhostMode.ghosttalkmute = true


--[[-------------------------------------------------------------------------
    notifyadmins: What ranks are notified when a player enter NLR area?
    for example: notifyadmins = { "superadmin", "admin" } 
--------------------------------------------------------------------------]]-- 
GhostMode.notifyadmins = {} 
 
 
--[[--------------------------------------------------------------
    playerkills: Does a player only become a ghost if he is killed
    by a player? Including himself? This means no propkill will ghost.
---------------------------------------------------------------]]-- 
GhostMode.playerkills = true



--[[________________________________________________________________

                         Ghost Layout & HUD 
__________________________________________________________________]]--


-- GHOST HUD
--[[---------------------------------------------------------------
    ghosttext: The text on the little HUD when you are a ghost
----------------------------------------------------------------]]-- 
GhostMode.ghosttext = "You are a ghost"


--[[------------------------------------------------------------------------ 
    ghosttextcolor: The color of the text appearing when being a ghost
    Color( red, green, blue, transparancy ) numbers can range from 1-255
-------------------------------------------------------------------------]]-- 
GhostMode.ghosttextcolor = Color(255,255,255,230)


--[[--------------------------------------------------------------------------
    ghosttextboxcolor: The color of the box containing the text when ghost
    Color( red, green, blue, transparency ) numbers can range from 1-255
----------------------------------------------------------------------------]]-- 
GhostMode.ghosttextboxcolor = Color(50,50,50,200) 


-- Respawn Text
--[[-----------------------------------------------------------------------------------
    respawntext: The text that appears after the player respawn from being a ghost
------------------------------------------------------------------------------------]]-- 
GhostMode.respawntext = "NLR Has Ceased"

 
--[[----------------------------------------------------------------------------------------------
    respawntextcolor: The color of the text after the player respawn from being a- ghost 
    Color( red, green, blue, transparency ) 
    i.e. Color( 255, 50, 50, 200 ) making the color a slighly red color with a bit transparency
-----------------------------------------------------------------------------------------------]]-- 
GhostMode.respawntextcolor = Color(0,0,0,200)


--[[---------------------------------------------------------------
    deathmarktext: The text that appears over where you died
    make it "" if you want to disable
----------------------------------------------------------------]]-- 
GhostMode.deathmarktext = "You died here"


--[[--------------------------------------------------------------------------
    deathmarkcolor: The color of the text that appears over where you died
--------------------------------------------------------------------------]]-- 
GhostMode.deathmarkcolor = Color(0,0,0,190)


--[[---------------------------------------------------------------
    deathmarkspeed: The speed of which the death mark spins in
    default: 25
----------------------------------------------------------------]]-- 
GhostMode.deathmarkspeed = 25


--[[--------------------------------------------------------------------------
    ghostcirclecolor: The color of the circle that you will ghost in.
--------------------------------------------------------------------------]]-- 
GhostMode.ghostcirclecolor = Color(255,255,255,255)


--[[--------------------------------------------------------------------------
    ghostcirclesize: The size of the dots that surround your death mark
    default: 3
    make it 0 if you want to disable
--------------------------------------------------------------------------]]-- 
GhostMode.ghostcirclesize = 3
    


--[[__________________________________________________________________________________

                             Version 1.72.
        Copy only this part into your configuration file to avoid
        overwriting old settings.
_____________________________________________________________________________________]]--


--[[--------------------------------------------------------------------------
    allowAdminSpecNLR: Are admins allowed to use the chat command "!allZones"
    to view ALL zones from players?
    true/false

    adminZoneCircleColor: If allowed, what color should the NLR zones admins see
    be to differeciate from own NLR zones?
---------------------------------------------------------------------------]]-- 
GhostMode.allowAdminSpecNLR = true

GhostMode.adminZoneCircleColor = Color(50, 150, 50, 255)


--[[--------------------------------------------------------------------------
    DrawSphere: Do you want a big sphere instead of just a circle?
    true/false

    CAUTION: THIS IS TAKES UP A LOT OF RESOURCES, USE ONLY IF YOU FEEL
    COMFORTABLE WITH IT.
--------------------------------------------------------------------------]]-- 
GhostMode.DrawSphere = false


--[[--------------------------------------------------------------------------
    NLRTimer: Do you want an NLR zone countdown displayed on the players 
    screen when he/she is within the zone?
--------------------------------------------------------------------------]]-- 
GhostMode.NLRTimer = true 


--[[--------------------------------------------------------------------------
    GhostMode.CanUseCMDs_Admins: A table of admins who can use chat commands
    like "!unnlr", "!nlr" and "!allZones".

    example:
    GhostMode.CanUseCMDs_Admins = {"admin", "superadmin", "owner"}
--------------------------------------------------------------------------]]-- 
GhostMode.CanUseCMDs_Admins = {"admin", "superadmin", "owner"}