----------------------------------------------/
----/ Created by Temparh
----/ 16th November 2013 
----/ cl_ghostmode.lua
----/ As a part of the Ghost Mode addon 


--[[---------------------------------------------------------------------------
	Includes and initial variables.
---------------------------------------------------------------------------]]--
DeathZones = {}  

include("ghost_configuration.lua")
include("extras/ghostmode_rpfix.lua")
 
local meta = FindMetaTable("Player") 


--[[---------------------------------------------------------------------------
	Player func: Is player ghost. BOOL
---------------------------------------------------------------------------]]--
function meta:IsGhost() 
	return self:GetNWBool("Ghost")
end 


--[[---------------------------------------------------------------------------
	Fonts.
---------------------------------------------------------------------------]]--
surface.CreateFont( "GhostFont", {
	font = "Arial",
	size = 30,
	weight = 500,
	antialias = true,
} )

surface.CreateFont( "BigGhostFont", {
	font = "Arial",
	size = 60,
	weight = 500,
	antialias = true,
} )

surface.CreateFont( "MonsterGhostFont", {
	font = "Arial",
	size = 160,
	weight = 500,
	antialias = true,
} )  


--[[---------------------------------------------------------------------------
	Ghost Information HUD (GIHUD)
---------------------------------------------------------------------------]]--
local wide = 600
local height = 75
local fade = 0
local shouldFade = false
hook.Add("HUDPaint", "paintGhost", function() 
	local ply = LocalPlayer()
	if ply:IsGhost() then
		boxheight = 75
		if GhostMode.NLRTimer then boxheight = 100 end
		draw.RoundedBox(0, ScrW()/2-(wide/2), 100, wide, boxheight, GhostMode.ghosttextboxcolor)
		draw.DrawText(GhostMode.ghosttext, "GhostFont",ScrW()/2, height+50, GhostMode.ghosttextcolor, TEXT_ALIGN_CENTER )

		if GhostMode.NLRTimer then
			timeLeft = 0
			zoneDistance = 50^5
			for k,v in pairs(DeathZones) do
				if zoneDistance > ply:GetPos():Distance(v["pos"]) then
					zoneDistance = ply:GetPos():Distance(v["pos"])
					timeLeft = v["time"]
				end
			end  
			draw.DrawText( "This NLR zone expires in ".. math.floor(math.max( (timeLeft + GhostMode.ghosttime) - CurTime(), 0)) .. " seconds" , "GhostFont",ScrW()/2, height+80, GhostMode.ghosttextcolor, TEXT_ALIGN_CENTER )
		end
	end
	local col = GhostMode.respawntextcolor

	draw.DrawText(GhostMode.respawntext, "BigGhostFont", ScrW()/2, ScrH()/2-100, Color(col.r,col.g,col.b,fade), TEXT_ALIGN_CENTER)
end)

 
--[[---------------------------------------------------------------------------
	Think: Contains fade math and F4Menu blocking.
---------------------------------------------------------------------------]]--
hook.Add("Think", "GhostThink", function() 
	local ply = LocalPlayer()
	local col = GhostMode.respawntextcolor
	if shouldFade then
		if fade == 0 then
			shouldFade = false
		elseif PreFaded && fade != col.a then
			fade = fade + 1
		else
			fade = fade - 1
		end
		if fade == col.a then
			timer.Simple(2, function() PreFaded = false end)
		end
	end
	 
	if ply:IsGhost() && GhostMode.completeblock then
		if DarkRP.getF4MenuPanel() != nil then
			DarkRP.getF4MenuPanel():Hide()
		end
	end

end)


--[[---------------------------------------------------------------------------
	Render: Draw 3D2D NLR zones.
---------------------------------------------------------------------------]]--
hook.Add("PostDrawOpaqueRenderables", "3D2DStuff", function() 
	local spinspeed = GhostMode.deathmarkspeed / 100   
	if angle == nil then
		angle = 0
	else
		angle = angle + spinspeed  
	end 

	for k,v in pairs(DeathZones) do 
		 
		-- Put nickname on grave if admin_AllZones
		graveText = GhostMode.deathmarktext   
		if v["nick"] then graveText = v["nick"] end   

		-- Change color if circle is admin_AllZones 
		circleColor = GhostMode.ghostcirclecolor
		if v["nick"] then circleColor = GhostMode.adminZoneCircleColor end   

		cam.Start3D2D( v["pos"]+Vector(0,0,50), Angle(0,angle,90), 0.1)
			draw.DrawText( graveText, "MonsterGhostFont",5, 5, GhostMode.deathmarkcolor, TEXT_ALIGN_CENTER)
		cam.End3D2D()
		cam.Start3D2D(  v["pos"]+Vector(0,0,50), Angle(0,angle-180,90), 0.1)
			draw.DrawText( graveText, "MonsterGhostFont",5, 5, GhostMode.deathmarkcolor, TEXT_ALIGN_CENTER)
		cam.End3D2D()

		cam.Start3D2D(  v["pos"]+Vector(0,0,40), Angle(0,0,0), GhostMode.ghostcirclesize )
			local dimension = GhostMode.distancetoscene / GhostMode.ghostcirclesize
			surface.DrawCircle( 0, 0, dimension, circleColor )  
		cam.End3D2D()

		if GhostMode.DrawSphere then 
			roll = 0

			--roll
			for i=1,12 do
				cam.Start3D2D( v["pos"]+Vector(0,0,0), Angle(90,0,roll), GhostMode.ghostcirclesize )
					local dimension = GhostMode.distancetoscene / GhostMode.ghostcirclesize
					surface.DrawCircle( 0, 0, dimension, circleColor )
				cam.End3D2D()
				roll = roll + 15
			end


			x_sqr = GhostMode.distancetoscene * GhostMode.distancetoscene

			local segments = 10
			move =  GhostMode.distancetoscene  / segments

			for i=1,segments-1 do
				moveUp = (move * i) * (move * i)
				newRadius = math.sqrt( x_sqr - moveUp )
				cam.Start3D2D( v["pos"]+Vector(0,0,move*i), Angle(0,0,0), GhostMode.ghostcirclesize )
					local dimension = newRadius / GhostMode.ghostcirclesize	
					surface.DrawCircle( 0, 0, dimension, circleColor )  
				cam.End3D2D()
			end
		end
	end
end)  


--[[---------------------------------------------------------------------------
	Netstream: Queue GIHUD fade-out.
---------------------------------------------------------------------------]]--
net.Receive("StartEndGhost", function(len)
	PreFaded = true
	timer.Simple(1, function() shouldFade = true fade = 1 end) 
end)	
 

--[[---------------------------------------------------------------------------
 	Netstream: Remove a specific NLR zone from client rendering.
---------------------------------------------------------------------------]]--
net.Receive("RemoveCertainZone", function(len)
	local vec = net.ReadVector()

	for key,zone in pairs(DeathZones) do
		if vec == zone["pos"] then
			table.remove(DeathZones, key)
		end
	end
end)


--[[---------------------------------------------------------------------------
	Netstream: Purge all NLR zones from client rendering.
---------------------------------------------------------------------------]]--
net.Receive("RemoveDeathZones", function(len)
	table.Empty(DeathZones)
end)


--[[---------------------------------------------------------------------------
	Netstream: Add a NLR zone for client rendering.
---------------------------------------------------------------------------]]--
net.Receive("AddDeathZone", function(len)
	local ply = LocalPlayer()
	local vector = net.ReadVector()
	local admin_OwnerNick = net.ReadString() or false

	ply:SetNWVector("DeathScene", ply:GetNWVector("CacheDeathScene"))
	local zone = { sid = ply:SteamID(), pos = vector, time = CurTime()}

	-- Check if we are dealing with an admin zone.
	if net.ReadBit() == 1 then
		zone = { sid = ply:SteamID(), pos = vector, time = CurTime(), nick = admin_OwnerNick}
	end


	table.insert(DeathZones, zone)
end) 

 
