 ----------------------------------------------/
----/ Created by Temparh
----/ January 10th 2015
----/ sv_ghostmode.lua
----/ As a part of the GhostMode addon
----/ All rights reserved 


--[[---------------------------------------------------------------------------
	Includes and initial variables.
---------------------------------------------------------------------------]]--

-- File downloads. 
AddCSLuaFile("ghost_configuration.lua") 
AddCSLuaFile("extras/ghostmode_rpfix.lua")

-- File includes.
include("ghost_configuration.lua")
include("extras/ghostmode_rpfix.lua")
 
-- .net caches
util.AddNetworkString("StartEndGhost")
util.AddNetworkString("AddDeathZone") 
util.AddNetworkString("RemoveDeathZones") 
util.AddNetworkString("RemoveCertainZone")
  
local meta = FindMetaTable("Player")  
  
-- GhostMode.DeathZones holds ALL active zones on the server.
GhostMode.DeathZones = {}  


--[[---------------------------------------------------------------------------
	Player func: Flag this player and excepting to ghostmode.
---------------------------------------------------------------------------]]--
function meta:GhostModeException(n)
	if not self:IsPlayer() then return end
	self:SetNWBool("GhostMode.Exception", n)  
end


--[[---------------------------------------------------------------------------
	Player func: Check if player has ghost-mode admin previliges.
---------------------------------------------------------------------------]]--
function meta:IsGhostModeAdmin()

		if gb.is_jrmod(self) then
			return true
		end

	return false
end


--[[---------------------------------------------------------------------------
	Hook: Revoke exception flag if job change occurs.
---------------------------------------------------------------------------]]--
hook.Add("OnPlayerChangedTeam", "GhostModeNLR_exception", function(ply, old, new)
    ply.weaponlist = {}
	ply:GhostModeException(false)
end)


--[[---------------------------------------------------------------------------
	Hook: Death of a player. Preparing ghost-mode.
---------------------------------------------------------------------------]]--
hook.Add("PlayerDeath", "GhostMode", function(ply, weapon, killer)
	-- If player is in an exception group
	for k,v in pairs(GhostMode.noghostgroups) do 
		if ply:IsUserGroup(v) then
			return -- Don't apply ghostmode
		end 
	end 

	-- If only player killing, but not killed by player 
	if !killer:IsPlayer() && GhostMode.playerkills then 
		return -- Don't apply ghostmode
	end

	if ply:GetNWBool("GhostMode.Exception") then
		return
	end
 
	-- If player is in a job excepted from Ghosting
	for k,v in pairs(GhostMode.noghostjob) do
		if ply:Team() == v then
			return -- Dont apply ghostmode.
		end
	end


	-- Prepare to apply GhostMode on player.
	ply:SetNWBool("CheckGhost", true) 
	if GhostMode.ghostneardeath then 
		ply:SetNWVector("CacheDeathScene", ply:GetPos())
	end
end)
 

--[[---------------------------------------------------------------------------
	Hook: Updating reconnecting players of NLR zones.
---------------------------------------------------------------------------]]--
hook.Add("PlayerInitialSpawn", "UPDATE ZONE", function(ply)
	ply:SetNWBool("Ghost", false)
	if #GhostMode.DeathZones > 1 then return end
	timer.Simple(1, function()
		for k,v in pairs(GhostMode.DeathZones) do
			if v["sid"] == ply:SteamID() then 	
				net.Start("AddDeathZone") 
					net.WriteVector(v["pos"])
				net.Send(ply)
			end  
		end
	end) 
end)


--[[---------------------------------------------------------------------------
	Hook: Player spawn, NLR update and timers.
---------------------------------------------------------------------------]]--
hook.Add("PlayerSpawn", "ghostmode", function(ply) 
	if ply:GetNWBool("CheckGhost") then
		if timer.Exists("Ghost"..ply:SteamID()) then
			timer.Destroy("Ghost"..ply:SteamID())
		end
 
		if GhostMode.ghostneardeath then
			ply:SetNWVector("DeathScene", ply:GetNWVector("CacheDeathScene"))
			local zone = { sid = ply:SteamID(), pos = ply:GetNWVector("CacheDeathScene"), time = CurTime() }
			table.insert(GhostMode.DeathZones, zone)
 
			-- Send death zone info to be drawn on client
			net.Start("AddDeathZone") 
				net.WriteVector(ply:GetNWVector("CacheDeathScene")) 
			net.Send(ply)
			 
			-- Reset death zone info for admins watching zones 
			sendList = GhostMode.createSeeAllList() 
			for p=1,#sendList do
				admin_SeeAllZones(sendList[p], false)
				admin_SeeAllZones(sendList[p], false)
			end

		end
		ply:SetNWBool("CheckGhost", false)
		ply:SetNWBool("SecretGhost", true)

		-- Ghost on spawn, unless kicking or ban is enabled.
		if !GhostMode.ghostneardeath then
			ply:EnableGhost()
		end  
		
		timer.Create("Ghost"..ply:SteamID(), GhostMode.ghosttime, 1, function() 
			if IsValid(ply) then
				if !GhostMode.ghostneardeath then
					ply:SetNWBool("Ghost", false)
					ply:DisableGhost()
					net.Start("StartEndGhost")
					net.Send(ply)
					ply:Spawn()
					ply:SetNWBool("SecretGhost", false)
				end
			end
		end)

	end
end)


--[[---------------------------------------------------------------------------
	DarkRP WA: Loadouts
---------------------------------------------------------------------------]]--
hook.Add("PlayerLoadout", "ghostloadout", function(ply)
	if ply:IsGhost() then
		return true
	end
end)
hook.Add("OnPlayerChangedTeam", "ghostmode", function(ply)
	if ply:IsGhost() then
		timer.Simple(0.1, function() 
			ply:StripWeapons()
			ply:DisableGhost()
			ply:EnableGhost()
		end) 
	end
end)


--[[---------------------------------------------------------------------------
	Hook: Disable player collission.
---------------------------------------------------------------------------]]--
hook.Add("ShouldCollide", "dfd", function(ent1, ent2)
	if ent1:IsPlayer() && ent2:IsPlayer() then
		if ent1:IsGhost() or ent2:IsGhost() then
			return false
		end
	end 
end)





--[[---------------------------------------------------------------------------
	If GM is reloaded, rerun timers.
---------------------------------------------------------------------------]]--
if timer.Exists("NLRCheckUp") then timer.Destroy("NLRCheckUp") end


--[[---------------------------------------------------------------------------
	Internal: Assemble a list of all zones and send them to admins.
---------------------------------------------------------------------------]]--
function GhostMode.createSeeAllList(ply)
	local sendList = {}
	for k,v in pairs(player.GetAll()) do
		if v:GetNWBool("admin_SeeAllZones") then
			table.insert(sendList, v)
		end
	end
	if ply then table.insert(sendList, ply) end
	return sendList
end



--[[---------------------------------------------------------------------------
	Timer: Think-like-func to check for NLR breach and exit.
---------------------------------------------------------------------------]]--
timer.Create("NLRCheckUp", 25 / GhostMode.Performance , 0, function() -- THIS IS THE LINE YOU WANT TO LOOK AT IF YOU WANNA ALTER PERFORMANCE OF THE SCRIPT.

	for k,v in pairs(player.GetAll()) do
 
		if v:IsGhost() then  
			if table.Count(v:GetWeapons()) != 0 then
				v:StripWeapons()
			end 
		end  

		-- HANDLING EXPIRED ZONES
		if GhostMode.DeathZones != nil then -- Check all zones
			for key,deathzones in pairs(GhostMode.DeathZones) do
				if deathzones["time"] + GhostMode.ghosttime < CurTime() then -- if zone IS expired
					for _,ply in pairs(player.GetAll()) do 
						if deathzones["sid"] == ply:SteamID() then -- Search all players and find its owner
							if ply:GetPos():Distance( deathzones["pos"] ) < GhostMode.distancetoscene then -- If player is within zone, ghost him.
								if GhostMode.nlrrespawn then  
									ply:Spawn()
								end   
							end  

							sendList = GhostMode.createSeeAllList(ply)
							-- REMOVE ZONE FROM CLIENT
							net.Start("RemoveCertainZone")
								net.WriteVector(deathzones["pos"])
							net.Send(sendList) 
						end
					end 


					table.remove(GhostMode.DeathZones, key)
				end
			end
 		end

		-- ZONE CHECKING 
		local shouldbeghost = false
		if GhostMode.DeathZones != nil then 
			for _,zones in pairs(GhostMode.DeathZones) do
				if zones["sid"] == v:SteamID() then -- If zone we're checking belongs to player we're checking.
					local pos = zones["pos"]
					-- If player is within the range of NLR zone, they should become ghosted
					if v:GetPos():Distance(pos) < GhostMode.distancetoscene then
						shouldbeghost = true
						v:SetNWVector("GhostMode_CurrentZone", Vector(zones["pos"].x, zones["pos"].y, zones["pos"].z))
					end
				end
			end
		end

		-- ACTUAL GHOSTING and ADMIN NOTIFY
		if !v:IsGhost() && shouldbeghost then -- If player is not a ghost already but is inside their zone
			v:EnableGhost() -- Make them a ghost
			if !v:GetNWBool("JustExitedNLR") then -- If player didn't recently enter NLR zone before this entering
				NotifyNLRBreach(v) -- Notify admins
			end
		end

		-- UNGHOSTING 
		if v:IsGhost() && !shouldbeghost && !v:GetNWBool("AdminNLR") then -- If player is a ghost but shouldn't be 
			v:DisableGhost() -- Unghost
			v:SetNWBool("JustExitedNLR", true) -- Set just exited to true to remove admin notify spam
 
 			-- Create anti-spam of notifications
			if !(timer.Exists("ExitNLR"..v:SteamID())) then
				timer.Create("ExitNLR"..v:SteamID(), 5, 1, function() 
					v:SetNWBool("JustExitedNLR", false)  
					timer.Destroy("ExitNLR"..v:SteamID())
				end)
			end
		end 
	end 
end) 
 

--[[---------------------------------------------------------------------------
 	Notify admins of NLR breach.
---------------------------------------------------------------------------]]--
function NotifyNLRBreach(breacher)
	if #GhostMode.notifyadmins < 1 then return end

	for _,ply in pairs(player.GetAll()) do
		for _, group in pairs(GhostMode.notifyadmins) do
			if ply:IsUserGroup(group) then
 				ply:ChatPrint(breacher:Nick().." just entered their NLR zone.")
			end
		end
	end
end


--[[---------------------------------------------------------------------------
	Player func: Enable ghost effects
---------------------------------------------------------------------------]]--
function meta:EnableGhost() 
	-- Kick player for entering NLR zone.
	if (GhostMode.kickPlayer) then
		self:Kick(GhostMode.kickMessage)
		return
	end

	-- Ban players for entering NLR zone
	if (GhostMode.banPlayer) then
		if (GhostMode.ulxBan) then
			RunConsoleCommand("ulx", "banid", self:SteamID(), GhostMode.banLength, GhostMode.banReason)
		else
			self:Ban(GhostMode.banLength, false)
			self:Kick(GhostMode.banReason)
		end
		return
	end


	-- Ammo saving
	self.ammolist = {}
	self.ammoListCounted = {}
	for _,weapon in pairs(self:GetWeapons()) do
		if self:GetAmmoCount(weapon:GetPrimaryAmmoType()) > 1 && !table.HasValue(self.ammoListCounted,weapon:GetPrimaryAmmoType()) then
			local ammo = { weapon:GetPrimaryAmmoType(), self:GetAmmoCount(weapon:GetPrimaryAmmoType()) }
			table.insert(self.ammolist,  ammo)
			table.insert(self.ammoListCounted, weapon:GetPrimaryAmmoType())
		end
	end

	-- Weapon saving
	self.weaponlist = {}
	for _,weapon in pairs(self:GetWeapons()) do
		table.insert(self.weaponlist, weapon:GetClass())
	end
	self:SetNWBool("Ghost", true)

	
	self:SetRenderMode(RENDERMODE_TRANSALPHA)
	self:SetNWInt("LastRender", self:GetRenderMode())
	self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	self.Babygod = GhostMode.invincibleGhost

	-- Material
	if GhostMode.usematerial then
		self:SetNWString("LastMaterial", self:GetMaterial())
		self:SetMaterial(GhostMode.ghostmaterial)
	end
	
	-- Strip all weapons.
	self:StripWeapons() 
 
	if GhostMode.invincibleGhost then
		self:GodEnable()
	end

	-- Custom colors
	local c = self:GetColor() 
	self:SetRenderMode(RENDERMODE_TRANSALPHA)
	self:SetColor(GhostMode.ghostcolor)
end


--[[---------------------------------------------------------------------------
	Player func: Remove ghost effects
---------------------------------------------------------------------------]]--
function meta:DisableGhost()
	-- Ghosting is disabled if you just ban/kick
	if GhostMode.banPlayer || GhostMode.kickPlayer then return end

	self:SetNWBool("Ghost", false)
	self:SetColor(Color(255,255,255,255))

	if GhostMode.usematerial then
		self:SetMaterial( self:GetNWString("LastMaterial") )
	end
	
	-- Make the player normal again and return weapons.
	self:GodDisable()
	self:SetCollisionGroup(COLLISION_GROUP_PLAYER)
	self.Babygod = false
	if self.weaponlist != nil then
		for _,otherweapon in pairs(self.weaponlist) do
			self:Give(tostring(otherweapon))
		end 
		self:SwitchToDefaultWeapon( )
	end


	-- Give back ammo
	self:RemoveAllAmmo()
	if self.ammolist != nil then
		for _,ammo in pairs(self.ammolist) do
			self:GiveAmmo(ammo[2], ammo[1])
		end
	end
	
end


--[[---------------------------------------------------------------------------
	Player func: Is player ghost.
---------------------------------------------------------------------------]]--
function meta:IsGhost() 
	return self:GetNWBool("Ghost")
end


--[[---------------------------------------------------------------------------
	Admin Command: Un-Ghost Player
---------------------------------------------------------------------------]]--
function UnGhostCommand(ply, args)
	-- Ghosting is disabled if you just ban/kick

	if !ply:IsGhostModeAdmin() then return end
	local len = string.len(args)
	local name = string.sub(args, 1, len)
	target = TempFindPlayer(name) or false
	if target then
		net.Start("RemoveDeathZones")
		net.Send(target)
		--[[
		for key, zones in pairs(GhostMode.DeathZones) do
			if zones["sid"] == target:SteamID() then
				
				zones["time"] = 0
			end
		end
]]--

		local deathzones = GhostMode.DeathZones

		for i = #deathzones, 1, -1 do
			if deathzones[i]["sid"] == target:SteamID() then
				table.remove(deathzones, i)
			end
		end

		target:SetNWBool("AdminNLR", false)
		if not (GhostMode.banPlayer || GhostMode.kickPlayer) then 
			target:DisableGhost()
			target:SetNWBool("SecretGhost", false)
			target:SetNWVector("DeathScene", Vector(0,0,0)) 
		end
		net.Start("StartEndGhost")
		net.Send(target)
		
	end
	return ""
end


--[[---------------------------------------------------------------------------
	Admin command: Ghost player. 
---------------------------------------------------------------------------]]--
function GhostCommand(ply, args)
	if GhostMode.banPlayer || GhostMode.kickPlayer then return end -- kick and or punish is enabled, command is useless.
	if !ply:IsGhostModeAdmin() then return end -- ADMINS ONLY
	local len = string.len(args)
	local name = args
	target = TempFindPlayer(name) or false
	if target:IsValid() then

		target:EnableGhost()
		target:SetNWBool("AdminNLR", true) -- AdminNLR overrides everything. Can only be removed by an admin.

	end

	return ""
end

concommand.Add("ghostmode_unghost", function(ply, cmd, args, argsString)
	UnGhostCommand(ply, argsString)
end)

concommand.Add("ghostmode_ghost", function(ply, cmd, args, argsString)
	GhostCommand(ply, argsString)
end)


--[[---------------------------------------------------------------------------
	RP Blocks and restrictions
---------------------------------------------------------------------------]]--

local blockings = {
	{"PlayerUse", 			"ghostinteract"},
	{"PlayerSpawnEffect", 	"ghostspawnitems"},
	{"PlayerSpawnNPC", 		"ghostspawnitems"},
	{"PlayerSpawnObject", 	"ghostspawnitems"},
	{"PlayerSpawnProp", 	"ghostspawnitems"},
	{"PlayerSpawnRagdoll", 	"ghostspawnitems"},
	{"PlayerSpawnSENT", 	"ghostspawnitems"},
	{"PlayerSpawnSWEP", 	"ghostspawnitems"},
	{"PlayerSpawnVehicle", 	"ghostspawnitems"},
	{"PlayerCanJoinTeam", 	"completeblock"}
};

local function itterateBlockings()
	if !#blockings then return end;
	for i=0, #blockings do
		local hook 		= blockings[i][0];
		local setting 	= blockings[i][1];
		hook.Add(hook, hook + "_" + setting, function(ply)
			if ply:IsGhost() && !GhostMode[setting] then return false end;
		end);
	end
end

hook.Add("PlayerCanHearPlayersVoice", "GHOSTBLOCK", function(listener, talker)	
	if talker:IsGhost() && GhostMode.ghosttalkmute then
		return false 
	end 
end) 
  
hook.Add("PlayerGiveSWEP", "GHOSTBLOCK", function(ply)	
	if ply:IsGhost() && !GhostMode.ghostspawnitems && (!ply:IsAdmin() || !ply:IsSuperAdmin()) then
		return false  
	end
end)  

hook.Add("PlayerCanSeePlayersChat", "GHOSTBLOCK", function(text, teamonly, listener, speaker)
	if listener:IsGhost() && !GhostMode.ghostshowchat then
		return false
	end
end)

hook.Add("CanPlayerSuicide", "GHOSTBLOCK", function(ply)
	if ply:IsGhost() or (ply:GetNWBool("SecretGhost") && GhostMode.dostriction) then
	--	return false
	end
end) 


--[[---------------------------------------------------------------------------
	Admin Command: See all NLR zones.
---------------------------------------------------------------------------]]--
function admin_SeeAllZones(ply, client)
	if !ply:IsGhostModeAdmin() then return end -- ADMIN ONLY
	if not GhostMode.allowAdminSpecNLR then return end -- Only if allowed by config
	client = client or true
	if (!isbool(client)) then client = true end

	seeZones = false

	if ply:GetNWBool("admin_SeeAllZones") then
		ply:SetNWBool("admin_SeeAllZones", false)
		if client then ply:ChatPrint("Disabled seeing other players' zones.") end
	else 
		ply:SetNWBool("admin_SeeAllZones", true)
		if client then ply:ChatPrint("Enabled seeing other players' zones.") end
		seeZones = true
	end
 

	for i=1,#GhostMode.DeathZones do
		local zone = GhostMode.DeathZones[i]
		if zone["sid"] != ply:SteamID() then -- Send all zones to client except our own. 
			-- If we're supposed to see zones
			if seeZones && #GhostMode.DeathZones > 0 then
				
				-- Find nickname of zone owner.
				name = "Unkown player's grave" -- if we for some reason cant find the name of the player just leave it unknown
				for k,v in pairs(player.GetAll()) do
					if v:SteamID() == zone["sid"] then
						name = v:Nick()
					end
				end
				
				-- Send data to client
				net.Start("AddDeathZone") 
					net.WriteVector(zone["pos"])
					net.WriteString(name)
					net.WriteBit(1)
				net.Send(ply)
			else
				net.Start("RemoveCertainZone")
					net.WriteVector(zone["pos"])
				net.Send(ply)
			end
		end
	end 

end


--[[---------------------------------------------------------------------------
	Internal: Stop chat command spam.
---------------------------------------------------------------------------]]--
function GhostMode.RecentChatCommand(ply)
	ply:SetNWBool("GhostMode_Recent_ChatCommand", true)

	timer.Create("GhostMode_Recent_ChatCommand_Timer", 2, 1, function()
		ply:SetNWBool("GhostMode_Recent_ChatCommand", false)
	end)
end


--[[---------------------------------------------------------------------------
	Chat Command Setup.
---------------------------------------------------------------------------]]--
GhostMode.tChatCMD = {}
function GhostMode.AddChatCommand(cmd, callback)
	local n = {cmd = cmd, callback = callback}
	GhostMode.tChatCMD[#GhostMode.tChatCMD+1] = n
end 

GhostMode.AddChatCommand("!nlr", GhostCommand)
GhostMode.AddChatCommand("!unnlr", UnGhostCommand)
GhostMode.AddChatCommand("!allZones", admin_SeeAllZones)
 
hook.Add("PlayerSay", "GhostMode_handleChatCMD", function(ply, text)
	if (ply:GetNWBool("GhostMode_Recent_ChatCommand")) then return end
    if(gb.is_mod(ply))then
        for i=1,#GhostMode.tChatCMD do
            local cmd 		= GhostMode.tChatCMD[i]["cmd"]
            local callback 	= GhostMode.tChatCMD[i]["callback"]

            if (string.Left(text, string.len(cmd)) == cmd) then
                callback(ply, string.sub(text, string.len(cmd)+2))
                GhostMode.RecentChatCommand(ply)
                return ""
            end
        end
	end
end)


--[[---------------------------------------------------------------------------
	DarkRP WA: Disable weapon drop if ghost.
---------------------------------------------------------------------------]]--
hook.Add("PlayerSay", "GHOSTBLOCK", function(ply, text) 	

	-- Blocking weapon drop while a ghost. DarkRP hates developers.
	cmd = "/drop"
	hitCmd = (string.sub(text,1,string.len(cmd))) == cmd
	sub = string.sub(text, string.len(cmd)+1 )
	if hitCmd then
		if GhostMode.DeathZones != nil then
			for _,zones in pairs(GhostMode.DeathZones) do
				if zones["sid"] == ply:SteamID() then
					local pos = zones["pos"]
					if ply:GetPos():Distance(pos) < GhostMode.distancetoscene+500 then
						ply:ChatPrint("You cannot drop weapons this close to where you died.")
						return false
					end
				end
			end
		end 
	end

	-- Player mute + exceptions
	if ply:IsGhost() && GhostMode.ghostchatmute then
		if (string.Left(text, 3) == "-- " && GhostMode.ghostoocmute) then -- if player talks in ooc and is not allowed to
			return -- Dont allow it
		end

		for k,v in pairs(GhostMode.ghostmutewhitelist) do -- Chat saying whitelist.
			if v == text then
				return
			end 
		end
		return ""
	end
end)


--[[---------------------------------------------------------------------------
	DarkRP WA: ConCommand hack for streamlining v2.4 and v2.5
	THESE COMMANDS ARE TAKEN FROM FPTjes DarkRP 2.4
---------------------------------------------------------------------------]]--
local function ConCommand(ply, _, args)
	if not args[1] then return end
	if ply:IsGhost() then return end

	local cmd = string.lower(args[1])
	local arg = table.concat(args, ' ', 2)
	local tbl = DarkRP.getChatCommand(cmd)
	local time = CurTime()

	if not tbl then return end

	ply.DrpCommandDelays = ply.DrpCommandDelays or {}

	if tbl.delay and ply.DrpCommandDelays[cmd] and ply.DrpCommandDelays[cmd] > time - tbl.delay then
		return
	end

	ply.DrpCommandDelays[cmd] = time

	tbl.callback(ply, arg)
end

function TempFindPlayer(info)
	if not info or info == "" then return nil end
	local pls = player.GetAll()

	for k = 1, #pls do -- Proven to be faster than pairs loop.
		local v = pls[k]
		if tonumber(info) == v:UserID() then
			return v
		end

		if info == v:SteamID() then
			return v
		end

		if string.find(string.lower(v:SteamName()), string.lower(tostring(info)), 1, true) ~= nil then
			return v
		end

		if string.find(string.lower(v:Name()), string.lower(tostring(info)), 1, true) ~= nil then
			return v
		end
	end
	return nil
end


--[[---------------------------------------------------------------------------
	DarkRP and Garry's Mod WA: This has a hard time loading or even
	checking game hooks, so we rely on timers for odd reasons. (Temporary)
---------------------------------------------------------------------------]]--
timer.Simple(2, function()
	concommand.Add("darkrp", ConCommand)

	print("GhostMode Fully Loaded!")
end)