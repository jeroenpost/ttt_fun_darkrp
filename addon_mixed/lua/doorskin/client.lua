DOORSKIN.Episodic = false

local fontstandard = "Roboto"
surface.CreateFont("doorSkin_Large", {
	font = fontstandard,
	size = 32,
	antialias = true,
	weight = 800
})
surface.CreateFont("doorSkin_Medium", {
	font = fontstandard,
	size = 24,
	antialias = true,
	weight = 800
})

surface.CreateFont("doorSkin_Small", {
	font = fontstandard,
	size = 20,
	antialias = true,
	weight = 800
})
surface.CreateFont("doorSkin_Tiny", {
	font = fontstandard,
	size = 12,
	antialias = true,
	weight = 600
})

if DOORSKIN.Episodic then
	DOORSKIN.NumBG = 3
else
	DOORSKIN.NumBG = 2
end

function DOORSKIN:ShadowText( text, font, x, y, col, ax, ay , d)
	draw.DrawText( text, font, x+d, y+d, Color(0,0,0,col.a), ax, ay )
	draw.DrawText( text, font, x, y, col, ax, ay)
end

local blur = Material("pp/blurscreen")
function DOORSKIN:DrawBlur(panel, amount)

	local x, y = panel:LocalToScreen(0,0)
	local w, h = ScrW(), ScrH()

	surface.SetDrawColor(255,255,255)
	surface.SetMaterial(blur)

	for i = 1, 3 do -- 3 pass blur i guess?
		blur:SetFloat("$blur", (i/3) * (amount or 7))
		blur:Recompute()

		render.UpdateScreenEffectTexture()

		surface.DrawTexturedRect(x*-1,y*-1,w,h)
	end
end

function DOORSKIN:DoorMenu( door )

	if IsValid( door ) then
		if not ((door:GetModel() == "models/props_c17/door01_left.mdl") or (door:GetModel() == "models/props_doors/door03_slotted_left.mdl")) then
			return
		end
	end

	local frame = vgui.Create("doorSkin_main") -- dimensions: 384, 512

	local w, h = 384, 512

	frame:SetSize( w, h+32 )
	frame.doorEnt = door

	local buttons = {}

	frame.model = vgui.Create("DModelPanel", frame)
	frame.model:SetSize( w - (w/8)*2 - 16, h-12 - 32 )
	frame.model:SetPos( 8 + w/8, 32 + 32 )
	frame.model:SetModel( door:GetModel() )

	buttons.next = vgui.Create("ari_button", frame)
	buttons.next:SetSize( w/2 - 8, 28 )
	buttons.next:SetPos( w/2+1, 34 )
	buttons.next:SetText("Next Skin")
	--buttons.next:SetColors( DOORSKIN.Colors.GoodDark, DOORSKIN.Colors.Good )

	buttons.prev = vgui.Create("ari_button", frame)
	buttons.prev:SetSize( w/2 - 8, 28 )
	buttons.prev:SetPos( 7, 34 )
	buttons.prev:SetText("Last Skin")
	--buttons.prev:SetColors( DOORSKIN.Colors.GoodDark, DOORSKIN.Colors.Good )

	buttons.nextb = vgui.Create("ari_button", frame)
	buttons.nextb:SetSize( w/2 - 8, 28 )
	buttons.nextb:SetPos( w/2+1, 34 + 2 + 28 )
	buttons.nextb:SetText("Next Bodygroup")
	--buttons.next:SetColors( DOORSKIN.Colors.GoodDark, DOORSKIN.Colors.Good )

	buttons.prevb = vgui.Create("ari_button", frame)
	buttons.prevb:SetSize( w/2 - 8, 28 )
	buttons.prevb:SetPos( 7, 34+2+28 )
	buttons.prevb:SetText("Last Bodygroup")
	--buttons.prev:SetColors( DOORSKIN.Colors.GoodDark, DOORSKIN.Colors.Good )

	--aesthetics:
	frame.model:SetFOV( 22 )
	frame.model:SetCamPos( Vector(200,0,-64) )
	frame.model:SetLookAt( Vector(0,23,-5) )

	frame.model.ang = math.pi*2
	frame.model.startAnim = CurTime()
	frame.model.startAng = math.pi
	frame.model.ticker = 0
	frame.model.rotatespeed = 0.0025
	frame.model.varspeed = 0.2
	frame.model.lastFrame = 0

	function frame.model:LayoutEntity( ent )
		local dt = CurTime() - self.lastFrame
		self.lastFrame = CurTime()
		--ent:SetAngles( Angle(0,self.ang,0) )

		local frac = InverseLerp( CurTime(), self.startAnim, self.startAnim + 1 )
		if frac > 1 then frac = 1 end
		if frac < 0 then frac = 0 end
		
		self.ang = QuadLerp( frac, self.startAng, math.pi*2)
		

		self:SetCamPos( Vector(math.cos(self.ang)*200, math.sin(self.ang)*200 +23, -64) )

		-- let's make it kinda move nicely when the mouse moves:
		local modAng = Angle(0,0,0)

		local mX, mY = gui.MouseX(), gui.MouseY()
		local cX, cY = self:LocalToScreen( self:GetWide()/2, self:GetTall()/2 ) -- center of the panel
		local dX, dY = mX - cX, mY - cY -- displacement of the mouse

		local scale = 0.01
		modAng.pitch = dY*scale
		modAng.yaw = dX*scale*2

		ent:SetAngles( modAng )

	end

	-- let's get the door data now.
	local doordata = {}
	doordata.skin = door:GetSkin()
	doordata.skincount = door:SkinCount()
	doordata.bodygroup = door:GetBodygroup( 1 )

	frame.model:GetEntity():SetSkin( doordata.skin )
	frame.model:GetEntity():SetBodygroup( 1, doordata.bodygroup )

	function buttons.next:DoClick()
		local curSkin = self:GetParent().model:GetEntity():GetSkin()
		curSkin = curSkin + 1
		if curSkin > self:GetParent().model:GetEntity():SkinCount() then
			curSkin = 1
		end
		self:GetParent().model:GetEntity():SetSkin( curSkin )

		--self:GetParent().model.startAnim = CurTime()
		--self:GetParent().model.startAng = (self:GetParent().model.ang) + math.pi/10
	end

	function buttons.prev:DoClick()
		local curSkin = self:GetParent().model:GetEntity():GetSkin()
		curSkin = curSkin - 1
		if curSkin < 1 then
			curSkin = self:GetParent().model:GetEntity():SkinCount()
		end
		self:GetParent().model:GetEntity():SetSkin( curSkin )

		--self:GetParent().model.startAnim = CurTime()
		--self:GetParent().model.startAng = (self:GetParent().model.ang) - math.pi/10
	end

	function buttons.nextb:DoClick()
		local curBG = self:GetParent().model:GetEntity():GetBodygroup(1) -- get the handle bodygroup
		curBG = curBG + 1

		local numBG = DOORSKIN.NumBG

		if curBG > (numBG) then
			curBG = 0
		end
		self:GetParent().model:GetEntity():SetBodyGroups("0"..tostring(curBG))

		--self:GetParent().model.startAnim = CurTime()
		--self:GetParent().model.startAng = (self:GetParent().model.ang) + math.pi/10
	end

	function buttons.prevb:DoClick()
		local curBG = self:GetParent().model:GetEntity():GetBodygroup(1) -- get the handle bodygroup
		curBG = curBG - 1

		local numBG = DOORSKIN.NumBG

		if curBG < 0 then
			curBG = (numBG)
		end
		self:GetParent().model:GetEntity():SetBodyGroups("0"..tostring(curBG))

		--self:GetParent().model.startAnim = CurTime()
		--self:GetParent().model.startAng = (self:GetParent().model.ang) - math.pi/10
	end
	

	-- submit button
	buttons.submit = vgui.Create("ari_button", frame)
	buttons.submit:SetSize( w-14, h*0.1 )
	buttons.submit:SetPos( 7, h*0.9 + 32 - 11 )
	buttons.submit:SetText("Apply Changes")
	--buttons.submit:SetColors( DOORSKIN.Colors.GoodDark, DOORSKIN.Colors.Good )

	function buttons.submit:DoClick()
		RunConsoleCommand( "doorskin_setskin", self:GetParent().doorEnt:EntIndex(), self:GetParent().model:GetEntity():GetSkin() )-- arg 1 is the door ID, arg 2 is the skin
		RunConsoleCommand( "doorskin_setbodygroup", self:GetParent().doorEnt:EntIndex(), self:GetParent().model:GetEntity():GetBodygroup(1) )
		
		self:GetParent().model.startAnim = CurTime()
		self:GetParent().model.startAng = (self:GetParent().model.ang) + math.pi*2

	end

	for k,v in pairs(buttons) do
		print(v:GetText())
		v:SetColors( Color(255,255,255,10), Color(255,255,255,50) )
	end
end

-- context menu modifications here

properties.Add( "doorSkin_Tools",
	{
		MenuLabel = "doorSkin Tools",
		Order = 2,
		MenuIcon = "icon16/wrench_orange.png",
		Filter = function( self, ent, ply )
			if IsValid( ent ) then
				local owner = ent:getDoorOwner()
				return ((owner == ply) and ((ent:GetModel() == "models/props_c17/door01_left.mdl") or (ent:GetModel() == "models/props_doors/door03_slotted_left.mdl")))
			end
		end,
		Action = function() end,
		MenuOpen = function( self, option, ent, tr )
			local submenu = option:AddSubMenu()
			local skinmenu = submenu:AddSubMenu( "Skins" )
			local bgmenu = submenu:AddSubMenu( "Bodygroups" )

			local numbg = DOORSKIN.NumBG
			local numskins = ent:SkinCount()

			local bgnames = {
				"Handle",
				"Pushbar",
				"Keypad"
			}

			for i = 1, numbg do
				local op = bgmenu:AddOption( bgnames[i], function() RunConsoleCommand( "doorskin_setbodygroup", ent:EntIndex(), i ) end )
			end

			for i = 1, numskins do
				local op = skinmenu:AddOption( "Skin "..tostring(i), function() RunConsoleCommand( "doorskin_setskin", ent:EntIndex(), i) end )
			end

		end
	
	}
)
properties.Add( "doorSkin_OpenMenu",
	{
		MenuLabel = "Open doorSkin Menu",
		Order = 1,
		MenuIcon = "icon16/book_edit.png",
		Filter = function( self, ent, ply )
			if IsValid( ent ) then
				local owner = ent:getDoorOwner()
				return ((owner == ply) and ((ent:GetModel() == "models/props_c17/door01_left.mdl") or (ent:GetModel() == "models/props_doors/door03_slotted_left.mdl")))
			end
		end,
		Action = function( self, ent )
			local owner = ent:getDoorOwner()
			if owner == LocalPlayer() then
				DOORSKIN:DoorMenu( ent )
			end
		end
	}
)

concommand.Add("doorskin_doordata", function(ply) -- simply get door data

	local tr = ply:GetEyeTrace()

	if tr.Entity then
		local doorData = tr.Entity:getDoorData()
		print("EntIndex: ", tr.Entity:EntIndex())
		if doorData then PrintTable( doorData ) else print("Invalid door.") end
		print("Length of doordata", #doorData)
		local doorData2 = {}
		doorData2.bodygroups = tr.Entity:GetBodyGroups()

		PrintTable( doorData2 )
	end

end)

concommand.Add("doorskin_openmenu", function(ply)

	local tr = ply:GetEyeTrace()

	if tr.Entity then
		local doorOwner = tr.Entity:getDoorOwner() -- make sure that the client owns the door before opening the menu

		if IsValid( doorOwner ) and ((tr.Entity:GetModel() == "models/props_c17/door01_left.mdl") or ( tr.Entity:GetModel() == "models/props_doors/door03_slotted_left.mdl")) then
			DOORSKIN:DoorMenu( tr.Entity )
		end
	end

end)

concommand.Add("doorskin_test", function(ply)
	--PrintTable( list.Get("DesktopWindows") )
end)

-- add this shizz to the keys menu
-- thanks Partixel for this code :)
local function AddButtonToFrame(Frame)
	Frame:SetTall(Frame:GetTall() + 110)
	local button = vgui.Create("DButton", Frame)
	button:SetPos(10, Frame:GetTall() - 110)
	button:SetSize(180, 100)
	return button
end
hook.Add("onKeysMenuOpened", "doorSkinMenu", function( ent, Frame)
	if DOORSKIN.UseKeysMenu == true then
		if ent:getDoorOwner() == LocalPlayer() or LocalPlayer():IsSuperAdmin() then
			local doorSkinMenu = AddButtonToFrame(Frame)
			doorSkinMenu:SetText("Edit Door Appearance")
			doorSkinMenu.DoClick = function()
				DOORSKIN:DoorMenu( ent )
			end
		end
	end
end)