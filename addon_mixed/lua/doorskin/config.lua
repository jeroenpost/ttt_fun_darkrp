DOORSKIN.UseKeysMenu = true -- true to enable the button in the keys menu, false to remove the button (if the button conflicts with another addon)

DOORSKIN.Colors = {} -- change these colors if you want, but I'm not responsible if something breaks :S
DOORSKIN.Colors.Bad = HexColor("#e74c3c")
DOORSKIN.Colors.BadDark = HexColor("#c0392b")
DOORSKIN.Colors.Good = HexColor("#2ecc71")
DOORSKIN.Colors.GoodDark = HexColor("#27ae60")
DOORSKIN.Colors.NeutralHigh = HexColor("#ecf0f1")
DOORSKIN.Colors.NeutralMed = HexColor("#bdc3c7")
DOORSKIN.Colors.NeutralLow = HexColor("#95a5a6")
DOORSKIN.Colors.NeutralDark = HexColor("#7f8c8d")
DOORSKIN.Colors.Turq = HexColor("#1abc9c")
DOORSKIN.Colors.TurqDark = HexColor("#16a085")