
concommand.Add("doorskin_setskin", function(ply, cmd, args)
	--print( "Attempting to set skin "..args[2].." of door "..args[1] )
	if #args == 2 then
		local doorEnt = ents.GetByIndex( tonumber(args[1]) )
		if doorEnt then
			local skin = tonumber(args[2])
			if doorEnt:getDoorOwner() == ply or ply:IsSuperAdmin() then
				doorEnt:SetSkin( skin )
			end
		end
	end
end)

concommand.Add("doorskin_setbodygroup", function(ply, cmd, args)
	if #args == 2 then
		local doorEnt = ents.GetByIndex( tonumber(args[1]) )
		if doorEnt then
			local bg = tonumber(args[2])
			--print("setting bg ",bg)
			if doorEnt:getDoorOwner() == ply or ply:IsSuperAdmin() then
				doorEnt:SetBodygroup( 1, bg )
			end
		end
	end
end)
