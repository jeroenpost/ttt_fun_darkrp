
AddCSLuaFile()	

RadioSystem = {}	

RadioSystem.donatorsetpass = true -- Set this to true if you want only donators to be able to set passwords.

RadioSystem.NeedBuying = false -- Should you have to need to buy the radio from the F4 menu?

RadioSystem.Donators = { "vip", "donator" } -- Add more groups if you want more VIP's.

RadioSystem.minimumfreq = 30 -- This is the minimum Non password frequency.

RadioSystem.maximumfreq = 49 -- This is the maximum Non password frequency.

RadioSystem.minimumfreqpassword = 60 -- This is the minimum password frequency.

RadioSystem.maximumfreqpassword = 69 -- This is the maximum password frequency.

RadioSystem.WordEnabled = true; -- should the word in chat work to open the menu.

RadioSystem.WordToSay = "!walkytalky"

RadioSystem.GUI = {}

RadioSystem.GUI.backgroundcolor = Color(115, 115, 115);

RadioSystem.GUI.backgroundcolorinside = Color(105, 105, 105);

RadioSystem.GUI.uniquecolor = Color(47, 115, 147);

