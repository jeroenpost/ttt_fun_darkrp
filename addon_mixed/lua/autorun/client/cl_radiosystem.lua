--ambient/office/zap1.wav
timer.Simple(1, function()
    include("autorun/sh_radiosystem.lua")
end)
AddCSLuaFile()
local xScreenRes = 1920
local yScreenRes = 1080
local wMod = ScrW() / xScreenRes
local hMod = ScrH() / yScreenRes
local state;
state = true;
local opened = 0;
concommand.Add("walky_talky", function()
    local freqpanel = vgui.Create("DFrame")
    freqpanel:SetPos(wMod * 800, hMod * 250)
    freqpanel:SetSize(wMod * 325, hMod * 525)
    freqpanel:SetTitle("")
    freqpanel:SetDraggable(true)
    freqpanel:ShowCloseButton(true)
    freqpanel:MakePopup()
    function draw.Circle(x, y, radius, seg)
        local cir = {}

        table.insert(cir, { x = x, y = y, u = 0.5, v = 0.5 })
        for i = 0, seg do
            local a = math.rad((i / seg) * -360)
            table.insert(cir, { x = x + math.sin(a) * radius, y = y + math.cos(a) * radius, u = math.sin(a) / 2 + 0.5, v = math.cos(a) / 2 + 0.5 })
        end

        local a = math.rad(0) -- This is need for non absolute segment counts
        table.insert(cir, { x = x + math.sin(a) * radius, y = y + math.cos(a) * radius, u = math.sin(a) / 2 + 0.5, v = math.cos(a) / 2 + 0.5 })

        surface.DrawPoly(cir)
    end

    local square1 = {
        { x = wMod * 34, y = hMod * 475 },
        { x = wMod * 50, y = hMod * 491 },
        { x = wMod * 45, y = hMod * 495 },
        { x = wMod * 30, y = hMod * 480 }
    }

    local square2 = {
        { x = wMod * 45, y = hMod * 475 },
        { x = wMod * 50, y = hMod * 480 },
        { x = wMod * 35, y = hMod * 495 },
        { x = wMod * 30, y = hMod * 491 }
    }

    local square3 = {
        { x = wMod * 284, y = hMod * 475 },
        { x = wMod * 299, y = hMod * 491 },
        { x = wMod * 295, y = hMod * 495 },
        { x = wMod * 280, y = hMod * 480 }
    }

    local square4 = {
        { x = wMod * 295, y = hMod * 475 },
        { x = wMod * 300, y = hMod * 480 },
        { x = wMod * 285, y = hMod * 495 },
        { x = wMod * 280, y = hMod * 491 }
    }
    function freqpanel:Paint(w, h)

        draw.RoundedBox(18, wMod * 2, 120 * hMod, w, h - 120 * hMod, RadioSystem.GUI.backgroundcolor) --bg
        draw.RoundedBox(18, wMod * 6, 125 * hMod, w - 10 * wMod, h - 130 * hMod, RadioSystem.GUI.backgroundcolorinside) --bg

        draw.RoundedBoxEx(8, wMod * 20, hMod * 3, wMod * 20, hMod * 118, RadioSystem.GUI.backgroundcolor, true, true, false, false) --antana
        draw.RoundedBox(4, wMod * 20, hMod * 30, wMod * 20, hMod * 3, Color(0, 0, 0)) -- stripes on antana
        draw.RoundedBox(4, wMod * 20, hMod * 60, wMod * 20, hMod * 3, Color(0, 0, 0)) -- stripes on antana
        draw.RoundedBox(4, wMod * 20, hMod * 90, wMod * 20, hMod * 3, Color(0, 0, 0)) -- stripes on antana
       -- draw.RoundedBox(5, wMod * 98, hMod * 175, wMod * 130, hMod * 40, Color(47, 115, 147)) -- BG for text
       -- draw.RoundedBox(0, wMod * 98, hMod * 175, wMod * 130, hMod * 2, Color(0, 0, 0)) -- #1
      --  draw.RoundedBox(0, wMod * 98, hMod * 175, wMod * 2, hMod * 40, Color(0, 0, 0)) -- #2
      --  draw.RoundedBox(0, wMod * 226, hMod * 175, wMod * 2, hMod * 40, Color(0, 0, 0)) -- #3
      --  draw.RoundedBox(0, wMod * 98, hMod * 215, wMod * 130, hMod * 2, Color(0, 0, 0)) -- #4
        draw.SimpleText("TTT-FUN WalkyTalky", "AckBarWriting", wMod * 63, hMod * 130, Color(255, 255, 0)) -- TEXT
        surface.SetDrawColor(0, 0, 0, 255)
        draw.NoTexture()

        draw.RoundedBox(0, wMod * 37, hMod * 150, wMod * 6, hMod * 20, Color(255, 255, 255, 150)) -- #4
        draw.RoundedBox(0, wMod * 30, hMod * 157, wMod * 20, hMod * 6, Color(255, 255, 255, 150)) -- #4
        draw.RoundedBox(0, wMod * 287, hMod * 150, wMod * 6, hMod * 20, Color(255, 255, 255, 150)) -- #4
        draw.RoundedBox(0, wMod * 280, hMod * 157, wMod * 20, hMod * 6, Color(255, 255, 255, 150)) -- #4
        draw.RoundedBox(0, wMod * 59, hMod * 238, wMod * 223, hMod * 204, Color(115, 115, 115, 170)) --bg For panel

        --draw.RoundedBox(4,wMod * 294,hMod * 475,1,1,Color(255,0,0))
        --draw.RoundedBox(4,wMod * 45,hMod * 475,1,1,Color(255,0,0))
        --draw.RoundedBox(4,wMod * 50,hMod * 475,1,1,Color(255,0,0))
        --draw.RoundedBox(4,wMod * 45,hMod * 472,1,1,Color(255,0,0))
        --draw.RoundedBox(4,wMod * 30,hMod * 491,1,1,Color(255,0,0))
    end

    local frequencyinterface = vgui.Create("DScrollPanel", freqpanel)
    frequencyinterface:SetPos(wMod * 30, hMod * 180)
    frequencyinterface:SetSize(wMod * 270, hMod * 300)

    function frequencyinterface.VBar:Paint(w, h)
        draw.RoundedBox(0, 0, 0, w, h, RadioSystem.GUI.uniquecolor)
    end

    function frequencyinterface.VBar.btnGrip:Paint(w, h)
        draw.RoundedBox(0, 0, 2, w, h, Color(0, 0, 0))
    end

    function frequencyinterface.VBar.btnUp:Paint(w, h)
        draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0))
        surface.SetDrawColor(Color(0, 0, 0))
        surface.DrawOutlinedRect(0, 0, w, h)
        if self:IsDown() then
            draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0))
        end
    end

    function frequencyinterface.VBar.btnDown:Paint(w, h)
        draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0))
        surface.SetDrawColor(Color(0, 0, 0))
        surface.DrawOutlinedRect(0, 0, w, h)
        if self:IsDown() then
            draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0))
        end
    end

    function frequencyinterface:Paint(w, h)
        draw.RoundedBox(0, 0, 0, w, h, Color(189, 195, 199)) --bg
       -- draw.RoundedBox(0, 0, 0, wMod * 2, h, Color(0, 0, 0, 255)) --#1
       -- draw.RoundedBox(0, wMod * 193, 0, wMod * 2, h, Color(0, 0, 0, 255)) --#2
      --  draw.RoundedBox(0, wMod * 1, 0, w, hMod * 2, Color(0, 0, 0)) --#3
      --  draw.RoundedBox(0, wMod * 1, hMod * 188, w, hMod * 2, Color(0, 0, 0)) --#4
        draw.SimpleText("Channels", "Trebuchet18", wMod * 10, hMod * 0, Color(0,0, 0)) -- TEXT
        draw.SimpleText("Secured Channels", "Trebuchet18", wMod * 125, hMod * 0, Color(0,0, 0)) -- TEXT
    end
    local currentChannel =  LocalPlayer():GetFunVar("radio_channel", 0)

    y = 20;

    for i = RadioSystem.minimumfreq, RadioSystem.maximumfreq do
        local temppanel = vgui.Create("DFrame", frequencyinterface)
        local offset = 0
        local offseth = 0
        if i > 39 then offset = 50 offseth = 170 end
        temppanel:SetPos(wMod * 0 +offset, hMod * 0 + y - offseth)
        temppanel:SetSize(wMod * 70, hMod * 20)
        temppanel:SetTitle("")
        temppanel:SetDraggable(true)
        temppanel:ShowCloseButton(false)

        local dbut = vgui.Create("DButton", temppanel)
        dbut:SetPos(wMod * (20), 0)
        dbut:SetFont("Trebuchet18")
        if currentChannel == i then
            dbut:SetColor(Color(255, 0, 0))
        else
            dbut:SetColor(Color(0, 0, 0))
        end

        dbut:SetText( i .."mhz")
        dbut:SizeToContents()


        function dbut:DoClick()
            freqpanel:Close()
            net.Start("changingchannel")
            net.WriteDouble(i)
            net.SendToServer()
        end


        function dbut:OnCursorEntered()
            dbut:SetColor(Color(255, 0, 0))
            surface.PlaySound("garrysmod/ui_hover.wav")
        end

        function dbut:OnCursorExited()
            if currentChannel == i then
                dbut:SetColor(Color(255, 0, 0))
            else
                dbut:SetColor(Color(0, 0, 0))
            end
        end

        function dbut:Paint()
        end

        function temppanel:Paint(w, h)
          --  draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 0)) --bg

           -- draw.RoundedBox(0, wMod * 2, hMod * 30, wMod * 210, hMod * 2, Color(0, 0, 0))
        end

        y = y + 17
    end

    y= 20
    for i = RadioSystem.minimumfreqpassword, RadioSystem.maximumfreqpassword do
        local temppanel = vgui.Create("DFrame", frequencyinterface)
        temppanel:SetPos(wMod * 180, hMod * 0 + y)
        temppanel:SetSize(wMod * 100, hMod * 20)
        temppanel:SetTitle("")
        temppanel:SetDraggable(true)
        temppanel:ShowCloseButton(false)
        local dbut = vgui.Create("DButton", temppanel)
        dbut:SetPos(wMod * 20, 0)
        dbut:SetFont("Trebuchet18")
        if currentChannel == i then
            dbut:SetColor(Color(255, 0, 0))
        else
            dbut:SetColor(Color(0, 0, 0))
        end
        dbut:SetText( i .. "mhz")
        dbut:SizeToContents()
        function dbut:Paint()
        end

        function dbut:OnCursorEntered()
            dbut:SetColor(Color(255, 0, 0))
            surface.PlaySound("garrysmod/ui_hover.wav")
        end

        function dbut:OnCursorExited()
            if currentChannel == i then
                dbut:SetColor(Color(255, 0, 0))
            else
                dbut:SetColor(Color(0, 0, 0))
            end
        end

        function dbut:DoClick()
            freqpanel:Close()
            net.Start("checkemptychannel")
            net.WriteDouble(i)
            net.SendToServer()
        end

        function temppanel:Paint(w, h)
        --    draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 0)) --bg

         --   draw.RoundedBox(0, wMod * 2, hMod * 30, wMod * 210, hMod * 2, Color(0, 0, 0))
        end

        y = y + 17
    end


    function statetrue()
        if onbut ~= nil then
            onbut:Remove()
            offbut:Remove()
        end
        onbut = vgui.Create("DButton", freqpanel)
        onbut:SetPos(wMod * 110, hMod * 490)
        onbut:SetFont("Trebuchet18")
        onbut:SetColor(Color(39, 174, 96))
        onbut:SetText("ON")
        onbut:SizeToContents()
        onbut:SetSize(wMod * 46, hMod * 26)

        function onbut:Paint(w, h)
            draw.RoundedBox(4, 0, 0, w, h, Color(44, 62, 80)) --bg
        end

        offbut = vgui.Create("DButton", freqpanel)
        offbut:SetPos(wMod * 180, hMod * 490)
        offbut:SetFont("Trebuchet18")
        offbut:SetColor(Color(0, 0, 0))
        offbut:SetText("OFF")
        offbut:SizeToContents()
        offbut:SetSize(wMod * 46, hMod * 26)

        function offbut:Paint(w, h)
            draw.RoundedBox(4, 0, 0, w, h, Color(44, 62, 80)) --bg
        end

        function offbut:DoClick()
            net.Start("changestateradio")
            net.WriteBool(false)
            net.SendToServer()
            freqpanel:Close()
           -- statefalse()
        end
    end

    function statefalse()
        if onbut ~= nil then
            onbut:Remove()
            offbut:Remove()
        end
        onbut = vgui.Create("DButton", freqpanel)
        onbut:SetPos(wMod * 110, hMod * 490)
        onbut:SetFont("Trebuchet18")
        onbut:SetColor(Color(0, 0, 0))
        onbut:SetText("ON")
        onbut:SizeToContents()
        onbut:SetSize(wMod * 46, hMod * 26)

        function onbut:Paint(w, h)
            draw.RoundedBox(4, 0, 0, w, h, Color(44, 62, 80)) --bg
        end

        function onbut:DoClick()
            statetrue();
            net.Start("changestateradio")
            net.WriteBool(true)
            net.SendToServer()
        end

        offbut = vgui.Create("DButton", freqpanel)
        offbut:SetPos(wMod * 180, hMod * 490)
        offbut:SetFont("Trebuchet18")
        offbut:SetColor(Color(255, 0, 0))
        offbut:SetText("OFF")
        offbut:SizeToContents()
        offbut:SetSize(wMod * 46, hMod * 26)

        function offbut:Paint(w, h)
            draw.RoundedBox(4, 0, 0, w, h, Color(44, 62, 80)) --bg
        end
    end

    if state == true and currentChannel ~= 0 then
        statetrue()
    else
        statefalse()
    end
end)

net.Receive("checkemptychannelrespond", function()
    local bool = net.ReadBool()
    local channel = net.ReadDouble()
    print(channel)
    if bool then

        local frame = vgui.Create("DFrame", freqpanel)
        frame:SetPos(wMod * 850, hMod * 450)
        frame:SetSize(wMod * 300, hMod * 90)
        frame:ShowCloseButton(false)
        frame:SetDraggable(false)
        frame:SetTitle("")
        frame:MakePopup()

        function frame:Paint(w, h)
            draw.RoundedBox(4, 0, 0, w, h, RadioSystem.GUI.backgroundcolor) --bg
            draw.SimpleText("Setting password for channel " .. channel, "Trebuchet18", wMod * 5, hMod * 5, Color(0, 0, 0)) -- TEXT
            --draw.RoundedBox(0, wMod *5, hMod * 20, wMod * 169, hMod * 1 ,Color(0,0,0)) -- underline
        end


        local text = vgui.Create("DTextEntry", frame)
        text:SetText("password")
        text:SetPos(wMod * 25, hMod * 45)
        text:SetSize(wMod * 160, hMod * 30)
        text.OnEnter = function(self) print(self:GetText()) end

        local dbut = vgui.Create("DButton", frame)
        dbut:SetPos(wMod * 190, hMod * 45)
        dbut:SetFont("Trebuchet18")
        dbut:SetColor(Color(0, 0, 0))
        dbut:SetText("Set")
        dbut:SetSize(wMod * 40, hMod * 30)

        function dbut:Paint(w, h)
            draw.RoundedBox(4, 0, 0, w, h, Color(0, 0, 0, 160)) --bg
        end

        function dbut:DoClick()
            frame:Close()
            net.Start("setinitialpasswordchannel")
            net.WriteDouble(channel)
            net.WriteString(text:GetText())
            net.SendToServer()
        end
    else

        local frame = vgui.Create("DFrame", freqpanel)
        frame:SetPos(wMod * 850, hMod * 450)
        frame:SetSize(wMod * 300, hMod * 90)
        frame:ShowCloseButton(false)
        frame:SetDraggable(false)
        frame:SetTitle("")
        frame:MakePopup()

        function frame:Paint(w, h)
            draw.RoundedBox(4, 0, 0, w, h, RadioSystem.GUI.backgroundcolor) --bg
            draw.SimpleText("Entering channel " .. channel, "Trebuchet18", wMod * 45, hMod * 5, Color(0, 0, 0)) -- TEXT
            --draw.RoundedBox(0, wMod *44, hMod * 20, wMod * 112, hMod * 1 ,Color(0,0,0)) -- underline
        end


        local text = vgui.Create("DTextEntry", frame)
        text:SetText("password")
        text:SetPos(wMod * 25, hMod * 45)
        text:SetSize(wMod * 160, hMod * 30)
        text.OnEnter = function(self) print(self:GetText()) end

        local dbut = vgui.Create("DButton", frame)
        dbut:SetPos(wMod * 190, hMod * 45)
        dbut:SetFont("Trebuchet18")
        dbut:SetColor(Color(0, 0, 0))
        dbut:SetText("Enter")
        dbut:SetSize(wMod * 40, hMod * 30)


        function dbut:Paint(w, h)
            draw.RoundedBox(4, 0, 0, w, h, Color(0, 0, 0, 160)) --bg
        end

        function dbut:DoClick()
            frame:Close()
            net.Start("joinpasswordchannel")
            net.WriteDouble(channel)
            net.WriteString(text:GetText())
            net.SendToServer()
        end
    end
end)


