include("autorun/sh_radiosystem.lua")

util.AddNetworkString("boughtfreq")
util.AddNetworkString("changingchannel")
util.AddNetworkString("changingchannelpassword")
util.AddNetworkString("setpasswordchannel")
util.AddNetworkString("setinitialpasswordchannel")
util.AddNetworkString("checkemptychannel")
util.AddNetworkString("checkemptychannelrespond")
util.AddNetworkString("joinpasswordchannel")
util.AddNetworkString("changestateradio")
util.AddNetworkString("openradio")
local RadioSystemServ = {}

RadioSystemServ.Players = {}

RadioSystemServ.plyfreqs = {}

RadioSystemServ.passfreqs = {}

RadioSystemServ.freqs = {}



net.Receive("changestateradio", function(len, ply)
    local bool = net.ReadBool();
    if bool == false then
        ply.blockhearing = true
        if ply.channel ~= nil then
            CheckEmptyChannel(ply.channel, ply)
        end
        ply.channel = nil
        ply:SetFunVar("radio_channel", 0)
    end
    if bool == true then
        ply.blockhearing = nil
    end
end)

net.Receive("joinpasswordchannel", function(len, ply)
    local channel = net.ReadDouble()
    local password = net.ReadString()
    ply:ChannelChangedPassword(channel, password)
end)

net.Receive("checkemptychannel", function(len, ply)
    local channel = net.ReadDouble()

    if RadioSystemServ.passfreqs[channel] == nil then
        net.Start("checkemptychannelrespond")
        net.WriteBool(true)
        net.WriteDouble(channel)
        net.Send(ply)
    else
        net.Start("checkemptychannelrespond")
        net.WriteBool(false)
        net.WriteDouble(channel)
        net.Send(ply)
    end
end)

for i = RadioSystem.minimumfreq, RadioSystem.maximumfreq do
    RadioSystemServ.freqs[i] = {}
end

for i = RadioSystem.minimumfreqpassword, RadioSystem.maximumfreqpassword do
    RadioSystemServ.freqs[i] = {}
    RadioSystemServ.passfreqs[i] = nil
end

local players = FindMetaTable("Player")

net.Receive("boughtfreq", function(length, ply)
    table.insert(RadioSystemServ.Players, ply)
end)

function players:ChangedPublicChannel(channel)
    if self:HasRadio() then
        if self.channel ~= nil then
            if self.channel >= RadioSystem.minimumfreqpassword and self.channel <= RadioSystem.maximumfreqpassword then
                CheckEmptyChannel(self.channel, self)
            end
        end
        table.insert(RadioSystemServ.freqs[channel], self)
        self.channel = channel;
        self:SetFunVar("radio_channel", channel)
    else
        self:PrintMessage(HUD_PRINTTALK, "Buy a radio first!")
    end
end

function players:HasRadio()
    if RadioSystem.NeedBuying == false then return true end
    for k, v in pairs(RadioSystemServ.Players) do
        if v == self then
            return true
        end
    end
    return false
end

function players:ChannelChangedPassword(channel, password, changed)
    if self:HasRadio() then
        if self.channel ~= nil then
            if self.channel >= RadioSystem.minimumfreqpassword and self.channel <= RadioSystem.maximumfreqpassword then
                CheckEmptyChannel(self.channel, self)
            end
        end
        if changed ~= nil and changed == true then
            self.channel = channel;
            self:PrintMessage(HUD_PRINTTALK, "You have set up channel " .. channel .. ", and the password is: " .. password .. ".")
            self:SetFunVar("radio_channel", channel)
            return
        end
        --print(RadioSystemServ.passfreqs[channel]) print(password)
        if RadioSystemServ.passfreqs[channel] ~= nil and RadioSystemServ.passfreqs[channel] == password then
            self:PrintMessage(HUD_PRINTTALK, "You have joined channel number " .. channel .. ".")
        else
            self:PrintMessage(HUD_PRINTTALK, "The password is incorrect!")
            return false
        end
        self.channel = channel;
        self:SetFunVar("radio_channel", channel)
    else
        self:PrintMessage(HUD_PRINTTALK, "Buy a radio first!")
    end
end

function CheckEmptyChannel(ch, ply)
    for k, v in pairs(player.GetAll()) do
        if v.channel == ch and v ~= ply then return; end
    end

    RadioSystemServ.passfreqs[ch] = nil;
end

function players:SetFreqPassword(channel, password)
    if RadioSystemServ.passfreqs[channel] == nil then

        if RadioSystem.donatorsetpass then

            if self:GetDonatorRadio() == false then self:PrintMessage(HUD_PRINTTALK, "You need to be VIP to set a password") return false end
        end
        RadioSystemServ.passfreqs[channel] = password;
        self:ChannelChangedPassword(channel, password, true)
    end
end


function players:GetDonatorRadio()
    if not gb.compare_rank(self, { "vip", "vipp", "vippp" }) and not gb.is_mod(self) then
        --darkrp.notify(ply,"You need to be VIP to use this")
        return false
    end
    return true
end

net.Receive("changingchannel", function(length, ply)
    if ply:HasRadio() then
        local channel = net.ReadDouble()
        ply:ChangedPublicChannel(channel)
        ply:PrintMessage(HUD_PRINTTALK, "You have changed to channel " .. channel .. ".")
    else
        ply:PrintMessage(HUD_PRINTTALK, "Buy a radio first!")
    end
end)

net.Receive("changingchannelpassword", function(length, ply)
    if ply:HasRadio() then
        local channel = net.ReadDouble()
        local password = net.ReadString()
        ply:ChannelChangedPassword(channel, password)
    else
        ply:PrintMessage(HUD_PRINTTALK, "Buy a radio first!")
    end
end)

net.Receive("setinitialpasswordchannel", function(length, ply)
    if ply:HasRadio() then
        local channel = net.ReadDouble()
        local password = net.ReadString()
        if RadioSystemServ.passfreqs[channel] == nil then
            ply:SetFreqPassword(channel, password)
        end
    else
        ply:PrintMessage(HUD_PRINTTALK, "Buy a radio first!")
    end
end)

hook.Add("PlayerCanHearPlayersVoice", "hearradio", function(listener, talker)
    if listener:HasRadio() and talker:HasRadio() then
        if listener.channel ~= nil and talker.channel ~= nil then
            if listener.channel == talker.channel then
                if not listener.blockhearing then
                    return true;
                end
            end
        end
    end
end)

hook.Add("PlayerCanSeePlayersChat", "gb_seeChat_walky", function(text, teamOnly, listener, talker)
    if listener:HasRadio() and talker.HasRadio and talker:HasRadio() then
        if listener.channel ~= nil and talker.channel ~= nil then
            if listener.channel == talker.channel then
                if not listener.blockhearing then
                    return true;
                end
            end
        end
    end
end)

hook.Add("PlayerSay", "checkwalkytalkychat", function(ply, text)
    if text == RadioSystem.WordToSay then
        ply:ConCommand("walky_talky")
    end
end)

hook.Add("PlayerDeath", "RemoveFreq", function(ply)
   -- for k, v in pairs(RadioSystemServ.Players) do
   --     if v == ply then
   --         table.RemoveByValue(RadioSystemServ.Players, ply)
  --          ply.channel = nil;
  --          return
   --     end
  --  end
end)

hook.Add("PlayerCanPickupWeapon", "noDoublePickup", function(ply, wep)
    if wep:GetClass() == "walky_talky" then
        table.insert(RadioSystemServ.Players, ply)
        return true
    end
end)