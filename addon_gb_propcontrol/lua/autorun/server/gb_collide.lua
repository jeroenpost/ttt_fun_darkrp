


    hook.Add("ShouldCollide","gb_shouldcollide",function(ent1,ent2)

        if not IsValid(ent1) or not IsValid(ent2) then return end
        if ent1:IsWorld() or ent2:IsWorld() then return true end
        if ent1:IsPlayer() or ent2:IsPlayer() then  return true end
        if (ent1:IsVehicle() or ent2:IsVehicle()) then   return true end
        -- Hoverboard
        if ent1.PlayerMountedTime or ent2.PlayerMountedTime then return true end


        return false


    end)

    hook.Add("PlayerSpawn","gb_enablecollisioncheck",function(ply)
        ply:SetCustomCollisionCheck( true )
    end)

    local function gb_addCustomCollisionCheck(ply, ent, _)
        if IsValid(_) then ent = _ end
        if IsValid(ent) then
            ent.EntityOwner = ply:UniqueID()

            ent:SetCustomCollisionCheck( true )

        end
    end

    hook.Add("PlayerSpawnProp", "gb_APAModelBypassFix_", function(ply, model)

        local arrested = false;

        if ply.isArrested && type(ply.isArrested) == "function" then arrested = ply:isArrested() end

        if ply.jail or ply.frozen or ply:IsFrozen() or arrested then
            return false
        end
    end)



    hook.Add("InitPostEntity","gb_autofreeseenable",function()
        timer.Create( "gb_APAntiAutoFreezeTimer",15, 0, function()
                for _,ent in pairs(ents.FindByClass("prop_physics")) do
                    if ent and IsValid(ent) and ent.freezeit and not ent.DecayTime then
                        local phys = ent:GetPhysicsObject()
                        if phys:IsValid()  then
                            if not table.HasValue(dontfreezepropslist, ent:GetClass()) then
                                phys:EnableMotion(false)
                            end
                        end
                    else
                      ent.freezeit = true
                    end
                end
        end)
    end)



    hook.Add("PlayerSpawnedEffect", "gb_apacTagEffect", gb_addCustomCollisionCheck)
    hook.Add("PlayerSpawnedNPC", "gb_apacTagNPC", gb_addCustomCollisionCheck)
    hook.Add("PlayerSpawnedProp", "gb_apacTagProp", gb_addCustomCollisionCheck)
    hook.Add("PlayerSpawnedRagdoll", "gb_apacTagRagdoll", gb_addCustomCollisionCheck)
    hook.Add("PlayerSpawnedSENT", "gb_apacTagSENT", gb_addCustomCollisionCheck)
    hook.Add("PlayerSpawnedSWEP", "gb_apacTagSWEP", gb_addCustomCollisionCheck)
    hook.Add("PlayerSpawnedVehicle", "gb_apacTagVehicle", gb_addCustomCollisionCheck)
