
AddCSLuaFile("autorun/client/cl_faceposer.lua")
util.AddNetworkString("faceposer.request")
if not faceposer.activeRequests then faceposer.activeRequests = {} end

concommand.Add("faceposer_acceptrequest", function(ply, __, args)
    if (args and args[1]) then
        if (type(tonumber(args[1])) == "number") then
            if (faceposer.activeRequests[tonumber(args[1])]) then
                local req = faceposer.activeRequests[tonumber(args[1])]
                if (IsValid(req.pros) and (CurTime() - req.time) <= 40 and req.pros:Team() == TEAM_PLASTIC_SURGEON) then
                    if not ply.acceptedFaceposer then ply.acceptedFaceposer = {} end
                    ply.acceptedFaceposer[req.pros:SteamID()] = true
                    req.pros._pRequestee = nil
                    req.pros:SetFunVar("faceposeAccepted"..ply:SteamID(),1)
                    faceposer.notify(req.pros, ply:Nick().." accepted your services")
                    req.pros:addMoney(5000)
                    if ply:canAfford(5000) then
                        ply:addMoney(-5000)
                        ply:setDarkRPVar("wanted", false)
                    end
                    table.remove(faceposer.activeRequests, tonumber(args[1]))
                end
            end
        end
    end
end)