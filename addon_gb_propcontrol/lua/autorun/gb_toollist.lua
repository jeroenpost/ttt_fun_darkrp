local function mergeTable( table1, table2)
    for k,v in ipairs(table2) do
        table.insert(table1, v)
    end
    return table1
end

local allowedTools = {"button","fading_door","keypad_willox","remover","camera","colour","material","precision","stacker","textscreen"}
local adminTools = {}

local memberTools = {}
local vipTools = {"money_pot","shareprops","moneypot","ladder"}
local vippTools = {"balloon","lamp", "light","skateboard"}
local vipppTools = {"eyeposer","faceposer","finger","hoverboard","inflator"}

vippTools = mergeTable(vippTools,vipTools)
vipppTools = mergeTable(vipppTools,vippTools)

local testOwner = {"colour","material","precision","stacker","fading_door","remover"}
local testFpp = {"remover","material"}

function gb_canUseTool(pl, _, toolmode,wt)

    local target = _.Entity

    if (toolmode == "colour" or toolmode == "material") and  pl:IsPlayer() and !pl:IsAdmin() and IsValid(target) and target:IsPlayer() then
        return false
    end

    if table.HasValue(testFpp, toolmode) and SERVER then
            return FPP.Protect.CanTool(pl, _, toolmode,wt)
    end
    if table.HasValue(testOwner, toolmode) then
        if IsValid(_.Entity) and SERVER then
            return FPP.Protect.CanTool(pl, _, toolmode,wt)
        end
    end

    if table.HasValue(allowedTools, toolmode) then
        return true
    end
    if table.HasValue(memberTools, toolmode) then
        if gb.IsMember(pl) or gb.compare_rank(pl, {"vip","vipp","vippp"})  then
            return true
        else
            if pl.gb_lasttoolmessage and pl.gb_lasttoolmessage > CurTime() then return false end
            pl.gb_lasttoolmessage = CurTime() + 3
            DarkRP.notify(pl, 1, 4, "Member only tool.")
            DarkRP.notify(pl, 1, 4, "Play 10 hours to become member, or become VIP!")
            return false
        end
    end
    if pl:IsAdmin() and table.HasValue(adminTools, toolmode) then
        return true
    end
    -- Check all the vip items
    if ( SERVER and (gb.compare_rank(pl, {"vip","vipp","vippp"}) or gb.is_mod(pl)) ) then
        if table.HasValue(vipTools, toolmode) then
            return true
        end
        if gb.compare_rank(pl, {"vipp","vippp"}) and table.HasValue(vippTools, toolmode) then
            return true
        end
        if (gb.compare_rank(pl, {"vippp"}) or gb.is_mod(pl)) and table.HasValue(vipppTools, toolmode) then
         if not FPP.RestrictedToolsPlayers then FPP.RestrictedToolsPlayers = {} end
                    if not FPP.RestrictedToolsPlayers[toolmode] then FPP.RestrictedToolsPlayers[toolmode] = {} end
            if toolmode =="faceposer" or toolmode == "finger" or toolmode == "eyeposer" then
                if pl:Team() ~= TEAM_PLASTIC_SURGEON then
                    DarkRP.notify(pl, 1, 4, "Only plastic surgeons can use this")
                    FPP.RestrictedToolsPlayers[toolmode][pl:SteamID()] = false
                    return false
                end
            end

            FPP.RestrictedToolsPlayers[toolmode][pl:SteamID()] = true
            if IsValid(_.Entity) then
                _.Entity.FPPCanTouch = _.Entity.FPPCanTouch or {}
                if(_.Entity.FPPCanTouch_old and _.Entity.FPPCanTouch_old[pl]) then

                else
                    local oldcantouch = _.Entity.FPPCanTouch[pl]
                        _.Entity.FPPCanTouch_old = _.Entity.FPPCanTouch_old or {}
                        _.Entity.FPPCanTouch_old[pl] = oldcantouch
                                _.Entity.FPPCanTouch[pl] = 4

                                timer.Simple(0.5,function()
                                    if IsValid(pl) and IsValid(_.Entity) then
                                        if _.Entity.FPPCanTouch_old[pl] then
                                            _.Entity.FPPCanTouch[pl] = _.Entity.FPPCanTouch_old[pl]
                                            _.Entity.FPPCanTouch_old[pl] = nil

                                        else
                                            _.Entity.FPPCanTouch[pl] = 0
                                        end
                                    end

                                end)
                end

            end

            return true
        end
    end
    if SERVER then
        -- Anti spam
        if pl.gb_lasttoolmessage and pl.gb_lasttoolmessage > CurTime() then return false end
        pl.gb_lasttoolmessage = CurTime() + 3
        -- Check VIP modes and display error message correctly
        if table.HasValue(vipTools, toolmode) then
            DarkRP.notify(pl, 1, 4, "This tool is VIP only")
        elseif table.HasValue(vippTools, toolmode) then
            DarkRP.notify(pl, 1, 4, "This tool is VIP+ only")
        elseif table.HasValue(vipppTools, toolmode) then
            DarkRP.notify(pl, 1, 4, "This tool is VIP++ only")
        else
            DarkRP.notify(pl, 1, 4, "This tool ("..toolmode..") is restricted")
        end
        return false
    end

    if CLIENT and not table.HasValue(vipppTools, toolmode) then
        return false
    end


end

hook.Add("CanTool", "gb_canusetool", gb_canUseTool, -10)

timer.Create("gb_sethookagain",120,0,function()
    hook.Add("CanTool", "gb_canusetool", gb_canUseTool, -10)
end)