print("Loading GB proplist")

hook.Add( "AddToolMenuTabs", "gb_tttfuntoolmenu", function()
    spawnmenu.ClearToolMenus()
    spawnmenu.AddToolTab( "AAAAAAA_Main", "TTT-FUN", "gui/ttt-fun.png" ) -- Add a new tab
    spawnmenu.AddToolCategory( "AAAAAAA_Main", "Tools", "TTT-FUN Tools" ) -- Add a category into that new tab



end )

hook.Add("AddGamemodeToolMenuCategories","gb_removeextratoolcategories",function() return false end)


local function removeOldTabls()
    for k, v in pairs( g_SpawnMenu.CreateMenu.Items ) do
        if ( v.Name ~= "#spawnmenu.content_tab" )  then
            g_SpawnMenu.CreateMenu.Items[k] = nil
            g_SpawnMenu.CreateMenu:CloseTab( v.Tab, true )
        end
    end
end

hook.Add("PopulateContent","gb_overridePopulateContent",function()
    removeOldTabls()
end)




local function DoCategory( name, t )

    if not t then return end

    spawnmenu.AddPropCategory( name, t.name, t.contents, t.icon, t.id, t.parentid )

end

if SERVER then
    AddCSLuaFile("proplists/001.lua")
    AddCSLuaFile("proplists/002.lua")
    AddCSLuaFile("proplists/003.lua")
    AddCSLuaFile("proplists/004.lua")
    AddCSLuaFile("proplists/005.lua")
    AddCSLuaFile("proplists/006.lua")
    AddCSLuaFile("proplists/007.lua")
    AddCSLuaFile("proplists/008.lua")
    AddCSLuaFile("proplists/009.lua")
    AddCSLuaFile("proplists/010.lua")
    AddCSLuaFile("proplists/011.lua")
    AddCSLuaFile("proplists/012.lua")
    AddCSLuaFile("proplists/013.lua")
end

include("proplists/001.lua")
include("proplists/002.lua")
include("proplists/003.lua")
include("proplists/004.lua")
include("proplists/005.lua")
include("proplists/006.lua")
include("proplists/007.lua")
include("proplists/008.lua")
include("proplists/009.lua")
include("proplists/010.lua")
include("proplists/011.lua")
include("proplists/012.lua")
include("proplists/013.lua")


hook.Add( "PopulatePropMenu", "TTT-FUN.SpawnMenu", function()

    DoCategory( "attt_fun", ttt_fun_spawn1 )
    DoCategory( "bttt_fun_props", ttt_fun_spawn2 )
    DoCategory( "cttt_fun_building", ttt_fun_spawn3 )
    DoCategory( "dttt_fun_wheels", ttt_fun_spawn4 )
    DoCategory( "ettt_fun_wheels", ttt_fun_spawn5 )
    DoCategory( "fttt_fun_wheels", ttt_fun_spawn6 )
    DoCategory( "gttt_fun_wheels", ttt_fun_spawn7 )
    DoCategory( "httt_fun_wheels", ttt_fun_spawn8 )
    DoCategory( "ittt_fun_wheels", ttt_fun_spawn9 )
    DoCategory( "jttt_fun_wheels", ttt_fun_spawn10 )
    DoCategory( "kttt_fun_wheels", ttt_fun_spawn11 )
    DoCategory( "lttt_fun_wheels", ttt_fun_spawn12 )
    DoCategory( "mttt_fun_wheels", ttt_fun_spawn13 )

    return false
end, HOOK_HIGH )

gb_propwhitelist = {}

if SERVER then
    local function AddToFpp( name, t, viplevel )
        if not FPP then FPP = {} end
        FPP.BlockedModels = {}
        if not gb_propwhitelist then gb_propwhitelist = {} end
        if not viplevel then viplevel = 9 end

        if t.contents then
            for k,v in pairs(t.contents) do
                if v.model then
                    local model = string.Replace( string.lower(v.model), "\\", "/" )
                    FPP.BlockedModels[model] = true
                    if not gb_propwhitelist[model] then
                        gb_propwhitelist[model] = viplevel
                    end
                end
            end
        end
    end





    function gb_propcontrol_propspawn( ply, model )

        if ply:IsFrozen() or ply.frozen then
            ply:ChatPrint("You can't spawn props when frozen")
            return false
        end

        local oldmodel = model
        local model = string.lower(string.Replace( model, "\\", "/" ))

        if not gb_propwhitelist or not gb_propwhitelist['models/xeon133/racewheelskinny/race-wheel-50_s.mdl'] then
            AddToFpp( "attt_fun", ttt_fun_spawn1 )
            AddToFpp( "bttt_fun_props", ttt_fun_spawn2 )
            AddToFpp( "cttt_fun_building", ttt_fun_spawn3 )
            AddToFpp( "dttt_fun_wheels", ttt_fun_spawn4,1 )
            AddToFpp( "ettt_fun_wheels", ttt_fun_spawn5,1 )
            AddToFpp( "fttt_fun_wheels", ttt_fun_spawn6,1 )
            AddToFpp( "gttt_fun_wheels", ttt_fun_spawn7,1 )
            AddToFpp( "httt_fun_wheels", ttt_fun_spawn8,1 )
            AddToFpp( "ittt_fun_wheels", ttt_fun_spawn9,1 )
            AddToFpp( "jttt_fun_wheels", ttt_fun_spawn10,1 )
            AddToFpp( "kttt_fun_wheels", ttt_fun_spawn11,1 )
            AddToFpp( "lttt_fun_wheels", ttt_fun_spawn12,2 )
            AddToFpp( "mttt_fun_wheels", ttt_fun_spawn13,3 )

        end


        if not gb_propwhitelist[model]  then
            ply:ChatPrint(model .." - Cannot be spawned")
            if ( ply:IsSuperAdmin() ) then
                FPP.BlockedModels[oldmodel] = true
                FPP.BlockedModels[model] = true
                return true
            end
            FPP.BlockedModels[oldmodel] = false
            FPP.BlockedModels[model] = false
            return false
        else
            FPP.BlockedModels[oldmodel] = true
            FPP.BlockedModels[model] = true
            --print(gb_propwhitelist[model])
            if gb_propwhitelist[model] ~= 9 then
                if  gb_propwhitelist[model] == 1 then
                    if  gb.compare_rank(ply, {"vip","vipp","vippp"}) then
                        return true
                    end
                    ply:ChatPrint(model .." - is VIP only!")
                    return false
                end
                if  gb_propwhitelist[model] == 2 then
                    if  gb.compare_rank(ply, {"vipp","vippp"}) then
                        return true
                    end
                    ply:ChatPrint(model .." - is VIP+ only!")
                    return false
                end
                if  gb_propwhitelist[model] == 3 then
                    if  gb.compare_rank(ply, {"vippp"}) then
                        return true
                    end
                    ply:ChatPrint(model .." - is VIP++ only!")
                    return false
                end
            end
            return true
        end

        return false
    end

    hook.Add("PlayerSpawnObject", "GB_SpawnEffect", gb_propcontrol_propspawn,HOOK_MONITOR_HIGH) -- prevents precaching
    hook.Add("PlayerSpawnProp", "GB_SpawnProp", function(ply,model)  return gb_propcontrol_propspawn(ply,model)  end ,HOOK_MONITOR_HIGH) -- PlayerSpawnObject isn't always called
    hook.Add("PlayerSpawnEffect", "GB_SpawnEffect", gb_propcontrol_propspawn,HOOK_MONITOR_HIGH)
    hook.Add("PlayerSpawnRagdoll", "GB_SpawnEffect", gb_propcontrol_propspawn,HOOK_MONITOR_HIGH)



    hook.Add("InitPostEntity","gb_spawnlist_load",function()


        function GAMEMODE:PlayerSpawnProp(ply, model)
            -- No prop spawning means no prop spawning.
            local allowed = GAMEMODE.Config.propspawning

            if not allowed then return false end
            if ply:isArrested() then return false end

            model = string.gsub(tostring(model), "\\", "/")
            model = string.gsub(tostring(model), "//", "/")

            local jobTable = ply:getJobTable()
            if jobTable.PlayerSpawnProp then
                jobTable.PlayerSpawnProp(ply, model)
            end
            if not gb_propcontrol_propspawn(ply,model) then
                return false
            end

            return self.Sandbox.PlayerSpawnProp(self, ply, model)
        end



        -- Add everything to FPP
        timer.Simple(5,function()
            if not FPP then FPP = {} end
            FPP.BlockedModels = {}

            -- ByHand
            FPP.BlockedModels["models/props_lab/keypad.mdl"] = true
            FPP.BlockedModels["models/lamps/torch.mdl"] = true
            FPP.BlockedModels["models/props_lab/powerbox02b.mdl"] = true
            FPP.BlockedModels["models/hunter/misc/stair1x1.mdl"] = true
            FPP.BlockedModels["models/cloudstrifexiii/boards/longhoverboard.mdl"] = true
            FPP.BlockedModels["models/cloudstrifexiii/boards/regularhoverboard.mdl"] = true
            FPP.BlockedModels["models/cloudstrifexiii/boards/trickhoverboard.mdl"] = true
            FPP.BlockedModels["models/squint_hoverboard/asltd.mdl"] = true
            FPP.BlockedModels["models/squint_hoverboard/hotrod.mdl"] = true
            FPP.BlockedModels["models/squint_hoverboard/hoverboard.mdl"] = true
            FPP.BlockedModels["models/jaanus/truehoverboard_2.mdl"] = true
            FPP.BlockedModels["models/jaanus/scopesboard.mdl"] = true
            FPP.BlockedModels["models/ut3/hoverboard.mdl"] = true

            AddToFpp( "attt_fun", ttt_fun_spawn1 )
            AddToFpp( "bttt_fun_props", ttt_fun_spawn2 )
            AddToFpp( "cttt_fun_building", ttt_fun_spawn3 )
            AddToFpp( "dttt_fun_wheels", ttt_fun_spawn4,1 )
            AddToFpp( "ettt_fun_wheels", ttt_fun_spawn5,1 )
            AddToFpp( "fttt_fun_wheels", ttt_fun_spawn6,1 )
            AddToFpp( "gttt_fun_wheels", ttt_fun_spawn7,1 )
            AddToFpp( "httt_fun_wheels", ttt_fun_spawn8,1 )
            AddToFpp( "ittt_fun_wheels", ttt_fun_spawn9,1 )
            AddToFpp( "jttt_fun_wheels", ttt_fun_spawn10,1 )
            AddToFpp( "kttt_fun_wheels", ttt_fun_spawn11,1 )
            AddToFpp( "lttt_fun_wheels", ttt_fun_spawn12,2 )
            AddToFpp( "mttt_fun_wheels", ttt_fun_spawn13,3 )

        end)

        hook.Add("PlayerSpawnObject", "GB_SpawnEffect", gb_propcontrol_propspawn,HOOK_MONITOR_HIGH) -- prevents precaching
        hook.Add("PlayerSpawnProp", "GB_SpawnProp", gb_propcontrol_propspawn,HOOK_MONITOR_HIGH) -- PlayerSpawnObject isn't always called
        hook.Add("PlayerSpawnEffect", "GB_SpawnEffect", gb_propcontrol_propspawn,HOOK_MONITOR_HIGH)
        hook.Add("PlayerSpawnRagdoll", "GB_SpawnEffect", gb_propcontrol_propspawn,HOOK_MONITOR_HIGH)

    end)
    AddToFpp( "attt_fun", ttt_fun_spawn1 )
    AddToFpp( "bttt_fun_props", ttt_fun_spawn2 )
    AddToFpp( "cttt_fun_building", ttt_fun_spawn3 )
    AddToFpp( "dttt_fun_wheels", ttt_fun_spawn4,1 )
    AddToFpp( "ettt_fun_wheels", ttt_fun_spawn5,1 )
    AddToFpp( "fttt_fun_wheels", ttt_fun_spawn6,1 )
    AddToFpp( "gttt_fun_wheels", ttt_fun_spawn7,1 )
    AddToFpp( "httt_fun_wheels", ttt_fun_spawn8,1 )
    AddToFpp( "ittt_fun_wheels", ttt_fun_spawn9,1 )
    AddToFpp( "jttt_fun_wheels", ttt_fun_spawn10,1 )
    AddToFpp( "kttt_fun_wheels", ttt_fun_spawn11,1 )
    AddToFpp( "lttt_fun_wheels", ttt_fun_spawn12,2 )
    AddToFpp( "mttt_fun_wheels", ttt_fun_spawn13,3 )

end