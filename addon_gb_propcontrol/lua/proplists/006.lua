ttt_fun_spawn6 = util.KeyValuesToTable([[    "TableToKeyValues"
        {
            "parentid"		"5"
        "icon"		"icon16/folder_image.png"
        "id"		"6"
        "contents"
        {
            "1"
        {
            "type"		"header"
        "text"		"Fences / Gates"
    }
        "2"
        {
            "type"		"model"
        "model"		"models/props/cs_militia/fencewoodlog01_short.mdl"
    }
        "3"
        {
            "type"		"model"
        "model"		"models/props/cs_militia/fencewoodlog02_short.mdl"
    }
        "4"
        {
            "type"		"model"
        "model"		"models/props/cs_militia/fencewoodlog03_long.mdl"
    }
        "5"
        {
            "type"		"model"
        "model"		"models/props/cs_militia/fencewoodlog04_long.mdl"
    }
        "6"
        {
            "type"		"model"
        "model"		"models/props/de_inferno/wood_fence.mdl"
    }
        "7"
        {
            "type"		"model"
        "model"		"models/props/de_inferno/wood_fence_corner.mdl"
    }
        "8"
        {
            "type"		"model"
        "model"		"models/props/cs_militia/housefence.mdl"
    }
        "9"
        {
            "type"		"model"
        "model"		"models/props/cs_militia/housefence_door.mdl"
    }
        "10"
        {
            "type"		"model"
        "model"		"models/props_c17/fence01a.mdl"
    }
        "11"
        {
            "type"		"model"
        "model"		"models/props_c17/fence01b.mdl"
    }
        "12"
        {
            "type"		"model"
        "model"		"models/props_c17/fence02a.mdl"
    }
        "13"
        {
            "type"		"model"
        "model"		"models/props_c17/fence02b.mdl"
    }
        "14"
        {
            "type"		"model"
        "model"		"models/props_c17/fence03a.mdl"
    }
        "15"
        {
            "type"		"model"
        "model"		"models/props_c17/fence04a.mdl"
    }
        "16"
        {
            "type"		"model"
        "model"		"models/props/de_inferno/wood_fence_end.mdl"
    }
        "17"
        {
            "type"		"model"
        "model"		"models/props_wasteland/exterior_fence001b.mdl"
    }
        "18"
        {
            "type"		"model"
        "model"		"models/props_c17/gate_door01a.mdl"
    }
        "19"
        {
            "type"		"model"
        "model"		"models/props/de_train/chainlinkgate.mdl"
    }
        "20"
        {
            "type"		"model"
        "model"		"models/props_wasteland/exterior_fence002a.mdl"
    }
        "21"
        {
            "type"		"model"
        "model"		"models/props_wasteland/exterior_fence001a.mdl"
    }
        "22"
        {
            "type"		"model"
        "model"		"models/props_wasteland/exterior_fence002b.mdl"
    }
        "23"
        {
            "type"		"model"
        "model"		"models/props_wasteland/exterior_fence002c.mdl"
    }
        "24"
        {
            "type"		"model"
        "model"		"models/props_wasteland/exterior_fence002d.mdl"
    }
        "25"
        {
            "type"		"model"
        "model"		"models/props_wasteland/exterior_fence002e.mdl"
    }
        "26"
        {
            "type"		"model"
        "model"		"models/props_wasteland/exterior_fence003a.mdl"
    }
        "27"
        {
            "type"		"model"
        "model"		"models/props_wasteland/exterior_fence003b.mdl"
    }
        "28"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence001a.mdl"
    }
        "29"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence001b.mdl"
    }
        "30"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence001c.mdl"
    }
        "31"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence001d.mdl"
    }
        "32"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence001g.mdl"
    }
        "33"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence002a.mdl"
    }
        "34"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence002b.mdl"
    }
        "35"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence002c.mdl"
    }
        "36"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence002d.mdl"
    }
        "37"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence002f.mdl"
    }
        "38"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence003a.mdl"
    }
        "39"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence003b.mdl"
    }
        "40"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence003d.mdl"
    }
        "41"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence003f.mdl"
    }
        "42"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence004a.mdl"
    }
        "43"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence004b.mdl"
    }
        "44"
        {
            "type"		"model"
        "model"		"models/props_wasteland/wood_fence01a.mdl"
    }
        "45"
        {
            "type"		"model"
        "model"		"models/props_wasteland/wood_fence01b.mdl"
    }
        "46"
        {
            "type"		"model"
        "model"		"models/props_wasteland/wood_fence01c.mdl"
    }
        "47"
        {
            "type"		"model"
        "model"		"models/props_wasteland/wood_fence02a.mdl"
    }
        "48"
        {
            "type"		"model"
        "model"		"models/props_wasteland/wood_fence02a_board01a.mdl"
    }
        "49"
        {
            "type"		"model"
        "model"		"models/props_wasteland/wood_fence02a_board09a.mdl"
    }
        "50"
        {
            "type"		"model"
        "model"		"models/props_wasteland/wood_fence02a_board10a.mdl"
    }
        "51"
        {
            "type"		"model"
        "model"		"models/props_building_details/courtyard_template001c_bars.mdl"
    }
        "52"
        {
            "type"		"model"
        "model"		"models/props_building_details/courtyard_template002c_bars.mdl"
    }
        "53"
        {
            "type"		"model"
        "model"		"models/props_building_details/storefront_template001a_bars.mdl"
    }
        "54"
        {
            "type"		"model"
        "model"		"models/props/de_inferno/railingspikedgate.mdl"
    }
        "55"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence001e.mdl"
    }
        "56"
        {
            "type"		"model"
        "model"		"models/props_c17/gate_door02a.mdl"
    }
        "57"
        {
            "type"		"model"
        "model"		"models/props/de_tides/gate_low.mdl"
    }
        "58"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence002e.mdl"
    }
        "59"
        {
            "type"		"model"
        "model"		"models/props_wasteland/interior_fence003e.mdl"
    }
        "60"
        {
            "type"		"model"
        "model"		"models/props_canal/canal_bars004.mdl"
    }
        "61"
        {
            "type"		"model"
        "model"		"models/props/cs_italy/it_doorc.mdl"
    }
        "62"
        {
            "type"		"model"
        "model"		"models/props_interiors/elevatorshaft_door01a.mdl"
    }
        "63"
        {
            "type"		"model"
        "model"		"models/props_wasteland/prison_celldoor001a.mdl"
    }
        "64"
        {
            "type"		"model"
        "model"		"models/props_wasteland/prison_celldoor001b.mdl"
    }
        "65"
        {
            "type"		"model"
        "model"		"models/props_wasteland/prison_heavydoor001a.mdl"
    }
        "66"
        {
            "type"		"model"
        "model"		"models/props_wasteland/prison_slidingdoor001a.mdl"
    }
        "67"
        {
            "type"		"model"
        "model"		"models/props_wasteland/prison_gate001a.mdl"
    }
        "68"
        {
            "type"		"model"
        "model"		"models/props_wasteland/prison_gate001b.mdl"
    }
        "69"
        {
            "type"		"model"
        "model"		"models/props_wasteland/prison_gate001c.mdl"
    }
        "70"
        {
            "type"		"model"
        "model"		"models/props_rooftop/railing01a.mdl"
    }
        "71"
        {
            "type"		"header"
        "text"		"Combine"
    }
        "72"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_med01a.mdl"
    }
        "73"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_med01b.mdl"
    }
        "74"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_med02a.mdl"
    }
        "75"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_med02b.mdl"
    }
        "76"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_med02c.mdl"
    }
        "77"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_med03b.mdl"
    }
        "78"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_med04b.mdl"
    }
        "79"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_short01a.mdl"
    }
        "80"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_short02a.mdl"
    }
        "81"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_short03a.mdl"
    }
        "82"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_tall01a.mdl"
    }
        "83"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_tall01b.mdl"
    }
        "84"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_tall02b.mdl"
    }
        "85"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_tall03a.mdl"
    }
        "86"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_tall03b.mdl"
    }
        "87"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_tall04a.mdl"
    }
        "88"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_fence01a.mdl"
    }
        "89"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_fence01b.mdl"
    }
        "90"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_barricade_tall04b.mdl"
    }
    }
        "name"		"Fences"
        "version"		"3"
    }
]])