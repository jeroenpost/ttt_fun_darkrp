ttt_fun_spawn12 = util.KeyValuesToTable([[
"TableToKeyValues"
{
	"parentid"		"1"
	"icon"		"icon16/key_add.png"
	"id"		"12"
	"contents"
	{
		"1"
		{
			"type"		"header"
			"text"		"VIP+ Props"
		}
		"2"
		{
			"type"		"model"
			"model"		"models/props/cs_havana/gazebo.mdl"
		}
		"3"
		{
			"type"		"model"
			"model"		"models/props/cs_assault/MoneyPallet_WasherDryer.mdl"
		}
		"4"
		{
			"type"		"model"
			"model"		"models\props/cs_office/Shelves_metal.mdl"
		}
		"5"
		{
			"type"		"model"
			"model"		"models\props/cs_office/Shelves_metal1.mdl"
		}
		"6"
		{
			"type"		"model"
			"model"		"models\props/cs_office/Shelves_metal2.mdl"
		}
		"7"
		{
			"type"		"model"
			"model"		"models\props/cs_office/Shelves_metal3.mdl"
		}
		"8"
		{
			"type"		"model"
			"model"		"models\props/cs_office/Snowman_arm.mdl"
		}
		"9"
		{
			"type"		"model"
			"model"		"models\props/cs_office/Snowman_body.mdl"
		}
		"10"
		{
			"type"		"model"
			"model"		"models\props/cs_office/Snowman_face.mdl"
		}
		"11"
		{
			"type"		"model"
			"model"		"models/props_junk/garbage256_composite001a.mdl"
		}
		"12"
		{
			"type"		"model"
			"model"		"models/props_junk/garbage256_composite001b.mdl"
		}
		"13"
		{
			"type"		"model"
			"model"		"models/props_junk/garbage256_composite002a.mdl"
		}
		"14"
		{
			"type"		"model"
			"model"		"models/props_junk/garbage256_composite002b.mdl"
		}
		"15"
		{
			"type"		"model"
			"model"		"models/props/cs_assault/Dollar.mdl"
		}
		"16"
		{
			"type"		"model"
			"model"		"models/props/cs_assault/Money.mdl"
		}
		"17"
		{
			"type"		"model"
			"model"		"models/props/cs_assault/MoneyPallet.mdl"
		}
		"18"
		{
			"type"		"model"
			"model"		"models/props/cs_assault/MoneyPallet02E.mdl"
		}
		"19"
		{
			"type"		"model"
			"model"		"models\props/CS_militia/furnace01.mdl"
		}
		"20"
		{
			"type"		"header"
			"text"		"Misc"
		}
		"21"
		{
			"type"		"model"
			"model"		"models/xqm/afterburner1.mdl"
		}
		"22"
		{
			"type"		"model"
			"model"		"models/xqm/afterburner1big.mdl"
		}
		"23"
		{
			"type"		"model"
			"model"		"models/xqm/afterburner1medium.mdl"
		}
		"24"
		{
			"type"		"model"
			"model"		"models/xqm/airplanewheel1.mdl"
		}
		"25"
		{
			"type"		"model"
			"model"		"models/xqm/airplanewheel1big.mdl"
		}
		"26"
		{
			"type"		"model"
			"model"		"models/xqm/airplanewheel1huge.mdl"
		}
		"27"
		{
			"type"		"model"
			"model"		"models/xqm/airplanewheel1large.mdl"
		}
		"28"
		{
			"type"		"model"
			"model"		"models/xqm/airplanewheel1medium.mdl"
		}
		"29"
		{
			"type"		"model"
			"model"		"models/xqm/box2s.mdl"
		}
		"30"
		{
			"type"		"model"
			"model"		"models/xqm/box3s.mdl"
		}
		"31"
		{
			"type"		"model"
			"model"		"models/xqm/box4s.mdl"
		}
		"32"
		{
			"type"		"model"
			"model"		"models/xqm/box4shollow.mdl"
		}
		"33"
		{
			"type"		"model"
			"model"		"models/xqm/box4shollowx2.mdl"
		}
		"34"
		{
			"type"		"model"
			"model"		"models/xqm/box5s.mdl"
		}
		"35"
		{
			"type"		"model"
			"model"		"models/xqm/boxfull.mdl"
		}
		"36"
		{
			"type"		"model"
			"model"		"models/xqm/boxtri.mdl"
		}
		"37"
		{
			"type"		"model"
			"model"		"models/xqm/button1.mdl"
		}
		"38"
		{
			"type"		"model"
			"model"		"models/xqm/button2.mdl"
		}
		"39"
		{
			"type"		"model"
			"model"		"models/xqm/button3.mdl"
		}
		"40"
		{
			"type"		"model"
			"model"		"models/xqm/coastertrain1.mdl"
		}
		"41"
		{
			"type"		"model"
			"model"		"models/xqm/coastertrain1seat.mdl"
		}
		"42"
		{
			"type"		"model"
			"model"		"models/xqm/coastertrain2seat.mdl"
		}
		"43"
		{
			"type"		"model"
			"model"		"models/xqm/cylinderx1.mdl"
		}
		"44"
		{
			"type"		"model"
			"model"		"models/xqm/cylinderx1big.mdl"
		}
		"45"
		{
			"type"		"model"
			"model"		"models/xqm/cylinderx1huge.mdl"
		}
		"46"
		{
			"type"		"model"
			"model"		"models/xqm/cylinderx1large.mdl"
		}
		"47"
		{
			"type"		"model"
			"model"		"models/xqm/cylinderx1medium.mdl"
		}
		"48"
		{
			"type"		"model"
			"model"		"models/xqm/cylinderx2.mdl"
		}
		"49"
		{
			"type"		"model"
			"model"		"models/xqm/cylinderx2big.mdl"
		}
		"50"
		{
			"type"		"model"
			"model"		"models/xqm/cylinderx2huge.mdl"
		}
		"51"
		{
			"type"		"model"
			"model"		"models/xqm/cylinderx2large.mdl"
		}
		"52"
		{
			"type"		"model"
			"model"		"models/xqm/cylinderx2medium.mdl"
		}
		"53"
		{
			"type"		"model"
			"model"		"models/xqm/deg180.mdl"
		}
		"54"
		{
			"type"		"model"
			"model"		"models/xqm/deg180single.mdl"
		}
		"55"
		{
			"type"		"model"
			"model"		"models/xqm/deg360.mdl"
		}
		"56"
		{
			"type"		"model"
			"model"		"models/xqm/deg360single.mdl"
		}
		"57"
		{
			"type"		"model"
			"model"		"models/xqm/deg45.mdl"
		}
		"58"
		{
			"type"		"model"
			"model"		"models/xqm/deg45single.mdl"
		}
		"59"
		{
			"type"		"model"
			"model"		"models/xqm/deg90.mdl"
		}
		"60"
		{
			"type"		"model"
			"model"		"models/xqm/deg90single.mdl"
		}
		"61"
		{
			"type"		"model"
			"model"		"models/xqm/helicopterrotor.mdl"
		}
		"62"
		{
			"type"		"model"
			"model"		"models/xqm/helicopterrotorbig.mdl"
		}
		"63"
		{
			"type"		"model"
			"model"		"models/xqm/helicopterrotorhuge.mdl"
		}
		"64"
		{
			"type"		"model"
			"model"		"models/xqm/hoverboard.mdl"
		}
		"65"
		{
			"type"		"model"
			"model"		"models/xqm/hydcontrolbox.mdl"
		}
		"66"
		{
			"type"		"model"
			"model"		"models/xqm/jetbody1.mdl"
		}
		"67"
		{
			"type"		"model"
			"model"		"models/xqm/jetbody2.mdl"
		}
		"70"
		{
			"type"		"model"
			"model"		"models/xqm/jetengine.mdl"
		}
		"71"
		{
			"type"		"model"
			"model"		"models/xqm/jetenginepropeller.mdl"
		}
		"72"
		{
			"type"		"model"
			"model"		"models/xqm/jetenginepropellerbig.mdl"
		}
		"73"
		{
			"type"		"model"
			"model"		"models/xqm/modernchair.mdl"
		}
		"74"
		{
			"type"		"model"
			"model"		"models/xqm/panel180.mdl"
		}
		"75"
		{
			"type"		"model"
			"model"		"models/xqm/panel1x1.mdl"
		}
		"76"
		{
			"type"		"model"
			"model"		"models/xqm/panel1x2.mdl"
		}
		"77"
		{
			"type"		"model"
			"model"		"models/xqm/panel1x3.mdl"
		}
		"78"
		{
			"type"		"model"
			"model"		"models/xqm/panel1x4.mdl"
		}
		"79"
		{
			"type"		"model"
			"model"		"models/xqm/panel2x2.mdl"
		}
		"80"
		{
			"type"		"model"
			"model"		"models/xqm/panel2x3.mdl"
		}
		"81"
		{
			"type"		"model"
			"model"		"models/xqm/panel2x4.mdl"
		}
		"82"
		{
			"type"		"model"
			"model"		"models/xqm/panel360.mdl"
		}
		"83"
		{
			"type"		"model"
			"model"		"models/xqm/panel3x4.mdl"
		}
		"84"
		{
			"type"		"model"
			"model"		"models/xqm/panel45.mdl"
		}
		"85"
		{
			"type"		"model"
			"model"		"models/xqm/panel4x4.mdl"
		}
		"86"
		{
			"type"		"model"
			"model"		"models/xqm/panel90.mdl"
		}
		"87"
		{
			"type"		"model"
			"model"		"models/xqm/pistontype1.mdl"
		}
		"88"
		{
			"type"		"model"
			"model"		"models/xqm/pistontype1big.mdl"
		}
		"89"
		{
			"type"		"model"
			"model"		"models/xqm/pistontype1huge.mdl"
		}
		"90"
		{
			"type"		"model"
			"model"		"models/xqm/pistontype1large.mdl"
		}
		"91"
		{
			"type"		"model"
			"model"		"models/xqm/pistontype1medium.mdl"
		}
		"92"
		{
			"type"		"model"
			"model"		"models/xqm/polex1.mdl"
		}
		"93"
		{
			"type"		"model"
			"model"		"models/xqm/polex2.mdl"
		}
		"94"
		{
			"type"		"model"
			"model"		"models/xqm/polex3.mdl"
		}
		"95"
		{
			"type"		"model"
			"model"		"models/xqm/polex4.mdl"
		}
		"96"
		{
			"type"		"model"
			"model"		"models/xqm/propeller1.mdl"
		}
		"97"
		{
			"type"		"model"
			"model"		"models/xqm/propeller1big.mdl"
		}
		"98"
		{
			"type"		"model"
			"model"		"models/xqm/quad1.mdl"
		}
		"99"
		{
			"type"		"model"
			"model"		"models/xqm/quad2.mdl"
		}
		"100"
		{
			"type"		"model"
			"model"		"models/xqm/quad3.mdl"
		}
		"101"
		{
			"type"		"model"
			"model"		"models/xqm/rhombus1.mdl"
		}
		"102"
		{
			"type"		"model"
			"model"		"models/xqm/rhombus2.mdl"
		}
		"103"
		{
			"type"		"model"
			"model"		"models/xqm/rhombus3.mdl"
		}
		"104"
		{
			"type"		"model"
			"model"		"models/xqm/triangle1x1.mdl"
		}
		"105"
		{
			"type"		"model"
			"model"		"models/xqm/triangle1x2.mdl"
		}
		"106"
		{
			"type"		"model"
			"model"		"models/xqm/triangle2x2.mdl"
		}
		"107"
		{
			"type"		"model"
			"model"		"models/xqm/triangle2x4.mdl"
		}
		"108"
		{
			"type"		"model"
			"model"		"models/xqm/triangle4x4.mdl"
		}
		"109"
		{
			"type"		"model"
			"model"		"models/xqm/triangle4x6.mdl"
		}
		"110"
		{
			"type"		"model"
			"model"		"models/xqm/trianglelong1.mdl"
		}
		"111"
		{
			"type"		"model"
			"model"		"models/xqm/trianglelong2.mdl"
		}
		"112"
		{
			"type"		"model"
			"model"		"models/xqm/trianglelong3.mdl"
		}
		"113"
		{
			"type"		"model"
			"model"		"models/xqm/trianglelong4.mdl"
		}
		"114"
		{
			"type"		"model"
			"model"		"models/xqm/wingpiece1.mdl"
		}
		"115"
		{
			"type"		"model"
			"model"		"models/xqm/wingpiece2.mdl"
		}
		"116"
		{
			"type"		"model"
			"model"		"models/xqm/wingpiece3.mdl"
		}
		"117"
		{
			"type"		"model"
			"model"		"models/xqm/wingtip1.mdl"
		}
	}
	"name"		"VIP+ Props"
	"version"		"3"
}

]])