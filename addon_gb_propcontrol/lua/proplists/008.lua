ttt_fun_spawn8 = util.KeyValuesToTable([[    "TableToKeyValues"
        {
            "parentid"		"5"
        "icon"		"icon16/folder_database.png"
        "id"		"8"
        "contents"
        {
            "1"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate.mdl"
    }
        "2"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025.mdl"
    }
        "3"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x025.mdl"
    }
        "4"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x05.mdl"
    }
        "5"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x075.mdl"
    }
        "6"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x1.mdl"
    }
        "7"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x125.mdl"
    }
        "8"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x150.mdl"
    }
        "9"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x16.mdl"
    }
        "10"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x175.mdl"
    }
        "11"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x2.mdl"
    }
        "12"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x24.mdl"
    }
        "13"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x3.mdl"
    }
        "14"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x32.mdl"
    }
        "15"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x4.mdl"
    }
        "16"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x5.mdl"
    }
        "17"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x6.mdl"
    }
        "18"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x7.mdl"
    }
        "19"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate025x8.mdl"
    }
        "20"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05.mdl"
    }
        "21"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x05.mdl"
    }
        "22"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x05_rounded.mdl"
    }
        "23"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x075.mdl"
    }
        "24"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x1.mdl"
    }
        "25"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x16.mdl"
    }
        "26"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x2.mdl"
    }
        "27"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x24.mdl"
    }
        "28"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x3.mdl"
    }
        "29"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x32.mdl"
    }
        "30"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x4.mdl"
    }
        "31"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x5.mdl"
    }
        "32"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x6.mdl"
    }
        "33"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x7.mdl"
    }
        "34"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate05x8.mdl"
    }
        "35"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075.mdl"
    }
        "36"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075x075.mdl"
    }
        "37"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075x1.mdl"
    }
        "38"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075x105.mdl"
    }
        "39"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075x16.mdl"
    }
        "40"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075x2.mdl"
    }
        "41"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075x24.mdl"
    }
        "42"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075x3.mdl"
    }
        "43"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075x32.mdl"
    }
        "44"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075x4.mdl"
    }
        "45"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075x5.mdl"
    }
        "46"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075x6.mdl"
    }
        "47"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075x7.mdl"
    }
        "48"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate075x8.mdl"
    }
        "49"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1.mdl"
    }
        "50"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate125.mdl"
    }
        "51"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate150.mdl"
    }
        "52"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate16.mdl"
    }
        "53"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate16x16.mdl"
    }
        "54"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate16x24.mdl"
    }
        "55"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate16x32.mdl"
    }
        "56"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate175.mdl"
    }
        "57"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x1.mdl"
    }
        "58"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x16.mdl"
    }
        "59"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x2.mdl"
    }
        "60"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x24.mdl"
    }
        "61"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x3.mdl"
    }
        "62"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x32.mdl"
    }
        "63"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x3x1trap.mdl"
    }
        "64"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x4.mdl"
    }
        "65"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x4x2trap.mdl"
    }
        "66"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x4x2trap1.mdl"
    }
        "67"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x5.mdl"
    }
        "68"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x6.mdl"
    }
        "69"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x7.mdl"
    }
        "70"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate1x8.mdl"
    }
        "71"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate2.mdl"
    }
        "72"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate24x24.mdl"
    }
        "73"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate24x32.mdl"
    }
        "74"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate2x16.mdl"
    }
        "75"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate2x2.mdl"
    }
        "76"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate2x24.mdl"
    }
        "77"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate2x3.mdl"
    }
        "78"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate2x32.mdl"
    }
        "79"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate2x4.mdl"
    }
        "80"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate2x5.mdl"
    }
        "81"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate2x6.mdl"
    }
        "82"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate2x7.mdl"
    }
        "83"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate2x8.mdl"
    }
        "84"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate3.mdl"
    }
        "85"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate32x32.mdl"
    }
        "86"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate3x16.mdl"
    }
        "87"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate3x24.mdl"
    }
        "88"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate3x3.mdl"
    }
        "89"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate3x32.mdl"
    }
        "90"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate3x4.mdl"
    }
        "91"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate3x5.mdl"
    }
        "92"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate3x6.mdl"
    }
        "93"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate3x7.mdl"
    }
        "94"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate3x8.mdl"
    }
        "95"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate4.mdl"
    }
        "96"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate4x16.mdl"
    }
        "97"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate4x24.mdl"
    }
        "98"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate4x32.mdl"
    }
        "99"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate4x4.mdl"
    }
        "100"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate4x5.mdl"
    }
        "101"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate4x6.mdl"
    }
        "102"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate4x7.mdl"
    }
        "103"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate4x8.mdl"
    }
        "104"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate5.mdl"
    }
        "105"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate5x16.mdl"
    }
        "106"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate5x24.mdl"
    }
        "107"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate5x32.mdl"
    }
        "108"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate5x5.mdl"
    }
        "109"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate5x6.mdl"
    }
        "110"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate5x7.mdl"
    }
        "111"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate5x8.mdl"
    }
        "112"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate6.mdl"
    }
        "113"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate6x16.mdl"
    }
        "114"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate6x24.mdl"
    }
        "115"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate6x32.mdl"
    }
        "116"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate6x6.mdl"
    }
        "117"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate6x7.mdl"
    }
        "118"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate6x8.mdl"
    }
        "119"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate7.mdl"
    }
        "120"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate7x16.mdl"
    }
        "121"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate7x24.mdl"
    }
        "122"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate7x32.mdl"
    }
        "123"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate7x7.mdl"
    }
        "124"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate7x8.mdl"
    }
        "125"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate8.mdl"
    }
        "126"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate8x16.mdl"
    }
        "127"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate8x24.mdl"
    }
        "128"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate8x32.mdl"
    }
        "129"
        {
            "type"		"model"
        "model"		"models/hunter/plates/plate8x8.mdl"
    }
        "130"
        {
            "type"		"model"
        "model"		"models/hunter/plates/platehole1x1.mdl"
    }
        "131"
        {
            "type"		"model"
        "model"		"models/hunter/plates/platehole1x2.mdl"
    }
        "132"
        {
            "type"		"model"
        "model"		"models/hunter/plates/platehole2x2.mdl"
    }
        "133"
        {
            "type"		"model"
        "model"		"models/hunter/plates/platehole3.mdl"
    }
        "134"
        {
            "type"		"model"
        "model"		"models/hunter/plates/tri1x1.mdl"
    }
        "135"
        {
            "type"		"model"
        "model"		"models/hunter/plates/tri2x1.mdl"
    }
        "136"
        {
            "type"		"model"
        "model"		"models/hunter/plates/tri3x1.mdl"
    }
    }
        "name"		"Plates"
        "version"		"3"
    }
]])