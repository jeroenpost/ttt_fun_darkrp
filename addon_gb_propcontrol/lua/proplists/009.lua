ttt_fun_spawn9 = util.KeyValuesToTable([[    "TableToKeyValues"
        {
            "parentid"		"5"
        "icon"		"icon16/folder_star.png"
        "id"		"9"
        "contents"
        {
            "1"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/025x025.mdl"
    }
        "2"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/025x025mirrored.mdl"
    }
        "3"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/05x05.mdl"
    }
        "4"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/05x05mirrored.mdl"
    }
        "5"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/05x05x05.mdl"
    }
        "6"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/075x075.mdl"
    }
        "7"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/075x075mirrored.mdl"
    }
        "8"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x05x05.mdl"
    }
        "9"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x05x1.mdl"
    }
        "10"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x1.mdl"
    }
        "11"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x1mirrored.mdl"
    }
        "12"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x1x1.mdl"
    }
        "13"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x1x1carved.mdl"
    }
        "14"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x1x1carved025.mdl"
    }
        "15"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x1x2.mdl"
    }
        "16"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x1x2carved.mdl"
    }
        "17"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x1x2carved025.mdl"
    }
        "18"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x1x3.mdl"
    }
        "19"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x1x4.mdl"
    }
        "20"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x1x4carved.mdl"
    }
        "21"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x1x4carved025.mdl"
    }
        "22"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/1x1x5.mdl"
    }
        "23"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/2x1x1.mdl"
    }
        "24"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/2x1x1carved.mdl"
    }
        "25"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/2x1x2carved.mdl"
    }
        "26"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/2x2.mdl"
    }
        "27"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/2x2mirrored.mdl"
    }
        "28"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/2x2x1.mdl"
    }
        "29"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/2x2x1carved.mdl"
    }
        "30"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/2x2x2.mdl"
    }
        "31"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/2x2x2carved.mdl"
    }
        "32"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/2x2x4carved.mdl"
    }
        "33"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/3x2x2.mdl"
    }
        "34"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/3x3.mdl"
    }
        "35"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/3x3mirrored.mdl"
    }
        "36"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/3x3x2.mdl"
    }
        "37"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/4x4.mdl"
    }
        "38"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/4x4mirrored.mdl"
    }
        "39"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/5x5.mdl"
    }
        "40"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/6x6.mdl"
    }
        "41"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/7x7.mdl"
    }
        "42"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/8x8.mdl"
    }
        "43"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/trapezium.mdl"
    }
        "44"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/trapezium3x3x1.mdl"
    }
        "45"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/trapezium3x3x1a.mdl"
    }
        "46"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/trapezium3x3x1b.mdl"
    }
        "47"
        {
            "type"		"model"
        "model"		"models/hunter/triangles/trapezium3x3x1c.mdl"
    }
        "48"
        {
            "type"		"model"
        "model"		"models/phxtended/tri1x1.mdl"
    }
        "49"
        {
            "type"		"model"
        "model"		"models/phxtended/tri1x1solid.mdl"
    }
        "50"
        {
            "type"		"model"
        "model"		"models/phxtended/tri1x1x1.mdl"
    }
        "51"
        {
            "type"		"model"
        "model"		"models/phxtended/tri1x1x1solid.mdl"
    }
        "52"
        {
            "type"		"model"
        "model"		"models/phxtended/tri1x1x2.mdl"
    }
        "53"
        {
            "type"		"model"
        "model"		"models/phxtended/tri1x1x2solid.mdl"
    }
        "54"
        {
            "type"		"model"
        "model"		"models/phxtended/tri2x1.mdl"
    }
        "55"
        {
            "type"		"model"
        "model"		"models/phxtended/tri2x1solid.mdl"
    }
        "56"
        {
            "type"		"model"
        "model"		"models/phxtended/tri2x1x1.mdl"
    }
        "57"
        {
            "type"		"model"
        "model"		"models/phxtended/tri2x1x1solid.mdl"
    }
        "58"
        {
            "type"		"model"
        "model"		"models/phxtended/tri2x1x2.mdl"
    }
        "59"
        {
            "type"		"model"
        "model"		"models/phxtended/tri2x1x2solid.mdl"
    }
        "60"
        {
            "type"		"model"
        "model"		"models/phxtended/tri2x2.mdl"
    }
        "61"
        {
            "type"		"model"
        "model"		"models/phxtended/tri2x2solid.mdl"
    }
        "62"
        {
            "type"		"model"
        "model"		"models/phxtended/tri2x2x1.mdl"
    }
        "63"
        {
            "type"		"model"
        "model"		"models/phxtended/tri2x2x1solid.mdl"
    }
        "64"
        {
            "type"		"model"
        "model"		"models/phxtended/tri2x2x2.mdl"
    }
        "65"
        {
            "type"		"model"
        "model"		"models/phxtended/tri2x2x2solid.mdl"
    }
        "66"
        {
            "type"		"model"
        "model"		"models/phxtended/trieq1x1x1.mdl"
    }
        "67"
        {
            "type"		"model"
        "model"		"models/phxtended/trieq1x1x1solid.mdl"
    }
        "68"
        {
            "type"		"model"
        "model"		"models/phxtended/trieq1x1x2.mdl"
    }
        "69"
        {
            "type"		"model"
        "model"		"models/phxtended/trieq1x1x2solid.mdl"
    }
        "70"
        {
            "type"		"model"
        "model"		"models/phxtended/trieq2x2x1.mdl"
    }
        "71"
        {
            "type"		"model"
        "model"		"models/phxtended/trieq2x2x1solid.mdl"
    }
        "72"
        {
            "type"		"model"
        "model"		"models/phxtended/trieq2x2x2.mdl"
    }
        "73"
        {
            "type"		"model"
        "model"		"models/phxtended/trieq2x2x2solid.mdl"
    }
    }
        "name"		"Triangles"
        "version"		"3"
    }
]])