ttt_fun_spawn13 = util.KeyValuesToTable([[
"TableToKeyValues"
{
	"parentid"		"1"
	"icon"		"icon16/key_delete.png"
	"id"		"13"
	"contents"
	{
		"1"
		{
			"type"		"header"
			"text"		"VIP++ Props"
		}
		"2"
		{
			"type"		"model"
			"model"		"models/props/de_port/portwalkway.mdl"
		}
		"3"
		{
			"type"		"model"
			"model"		"models\props/CS_militia/bar01.mdl"
		}
		"4"
		{
			"type"		"model"
			"model"		"models\props/CS_militia/barstool01.mdl"
		}
		"5"
		{
			"type"		"model"
			"model"		"models\props/CS_militia/bottle01.mdl"
		}
		"6"
		{
			"type"		"model"
			"model"		"models\props/CS_militia/bottle02.mdl"
		}
		"7"
		{
			"type"		"model"
			"model"		"models\props/CS_militia/gun_cabinet.mdl"
		}
		"8"
		{
			"type"		"model"
			"model"		"models\props/CS_militia/mountedfish01.mdl"
		}
		"9"
		{
			"type"		"model"
			"model"		"models\props/CS_militia/television_console01.mdl"
		}
		"10"
		{
			"type"		"model"
			"model"		"models/props_combine/combine_fence01a.mdl"
		}
		"11"
		{
			"type"		"model"
			"model"		"models/props_combine/combine_fence01b.mdl"
		}
		"12"
		{
			"type"		"model"
			"model"		"models/props_combine/combine_window001.mdl"
		}
		"13"
		{
			"type"		"model"
			"model"		"models/props_combine/headcrabcannister01a.mdl"
		}
		"14"
		{
			"type"		"model"
			"model"		"models/props_combine/weaponstripper.mdl"
		}
		"15"
		{
			"type"		"model"
			"model"		"models/props_lab/securitybank.mdl"
		}
		"16"
		{
			"type"		"model"
			"model"		"models/xqm/podremake.mdl"
		}
		"17"
		{
			"type"		"model"
			"model"		"models\props/CS_militia/haybale_target.mdl"
		}
		"18"
		{
			"type"		"model"
			"model"		"models\props/CS_militia/haybale_target_02.mdl"
		}
		"19"
		{
			"type"		"model"
			"model"		"models\props/CS_militia/haybale_target_03.mdl"
		}
		"20"
		{
			"type"		"model"
			"model"		"models\props/CS_militia/logpile2.mdl"
		}
		"21"
		{
			"type"		"model"
			"model"		"models/xqm/jetbody3.mdl"
		}
		"22"
		{
			"type"		"model"
			"model"		"models/props/de_dust/door01a.mdl"
		}
		"23"
		{
			"type"		"model"
			"model"		"models/props/de_dust/du_crate_128x128.mdl"
		}
		"24"
		{
			"type"		"model"
			"model"		"models/props/de_dust/du_crate_64x64.mdl"
		}
		"25"
		{
			"type"		"model"
			"model"		"models/props/de_dust/du_crate_64x64_stone.mdl"
		}
		"26"
		{
			"type"		"model"
			"model"		"models/props/de_dust/du_crate_64x80.mdl"
		}
		"27"
		{
			"type"		"model"
			"model"		"models/props/de_dust/du_crate_96x96.mdl"
		}
		"28"
		{
			"type"		"model"
			"model"		"models/props/de_dust/grainbasket01a.mdl"
		}
		"29"
		{
			"type"		"model"
			"model"		"models/props/de_dust/grainbasket01b.mdl"
		}
		"30"
		{
			"type"		"model"
			"model"		"models/props/de_dust/grainbasket01c.mdl"
		}
		"31"
		{
			"type"		"model"
			"model"		"models/props/de_dust/stoneblock01a.mdl"
		}
		"32"
		{
			"type"		"model"
			"model"		"models/props/de_dust/stoneblocks48.mdl"
		}
		"33"
		{
			"type"		"model"
			"model"		"models/props/de_dust/wagon.mdl"
		}
		"34"
		{
			"type"		"header"
			"text"		"Garden / Plants"
		}
		"35"
		{
			"type"		"model"
			"model"		"models/props/de_tides/menu_stand.mdl"
		}
		"36"
		{
			"type"		"model"
			"model"		"models/props/de_tides/patio_chair.mdl"
		}
		"37"
		{
			"type"		"model"
			"model"		"models/props/de_tides/patio_chair2.mdl"
		}
		"38"
		{
			"type"		"model"
			"model"		"models/props/de_tides/patio_table.mdl"
		}
		"39"
		{
			"type"		"model"
			"model"		"models/props/de_tides/patio_table2.mdl"
		}
		"40"
		{
			"type"		"model"
			"model"		"models/props/de_tides/restaurant_table.mdl"
		}
		"41"
		{
			"type"		"model"
			"model"		"models/props/de_tides/tides_banner_a.mdl"
		}
		"42"
		{
			"type"		"model"
			"model"		"models/props/de_tides/tides_staffonly_sign.mdl"
		}
		"43"
		{
			"type"		"model"
			"model"		"models/props/de_tides/tides_streetlight.mdl"
		}
		"44"
		{
			"type"		"model"
			"model"		"models/props/de_tides/vending_cart.mdl"
		}
		"45"
		{
			"type"		"model"
			"model"		"models/props/de_tides/vending_cart_base.mdl"
		}
		"46"
		{
			"type"		"model"
			"model"		"models/props/de_tides/vending_cart_top.mdl"
		}
		"47"
		{
			"type"		"model"
			"model"		"models/props/de_tides/vending_cart_wheell.mdl"
		}
		"48"
		{
			"type"		"model"
			"model"		"models/props/de_tides/vending_cart_wheelr.mdl"
		}
		"49"
		{
			"type"		"model"
			"model"		"models/props/de_tides/vending_hat.mdl"
		}
		"50"
		{
			"type"		"model"
			"model"		"models/props/de_tides/vending_tshirt.mdl"
		}
		"51"
		{
			"type"		"model"
			"model"		"models/props/de_tides/vending_turtle.mdl"
		}
		"52"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/ammo_can_01.mdl"
		}
		"53"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/ammo_can_02.mdl"
		}
		"54"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/ammo_can_03.mdl"
		}
		"55"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/concretebags.mdl"
		}
		"56"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/concretebags2.mdl"
		}
		"57"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/concretebags3.mdl"
		}
		"58"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/concretebags4.mdl"
		}
		"59"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/lighthanging.mdl"
		}
		"60"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/prodcratesa.mdl"
		}
		"61"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/prodcratesb.mdl"
		}
		"62"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/pushcart.mdl"
		}
		"63"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/wood_pallet_01.mdl"
		}
		"64"
		{
			"type"		"model"
			"model"		"models/props/de_piranesi/pi_bench.mdl"
		}
		"65"
		{
			"type"		"model"
			"model"		"models/props/de_piranesi/pi_bucket.mdl"
		}
		"66"
		{
			"type"		"model"
			"model"		"models/props/de_piranesi/pi_merlon.mdl"
		}
		"67"
		{
			"type"		"model"
			"model"		"models/props/de_piranesi/pi_orrery.mdl"
		}
		"68"
		{
			"type"		"model"
			"model"		"models/props/de_piranesi/pi_sundial.mdl"
		}
		"69"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/de_inferno_well.mdl"
		}
		"70"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/doorarcha.mdl"
		}
		"71"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/doorarchb.mdl"
		}
		"72"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/fireplace.mdl"
		}
		"73"
		{
			"type"		"model"
			"model"		"models/props/de_tides/planter.mdl"
		}
		"74"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/flower_barrel.mdl"
		}
		"75"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/flower_barrel_p8.mdl"
		}
		"76"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/fountain.mdl"
		}
		"77"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/fountain_bowl.mdl"
		}
		"78"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/fountain_bowl_p1.mdl"
		}
		"79"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/fountain_bowl_p10.mdl"
		}
		"80"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/pot_big.mdl"
		}
		"81"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/potted_plant1.mdl"
		}
		"82"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/potted_plant1_p1.mdl"
		}
		"83"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/potted_plant2.mdl"
		}
		"84"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/potted_plant2_p1.mdl"
		}
		"85"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/potted_plant3.mdl"
		}
		"86"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/potted_plant3_p1.mdl"
		}
		"87"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/bell_large.mdl"
		}
		"88"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/bell_largeb.mdl"
		}
		"89"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/bell_small.mdl"
		}
		"90"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/bell_smallb.mdl"
		}
		"91"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/bench_concrete.mdl"
		}
		"92"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/bench_wood.mdl"
		}
		"93"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/cactus2.mdl"
		}
		"94"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/cannon_base.mdl"
		}
		"95"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/cannon_gun.mdl"
		}
		"96"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/chimney01.mdl"
		}
		"97"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/chimney02.mdl"
		}
		"98"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/churchprop01.mdl"
		}
		"99"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/churchprop02.mdl"
		}
		"100"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/churchprop03.mdl"
		}
		"101"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/churchprop04.mdl"
		}
		"102"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/churchprop05.mdl"
		}
		"103"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/clayoven.mdl"
		}
		"104"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/claypot03.mdl"
		}
		"105"
		{
			"type"		"model"
			"model"		"models/props/de_inferno/claypot03_damage_01.mdl"
		}
		"106"
		{
			"type"		"model"
			"model"		"models/props_c17/playground_swingset_seat01a.mdl"
		}
		"107"
		{
			"type"		"model"
			"model"		"models/props_c17/playground_teetertoter_seat.mdl"
		}
		"108"
		{
			"type"		"model"
			"model"		"models/props_c17/playgroundslide01.mdl"
		}
		"109"
		{
			"type"		"model"
			"model"		"models/props_c17/playgroundTick-tack-toe_block01a.mdl"
		}
		"110"
		{
			"type"		"model"
			"model"		"models/props_c17/playgroundTick-tack-toe_post01.mdl"
		}
		"111"
		{
			"type"		"model"
			"model"		"models/props_c17/statue_horse.mdl"
		}
		"112"
		{
			"type"		"model"
			"model"		"models/props_c17/playground_carousel01.mdl"
		}
		"113"
		{
			"type"		"model"
			"model"		"models/props_c17/playground_jungle_gym01a.mdl"
		}
		"114"
		{
			"type"		"model"
			"model"		"models/props_c17/playground_jungle_gym01b.mdl"
		}
		"115"
		{
			"type"		"model"
			"model"		"models/props_c17/playground_swingset01.mdl"
		}
		"116"
		{
			"type"		"header"
			"text"		"Office"
		}
		"117"
		{
			"type"		"model"
			"model"		"models/props/de_tides/lights_studio.mdl"
		}
		"118"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/wall_console1.mdl"
		}
		"119"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/wall_console2.mdl"
		}
		"120"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/wall_console3.mdl"
		}
		"121"
		{
			"type"		"model"
			"model"		"models/props_lab/servers.mdl"
		}
		"122"
		{
			"type"		"model"
			"model"		"models/props_lab/workspace001.mdl"
		}
		"123"
		{
			"type"		"model"
			"model"		"models/props_lab/workspace002.mdl"
		}
		"124"
		{
			"type"		"model"
			"model"		"models/props_lab/workspace003.mdl"
		}
		"125"
		{
			"type"		"model"
			"model"		"models/props_lab/workspace004.mdl"
		}
		"126"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/desk_console1.mdl"
		}
		"127"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/desk_console1a.mdl"
		}
		"128"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/desk_console1b.mdl"
		}
		"129"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/desk_console2.mdl"
		}
		"130"
		{
			"type"		"model"
			"model"		"models/props/de_prodigy/desk_console3.mdl"
		}
	}
	"name"		"VIP++ Props"
	"version"		"3"
}

]])