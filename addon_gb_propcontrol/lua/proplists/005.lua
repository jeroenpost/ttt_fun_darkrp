ttt_fun_spawn5 = util.KeyValuesToTable([[    "TableToKeyValues"
        {
            "parentid"		"1"
        "icon"		"icon16/key.png"
        "id"		"5"
        "contents"
        {
            "1"
        {
            "type"		"header"
        "text"		"VIP Props"
    }
        "2"
        {
            "type"		"model"
        "model"		"models/props/cs_havana/bookcase_small.mdl"
    }
        "3"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Bookshelf3.mdl"
    }
        "4"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Bookshelf1.mdl"
    }
        "5"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/furniture_shelf01a.mdl"
    }
        "6"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/shelves_wood.mdl"
    }
        "7"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/shelves.mdl"
    }
        "8"
        {
            "type"		"model"
        "model"		"models\props/cs_office/file_cabinet1.mdl"
    }
        "9"
        {
            "type"		"model"
        "model"		"models\props/cs_office/file_cabinet1_group.mdl"
    }
        "10"
        {
            "type"		"model"
        "model"		"models\props/cs_office/file_cabinet2.mdl"
    }
        "11"
        {
            "type"		"model"
        "model"		"models\props/cs_office/file_cabinet3.mdl"
    }
        "12"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/BarrelWarning.mdl"
    }
        "13"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/ChainTrainStationSign.mdl"
    }
        "14"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/ConsolePanelLoadingBay.mdl"
    }
        "15"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/FireHydrant.mdl"
    }
        "16"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/couch.mdl"
    }
        "17"
        {
            "type"		"model"
        "model"		"models\props/cs_office/sofa.mdl"
    }
        "18"
        {
            "type"		"model"
        "model"		"models\props/cs_office/sofa_chair.mdl"
    }
        "19"
        {
            "type"		"model"
        "model"		"models/nova/chair_office02.mdl"
    }
        "20"
        {
            "type"		"model"
        "model"		"models/nova/chair_office01.mdl"
    }
        "21"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Chair_office.mdl"
    }
        "22"
        {
            "type"		"model"
        "model"		"models/nova/chair_wood01.mdl"
    }
        "23"
        {
            "type"		"model"
        "model"		"models/nova/chair_plastic01.mdl"
    }
        "24"
        {
            "type"		"model"
        "model"		"models/props_trainstation/bench_indoor001a.mdl"
    }
        "25"
        {
            "type"		"model"
        "model"		"models/nova/jeep_seat.mdl"
    }
        "26"
        {
            "type"		"model"
        "model"		"models/nova/airboat_seat.mdl"
    }
        "27"
        {
            "type"		"model"
        "model"		"models/props_trainstation/BenchOutdoor01a.mdl"
    }
        "28"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/wood_bench.mdl"
    }
        "29"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/HandTruck.mdl"
    }
        "30"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/bottle03.mdl"
    }
        "31"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/ACUnit02.mdl"
    }
        "32"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/camera.mdl"
    }
        "33"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/black_dama.mdl"
    }
        "34"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/white_dama.mdl"
    }
        "35"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/NoParking.mdl"
    }
        "36"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/NoStopsSign.mdl"
    }
        "37"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/stoplight.mdl"
    }
        "38"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/TrainStationSign.mdl"
    }
        "39"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/axe.mdl"
    }
        "40"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/bathroomwallhole01_wood_broken.mdl"
    }
        "41"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/newspaperstack01.mdl"
    }
        "42"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/caseofbeer01.mdl"
    }
        "43"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/circularsaw01.mdl"
    }
        "44"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/wood_table.mdl"
    }
        "45"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Table_coffee.mdl"
    }
        "46"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/table_kitchen.mdl"
    }
        "47"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/table_shed.mdl"
    }
        "48"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/BoulderRing01.mdl"
    }
        "49"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/fishriver01.mdl"
    }
        "50"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/lightfixture01.mdl"
    }
        "51"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Light_shop.mdl"
    }
        "52"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Light_ceiling.mdl"
    }
        "53"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/light_shop2.mdl"
    }
        "54"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/wall_light.mdl"
    }
        "55"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/spotlight.mdl"
    }
        "56"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/Floodlight.mdl"
    }
        "57"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/Floodlight01.mdl"
    }
        "58"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/Floodlight02.mdl"
    }
        "59"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/streetlight.mdl"
    }
        "60"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/light_shop2.mdl"
    }
        "61"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/mailbox01.mdl"
    }
        "62"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/microwave01.mdl"
    }
        "63"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/militiawindow02_breakable.mdl"
    }
        "64"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/militiawindow02_breakable_frame.mdl"
    }
        "65"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/wndw01.mdl"
    }
        "66"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/militiawindow01.mdl"
    }
        "67"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/toothbrushset01.mdl"
    }
        "68"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/toilet.mdl"
    }
        "69"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/oldphone01.mdl"
    }
        "70"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/paintbucket01.mdl"
    }
        "71"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/reloadingpress01.mdl"
    }
        "72"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/reload_bullet_tray.mdl"
    }
        "73"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/car_militia.mdl"
    }
        "74"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/urine_trough.mdl"
    }
        "75"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/vent01.mdl"
    }
        "76"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/light_red2.mdl"
    }
        "77"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/StreetSign01.mdl"
    }
        "78"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/StreetSign02.mdl"
    }
        "79"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/LifePreserver.mdl"
    }
        "80"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/VentilationDuct02.mdl"
    }
        "81"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/wall_vent.mdl"
    }
        "82"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/light_red1.mdl"
    }
        "83"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/refrigerator01.mdl"
    }
        "84"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/dryer.mdl"
    }
        "85"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/stove01.mdl"
    }
        "86"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/ladderwood.mdl"
    }
        "87"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/sawhorse.mdl"
    }
        "88"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/sheetrock_leaning.mdl"
    }
        "89"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/roof_vent.mdl"
    }
        "90"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/emergency_lighta.mdl"
    }
        "91"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/VentilationDuct02Large.mdl"
    }
        "92"
        {
            "type"		"model"
        "model"		"models/food/burger.mdl"
    }
        "93"
        {
            "type"		"model"
        "model"		"models/props_junk/garbage128_composite001c.mdl"
    }
        "94"
        {
            "type"		"model"
        "model"		"models/props_junk/garbage_bag001a.mdl"
    }
        "95"
        {
            "type"		"model"
        "model"		"models/props_junk/garbage128_composite001b.mdl"
    }
        "96"
        {
            "type"		"model"
        "model"		"models/props/cs_havana/bookcase_large.mdl"
    }
        "97"
        {
            "type"		"model"
        "model"		"models/food/hotdog.mdl"
    }
        "98"
        {
            "type"		"model"
        "model"		"models/props_junk/garbage_carboard002a.mdl"
    }
        "99"
        {
            "type"		"model"
        "model"		"models/props_junk/TrashBin01a.mdl"
    }
        "100"
        {
            "type"		"model"
        "model"		"models/props_junk/Wheebarrow01a.mdl"
    }
        "101"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/reload_scale.mdl"
    }
        "102"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/file_cabinet1_group.mdl"
    }
        "103"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/skylight.mdl"
    }
        "104"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/skylight_glass.mdl"
    }
        "105"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/equipment1.mdl"
    }
        "106"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/SmokeStack01.mdl"
    }
        "107"
        {
            "type"		"model"
        "model"		"models\props/cs_office/address.mdl"
    }
        "108"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Awning_long.mdl"
    }
        "109"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Awning_short.mdl"
    }
        "110"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Water_bottle.mdl"
    }
        "111"
        {
            "type"		"model"
        "model"		"models\props/cs_office/coffee_mug.mdl"
    }
        "112"
        {
            "type"		"model"
        "model"		"models\props/cs_office/coffee_mug2.mdl"
    }
        "113"
        {
            "type"		"model"
        "model"		"models\props/cs_office/coffee_mug3.mdl"
    }
        "114"
        {
            "type"		"model"
        "model"		"models\props/cs_office/TV_plasma.mdl"
    }
        "115"
        {
            "type"		"model"
        "model"		"models\props/cs_office/computer.mdl"
    }
        "116"
        {
            "type"		"model"
        "model"		"models/props_lab/monitor01a.mdl"
    }
        "117"
        {
            "type"		"model"
        "model"		"models/props_lab/monitor01b.mdl"
    }
        "118"
        {
            "type"		"model"
        "model"		"models\props/cs_office/computer_caseB.mdl"
    }
        "119"
        {
            "type"		"model"
        "model"		"models\props/cs_office/computer_keyboard.mdl"
    }
        "120"
        {
            "type"		"model"
        "model"		"models\props/cs_office/computer_monitor.mdl"
    }
        "121"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/tv_console.mdl"
    }
        "122"
        {
            "type"		"model"
        "model"		"models\props/cs_office/microwave.mdl"
    }
        "123"
        {
            "type"		"model"
        "model"		"models\props/cs_office/computer_mouse.mdl"
    }
        "124"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Exit_ceiling.mdl"
    }
        "125"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Exit_wall.mdl"
    }
        "126"
        {
            "type"		"model"
        "model"		"models\props/cs_office/file_box.mdl"
    }
        "127"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Fire_Extinguisher.mdl"
    }
        "128"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Light_security.mdl"
    }
        "129"
        {
            "type"		"model"
        "model"		"models/maxofs2d/balloon_classic.mdl"
    }
        "130"
        {
            "type"		"model"
        "model"		"models/player/items/humans/top_hat.mdl"
    }
        "131"
        {
            "type"		"model"
        "model"		"models/maxofs2d/balloon_gman.mdl"
    }
        "132"
        {
            "type"		"model"
        "model"		"models/maxofs2d/balloon_mossman.mdl"
    }
        "133"
        {
            "type"		"model"
        "model"		"models/maxofs2d/button_01.mdl"
    }
        "134"
        {
            "type"		"model"
        "model"		"models/maxofs2d/button_02.mdl"
    }
        "135"
        {
            "type"		"model"
        "model"		"models/maxofs2d/button_03.mdl"
    }
        "136"
        {
            "type"		"model"
        "model"		"models/maxofs2d/button_04.mdl"
    }
        "137"
        {
            "type"		"model"
        "model"		"models/maxofs2d/button_05.mdl"
    }
        "138"
        {
            "type"		"model"
        "model"		"models/maxofs2d/button_06.mdl"
    }
        "139"
        {
            "type"		"model"
        "model"		"models/maxofs2d/button_slider.mdl"
    }
        "140"
        {
            "type"		"model"
        "model"		"models/maxofs2d/camera.mdl"
    }
        "141"
        {
            "type"		"model"
        "model"		"models/maxofs2d/companion_doll.mdl"
    }
        "142"
        {
            "type"		"model"
        "model"		"models/maxofs2d/construct_sign.mdl"
    }
        "143"
        {
            "type"		"model"
        "model"		"models/maxofs2d/hover_basic.mdl"
    }
        "144"
        {
            "type"		"model"
        "model"		"models/maxofs2d/hover_classic.mdl"
    }
        "145"
        {
            "type"		"model"
        "model"		"models/maxofs2d/hover_plate.mdl"
    }
        "146"
        {
            "type"		"model"
        "model"		"models/maxofs2d/hover_propeller.mdl"
    }
        "147"
        {
            "type"		"model"
        "model"		"models/maxofs2d/hover_rings.mdl"
    }
        "148"
        {
            "type"		"model"
        "model"		"models/maxofs2d/lamp_flashlight.mdl"
    }
        "149"
        {
            "type"		"model"
        "model"		"models/maxofs2d/lamp_projector.mdl"
    }
        "150"
        {
            "type"		"model"
        "model"		"models/maxofs2d/light_tubular.mdl"
    }
        "151"
        {
            "type"		"model"
        "model"		"models/maxofs2d/logo_gmod_b.mdl"
    }
        "152"
        {
            "type"		"model"
        "model"		"models/maxofs2d/motion_sensor.mdl"
    }
        "153"
        {
            "type"		"model"
        "model"		"models/maxofs2d/thruster_projector.mdl"
    }
        "154"
        {
            "type"		"model"
        "model"		"models/maxofs2d/thruster_propeller.mdl"
    }
        "155"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/boxes_frontroom.mdl"
    }
        "156"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/boxes_garage_lower.mdl"
    }
        "157"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/washer_box.mdl"
    }
        "158"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/washer_box2.mdl"
    }
        "159"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/NuclearContainerBoxClosed.mdl"
    }
        "160"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Cardboard_box01.mdl"
    }
        "161"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Cardboard_box02.mdl"
    }
        "162"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Cardboard_box03.mdl"
    }
        "163"
        {
            "type"		"model"
        "model"		"models/maxofs2d/cube_tool.mdl"
    }
        "164"
        {
            "type"		"model"
        "model"		"models/props_junk/wood_crate001a.mdl"
    }
        "165"
        {
            "type"		"model"
        "model"		"models/props_junk/wood_crate001a_damaged.mdl"
    }
        "166"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/crate_extrasmall.mdl"
    }
        "167"
        {
            "type"		"model"
        "model"		"models/props_junk/wood_crate002a.mdl"
    }
        "168"
        {
            "type"		"model"
        "model"		"models/props_junk/wood_pallet001a.mdl"
    }
        "169"
        {
            "type"		"model"
        "model"		"models/props_lab/blastdoor001a.mdl"
    }
        "170"
        {
            "type"		"model"
        "model"		"models/props_lab/blastdoor001b.mdl"
    }
        "171"
        {
            "type"		"model"
        "model"		"models/props_lab/blastdoor001c.mdl"
    }
        "172"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offcertificatea.mdl"
    }
        "173"
        {
            "type"		"model"
        "model"		"models/maxofs2d/gm_painting.mdl"
    }
        "174"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offcorkboarda.mdl"
    }
        "175"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offinspa.mdl"
    }
        "176"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offinspb.mdl"
    }
        "177"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offinspc.mdl"
    }
        "178"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offinspd.mdl"
    }
        "179"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offinspf.mdl"
    }
        "180"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offinspg.mdl"
    }
        "181"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offpaintinga.mdl"
    }
        "182"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offpaintingb.mdl"
    }
        "183"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offpaintingd.mdl"
    }
        "184"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offpaintinge.mdl"
    }
        "185"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offpaintingf.mdl"
    }
        "186"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offpaintingg.mdl"
    }
        "187"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offpaintingh.mdl"
    }
        "188"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offpaintingi.mdl"
    }
        "189"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offpaintingj.mdl"
    }
        "190"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offpaintingk.mdl"
    }
        "191"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offpaintingl.mdl"
    }
        "192"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offpaintingm.mdl"
    }
        "193"
        {
            "type"		"model"
        "model"		"models\props/cs_office/offpaintingo.mdl"
    }
        "194"
        {
            "type"		"model"
        "model"		"models/noesis/donut.mdl"
    }
        "195"
        {
            "type"		"model"
        "model"		"models\props/cs_office/paperbox_pile_01.mdl"
    }
        "196"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Paper_towels.mdl"
    }
        "197"
        {
            "type"		"model"
        "model"		"models\props/cs_office/phone.mdl"
    }
        "198"
        {
            "type"		"model"
        "model"		"models\props/cs_office/phone_p1.mdl"
    }
        "199"
        {
            "type"		"model"
        "model"		"models\props/cs_office/phone_p2.mdl"
    }
        "200"
        {
            "type"		"model"
        "model"		"models\props/cs_office/plant01.mdl"
    }
        "201"
        {
            "type"		"model"
        "model"		"models\props/cs_office/projector.mdl"
    }
        "202"
        {
            "type"		"model"
        "model"		"models\props/cs_office/projector_remote.mdl"
    }
        "203"
        {
            "type"		"model"
        "model"		"models\props/cs_office/radio.mdl"
    }
        "204"
        {
            "type"		"model"
        "model"		"models\props/cs_office/trash_can_p8.mdl"
    }
        "205"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Vending_machine.mdl"
    }
        "206"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/NuclearControlBox.mdl"
    }
        "207"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/NuclearTestCabinet.mdl"
    }
        "208"
        {
            "type"		"model"
        "model"		"models/props/cs_assault/TicketMachine.mdl"
    }
        "209"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/footlocker01_open.mdl"
    }
        "210"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/food_stack.mdl"
    }
        "211"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/fertilizer.mdl"
    }
        "212"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Bookshelf2.mdl"
    }
        "213"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/IndustrialLight01.mdl"
    }
        "214"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/clock.mdl"
    }
        "215"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/militiarock05.mdl"
    }
        "216"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/rocksteppingstones01.mdl"
    }
        "217"
        {
            "type"		"model"
        "model"		"models/props/de_nuke/cinderblock_stack.mdl"
    }
        "218"
        {
            "type"		"model"
        "model"		"models\props/cs_office/Snowman_hat.mdl"
    }
        "219"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/footlocker01_closed.mdl"
    }
        "220"
        {
            "type"		"model"
        "model"		"models\props/CS_militia/housefence_door.mdl"
    }
        "221"
        {
            "type"		"model"
        "model"		"models/XQM/Rails/gumball_1.mdl"
    }
        "222"
        {
            "type"		"model"
        "model"		"models/XQM/Rails/trackball_1.mdl"
    }
        "223"
        {
            "type"		"model"
        "model"		"models/props_phx/gibs/flakgib1.mdl"
    }
        "224"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_intmonitor001.mdl"
    }
        "225"
        {
            "type"		"model"
        "model"		"models/props_phx/gibs/wooden_wheel2_gib1.mdl"
    }
        "226"
        {
            "type"		"model"
        "model"		"models/props_phx/gibs/wooden_wheel2_gib2.mdl"
    }
        "227"
        {
            "type"		"model"
        "model"		"models/props_phx/gibs/wooden_wheel1_gib1.mdl"
    }
        "228"
        {
            "type"		"model"
        "model"		"models/props_phx/gibs/wooden_wheel1_gib2.mdl"
    }
        "229"
        {
            "type"		"model"
        "model"		"models/props_phx/gibs/wooden_wheel1_gib3.mdl"
    }
        "230"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_interface001.mdl"
    }
        "231"
        {
            "type"		"model"
        "model"		"models/props_combine/combine_monitorbay.mdl"
    }
        "232"
        {
            "type"		"model"
        "model"		"models/props_phx/gibs/tire1_gib.mdl"
    }
        "233"
        {
            "type"		"model"
        "model"		"models/props_lab/tpplug.mdl"
    }
        "234"
        {
            "type"		"model"
        "model"		"models/props_lab/tpplugholder_single.mdl"
    }
        "235"
        {
            "type"		"model"
        "model"		"models/props_lab/tpplugholder.mdl"
    }
        "236"
        {
            "type"		"model"
        "model"		"models/props_trainstation/payphone001a.mdl"
    }
        "237"
        {
            "type"		"model"
        "model"		"models/props_trainstation/payphone_reciever001a.mdl"
    }
        "238"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/black_bishop.mdl"
    }
        "239"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/black_knight.mdl"
    }
        "240"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/black_queen.mdl"
    }
        "241"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/black_king.mdl"
    }
        "242"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/black_rook.mdl"
    }
        "243"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/black_pawn.mdl"
    }
        "244"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/white_queen.mdl"
    }
        "245"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/white_knight.mdl"
    }
        "246"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/white_pawn.mdl"
    }
        "247"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/white_bishop.mdl"
    }
        "248"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/white_king.mdl"
    }
        "249"
        {
            "type"		"model"
        "model"		"models/props_phx/games/chess/white_rook.mdl"
    }
    }
        "name"		"VIP Props"
        "version"		"3"
    }
]])