ttt_fun_spawn7 = util.KeyValuesToTable([[   "TableToKeyValues"
        {
            "parentid"		"5"
        "icon"		"icon16/folder_brick.png"
        "id"		"7"
        "contents"
        {
            "1"
        {
            "type"		"model"
        "model"		"models/hunter/geometric/hex025x1.mdl"
    }
        "2"
        {
            "type"		"model"
        "model"		"models/hunter/geometric/hex05x1.mdl"
    }
        "3"
        {
            "type"		"model"
        "model"		"models/hunter/geometric/hex1x05.mdl"
    }
        "4"
        {
            "type"		"model"
        "model"		"models/hunter/geometric/hex1x1.mdl"
    }
        "5"
        {
            "type"		"model"
        "model"		"models/hunter/geometric/para1x1.mdl"
    }
        "6"
        {
            "type"		"model"
        "model"		"models/hunter/geometric/pent1x1.mdl"
    }
        "7"
        {
            "type"		"model"
        "model"		"models/hunter/geometric/tri1x1eq.mdl"
    }
        "8"
        {
            "type"		"model"
        "model"		"models/hunter/misc/cone1x05.mdl"
    }
        "9"
        {
            "type"		"model"
        "model"		"models/hunter/misc/cone1x1.mdl"
    }
        "10"
        {
            "type"		"model"
        "model"		"models/hunter/misc/cone2x05.mdl"
    }
        "11"
        {
            "type"		"model"
        "model"		"models/hunter/misc/cone2x1.mdl"
    }
        "12"
        {
            "type"		"model"
        "model"		"models/hunter/misc/cone2x2.mdl"
    }
        "13"
        {
            "type"		"model"
        "model"		"models/hunter/misc/cone4x1.mdl"
    }
        "14"
        {
            "type"		"model"
        "model"		"models/hunter/misc/cone4x2.mdl"
    }
        "15"
        {
            "type"		"model"
        "model"		"models/hunter/misc/lift2x2.mdl"
    }
        "16"
        {
            "type"		"model"
        "model"		"models/hunter/misc/platehole1x1a.mdl"
    }
        "17"
        {
            "type"		"model"
        "model"		"models/hunter/misc/platehole1x1b.mdl"
    }
        "18"
        {
            "type"		"model"
        "model"		"models/hunter/misc/platehole1x1c.mdl"
    }
        "19"
        {
            "type"		"model"
        "model"		"models/hunter/misc/platehole1x1d.mdl"
    }
        "20"
        {
            "type"		"model"
        "model"		"models/hunter/misc/platehole4x4.mdl"
    }
        "21"
        {
            "type"		"model"
        "model"		"models/hunter/misc/platehole4x4b.mdl"
    }
        "22"
        {
            "type"		"model"
        "model"		"models/hunter/misc/platehole4x4c.mdl"
    }
        "23"
        {
            "type"		"model"
        "model"		"models/hunter/misc/platehole4x4d.mdl"
    }
        "24"
        {
            "type"		"model"
        "model"		"models/hunter/misc/roundthing1.mdl"
    }
        "25"
        {
            "type"		"model"
        "model"		"models/hunter/misc/roundthing2.mdl"
    }
        "26"
        {
            "type"		"model"
        "model"		"models/hunter/misc/roundthing3.mdl"
    }
        "27"
        {
            "type"		"model"
        "model"		"models/hunter/misc/roundthing4.mdl"
    }
        "28"
        {
            "type"		"model"
        "model"		"models/hunter/misc/shell2x2.mdl"
    }
        "29"
        {
            "type"		"model"
        "model"		"models/hunter/misc/shell2x2a.mdl"
    }
        "30"
        {
            "type"		"model"
        "model"		"models/hunter/misc/shell2x2b.mdl"
    }
        "31"
        {
            "type"		"model"
        "model"		"models/hunter/misc/shell2x2c.mdl"
    }
        "32"
        {
            "type"		"model"
        "model"		"models/hunter/misc/shell2x2d.mdl"
    }
        "33"
        {
            "type"		"model"
        "model"		"models/hunter/misc/shell2x2e.mdl"
    }
        "34"
        {
            "type"		"model"
        "model"		"models/hunter/misc/shell2x2x45.mdl"
    }
        "35"
        {
            "type"		"model"
        "model"		"models/hunter/misc/sphere025x025.mdl"
    }
        "36"
        {
            "type"		"model"
        "model"		"models/hunter/misc/sphere075x075.mdl"
    }
        "37"
        {
            "type"		"model"
        "model"		"models/hunter/misc/sphere175x175.mdl"
    }
        "38"
        {
            "type"		"model"
        "model"		"models/hunter/misc/sphere1x1.mdl"
    }
        "39"
        {
            "type"		"model"
        "model"		"models/hunter/misc/sphere2x2.mdl"
    }
        "40"
        {
            "type"		"model"
        "model"		"models/hunter/misc/sphere375x375.mdl"
    }
        "41"
        {
            "type"		"model"
        "model"		"models/hunter/misc/squarecap1x1x1.mdl"
    }
        "42"
        {
            "type"		"model"
        "model"		"models/hunter/misc/squarecap2x1x1.mdl"
    }
        "43"
        {
            "type"		"model"
        "model"		"models/hunter/misc/squarecap2x1x2.mdl"
    }
        "44"
        {
            "type"		"model"
        "model"		"models/hunter/misc/squarecap2x2x2.mdl"
    }
        "45"
        {
            "type"		"model"
        "model"		"models/hunter/misc/stair1x1.mdl"
    }
        "46"
        {
            "type"		"model"
        "model"		"models/hunter/misc/stair1x1inside.mdl"
    }
        "47"
        {
            "type"		"model"
        "model"		"models/hunter/misc/stair1x1outside.mdl"
    }
        "48"
        {
            "type"		"header"
        "text"		"Blocks"
    }
        "49"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube025x025x025.mdl"
    }
        "50"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube025x05x025.mdl"
    }
        "51"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube025x075x025.mdl"
    }
        "52"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube025x125x025.mdl"
    }
        "53"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube025x150x025.mdl"
    }
        "54"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube025x1x025.mdl"
    }
        "55"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube025x2x025.mdl"
    }
        "56"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube025x3x025.mdl"
    }
        "57"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube025x4x025.mdl"
    }
        "58"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube025x5x025.mdl"
    }
        "59"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube025x6x025.mdl"
    }
        "60"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube025x7x025.mdl"
    }
        "61"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube025x8x025.mdl"
    }
        "62"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x05x025.mdl"
    }
        "63"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x05x05.mdl"
    }
        "64"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x075x025.mdl"
    }
        "65"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x105x05.mdl"
    }
        "66"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x1x025.mdl"
    }
        "67"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x1x05.mdl"
    }
        "68"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x2x025.mdl"
    }
        "69"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x2x05.mdl"
    }
        "70"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x3x025.mdl"
    }
        "71"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x3x05.mdl"
    }
        "72"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x4x025.mdl"
    }
        "73"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x4x05.mdl"
    }
        "74"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x5x025.mdl"
    }
        "75"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x5x05.mdl"
    }
        "76"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x6x025.mdl"
    }
        "77"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x6x05.mdl"
    }
        "78"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x7x025.mdl"
    }
        "79"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x7x05.mdl"
    }
        "80"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x8x025.mdl"
    }
        "81"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube05x8x05.mdl"
    }
        "82"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x075x025.mdl"
    }
        "83"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x075x075.mdl"
    }
        "84"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x1x025.mdl"
    }
        "85"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x1x075.mdl"
    }
        "86"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x1x1.mdl"
    }
        "87"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x2x025.mdl"
    }
        "88"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x2x075.mdl"
    }
        "89"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x2x1.mdl"
    }
        "90"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x3x025.mdl"
    }
        "91"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x3x075.mdl"
    }
        "92"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x3x1.mdl"
    }
        "93"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x4x025.mdl"
    }
        "94"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x4x075.mdl"
    }
        "95"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x5x075.mdl"
    }
        "96"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x6x025.mdl"
    }
        "97"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x6x075.mdl"
    }
        "98"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x7x075.mdl"
    }
        "99"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x8x025.mdl"
    }
        "100"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube075x8x075.mdl"
    }
        "101"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube125x125x025.mdl"
    }
        "102"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube150x150x025.mdl"
    }
        "103"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x150x1.mdl"
    }
        "104"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x1x025.mdl"
    }
        "105"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x1x05.mdl"
    }
        "106"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x1x1.mdl"
    }
        "107"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x2x025.mdl"
    }
        "108"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x2x05.mdl"
    }
        "109"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x2x1.mdl"
    }
        "110"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x3x025.mdl"
    }
        "111"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x3x1.mdl"
    }
        "112"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x4x025.mdl"
    }
        "113"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x4x05.mdl"
    }
        "114"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x4x1.mdl"
    }
        "115"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x5x025.mdl"
    }
        "116"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x6x025.mdl"
    }
        "117"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x6x05.mdl"
    }
        "118"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x6x1.mdl"
    }
        "119"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x7x025.mdl"
    }
        "120"
        {
            "type"		"model"
        "model"		"models/hunter/blocks/cube1x8x025.mdl"
    }
    }
        "name"		"Geometric"
        "version"		"3"
    }
]])