AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include( "shared.lua" )

function ENT:Initialize()
    self:SetModel( "models/Humans/Group02/male_09.mdl" )
    self:SetHullType( HULL_HUMAN )
    self:SetHullSizeNormal( )
    self:SetNPCState( NPC_STATE_SCRIPT )
    self:SetSolid( SOLID_BBOX )
    self:CapabilitiesAdd( CAP_ANIMATEDFACE )
    self:SetUseType( SIMPLE_USE )
    self:DropToFloor()

    self:SetMaxYawSpeed( 180 )
end

function VEHUPGRADE_NPCSpawn()	
	if not file.IsDir("craphead_scripts", "DATA") then
		file.CreateDir("craphead_scripts", "DATA")
	end
	
	if not file.IsDir("craphead_scripts/vehicle_upgrades/".. string.lower(string.lower(game.GetMap())) .."", "DATA") then
		file.CreateDir("craphead_scripts/vehicle_upgrades/".. string.lower(string.lower(game.GetMap())) .."", "DATA")
	end
	
	if not file.Exists( "craphead_scripts/vehicle_upgrades/".. string.lower(string.lower(game.GetMap())) .."/vehupgradesnpc_location.txt", "DATA" ) then
		file.Write("craphead_scripts/vehicle_upgrades/".. string.lower(string.lower(game.GetMap())) .."/vehupgradesnpc_location.txt", "0;-0;-0;0;0;0", "DATA")
	end
	
	local PositionFile = file.Read("craphead_scripts/vehicle_upgrades/".. string.lower(string.lower(game.GetMap())) .."/vehupgradesnpc_location.txt", "DATA")
	 
	local ThePosition = string.Explode( ";", PositionFile )
		
	local TheVector = Vector(ThePosition[1], ThePosition[2], ThePosition[3])
	local TheAngle = Angle(tonumber(ThePosition[4]), ThePosition[5], ThePosition[6])

    if ThePosition[1] == 0 and ThePosition[2] == 0 and ThePosition[3] == 0 then return end
	local VehUpgradesNPC = ents.Create("npc_vehupgrades")
	VehUpgradesNPC:SetModel( VEHUPGRADE_NPCModel )
	VehUpgradesNPC:SetPos( TheVector )
	VehUpgradesNPC:SetAngles( TheAngle )
	VehUpgradesNPC:Spawn()
	VehUpgradesNPC:SetMoveType( MOVETYPE_NONE )
	VehUpgradesNPC:SetSolid( SOLID_BBOX )
	VehUpgradesNPC:SetCollisionGroup( COLLISION_GROUP_PLAYER )
		

end
--timer.Simple(1, VEHUPGRADE_NPCSpawn)

function VEHUPGRADE_NPCPosition( ply )
	if ply:IsAdmin() then
		local HisVector = string.Explode(" ", tostring(ply:GetPos()))
		local HisAngles = string.Explode(" ", tostring(ply:GetAngles()))
		
		file.Write("craphead_scripts/vehicle_upgrades/".. string.lower(string.lower(game.GetMap())) .."/vehupgradesnpc_location.txt", ""..(HisVector[1])..";"..(HisVector[2])..";"..(HisVector[3])..";"..(HisAngles[1])..";"..(HisAngles[2])..";"..(HisAngles[3]).."", "DATA")
		ply:ChatPrint("New position for the upgrades NPC has been succesfully set. Please restart your server!")
	else
		ply:ChatPrint("Only administrators can perform this action")
	end
end
concommand.Add("vehupgradenpc_setpos", VEHUPGRADE_NPCPosition)

function ENT:AcceptInput(ply, caller)
	if caller:IsPlayer() && !caller.CantUse then
		caller.CantUse = true
		timer.Simple(3, function()  caller.CantUse = false end)
		
		umsg.Start("VEHUPGRADE_CarUpgrades", caller)
		umsg.End()
	end
end
