UpgradedVehicles = {}

UpgradedVehicles["models/tdmcars/cad_lmp.mdl"] = {
	Name = "Cadillac LMP",
	Model = "models/tdmcars/cad_lmp.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/chev_blazer.mdl"] = {
	Name = "Chevrolet Blazer",
	Model = "models/tdmcars/chev_blazer.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/chr_ptcruiser.mdl"] = {
	Name = "Chrysler PT Cruiser",
	Model = "models/tdmcars/chr_ptcruiser.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/citroen_c1.mdl"] = {
	Name = "Citroen C1",
	Model = "models/tdmcars/citroen_c1.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/dod_charger12.mdl"] = {
	Name = "Dodge Charger SRT8 2012",
	Model = "models/tdmcars/dod_charger12.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/fer_enzo.mdl"] = {
	Name = "Ferrari Enzo",
	Model = "models/tdmcars/fer_enzo.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/fer_f50.mdl"] = {
	Name = "Ferrari F50",
	Model = "models/tdmcars/fer_f50.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/crownvic_taxi.mdl"] = {
	Name = "Ford Crown Victoria Taxi",
	Model = "models/tdmcars/crownvic_taxi.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/gmc_syclone.mdl"] = {
	Name = "GMC Syclone",
	Model = "models/tdmcars/gmc_syclone.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/coltralliart.mdl"] = {
	Name = "Mitsubishi Colt Ralliart",
	Model = "models/tdmcars/coltralliart.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/bmwm1.mdl"] = {
	Name = "BMW M1 1981",
	Model = "models/tdmcars/bmwm1.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/bug_eb110.mdl"] = {
	Name = "Bugatti EB110",
	Model = "models/tdmcars/bug_eb110.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/bug_veyron.mdl"] = {
	Name = "Bugatti Veyron",
	Model = "models/tdmcars/bug_veyron.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/bug_veyronss.mdl"] = {
	Name = "Bugatti Veyron SS",
	Model = "models/tdmcars/bug_veyronss.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/audir8.mdl"] = {
	Name = "Audi R8 V10",
	Model = "models/tdmcars/audir8.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/aud_rs4avant.mdl"] = {
	Name = "Audi RS4 Avant",
	Model = "models/tdmcars/aud_rs4avant.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/murcielago.mdl"] = {
	Name = "Lamborghini Murcielago LP640",
	Model = "models/tdmcars/murcielago.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/ferrari250gt.mdl"] = {
	Name = "Ferrari 250 GT",
	Model = "models/tdmcars/ferrari250gt.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/bmw507.mdl"] = {
	Name = "BMW 507",
	Model = "models/tdmcars/bmw507.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/ford_transit.mdl"] = {
	Name = "Ford Transit",
	Model = "models/tdmcars/ford_transit.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/dbs.mdl"] = {
	Name = "Aston Martin DBS",
	Model = "models/tdmcars/dbs.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/chev_impala96.mdl"] = {
	Name = "Chevrolet Impala SS 96",
	Model = "models/tdmcars/chev_impala96.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/s5.mdl"] = {
	Name = "Audi S5",
	Model = "models/tdmcars/s5.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/auditt.mdl"] = {
	Name = "Audi TT 07",
	Model = "models/tdmcars/auditt.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/bmwm3e92.mdl"] = {
	Name = "BMW M3 E92",
	Model = "models/tdmcars/bmwm3e92.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/bmwm5e60.mdl"] = {
	Name = "BMW M5 E60",
	Model = "models/tdmcars/bmwm5e60.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/cad_escalade.mdl"] = {
	Name = "Cadillac Escalade 2012",
	Model = "models/tdmcars/cad_escalade.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/69camaro.mdl"] = {
	Name = "Chevrolet Camaro SS 69 DBS",
	Model = "models/tdmcars/69camaro.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/chev_camzl1.mdl"] = {
	Name = "Chevrolet Camaro ZL1",
	Model = "models/tdmcars/chev_camzl1.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/chevelless.mdl"] = {
	Name = "Chevrolet Chevelle SS",
	Model = "models/tdmcars/chevelless.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/spark.mdl"] = {
	Name = "Chevrolet Spark",
	Model = "models/tdmcars/spark.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/chr_300c.mdl"] = {
	Name = "Chrysler 300C",
	Model = "models/tdmcars/chr_300c.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/cit_c4.mdl"] = {
	Name = "Citroen C4",
	Model = "models/tdmcars/cit_c4.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/chargersrt8.mdl"] = {
	Name = "Dodge Charger SRT-8",
	Model = "models/tdmcars/chargersrt8.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/dodgeram.mdl"] = {
	Name = "Dodge Ram SRT-10",
	Model = "models/tdmcars/dodgeram.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/fer_458spid.mdl"] = {
	Name = "Ferrari 458 Spider",
	Model = "models/tdmcars/fer_458spid.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/ferrari512tr.mdl"] = {
	Name = "Ferrari 512 TR",
	Model = "models/tdmcars/ferrari512tr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/ford_coupe_40.mdl"] = {
	Name = "1940 Ford Coupe Deluxe",
	Model = "models/tdmcars/ford_coupe_40.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/focusrs.mdl"] = {
	Name = "Ford Focus RS",
	Model = "models/tdmcars/focusrs.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/gt05.mdl"] = {
	Name = "Ford GT 05",
	Model = "models/tdmcars/gt05.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/gt500.mdl"] = {
	Name = "Ford Shelby GT500",
	Model = "models/tdmcars/gt500.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/gmcvan.mdl"] = {
	Name = "GMC Vandura",
	Model = "models/tdmcars/gmcvan.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/civic_typer.mdl"] = {
	Name = "Honda Civic Type R",
	Model = "models/tdmcars/civic_typer.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/hud_hornet.mdl"] = {
	Name = "Hudson Hornet",
	Model = "models/tdmcars/hud_hornet.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/hummerh1.mdl"] = {
	Name = "Hummer H1",
	Model = "models/tdmcars/hummerh1.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/wrangler.mdl"] = {
	Name = "Jeep Wrangler",
	Model = "models/tdmcars/wrangler.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/kia_ceed.mdl"] = {
	Name = "Kia Ceed",
	Model = "models/tdmcars/kia_ceed.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/gallardo.mdl"] = {
	Name = "Lamborghini Gallardo",
	Model = "models/tdmcars/gallardo.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/reventon_roadster.mdl"] = {
	Name = "Lamborghini Reventon",
	Model = "models/tdmcars/reventon_roadster.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/landrover.mdl"] = {
	Name = "Range Rover",
	Model = "models/tdmcars/landrover.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/mx5.mdl"] = {
	Name = "Mazda MX5",
	Model = "models/tdmcars/mx5.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/rx8.mdl"] = {
	Name = "Mazda RX8",
	Model = "models/tdmcars/rx8.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/mercedes_c32.mdl"] = {
	Name = "Mercedes C32 AMG",
	Model = "models/tdmcars/mercedes_c32.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/sl65amg.mdl"] = {
	Name = "Mercedes SL65 AMG",
	Model = "models/tdmcars/sl65amg.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/mer_slsamg.mdl"] = {
	Name = "Mercedes SLS AMG",
	Model = "models/tdmcars/mer_slsamg.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/cooper65.mdl"] = {
	Name = "1965 Mini Cooper",
	Model = "models/tdmcars/cooper65.mdl",
	NitroPower = 1200000,
	HydroPower = 150000,
}

UpgradedVehicles["odels/tdmcars/coltralliart.mdl"] = {
	Name = "Colt Ralliart",
	Model = "models/tdmcars/coltralliart.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/mitsu_evox.mdl"] = {
	Name = "Mitsubishi Evo Lancer X",
	Model = "models/tdmcars/mitsu_evox.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/350z.mdl"] = {
	Name = "Nissan 350z",
	Model = "models/tdmcars/350z.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/nissan_gtr.mdl"] = {
	Name = "Nissan GT-R SpecV",
	Model = "models/tdmcars/nissan_gtr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/skyline_r34.mdl"] = {
	Name = "Nissan Skyline R34",
	Model = "models/tdmcars/skyline_r34.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/zondac12.mdl"] = {
	Name = "Pagani Zonda C12",
	Model = "models/tdmcars/zondac12.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/997gt3.mdl"] = {
	Name = "Porche 997 GT3",
	Model = "models/tdmcars/997gt3.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/cayenne.mdl"] = {
	Name = "Porche Cayenne Turbo S",
	Model = "models/tdmcars/cayenne.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/subaru.mdl"] = {
	Name = "Suburu Impreza WRX",
	Model = "models/tdmcars/subaru.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/prius.mdl"] = {
	Name = "Toyota Prius",
	Model = "models/tdmcars/prius.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/supra.mdl"] = {
	Name = "Toyota Supra",
	Model = "models/tdmcars/supra.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/beetle.mdl"] = {
	Name = "VW Beetle",
	Model = "models/tdmcars/beetle.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/golf3.mdl"] = {
	Name = "VW Golf MK3",
	Model = "models/tdmcars/golf3.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/golf_mk2.mdl"] = {
	Name = "VW Golf MK II",
	Model = "models/tdmcars/golf_mk2.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/scirocco.mdl"] = {
	Name = "VW Scirocco",
	Model = "models/tdmcars/scirocco.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/242turbo.mdl"] = {
	Name = "Volvo 242 Turbo",
	Model = "models/tdmcars/242turbo.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/trucks/scania_high.mdl"] = {
	Name = "Swift Semi-Truck",
	Model = "models/tdmcars/trucks/scania_high.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/trucks/scania_low.mdl"] = {
	Name = "Swift Semi-Truck",
	Model = "models/tdmcars/trucks/scania_low.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}
UpgradedVehicles["models/tdmcars/trucks/scania_med.mdl"] = {
	Name = "Swift Semi-Truck",
	Model = "models/tdmcars/trucks/scania_med.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/tdmcars/bus.mdl"] = {
	Name = "Bus",
	Model = "models/tdmcars/bus.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/bmw-m5.mdl"] = {
	Name = "BMW M5",
	Model = "models/sickness/bmw-m5.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/oracledr.mdl"] = {
	Name = "Oracle XS Sedan",
	Model = "models/sickness/oracledr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/superddr.mdl"] = {
	Name = "Rolls Royce Phantom",
	Model = "models/sickness/superddr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/emperordr.mdl"] = {
	Name = "Lincoln Emperor",
	Model = "models/sickness/emperordr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/blistadr.mdl"] = {
	Name = "Blista Sport",
	Model = "models/sickness/blistadr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/stretchdr.mdl"] = {
	Name = "Dundreary Stretch Limousine",
	Model = "models/sickness/stretchdr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/hummer-h2.mdl"] = {
	Name = "Hummer H2",
	Model = "models/sickness/hummer-h2.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/speedodr.mdl"] = {
	Name = "Vapid ST Van MK2",
	Model = "models/sickness/speedodr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/minivandr.mdl"] = {
	Name = "Vapid Minivan Sport",
	Model = "models/sickness/minivandr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/pmp600fixed.mdl"] = {
	Name = "PMP600",
	Model = "models/sickness/pmp600fixed.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/vanquish.mdl"] = {
	Name = "Aston Martin Vanquish",
	Model = "models/sickness/vanquish.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/stalliondr.mdl"] = {
	Name = "Stallion GTO",
	Model = "models/sickness/stalliondr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/voodoodr.mdl"] = {
	Name = "Voodoo SS",
	Model = "models/sickness/voodoodr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/cavalcadedr.mdl"] = {
	Name = "Albany Cavalcade RS",
	Model = "models/sickness/cavalcadedr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/yankeedr.mdl"] = {
	Name = "DS9 Yankee Truck",
	Model = "models/sickness/yankeedr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/muledr.mdl"] = {
	Name = "Maibatsu Mule Cargo Truck",
	Model = "models/sickness/muledr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/international_2674.mdl"] = {
	Name = "International 2674 Truck",
	Model = "models/sickness/international_2674.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/sultanrsdr.mdl"] = {
	Name = "Mitsubishi Evo",
	Model = "models/sickness/sultanrsdr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}
UpgradedVehicles["models/sickness/buffalodr.mdl"] = {
	Name = "Dodge Charger",
	Model = "models/sickness/buffalodr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}
UpgradedVehicles["models/sickness/f50.mdl"] = {
	Name = "Ferrari F50",
	Model = "models/sickness/f50.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/bobcatdr.mdl"] = {
	Name = "Bobcat",
	Model = "models/sickness/bobcatdr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/cognoscentidr.mdl"] = {
	Name = "Bentley Flying Spurr",
	Model = "models/sickness/cognoscentidr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/phantomdr.mdl"] = {
	Name = "Phantom SemiTruck",
	Model = "models/sickness/phantomdr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/huntleydr.mdl"] = {
	Name = "Vapid Huntley Sport 4x4",
	Model = "models/sickness/huntleydr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/romerodr.mdl"] = {
	Name = "Romero",
	Model = "models/sickness/romerodr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/murcielago.mdl"] = {
	Name = "Murcielago",
	Model =	"models/sickness/murcielago.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/911turbo.mdl"] = {
	Name = "911 Turbo",
	Model = "models/sickness/911turbo.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/360spyder.mdl"] = {
	Name = "360 Spyder",
	Model = "models/sickness/360spyder.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/lotus_elise.mdl"] = {
	Name = "Lotus Elise",
	Model = "models/sickness/lotus_elise.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/shubert_truck1.mdl"] = {
	Name = "Shubert Truck",
	Model = "models/sickness/shubert_truck1.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/gtabus.mdl"] = {
	Name = "Bus",
	Model = "models/sickness/gtabus.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/factiondr.mdl"] = {
	Name = "Faction",
	Model = "models/sickness/factiondr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/hakumaidr.mdl"] = {
	Name = "Hakumia",
	Model = "models/sickness/hakumaidr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/virgodr.mdl"] = {
	Name = "Virgo",
	Model = "models/sickness/virgodr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/1972markiv.mdl"] = {
	Name = "Lincoln MK4",
	Model = "models/sickness/1972markiv.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/diablovt.mdl"] = {
	Name = "Diablo VT",
	Model = "models/sickness/diablovt.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/cl55.mdl"] = {
	Name = "Mercedes CL55 AMG",
	Model = "models/sickness/cl55.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/landstalkerdr.mdl"] = {
	Name = "Landstalker",
	Model = "models/sickness/landstalkerdr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/serranodr.mdl"] = {
	Name = "Serrano",
	Model = "models/sickness/serranodr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/dilettante.mdl"] = {
	Name = "Dilettante",
	Model = "models/sickness/dilettante.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/lokusdr.mdl"] = {
	Name = "Lokus",
	Model = "models/sickness/lokusdr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/meritdr.mdl"] = {
	Name = "Merit",
	Model = "models/sickness/meritdr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/primodr.mdl"] = {
	Name = "Primo",
	Model = "models/sickness/primodr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/willarddr.mdl"] = {
	Name = "Willard",
	Model = "models/sickness/willarddr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/evocitybus.mdl"] = {
	Name = "Evocity Bus",
	Model = "models/sickness/evocitybus.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/steeddr.mdl"] = {
	Name = "Steed",
	Model = "models/sickness/steeddr.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/plowtruck.mdl"] = {
	Name = "Shubert Plowtruck",
	Model = "models/sickness/plowtruck.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}

UpgradedVehicles["models/sickness/zil.mdl"] = {
	Name = "Zil 130",
	Model = "models/sickness/zil.mdl",
	NitroPower = 2200000,
	HydroPower = 250000,
}