function VEHUPGRADE_UnderglowMenu()
	local GUI_GlowMenu = vgui.Create("DFrame")
	GUI_GlowMenu:SetTitle("")
	GUI_GlowMenu:SetSize(420, 260)
	GUI_GlowMenu:Center()
	GUI_GlowMenu.Paint = function(CHPaint)
		-- Draw the menu background color.
		draw.RoundedBox( 6, 0, 0, CHPaint:GetWide(), CHPaint:GetTall(), Color( 255, 255, 255, 150 ) )

		-- Draw the outline of the menu.
		surface.SetDrawColor(0,0,0,255)
		surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), CHPaint:GetTall())

		surface.SetDrawColor(0,0,0,255)
		surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), 25)

		-- Draw the top title.
		draw.SimpleText("Upgrade Central - Underglow Store", "UpgradesTop", 125,12.5, Color(70,70,70,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	GUI_GlowMenu:MakePopup()
	GUI_GlowMenu:ShowCloseButton(false)

	local GUI_Main_Exit = vgui.Create("DButton", GUI_GlowMenu)
	GUI_Main_Exit:SetSize(16,16)
	GUI_Main_Exit:SetPos(400,5)
	GUI_Main_Exit:SetText("")
	GUI_Main_Exit.Paint = function()
		surface.SetMaterial(Material("icon16/cross.png"))
		surface.SetDrawColor(200,200,0,200)
		surface.DrawTexturedRect(0,0,GUI_Main_Exit:GetWide(),GUI_Main_Exit:GetTall())
	end
	GUI_Main_Exit.DoClick = function()
		GUI_GlowMenu:Remove()
	end
	
	local GUI_Back = vgui.Create("DButton", GUI_GlowMenu)
	GUI_Back:SetSize(16,16)
	GUI_Back:SetPos(380,5)
	GUI_Back:SetText("")
	GUI_Back.Paint = function()
		surface.SetMaterial(Material("icon16/arrow_left.png"))
		surface.SetDrawColor(200,200,0,200)
		surface.DrawTexturedRect(0,0,GUI_Back:GetWide(),GUI_Back:GetTall())
	end
	GUI_Back.DoClick = function()
		GUI_GlowMenu:Remove()
		VEHUPGRADE_CarUpgrades()
	end

	local GUI_Glow1 = vgui.Create("DButton", GUI_GlowMenu)
	GUI_Glow1:SetSize(400, 40)
	GUI_Glow1:SetPos(10,30)
	GUI_Glow1:SetText("")
	GUI_Glow1.DoClick = function()
		GUI_GlowMenu:Remove()
		net.Start("VEHUPGRADE_BuyUnderglow")
			net.WriteDouble(1)
		net.SendToServer()
	end
	GUI_Glow1.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 200 -- x pos
		struc.pos[2] = 20 -- y pos
		struc.color = Color(255,255,255,255) -- Red
		struc.text = "Blue" -- Text
		struc.font = "TargetID" -- Font
		struc.xalign = TEXT_ALIGN_CENTER-- Horizontal Alignment
		struc.yalign = TEXT_ALIGN_CENTER -- Vertical Alignment
		draw.Text( struc )
	end

	local GUI_Glow2 = vgui.Create("DButton", GUI_GlowMenu)
	GUI_Glow2:SetSize(400, 40)
	GUI_Glow2:SetPos(10,75)
	GUI_Glow2:SetText("")
	GUI_Glow2.DoClick = function()
		GUI_GlowMenu:Remove()
		net.Start("VEHUPGRADE_BuyUnderglow")
			net.WriteDouble(2)
		net.SendToServer()
	end
	GUI_Glow2.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 200 -- x pos
		struc.pos[2] = 20 -- y pos
		struc.color = Color(255,255,255,255) -- Red
		struc.text = "Green" -- Text
		struc.font = "TargetID" -- Font
		struc.xalign = TEXT_ALIGN_CENTER-- Horizontal Alignment
		struc.yalign = TEXT_ALIGN_CENTER -- Vertical Alignment
		draw.Text( struc )
	end

	local GUI_Glow3 = vgui.Create("DButton", GUI_GlowMenu)
	GUI_Glow3:SetSize(400, 40)
	GUI_Glow3:SetPos(10,120)
	GUI_Glow3:SetText("")
	GUI_Glow3.DoClick = function()
		GUI_GlowMenu:Remove()
		net.Start("VEHUPGRADE_BuyUnderglow")
			net.WriteDouble(3)
		net.SendToServer()
	end
	GUI_Glow3.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 200 -- x pos
		struc.pos[2] = 20 -- y pos
		struc.color = Color(255,255,255,255) -- Red
		struc.text = "Red" -- Text
		struc.font = "TargetID" -- Font
		struc.xalign = TEXT_ALIGN_CENTER-- Horizontal Alignment
		struc.yalign = TEXT_ALIGN_CENTER -- Vertical Alignment
		draw.Text( struc )
	end

	local GUI_Glow4 = vgui.Create("DButton", GUI_GlowMenu)
	GUI_Glow4:SetSize(400, 40)
	GUI_Glow4:SetPos(10,165)
	GUI_Glow4:SetText("")
	GUI_Glow4.DoClick = function()
		GUI_GlowMenu:Remove()
		net.Start("VEHUPGRADE_BuyUnderglow")
			net.WriteDouble(4)
		net.SendToServer()
	end
	GUI_Glow4.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 200 -- x pos
		struc.pos[2] = 20 -- y pos
		struc.color = Color(255,255,255,255) -- Red
		struc.text = "Very Light Blue" -- Text
		struc.font = "TargetID" -- Font
		struc.xalign = TEXT_ALIGN_CENTER-- Horizontal Alignment
		struc.yalign = TEXT_ALIGN_CENTER -- Vertical Alignment
		draw.Text( struc )
	end

	local GUI_Glow5 = vgui.Create("DButton", GUI_GlowMenu)
	GUI_Glow5:SetSize(400, 40)
	GUI_Glow5:SetPos(10,210)
	GUI_Glow5:SetText("")
	GUI_Glow5.DoClick = function()
		GUI_GlowMenu:Remove()
		net.Start("VEHUPGRADE_BuyUnderglow")
			net.WriteDouble(5)
		net.SendToServer()
	end
	GUI_Glow5.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 200 -- x pos
		struc.pos[2] = 20 -- y pos
		struc.color = Color(255,255,255,255) -- Red
		struc.text = "Purple" -- Text
		struc.font = "TargetID" -- Font
		struc.xalign = TEXT_ALIGN_CENTER-- Horizontal Alignment
		struc.yalign = TEXT_ALIGN_CENTER -- Vertical Alignment
		draw.Text( struc )
	end
end