surface.CreateFont("UpgradesTop", {
	font = "Tahoma", 
	size = 16, 
	weight = 600
})

surface.CreateFont("Trebuchet20", {
	font = "Trebuchet MS", 
	size = 18, 
	weight = 900
})

surface.CreateFont("UiBold", {
	font = "Tahoma", 
	size = 15, 
	weight = 600
})

surface.CreateFont("HornMenuFont", {
    font = "coolvetica", 
    size = 20, 
    weight = 400
})