function VEHUPGRADE_CarUpgrades()
	local GUI_UpgradeMenu = vgui.Create("DFrame")
	GUI_UpgradeMenu:SetTitle("")
	GUI_UpgradeMenu:SetSize(460, 360)
	GUI_UpgradeMenu:Center()
	GUI_UpgradeMenu.Paint = function(CHPaint)
		-- Draw the menu background color.
		draw.RoundedBox( 6, 0, 0, CHPaint:GetWide(), CHPaint:GetTall(), Color( 255, 255, 255, 150 ) )

		-- Draw the outline of the menu.
		surface.SetDrawColor(0,0,0,255)
		surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), CHPaint:GetTall())

		surface.SetDrawColor(0,0,0,255)
		surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), 25)

		-- Draw the top title.
		draw.SimpleText("Vehicle Upgrade Central", "UpgradesTop", 92,12.5, Color(70,70,70,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	GUI_UpgradeMenu:MakePopup()
	GUI_UpgradeMenu:ShowCloseButton(false)

	local GUI_Main_Exit = vgui.Create("DButton", GUI_UpgradeMenu)
	GUI_Main_Exit:SetSize(16,16)
	GUI_Main_Exit:SetPos(400,5)
	GUI_Main_Exit:SetText("")
	GUI_Main_Exit.Paint = function()
		surface.SetMaterial(Material("icon16/cross.png"))
		surface.SetDrawColor(200,200,0,200)
		surface.DrawTexturedRect(0,0,GUI_Main_Exit:GetWide(),GUI_Main_Exit:GetTall())
	end
	GUI_Main_Exit.DoClick = function()
		GUI_UpgradeMenu:Remove()
	end

	local GUI_Upgrade1 = vgui.Create("DButton", GUI_UpgradeMenu)
	GUI_Upgrade1:SetSize(400, 40)
	GUI_Upgrade1:SetPos(10,30)
	GUI_Upgrade1:SetText("")
	GUI_Upgrade1.DoClick = function()
		GUI_UpgradeMenu:Remove()
		net.Start("VEHUPGRADE_BuyHydraulics")
		net.SendToServer()
	end
	GUI_Upgrade1.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 200
		struc.pos[2] = 20
		struc.color = Color(255,255,255,255)
		struc.text = "Purchase ".. VehicleUpgrades["HYDRAULICS"].Name .." ($".. util.UpgradesFormatNumber(VehicleUpgrades["HYDRAULICS"].Price) ..")"
		struc.font = "TargetID"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	local GUI_Upgrade2 = vgui.Create("DButton", GUI_UpgradeMenu)
	GUI_Upgrade2:SetSize(400, 40)
	GUI_Upgrade2:SetPos(10,75)
	GUI_Upgrade2:SetText("")
	GUI_Upgrade2.DoClick = function()
		GUI_UpgradeMenu:Remove()
		net.Start("VEHUPGRADE_BuyNitrous")
		net.SendToServer()
	end
	GUI_Upgrade2.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 200
		struc.pos[2] = 20
		struc.color = Color(255,255,255,255)
		struc.text = "Purchase ".. VehicleUpgrades["NITROUS"].Name .." ($".. util.UpgradesFormatNumber(VehicleUpgrades["NITROUS"].Price) ..")"
		struc.font = "TargetID"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	local GUI_Upgrade3 = vgui.Create("DButton", GUI_UpgradeMenu)
	GUI_Upgrade3:SetSize(400, 40)
	GUI_Upgrade3:SetPos(10,120)
	GUI_Upgrade3:SetText("")
	GUI_Upgrade3.DoClick = function()
		GUI_UpgradeMenu:Remove()
		net.Start("VEHUPGRADE_BuyArmor")
		net.SendToServer()
	end
	GUI_Upgrade3.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 200
		struc.pos[2] = 20
		struc.color = Color(255,255,255,255)
		struc.text = "Purchase ".. VehicleUpgrades["ARMOR"].Name .." ($".. util.UpgradesFormatNumber(VehicleUpgrades["ARMOR"].Price) ..")"
		struc.font = "TargetID"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	local GUI_Upgrade5 = vgui.Create("DButton", GUI_UpgradeMenu)
	GUI_Upgrade5:SetSize(400, 40)
	GUI_Upgrade5:SetPos(10,165)
	GUI_Upgrade5:SetText("")
	GUI_Upgrade5.DoClick = function()
		GUI_UpgradeMenu:Remove()
		VEHUPGRADE_UnderglowMenu()
	end
	GUI_Upgrade5.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 200
		struc.pos[2] = 20
		struc.color = Color(255,255,255,255)
		struc.text = "Purchase ".. VehicleUpgrades["UNDERGLOW"].Name .." ($".. util.UpgradesFormatNumber(VehicleUpgrades["UNDERGLOW"].Price) ..")"
		struc.font = "TargetID"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	local GUI_Upgrade6 = vgui.Create("DButton", GUI_UpgradeMenu)
	GUI_Upgrade6:SetSize(400, 40)
	GUI_Upgrade6:SetPos(10,210)
	GUI_Upgrade6:SetText("")
	GUI_Upgrade6.DoClick = function()
		GUI_UpgradeMenu:Remove()
		VEHUPGRADE_HornMenu()
	end
	GUI_Upgrade6.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 200
		struc.pos[2] = 20
		struc.color = Color(255,255,255,255)
		struc.text = "Purchase ".. VehicleUpgrades["HORN"].Name .." ($".. util.UpgradesFormatNumber(VehicleUpgrades["HORN"].Price) ..")"
		struc.font = "TargetID"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
    end

    local GUI_Upgrade7 = vgui.Create("DButton", GUI_UpgradeMenu)
    GUI_Upgrade7:SetSize(400, 40)
    GUI_Upgrade7:SetPos(10,255)
    GUI_Upgrade7:SetText("")
    GUI_Upgrade7.DoClick = function()
        GUI_UpgradeMenu:Remove()
        net.Start("VEHUPGRADE_BuyALARM")
        net.SendToServer()
    end
    GUI_Upgrade7.Paint = function(CHPaint)
        draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

        local struc = {}
        struc.pos = {}
        struc.pos[1] = 200
        struc.pos[2] = 20
        struc.color = Color(255,255,255,255)
        struc.text = "Purchase ".. VehicleUpgrades["ALARM"].Name .." ($".. util.UpgradesFormatNumber(VehicleUpgrades["ALARM"].Price) ..")"
        struc.font = "TargetID"
        struc.xalign = TEXT_ALIGN_CENTER
        struc.yalign = TEXT_ALIGN_CENTER
        draw.Text( struc )
    end
end
usermessage.Hook("VEHUPGRADE_CarUpgrades", VEHUPGRADE_CarUpgrades)