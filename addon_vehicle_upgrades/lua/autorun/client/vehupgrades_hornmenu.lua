function VEHUPGRADE_HornMenu()
	local GUI_HornMenu = vgui.Create("DFrame")
	GUI_HornMenu:SetTitle("")
	GUI_HornMenu:SetSize(420, 360)
	GUI_HornMenu:Center()
	GUI_HornMenu.Paint = function(CHPaint)
		-- Draw the menu background color.
		draw.RoundedBox( 6, 0, 0, CHPaint:GetWide(), CHPaint:GetTall(), Color( 255, 255, 255, 150 ) )

		-- Draw the outline of the menu.
		surface.SetDrawColor(0,0,0,255)
		surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), CHPaint:GetTall())

		surface.SetDrawColor(0,0,0,255)
		surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), 25)

		-- Draw the top title.
		draw.SimpleText("Upgrade Central - Horn Store", "UpgradesTop", 105,12.5, Color(70,70,70,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	GUI_HornMenu:MakePopup()
	GUI_HornMenu:ShowCloseButton(false)

	local GUI_Main_Exit = vgui.Create("DButton", GUI_HornMenu)
	GUI_Main_Exit:SetSize(16,16)
	GUI_Main_Exit:SetPos(400,5)
	GUI_Main_Exit:SetText("")
	GUI_Main_Exit.Paint = function()
		surface.SetMaterial(Material("icon16/cross.png"))
		surface.SetDrawColor(200,200,0,200)
		surface.DrawTexturedRect(0,0,GUI_Main_Exit:GetWide(),GUI_Main_Exit:GetTall())
	end
	GUI_Main_Exit.DoClick = function()
		GUI_HornMenu:Remove()
	end
	
	local GUI_Back = vgui.Create("DButton", GUI_HornMenu)
	GUI_Back:SetSize(16,16)
	GUI_Back:SetPos(380,5)
	GUI_Back:SetText("")
	GUI_Back.Paint = function()
		surface.SetMaterial(Material("icon16/arrow_left.png"))
		surface.SetDrawColor(200,200,0,200)
		surface.DrawTexturedRect(0,0,GUI_Back:GetWide(),GUI_Back:GetTall())
	end
	GUI_Back.DoClick = function()
		GUI_HornMenu:Remove()
		VEHUPGRADE_CarUpgrades()
	end
	
	local HornBG1 = vgui.Create( "DPanel", GUI_HornMenu )
	HornBG1:SetPos( 10, 30 )
	HornBG1:SetSize( 400, 50 )
	HornBG1.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(20,20,20,80))
	end

	local GUI_HornText = vgui.Create( "DLabel", HornBG1 )
	GUI_HornText:SetText( "Regular Double Horn" )
	GUI_HornText:SetPos( 136, 3  )
	GUI_HornText:SetFont( "HornMenuFont" )
	GUI_HornText:SetTextColor( Color( 255, 255, 255, 255 ) )
	GUI_HornText:SizeToContents()

	local GUI_HornBuy = vgui.Create("DButton", HornBG1)
	GUI_HornBuy:SetSize(110,20)
	GUI_HornBuy:SetPos(90,25)
	GUI_HornBuy:SetText("")
	GUI_HornBuy.DoClick = function()
		GUI_HornMenu:Remove()
		net.Start("VEHUPGRADE_BuyHorn")
			net.WriteDouble(1)
		net.SendToServer()
	end
	GUI_HornBuy.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 55
		struc.pos[2] = 10
		struc.color = Color(255,255,255,255)
		struc.text = "Purchase Horn"
		struc.font = "UiBold"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	local GUI_HornListen = vgui.Create("DButton", HornBG1)
	GUI_HornListen:SetSize(110,20)
	GUI_HornListen:SetPos(200,25)
	GUI_HornListen:SetText("")
	GUI_HornListen.DoClick = function()
		LocalPlayer():EmitSound("craphead_scripts/vehicle_upgrades/horns/horn_double.wav")
	end
	GUI_HornListen.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 55
		struc.pos[2] = 10
		struc.color = Color(255,255,255,255)
		struc.text = "Listen To Horn"
		struc.font = "UiBold"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	-- Horn Nr. 2
	local HornBG2 = vgui.Create( "DPanel", GUI_HornMenu )
	HornBG2:SetPos( 10, 85 )
	HornBG2:SetSize( 400, 50 )
	HornBG2.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(20,20,20,80))
	end

	local GUI_Horn2Text = vgui.Create( "DLabel", HornBG2 )
	GUI_Horn2Text:SetText( "Trucker Horn" )
	GUI_Horn2Text:SetPos( 154, 3  )
	GUI_Horn2Text:SetFont( "HornMenuFont" )
	GUI_Horn2Text:SetTextColor( Color( 255, 255, 255, 255 ) )
	GUI_Horn2Text:SizeToContents()

	local GUI_Horn2Buy = vgui.Create("DButton", HornBG2)
	GUI_Horn2Buy:SetSize(110,20)
	GUI_Horn2Buy:SetPos(90,25)
	GUI_Horn2Buy:SetText("")
	GUI_Horn2Buy.DoClick = function()
		GUI_HornMenu:Remove()
		net.Start("VEHUPGRADE_BuyHorn")
			net.WriteDouble(2)
		net.SendToServer()
	end
	GUI_Horn2Buy.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 55
		struc.pos[2] = 10
		struc.color = Color(255,255,255,255)
		struc.text = "Purchase Horn"
		struc.font = "UiBold"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	local GUI_Horn2Listen = vgui.Create("DButton", HornBG2)
	GUI_Horn2Listen:SetSize(110,20)
	GUI_Horn2Listen:SetPos(200,25)
	GUI_Horn2Listen:SetText("")
	GUI_Horn2Listen.DoClick = function()
		LocalPlayer():EmitSound("craphead_scripts/vehicle_upgrades/horns/firetruck_horn.mp3")
	end
	GUI_Horn2Listen.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 55
		struc.pos[2] = 10
		struc.color = Color(255,255,255,255)
		struc.text = "Listen To Horn"
		struc.font = "UiBold"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	-- Horn Nr. 3
	local HornBG3 = vgui.Create( "DPanel", GUI_HornMenu )
	HornBG3:SetPos( 10, 140 )
	HornBG3:SetSize( 400, 50 )
	HornBG3.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(20,20,20,80))
	end

	local GUI_HornText = vgui.Create( "DLabel", HornBG3 )
	GUI_HornText:SetText( "Fancy Dixie Horn" )
	GUI_HornText:SetPos( 142, 3  )
	GUI_HornText:SetFont( "HornMenuFont" )
	GUI_HornText:SetTextColor( Color( 255, 255, 255, 255 ) )
	GUI_HornText:SizeToContents()

	local GUI_HornBuy = vgui.Create("DButton", HornBG3)
	GUI_HornBuy:SetSize(110,20)
	GUI_HornBuy:SetPos(90,25)
	GUI_HornBuy:SetText("")
	GUI_HornBuy.DoClick = function()
		GUI_HornMenu:Remove()
		net.Start("VEHUPGRADE_BuyHorn")
			net.WriteDouble(3)
		net.SendToServer()
	end
	GUI_HornBuy.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 55
		struc.pos[2] = 10
		struc.color = Color(255,255,255,255)
		struc.text = "Purchase Horn"
		struc.font = "UiBold"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	local GUI_HornListen = vgui.Create("DButton", HornBG3)
	GUI_HornListen:SetSize(110,20)
	GUI_HornListen:SetPos(200,25)
	GUI_HornListen:SetText("")
	GUI_HornListen.DoClick = function()
		LocalPlayer():EmitSound("craphead_scripts/vehicle_upgrades/horns/horn_dixie.mp3")
	end
	GUI_HornListen.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 55
		struc.pos[2] = 10
		struc.color = Color(255,255,255,255)
		struc.text = "Listen To Horn"
		struc.font = "UiBold"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	-- Horn Nr. 4
	local HornBG4 = vgui.Create( "DPanel", GUI_HornMenu )
	HornBG4:SetPos( 10, 195 )
	HornBG4:SetSize( 400, 50 )
	HornBG4.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(20,20,20,80))
	end

	local GUI_HornText = vgui.Create( "DLabel", HornBG4 )
	GUI_HornText:SetText( "Powerful Double Horn" )
	GUI_HornText:SetPos( 130, 3  )
	GUI_HornText:SetFont( "HornMenuFont" )
	GUI_HornText:SetTextColor( Color( 255, 255, 255, 255 ) )
	GUI_HornText:SizeToContents()

	local GUI_HornBuy = vgui.Create("DButton", HornBG4)
	GUI_HornBuy:SetSize(110,20)
	GUI_HornBuy:SetPos(90,25)
	GUI_HornBuy:SetText("")
	GUI_HornBuy.DoClick = function()
		GUI_HornMenu:Remove()
		net.Start("VEHUPGRADE_BuyHorn")
			net.WriteDouble(4)
		net.SendToServer()
	end
	GUI_HornBuy.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 55
		struc.pos[2] = 10
		struc.color = Color(255,255,255,255)
		struc.text = "Purchase Horn"
		struc.font = "UiBold"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	local GUI_HornListen = vgui.Create("DButton", HornBG4)
	GUI_HornListen:SetSize(110,20)
	GUI_HornListen:SetPos(200,25)
	GUI_HornListen:SetText("")
	GUI_HornListen.DoClick = function()
		LocalPlayer():EmitSound("craphead_scripts/vehicle_upgrades/horns/horn_powerful_double.mp3")
	end
	GUI_HornListen.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 55
		struc.pos[2] = 10
		struc.color = Color(255,255,255,255)
		struc.text = "Listen To Horn"
		struc.font = "UiBold"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	-- Horn Nr. 5
	local HornBG5 = vgui.Create( "DPanel", GUI_HornMenu )
	HornBG5:SetPos( 10, 250 )
	HornBG5:SetSize( 400, 50 )
	HornBG5.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(20,20,20,80))
	end

	local GUI_HornText = vgui.Create( "DLabel", HornBG5 )
	GUI_HornText:SetText( "Stupid Clown Horn" )
	GUI_HornText:SetPos( 137, 3  )
	GUI_HornText:SetFont( "HornMenuFont" )
	GUI_HornText:SetTextColor( Color( 255, 255, 255, 255 ) )
	GUI_HornText:SizeToContents()

	local GUI_HornBuy = vgui.Create("DButton", HornBG5)
	GUI_HornBuy:SetSize(110,20)
	GUI_HornBuy:SetPos(90,25)
	GUI_HornBuy:SetText("")
	GUI_HornBuy.DoClick = function()
		GUI_HornMenu:Remove()
		net.Start("VEHUPGRADE_BuyHorn")
			net.WriteDouble(5)
		net.SendToServer()
	end
	GUI_HornBuy.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 55
		struc.pos[2] = 10
		struc.color = Color(255,255,255,255)
		struc.text = "Purchase Horn"
		struc.font = "UiBold"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	local GUI_HornListen = vgui.Create("DButton", HornBG5)
	GUI_HornListen:SetSize(110,20)
	GUI_HornListen:SetPos(200,25)
	GUI_HornListen:SetText("")
	GUI_HornListen.DoClick = function()
		LocalPlayer():EmitSound("craphead_scripts/vehicle_upgrades/horns/horn_clown.mp3")
	end
	GUI_HornListen.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 55
		struc.pos[2] = 10
		struc.color = Color(255,255,255,255)
		struc.text = "Listen To Horn"
		struc.font = "UiBold"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	-- Horn Nr. 6
	local HornBG6 = vgui.Create( "DPanel", GUI_HornMenu )
	HornBG6:SetPos( 10, 305 )
	HornBG6:SetSize( 400, 50 )
	HornBG6.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(20,20,20,80))
	end

	local GUI_HornText = vgui.Create( "DLabel", HornBG6 )
	GUI_HornText:SetText( "Single Honk Horn" )
	GUI_HornText:SetPos( 140, 3  )
	GUI_HornText:SetFont( "HornMenuFont" )
	GUI_HornText:SetTextColor( Color( 255, 255, 255, 255 ) )
	GUI_HornText:SizeToContents()

	local GUI_HornBuy = vgui.Create("DButton", HornBG6)
	GUI_HornBuy:SetSize(110,20)
	GUI_HornBuy:SetPos(90,25)
	GUI_HornBuy:SetText("")
	GUI_HornBuy.DoClick = function()
		GUI_HornMenu:Remove()
		net.Start("VEHUPGRADE_BuyHorn")
			net.WriteDouble(6)
		net.SendToServer()
	end
	GUI_HornBuy.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 55
		struc.pos[2] = 10
		struc.color = Color(255,255,255,255)
		struc.text = "Purchase Horn"
		struc.font = "UiBold"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end

	local GUI_HornListen = vgui.Create("DButton", HornBG6)
	GUI_HornListen:SetSize(110,20)
	GUI_HornListen:SetPos(200,25)
	GUI_HornListen:SetText("")
	GUI_HornListen.DoClick = function()
		LocalPlayer():EmitSound("craphead_scripts/vehicle_upgrades/horns/horn_single.wav")
	end
	GUI_HornListen.Paint = function(CHPaint)
		draw.RoundedBox(8,1,1,CHPaint:GetWide()-2,CHPaint:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 55
		struc.pos[2] = 10
		struc.color = Color(255,255,255,255)
		struc.text = "Listen To Horn"
		struc.font = "UiBold"
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end
end