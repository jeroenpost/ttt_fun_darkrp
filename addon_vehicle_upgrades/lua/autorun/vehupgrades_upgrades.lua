VehicleUpgrades = {}

VehicleUpgrades["HYDRAULICS"] = {
	Name = "Hydraulics",
	Price = 20000,
	AllowedULXRanks = {
		"user",
		"admin",
		"superadmin",
		"owner",
	},
}

VehicleUpgrades["NITROUS"] = {
	Name = "Nitrous",
	Price = 18000,
	AllowedULXRanks = {
		"user",
		"admin",
		"superadmin",
		"owner",
	},
}

VehicleUpgrades["ARMOR"] = {
	Name = "Armor",
	Price = 12000,
	AllowedULXRanks = {
		"user",
		"admin",
		"superadmin",
		"owner",
	},
}

VehicleUpgrades["UNDERGLOW"] = {
	Name = "Underglow",
	Price = 13000,
	AllowedULXRanks = {
		"user",
		"admin",
		"superadmin",
		"owner",
	},
}

VehicleUpgrades["HORN"] = {
	Name = "Horn",
	Price = 11000,
	AllowedULXRanks = {
		"user",
		"admin",
		"superadmin",
		"owner",
	},
}

VehicleUpgrades["ALARM"] = {
    Name = "Alarm System",
    Price = 50000,
    AllowedULXRanks = {
        "user",
        "admin",
        "superadmin",
        "owner",
    },
}