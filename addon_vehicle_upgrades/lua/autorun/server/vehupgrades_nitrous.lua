function VEHUPGRADE_DoNitrous( ply, key )
	if ply:InVehicle() and ply:GetVehicle():GetClass() == "prop_vehicle_jeep" then
		if key == IN_ATTACK then
			ply.NitroTime = ply.NitroTime or 0
			if ply.NitroTime + 50 > CurTime() then
				return
			end
			ply.NitroTime = CurTime()
			
			if not ply:GetVehicle().CanBeUpgraded then
				return
			end
			
			if tobool(ply:GetVehicle().Nitrous) then
                local power = UpgradedVehicles[ply:GetVehicle():GetModel()]
                if power then
                    power = power.NitroPower
                else
                    power = 2200000
                end
				local NitroForce = ply:GetVehicle():GetForward() * power
				
				if NitroForce then
					ply:GetVehicle():GetPhysicsObject():ApplyForceCenter( NitroForce )
					ply:GetVehicle():EmitSound( "craphead_scripts/vehicle_upgrades/nitrous.mp3" )
				end
			end
		end
	end		
end
hook.Add("KeyPress", "VEHUPGRADE_DoNitrous", VEHUPGRADE_DoNitrous)

util.AddNetworkString("VEHUPGRADE_BuyNitrous")
net.Receive("VEHUPGRADE_BuyNitrous", function(length, ply)

    local Vehicle
    for k, v in pairs(ents.FindInSphere(ply:GetPos(),VEHUPGRADE_VehicleToNPCDistance)) do
        --local doorData = v:getDoorData()
        if v.VehicleTable and v.OwnerID == ply:SteamID() then
            Vehicle = v
            break
        end
    end

	
	if not IsValid(Vehicle) then
        DarkRP.notify(ply, 1, 5,  "Make sure your car is close the the car tuner!")
		return 
	end
	
	if ply:GetPos():Distance(Vehicle:GetPos()) > VEHUPGRADE_VehicleToNPCDistance then
		DarkRP.notify(ply, 1, 5,  "You need to bring your vehicle closer to the NPC!")
		return
	end
	
	if not Vehicle.CanBeUpgraded then
		DarkRP.notify(ply, 1, 5,  "This vehicle cannot be upgraded!")
		return
	end
	
	if VEHUPGRADE_UseULXRankRestrictions then
		if not table.HasValue( VehicleUpgrades["NITROUS"].AllowedULXRanks, ply:GetUserGroup() ) then
			DarkRP.notify(ply, 1, 5, "You are not the required ulx rank!")
			return
		end
	end
	
	if tobool(Vehicle.Nitrous) then 
		DarkRP.notify(ply, 1, 5, "You already own nitrous for this vehicle!" ) 
		return
	end
	
	if ply:getDarkRPVar("money") < VehicleUpgrades["NITROUS"].Price then
		DarkRP.notify(ply, 1, 5, "It costs $".. util.UpgradesFormatNumber(VehicleUpgrades["NITROUS"].Price) .." to buy nitrous for this car." )
		return
	end
	
	ply:addMoney( VehicleUpgrades["NITROUS"].Price * -1)
	DarkRP.notify(ply, 1, 5, "Succesfully purchased nitrous for your vehicle." )
	
	Vehicle.Nitrous = true
	file.Write("craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(ply:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(Vehicle:GetModel())) .."/upgrade_nitrous.txt", "true", "DATA")
end)