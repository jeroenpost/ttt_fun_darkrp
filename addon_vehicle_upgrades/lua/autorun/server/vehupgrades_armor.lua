util.AddNetworkString("VEHUPGRADE_BuyArmor")
net.Receive("VEHUPGRADE_BuyArmor", function(length, ply)

    local Vehicle
    for k, v in pairs(ents.FindInSphere(ply:GetPos(),VEHUPGRADE_VehicleToNPCDistance)) do
        --local doorData = v:getDoorData()
        if v.VehicleTable and v.OwnerID == ply:SteamID() then
            Vehicle = v
            break
        end
    end


    if not IsValid(Vehicle) then
        DarkRP.notify(ply, 1, 5,  "Make sure your car is close the the car tuner!")
        return
    end
	
	if ply:GetPos():Distance(Vehicle:GetPos()) > VEHUPGRADE_VehicleToNPCDistance then
		DarkRP.notify(ply, 1, 5,  "You need to bring your vehicle closer to the NPC!")
		return
	end
	
	if not Vehicle.CanBeUpgraded then
		DarkRP.notify(ply, 1, 5,  "This vehicle cannot be upgraded!")
		return
	end
	
	if VEHUPGRADE_UseULXRankRestrictions then
		if not table.HasValue( VehicleUpgrades["ARMOR"].AllowedULXRanks, ply:GetUserGroup() ) then
			DarkRP.notify(ply, 1, 5, "You are not the required ulx rank!")
			return
		end
	end
	
	if tobool(Vehicle.Armor) then 
		DarkRP.notify(ply, 1, 5, "You already own armor for this vehicle!" ) 
		return
	end
	
	if ply:getDarkRPVar("money") < VehicleUpgrades["ARMOR"].Price then
		DarkRP.notify(ply, 1, 5, "It costs $".. util.UpgradesFormatNumber(VehicleUpgrades["ARMOR"].Price) .." to buy armor for this car." )
		return
	end
	
	ply:addMoney( VehicleUpgrades["ARMOR"].Price * -1)
	DarkRP.notify(ply, 1, 5, "Succesfully purchased armor for your vehicle." )
	
	Vehicle.Armor = true
	Vehicle:SetHealth( Vehicle:Health() + VEHUPGRADE_AmountOfArmor )
	file.Write("craphead_scripts/vehicle_upgrades/".. string.lower(string.lower(string.gsub(ply:SteamID(), ":", "_"))) .."/".. string.GetFileFromFilename(string.StripExtension(Vehicle:GetModel())) .."/upgrade_armor.txt", "true", "DATA")
end)