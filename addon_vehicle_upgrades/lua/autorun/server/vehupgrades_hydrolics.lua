function VEHUPGRADE_DoHydrolics( ply, key )
	if ply:InVehicle() and ply:GetVehicle():GetClass() == "prop_vehicle_jeep" then
		if key == IN_WALK then
			ply.HydroTime = ply.HydroTime or 0
			if ply.HydroTime + 1 > CurTime() then
				return
			end
			ply.HydroTime = CurTime()
			
			if not ply:GetVehicle().CanBeUpgraded then
				return
			end
			
			if tobool(ply:GetVehicle().Hydraulics) then
                local power = UpgradedVehicles[ply:GetVehicle():GetModel()]
                if power then
                    power = power.HydroPower
                else
                    power = 250000
                end
				local HydroForce = ply:GetVehicle():GetUp() * power
				
				if HydroForce then
					ply:GetVehicle():GetPhysicsObject():ApplyForceCenter( HydroForce )
				end
			end
		end
	end		
end
hook.Add("KeyPress", "VEHUPGRADE_DoHydrolics", VEHUPGRADE_DoHydrolics)

util.AddNetworkString("VEHUPGRADE_BuyHydraulics")
net.Receive("VEHUPGRADE_BuyHydraulics", function(length, ply)

    local Vehicle
    for k, v in pairs(ents.FindInSphere(ply:GetPos(),VEHUPGRADE_VehicleToNPCDistance)) do
        --local doorData = v:getDoorData()
        if v.VehicleTable and v.OwnerID == ply:SteamID() then
            Vehicle = v
            break
        end
    end

    if not IsValid(Vehicle) then
        DarkRP.notify(ply, 1, 5,  "Make sure your car is close the the car tuner!")
        return
    end
	
	if ply:GetPos():Distance(Vehicle:GetPos()) > VEHUPGRADE_VehicleToNPCDistance then
		DarkRP.notify(ply, 1, 5,  "You need to bring your vehicle closer to the NPC!")
		return
	end
	
	if not Vehicle.CanBeUpgraded then
		DarkRP.notify(ply, 1, 5,  "This vehicle cannot be upgraded!")
		return
	end
	
	if VEHUPGRADE_UseULXRankRestrictions then
		if not table.HasValue( VehicleUpgrades["HYDRAULICS"].AllowedULXRanks, ply:GetUserGroup() ) then
			DarkRP.notify(ply, 1, 5, "You are not the required ulx rank!")
			return
		end
	end
	
	if tobool(Vehicle.Hydraulics) then 
		DarkRP.notify(ply, 1, 5, "You already own hydraulics for this vehicle!" ) 
		return
	end
	
	if ply:getDarkRPVar("money") < VehicleUpgrades["HYDRAULICS"].Price then
		DarkRP.notify(ply, 1, 5, "It costs $".. util.UpgradesFormatNumber(VehicleUpgrades["HYDRAULICS"].Price) .." to buy hydraulics for this car." )
		return
	end
	
	ply:addMoney( VehicleUpgrades["HYDRAULICS"].Price * -1)
	DarkRP.notify(ply, 1, 5, "Succesfully purchased hydraulics for your vehicle." )
	
	Vehicle.Hydraulics = true
	file.Write("craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(ply:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(Vehicle:GetModel())) .."/upgrade_hydraulics.txt", "true", "DATA")
end)