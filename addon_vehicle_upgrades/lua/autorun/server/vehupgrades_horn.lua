function VEHUPGRADE_DoHorn( ply, key )
	if ply:InVehicle() and ply:GetVehicle():GetClass() == "prop_vehicle_jeep" then
		if key == IN_SPEED then
			ply.HornTime = ply.HornTime or 0
			if ply.HornTime + 3 > CurTime() then
				return
			end
			ply.HornTime = CurTime()
			
			if not ply:GetVehicle().CanBeUpgraded then
				return
            end
            if not ply:GetVehicle().Horn then ply:GetVehicle().Horn = 0 end
			
			if tonumber(ply:GetVehicle().Horn) > 0 then
				if tonumber(ply:GetVehicle().Horn) == 1 then -- Regular Double Horn
					ply:GetVehicle():EmitSound( "craphead_scripts/vehicle_upgrades/horns/horn_double.mp3" )
				elseif tonumber(ply:GetVehicle().Horn) == 2 then -- Trucker Horn
					ply:GetVehicle():EmitSound( "craphead_scripts/vehicle_upgrades/horns/firetruck_horn.mp3" )
				elseif tonumber(ply:GetVehicle().Horn) == 3 then -- Fancy Dixie Horn
					ply:GetVehicle():EmitSound( "craphead_scripts/vehicle_upgrades/horns/horn_dixie.mp3" )
				elseif tonumber(ply:GetVehicle().Horn) == 4 then -- Powerful Double Horn
					ply:GetVehicle():EmitSound( "craphead_scripts/vehicle_upgrades/horns/horn_powerful_double.mp3" )
				elseif tonumber(ply:GetVehicle().Horn) == 5 then -- Stupid Clown Horn
					ply:GetVehicle():EmitSound( "craphead_scripts/vehicle_upgrades/horns/horn_clown.mp3" )
				elseif tonumber(ply:GetVehicle().Horn) == 6 then -- Single Honk Horn
					ply:GetVehicle():EmitSound( "craphead_scripts/vehicle_upgrades/horns/horn_single.mp3" )
				end
			end
		end
	end		
end
hook.Add("KeyPress", "VEHUPGRADE_DoHorn", VEHUPGRADE_DoHorn)

util.AddNetworkString("VEHUPGRADE_BuyHorn")
net.Receive("VEHUPGRADE_BuyHorn", function(length, ply)
	local HornNumber = tonumber(net.ReadDouble())

    local Vehicle
    for k, v in pairs(ents.FindInSphere(ply:GetPos(),VEHUPGRADE_VehicleToNPCDistance)) do
        --local doorData = v:getDoorData()
        if v.VehicleTable and v.OwnerID == ply:SteamID() then
            Vehicle = v
            break
        end
    end


    if not IsValid(Vehicle) then
        DarkRP.notify(ply, 1, 5,  "Make sure your car is close the the car tuner!")
        return
    end
	
	if ply:GetPos():Distance(Vehicle:GetPos()) > VEHUPGRADE_VehicleToNPCDistance then
		DarkRP.notify(ply, 1, 5,  "You need to bring your vehicle closer to the NPC!")
		return
	end
	
	if not Vehicle.CanBeUpgraded then
		DarkRP.notify(ply, 1, 5,  "This vehicle cannot be upgraded!")
		return
	end
	
	if VEHUPGRADE_UseULXRankRestrictions then
		if not table.HasValue( VehicleUpgrades["HORN"].AllowedULXRanks, ply:GetUserGroup() ) then
			DarkRP.notify(ply, 1, 5, "You are not the required ulx rank!")
			return
		end
	end
	
	if tonumber(Vehicle.Horn) == HornNumber then 
		DarkRP.notify(ply, 1, 5, "You already own this horn for this vehicle!" ) 
		return
	end
	
	if ply:getDarkRPVar("money") < VehicleUpgrades["HORN"].Price then
		DarkRP.notify(ply, 1, 5, "It costs $".. util.UpgradesFormatNumber(VehicleUpgrades["HORN"].Price) .." to buy a horn for this car." )
		return
	end
	
	ply:addMoney( VehicleUpgrades["HORN"].Price * -1)
	DarkRP.notify(ply, 1, 5, "Succesfully purchased horn for your vehicle." )
	
	Vehicle.Horn = HornNumber
	file.Write("craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(ply:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(Vehicle:GetModel())) .."/upgrade_horn.txt", HornNumber, "DATA")
end)