function VEHUPGRADE_SpawnedVehicle( vehicle )
	
	if vehicle:GetClass() == "prop_vehicle_jeep" then
	
		timer.Simple(1, function()
			--local doorData = vehicle:getDoorData()
			local owner = vehicle.Owner --player.GetByID(doorData.owner)
			
			if IsValid(owner) then
				
				if table.HasValue( VEHUPGRADE_BannedCarModels, vehicle:GetModel() ) then
					vehicle.CanBeUpgraded = false
					return
				end
				
				if not file.IsDir("craphead_scripts/vehicle_upgrades/"..string.lower(string.gsub(owner:SteamID(), ":", "_")), "DATA") then
					file.CreateDir("craphead_scripts/vehicle_upgrades/"..string.lower(string.gsub(owner:SteamID(), ":", "_")), "DATA")
				end
				
				if not file.IsDir("craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())), "DATA") then
					file.CreateDir("craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())), "DATA")
				end
				
				-- Hydraulics
				if not file.Exists( "craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_hydraulics.txt", "DATA" ) then
					file.Write("craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_hydraulics.txt", "false", "DATA")
				end
				-- Nitrous
				if not file.Exists( "craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_nitrous.txt", "DATA" ) then
					file.Write("craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_nitrous.txt", "false", "DATA")
				end
				-- Armor
				if not file.Exists( "craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_armor.txt", "DATA" ) then
					file.Write("craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_armor.txt", "false", "DATA")
				end
				-- Underglow
				if not file.Exists( "craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_underglow.txt", "DATA" ) then
					file.Write("craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_underglow.txt", "0", "DATA")
				end
				-- Horn
				if not file.Exists( "craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_horn.txt", "DATA" ) then
					file.Write("craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_horn.txt", "0", "DATA")
                end
                -- Alarm
                if not file.Exists( "craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_alarm.txt", "DATA" ) then
                    file.Write("craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(owner:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_alarm.txt", false, "DATA")
                end
				
				vehicle.CanBeUpgraded = true
				vehicle.Hydraulics = file.Read("craphead_scripts/vehicle_upgrades/".. string.lower(string.lower(string.gsub(owner:SteamID(), ":", "_"))) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_hydraulics.txt", "DATA")
				vehicle.Nitrous = file.Read("craphead_scripts/vehicle_upgrades/".. string.lower(string.lower(string.gsub(owner:SteamID(), ":", "_"))) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_nitrous.txt", "DATA")
				vehicle.Armor = file.Read("craphead_scripts/vehicle_upgrades/".. string.lower(string.lower(string.gsub(owner:SteamID(), ":", "_"))) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_armor.txt", "DATA")
				vehicle.Underglow = file.Read("craphead_scripts/vehicle_upgrades/".. string.lower(string.lower(string.gsub(owner:SteamID(), ":", "_"))) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_underglow.txt", "DATA")
				vehicle.Horn = file.Read("craphead_scripts/vehicle_upgrades/".. string.lower(string.lower(string.gsub(owner:SteamID(), ":", "_"))) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_horn.txt", "DATA")
                vehicle.Alarm = file.Read("craphead_scripts/vehicle_upgrades/".. string.lower(string.lower(string.gsub(owner:SteamID(), ":", "_"))) .."/".. string.GetFileFromFilename(string.StripExtension(vehicle:GetModel())) .."/upgrade_alarm.txt", "DATA")

				-- Spawn Underglow Entity
				local UnderglowEntity = ents.Create("vehicle_underglow")
				UnderglowEntity:SetPos( vehicle:GetPos() + Vector(0,0,0) )
				UnderglowEntity:SetParent( vehicle )
				UnderglowEntity:Spawn()
				if vehicle.Armor then
					vehicle:SetHealth( vehicle:Health() + VEHUPGRADE_AmountOfArmor )
				end
			else
				vehicle.CanBeUpgraded = false
			end
		end)
	end
end
hook.Add( "OnEntityCreated", "VEHUPGRADE_SpawnedVehicle", VEHUPGRADE_SpawnedVehicle )

function VEHUPGRADE_UnderglowOff(ply, vehicle)
	if VEHUPGRADE_UnderglowOffOnExit then
		vehicle:SetNWBool( "GlowOn", false )
	end
end
hook.Add("PlayerLeaveVehicle", "VEHUPGRADE_UnderglowOff", VEHUPGRADE_UnderglowOff)