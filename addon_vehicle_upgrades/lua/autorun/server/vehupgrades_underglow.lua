function VEHUPGRADE_DoUnderglow( ply, key )
	if ply:InVehicle() and ply:GetVehicle():GetClass() == "prop_vehicle_jeep" then
		if key == IN_ATTACK2 then
			ply.GlowTime = ply.GlowTime or 0
			if ply.GlowTime + 1 > CurTime() then
				return
			end
			ply.GlowTime = CurTime()
			
			if not ply:GetVehicle().CanBeUpgraded then
				return
			end
			
			if ply:GetVehicle().Underglow and tonumber(ply:GetVehicle().Underglow) > 0 then
				if tonumber(ply:GetVehicle().Underglow) == 1 then // Blue
					ply:GetVehicle():SetNWString( "GlowColor", "Blue" )
					ply:GetVehicle():SetNWBool( "GlowOn", !ply:GetVehicle():GetNWBool("GlowOn") )
				elseif tonumber(ply:GetVehicle().Underglow) == 2 then // Green
					ply:GetVehicle():SetNWString( "GlowColor", "Green" )
					ply:GetVehicle():SetNWBool( "GlowOn", !ply:GetVehicle():GetNWBool("GlowOn") )
				elseif tonumber(ply:GetVehicle().Underglow) == 3 then // Red
					ply:GetVehicle():SetNWString( "GlowColor", "Red" )
					ply:GetVehicle():SetNWBool( "GlowOn", !ply:GetVehicle():GetNWBool("GlowOn") )
				elseif tonumber(ply:GetVehicle().Underglow) == 4 then // Very Light Blue
					ply:GetVehicle():SetNWString( "GlowColor", "VeryLightBlue" )
					ply:GetVehicle():SetNWBool( "GlowOn", !ply:GetVehicle():GetNWBool("GlowOn") )
				elseif tonumber(ply:GetVehicle().Underglow) == 5 then // Purple
					ply:GetVehicle():SetNWString( "GlowColor", "Purple" )
					ply:GetVehicle():SetNWBool( "GlowOn", !ply:GetVehicle():GetNWBool("GlowOn") )
				end
			end
		end
	end		
end
hook.Add("KeyPress", "VEHUPGRADE_DoUnderglow", VEHUPGRADE_DoUnderglow)

util.AddNetworkString("VEHUPGRADE_BuyUnderglow")
net.Receive("VEHUPGRADE_BuyUnderglow", function(length, ply)
	local UnderglowColor = tonumber(net.ReadDouble())

    local Vehicle
    for k, v in pairs(ents.FindInSphere(ply:GetPos(),VEHUPGRADE_VehicleToNPCDistance)) do
        --local doorData = v:getDoorData()
        if v.VehicleTable and v.OwnerID == ply:SteamID() then
            Vehicle = v
            break
        end
    end


    if not IsValid(Vehicle) then
        DarkRP.notify(ply, 1, 5,  "Make sure your car is close the the car tuner!")
        return
    end
	
	if ply:GetPos():Distance(Vehicle:GetPos()) > VEHUPGRADE_VehicleToNPCDistance then
		DarkRP.notify(ply, 1, 5,  "You need to bring your vehicle closer to the NPC!")
		return
	end
	
	if not Vehicle.CanBeUpgraded then
		DarkRP.notify(ply, 1, 5,  "This vehicle cannot be upgraded!")
		return
	end
	
	if VEHUPGRADE_UseULXRankRestrictions then
		if not table.HasValue( VehicleUpgrades["UNDERGLOW"].AllowedULXRanks, ply:GetUserGroup() ) then
			DarkRP.notify(ply, 1, 5, "You are not the required ulx rank!")
			return
		end
	end
	
	if tonumber(Vehicle.Underglow) == UnderglowColor then 
		DarkRP.notify(ply, 1, 5, "You already own this underglow color for this vehicle!" ) 
		return
	end
	
	if ply:getDarkRPVar("money") < VehicleUpgrades["UNDERGLOW"].Price then
		DarkRP.notify(ply, 1, 5, "It costs $".. util.UpgradesFormatNumber(VehicleUpgrades["UNDERGLOW"].Price) .." to buy underglow for this car." )
		return
	end
	
	ply:addMoney( VehicleUpgrades["UNDERGLOW"].Price * -1)
	DarkRP.notify(ply, 1, 5, "Succesfully purchased underglow for your vehicle." )
	
	Vehicle.Underglow = UnderglowColor
	file.Write("craphead_scripts/vehicle_upgrades/".. string.lower(string.gsub(ply:SteamID(), ":", "_")) .."/".. string.GetFileFromFilename(string.StripExtension(Vehicle:GetModel())) .."/upgrade_underglow.txt", UnderglowColor, "DATA")
end)