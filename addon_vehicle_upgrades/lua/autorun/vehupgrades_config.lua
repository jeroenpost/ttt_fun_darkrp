-- General Config
VEHUPGRADE_UseULXRankRestrictions = false -- Determains if ulx rank restriction should be on. You can set the rank restrictions to each upgrade a bit further down in this file! true/false [Default = false]
VEHUPGRADE_NPCModel = "models/odessa.mdl"
VEHUPGRADE_AmountOfArmor = 100 -- How much armor should be given with the armor upgrade? [Default = 100]
VEHUPGRADE_UnderglowOffOnExit = false -- Should underglow turn off when you exit your vehicle? true/false [Default = false]
VEHUPGRADE_VehicleToNPCDistance = 350 -- The distance between the vehicle and the NPC before it can be upgraded. [Default = 250]

VEHUPGRADE_BannedCarModels = { -- This is a list of vehicles that cannot be upgraded!
	"models/sickness/lcpddr.mdl", -- Sickness Police Vehicle
	"models/tdmcars/copcar.mdl", -- TDM Police Vehicle
	"models/sickness/meatwagon.mdl", -- Sickness Paramedic Vehicle
	"models/sickness/stockade2dr.mdl", -- Sickness SWAT Van
	"models/sickness/truckfire.mdl", -- Sickness Firetruck
	"models/sickness/towtruckdr.mdl", -- Sickness Towtruck
	"models/tdmcars/trailers/container.mdl", -- TDM Container
	"models/tdmcars/trailers/food_cistern.mdl", -- TDM Container
	"models/tdmcars/trailers/logtrailer_planks.mdl" -- TDM Container

	-- THE LAST LINE SHOULD NOT HAVE A COMMA AT THE END. BE AWARE OF THIS WHEN EDITING THIS!
}

-- DON'T WORRY ABOUT THIS!
function util.UpgradesFormatNumber( n )
    if (!n or type(n) != "number") then
        return 0
    end
	
    if n >= 1e14 then return tostring(n) end
    n = tostring(n)
    sep = sep or ","
    local dp = string.find(n, "%.") or #n+1
    for i=dp-4, 1, -3 do
        n = n:sub(1, i) .. sep .. n:sub(i+1)
    end
    return n
end