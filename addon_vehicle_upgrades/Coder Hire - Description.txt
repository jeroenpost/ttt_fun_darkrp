##DarkRP Support##
DarkRP 2.4.3 ![Alt text](http://s3-eu-west-1.amazonaws.com/coderhire-ratings/991eb9bb1af4b20ea9f8a654cf571a59.png)  
DarkRP 2.5.0 ![Alt text](http://s3-eu-west-1.amazonaws.com/coderhire-ratings/991eb9bb1af4b20ea9f8a654cf571a59.png)  

Other custom versions of DarkRP are not supported, and don't count on my support if you're using a custom version of DarkRP with modified core features.

##Description##
This addon adds a new way of upgrading vehicles. Instead of just changing the color of the vehicle, you can now upgrade the vehicle in different ways.  
With easy setup and LOTS of configuration this addon is perfect for any server with a car dealer.  

**NOTE: A car dealer (or some other way of buying vehicles) is required!**  
**The way vehicles are marked as being able to be upgraded is a check if they have an owner 1 second after being spawned.**  
**This means all car dealers will work, but spawning the vehicle from the Q menu will not work.**  

##Features##
- A large selection of upgrades (se below).  
- Lots of configuration making everything different from server to server.  
- Ability to restrict upgrades to specific ulx ranks (see VEHUPGRADE_UseULXRankRestrictions in vehupgrades_config.lua).  
- Blacklist vehicles from being upgradeable. By default a lot of government vehicles are blacklisted.  
- Saves by SteamID and vehicle model in txt format.  
- And much more!  

##Upgrades##
- Hydraulics (Ability to jump with the car. Activate Key = ALT)  
- Nitrous (Ability to give the car a quick nitrous boost. Activate Key = Mouse 1)  
- Armor (Gives the vehicle a configureable amount of extra health)  
- Underglow (Choose between 5 colors of underglow for your vehicle. Activate Key = Mouse 2)  
- Horn (Choose between 6 horns for your vehicle. Activate Key = Shift)  

##Installation##
This addon supports both DarkRP 2.4.3 and DarkRP 2.5.0.  
**You will find the version for DarkRP 2.4.3 here:**  
DarkRP Vehicle Upgrades/VehUpgrades 2.4.3/DarkRP Vehicle Upgrades  
Extract the last DarkRP Vehicle Upgrades to your addons.  

**You will find the version for DarkRP 2.5.0 here:**  
DarkRP Vehicle Upgrades/VehUpgrades 2.5.0/DarkRP Vehicle Upgrades  
Extract the last DarkRP Vehicle Upgrades to your addons.   

Once you've done that, you will want to set up the car upgrades NPC.
Go the the location you want it to be at and type "vehupgradenpc\_setpos" into your console.  
Remember to aim in the right direction, so you get the right angle on these 2 things.  

You must be an administrator on your server to perform this action.  

##Customizing##
This addon has 3 different config files.

#1 - DarkRP Vehicle Upgrades/lua/autorun/vehupgrades_config.lua  
This is the default configuration for the addon.  
In here you will find general configuration like amount of armor to give, use ulx ranks and more!  

#2 - DarkRP Vehicle Upgrades/lua/autorun/vehupgrades_vehicles.lua  
This config file is extremely large. It includes a list of supported vehicles.   
This is where you need to go if you want to add more vehicles that can be upgraded.  
It includes configuration like hydraulics power and nitrous power for each vehicle.  

#3 - DarkRP Vehicle Upgrades/lua/autorun/vehupgrades_upgrades.lua  
This configuration file is where you will find settings for the 5 different upgrades.  
It includes configuration like name, price and ulx ranks allowed.  

Please don't be afraid of the configuration. Everything is well explaned at the top of each file!  

##Errors & Support##
If you find any problems with the script, please PM me with details of the situation and a copypaste of the error in console. Additionally, i rarely give additional support for my scripts. If there is a general error with the script, an error that you can prove happens, and is my fault. Send me a PM here on CoderHire. We'll figure it out there, and perhaps a chat on Steam after I've responded to your PM. I am also not interested in modifying you a custom version of the addon. Also not upon payment. Sorry!

Conflicting addons is not to be said if I will support that or not. This is something I will decide upon confrontation about a conflicting addon. If you have some sort of proof that an addon is conflicting with my addon, please send me a PM with the details you might have.

Thank you!