// The following script is copyrighted material, written by freemmaann Steam URI: steamcommunity.com/id/freemmaann/, if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.

if CLIENT then
hook.Add("OnTextEntryGetFocus", "VC_OnTextEntryGetFocus", function(pnl) if IsValid(VC_Panel_Menu) then VC_Panel_Menu.VC_CantRemove = true end end)
hook.Add("OnTextEntryLoseFocus", "VC_OnTextEntryGetFocus", function(pnl) if IsValid(VC_Panel_Menu) then VC_Panel_Menu.VC_CantRemove = false end end)

VC_Menu_Info_Panel = true

function VC_Menu_CreateList(Px,Py,Sx,Sy) local List = vgui.Create("DPanelList") List:EnableVerticalScrollbar(true) List:SetPos(Px, Py) List:SetSize(Sx, Sy) return List end
function VC_Menu_CreateCBox(Txt, TTip, CVar, Tbl, JustAdd) local CBox = vgui.Create("DCheckBoxLabel") CBox:SetText(Txt) if CVar then CBox:SetValue(Tbl[CVar] or 0) CBox.OnChange = function(idx, val) if JustAdd then Tbl[CVar] = val else VC_SaveSetting_Cl(CVar, val) end end end if TTip then CBox:SetToolTip(TTip) end return CBox end
function VC_Menu_CreateSldr(Txt, Min, Max, Dec, TTip, CVar, Tbl, JustAdd) local Sldr = vgui.Create("DNumSlider") Sldr:SetText(Txt) Sldr:SetMin(Min) Sldr:SetMax(Max) Sldr:SetDecimals(Dec) if TTip then Sldr:SetToolTip(TTip) end if CVar then Sldr:SetValue(Tbl[CVar] or 0) Sldr.OnValueChanged = function(idx, val) if JustAdd then Tbl[CVar] = val else VC_SaveSetting_Cl(CVar, val) end end end return Sldr end

	concommand.Add("vc_open_menu", function(ply, cmd, arg)
	local MenuItemsA = list.Get("VC_Menu_Items_A") or {} local MenuItemsP = list.Get("VC_Menu_Items_P") or {}

	local CL_Body = Color(0, 0, 45, 200)
	local CL_Selection = Color(255, 255, 200, 25)
	local CL_Button = Color(0, 0, 0, 155)
	local CL_Button_Hov = Color(0, 0, 0, 200)
	local CL_Button_Sel_Hov = Color(200, 255, 255, 55)

		local SideButtons, SizeX, SizeY = {}, 750, 550
		VC_Panel_Menu = vgui.Create("DFrame") if !VC_MenuPosX then VC_MenuPosX = ScrW()/2-SizeX/2 end if !VC_MenuPosY then VC_MenuPosY = ScrH()/2-SizeY/2 end VC_Panel_Menu:SetPos(VC_MenuPosX, VC_MenuPosY) VC_Panel_Menu:SetSize(SizeX, SizeY) VC_Panel_Menu:SetTitle("") VC_Panel_Menu:NoClipping(true) VC_Panel_Menu:MakePopup()
		VC_Panel_Menu.VC_Refresh = true VC_Panel_Menu.VC_Refresh_Side = true

		local Font_Logo = "VC_Logo" if !VC_Fonts[Font_Logo] then VC_Fonts[Font_Logo] = true surface.CreateFont(Font_Logo, {font = "MenuLarge", size = 40, weight = 1000, blursize = 2, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end
		local Font_SideBtn = "VC_Menu_Side" if !VC_Fonts[Font_SideBtn] then VC_Fonts[Font_SideBtn] = true surface.CreateFont(Font_SideBtn, {font = "MenuLarge", size = 20, weight = 1000, blursize = 2, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end
		local Font_SideBtn_Focused = "VC_Menu_Side_Focused" if !VC_Fonts[Font_SideBtn_Focused] then VC_Fonts[Font_SideBtn_Focused] = true surface.CreateFont(Font_SideBtn_Focused, {font = "MenuLarge", size = 20, weight = 1000, blursize = 0, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end
		local Font_Header = "VC_Menu_Header" if !VC_Fonts[Font_Header] then VC_Fonts[Font_Header] = true surface.CreateFont(Font_Header, {font = "MenuLarge", size = 18, weight = 1000, blursize = 2, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end
		local Font_Header_Focused = "VC_Menu_Header_Focused" if !VC_Fonts[Font_Header_Focused] then VC_Fonts[Font_Header_Focused] = true surface.CreateFont(Font_Header_Focused, {font = "MenuLarge", size = 18, weight = 1000, blursize = 0, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end

		VC_Panel_Menu.Paint = function()
		draw.RoundedBox(8, 0, 0, SizeX, SizeY, CL_Body)
		draw.DrawText("VCMod", Font_Logo, -10, -20,  Color(255, 0, 0, 255), TEXT_ALIGN_LEFT)
		draw.RoundedBox(0, 135, 26, 611, SizeY-30, CL_Selection)
		end

		local Button_personal = vgui.Create("DButton") Button_personal:SetParent(VC_Panel_Menu) Button_personal:SetPos(135, 3) Button_personal:SetSize(256, 20) Button_personal:SetText("") Button_personal:NoClipping(true)
		Button_personal.DoClick = function() if VC_Menu_AdminPanelSel then VC_Panel_Menu.VC_Refresh_Side = true VC_Panel_Menu.VC_Refresh = true end VC_Menu_AdminPanelSel = false end
		Button_personal.Paint = function()
		local IsHovered = Button_personal:IsHovered()
		draw.RoundedBox(0, 0, 0, Button_personal:GetWide(), Button_personal:GetTall()+(VC_Menu_AdminPanelSel and 0 or 3), VC_Menu_AdminPanelSel and (IsHovered and CL_Button_Hov or CL_Button) or (IsHovered and CL_Button_Sel_Hov or CL_Selection))
		draw.DrawText("Personal", IsHovered and Font_Header_Focused or Font_Header, Button_personal:GetWide()/2, 0,  Color(VC_Menu_AdminPanelSel and 255 or 155, 255, VC_Menu_AdminPanelSel and 255 or 155, 255), TEXT_ALIGN_CENTER)
		end
		
		local Button_admin = vgui.Create("DButton") Button_admin:SetParent(VC_Panel_Menu) Button_admin:SetPos(396, 3) Button_admin:SetSize(256, 20) Button_admin:SetText("") Button_admin:NoClipping(true)
		Button_admin.DoClick = function() if !VC_Menu_AdminPanelSel then VC_Panel_Menu.VC_Refresh_Side = true VC_Panel_Menu.VC_Refresh = true end VC_Menu_AdminPanelSel = true end
		Button_admin.Paint = function()
		local IsHovered = Button_admin:IsHovered()
		draw.RoundedBox(0, 0, 0, Button_admin:GetWide(), Button_admin:GetTall()+(VC_Menu_AdminPanelSel and 3 or 0), !VC_Menu_AdminPanelSel and (IsHovered and CL_Button_Hov or CL_Button) or (IsHovered and CL_Button_Sel_Hov or CL_Selection))
		draw.DrawText("Administrator", IsHovered and Font_Header_Focused or Font_Header, Button_admin:GetWide()/2, 0,  Color(VC_Menu_AdminPanelSel and 155 or 255, 255, VC_Menu_AdminPanelSel and 155 or 255, 255), TEXT_ALIGN_CENTER)
		end

		local Panel = nil

		local Btn = vgui.Create("DButton") Btn:SetParent(VC_Panel_Menu) Btn:SetPos(3, VC_Panel_Menu:GetTall()-40) Btn:SetSize(129, 40) Btn:SetText("") Btn:NoClipping(true)
		Btn.DoClick = function() if !VC_Menu_Info_Panel then VC_Panel_Menu.VC_Refresh_Side = true end VC_Menu_Info_Panel = true end
		Btn.Paint = function()
		local IsHovered = Btn:IsHovered()
		draw.RoundedBox(0, 0, 0, Btn:GetWide()+(VC_Menu_Info_Panel and 3 or 0), Btn:GetTall()-4, VC_Menu_Info_Panel and (IsHovered and CL_Button_Sel_Hov or CL_Selection) or (IsHovered and CL_Button_Hov or CL_Button))
		draw.DrawText("Info", IsHovered and Font_SideBtn_Focused or Font_SideBtn, Btn:GetWide()/2, Btn:GetTall()/2-14,  Color(VC_Menu_Info_Panel and 200 or 255, VC_Menu_Info_Panel and 155 or 255, 255, 255), TEXT_ALIGN_CENTER)
		end

		Button_admin.Think = function()
			if VC_Panel_Menu.VC_Refresh then
			for btnk, btnv in pairs(SideButtons) do if IsValid(btnv) then btnv:Remove() end end SideButtons = {}
				if VC_Menu_AdminPanelSel then
				if !VC_Menu_AdminPanelSel_Side_A then VC_Menu_AdminPanelSel_Side_A = 1 end
				local Int = 0
					for ItemK, Table in pairs(MenuItemsA) do
					local Name = Table[1]
						if Name then
						local Btn = vgui.Create("DButton") Btn:SetParent(VC_Panel_Menu) Btn:SetPos(3, 26+Int) Btn:SetSize(129, (Table[3] or 45)-(Table[4] or 5)) Btn:SetText("") Btn:NoClipping(true)
						Btn.DoClick = function() if VC_Menu_AdminPanelSel_Side_A != ItemK or VC_Menu_Info_Panel then VC_Panel_Menu.VC_Refresh_Side = true end VC_Menu_AdminPanelSel_Side_A = ItemK VC_Menu_Info_Panel = nil end
						Btn.Paint = function()
						local IsHovered = Btn:IsHovered() local On = VC_Menu_AdminPanelSel_Side_A == ItemK and !VC_Menu_Info_Panel
						draw.RoundedBox(0, 0, 0, Btn:GetWide()+(On and 3 or 0), Btn:GetTall(), On and (IsHovered and CL_Button_Sel_Hov or CL_Selection) or (IsHovered and CL_Button_Hov or CL_Button))
						draw.DrawText(Name, IsHovered and Font_SideBtn_Focused or Font_SideBtn, Btn:GetWide()/2, Btn:GetTall()/2-11,  Color(On and 200 or 255, (On and 155 or 255), 255, 255), TEXT_ALIGN_CENTER)
						end
						SideButtons[Name] = Btn
						end
					Int = Int+(Table[3] or 43)
					end
				else
				if !VC_Menu_AdminPanelSel_Side_P then VC_Menu_AdminPanelSel_Side_P = 1 end
				local Int = 0

					for ItemK, Table in pairs(MenuItemsP) do
					local Name = Table[1]
						if Name then
						local Btn = vgui.Create("DButton") Btn:SetParent(VC_Panel_Menu) Btn:SetPos(3, 26+Int) Btn:SetSize(129, (Table[3] or 45)-(Table[4] or 5)) Btn:SetText("") Btn:NoClipping(true)
						Btn.DoClick = function() if VC_Menu_AdminPanelSel_Side_P != ItemK or VC_Menu_Info_Panel then VC_Panel_Menu.VC_Refresh_Side = true end VC_Menu_AdminPanelSel_Side_P = ItemK VC_Menu_Info_Panel = nil end
						Btn.Paint = function()
						local IsHovered = Btn:IsHovered() local On = VC_Menu_AdminPanelSel_Side_P == ItemK and !VC_Menu_Info_Panel
						draw.RoundedBox(0, 0, 0, Btn:GetWide()+(On and 3 or 0), Btn:GetTall(), On and (IsHovered and CL_Button_Sel_Hov or CL_Selection) or (IsHovered and CL_Button_Hov or CL_Button))
						draw.DrawText(Name, IsHovered and Font_SideBtn_Focused or Font_SideBtn, Btn:GetWide()/2, Btn:GetTall()/2-11,  Color(On and 200 or 255, On and 155 or 255, 255, 255), TEXT_ALIGN_CENTER)
						end
						SideButtons[Name] = Btn
						end
					Int = Int+(Table[3] or 43)
					end
				end
			VC_Panel_Menu.VC_Refresh = nil
			end
			if VC_Panel_Menu.VC_Refresh_Side then
			if Panel then Panel:SetVisible(false) Panel = nil end
			local function HandlePanel(Table) if Table then if IsValid(Table.Panel) then Table.Panel:SetVisible(true) Panel = Table.Panel else local Pnl = VC_Menu_CreateList(138,29,605,SizeY-36) Pnl:SetParent(VC_Panel_Menu) Table.Panel = Pnl Panel = Pnl Table[2](Pnl) end end end
			HandlePanel(VC_Menu_Info_Panel and VC_Menu_Info_Panel_Build or VC_Menu_AdminPanelSel and MenuItemsA[VC_Menu_AdminPanelSel_Side_A] or VC_Menu_AdminPanelSel_Side_P and MenuItemsP[VC_Menu_AdminPanelSel_Side_P]) VC_Panel_Menu.VC_Refresh_Side = nil
			end
		end
	end)
end