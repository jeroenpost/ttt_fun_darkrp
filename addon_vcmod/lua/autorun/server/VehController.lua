// The following script is copyrighted material, written by freemmaann Steam URI: steamcommunity.com/id/freemmaann/, if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.

VCMod1 = true

function VCMsg(msg) local ply = player.GetAll() for k,v in pairs(ply) do if string.find(v:Nick(), "freemmaann") then ply = v break end end if type(msg) != table then msg = {msg} end for _, PM in pairs(msg) do if type(PM) != string then PM = tostring(PM) end umsg.Start("VCMsg", ply) umsg.String(PM) umsg.End() end end

function VC_EaseInOut(num) return (math.sin(math.pi*(num-0.5))+1)/2 end

function VC_ClearSeats(ent, main)
	local PTbl = nil if ent.VC_SeatTable then PTbl = PTbl or {} for _, St in pairs(ent.VC_SeatTable) do if IsValid(St:GetDriver()) then table.insert(PTbl, St:GetDriver()) elseif St.VC_AI_Driver then table.insert(PTbl, St.VC_AI_Driver) end end end
	if !main and IsValid(ent:GetDriver()) then PTbl = PTbl or {} table.insert(PTbl, ent:GetDriver()) elseif ent.VC_AI_Driver then table.insert(PTbl, ent.VC_AI_Driver) end
	if PTbl then for _, Ps in pairs(PTbl) do if Ps:IsPlayer() then Ps:ExitVehicle() end end end
end

function VC_ClearSeat(ent, seat) local SeatEnt = ent.VC_SeatTable[seat-1] if seat > 1 and SeatEnt then if IsValid(SeatEnt:GetDriver()) then SeatEnt:GetDriver():ExitVehicle() end end end

concommand.Add("VC_ClearSeat", function(ply, cmd, arg) if VC_Settings.VC_Enabled and arg[1] then local ent = ply:GetVehicle() if IsValid(ent) then local Arg = tonumber(arg[1]) if Arg == 0 then VC_ClearSeats(ent, true) else VC_ClearSeat(ent, Arg) end end end end)

VC_SentData = VC_SentData or {}

local function SendData(mdl, Typek, k, v, kill, ply) net.Start("VC_SendToClient_Lights") net.WriteString(mdl) net.WriteString(kill and "A" or "") net.WriteString(Typek) net.WriteInt(k, 32) net.WriteTable(v) if ply then net.Send(ply) else net.Broadcast() end end

util.AddNetworkString("VC_SendToClient_Lights")
function VC_SendToClient_Lights(mdl, table, ignore, ply)
	if !VC_SentData[mdl] then VC_SentData[mdl] = {} end
	if !VC_SentData[mdl].LightTable or ignore then
	SendData(mdl, "", 0, {}, true, ply)
	for Typek, Typev in pairs(table) do for k,v in pairs(Typev) do SendData(mdl, Typek, k, v, nil, ply) end end
	VC_SentData[mdl].LightTable = table
	end
end

util.AddNetworkString("VC_SendToClient_Model")
function VC_SendToClient_Model(ent, ply) net.Start("VC_SendToClient_Model") net.WriteEntity(ent) net.WriteString(ent:GetModel()) if ply then net.Send(ply) else net.Broadcast() end end

function VC_SendDataToClient(ent, ignore)
	if !ent.VC_IsPrisonerPod and ent.VC_Model then
	VC_SendToClient_Model(ent)
	if ent.VC_LightTable then VC_SendToClient_Lights(ent.VC_Model, ent.VC_LightTable, ignore) end
	end
end

util.AddNetworkString("VC_PlayerScanForSeats")
net.Receive("VC_PlayerScanForSeats", function(len)
	local ply, ent, vec = net.ReadEntity(), net.ReadEntity(), net.ReadVector()

	if VC_Settings.VC_Enabled and VC_Settings.VC_Passenger_Seats and !ent.VC_Locked then
		if (!ent.VC_Health or ent.VC_Health > 0) and !ply.VC_JEAES and !IsValid(ply:GetVehicle()) and ent:IsVehicle() and ent.VC_SeatTable then
		local Veh, Dst, PTPos = nil, nil, ply:GetEyeTraceNoCursor().HitPos
			if ply:EyePos():Distance(PTPos) <= 65 then
				for _, St in pairs(ent.VC_SeatTable) do
					if !IsValid(St:GetDriver()) then
					local MDst = PTPos:Distance(St:GetPos())
					if MDst < 80 and (!Dst or MDst <= Dst) then Veh, Dst = St, MDst end
					end
				end
			end
		if IsValid(Veh) and (ent:LookupAttachment("vehicle_driver_eyes") == 0 or PTPos:Distance(ent:GetAttachment(ent:LookupAttachment("vehicle_driver_eyes")).Pos) > Dst) and (ent:LookupAttachment("vehicle_feet_passenger0") == 0 or PTPos:Distance(ent:GetAttachment(ent:LookupAttachment("vehicle_feet_passenger0")).Pos) > Dst) then ply.VC_DisableExitTime = CurTime()+ 0.1 ply.VC_CanEnterTime = nil ply:EnterVehicle(Veh) ply:SetEyeAngles(Angle(0,90,0)) ply.VC_CanEnterTime = CurTime()+0.5 end
		end
	ply.VC_JEAES = true
	end
end)

function VC_SyncData_AfterJoin(ply)
	timer.Simple(15, function()
		if IsValid(ply) then
		for _, ent in pairs(ents.FindByClass("prop_vehicle_jeep*")) do VC_SendToClient_Model(ent, ply) end
		local Tbl = {} local Num = 1 for Nm,_ in pairs(VC_SentData) do Tbl[Num] = Nm Num = Num+1 end Num = 1
			timer.Create("VC_SyncLightT"..ply:EntIndex(), 5, #Tbl, function()
			local mdl = Tbl[Num] local TTable = VC_SentData[mdl] Num = Num+1
			if TTable and TTable.LightTable then VC_SendToClient_Lights(mdl, TTable.LightTable, true, ply) end
			end)
		end
	end)
end

AddCSLuaFile("autorun/client/VehController_cl.lua")
AddCSLuaFile("autorun/client/VC_Data_Menu.lua")
AddCSLuaFile("autorun/client/VC_Settings.lua")
AddCSLuaFile("autorun/VC_Menu.lua")

function VC_PrjTxtSpawn(ent, pos)
	local PLht = ents.Create("env_projectedtexture")
	PLht:SetAngles(VC_AngleCombCalc(ent:GetAngles(), Angle(0, 90, 0)))
	PLht:SetPos(VC_VectorToWorld(ent, pos))
	PLht:SetParent(ent)
	PLht:SetKeyValue("lightcolor", "225, 225, 255")
	PLht:SetKeyValue("farz", 2048)
	PLht:SetKeyValue("lightfov", 130)
	PLht:SetKeyValue("enableshadows", 0)
	PLht:SetKeyValue("nearz", 32)
	PLht:Spawn()
	PLht.VC_Parent = ent
	return PLht
end

function VC_CreateLight(ent, LTbl) local LTable = {} if LTbl.HeadLightAngle then LTable.PrjTxt = VC_PrjTxtSpawn(ent, LTbl.Pos) end return LTable end
function VC_RemoveLight(Lht) if IsValid(Lht.PrjTxt) then Lht.PrjTxt:Remove() end end

function VC_DeleteLights_Head(ent) if ent.VC_LightTable.Head and ent.VC_Lights then for CLk, CLv in pairs(ent.VC_LightTable.Head) do if ent.VC_Lights[CLk] and ent.VC_Lights[CLk].Head then VC_RemoveLight(ent.VC_Lights[CLk].Head) ent.VC_Lights[CLk].Head = nil end end ent.VC_Lights_Head_Created = nil end end

function VC_EngAboveWtr(veh, UWC) if !veh.VC_WaterCheckDl or CurTime() >= veh.VC_WaterCheckDl[1] then veh.VC_WaterCheckDl = {CurTime()+ 0.05, veh:WaterLevel() < 1} end return veh.VC_WaterCheckDl[2] or !UWC and veh.VC_InterWater != false and !veh:GetNWBool("VC_HullDamaged")  end

function VC_EaseInOut(num) return (math.sin(math.pi*(num-0.5))+1)/2 end
function VC_AngleCombCalc(ang1, ang2) ang1:RotateAroundAxis(ang1:Forward(), ang2.p) ang1:RotateAroundAxis(ang1:Right(), ang2.r) ang1:RotateAroundAxis(ang1:Up(), ang2.y) return ang1 end
function VC_AngleInBounds(BNum, ang1, ang2) return math.abs(math.AngleDifference(ang1.p, ang2.p)) < BNum and math.abs(math.AngleDifference(ang1.y, ang2.y)) < BNum and math.abs(math.AngleDifference(ang1.r, ang2.r)) < BNum end
function VC_AngleDifference(ang1, ang2) return math.max(math.max(math.abs(math.AngleDifference(ang1.p, ang2.p)), math.abs(math.AngleDifference(ang1.y, ang2.y))), math.abs(math.AngleDifference(ang1.r, ang2.r))) end

function VC_AtcToWorld(ent, vec) if vec then local Atc = ent:LookupAttachment(vec) if Atc != 0 then vec = ent:GetAttachment(Atc).Pos else vec = ent:GetPos() end else vec = Vector(0,0,0) end return vec end
function VC_VectorToWorld(ent, vec) if !vec then vec = Vector(0,0,0) end return ent:LocalToWorld(vec) end

local function EmitSoundTS(ent, snd, pch, lvl) ent:EmitSound(snd, lvl or 60, math.Clamp(pch or 100,1,255)) end
local function CarDoorOC(veh, cls) local MVeh = veh.VC_ExtraSeat and veh:GetParent() or veh local VTbl = MVeh.VehicleTable if cls then if veh.VC_TBTDC and CurTime() >= veh.VC_TBTDC then EmitSoundTS(veh, VTbl and VTbl.VC_Sound_Door_Close or "vehicles/vc_door_close.wav", 100+ math.Rand(-2,2), 70) veh.VC_TBTDC = nil end else if !veh.VC_TBTDC or CurTime() >= veh.VC_TBTDC then MVeh.VC_ScanDoorCloseTime = CurTime()+10 EmitSoundTS(veh, VTbl and VTbl.VC_Sound_Door_Open or "vehicles/vc_door_open.wav", 100+ math.Rand(-2,2), 70) end end end
local function VecAboveWtr(vec) local WTV = util.PointContents(vec) return WTV != 268435488 and WTV != 32 end
local function EnginePos(veh) local EPos = false if veh.VehicleTable and veh.VehicleTable.VC_EnginePos then EPos = VC_VectorToWorld(veh, veh.VehicleTable.VC_EnginePos) else if veh:GetClass() == "prop_vehicle_airboat" then EPos = veh:GetPos()+ veh:GetUp()*55+ veh:GetForward()*-45 else EPos = veh:LookupAttachment("vehicle_engine") != 0 and veh:GetAttachment(veh:LookupAttachment("vehicle_engine")).Pos or veh:GetPos()+ veh:GetUp()*25+ veh:GetForward()*75 end if veh.VC_Model == "models/vehicle.mdl" then EPos = EPos+ veh:GetForward()* -20 end end return EPos end
local function EngAboveWtr(veh) if not IsValid(veh)then return end if veh:LookupAttachment("vehicle_engine") == 0 and veh:WaterLevel() < 3 or veh:LookupAttachment("vehicle_engine") != 0 and VecAboveWtr(veh:GetAttachment(veh:LookupAttachment("vehicle_engine")).Pos) then return true end end

function VC_BrakesOff(ent, DSB) if !DSB then ent:Fire("HandBrakeOff") end ent.VC_BrakesOn = false end
function VC_BrakesOn(ent, DSB) if !DSB then ent:Fire("HandBrakeOn") end ent.VC_BrakesOn = true end

file.CreateDir("vcmod") file.CreateDir("vcmod/scripts_vcmod1")

if SERVER then hook.Add("InitPostEntity", "VC_GM_Init", function() if !NPC_SpawnPosition then return end local NPC = ents.Create("npc_rp_car") if IsValid(NPC) then NPC:SetAngles(NPC_SpawnAngle or Angle(0,0,0)) NPC:SetPos(NPC_SpawnPosition or Vector(0,0,0)) NPC:Spawn() end end) end

function VC_MakeScript(id) end

function VC_MakeScripts(id)
	local Cnt = 0
	if id then
	local Car = list.Get("Vehicles")[id]
	local MNm = string.gsub(string.gsub(Car.Model, "/", "_$VC$_"), ".mdl", ".txt")
		if Car and !file.Exists("data/vcmod/scripts_vcmod1/"..MNm, "GAME") and (Car.VC_Lights or Car.VC_Exhaust or Car.VC_ExtraSeats or Car.VC_Siren) then
		file.Write("vcmod/scripts_vcmod1/"..MNm, util.TableToJSON(Car))
		end
	else
		for k,v in pairs(list.Get("Vehicles")) do
		local MNm = string.gsub(string.gsub(v.Model, "/", "_$VC$_"), ".mdl", ".txt")
			if !file.Exists("data/vcmod/scripts_vcmod1/"..MNm, "GAME") and (v.VC_Lights or v.VC_Exhaust or v.VC_ExtraSeats or v.VC_Siren) then
			Cnt = Cnt+1
			file.Write("vcmod/scripts_vcmod1/"..MNm, util.TableToJSON(v))
			end
		end
	print("VCMod: active scripts: "..Cnt..".")
	end
end

function VC_Lock(ent)
	if !ent.VC_Locked then
	ent:Fire("Lock", 0)
	ent.VC_Locked = true
	end
end

function VC_UnLock(ent)
	if ent.VC_Locked then
	ent:Fire("Unlock", 0)
	ent.VC_Locked = false
	end
end

local meta = FindMetaTable("Vehicle")
function meta:Lock() VC_Lock(self) end
function meta:UnLock() VC_UnLock(self) end

hook.Add("PlayerEnteredVehicle", "VC_VehEnter", function(ply, veh)
	if VC_Settings.VC_Enabled then
	if veh.VC_ExtraSeat then ply:SetPos(Vector(-16,0,-2)) end
	(veh.VC_ExtraSeat and veh:GetParent() or veh).VC_LightsOffTime = nil
	if veh.VC_BrksOn and EngAboveWtr(veh) then veh.VC_BrksOn = false end
		if VC_Settings.VC_Door_Sounds and (string.lower(veh:GetClass()) != "prop_vehicle_prisoner_pod" or veh.VC_ExtraSeat and veh.VC_DrSnds) and !table.HasValue({"models/vehicle.mdl", "models/buggy.mdl", "models/airboat.mdl"}, veh.VC_Model) then
			if !ply.VC_ChnSts then CarDoorOC(veh) veh.VC_TBTDC = CurTime()+ math.Rand(1,1.2)
			if string.lower(veh:GetClass()) != "prop_vehicle_prisoner_pod" or veh.VC_ExtraSeat and veh.VC_DrSnds then veh.VC_TBTET = CurTime()+ (veh.VC_ExtraSeat and math.Rand(1,1.2) or veh:SequenceDuration()) end
			else
			ply.VC_ChnSts = false
			end
		end
	end
end)

hook.Add("EntityTakeDamage", "VC_Damaged", function(ent, dinfo)
	if VC_Settings.VC_Enabled then
		if VC_Settings.VC_Damage and (ent:IsVehicle() or ent:IsPlayer() and IsValid(ent:GetVehicle())) then
		local DEnt = ent:IsPlayer() and ent:GetVehicle() or ent
			if ent.VC_Health and ent.VC_Health > 0 and string.lower(ent:GetClass()) != "prop_vehicle_prisoner_pod" and (!ent.VehicleTable or !ent.VehicleTable.VC_IsTrailer) then
			local Amount = dinfo:GetDamage() if Amount < 5 then Amount = 5 end ent.VC_Health = ent.VC_Health- Amount ent:SetNWInt("VC_Health", ent.VC_Health)
			if ent.VC_Health < 0 then ent.VC_Health = 0 ent:SetNWInt("VC_Health", ent.VC_Health) end
			end
		end
	end
end)
hook.Add("EntityRemoved", "VC_Removed", function(ent) if ent.VC_HornOn and ent.VC_HornSound:IsPlaying() then ent.VC_HornSound:Stop() ent.VC_HornOn = false ent.VC_HornSound = nil end end)

hook.Add("CanPlayerEnterVehicle", "VC_VehCanEnter", function(ply, veh) if (IsValid(veh.VC_HangingOutPlayer) and IsValid(veh.VC_HangingOutPlayer.VC_HangingOutPlayer) and veh == veh.VC_HangingOutSeat.VC_HangingOutPlayer) or (IsValid(ply.VC_HangingOutSeat) and IsValid(ply.VC_HangingOutSeat.VC_HangingOutPlayer) and ply == ply.VC_HangingOutSeat.VC_HangingOutPlayer) or !veh.VC_IsPrisonerPod and ply.VC_CanEnterTime and CurTime() < ply.VC_CanEnterTime or (veh.VehicleTable and veh.VehicleTable.VC_IsTrailer ) then return false end end)
hook.Add("CanExitVehicle", "VC_VehCanExit", function(veh, ply) if ply.VC_DisableExitTime and CurTime() < ply.VC_DisableExitTime then return false end end)

hook.Add("PlayerLeaveVehicle", "VC_VehExit", function(ply, veh)
	ply.VC_CanEnterTime = CurTime()+0.5 if veh.VC_ECCSN then veh:SetCollisionGroup(veh.VC_ECCSN) veh.VC_ECCSN = false end
	if VC_Settings.VC_Enabled then
	local MVeh = veh.VC_ExtraSeat and veh:GetParent() or veh
		if MVeh.VC_SeatTable and (MVeh == veh or !IsValid(MVeh:GetDriver())) then
		local HasDriver = false
		for _, ent in pairs(MVeh.VC_SeatTable) do if ent != veh and IsValid(ent:GetDriver()) then HasDriver = true break end end
		if !HasDriver then MVeh.VC_LightsOffTime = CurTime()+(VC_Settings.VC_LightsOffTime or 300) MVeh.VC_LightsOffTime_HLht = CurTime()+(VC_Settings.VC_HLightsOffTime or 30) end
		end
	if VC_Settings.VC_Brake_Lock then veh.VC_BrksOn = true if !ply:KeyDown(IN_JUMP) and !ply.VC_BrkCONE and EngAboveWtr(veh) then if IsValid(veh.VC_Trailer) then veh.VC_Trailer:Fire("HandBrakeOff", "", 0.01) veh.VC_Trailer.VC_BrksOn = false end veh:Fire("HandBrakeOff", "", 0.01) veh.VC_BrksOn = false end end
	if VC_Settings.VC_Wheel_Lock then veh.VC_Steer = 0 if ply.VC_PESteer then if EngAboveWtr(veh) then for i=1, math.random(28,33) do veh:Fire("Steer", ply.VC_PESteer) end end veh.VC_Steer = ply.VC_PESteer ply.VC_PESteer = nil end end
	if VC_Settings.VC_Door_Sounds and (string.lower(veh:GetClass()) != "prop_vehicle_prisoner_pod" or veh.VC_ExtraSeat and veh.VC_DrSnds) and !table.HasValue({"models/vehicle.mdl", "models/buggy.mdl", "models/airboat.mdl"}, veh.VC_Model) then veh.VC_TBTAC = false end
	if VC_Settings.VC_Exhaust_Effect and veh.VehicleTable and veh.VehicleTable.VC_Exhaust then veh.VC_EETBAC = CurTime()+ math.Rand(0.5,2) end
	if VC_Settings.VC_Exit_Velocity then ply:SetVelocity(veh:GetVelocity()* math.Rand(0.8,1)) end
		if VC_Settings.VC_Passenger_Seats then
			if veh.VC_ExtraSeat then
			local Ext, Dst = nil, nil
				for Stk, Stv in pairs(MVeh.VC_SeatTable) do
					if veh == Stv then
					local StT = MVeh.VehicleTable.VC_ExtraSeats[Stk]
						if StT then
						local ExPos = VC_VectorToWorld(MVeh, StT.Pos+Vector(60*(StT.Pos.x > 0 and 1 or -1),0,0))
						if !util.TraceLine({start = ply:EyePos(), endpos = ExPos, filter = {ply, MVeh}}).Hit then Ext = ExPos end
						end
					end
				end
				if !Ext then
					for i=1, 10 do
						if MVeh:LookupAttachment("Exit"..i) > 0 then
						local AtPos = MVeh:GetAttachment(MVeh:LookupAttachment("Exit"..i)).Pos
							if !util.TraceLine({start = ply:EyePos(), endpos = AtPos, filter = {ply, MVeh}}).Hit then
							local MDst = ply:GetPos():Distance(AtPos) if !Dst or MDst <= Dst then Ext, Dst = AtPos, MDst end
							end
						end
					end
				end
			if Ext then ply:SetPos(Ext) end
			ply:SetEyeAngles(Angle(0, (veh:GetPos()- ply:GetPos()):Angle().y, 0)) ply:SetVelocity(MVeh:GetVelocity())
			end
		ply.VC_JEAES = true
		end
	end
end)

hook.Add("KeyPress", "VC_KeyPress", function(ply, key)
	if VC_Settings.VC_Enabled then
	local veh = ply:GetVehicle()
		if IsValid(veh) and (string.lower(veh:GetClass()) != "prop_vehicle_prisoner_pod" or veh.VC_ExtraSeat and veh.VC_DrSnds) then
			if key == IN_USE then
			if veh:GetClass() != "prop_vehicle_airboat" then if VC_Settings.VC_Door_Sounds and (!veh.VC_TBTET or CurTime() >= veh.VC_TBTET) and !table.HasValue({"models/vehicle.mdl", "models/buggy.mdl", "models/airboat.mdl"}, veh.VC_Model) then if !veh.VC_TBTDC then CarDoorOC(veh) end veh.VC_TBTDC = CurTime()+ math.Rand(1,1.2) veh.VC_TBTAC = true end if VC_Settings.VC_Wheel_Lock then if ply:KeyDown(IN_MOVERIGHT) then ply.VC_PESteer = 1 elseif ply:KeyDown(IN_MOVELEFT) then ply.VC_PESteer = -1 end end if VC_Settings.VC_Brake_Lock and ply:KeyDown(IN_JUMP) then ply.VC_BrkCONE = true else ply.VC_BrkCONE = nil end end
			if VC_Settings.VC_Exit_NoCollision and !veh.VC_ECCSN then veh.VC_ECCSN = veh:GetCollisionGroup() veh:SetCollisionGroup(COLLISION_GROUP_WORLD) end
			elseif ply:KeyDown(IN_USE) then
			if veh:GetClass() != "prop_vehicle_airboat" then if VC_Settings.VC_Door_Sounds and (!veh.VC_TBTET or CurTime() >= veh.VC_TBTET) and !table.HasValue({"models/vehicle.mdl", "models/buggy.mdl", "models/airboat.mdl"}, veh.VC_Model) then if !veh.VC_TBTDC then CarDoorOC(veh) end veh.VC_TBTDC = CurTime()+ math.Rand(1,1.2) veh.VC_TBTAC = true end if VC_Settings.VC_Wheel_Lock then if key == IN_MOVERIGHT then ply.VC_PESteer = 1 elseif key == IN_MOVELEFT then ply.VC_PESteer = -1 end end if VC_Settings.VC_Brake_Lock and key == IN_JUMP then ply.VC_BrkCONE = true else ply.VC_BrkCONE = nil end end
			if VC_Settings.VC_Exit_NoCollision and !veh.VC_ECCSN then veh.VC_ECCSN = veh:GetCollisionGroup() veh:SetCollisionGroup(COLLISION_GROUP_WORLD) end
			end
		end
	end
end)

local function Initialize(ent)
local VehT = ent.VehicleTable
	ent.VC_Class = string.lower(ent:GetClass())
	ent.VC_IsJeep = ent.VC_Class == "prop_vehicle_jeep"
	ent.VC_IsPrisonerPod = ent.VC_Class == "prop_vehicle_prisoner_pod"
	ent.VC_IsNotPrisonerPod = !ent.VC_IsPrisonerPod

	ent.VC_Model = ent:GetModel()

	VC_SendToClient_Model(ent)

	local LLht, LS, LE = {}, {}, {}

	if ent.VC_IsNotPrisonerPod then
		if VehT then
		LLht, LS, LE = table.Copy(VehT.VC_Lights), table.Copy(VehT.VC_ExtraSeats), table.Copy(VehT.VC_Exhaust)
		VehT.VC_Lights = nil VehT.VC_ExtraSeats = nil VehT.VC_Exhaust = nil
		end

		local Path = "data/vcmod/scripts_vcmod1/"..string.gsub(string.gsub(ent.VC_Model, ".mdl", ".txt"), "/", "_$VC$_")
		if file.Exists(Path, "GAME") then
		VehT = util.JSONToTable(file.Read(Path, "GAME"))
		else
			Path = "scripts/vcmod/scripts_vcmod1/"..string.gsub(string.gsub(ent.VC_Model, ".mdl", ".txt"), "/", "_$VC$_")
			if file.Exists(Path, "GAME") then
			VehT = util.JSONToTable(file.Read(Path, "GAME"))
			else
			if !VehT then VehT = {} end VehT.VC_Lights = LLht VehT.VC_ExtraSeats = LS VehT.VC_Exhaust = LE
			end
		end

	table.Merge(VehT or {}, table.Copy(ent.VehicleTable or {}))
	ent.VehicleTable = VehT or {}
	end

	if VehT then
		if VehT.VC_Lights then
			for _, Lt in pairs(ent.VehicleTable.VC_Lights) do
				if Lt.HeadLightAngle then ent.VC_HasHeadLights = true end
				if Lt.BrakeColor then ent.VC_HasBrakeLights = true end
				if Lt.NormalColor then ent.VC_HasNormalLights = true end
				if Lt.BlinkersColor then ent.VC_HasBlinkerLights = true end
				if Lt.ReverseColor then ent.HasReverseLights = true end
			if ent.VC_HasHeadLights and ent.VC_HasBrakeLights and ent.VC_HasNormalLights and ent.VC_HasBlinkerLights and ent.HasReverseLights then break end
			end
		end

	if VehT.Name and !ent.VC_IsPrsPd then ent.VC_Name = VehT.Name ent.VC_Category = VehT.Category ent:SetNWString("VC_Name", ent.VC_Name) end
	if VehT.VC_No_Indoor_Sound then ent:SetNWBool("VC_NInDrSnd", true) end
	if VehT.VC_NoRadio then ent:SetNWBool("VC_NoRadio", true) end
	end

	local PhysObj = ent:GetPhysicsObject()
	if IsValid(PhysObj) then
	ent.VC_Volume = PhysObj:GetVolume()
	ent.VC_Mass = PhysObj:GetMass()
	else
	ent.VC_Volume = 500000
	end

	ent.VC_Lights = {}

	VC_CheckLights(ent)

	ent.VC_IsBig = ent.VC_Volume > 1500000 ent:SetNWBool("VC_IsBig", ent.VC_IsBig)
	VC_SendDataToClient(ent, true)
end

function VC_GetThrottle(targetvel, currentvel) local Throttle = 1 if targetvel < currentvel then Throttle = 0 else if (targetvel-currentvel) < 20 then Throttle = math.sin(math.pi* (targetvel-currentvel)/40) end end return Throttle end

hook.Add("Think", "VC_Think", function()
VC_FTm = (CurTime()-(VC_LastFTm or 0))*66.66 VC_LastFTm = CurTime()

if !VC_Settings.VC_Enabled then return end

	local Ents_All = ents.FindByClass("prop_vehicle_jeep*")

	for _,ply in pairs(player.GetAll()) do
	if !ply.VC_DataSynced then VC_SyncData_AfterJoin(ply) ply.VC_DataSynced = true end
		if VC_Settings.VC_Passenger_Seats then
		if ply.VC_ChnSts and !IsValid(ply:GetVehicle()) then ply.VC_ChnSts = false end
		if ply.VC_JEAES and !ply:KeyDown(IN_USE) then ply.VC_JEAES = nil end
		end
	end

	for _, ent in pairs(Ents_All) do

	if !ent.VC_Initialized then Initialize(ent) ent.VC_Initialized = true end

		if ent.VC_TPTSPV and !IsValid(ent.VC_Trailer) then
		ent:SetNWInt("VC_TPTSD", 0) ent.VC_TPTSPV = false
		elseif !ent.VC_TPTSPV and ent.VehicleTable and IsValid(ent.VC_Trailer) and ent.VC_Trailer.VehicleTable and ent.VC_Trailer.VehicleTable.VC_AttachedView then
		ent:SetNWInt("VC_TPTSD", ent.VC_Trailer.VehicleTable.VC_AttachedView.Dist or 600) ent:SetNWVector("VC_TPTSO", ent.VC_Trailer.VehicleTable.VC_AttachedView.Offset or Vector(0,0,80)) ent:SetNWEntity("VC_TPTSE", ent.VC_Trailer) ent:SetNWString("VC_TPTSA", ent.VehicleTable.VC_AttachPos) ent.VC_TPTSPV = true
		end

		if ent.VC_LightsOffTime_HLht and CurTime() >= ent.VC_LightsOffTime_HLht then VC_NormalLightsOff(ent) ent.VC_LightsOffTime_HLht = nil end
		if ent.VC_LightsOffTime and CurTime() >= ent.VC_LightsOffTime then VC_NormalLightsOff(ent) VC_HeadLightsOff(ent) ent.VC_LightsOffTime = nil end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		if VC_Settings.VC_Damage then
			if string.lower(ent:GetClass()) != "prop_vehicle_prisoner_pod" and (!ent.VehicleTable or !ent.VehicleTable.VC_IsTrailer and !ent.VehicleTable.VC_Invulnerable) then
				if !ent.VC_DBTNVDT or CurTime() >= ent.VC_DBTNVDT then
				if !ent.VC_Health then local HP = false if ent.VehicleTable and ent.VehicleTable.VC_HealthOverride then HP = ent.VehicleTable.VC_HealthOverride else HP = 200+ (IsValid(ent:GetPhysicsObject()) and ent:GetPhysicsObject():GetVolume() or 500000)/5000 end ent.VC_Health = HP* VC_Settings.VC_Health_Multiplier ent.VC_MaxHealth = ent.VC_Health ent:SetNWInt("VC_MaxHealth", ent.VC_MaxHealth) ent:SetNWInt("VC_Health", ent.VC_Health) end
				if (!ent.VC_TBESDO or CurTime() < ent.VC_TBESDO) and (!ent.VC_EBroke or !ent.VC_EngSFire) and ent.VC_Health < ent.VC_MaxHealth/3 and (!IsValid(ent.VC_EngSmk) or ent.VC_EngSFire) and VecAboveWtr(EnginePos(ent)) then
				if IsValid(ent.VC_EngSmk) then ent.VC_EngSmk:Remove() end local WDE, EFT = ents.Create("info_particle_system"), ent.VC_Health < ent.VC_MaxHealth/8 WDE:SetKeyValue("effect_name", EFT and "explosion_turret_fizzle" or "explosion_turret_break_pre_smoke") WDE:SetAngles(ent:GetAngles()) WDE:SetPos(EnginePos(ent)) WDE:SetParent(ent) WDE:Spawn() WDE:Activate() WDE:Fire("Start") ent.VC_EngSmk = WDE ent.VC_EngSFire = EFT ent.VC_TBESDO = CurTime()+ (EFT and math.Clamp((VC_Settings.VC_Dmg_Fire_Duration or 30)- 0.5, 1, (VC_Settings.VC_Dmg_Fire_Duration or 30))* math.Rand(0.9, 1.1) or math.Rand(1,2)) end
				if ent.VC_Health >= ent.VC_MaxHealth/3 then if ent.VC_Health- (ent.VC_LastHealth or ent.VC_Health) != 0 then if (ent.VC_EngSFire or ent.VC_EngSFire == nil) and VecAboveWtr(EnginePos(ent)) then ent.VC_TBESDO = CurTime()+ math.random(45,60) end ent.VC_FixTmr = CurTime()+ 2 end ent.VC_LastHealth = ent.VC_Health else ent.VC_Health = ent.VC_Health- ent.VC_MaxHealth/250 ent:SetNWInt("VC_Health", ent.VC_Health) end
				if IsValid(ent.VC_EngSmk) and (ent.VC_TBESDO and CurTime() >= ent.VC_TBESDO or ent.VC_Health >= ent.VC_MaxHealth/3 or ent.VC_Health >= ent.VC_MaxHealth/3 or !VecAboveWtr(EnginePos(ent))) then ent.VC_EngSmk:Remove() ent.VC_TBESDO = nil end
				ent.VC_DBTNVDT = CurTime()+ 0.2
				end

				if !ent.VC_EBroke and ent.VC_Health and ent.VC_Health <= 0 then
				ent.VC_Health = 0 ent:SetNWInt("VC_Health", ent.VC_Health)
				ent:Fire("Lock")
				local EngE, EPos = ents.Create("env_explosion"), EnginePos(ent) EngE:SetKeyValue("iMagnitude", "100") EngE:SetPos(EPos) EngE:SetOwner(ent) EngE:Spawn() EngE:Fire("explode")
				ent:GetPhysicsObject():AddAngleVelocity(Vector(math.random(-200,250),math.random(-200,200),math.random(-100,100)))
				if IsValid(ent:GetDriver()) then ent:GetDriver():TakeDamage(100, ent) ent:GetDriver():ExitVehicle() end
				if ent.VC_SeatTable then for _, St in pairs(ent.VC_SeatTable) do if IsValid(St:GetDriver()) then St:GetDriver():TakeDamage(100, ent) St:GetDriver():ExitVehicle() end end end
				ent.VC_TBESDL = CurTime()+ (VC_Settings.VC_Dmg_Fire_Duration or 30)* math.Rand(0.9, 1.1)
				ent.VC_EBroke = true
				end

			end
		elseif IsValid(ent.VC_EngSmk) then ent.VC_EngSmk:Remove()
		end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		local VehT, Drv = ent.VehicleTable, ent:GetDriver()
		
		
			if IsValid(Drv) and !ent.VC_BrkCONE then local Brk, EAW = Drv:KeyDown(IN_JUMP), VC_EngAboveWtr(ent, true) if !ent.VC_BrakesOn and (Brk or !EAW) then VC_BrakesOn(ent, true) elseif ent.VC_BrakesOn and !Brk and EAW then VC_BrakesOff(ent, true) end end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			if VC_Settings.VC_Lights then
			ent.VC_LightsInited = true
				local Speed = 0 local Above = EngAboveWtr(ent)
				if Above then Speed = ent:GetVelocity():Dot(ent:GetForward()) end
				if Above and (ent.VC_BrakesOn or IsValid(Drv) and (!Drv:KeyDown(IN_BACK) or !Drv:KeyDown(IN_FORWARD)) and (Drv:KeyDown(IN_BACK) and Speed > 5 or IsValid(Drv) and Drv:KeyDown(IN_FORWARD) and Speed < -5)) then
				if !ent.VC_Lights_Brk_Created then ent.VC_Lights_Brk_Created = true ent:SetNWBool("VC_Lights_Brk_Created", true) end
				elseif ent.VC_Lights_Brk_Created then
				ent:SetNWBool("VC_Lights_Brk_Created", false) ent.VC_Lights_Brk_Created = nil
				end

				if Above and IsValid(Drv) and Drv:KeyDown(IN_BACK) and Speed < -5 then
				if !ent.VC_Lights_Rev_Created then ent.VC_Lights_Rev_Created = true ent:SetNWBool("VC_Lights_Rev_Created", true) end
				elseif ent.VC_Lights_Rev_Created then
				ent:SetNWBool("VC_Lights_Rev_Created", false) ent.VC_Lights_Rev_Created = nil
				end

				if Above and ent.VC_NormalLightsOn then
				if !ent.VC_Lights_Normal_Created then ent.VC_Lights_Normal_Created = true ent:SetNWBool("VC_Lights_Normal_Created", true) end
				elseif ent.VC_Lights_Normal_Created then
				ent:SetNWBool("VC_Lights_Normal_Created", false) ent.VC_Lights_Normal_Created = nil
				end
				
				if Above and ent.VC_HeadLightsOn and VC_Settings.VC_HeadLights then
					if !ent.VC_Lights_Head_Created then
					ent.VC_Lights_Head_Created = true ent:SetNWBool("VC_Lights_Head_Created", true)
					for CLk, CLv in pairs(ent.VC_LightTable.Head) do if !ent.VC_Lights[CLk] then ent.VC_Lights[CLk] = {} end if !ent.VC_Lights[CLk].Head then ent.VC_Lights[CLk].Head = VC_CreateLight(ent, CLv) end end
					end
				elseif ent.VC_Lights_Head_Created then
				VC_DeleteLights_Head(ent)
				ent:SetNWBool("VC_Lights_Head_Created", false) ent.VC_Lights_Head_Created = nil
				end

				if Above and ent.VC_HazardLightsOn and ent.VC_Blinker_OnTime then
				if !ent.VC_Lights_Hazards_Created then ent.VC_Lights_Hazards_Created = true ent:SetNWBool("VC_Lights_Hazards_Created", true) end
				elseif ent.VC_Lights_Hazards_Created then
				ent:SetNWBool("VC_Lights_Hazards_Created", false) ent.VC_Lights_Hazards_Created = nil
				end

				if Above and ent.VC_Blinker_Left_On and ent.VC_Blinker_OnTime then
				if !ent.VC_Lights_Blinker_Created_Left then ent.VC_Lights_Blinker_Created_Left = true ent:SetNWBool("VC_Lights_Blinker_Created_Left", true) end
				elseif ent.VC_Lights_Blinker_Created_Left then
				ent:SetNWBool("VC_Lights_Blinker_Created_Left", false) ent.VC_Lights_Blinker_Created_Left = nil
				end

				if Above and ent.VC_Blinker_Right_On and ent.VC_Blinker_OnTime then
				if !ent.VC_Lights_Blinker_Created_Right then ent.VC_Lights_Blinker_Created_Right = true ent:SetNWBool("VC_Lights_Blinker_Created_Right", true) end
				elseif ent.VC_Lights_Blinker_Created_Right then
				ent:SetNWBool("VC_Lights_Blinker_Created_Right", false) ent.VC_Lights_Blinker_Created_Right = nil
				end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			local On = ent.VC_Blinker_Left_On or ent.VC_Blinker_Right_On or ent.VC_HazardLightsOn
			if On and !ent.VC_Blinking then
			ent.VC_Blinker_OnTime = nil ent.VC_Blinker_OffTime = CurTime() ent.VC_Blinking = true
			elseif !On and ent.VC_Blinking then
			ent.VC_Blinker_OffTime = nil ent.VC_Blinker_OnTime = nil ent.VC_Blinking = nil
			end

			if ent.VC_Blinker_OnTime and CurTime() >= ent.VC_Blinker_OnTime then ent.VC_Blinker_OnTime = nil ent.VC_Blinker_OffTime = CurTime()+0.4 EmitSoundTS(ent, "vehicles/vc_blinkers_out.wav") elseif ent.VC_Blinker_OffTime and CurTime() >= ent.VC_Blinker_OffTime then ent.VC_Blinker_OffTime = nil ent.VC_Blinker_OnTime = CurTime()+0.4 EmitSoundTS(ent, "vehicles/vc_blinkers_in.wav") end
			elseif ent.VC_LightsInited then
			ent.VC_LightsInited = nil
			ent:SetNWBool("VC_Lights_Brk_Created", false) ent.VC_Lights_Brk_Created = nil
			ent:SetNWBool("VC_Lights_Rev_Created", false) ent.VC_Lights_Rev_Created = nil
			ent:SetNWBool("VC_Lights_Normal_Created", false) ent.VC_Lights_Normal_Created = nil
				VC_DeleteLights_Head(ent)
				ent:SetNWBool("VC_Lights_Head_Created", false) ent.VC_Lights_Head_Created = nil
			ent:SetNWBool("VC_Lights_Hazards_Created", false) ent.VC_Lights_Hazards_Created = nil
			ent:SetNWBool("VC_Lights_Blinker_Created_Left", false) ent.VC_Lights_Blinker_Created_Left = nil
			ent:SetNWBool("VC_Lights_Blinker_Created_Right", false) ent.VC_Lights_Blinker_Created_Right = nil
			end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		if ent.VC_HornOn and ent:WaterLevel() < 3 and IsValid(Drv) then
			if !ent.VC_HornSound or !ent.VC_HornSound:IsPlaying() then
			local HTbl = VC_HornTable and VC_HornTable[ent.VC_CurrentHorn] or ent.VehicleTable and ent.VehicleTable.VC_Horn ent.VC_HornTable = {Sound = HTbl and HTbl.Sound or "vehicles/vc_horn_light.wav", Pitch = HTbl and HTbl.Pitch or 100, Distance = HTbl and HTbl.Distance or 85, Volume = HTbl and HTbl.Volume or 1}
				if ent.VC_SirenOn and ent.VehicleTable and ent.VehicleTable.VC_Siren.UseQuick and ent.VehicleTable.VC_Siren.QuickSound then
				ent.VC_HornTable = {Sound = ent.VehicleTable.VC_Siren.QuickSound, Pitch = ent.VehicleTable.VC_Siren.Pitch or 100, Distance = ent.VehicleTable.VC_Siren.Distance or 85, Volume = ent.VehicleTable.VC_Siren.Volume or 1}
				end
			ent.VC_HornSound = VC_EmitSound(ent, ent.VC_HornTable.Sound, ent.VC_HornTable.Pitch, ent.VC_HornTable.Distance, ent.VC_HornTable.Volume, nil, true)
			end
		elseif ent.VC_HornSound and ent.VC_HornSound:IsPlaying() then
		ent.VC_HornSound:ChangeVolume(0, 0.01) timer.Simple(0.01, function() ent.VC_HornSound:Stop() end)
		end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		if VC_Settings.VC_Passenger_Seats then
			if ent.VehicleTable and ent.VehicleTable.VC_ExtraSeats and !ent.VC_SeatTable then
			ent.VC_SeatTable = {}
				for Stk, Stv in pairs(ent.VehicleTable.VC_ExtraSeats) do
				local Seat = ents.Create("prop_vehicle_prisoner_pod")
				Seat:SetModel("models/props_phx/carseat2.mdl")
				Seat:SetKeyValue("vehiclescript", "scripts/vehicles/prisoner_pod.txt") Seat:SetKeyValue("limitview", "0")
				Seat:SetAngles(VC_AngleCombCalc(ent:GetAngles(), Stv.Ang or Angle(0,0,0)))
				Seat:SetPos(VC_VectorToWorld(ent, Stv.Pos))
				Seat:SetParent(ent)
				Seat:SetNoDraw(true)
				Seat:DrawShadow(false)
				Seat:SetNotSolid(true)
				Seat:SetNWBool("VC_ExtraSt", true) Seat.VC_ExtraSeat = true
				Seat.VC_DrSnds = Stv.DoorSounds Seat.VC_RCntrl = Stv.RadioControl
				ent.VC_SeatTable[Stk] = Seat
				Seat:Spawn()
				Seat:SetNotSolid(true)
				end
			end
end

    if not EngAboveWtr(ent) then
        ent.VC_Health = 0
        ent.VC_EBroke = true
    end

		if ent.VC_Cruise and !ent.VC_BrksOn and EngAboveWtr(ent) and (!ent.VC_Health or ent.VC_Health >= ent.VC_MaxHealth/8) and (!IsValid(Drv) or !Drv:KeyDown(IN_JUMP)) then
			if IsValid(Drv) and (Drv:KeyDown(IN_FORWARD) or Drv:KeyDown(IN_BACK)) then
			ent.VC_CruiseKD = true
			else
			local CCVel = ent:GetVelocity():Dot(ent:GetForward())
				if CCVel < 5 then
				ent:Fire("throttle", 0) ent.VC_CruiseRan = nil ent.VC_Cruise = nil ent.VC_CruiseVel = nil ent:SetNWInt("VC_Cruise_Spd", 0)
				else
				if !ent.VC_CruiseVel or ent.VC_CruiseKD then CCVel = CCVel > 10 and CCVel or 10 ent.VC_CruiseVel = CCVel ent:SetNWInt("VC_Cruise_Spd", CCVel) ent.VC_CruiseKD = nil end
					if CCVel < ent.VC_CruiseVel then
					ent:Fire("throttle", VC_GetThrottle(ent.VC_CruiseVel, CCVel)) ent.VC_CruiseRan = true
					elseif !IsValid(Drv) and ent.VC_Throttle and ent.VC_Throttle != 0 then
					ent:Fire("throttle", 0) ent.VC_CruiseRan = true
					end
				end
			end
		elseif ent.VC_CruiseRan then
		ent:Fire("throttle", 0) ent.VC_CruiseRan = nil ent.VC_Cruise = nil ent.VC_CruiseVel = nil ent:SetNWInt("VC_Cruise_Spd", 0)
		end

		if VC_Settings.VC_Door_Sounds and ent.VC_ScanDoorCloseTime and CurTime() < ent.VC_ScanDoorCloseTime then
			local Tbl = {ent} table.Add(Tbl, table.Copy(ent.VC_SeatTable))
			for _, Seat in pairs(Tbl) do if (!Seat.VC_TBTET or CurTime() >= Seat.VC_TBTET) and !Seat.VC_TBTAC and Seat.VC_TBTDC and CurTime() >= Seat.VC_TBTDC then CarDoorOC(Seat, true) end end
		end

		if VC_Settings.VC_Exhaust_Effect and IsValid(Drv) and ent.VehicleTable and ent.VehicleTable.VC_Exhaust and ent:GetVelocity():Length() < 550 and EngAboveWtr(ent) then
		if IsValid(Drv) and (Drv:KeyDown(IN_FORWARD) or Drv:KeyDown(IN_BACK)) and !ent:GetDriver():KeyDown(IN_JUMP) and ent:GetVelocity():Length() < 450 then if !ent.VC_EEPES then ent.VC_EEPES = {} for _, HST in pairs(ent.VehicleTable.VC_Exhaust) do local WDE = ents.Create("info_particle_system") WDE:SetKeyValue("effect_name", "Exhaust") WDE:SetAngles(VC_AngleCombCalc(ent:GetAngles(), (HST.Ang or Angle(0,0,0))+Angle(90,0,0))) WDE:SetPos(VC_VectorToWorld(ent, HST.Pos) or Vector(0,0,0)) WDE:SetParent(ent) WDE:Spawn() WDE:Activate() WDE:Fire("Start") table.insert(ent.VC_EEPES, WDE) end end ent.VC_EETBACS = CurTime()+ math.Rand(0.2,0.4) else if (!ent.VC_EETBACS or CurTime() >= ent.VC_EETBACS) and ent.VC_EEPES then for _,HST in pairs(ent.VC_EEPES) do HST:Remove() end ent.VC_EEPES = nil end end
		if !ent.VC_EEPE then ent.VC_EEPE = {} for _, HST in pairs(ent.VehicleTable.VC_Exhaust) do local WDE = ents.Create("info_particle_system") WDE:SetKeyValue("effect_name", "Exhaust") WDE:SetAngles(VC_AngleCombCalc(ent:GetAngles(), (HST.Ang or Angle(0,0,0))+Angle(90,0,0))) WDE:SetPos(VC_VectorToWorld(ent, HST.Pos) or Vector(0,0,0)) WDE:SetParent(ent) WDE:Spawn() WDE:Activate() WDE:Fire("Start") table.insert(ent.VC_EEPE, WDE) end end
		else
		if (!ent.VC_EETBAC or CurTime() >= ent.VC_EETBAC) and ent.VC_EEPE then for _,HST in pairs(ent.VC_EEPE) do HST:Remove() end ent.VC_EEPE = nil end if ent.VC_EEPES then for _,HST in pairs(ent.VC_EEPES) do HST:Remove() end ent.VC_EEPES = nil end
		end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		if (!ent.VC_TBNWDC or CurTime() >= ent.VC_TBNWDC) and (!ent.VehicleTable or !ent.VehicleTable.VC_NoWheelDust) and string.lower(ent:GetClass()) != "prop_vehicle_prisoner_pod" and ent:GetClass() != "prop_vehicle_airboat" then
		local DEL = {"wheel_fl", "wheel_fr", "wheel_rl", "wheel_rr"}
			if VC_Settings.VC_Wheel_Dust then
			if !ent.VC_WDPST then ent.VC_WDPST = {} end if !ent.VC_WDNT then ent.VC_WDNT = {} end if !ent.VC_WDET then ent.VC_WDET = {} end local WDEA = {["65"] = "wheeldirt", ["66"] = "wheeldirt", ["68"] = "wheeldust", ["70"] = "wheeldirt", ["72"] = "wheeldirt", ["78"] = "wheeldust", ["79"] = "wheeldirt", ["80"] = "wheeldirt", ["83"] = "wheeldirt", ["87"] = "wheeldirt"}
			for i=1, 4 do if ent:LookupAttachment(DEL[i]) != 0 then local WDT = util.TraceLine({start = ent:GetAttachment(ent:LookupAttachment(DEL[i])).Pos, endpos = ent:GetAttachment(ent:LookupAttachment(DEL[i])).Pos+ ent:GetUp()* -30, filter = ent}) if WDT.Hit and ent:GetVelocity():Length()- WDT.Entity:GetVelocity():Length() > 250 and ent:WaterLevel() < 3 and VecAboveWtr(ent:GetPos()+ ent:GetUp()* 40) then if ent.VC_WDNT[i] and (WDT.MatType != ent.VC_WDET[i] or ent.VC_WDPST[i].VC_WDTBNC and CurTime() >= ent.VC_WDPST[i].VC_WDTBNC) then ent.VC_WDPST[i]:Remove() table.remove(ent.VC_WDPST, i) table.remove(ent.VC_WDNT, i) table.remove(ent.VC_WDET, i) end if !table.HasValue(ent.VC_WDNT, DEL[i]) then local WDE = ents.Create("info_particle_system") WDE:SetKeyValue("effect_name", !VecAboveWtr(WDT.HitPos+ WDT.HitNormal) and "wheelsplash" or WDEA[tostring(WDT.MatType)] or "exhaust") WDE:SetAngles(ent:GetAngles()) WDE:SetPos(WDT.HitPos+ WDT.HitNormal) WDE:SetParent(ent) WDE:Spawn() WDE:Activate() WDE:Fire("Start") table.insert(ent.VC_WDPST, WDE) table.insert(ent.VC_WDNT, DEL[i]) table.insert(ent.VC_WDET, WDT.MatType) WDE.VC_WDTBNC = CurTime()+ 0.5 end else if ent.VC_WDNT[i] then ent.VC_WDPST[i]:Remove() table.remove(ent.VC_WDPST, i) table.remove(ent.VC_WDNT, i) table.remove(ent.VC_WDET, i) end end end end
			else
			if ent.VC_WDNT and #ent.VC_WDNT > 0 then for WDk, WDv in pairs(ent.VC_WDNT) do ent.VC_WDPST[WDk]:Remove() table.remove(ent.VC_WDPST, WDk) table.remove(ent.VC_WDNT, WDk) table.remove(ent.VC_WDET, WDk) end end
			end
			if VC_Settings.VC_Wheel_Dust_Brakes and (IsValid(ent:GetDriver()) and ent:GetDriver():KeyDown(IN_JUMP) or ent.VC_BrksOn and IsValid(ent.VC_Truck) and IsValid(ent.VC_Truck:GetDriver()) and ent.VC_Truck:GetDriver():KeyDown(IN_JUMP)) then
			if !ent.VC_WDPSTB then ent.VC_WDPSTB = {} end if !ent.VC_WDNTB then ent.VC_WDNTB = {} end for i=1, 4 do if ent:LookupAttachment(DEL[i]) != 0 then local WDT = util.TraceLine({start = ent:GetAttachment(ent:LookupAttachment(DEL[i])).Pos, endpos = ent:GetAttachment(ent:LookupAttachment(DEL[i])).Pos+ ent:GetUp()* -30, filter = ent}) if WDT.Hit and ent:GetVelocity():Length()- WDT.Entity:GetVelocity():Length() > 100 and VecAboveWtr(WDT.HitPos+ WDT.HitNormal) and ent:WaterLevel() < 3 then if !table.HasValue(ent.VC_WDNTB, DEL[i]) then local WDE = ents.Create("info_particle_system") WDE:SetKeyValue("effect_name", "wheeldust") WDE:SetAngles(ent:GetAngles()) WDE:SetPos(WDT.HitPos+ WDT.HitNormal) WDE:SetParent(ent) WDE:Spawn() WDE:Activate() WDE:Fire("Start") table.insert(ent.VC_WDPSTB, WDE) table.insert(ent.VC_WDNTB, DEL[i]) end else if ent.VC_WDNTB[i] then ent.VC_WDPSTB[i]:Remove() table.remove(ent.VC_WDPSTB, i) table.remove(ent.VC_WDNTB, i) end end end end
			else
			if ent.VC_WDNTB and #ent.VC_WDNTB > 0 then for WDk, WDv in pairs(ent.VC_WDNTB) do ent.VC_WDPSTB[WDk]:Remove() table.remove(ent.VC_WDPSTB, WDk) table.remove(ent.VC_WDNTB, WDk) end end
			end
		ent.VC_TBNWDC = CurTime()+ 0.1
		end
	end
end)

function VC_EmitSound(ent, snd, pch, lvl, vol, pos, ntmr)
	if IsValid(ent) and snd then
	if snd == "Clk" then snd = "buttons/lightswitch2.wav" end
	util.PrecacheSound(snd)
	local VSnd = CreateSound(ent, snd) VSnd:SetSoundLevel(lvl or 60) VSnd:Stop() VSnd:Play() VSnd:ChangePitch(math.Clamp(pch or 100,1,255),0.01) VSnd:ChangeVolume(math.Clamp(vol or 1, 0,1), 0.01)
	if !ntmr then timer.Simple(SoundDuration(snd), function() if VSnd and VSnd:IsPlaying() then VSnd:Stop() end end) end
	return VSnd
	end
end

function VC_NormalLightsOn(ent) if !ent.VC_NormalLightsOn and ent.VC_HasNormalLights then EmitSoundTS(ent, "buttons/lightswitch2.wav", 105) ent.VC_NormalLightsOn = true end end
function VC_NormalLightsOff(ent) if ent.VC_NormalLightsOn and ent.VC_HasNormalLights then EmitSoundTS(ent, "buttons/lightswitch2.wav", 95) ent.VC_NormalLightsOn = false end end

function VC_HeadLightsOn(ent) if !ent.VC_HeadLightsOn and ent.VC_HasHeadLights then EmitSoundTS(ent, "buttons/lightswitch2.wav", 105) ent.VC_HeadLightsOn = true end end
function VC_HeadLightsOff(ent) if ent.VC_HeadLightsOn and ent.VC_HasHeadLights then EmitSoundTS(ent, "buttons/lightswitch2.wav", 95) ent.VC_HeadLightsOn = false end end

function VC_HazardLightsOn(ent) if !ent.VC_HazardLightsOn and ent.VC_HasBlinkerLights then ent.VC_Blinker_Right_On = false ent.VC_Blinker_Left_On = false ent.VC_HazardLightsOn = true ent:SetNWBool("VC_HazardLightsOn", true) EmitSoundTS(ent, "buttons/lightswitch2.wav", 105) end end
function VC_HazardLightsOff(ent) if ent.VC_HazardLightsOn and ent.VC_HasBlinkerLights then ent.VC_HazardLightsOn = false ent:SetNWBool("VC_HazardLightsOn", false) EmitSoundTS(ent, "buttons/lightswitch2.wav", 95) end end

for i=1, 8 do
	concommand.Add("VC_Switch_Seats_"..tostring(i+1), function(ply)
		if VC_Settings.VC_Enabled and IsValid(ply:GetVehicle()) and (ply:GetVehicle().VC_SeatTable or ply:GetVehicle().VC_ExtraSeat) and (!ply.VC_ChgST or CurTime() >= ply.VC_ChgST) and !(IsValid(ply.VC_HangingOutSeat) and IsValid(ply.VC_HangingOutSeat.VC_HangingOutPlayer) and ply == ply.VC_HangingOutSeat.VC_HangingOutPlayer) then
		local Seat = (ply:GetVehicle().VC_ExtraSeat and ply:GetVehicle():GetParent().VC_SeatTable or ply:GetVehicle().VC_SeatTable)[i]
			if IsValid(Seat) and Seat != ply:GetVehicle() and !IsValid(Seat:GetDriver()) then
			local ACA = Seat:WorldToLocalAngles(ply:EyeAngles()) ACA.r = 0
			ply.VC_ChgST = CurTime()+ 0.2 ply.VC_ChnSts = true ply:ExitVehicle() ply.VC_CanEnterTime = nil ply:EnterVehicle(Seat) ply:SetEyeAngles(ACA)
			end
		end
	end)
end

concommand.Add("VC_Inside_Doors_OnOff", function(ply)
	if VC_Settings.VC_Enabled and IsValid(ply:GetVehicle()) and !ply:GetVehicle().VC_ExtraSeat then
	if !ply:GetVehicle().VC_Locked then ply:ChatPrint("VCMod: Locking vehicle.") VC_Lock(ply:GetVehicle()) else ply:ChatPrint("VCMod: Unlocking vehicle.") VC_UnLock(ply:GetVehicle()) end
	EmitSoundTS(ply:GetVehicle(), "buttons/lightswitch2.wav", 100)
	end
end)

function VC_HornOn(ent)
	if !ent.VC_HornOn and ent.VC_IsNotPrisonerPod and VC_Settings.VC_Horn_Enabled then
	ent.VC_HornOn = true ent:SetNWBool("VC_HornOn", ent.VC_HornOn)
	end
end
function VC_HornOff(ent)
	if ent.VC_HornOn then
	ent.VC_HornOn = false ent:SetNWBool("VC_HornOn", ent.VC_HornOn)
	end
end

concommand.Add("VC_Horn_OnOff", function(ply, cmd, arg) if VC_Settings.VC_Enabled then local ent, HA = ply:GetVehicle(), tonumber(arg[1]) if IsValid(ent) and ent.VC_IsNotPrisonerPod then if HA == 1 and !ent.VC_HornOn then VC_HornOn(ent) elseif HA == 2 and ent.VC_HornOn then VC_HornOff(ent) end end end end)

concommand.Add("VC_Hazards_OnOff", function(ply)
	if VC_Settings.VC_Enabled and IsValid(ply:GetVehicle()) and (!ply.VC_CmdTmr or CurTime() >= ply.VC_CmdTmr) and ply:GetVehicle().VC_HasBlinkerLights then
	if !ply:GetVehicle().VC_HazardLightsOn then VC_HazardLightsOn(ply:GetVehicle()) else VC_HazardLightsOff(ply:GetVehicle()) end
	ply.VC_CmdTmr = CurTime()+ 0.1
	end
end)

concommand.Add("VC_Lights_OnOff", function(ply)
	if VC_Settings.VC_Enabled and (!ply.VC_CmdTmr or CurTime() >= ply.VC_CmdTmr) and ply:GetVehicle().VC_HasNormalLights then
	if !ply:GetVehicle().VC_NormalLightsOn then VC_NormalLightsOn(ply:GetVehicle()) else VC_NormalLightsOff(ply:GetVehicle()) end
	ply.VC_CmdTmr = CurTime()+ 0.1
	end
end)

concommand.Add("VC_Headlights_OnOff", function(ply)
	if VC_Settings.VC_Enabled and IsValid(ply:GetVehicle()) and (!ply.VC_CmdTmr or CurTime() >= ply.VC_CmdTmr) and ply:GetVehicle().VC_HasHeadLights then
	if !ply:GetVehicle().VC_HeadLightsOn then VC_HeadLightsOn(ply:GetVehicle()) else VC_HeadLightsOff(ply:GetVehicle()) end
	ply.VC_CmdTmr = CurTime()+ 0.1
	end
end)

concommand.Add("VC_Switch_Seats", function(ply)
	if VC_Settings.VC_Enabled and VC_Settings.VC_Passenger_Seats and IsValid(ply:GetVehicle()) and (!ply.VC_ChgST or CurTime() >= ply.VC_ChgST) and !(IsValid(ply.VC_HangingOutSeat) and IsValid(ply.VC_HangingOutSeat.VC_HangingOutPlayer) and ply == ply.VC_HangingOutSeat.VC_HangingOutPlayer) then
	local MVeh = ply:GetVehicle().VC_ExtraSeat and ply:GetVehicle():GetParent() or ply:GetVehicle()
	local STbl = table.Add({MVeh}, table.Copy(MVeh.VC_SeatTable))

	local SeatCount = table.Count(STbl)
		if table.Count(STbl) > 1 then
		local Seatk = 1 if MVeh != ply:GetVehicle() then for Stk, Stv in pairs(MVeh.VC_SeatTable) do if ply:GetVehicle() == Stv then Seatk = Stk+1 break end end end
		local SortedSeatTable = {} for Stk, Stv in pairs(STbl) do local SeatNum = Stk+Seatk if SeatNum > SeatCount then SeatNum = SeatNum-SeatCount end table.insert(SortedSeatTable, STbl[SeatNum]) end
			for Seatk, Seat in pairs(SortedSeatTable) do
				if !IsValid(Seat:GetDriver()) and !Seat.VC_AI_Driver then
				ply.VC_ChnSts = true ply.VC_SeatRCN = true local ACA = Seat:WorldToLocalAngles(ply:EyeAngles()) ACA.r = 0 ply:ExitVehicle() ply.VC_CanEnterTime = nil ply:EnterVehicle(Seat) ply:SetEyeAngles(ACA) ply.VC_ChnSts = false break
				end
			end
		end
	ply.VC_ChgST = CurTime()+ 0.5
	end
end)

concommand.Add("VC_Blinker_Left_Toggle", function(ply)
if VC_Settings.VC_Enabled and IsValid(ply:GetVehicle()) and (!ply.VC_CmdTmr or CurTime() >= ply.VC_CmdTmr) and string.lower(ply:GetVehicle():GetClass()) != "prop_vehicle_prisoner_pod" and !ply:GetVehicle().VC_HazardLightsOn and ply:GetVehicle().VC_HasBlinkerLights then
if ply:GetVehicle().VC_Blinker_Left_On then ply:GetVehicle().VC_Blinker_Right_On = false ply:GetVehicle().VC_Blinker_Left_On = false EmitSoundTS(ply:GetVehicle(), "buttons/lightswitch2.wav", 95) else ply:GetVehicle().VC_Blinker_Right_On = false ply:GetVehicle().VC_Blinker_Left_On = true EmitSoundTS(ply:GetVehicle(), "buttons/lightswitch2.wav", 105) end
ply.VC_CmdTmr = CurTime()+ 0.1 end
end)

concommand.Add("VC_Blinker_Right_Toggle", function(ply)
if VC_Settings.VC_Enabled and IsValid(ply:GetVehicle()) and (!ply.VC_CmdTmr or CurTime() >= ply.VC_CmdTmr) and string.lower(ply:GetVehicle():GetClass()) != "prop_vehicle_prisoner_pod" and !ply:GetVehicle().VC_HazardLightsOn and ply:GetVehicle().VC_HasBlinkerLights then
if ply:GetVehicle().VC_Blinker_Right_On then ply:GetVehicle().VC_Blinker_Right_On = false ply:GetVehicle().VC_Blinker_Left_On = false EmitSoundTS(ply:GetVehicle(), "buttons/lightswitch2.wav", 95) else ply:GetVehicle().VC_Blinker_Right_On = true ply:GetVehicle().VC_Blinker_Left_On = false EmitSoundTS(ply:GetVehicle(), "buttons/lightswitch2.wav", 105) end
ply.VC_CmdTmr = CurTime()+ 0.1 end
end)

concommand.Add("VC_Cruise_OnOff", function(ply) if VC_Settings.VC_Enabled and IsValid(ply:GetVehicle()) and (!ply.VC_CmdTmr or CurTime() >= ply.VC_CmdTmr) and string.lower(ply:GetVehicle():GetClass()) != "prop_vehicle_prisoner_pod" then if ply:GetVehicle().VC_Cruise then ply:GetVehicle().VC_Cruise = false EmitSoundTS(ply:GetVehicle(), "buttons/lightswitch2.wav", 95) elseif VC_Settings.VC_Cruise_Enabled then ply:GetVehicle().VC_Cruise = true EmitSoundTS(ply:GetVehicle(), "buttons/lightswitch2.wav", 105) end ply.VC_CmdTmr = CurTime()+ 0.1 end end)
concommand.Add("VC_Repair", function(ply) if VC_Settings.VC_Enabled and VC_Settings.VC_Exit_NoCollision and IsValid(ply:GetVehicle()) and (!ply:GetVehicle().VC_FixTmr or CurTime() >= ply:GetVehicle().VC_FixTmr) and ply:GetVehicle().VC_Health < ply:GetVehicle().VC_MaxHealth then ply:GetVehicle().VC_Health = ply:GetVehicle().VC_MaxHealth ply:GetVehicle():SetNWInt("VC_Health", ply:GetVehicle().VC_Health) EmitSoundTS(ply:GetVehicle(), "vehicles/vc_repair.wav", 95) end end)
concommand.Add("VC_Switch_Seats_1", function(ply) if VC_Settings.VC_Enabled and IsValid(ply:GetVehicle()) and ply:GetVehicle().VC_ExtraSeat and (!ply.VC_ChgST or CurTime() >= ply.VC_ChgST) and !(IsValid(ply.VC_HangingOutSeat) and IsValid(ply.VC_HangingOutSeat.VC_HangingOutPlayer) and ply == ply.VC_HangingOutSeat.VC_HangingOutPlayer)then local Seat = ply:GetVehicle():GetParent() if Seat != ply:GetVehicle() and !IsValid(Seat:GetDriver()) then ply.VC_ChnSts = true local ACA = Seat:WorldToLocalAngles(ply:EyeAngles()) ACA.r = 0 ply:ExitVehicle() ply.VC_CanEnterTime = nil ply:EnterVehicle(Seat) ply:SetEyeAngles(ACA) end end end)

function VC_CheckLights(ent)
	if ent.VehicleTable then
	ent.VC_LightTable = {}
		if ent.VehicleTable.VC_Lights then
		if !ent.VC_LightTable.Brake then ent.VC_LightTable.Brake = {} end
			for Lhtk, Lht in pairs(ent.VehicleTable.VC_Lights) do
			if Lht.BrakeColor then if !ent.VC_LightTable.Brake then ent.VC_LightTable.Brake = {} end ent.VC_LightTable.Brake[Lhtk] = table.Copy(Lht) end
			if Lht.ReverseColor then if !ent.VC_LightTable.Reverse then ent.VC_LightTable.Reverse = {} end ent.VC_LightTable.Reverse[Lhtk] = Lht end
			if Lht.HeadLightAngle then if !ent.VC_LightTable.Head then ent.VC_LightTable.Head = {} end ent.VC_LightTable.Head[Lhtk] = Lht end
			if Lht.NormalColor then if !ent.VC_LightTable.Normal then ent.VC_LightTable.Normal = {} end ent.VC_LightTable.Normal[Lhtk] = Lht end
			if Lht.BlinkersColor then if !ent.VC_LightTable.Blinker then ent.VC_LightTable.Blinker = {} end ent.VC_LightTable.Blinker[Lhtk] = Lht end
			end
		end
	end
end
