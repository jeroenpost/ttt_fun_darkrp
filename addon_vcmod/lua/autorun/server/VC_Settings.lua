// The following script is copyrighted material, written by freemmaann Steam URI: steamcommunity.com/id/freemmaann/, if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.

file.CreateDir("vcmod")

local Settings = {
VC_Enabled = true,
VC_Lights = true,
VC_HeadLights = false,
VC_Wheel_Lock = true,
VC_Brake_Lock = true,
VC_Door_Sounds = true,
VC_Wheel_Dust = true,
VC_Wheel_Dust_Brakes = true,
VC_Exit_Velocity = true,
VC_Exit_NoCollision = true,
VC_Exhaust_Effect = true,
VC_Damage = true,
VC_Passenger_Seats = true,
VC_Dmg_Fire_Duration = 30,
VC_Health_Multiplier = 1,
VC_LightsOffTime = 300,
VC_HLightsOffTime = 30,
VC_Horn_Enabled = true,
VC_Cruise_Enabled = true,
}

VC_Settings = VC_Settings or {}

function VC_ResetSettings_Cl() file.Write("vcmod/settings_sv_VCMod1.txt", util.TableToJSON(Settings)) end

function VC_LoadSettings_Cl() local Tbl = {} if file.Exists("vcmod/settings_sv_VCMod1.txt", "DATA") then Tbl = util.JSONToTable(file.Read("vcmod/settings_sv_VCMod1.txt", "DATA")) else Tbl = Settings end VC_Settings = table.Copy(Tbl) end

function VC_SaveSetting_Sv(k,v)
	if k and v != nil then
	local Tbl = {}
	if file.Exists("vcmod/settings_sv_VCMod1.txt", "DATA") then Tbl = util.JSONToTable(file.Read("vcmod/settings_sv_VCMod1.txt", "DATA")) else Tbl = Settings end
	Tbl[k] = v
	VC_Settings = Tbl
	file.Write("vcmod/settings_sv_VCMod1.txt", util.TableToJSON(Tbl))
	end
end

util.AddNetworkString("VC_SendToClient_Options")
function VC_GetSettings(ply) net.Start("VC_SendToClient_Options") net.WriteTable(VC_Settings) if ply then net.Send(ply) else net.Broadcast() end end

VC_LoadSettings_Cl()

concommand.Add("VC_GetSettings_Sv", function(ply, cmd, arg) if ply:IsAdmin() then VC_GetSettings(ply) end end)

util.AddNetworkString("VC_SendSettingsToServer")
net.Receive("VC_SendSettingsToServer", function(len)
	local ply, Tbl = net.ReadEntity(), net.ReadTable()
	if ply:IsAdmin() then
	VC_Settings = Tbl
	file.Write("vcmod/settings_sv_VCMod1.txt", util.TableToJSON(Tbl))
	end
end)