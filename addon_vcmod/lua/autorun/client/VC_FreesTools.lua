// The following script is copyrighted material, written by freemmaann Steam URI: steamcommunity.com/id/freemmaann/, if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.

local ConVarT = {["FT_PEScale"] = "1", ["FT_PEPos_x"] = "0", ["FT_PEPos_y"] = "0", ["FT_PEPos_z"] = "0"} for CVk,CVv in pairs(ConVarT) do if !ConVarExists(CVk) then CreateClientConVar(CVk, CVv, true, false) end end

hook.Add("Think", "PosEnittyThink", function()
	if IsValid(LocalPlayer().FT_GPEAEE) then
		if IsValid(LocalPlayer().FT_GPEAEE:GetParent()) then
		for Con,_ in pairs(ConVarT) do _G[Con] = GetConVarNumber(Con) end
		LocalPlayer().FT_GPEAEE:SetPos(LocalPlayer().FT_GPEAEE:GetParent():GetPos()+ LocalPlayer().FT_GPEAEE:GetParent():GetRight()* -FT_PEPos_y+ LocalPlayer().FT_GPEAEE:GetParent():GetForward()* FT_PEPos_x+ LocalPlayer().FT_GPEAEE:GetParent():GetUp()* FT_PEPos_z)
		LocalPlayer().FT_GPEAEE:SetModelScale(FT_PEScale, 0)
		else
		LocalPlayer().FT_GPEAEE:Remove()
		end
	end
end)

concommand.Add("FT_PECreateRem", function(ply)
	if IsValid(ply:GetVehicle()) then
		if !IsValid(ply.FT_GPEAEE) then
		local veh = ply:GetVehicle():GetNWBool("VC_ExtraSt") and ply:GetVehicle():GetParent() or ply:GetVehicle()
		ply.FT_GPEAEE = ClientsideModel("models/combine_helicopter/helicopter_bomb01.mdl", RENDERGROUP_OPAQUE)
		ply.FT_GPEAEE:SetModelScale(GetConVarNumber("FT_PEScale"), 0)
		ply.FT_GPEAEE:SetPos(veh:GetPos()+ veh:GetRight()* -GetConVarNumber("FT_PEPos_y")+ veh:GetForward()* GetConVarNumber("FT_PEPos_x")+ veh:GetUp()* GetConVarNumber("FT_PEPos_z"))
		ply.FT_GPEAEE:SetAngles(veh:GetAngles())
		ply.FT_GPEAEE:SetParent(veh)
		else
		ply.FT_GPEAEE:Remove()
		end
	else
	LocalPlayer():ChatPrint("Get inside a car first.")
	end
end)

concommand.Add("FT_PEPrintPos", function(ply)
LocalPlayer():ChatPrint("Vector("..tostring(math.Round(GetConVarNumber("FT_PEPos_x")*10)/10)..", "..tostring(math.Round(GetConVarNumber("FT_PEPos_y")*10)/10)..", "..tostring(math.Round(GetConVarNumber("FT_PEPos_z")*10)/10)..")")
end)

concommand.Add("FT_TraceEnt", function(ply)
local tr = LocalPlayer():GetEyeTraceNoCursor()
	if IsValid(tr.Entity) then
	local vec = tr.Entity:WorldToLocal(tr.HitPos)
	LocalPlayer():ChatPrint("Vector("..tostring(math.Round(vec.x* 10)/10)..", "..tostring(math.Round(vec.y* 10)/10)..", "..tostring(math.Round(vec.z* 10)/10)..")")
	else
	LocalPlayer():ChatPrint("Look at an entity first")
	end
end)

concommand.Add("GetAttachPosition", function(ply, cmd, arg)
	if IsValid(ply:GetVehicle()) then
		if ply:GetVehicle():GetVelocity():Length() <= 10 then
		local Rg = arg[1]
			if Rg then
				if ply:GetVehicle():LookupAttachment(Rg) != 0 then
				local vec = ply:GetVehicle():WorldToLocal(ply:GetVehicle():GetAttachment(ply:GetVehicle():LookupAttachment(Rg)).Pos)
				LocalPlayer():ChatPrint("Vector("..tostring(math.Round(vec.x*10)/10)..", "..tostring(math.Round(vec.y*10)/10)..", "..tostring(math.Round(vec.z*10)/10)..")")
				else
				LocalPlayer():ChatPrint("The attachment is not valid.")
				end
			else
			LocalPlayer():ChatPrint("Write in the name of the attachment you want.")
			end
		else
		LocalPlayer():ChatPrint("Stop the car for the best results.")
		end
	else
	LocalPlayer():ChatPrint("Get inside a car first.")
	end
end)

hook.Add("PopulateToolMenu", "PEnt_Menu", function()
	spawnmenu.AddToolMenuOption("Utilities", "VCMod", "Positioning", "Positioning", "", "", function(Pnl)
	Pnl:AddControl("Label", {Text = "Attachments"})
	local AName, ABtn = vgui.Create("DTextEntry", Pnl), vgui.Create("DButton", Pnl) AName:SetTall(20) Pnl:AddItem(AName) Pnl:AddItem(ABtn) ABtn:SetText("Get Attachment Vector") ABtn.DoClick = function() RunConsoleCommand("GetAttachPosition", AName:GetValue()) end
	Pnl:AddControl("Label", {Text = ""})
	Pnl:AddControl("Button", {Label = "Create/Remove Positioning Entity", Command = "FT_PECreateRem"})
	Pnl:AddControl("Button", {Label = "Print Entitys Position", Command = "FT_PEPrintPos"})
	Pnl:AddControl("Button", {Label = "Print Trace HitPos", Command = "FT_TraceEnt"})
	Pnl:AddControl("Slider", {Label = "Entity's Scale", Command = "FT_PEScale", Type = "Float", min = 0.01, max = 1})
	Pnl:AddControl("Slider", {Label = "Entity's Position x", Command = "FT_PEPos_x", Type = "Float", min = -150, max = 150})
	Pnl:AddControl("Slider", {Label = "Entity's Position y", Command = "FT_PEPos_y", Type = "Float", min = -150, max = 150})
	Pnl:AddControl("Slider", {Label = "Entity's Position z", Command = "FT_PEPos_z", Type = "Float", min = -150, max = 150})
	end)
end)