// The following script is copyrighted material, written by freemmaann Steam URI: steamcommunity.com/id/freemmaann/, if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.

list.Set("DesktopWindows", "VCMod", {title = "VCMod Menu", icon = "icon16/car.png", init = function(icon, window) window:Close() RunConsoleCommand("vc_open_menu") end})

VCMod1 = true

function VCMsg(msg) if type(msg) != table then msg = {msg} end for _, PM in pairs(msg) do if type(PM) != string then PM = tostring(PM) end player.GetAll()[1]:ChatPrint(PM) end end
function VCCMsg(msg) chat.AddText(Color(0, 255, 0), "VCMod: "..tostring(msg)) end

function VC_EaseInOut(num) return (math.sin(math.pi*(num-0.5))+1)/2 end

function VC_FTm() local FTm = FrameTime()*100 return FTm end 

concommand.Add("vc_getparticles", function(ply, cmd, arg) ply:ChatPrint('VCMod: Particle effects are not bundled with VCMod for legal matters.\nYou can still get particle effects from here: "http://www.filedropper.com/vcmodparticles".') end)

local function CheckForEffects()
	if !file.Exists("models/vehicle.mdl", "GAME") and !file.Exists("particles/vehicle.pcf", "GAME") then
	GAMEMODE:AddNotify('VCMod: you appear to be missing particle effects, enter "vc_getparticles" in console to get them.', 1, 6)
	end
end

timer.Create("vcmod_pcheck", 30, 4, function() CheckForEffects() end)
timer.Simple(10, function() CheckForEffects() end)

game.AddParticles("particles/vehicle.pcf")
game.AddParticles("particles/weapon_fx.pcf")

local function VectorInBounds(BNum, vec1, vec2, ent) local VB1x, VB1y, VB1z, VB2x, VB2y, VB2z = vec1.x, vec1.y, vec1.z, vec2.x, vec2.y, vec2.z if VB1x > VB2x- BNum and VB1x < VB2x+ BNum and VB1y > VB2y- BNum and VB1y < VB2y+ BNum and VB1z > VB2z- BNum and VB1z < VB2z+ BNum then if ent and ent.VC_CTPVIBC then ent.VC_CTPVIBC = false end return true end end
local function AngleInBounds(BNum, ang1, ang2) local AB1p, AB1y, AB1r, AB2p, AB2y, AB2r = ang1.p, ang1.y, ang1.r, ang2.p, ang2.y, ang2.r if AB1p > AB2p- BNum and AB1p < AB2p+ BNum and AB1y > AB2y- BNum and AB1y < AB2y+ BNum and AB1r > AB2r- BNum and AB1r < AB2r+ BNum then return true end end

function VC_MakeScripts() end

hook.Add("ShouldDrawLocalPlayer", "VC_ShouldDrawLocalPlayer", function(ply) if VC_Settings.VC_Enabled and IsValid(ply:GetVehicle()) and ply:GetVehicle():GetThirdPersonMode() then return true end end)

VC_Global_Data = VC_Global_Data or {}

hook.Add("InitPostEntity", "VC_InitPE_Cl", function()
	usermessage.Hook("VCMsg", function(dt) VCCMsg(dt:ReadString()) end)
	net.Receive("VC_SendToClient_Options", function(len) local Tbl = net.ReadTable() VC_Settings_TempTbl = Tbl end)
	net.Receive("VC_SendToClient_Model", function(len) local ent, mdl = net.ReadEntity(), net.ReadString() if IsValid(ent) then ent.VC_Model = mdl end end)
	net.Receive("VC_SendToClient_Lights", function(len) local mdl, kill = net.ReadString(), net.ReadString() if kill == "A" then VC_Global_Data[mdl] = nil else local type, key, table = net.ReadString(), net.ReadInt(32), net.ReadTable() if !VC_Global_Data[mdl] then VC_Global_Data[mdl] = {LightTable = {}} end if !VC_Global_Data[mdl].LightTable[type] then VC_Global_Data[mdl].LightTable[type] = {} end VC_Global_Data[mdl].LightTable[type][key] = table end end)
end)

hook.Add("CalcView", "VC_ViewCalc", function(ply, pos, ang, fov)
	if !ply.VC_TPRCU then ply.VC_TPRCU = true end
	local FTm = FrameTime()*100 if FTm < 1 then FTm = 1 end
	if VC_Settings.VC_Enabled then
	VC_TPDV = math.Clamp(VC_Settings.VC_ThirdPerson_Vec_Stf, 0, 100) VC_TPDA = math.Clamp(VC_Settings.VC_ThirdPerson_Ang_Stf, 0, 100)
		if IsValid(ply:GetVehicle()) and ply:GetVehicle():WaterLevel() < 3 then
			if VC_Settings.VC_Keyboard_Input and !vgui.CursorVisible() then
			if !ply.VC_KeyRPst and input.IsKeyDown(KEY_R) then RunConsoleCommand("VC_Horn_OnOff", "1") ply.VC_KeyRPst = true elseif ply.VC_KeyRPst and !input.IsKeyDown(KEY_R) then RunConsoleCommand("VC_Horn_OnOff", "2") ply.VC_KeyRPst = nil end			
			if !ply.VC_KeyBPst and input.IsKeyDown(KEY_B) then RunConsoleCommand("VC_Cruise_OnOff") ply.VC_KeyBPst = true elseif ply.VC_KeyBPst and !input.IsKeyDown(KEY_B) then ply.VC_KeyBPst = nil end
			local Key = input.IsKeyDown(KEY_COMMA) and input.IsKeyDown(KEY_LALT) if !ply.VC_KeyAltPrPst and Key then RunConsoleCommand("VC_Blinker_Left_Toggle") ply.VC_KeyAltPrPst = true elseif ply.VC_KeyAltPrPst and !Key then ply.VC_KeyAltPrPst = nil end
			local Key = input.IsKeyDown(KEY_PERIOD) and input.IsKeyDown(KEY_LALT) if !ply.VC_KeyAltPrPer and Key then RunConsoleCommand("VC_Blinker_Right_Toggle") ply.VC_KeyAltPrPer = true elseif ply.VC_KeyAltPrPer and !Key then ply.VC_KeyAltPrPer = nil end
			local Key = input.IsKeyDown(KEY_SLASH) and input.IsKeyDown(KEY_LALT) if !ply.VC_KeyAltPrSlsh and Key then RunConsoleCommand("VC_Hazards_OnOff") ply.VC_KeyAltPrSlsh = true elseif ply.VC_KeyAltPrSlsh and !Key then ply.VC_KeyAltPrSlsh = nil end
			local Key = input.IsKeyDown(KEY_V) and input.IsKeyDown(KEY_LALT) if !ply.VC_KeyAltPrV and Key then RunConsoleCommand("VC_Inside_Doors_OnOff") ply.VC_KeyAltPrV = true elseif ply.VC_KeyAltPrV and !Key then ply.VC_KeyAltPrV = nil end
			if !ply.VC_KeyFDT and input.IsKeyDown(KEY_F) then ply.VC_KeyFDT = CurTime() ply.VC_APLS = true elseif ply.VC_KeyFDT and !input.IsKeyDown(KEY_F) then if CurTime() < ply.VC_KeyFDT+ 0.2 then RunConsoleCommand("VC_Headlights_OnOff") else if ply.VC_APLS then RunConsoleCommand("VC_Lights_OnOff") end end ply.VC_KeyFDT = nil ply.VC_APLS = nil end
			if ply.VC_APLS and ply.VC_KeyFDT and CurTime() >= ply.VC_KeyFDT+ 0.2 then if (ply:KeyDown(IN_MOVELEFT) or ply:KeyDown(IN_MOVERIGHT)) and !ply:GetVehicle():GetNWBool("VC_HazLOn") then else RunConsoleCommand("VC_Lights_OnOff") end ply.VC_APLS = nil end
				if (!ply.VC_UpKeysPressTime or CurTime() >= ply.VC_UpKeysPressTime) then
					if input.IsKeyDown(KEY_LALT) then
						if !ply:GetVehicle().VC_ExtraSeat then
						for i=1, 10 do local Ib = (i == 10 and 0 or i) if input.IsKeyDown(_G["KEY_"..Ib]) then RunConsoleCommand("VC_ClearSeat", Ib) ply.VC_UpKeysPressTime = CurTime()+0.2 end end
						end
					else
					for i=1, 10 do local Ib = (i == 10 and 0 or i) if input.IsKeyDown(_G["KEY_"..Ib]) then if Ib > 0 then RunConsoleCommand("VC_Switch_Seats_"..Ib) else RunConsoleCommand("VC_Switch_Seats") end ply.VC_UpKeysPressTime = CurTime()+0.2 end end
					end
				end
			end
		else
		if ply.VC_Radio then ply.VC_Radio:Remove() ply.VC_CrntSt = nil ply.VC_Radio = nil end
		end
	elseif ply.VC_Radio then ply.VC_Radio:Remove() ply.VC_CrntSt = nil ply.VC_Radio = nil
	end

	if VC_Settings.VC_Enabled and IsValid(ply:GetVehicle()) then
		if !VC_PrintedChatMsg and (ply:GetVehicle():GetClass() != "prop_vehicle_prisoner_pod" or ply:GetVehicle():GetNWBool("VC_ExtraSt")) then
		LocalPlayer():PrintMessage(HUD_PRINTTALK, 'VCMod: To adjust options type "vc_open_menu" in console.')
		VC_PrintedChatMsg = true
		end

	local Veh, MMvd = ply:GetVehicle():GetNWBool("VC_ExtraSt") and ply:GetVehicle():GetParent() or ply:GetVehicle(), false
if !Veh.VC_Initialized then VC_Initialize(Veh) Veh.VC_Initialized = true end
		if ply:ShouldDrawLocalPlayer() and ply:GetVehicle():GetThirdPersonMode() and GetViewEntity() == LocalPlayer() then
			if IsValid(Veh) and (!Veh.VC_IsPrisonerPod or Veh:GetNWBool("VC_ExtraSt")) then
				if VC_Settings.VC_ThirdPerson_Dynamic and VC_TPDA < 100 or VC_Settings.VC_ThirdPerson_Auto or VC_Settings.VC_MouseControl then if !ply.VC_LEASBC or ply:EyeAngles() != ply.VC_LEASBC then ply.VC_CnstAV = (ply.VC_CnstAV or ply:EyeAngles())+ (ply:EyeAngles()- (ply.VC_LEASBC or ply:EyeAngles())) ply.VC_LEASBC = ply.VC_CnstAV MMvd = true end if MMvd or !vgui.CursorVisible() and (input.IsMouseDown(MOUSE_LEFT) or input.IsMouseDown(MOUSE_RIGHT) or input.IsMouseDown(MOUSE_MIDDLE)) then ply.VC_LEAACT = CurTime() ply.VC_LEAACR = math.random(4.5, 6) end else if ply.VC_CnstAV then ply.VC_CnstAV = nil ply.VC_LEASBC = nil end end
				if VC_Settings.VC_ThirdPerson_Auto and !Veh.VC_IsPrisonerPod and VC_Settings.VC_ThirdPerson_Ang_Stf > 0 and CurTime() >= ply.VC_LEAACT+ (Veh:GetVelocity():Length() >= 50 and 1.5 or ply.VC_LEAACR) then local FAng = VC_Settings.VC_ThirdPerson_Auto_Back and Veh:GetVelocity():Dot(Veh:GetRight()) > 150 and -90 or 90 if !AngleInBounds(0.1, ply.VC_CnstAV or ply:EyeAngles(), Angle(VC_Settings.VC_ThirdPerson_Auto_Pitch,FAng,0)) then local CAng = LerpAngle((0.0075+ math.Clamp(Veh:GetVelocity():Length()/(FAng > 0 and 20000 or 10000), 0, 0.0775))*FTm, ply.VC_CnstAV, (Veh:GetAngles()- ply:GetVehicle():GetAngles())+ Angle(VC_Settings.VC_ThirdPerson_Auto_Pitch,FAng,0)) local PAng = ply:EyeAngles()- (ply.VC_CnstAV- CAng) ply:SetEyeAngles(Angle(PAng.p, PAng.y, ply:EyeAngles().r)) ply.VC_CnstAV = Angle(CAng.p, CAng.y, ply:EyeAngles().r) ply.VC_LEASBC = ply:EyeAngles() ply.VC_LEAACT = 1 end end
				if VC_Settings.VC_ThirdPerson_Dynamic and (VC_TPDA < 100 or VC_TPDV < 100) or VC_Settings.VC_MouseControl or VC_Settings.VC_ThirdPerson_Cam_World then
				local View, Fltr, APos, CFOV, APVD = {}, Veh, pos, fov, 250
				if !Veh.VC_CVTPVOU then local WSmin, WSmax = Veh:WorldSpaceAABB() local Size = WSmax - WSmin Veh.VC_CVTPVOO = (Size.x + Size.y + Size.z)* 0.31 Veh.VC_CVTPVOU = Size.z end APos = Veh:GetPos()+ ang:Up()* Veh.VC_CVTPVOU* 0.645 APVD = Veh.VC_CVTPVOO
				if VC_Settings.VC_MouseControl then if input.IsMouseDown(MOUSE_MIDDLE) and !vgui.CursorVisible() then if !ply.VC_TPVLB and VC_Settings.VC_ThirdPerson_Ang_Stf > 0 then ply.VC_TPVLB = ply.VC_CnstAV or ply:EyeAngles() ply.VC_TPVEAS = (Veh:GetAngles()- ply:GetVehicle():GetAngles())+ Angle(0,-90,0) end else if ply.VC_TPVLB then ply.VC_TPVEAS = ply.VC_TPVLB ply.VC_TPVLB = nil end end if ply.VC_TPVEAS then if !MMvd and !AngleInBounds(0.3, ply.VC_CnstAV or ply:EyeAngles(), ply.VC_TPVEAS) then local CAng = LerpAngle(0.3*FTm, ply.VC_CnstAV or ply:EyeAngles(), ply.VC_TPVEAS) local PAng = ply:EyeAngles()- (ply.VC_CnstAV- CAng) ply:SetEyeAngles(Angle(PAng.p, PAng.y, ply:EyeAngles().r)) ply.VC_CnstAV = Angle(CAng.p, CAng.y, ply:EyeAngles().r) ply.VC_LEASBC = ply:EyeAngles() else ply.VC_TPVEAS = nil end end APVD = APVD* (ply.VC_TPVDM or 1) end
				if VC_Settings.VC_ThirdPerson_Dynamic then
				if VC_TPDA < 100 then local TAng = LerpAngle(VC_TPDA/100*FTm, ((ply.VC_CCAng or Veh:GetAngles())- Veh:GetAngles())+ ply:EyeAngles(), (ply.VC_CnstAV or ply:EyeAngles())) ply:SetEyeAngles(Angle(VC_Settings.VC_ThirdPerson_Ang_Pitch and TAng.p or ply:EyeAngles().p, TAng.y, ply:EyeAngles().r)) ply.VC_CCAng = Veh:GetAngles() ply.VC_LEASBC = ply:EyeAngles() else if ply.VC_CCAng then ply:SetEyeAngles(ply.VC_CnstAV or ply:EyeAngles()) ply.VC_CCAng = nil end end
				if VC_TPDV < 100 then APos = LerpVector(VC_TPDV/100*FTm, ply.VC_CnstVV or APos, APos) ply.VC_CnstVV = APos else ply.VC_CnstVV = nil end
				ply.VC_TPVDC = ply.VC_TPVDC or APVD*1.02
				else
				if ply.VC_CCAng then ply:SetEyeAngles(ply.VC_CnstAV or ply:EyeAngles()) ply.VC_CCAng = nil end ply.VC_CnstVV = nil
				end
					ply.VC_TPVDC = ply.VC_TPVDC or APVD if ply.VC_TPVDC > APVD+ 0.05 or ply.VC_TPVDC < APVD- 0.05 then ply.VC_TPVDC = Lerp(0.05*FTm, ply.VC_TPVDC, APVD) end
					local TTTr = util.TraceLine({start = APos, endpos = APos+ ang:Forward()* -ply.VC_TPVDC, filter = Fltr, mask = VC_Settings.VC_ThirdPerson_Cam_World and MASK_NPCWORLDSTATIC or nil})
					View.origin = TTTr.HitPos+ TTTr.HitNormal* (3+ ply.VC_TPVDC/600)
					return View
				end
			end
			elseif ply.VC_TPRCU then
		ply.VC_LEAACT = nil ply.VC_TPTCAS = nil ply.VC_TPVDC = nil ply.VC_CCAng = nil ply.VC_LEASBC = nil if ply.VC_CnstAV and IsValid(ply:GetVehicle()) then ply:SetEyeAngles(ply.VC_CnstAV) end ply.VC_CnstVV = nil ply.VC_CnstAV = nil ply.VC_TPRCU = false
		end
	elseif ply.VC_TPRCU then
	ply.VC_LEAACT = nil ply.VC_TPTCAS = nil ply.VC_TPVDC = nil ply.VC_CCAng = nil ply.VC_LEASBC = nil if ply.VC_CnstAV and IsValid(ply:GetVehicle()) then ply:SetEyeAngles(ply.VC_CnstAV) end ply.VC_CnstVV = nil ply.VC_CnstAV = nil ply.VC_TPRCU = false
	end
end)

hook.Add("PlayerBindPress", "VC_BindPress", function(ply, bind)
	if VC_Settings.VC_Enabled and !vgui.CursorVisible() then
		if VC_Settings.VC_MouseControl and IsValid(ply:GetVehicle()) and (!ply:GetVehicle().VC_IsPrisonerPod or ply:GetVehicle():GetNWBool("VC_ExtraSt")) and ply:ShouldDrawLocalPlayer() and GetViewEntity() == LocalPlayer() then
		local InK = 0 if string.find(bind, "invprev") then InK = -0.1 elseif string.find(bind, "invnext") then InK = 0.1 end
		ply.VC_TPVDM = math.Clamp((ply.VC_TPVDM or 1)+ InK, 1, 2.5)
		end
		if !IsValid(ply:GetVehicle()) and string.find(bind, "use") and (!ply.VC_CheckVehicleEnter or CurTime() >= ply.VC_CheckVehicleEnter) then
			local tr = ply:GetEyeTraceNoCursor()
			if IsValid(tr.Entity) and tr.Entity:IsVehicle() and ply:GetShootPos():Distance(tr.HitPos) <= 75 then
			net.Start("VC_PlayerScanForSeats")
			net.WriteEntity(ply)
			net.WriteEntity(tr.Entity)
			net.WriteVector(tr.HitPos)
			net.SendToServer()
			end
		ply.VC_CheckVehicleEnter = CurTime()+0.5
		end
	end
end)

local function Handle_Light_Draw_Single(ent, Lhk, Lhv, col, colid, sideid, distnum)
	if !ent.VC_DamagedLights or !ent.VC_DamagedLights[Lhk] then
	local SPos = Lhv.Pos or Vector(0,0,0)
		if !sideid or (sideid == 1 and SPos.x < 0 or sideid == 2 and SPos.x > 0) then
		local LSize = Lhv.HeadLightAngle and 1.1 or Lhv.Size or 1
		local Col = string.Explode(" ", Lhv[col] or "225 225 255")
			if !ent.VC_Lights_PixVisTbl then ent.VC_Lights_PixVisTbl = {} end if !ent.VC_Lights_PixVisTbl[Lhk] then ent.VC_Lights_PixVisTbl[Lhk] = util.GetPixelVisibleHandle() end
			Pos = ent:LocalToWorld(SPos)
			local Vis = util.PixelVisible(Lhv.DLPos and ent:LocalToWorld(Lhv.DLPos) or Pos, 3, ent.VC_Lights_PixVisTbl[Lhk])*255
				if Vis > 0 then
				local Mat = "sprites/light_ignorez" if !ent.VC_Lights_MatTbl then ent.VC_Lights_MatTbl = {} end if !ent.VC_Lights_MatTbl[Mat] then ent.VC_Lights_MatTbl[Mat] = Material(Mat) end
				render.SetMaterial(ent.VC_Lights_MatTbl[Mat])

				local Size = LSize*100*distnum

				if !Lhv.SpecInit then
					if Lhv.SpecLine and Lhv.SpecLine.Use then
					if !Lhv.SpecTable then Lhv.SpecTable = {} end
					if Lhv.SpecLine.Amount and Lhv.SpecLine.Amount > 2 then local Am = Lhv.SpecLine.Amount-2 for i=1,Am do Lhv.SpecTable[i] = LerpVector(i/(Am+1), SPos, Lhv.SpecLine.Pos or Vector(0,0,0)) end end
					local Cnt = table.Count(Lhv.SpecTable) Lhv.SpecTable[Cnt+1] = Lhv.Pos or Vector(0,0,0) Lhv.SpecTable[Cnt+2] = Lhv.SpecLine.Pos or Vector(0,0,0) Lhv.DLPos = (Lhv.SpecLine.Pos+Lhv.Pos)/2
					end

					if Lhv.SpecCircle and Lhv.SpecCircle.Use then
					if !Lhv.SpecTable then Lhv.SpecTable = {} end
					local Am, SAm = Lhv.SpecCircle.Amount or 3, table.Count(Lhv.SpecTable) for i=1,Am do local TVec = Vector(Lhv.SpecCircle.Radius or 1,0,0) TVec:Rotate(Angle(i/Am*360, 0, 0)) Lhv.SpecTable[SAm+i] = SPos+TVec end
					end
					
					if Lhv.SpecRec and Lhv.SpecRec.Use then
					if !Lhv.SpecTable then Lhv.SpecTable = {} end local SAm = table.Count(Lhv.SpecTable)
					local Pos1, Pos2, Pos3, Pos4, AmH, AmV = Lhv.SpecRec.Pos1, Lhv.SpecRec.Pos2, Lhv.SpecRec.Pos3, Lhv.SpecRec.Pos4, (Lhv.SpecRec.AmountH or 2)-2, (Lhv.SpecRec.AmountV or 2)-2
					Lhv.SpecTable[SAm+1] = Lhv.SpecRec.Pos1 Lhv.SpecTable[SAm+2] = Lhv.SpecRec.Pos2 Lhv.SpecTable[SAm+3] = Lhv.SpecRec.Pos3 Lhv.SpecTable[SAm+4] = Lhv.SpecRec.Pos4
					SAm = table.Count(Lhv.SpecTable) for i=1, AmH do Lhv.SpecTable[SAm+i] = LerpVector(i/(AmH+1), Pos1, Pos2) end
					SAm = table.Count(Lhv.SpecTable) for i=1, AmV do Lhv.SpecTable[SAm+i] = LerpVector(i/(AmV+1), Pos2, Pos3) end
					SAm = table.Count(Lhv.SpecTable) for i=1, AmH do Lhv.SpecTable[SAm+i] = LerpVector(i/(AmH+1), Pos3, Pos4) end
					SAm = table.Count(Lhv.SpecTable) for i=1, AmV do Lhv.SpecTable[SAm+i] = LerpVector(i/(AmV+1), Pos4, Pos1) end
					end
				Lhv.SpecInit = true
				end

				if Lhv.SpecTable then
				for i=1, table.Count(Lhv.SpecTable) do render.DrawSprite(ent:LocalToWorld(Lhv.SpecTable[i]), Size, Size, Color(Col[1], Col[2], Col[3], Vis)) end
				else
				render.DrawSprite(Pos, Size, Size, Color(Col[1], Col[2], Col[3], Vis))
				end
			end

			if VC_Settings.VC_DynamicLights and Lhv.DynLight then
			local DLight = DynamicLight(ent:EntIndex()..Lhk..colid)
			DLight.Pos = ent:LocalToWorld(Lhv.DLPos or SPos)
			DLight.r = Col[1] DLight.g = Col[2] DLight.b = Col[3]
			DLight.Brightness = (Lhv.DynLightBrightness or ((LSize*1)/2))*distnum
			DLight.Size = 180*((Lhv.DynamicSize or (LSize*0.5)))*distnum
			DLight.Decay = 1000
			DLight.DieTime = CurTime()+0.1
			end
		end
	end
end

local function Handle_Light_Draw_Multi(ent, table, col, colid, sideid, distnum) for Lhk, Lhv in pairs(table) do Handle_Light_Draw_Single(ent, Lhk, Lhv, col, colid, sideid, distnum) end end

function VC_Initialize(ent)
	timer.Simple(2, function()
		if IsValid(ent) then
		ent.VC_Class = string.lower(ent:GetClass())
		ent.VC_IsJeep = ent.VC_Class == "prop_vehicle_jeep"
		ent.VC_IsPrisonerPod = ent.VC_Class == "prop_vehicle_prisoner_pod"
		ent.VC_IsAirboat = ent.VC_Class == "prop_vehicle_airboat"
		ent.VC_ExtraSeat = ent.VC_IsPrisonerPod and ent:GetNWBool("VC_ExtraSt")

		local Atc = ent:LookupAttachment("vehicle_driver_eyes") if Atc != "0" then ent.VC_EyePos = (ent.VC_ExtraSeat and ent:GetParent() or ent):WorldToLocal(ent:GetAttachment(Atc).Pos) ent.VC_LeftSteer = ent.VC_EyePos.x < 0 end
		end
	end)
end

hook.Add("PostDrawTranslucentRenderables", "VC_PostDrawTranslucentRenderables", function()
	for _, ent in pairs(ents.FindByClass("prop_vehicle_jeep*")) do
		if (ent:GetNWInt("VC_MaxHealth") == 0 or ent:GetNWInt("VC_Health") > 0) and VC_Global_Data[ent.VC_Model] then
		local LhtTbl = VC_Global_Data[ent.VC_Model].LightTable

			if !ent.VC_Lht_DstCheckT or CurTime() >= ent.VC_Lht_DstCheckT then if !ent.VC_Lht_DstCheckMult then ent.VC_Lht_DstCheckMult = 1 end ent.VC_Lht_DstCheck = LocalPlayer():GetPos():Distance(ent:GetPos()) ent.VC_Lht_DstCheck = ent.VC_Lht_DstCheck < VC_Settings.VC_LightDistance ent.VC_Lht_DstCheckT = CurTime()+0.5 end
				if ent.VC_Lht_DstCheck then
				if ent.VC_Lht_DstCheckMult < 1 then ent.VC_Lht_DstCheckMult = ent.VC_Lht_DstCheckMult+0.01*VC_FTm() if ent.VC_Lht_DstCheckMult > 1 then ent.VC_Lht_DstCheckMult = 1 end end
				elseif ent.VC_Lht_DstCheckMult > 0 then ent.VC_Lht_DstCheckMult = ent.VC_Lht_DstCheckMult-0.01*VC_FTm() if ent.VC_Lht_DstCheckMult < 0 then ent.VC_Lht_DstCheckMult = 0 end
				end
			
			if ent.VC_Lht_DstCheckMult > 0 then
			if ent:GetNWBool("VC_Lights_Brk_Created") and LhtTbl and LhtTbl.Brake then Handle_Light_Draw_Multi(ent, LhtTbl.Brake, "BrakeColor", 2, nil, ent.VC_Lht_DstCheckMult) end
			if ent:GetNWBool("VC_Lights_Normal_Created") and LhtTbl and LhtTbl.Normal then Handle_Light_Draw_Multi(ent, LhtTbl.Normal, "NormalColor", 3, nil, ent.VC_Lht_DstCheckMult) end
			if ent:GetNWBool("VC_Lights_Rev_Created") and LhtTbl and LhtTbl.Reverse then Handle_Light_Draw_Multi(ent, LhtTbl.Reverse, "ReverseColor", 4, nil, ent.VC_Lht_DstCheckMult) end
			if ent:GetNWBool("VC_Lights_Head_Created") and LhtTbl and LhtTbl.Head then Handle_Light_Draw_Multi(ent, LhtTbl.Head, "HeadColor", 5, nil, ent.VC_Lht_DstCheckMult) end
			if ent:GetNWBool("VC_Lights_Hazards_Created") and LhtTbl and LhtTbl.Blinker then Handle_Light_Draw_Multi(ent, LhtTbl.Blinker, "BlinkersColor", 6, nil, ent.VC_Lht_DstCheckMult) end
			if ent:GetNWBool("VC_Lights_Blinker_Created_Left") and LhtTbl and LhtTbl.Blinker then Handle_Light_Draw_Multi(ent, LhtTbl.Blinker, "BlinkersColor", 6, 1, ent.VC_Lht_DstCheckMult) end
			if ent:GetNWBool("VC_Lights_Blinker_Created_Right") and LhtTbl and LhtTbl.Blinker then Handle_Light_Draw_Multi(ent, LhtTbl.Blinker, "BlinkersColor", 6, 2, ent.VC_Lht_DstCheckMult) end
			end
		end
	end
end)

VC_Fonts = {}

hook.Add("HUDPaint", "VC_HUDPaint", function()
	local ply, FTm = LocalPlayer(), VC_FTm() local Wep = ply:GetActiveWeapon() if !IsValid(Wep) then Wep = nil end local Went = Wep and Wep:GetNWEntity("VC_DamagedEnt") if !IsValid(Went) then Went = nil end
	local Veh = ply:GetVehicle() if !IsValid(Veh) or IsValid(Veh:GetParent()) then Veh = nil end local DrvV = Veh and GetViewEntity() == LocalPlayer()

	local On = Wep and Wep:GetClass() == "vc_repair" and Went
	if On then
	if !ply.VC_Rep_CFI or ply.VC_Rep_CFI < 1 then ply.VC_Rep_CFI = math.Round(((ply.VC_Rep_CFI or 0)+ 0.05*FTm)*100)/100 else ply.VC_Rep_CFI = 1 end
	elseif ply.VC_Rep_CFI then
	if ply.VC_Rep_CFI > 0 then ply.VC_Rep_CFI = math.Round((ply.VC_Rep_CFI- 0.05*FTm)*100)/100 else ply.VC_Rep_CFI = nil end
	end

	if !VC_Fonts["VC_Cruise"] then VC_Fonts["VC_Cruise"] = true surface.CreateFont("VC_Cruise", {font = "MenuLarge", size = 26, weight = 1000, blursize = 0, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false,outline = false}) end

	if ply.VC_Rep_CFI then
	local Int = 0
		if On then
		Int = Went and Went:GetNWInt("VC_Health") or 0 if Int < 0 then Int = 0 end
		if Went and Went:GetNWInt("VC_MaxHealth") > 0 then Int = Int/Went:GetNWInt("VC_MaxHealth") end
		ply.VC_HUD_Rep_LastInt = Lerp(0.1, ply.VC_HUD_Rep_LastInt or 0, Int)
		end

	Int = ply.VC_HUD_Rep_LastInt or 0

	draw.RoundedBox(4, ScrW()/2- 300/2, ScrH()/1.1-15, 300, 45, Color(0, 0, 0, 150*ply.VC_Rep_CFI))
	if !VC_Fonts["VC_RepairSmall"] then VC_Fonts["VC_RepairSmall"] = true surface.CreateFont("VC_RepairSmall", {font = "MenuLarge", size = 16, weight = 1000, blursize = 0, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false,outline = false}) end
	draw.SimpleText("Repairing vehicle", "VC_Cruise", ScrW()/2, ScrH()/1.1-3, Color(255, 255, 255, 255*ply.VC_Rep_CFI), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)

	draw.RoundedBox(0, ScrW()/2- 295/2, ScrH()/1.1+13, 295, 15, Color(0, 0, 0, 150*ply.VC_Rep_CFI))
	draw.RoundedBox(0, ScrW()/2- 295/2, ScrH()/1.1+13, 295*Int, 15, Int < 0.125 and Color(255,0,0,150*ply.VC_Rep_CFI) or Int < 0.333 and Color(255,155,0,150*ply.VC_Rep_CFI) or Color(0, 100+155*Int, 100+155*(1-Int), 150*ply.VC_Rep_CFI))
	draw.SimpleText("Health: "..math.Round(Int*100).."%", "VC_RepairSmall", ScrW()/2, ScrH()/1.1+19, Color(255, 255, 255, 255*ply.VC_Rep_CFI), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end

	if DrvV and Veh:GetNWInt("VC_Cruise_Spd") > 0 then
	if !ply.VC_Cruise_CFI or ply.VC_Cruise_CFI < 1 then ply.VC_Cruise_CFI = math.Round(((ply.VC_Cruise_CFI or 0)+ 0.05*FTm)*100)/100 else ply.VC_Cruise_CFI = 1 end
	elseif ply.VC_Cruise_CFI then
	if ply.VC_Cruise_CFI > 0 then ply.VC_Cruise_CFI = math.Round((ply.VC_Cruise_CFI- 0.05*FTm)*100)/100 else ply.VC_Cruise_CFI = nil end
	end

	if ply.VC_Cruise_CFI then
	draw.RoundedBox(4, ScrW()/2- 300/2, ScrH()/1.1- 20, 300, 40, Color(0, 0, 0, 150))
	draw.SimpleText("Cruising", "VC_Cruise", ScrW()/2, ScrH()/1.1, Color(255, 255-200, 255-200, 180), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	elseif ply.VC_Cruise_HUD_L then
	ply.VC_Cruise_HUD_L = nil ply.VC_Cruise_KDV = nil
	end
end)

hook.Add("PopulateToolMenu", "VC_MainMenu", function() spawnmenu.AddToolMenuOption("Utilities", "VCMod", "VC_Main", "VC_Main", "", "", function(Pnl) local Btn = vgui.Create("DButton") Btn:SetText("VCMod menu") Btn:SetToolTip('You can also open this menu by:\nConsole command: "vc_open_menu"\nUsing the "C" menu') Pnl:AddItem(Btn) Btn.DoClick = function() RunConsoleCommand("vc_open_menu") end end) end)