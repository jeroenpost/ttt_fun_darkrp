// The following script is copyrighted material, written by freemmaann Steam URI: steamcommunity.com/id/freemmaann/, if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.

file.CreateDir("vcmod")

local Settings = {
VC_Enabled = true,
VC_ThirdPerson_Dynamic = true,
VC_MouseControl = true,
VC_ThirdPerson_Auto = true,
VC_ThirdPerson_Vec_Stf = 100,
VC_ThirdPerson_Ang_Stf = 5,
VC_ThirdPerson_Auto_Pitch = 2.5,
VC_ThirdPerson_Ang_Pitch = true,
VC_ThirdPerson_Auto_Back = true,
VC_ThirdPerson_Cam_World = false,
VC_DynamicLights = true,
VC_LightDistance = 8000,
VC_Keyboard_Input = true,
}

VC_Settings = VC_Settings or {}

function VC_ResetSettings_Cl() file.Write("vcmod/settings_cl_VCMod1.txt", util.TableToJSON(Settings)) end

function VC_LoadSettings_Cl() local Tbl = {} if file.Exists("vcmod/settings_cl_VCMod1.txt", "DATA") then Tbl = util.JSONToTable(file.Read("vcmod/settings_cl_VCMod1.txt", "DATA")) else Tbl = Settings end VC_Settings = table.Copy(Tbl) end

function VC_SaveSetting_Cl(k,v)
	if k and v != nil then
	local Tbl = {}
	if file.Exists("vcmod/settings_cl_VCMod1.txt", "DATA") then Tbl = util.JSONToTable(file.Read("vcmod/settings_cl_VCMod1.txt", "DATA")) else Tbl = Settings end
	Tbl[k] = v
	VC_Settings = Tbl
	file.Write("vcmod/settings_cl_VCMod1.txt", util.TableToJSON(Tbl))
	end
end

VC_LoadSettings_Cl()

concommand.Add("VC_SaveSetting_Cl", function(ply, cmd, arg) if arg and arg[1] and arg[2] then VC_SaveSetting_Cl(arg[1], arg[2]) end end)