// The following script is copyrighted material, written by freemmaann Steam URI: steamcommunity.com/id/freemmaann/, if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.

local Int = 1

local function BuildMenu(Pnl)
	local Font = "VC_Treb24" if !VC_Fonts[Font] then VC_Fonts[Font] = true surface.CreateFont(Font, {font = "Trebuchet24", size = 26, weight = 10000, blursize = 0, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end
	local Font2 = "VC_Treb24_Small" if !VC_Fonts[Font2] then VC_Fonts[Font2] = true surface.CreateFont(Font2, {font = "Trebuchet24", size = 17, weight = 500, blursize = 0, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end
	local Font_New = "VC_New" if !VC_Fonts[Font_New] then VC_Fonts[Font_New] = true surface.CreateFont(Font_New, {Font_New = "Trebuchet24", size = 32, weight = 10000, blursize = 0, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end

	local Feat = {
	"Works on over 500 vehicles",
	"Lights",
	"Passenger seats",
	"Dynamic third person view",
	"Horn",
	"Damage (vehicle health)",
	"Wheel dust",
	"Cruise control",
	}

	local Btn = vgui.Create("DButton") Btn:SetPos(220, 337) Btn:SetSize(75, 20) Btn:SetText("Youtube") Btn:SetParent(Pnl)
	Btn.DoClick = function() gui.OpenURL("http://www.youtube.com/watch?v=bpsQO7D4Vh0") end

	local Btn = vgui.Create("DButton") Btn:SetPos(Pnl:GetWide()-78, Pnl:GetTall()-23) Btn:SetSize(75, 20) Btn:SetText("freemmaann") Btn:SetParent(Pnl)
	Btn.DoClick = function() gui.OpenURL("http://steamcommunity.com/id/freemmaann/") end

	local SP = false and game.SinglePlayer()
	if !SP then
	local Btn = vgui.Create("DButton") Btn:SetPos(350, 241) Btn:SetSize(120, 20) Btn:SetText("ScriptFodder.com") Btn:SetParent(Pnl)
	Btn.DoClick = function() gui.OpenURL("http://scriptfodder.com/scripts/view/21") end
	end

	local Icon = Material("icon16/tick.png")
	Pnl.Paint = function()
	draw.RoundedBox(0, 0, 0, Pnl:GetWide(), Pnl:GetTall(), Color(255, 255, 255, 200))
	draw.DrawText(SP and "You are using VCMod" or "This server is using VCMod", Font, Pnl:GetWide()/2, 10,  Color(0, 0, 0, 255), TEXT_ALIGN_CENTER)
	draw.DrawText("What is it for? Gives vehicles these:", Font2, 40, 45,  Color(0, 0, 0, 255), TEXT_ALIGN_LEFT)
		for i=1, #Feat do
		surface.SetMaterial(Icon) surface.DrawTexturedRect(50, 50+(18*i), 15, 15)
		draw.DrawText(Feat[i], Font2, 70, 50+(18*i),  Color(0, 0, 0, 255), TEXT_ALIGN_LEFT)
		end
		draw.DrawText("And much more.", Font2, 40, 218,  Color(0, 0, 0, 255), TEXT_ALIGN_LEFT)
		if !SP then
		draw.DrawText("Want it on your own server? You can buy it here:", Font2, 40, 243,  Color(0, 0, 0, 255), TEXT_ALIGN_LEFT)
		end

	draw.DrawText("Want police lights?", Font, Pnl:GetWide()/2, 305,  Color(0, 0, 0, 255), TEXT_ALIGN_CENTER)
	draw.DrawText("They will be released shortly:", Font2, 40, 340,  Color(0, 0, 0, 255), TEXT_ALIGN_LEFT)

	draw.DrawText("VCMod was created by: skype: comman6, steam:", Font2, Pnl:GetWide()-78, Pnl:GetTall()-20,  Color(0, 0, 0, 255), TEXT_ALIGN_RIGHT)
	end
end
VC_Menu_Info_Panel_Build = {"Info", BuildMenu}

local function BuildMenu(Pnl)
	local Font_Header = "VC_Menu_Header" if !VC_Fonts[Font_Header] then VC_Fonts[Font_Header] = true surface.CreateFont(Font_Header, {font = "MenuLarge", size = 18, weight = 1000, blursize = 2, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end
	local Font_Header_Focused = "VC_Menu_Header_Focused" if !VC_Fonts[Font_Header_Focused] then VC_Fonts[Font_Header_Focused] = true surface.CreateFont(Font_Header_Focused, {font = "MenuLarge", size = 18, weight = 1000, blursize = 0, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end
	local Font_Info = "VC_Info" if !VC_Fonts[Font_Info] then VC_Fonts[Font_Info] = true surface.CreateFont(Font_Info, {font = "MenuLarge", size = 24, weight = 1000, blursize = 0, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end

	local List = VC_Menu_CreateList(0, 35, Pnl:GetWide(), Pnl:GetTall()-35) List:SetParent(Pnl)

	local CBox = VC_Menu_CreateCBox("Enabled clientside", "Basically shuts down all the stuff bellow.", "VC_Enabled", VC_Settings) List:AddItem(CBox)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText("Lag reduction") List:AddItem(SNLbl)
	local Sldr = VC_Menu_CreateSldr("Light distance", 0, 15000, 0, "How far the lights can be seen, reduces the lag abit.", "VC_LightDistance", VC_Settings) List:AddItem(Sldr)
	local CBox = VC_Menu_CreateCBox("Dynamic lights", "If lights are lagging for you, uncheck this.", "VC_DynamicLights", VC_Settings) List:AddItem(CBox)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText("Input") List:AddItem(SNLbl)
	local CBox = VC_Menu_CreateCBox("Keyboard", "Allows you to use your buttons for features (horn, lights, etc).", "VC_Keyboard_Input", VC_Settings) List:AddItem(CBox)
	local CBox = VC_Menu_CreateCBox("Mouse", "Allows you to use your mouse for features (middle mouse buttons, mouse wheel).", "VC_MouseControl", VC_Settings) List:AddItem(CBox)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText("Third person view") List:AddItem(SNLbl)
	local CBox = VC_Menu_CreateCBox("Dynamic view", "Controls if VCMOd view controls are on or off.", "VC_ThirdPerson_Dynamic", VC_Settings) List:AddItem(CBox)
	local CBox = VC_Menu_CreateCBox("Auto focus", "Focuses the camera behind the vehicle.", "VC_ThirdPerson_Auto", VC_Settings) List:AddItem(CBox)
	local CBox = VC_Menu_CreateCBox("Auto focus reverse", "Focuses the camera in front of the vehicle while reversing.", "VC_ThirdPerson_Auto_Back", VC_Settings) List:AddItem(CBox)
	local Sldr = VC_Menu_CreateSldr("Auto focus heiht", 0, 20, 0, "How high behind the car the camera will focus.", "VC_ThirdPerson_Auto_Pitch", VC_Settings) List:AddItem(Sldr)
	local Sldr = VC_Menu_CreateSldr("Vector stiffness %", 0, 100, 0, "How far the view will stay behind before slowly returning to normal.", "VC_ThirdPerson_Vec_Stf", VC_Settings) List:AddItem(Sldr)
	local Sldr = VC_Menu_CreateSldr("Angle stiffness %", 0, 100, 0, "How far the view will sway from side to side before slowly returning to normal.", "VC_ThirdPerson_Ang_Stf", VC_Settings) List:AddItem(Sldr)
	local CBox = VC_Menu_CreateCBox("Ignore world", "Lets the view pass through objects.", "VC_ThirdPerson_Cam_World", VC_Settings) List:AddItem(CBox)

	Pnl.Paint = function()
	draw.DrawText("These options will only affect you", Font_Info, List:GetWide()/2, 5,  Color(255 , 155, 155, 255), TEXT_ALIGN_CENTER)
	end

	return Draw
end
list.Set("VC_Menu_Items_P", Int, {"All", BuildMenu}) Int = Int+1

local function BuildMenu(Pnl)
	local Font_Info = "VC_Info" if !VC_Fonts[Font_Info] then VC_Fonts[Font_Info] = true surface.CreateFont(Font_Info, {font = "MenuLarge", size = 24, weight = 1000, blursize = 0, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end

	local List = VC_Menu_CreateList(0, 35, Pnl:GetWide(), Pnl:GetTall()-35) List:SetParent(Pnl)

	local CBox = VC_Menu_CreateCBox("Keyboard", "Allows you to use your buttons for features (horn, lights, etc).", "VC_Keyboard_Input", VC_Settings) List:AddItem(CBox)

	local FSpc = "          "
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText("Blinkers:") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText(FSpc.."Left ALT + V - Lock/Unlock the vehicle.") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText(FSpc.."Left ALT + COMMA - Toggle left blinkers.") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText(FSpc.."Left ALT + PERIOD - Toggle right blinkers.") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText("Lights:") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText(FSpc.."Tap F - Headlights.") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText(FSpc.."Hold F - Night Lights.") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText("Other:") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText(FSpc.."Left ALT + V - Lock/Unlock the vehicle.") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText(FSpc.."Hold R - Horn.") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText(FSpc.."Tap B - Cruise.") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText(FSpc.."Number keys above letters 1-9, 0 - switch seats, 0, to cycle through all available.") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText(FSpc.."Hold LEFT ALT + number keys above letters 2-9, 0 - kick people out, 0, to kick out all people.") List:AddItem(SNLbl)
	local CBox = VC_Menu_CreateCBox("Mouse", "Allows you to use your mouse for features (middle mouse buttons, mouse wheel).", "VC_MouseControl", VC_Settings) List:AddItem(CBox)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText(FSpc.."Hold the middle button to look behind, release to reset.") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText(FSpc.."Scroll up/down to zoom in/out, zoomed in is the default zoom level.") List:AddItem(SNLbl)

		Pnl.Paint = function()
		draw.DrawText("Key bindings", Font_Info, List:GetWide()/2, 5,  Color(255 , 155, 155, 255), TEXT_ALIGN_CENTER)
		end

		Pnl.Think = function()
		if VC_Settings_TempTbl then for k,v in pairs(VC_Settings_TempTbl) do ElTbl[k]:SetValue(v) Settings_Sv[k] = v end VC_Settings_TempTbl = nil end
		end

	return Draw
end
list.Set("VC_Menu_Items_P", Int, {"Key bindings", BuildMenu}) Int = Int+1

local function BuildMenu(Pnl)
	local Font_Header = "VC_Menu_Header" if !VC_Fonts[Font_Header] then VC_Fonts[Font_Header] = true surface.CreateFont(Font_Header, {font = "MenuLarge", size = 18, weight = 1000, blursize = 2, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end
	local Font_Header_Focused = "VC_Menu_Header_Focused" if !VC_Fonts[Font_Header_Focused] then VC_Fonts[Font_Header_Focused] = true surface.CreateFont(Font_Header_Focused, {font = "MenuLarge", size = 18, weight = 1000, blursize = 0, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end
	local Font_Info = "VC_Info" if !VC_Fonts[Font_Info] then VC_Fonts[Font_Info] = true surface.CreateFont(Font_Info, {font = "MenuLarge", size = 24, weight = 1000, blursize = 0, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end

	local List = VC_Menu_CreateList(0, 35, Pnl:GetWide(), Pnl:GetTall()-35) List:SetParent(Pnl)

	local ElTbl = {}

	local Settings_Sv = {}

	local CBox = VC_Menu_CreateCBox("Enabled serverside", "Basically shuts down all the stuff bellow.", "VC_Enabled", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Enabled = CBox
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText("Lights") List:AddItem(SNLbl)
	local CBox = VC_Menu_CreateCBox("Lights", "Enables/Disables lights on vehicles including the headlights.", "VC_Lights", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Lights = CBox
	local CBox = VC_Menu_CreateCBox("Headlights", "Enables/Disables headlights on vehicles.", "VC_HeadLights", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_HeadLights = CBox
	local Sldr = VC_Menu_CreateSldr("Lights off time", 0, 600, 0, "Lights will turn off if the car is left alone after this time.", "VC_LightsOffTime", Settings_Sv, true) List:AddItem(Sldr) ElTbl.VC_LightsOffTime = Sldr
	local Sldr = VC_Menu_CreateSldr("HeadLights off time", 0, 120, 0, "Headlights will turn off if the car is left alone after this time.", "VC_HLightsOffTime", Settings_Sv, true) List:AddItem(Sldr) ElTbl.VC_HLightsOffTime = Sldr
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText("Health & Damage") List:AddItem(SNLbl)
	local CBox = VC_Menu_CreateCBox("Damage", "With this the vehicle will receive physical and weapon based damage.", "VC_Damage", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Damage = CBox
	local Sldr = VC_Menu_CreateSldr("Fire Duration", 0, 600, 1, "For how long the fire will last after the vehicle has exploded.", "VC_Dmg_Fire_Duration", Settings_Sv, true) List:AddItem(Sldr) ElTbl.VC_Dmg_Fire_Duration = Sldr
	local Sldr = VC_Menu_CreateSldr("Starting health multiplier", 0, 5, 1, "Starting health multiplier, applied to a car when it is spawned.", "VC_Health_Multiplier", Settings_Sv, true) List:AddItem(Sldr) ElTbl.VC_Health_Multiplier = Sldr
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText("Other options") List:AddItem(SNLbl)
	local CBox = VC_Menu_CreateCBox("Steering wheel lock on exit", "Locks the cars steering wheel when a driver exits a vehicle.", "VC_Wheel_Lock", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Wheel_Lock = CBox
	local CBox = VC_Menu_CreateCBox("Brakes lock on exit", "Locks the cars handbrake when a driver exits.", "VC_Brake_Lock", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Brake_Lock = CBox
	local CBox = VC_Menu_CreateCBox("Door sounds", "Emit sounds when a door is being open/closed.", "VC_Door_Sounds", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Door_Sounds = CBox
	local CBox = VC_Menu_CreateCBox("Wheel dust", "Emit an effect from the wheels when driving around.", "VC_Wheel_Dust", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Wheel_Dust = CBox
	local CBox = VC_Menu_CreateCBox("Wheel dust while braking", "Emit an effect from the wheels when driving around.", "VC_Wheel_Dust_Brakes", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Wheel_Dust_Brakes = CBox
	local CBox = VC_Menu_CreateCBox("Match player speed when exit", "Players speed gets set to the ones of a car when they exit a vehicle.", "VC_Exit_Velocity", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Exit_Velocity = CBox
	local CBox = VC_Menu_CreateCBox("Nocollide players with car on exit", "This allows for more stable exit.", "VC_Exit_NoCollision", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Exit_NoCollision = CBox
	local CBox = VC_Menu_CreateCBox("Horn", "This will allow people to use cars horn.", "VC_Horn_Enabled", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Horn_Enabled = CBox
	local CBox = VC_Menu_CreateCBox("Cruise", "This will allow people to use cars cruise.", "VC_Cruise_Enabled", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Cruise_Enabled = CBox
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText("Exhaust & Passenger seats") List:AddItem(SNLbl)
	local CBox = VC_Menu_CreateCBox("Exhaust", "Exhaust effect when a vehicle is idle or in movement.", "VC_Exhaust_Effect", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Exhaust_Effect = CBox
	local CBox = VC_Menu_CreateCBox("Passenger seats", "Allows a vehicle to have passenger seats.", "VC_Passenger_Seats", Settings_Sv, true) List:AddItem(CBox) ElTbl.VC_Passenger_Seats = CBox

	local Btn = vgui.Create("DButton") Btn:SetPos(Pnl:GetWide()/2-75, Pnl:GetTall()-20) Btn:SetSize(75, 20) Btn:SetText("Save") Btn:SetParent(Pnl)
	Btn.DoClick = function()
		if LocalPlayer():IsAdmin() then
		net.Start("VC_SendSettingsToServer")
		net.WriteEntity(LocalPlayer())
		net.WriteTable(Settings_Sv)
		net.SendToServer()
		end
	end
	local Btn = vgui.Create("DButton") Btn:SetPos(Pnl:GetWide()/2, Pnl:GetTall()-20) Btn:SetSize(75, 20) Btn:SetText("Load") Btn:SetParent(Pnl)
	Btn.DoClick = function() RunConsoleCommand("VC_GetSettings_Sv") end

	RunConsoleCommand("VC_GetSettings_Sv")

		Pnl.Paint = function()
		draw.DrawText("These settings can only be applied by an administrator", Font_Info, List:GetWide()/2, 5,  Color(255 , 155, 155, 255), TEXT_ALIGN_CENTER)
		end

		Pnl.Think = function()
		if VC_Settings_TempTbl then for k,v in pairs(VC_Settings_TempTbl) do ElTbl[k]:SetValue(v) Settings_Sv[k] = v end VC_Settings_TempTbl = nil end
		end

	return Draw
end
list.Set("VC_Menu_Items_A", Int, {"All", BuildMenu}) Int = Int+1

local function BuildMenu(Pnl)
	local Font_Info = "VC_Info" if !VC_Fonts[Font_Info] then VC_Fonts[Font_Info] = true surface.CreateFont(Font_Info, {font = "MenuLarge", size = 24, weight = 1000, blursize = 0, scanlines = 0, antialias = true, underline = false, italic = false, strikeout = false, symbol = false, rotary = false, shadow = false, additive = false, outline = false}) end

	local List = VC_Menu_CreateList(0, 35, Pnl:GetWide(), Pnl:GetTall()-35) List:SetParent(Pnl)

	local SNLbl = vgui.Create("DLabel") SNLbl:SetText("Still in development, will allow you to setup the NPC with this menu.") List:AddItem(SNLbl)
	local SNLbl = vgui.Create("DLabel") SNLbl:SetText('For now you will have to do it manually, open the "/autorun/VC_DarkRP_NPC.lua" file.') List:AddItem(SNLbl)

		Pnl.Paint = function()
		draw.DrawText("NPC Settings", Font_Info, List:GetWide()/2, 5,  Color(255 , 155, 155, 255), TEXT_ALIGN_CENTER)
		end

		Pnl.Think = function()
		if VC_Settings_TempTbl then for k,v in pairs(VC_Settings_TempTbl) do ElTbl[k]:SetValue(v) Settings_Sv[k] = v end VC_Settings_TempTbl = nil end
		end

	return Draw
end
list.Set("VC_Menu_Items_A", Int, {"NPC", BuildMenu}) Int = Int+1