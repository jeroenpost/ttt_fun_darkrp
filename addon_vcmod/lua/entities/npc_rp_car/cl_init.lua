include( 'shared.lua' )
ENT.RenderGroup = RENDERGROUP_BOTH
 
function ENT:Draw( )
    self.Entity:DrawModel( )
end
surface.CreateFont ("NPCHUD", {
	size = 12,
	weight = 600,
	antialias = true,
	shadow = false,
	font = "DejaVu Sans"})

local CurrentCars = {}
local HasCars = false
local isTesting = false
net.Receive( "SendCurrentCars", function()
	local id = net.ReadInt( 8 )
	if( id == -1 ) then
		HasCars = true
	else
		table.insert( CurrentCars, id )
	end
end )

local function GetCurrentCars()
	HasCars = false
	CurrentCars = {}
	net.Start( "RequestCurrentCars" )
	net.SendToServer()
end

local function AttemptBuyCar( id )
	if( id > #VC_NPC_Cars ) then return end
	net.Start( "AttemptBuyCar" )
		net.WriteInt( id, 8 )
	net.SendToServer()
end

local function AttemptSellCar( id )
	if( id > #VC_NPC_Cars ) then return end
	net.Start( "AttemptSellCar" )
		net.WriteInt( id, 8 )
	net.SendToServer()
end

local function AttemptSpawnCar( id )
	if( id > #VC_NPC_Cars ) then return end
	net.Start( "AttemptSpawnCar" )
		net.WriteInt( id, 8 )
	net.SendToServer()
end

local function AttemptRemoveCar()
	net.Start( "AttemptRemoveCar" )
	net.SendToServer()
end
local function AttemptTestCar( id )
	if( id > #VC_NPC_Cars ) then return end
	net.Start( "AttemptTestCar" )
		net.WriteInt( id, 8 )
	net.SendToServer()
end
function AttemptDraw()
	if( HasCars == false ) then
		timer.Simple( 0.05, function()
			AttemptDraw()
		end )
		return
	end
	local MainFrame = vgui.Create( "DFrame" )
	MainFrame:SetPos( ScrW(  ) / 2 - 250,  ScrH(  ) / 2 - 200 )
	MainFrame:SetSize( 500, 425 )
	MainFrame:SetTitle( "VC Car NPC" )
	MainFrame:SetVisible( true )
	MainFrame:SetDraggable( false )
	MainFrame:MakePopup(  )
	MainFrame:MakePopup()
	MainFrame.Paint = function()
		draw.RoundedBox( 8, 0, 0, MainFrame:GetWide(), MainFrame:GetTall(), Color( 0, 0, 0, 200 ) )
	end

	local Putawayyourcarbutton = vgui.Create( "DButton" )
	Putawayyourcarbutton:SetParent( MainFrame )
	Putawayyourcarbutton:SetFont("NPCHUD")
	Putawayyourcarbutton:SetTextColor(Color(0,0,0,255))
	Putawayyourcarbutton:SetPos( 200, 20 )
	Putawayyourcarbutton:SetSize( 125, 25 )
	Putawayyourcarbutton:SetText( "Put away your car!" )
	Putawayyourcarbutton.DoClick = function()	
		AttemptRemoveCar()
		MainFrame:Close()
	end

	local Tabs = vgui.Create( "DPropertySheet" )
	Tabs:SetParent( MainFrame )
	Tabs:SetPos( 10, 55 )
	Tabs:SetSize( 480, 360 )
	
	local Spawn = vgui.Create("DPanelList")
	Spawn:SetSize(475, 355)
	Spawn:SetPos(5, 15)
	Spawn:SetSpacing(5)
	Spawn:EnableHorizontal(false)
	Spawn:EnableVerticalScrollbar(true)
	
	local Buy = vgui.Create("DPanelList")
	Buy:SetSize(475, 355)
	Buy:SetPos(5, 15)
	Buy:SetSpacing(5)
	Buy:EnableHorizontal(false)
	Buy:EnableVerticalScrollbar(true)
	
	local Sell = vgui.Create("DPanelList")
	Sell:SetSize(475, 355)
	Sell:SetPos(5, 15)
	Sell:SetSpacing(5)
	Sell:EnableHorizontal(false)
	Sell:EnableVerticalScrollbar(true)
	
	Tabs:AddSheet( "Spawn", Spawn, nil, false, false, "Spawn" )
	Tabs:AddSheet( "Buy", Buy, nil, false, false, "Buy" )
	Tabs:AddSheet( "Sell", Sell, nil, false, false, "Sell" )
	
	--Spawning	
	for k,v in pairs( VC_NPC_Cars ) do
		local hascar = false
		for k2, v2 in pairs( CurrentCars ) do
			if( k == v2 ) then
				hascar = true
			end
		end
		if( hascar == true ) then
			local DermaPanel = vgui.Create( "DPanel", Spawn )
			DermaPanel:SetSize( 470, 67 )
			
			if(v.vip) then
				DermaPanel.Paint = function()
					surface.SetDrawColor(0,145,255,255)
					surface.DrawRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
					surface.SetDrawColor(0,0,255,255)
					surface.DrawOutlinedRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
				end	
			else
				if(v.price <= 70000) then
					DermaPanel.Paint = function()
						surface.SetDrawColor(200,200,200,255)
						surface.DrawRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
						surface.SetDrawColor(0,0,0,255)
						surface.DrawOutlinedRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
					end	
				elseif(v.price <= 150000) then
					DermaPanel.Paint = function()
						surface.SetDrawColor(255,152,83,255)
						surface.DrawRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
						surface.SetDrawColor(255,127,39,255)
						surface.DrawOutlinedRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
					end	
				else
					DermaPanel.Paint = function()
						surface.SetDrawColor(255,255,130,255)
						surface.DrawRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
						surface.SetDrawColor(255,255,50,255)
						surface.DrawOutlinedRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
					end	
				end
			end
			
			
			local DermaButton = vgui.Create( "DButton" )
			DermaButton:SetParent( DermaPanel )
			DermaButton:SetFont("NPCHUD")
			DermaButton:SetTextColor(Color(0,0,0,255))
			DermaButton:SetPos( 280, 20 )
			DermaButton:SetSize( 150, 25 )
			DermaButton:SetText("Spawn")
			DermaButton.DoClick = function()	
				AttemptSpawnCar( k )
				MainFrame:Close()
			end

			local DermaIcon = vgui.Create( "SpawnIcon" )
			DermaIcon:SetParent( DermaPanel )
			DermaIcon:SetPos( 2, 2 )
			DermaIcon:SetModel( v.model )
			DermaIcon:SetToolTip( v.name )
		
			local DermaLabel = vgui.Create( "DLabel" )
			DermaLabel:SetText( v.name.."\n"..v.info)
			DermaLabel:SetFont("NPCHUD")
			DermaLabel:SetTextColor(Color(0,0,0,255))
			DermaLabel:SetParent( DermaPanel )
			DermaLabel:SetPos( 100, 5 )
			DermaLabel:SetSize( 150, 25 )
			Spawn:AddItem( DermaPanel )
		end
	end
	
	--Buying
	for k,v in pairs( VC_NPC_Cars ) do
		local hascar = false
		for k2, v2 in pairs( CurrentCars ) do
			if( k == v2 ) then
				hascar = true
			end
		end
		if( hascar == false ) then
			local DermaPanel = vgui.Create( "DPanel", Buy )
			DermaPanel:SetSize( 470, 67 )
			
			if(v.vip) then
				DermaPanel.Paint = function()
					surface.SetDrawColor(0,145,255,255)
					surface.DrawRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
					surface.SetDrawColor(0,0,255,255)
					surface.DrawOutlinedRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
				end	
			else
				if(v.price <= 70000) then
					DermaPanel.Paint = function()
						surface.SetDrawColor(200,200,200,255)
						surface.DrawRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
						surface.SetDrawColor(0,0,0,255)
						surface.DrawOutlinedRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
					end	
				elseif(v.price <= 150000) then
					DermaPanel.Paint = function()
						surface.SetDrawColor(255,152,83,255)
						surface.DrawRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
						surface.SetDrawColor(255,127,39,255)
						surface.DrawOutlinedRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
					end	
				else
					DermaPanel.Paint = function()
						surface.SetDrawColor(255,255,130,255)
						surface.DrawRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
						surface.SetDrawColor(255,255,50,255)
						surface.DrawOutlinedRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
					end	
				end
			end
			local DermaButton = vgui.Create( "DButton" )
			DermaButton:SetParent( DermaPanel )
			DermaButton:SetFont("NPCHUD")
			DermaButton:SetTextColor(Color(0,0,0,255))
			DermaButton:SetPos( 280, 20 )
			DermaButton:SetSize( 150, 25 )
			DermaButton:SetText("$"..v.price)
			DermaButton.DoClick = function()	
				local DermaButton = DermaMenu()
				DermaButton:AddOption("Test Drive", function() 
					//MsgN(k)
					AttemptTestCar( k )
					MainFrame:Close()
				end )
				DermaButton:AddOption("Buy", function() 
					AttemptBuyCar( k )
					MainFrame:Close()
				end )
				DermaButton:AddOption("Cancel", function() 
				end )
				DermaButton:Open()
			end

			local DermaIcon = vgui.Create( "SpawnIcon" )
			DermaIcon:SetParent( DermaPanel )
			DermaIcon:SetPos( 2, 2 )
			DermaIcon:SetModel( v.model )
			DermaIcon:SetToolTip( v.name )
		
			local DermaLabel = vgui.Create( "DLabel" )
			DermaLabel:SetText( v.name.."\n"..v.info)
			DermaLabel:SetFont("NPCHUD")
			DermaLabel:SetTextColor(Color(0,0,0,255))
			DermaLabel:SetParent( DermaPanel )
			DermaLabel:SetPos( 100, 5 )
			DermaLabel:SetSize( 150, 25 )
			Buy:AddItem( DermaPanel )
		end
	end
	--Selling
	for k,v in pairs( VC_NPC_Cars ) do
		local hascar = false
		for k2, v2 in pairs( CurrentCars ) do
			if( k == v2 ) then
				hascar = true
			end
		end
		if( hascar == true ) then
			local DermaPanel = vgui.Create( "DPanel", Sell )
			DermaPanel:SetSize( 470, 67 )
			
			if(v.vip) then
				DermaPanel.Paint = function()
					surface.SetDrawColor(0,145,255,255)
					surface.DrawRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
					surface.SetDrawColor(0,0,255,255)
					surface.DrawOutlinedRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
				end	
			else
				if(v.price <= 70000) then
					DermaPanel.Paint = function()
						surface.SetDrawColor(200,200,200,255)
						surface.DrawRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
						surface.SetDrawColor(0,0,0,255)
						surface.DrawOutlinedRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
					end	
				elseif(v.price <= 150000) then
					DermaPanel.Paint = function()
						surface.SetDrawColor(255,152,83,255)
						surface.DrawRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
						surface.SetDrawColor(255,127,39,255)
						surface.DrawOutlinedRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
					end	
				else
					DermaPanel.Paint = function()
						surface.SetDrawColor(255,255,130,255)
						surface.DrawRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
						surface.SetDrawColor(255,255,50,255)
						surface.DrawOutlinedRect(0,0,DermaPanel:GetWide(), DermaPanel:GetTall())
					end	
				end
			end
			
			local DermaButton = vgui.Create( "DButton" )
			DermaButton:SetParent( DermaPanel )
			DermaButton:SetFont("NPCHUD")
			DermaButton:SetTextColor(Color(0,0,0,255))
			DermaButton:SetPos( 280, 20 )
			DermaButton:SetSize( 150, 25 )
			DermaButton:SetText("$"..v.price * .75)
			DermaButton.DoClick = function()	
				local DermaButton = DermaMenu()
				DermaButton:AddOption("Sell", function() 
					//MsgN(k)
					AttemptSellCar( k )
					MainFrame:Close()
				end )
				DermaButton:AddOption("Cancel", function() 
				end )
				DermaButton:Open()
			end

			local DermaIcon = vgui.Create( "SpawnIcon" )
			DermaIcon:SetParent( DermaPanel )
			DermaIcon:SetPos( 2, 2 )
			DermaIcon:SetModel( v.model )
			DermaIcon:SetToolTip( v.name )
		
			local DermaLabel = vgui.Create( "DLabel" )
			DermaLabel:SetText( v.name.."\n"..v.info)
			DermaLabel:SetFont("NPCHUD")
			DermaLabel:SetTextColor(Color(0,0,0,255))
			DermaLabel:SetParent( DermaPanel )
			DermaLabel:SetPos( 100, 5 )
			DermaLabel:SetSize( 150, 25 )
			Sell:AddItem( DermaPanel )
		end
	end
end
net.Receive( "OpenCarMenu", function()
	GetCurrentCars()
	AttemptDraw()
end )