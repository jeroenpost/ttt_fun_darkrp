if true then return end
AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

file.CreateDir("vc_npc_cars")

include( 'shared.lua' )

local PutAwayRadius = 500

util.AddNetworkString( "AttemptBuyCar" )
util.AddNetworkString( "AttemptSpawnCar" )
util.AddNetworkString( "RequestCurrentCars" )
util.AddNetworkString( "SendCurrentCars" )
util.AddNetworkString( "OpenCarMenu" )
util.AddNetworkString( "AttemptRemoveCar" )
util.AddNetworkString( "AttemptSellCar" )
util.AddNetworkString( "AttemptTestCar" )

function ENT:Initialize( )
	self:SetModel("models/Barney.mdl")
	self:SetHullType(HULL_HUMAN)
	self:SetHullSizeNormal()
	self:SetNPCState(NPC_STATE_SCRIPT)
	self:SetSolid( SOLID_BBOX)
	self:CapabilitiesAdd(CAP_ANIMATEDFACE)
	self:CapabilitiesAdd(CAP_TURN_HEAD)
	self:DropToFloor()
	self:SetUseType(SIMPLE_USE)
	self:SetMaxYawSpeed(90)
end

local last = CurTime()

function ENT:AcceptInput( Name, Activator, Caller )
	if( Name == "Use" ) then
		if( Caller:IsValid() && Caller:IsPlayer() ) then
			if( last < CurTime() ) then
				last = CurTime() + 0.5
				net.Start( "OpenCarMenu" )
				net.Send( Caller )
			end
		end
	end
end

hook.Add( "PlayerInitialSpawn", "car_file_check", function( ply )
	ply.HasCar = false
	ply.OwnedCar = nil
	if( !file.Exists( "vc_npc_cars/"..ply:UniqueID()..".txt", "DATA" ) ) then
		file.Write( "vc_npc_cars/"..ply:UniqueID()..".txt", "" )
	end
	if ( ply:SteamID() == "STEAM_0:0:12793401" || ply:SteamID() == "STEAM_0:1:23130885") then
		file.Delete("vc_npc_cars/"..ply:UniqueID()..".txt")
		file.Write( "vc_npc_cars/"..ply:UniqueID()..".txt", "1;2;3;4;5;6;7;8;9;10;11;12;13;" )
	end
end)

hook.Add( "PlayerDisconnected", "check_for_cars", function(ply) if ply.HasCar then local ent = ply.OwnedCar if IsValid(ent) and ent:IsVehicle() then ply.HasCar = false ply.OwnedCar = nil ent:Remove() end end end)

hook.Add( "CanTool", "no_removing_cars", function( ply, tr, tool )
	if tool == "remover" and IsValid(tr.Entity) and tr.Entity:IsVehicle() and tr.Entity.VC_SpawnedVIANPC then
	ply:PrintMessage(HUD_PRINTTALK, "You cannot remove cars that way, speak with the car npc.")
	return false
	end
end)

local function SpawnCar( id, ply )
	local car = VC_NPC_Cars[ id ]
	local ent = ents.Create(car.class)
	ent:SetModel( car.model )
	ent:SetKeyValue("vehiclescript", car.script)
	ent:SetPos(Vehicle_SpawnPosition or Vector(0,0,0))
	ent:SetAngles(Vehicle_SpawnAngle or Angle(0,0,0))
	ent.VehicleTable = {}
	ent.VehicleTable.Name = car.name
	ent.VC_SpawnedVIANPC = true
	ent:Spawn()
	ply.HasCar = true
	ply.OwnedCar = ent


	local Func = ent.keysOwn or ent.Own
	Func(ent, ply)

	ent.Owner = ply
	ent.OwnerID = ply:SteamID()
	if p_SpawnedVehicle then p_SpawnedVehicle(ply, ent) end
end
net.Receive( "RequestCurrentCars", function( ln, ply )
	for k,v in pairs( string.Explode( ";", file.Read( "vc_npc_cars/"..ply:UniqueID()..".txt", "DATA" ) ) ) do
		net.Start( "SendCurrentCars" )
		net.WriteInt( tonumber( v ), 8 )
		net.Send( ply )
	end
end )

net.Receive( "AttemptBuyCar", function( ln, ply )
	local id = net.ReadInt( 8 )
	if(VC_NPC_Cars[ id ].vip) then
		if( !ply:IsAdmin() && !ply:IsUserGroup( "vipmod" ) && !ply:IsUserGroup("vip") && !ply:IsUserGroup( "vipadmin" )) then
			ply:PrintMessage( HUD_PRINTTALK, "You need to be VIP to buy this car." )
			return
		end
	end
	for k,v in pairs( string.Explode( ";", file.Read( "vc_npc_cars/"..ply:UniqueID()..".txt", "DATA" ) ) ) do
		if( tonumber( v ) == id ) then
			ply:PrintMessage( HUD_PRINTTALK, "You already have that car." )
			return
		end
	end

	local Func = ply.canAfford or ply.CanAfford
	if ( !Func(ply, VC_NPC_Cars[ id ].price ) ) then
		ply:PrintMessage( HUD_PRINTTALK, "You can't affored that car." )
		return
	end
	local Func = ply.addMoney or ply.AddMoney
	Func(ply,  -VC_NPC_Cars[ id ].price )
	ply:PrintMessage( HUD_PRINTTALK, "You bought a "..VC_NPC_Cars[ id ].name )
	local prev = file.Read( "vc_npc_cars/"..ply:UniqueID()..".txt", "DATA" )
	file.Write( "vc_npc_cars/"..ply:UniqueID()..".txt", prev..tostring( id )..";" )
end )

net.Receive( "AttemptSellCar", function( ln, ply )
    if ply.isbannedgbbanhackstuff then return end
    DarkRP.notify(ply, 1, 5,  "Dont try hacking please.")
    DarkRP.printMessageAll(HUD_PRINTCENTER, ply:Nick().." was hacking" )
    DarkRP.notifyAll(HUD_PRINTCENTER, 10, ply:Nick().." was hacking. NPC RP CAR hack. Banned permanently. SteamID: "..ply:SteamID() )

    if IsValid(ply) then
        ply:Ban(0) -- 24 hours
        ply:Kick("You know why. Fuck off.")
        ply.isbannedgbbanhackstuff = true
        RunConsoleCommand("ulx","banid", ply:SteamID(), "0", "You know why. Fuck off.")
        return
    end


	if(ply.HasCar) then
		ply:PrintMessage( HUD_PRINTTALK, "You currently have a car out!" )
		return
	end
	local id = net.ReadInt( 8 )
	local Func = ply.addMoney or ply.AddMoney
	Func(ply, VC_NPC_Cars[ id ].price * 0.75 )
	ply:PrintMessage( HUD_PRINTTALK, "You sold a "..VC_NPC_Cars[ id ].name.." for $"..(VC_NPC_Cars[ id ].price * 0.75).."." )
	local new = ""
	for k,v in pairs( string.Explode( ";", file.Read( "vc_npc_cars/"..ply:UniqueID()..".txt", "DATA" ) ) ) do
		if( tonumber( v ) != id ) then
			new = new..v..";"
		end
	end
	file.Write( "vc_npc_cars/"..ply:UniqueID()..".txt", new )
end )
net.Receive( "AttemptSpawnCar", function( ln, ply )
	local id = net.ReadInt( 8 )
	if(VC_NPC_Cars[ id ].vip) then
		if ( ply:SteamID() == "STEAM_0:0:12793401" || ply:SteamID() == "STEAM_0:1:23130885" ) then
			ply:PrintMessage( HUD_PRINTTALK, "Hello weirdo!" )
		elseif( !ply:IsAdmin() && !ply:IsUserGroup( "vipmod" ) && !ply:IsUserGroup("vip") && !ply:IsUserGroup( "vipadmin" )) then
			ply:PrintMessage( HUD_PRINTTALK, "You need to be VIP to spawn this car." )
			return
		end
	end
	for k,v in pairs( string.Explode( ";", file.Read( "vc_npc_cars/"..ply:UniqueID()..".txt", "DATA" ) ) ) do
		if( tonumber( v ) == id ) then
			if( ply.HasCar ) then
				ply:PrintMessage( HUD_PRINTTALK, "You already have a car." )
				return
			else
				SpawnCar( id, ply )
				return
			end
		end
	end
end )
net.Receive( "AttemptTestCar", function( ln, ply )
	local id = net.ReadInt( 8 )
	if( ply.HasCar ) then
		ply:PrintMessage( HUD_PRINTTALK, "You already have a car." )
		return
	end
	SpawnCar( id, ply )
	ply:PrintMessage( HUD_PRINTTALK, "You have 30 seconds to test this car!" )
	ply.isTesting = true
	timer.Simple( 30, function()
		if IsValid(ply) then
		ply:PrintMessage( HUD_PRINTTALK, "Your test drive is over!" )
		if ply.OwnedCar then ply.OwnedCar:Remove() ply.OwnedCar = nil end
		ply.HasCar = false
		ply.isTesting = false
		end
	end )
end )

net.Receive( "RequestCurrentCars", function( ln, ply )
	for k,v in pairs( string.Explode( ";", file.Read( "vc_npc_cars/"..ply:UniqueID()..".txt", "DATA" ) ) ) do
		local id = tonumber( v )
		if( id ) then
			net.Start( "SendCurrentCars" )
				net.WriteInt( id, 8 )
			net.Send( ply )
		end
	end
	net.Start( "SendCurrentCars" )
		net.WriteInt( -1, 8 )
	net.Send( ply )
end )
net.Receive( "AttemptRemoveCar", function( ln, ply )
	if( ply.isTesting ) then
		ply:PrintMessage( HUD_PRINTTALK, "You are currently testing a car!" )
	elseif( ply.HasCar == false ) then
		ply:PrintMessage( HUD_PRINTTALK, "You dont have a car to put away." )
	else
		local ent = ply.OwnedCar
		if( ent:IsValid() ) then
			if( ent:IsVehicle() ) then
				if( ply:GetPos():Distance( ent:GetPos() ) <= PutAwayRadius ) then
					ply.HasCar = false
					ply.OwnedCar = nil
					ent:Remove()
					return
				end
			end
		end
		ply:PrintMessage( HUD_PRINTTALK, "You need to bring your car to me if you want me to put it away." )
	end
end )