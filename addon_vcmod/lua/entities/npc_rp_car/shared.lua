ENT.Base = "base_ai" 
ENT.Type = "ai"
ENT.PrintName		= "Car NPC"
ENT.Author			= "freemmaann"
ENT.Category		= "VCMod"
ENT.Contact			= "N/A"
ENT.Purpose			= "N/A"
ENT.Instructions	= "Press E"
ENT.Spawnable		= true
ENT.AdminSpawnable		= true
ENT.AutomaticFrameAdvance = true

function ENT:SetAutomaticFrameAdvance( bUsingAnim )
	self.AutomaticFrameAdvance = bUsingAnim
end

cars = {}

local function AddCar( name, info, model, class, script, price, vip )
	table.insert( cars, { name = name, info = info, model = model, class = class, script = script, price = price, vip = vip } )
end

Car_SpawnPosition = Vector(0,0,0)
Car_SpawnAngle = Angle(0,0,0)

//AddCar( name, info, model, class, script, price, vip )
AddCar("Jeep", "", "models/buggy.mdl", "prop_vehicle_jeep", "scripts/vehicles/jeep_test.txt", 2000, false)
AddCar("Audi TT", "", "models/tdmcars/auditt.mdl", "prop_vehicle_jeep", "scripts/vehicles/TDMCars/auditt.txt", 30000, false)
AddCar("BMW", "", "models/tdmcars/bmwm5e60.mdl", "prop_vehicle_jeep", "scripts/vehicles/TDMCars/bmwm5e60.txt", 70000, false)
AddCar("Citroen C4", "", "models/tdmcars/cit_c4.mdl", "prop_vehicle_jeep", "scripts/vehicles/TDMCars/c4.txt", 80000, false)
AddCar("Chevelle SS", "", "models/tdmcars/chevelless.mdl", "prop_vehicle_jeep", "scripts/vehicles/TDMCars/chevelless.txt", 100000, false)
AddCar("Focus", "", "models/tdmcars/focusrs.mdl", "prop_vehicle_jeep", "scripts/vehicles/TDMCars/focusrs.txt", 150000, false)
AddCar("Charger", "", "models/tdmcars/chargersrt8.mdl", "prop_vehicle_jeep", "scripts/vehicles/TDMCars/chargersrt8.txt", 200000, false)
AddCar("Mazda RX8", "", "models/tdmcars/rx8.mdl", "prop_vehicle_jeep", "scripts/vehicles/TDMCars/rx8.txt", 300000, false)
AddCar("Wrangler", "", "models/tdmcars/wrangler.mdl", "prop_vehicle_jeep", "scripts/vehicles/TDMCars/wrangler.txt", 400000, false)
AddCar("Skyline GTR", "", "models/tdmcars/skyline_r34.mdl", "prop_vehicle_jeep", "scripts/vehicles/TDMCars/skyline_r34.txt", 500000, false)
AddCar("Porsche 997GT", "VIP Car", "models/tdmcars/997gt3.mdl", "prop_vehicle_jeep", "scripts/vehicles/TDMCars/997gt3.txt", 3000, true)
AddCar("Ferrari 512", "VIP Car", "models/tdmcars/ferrari512tr.mdl", "prop_vehicle_jeep", "scripts/vehicles/TDMCars/ferrari512tr.txt", 500000, true)
AddCar("Ferrari 250GT", "VIP Car", "models/tdmcars/ferrari250gt.mdl", "prop_vehicle_jeep", "scripts/vehicles/TDMCars/ferrari250gt.txt", 100000, true)
