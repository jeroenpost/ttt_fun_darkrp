VCMod (coderhire version)

*****************************************************************

To apply a vehicle with a custom script for lights, seats, exhaust you have to alter the vehicles spawn script (lua/autorun/YOURCAR.lua).

Example taken from one of TDM vehicles:

*****************************************************************Example start.
local V = {
			Name = "Volkswagen Golf MK3", 
			Class = "prop_vehicle_jeep",
			Category = "TDM Cars",
			Author = "TheDanishMaster, freemmaann, Turn 10",
			Information = "A drivable VW Golf MK3 by TheDanishMaster",
			Model = "models/tdmcars/golf3.mdl",
			//Vehicle Controller
			VC_Lights = { //Pos can be a simple Vector() relative to the vehicle or an attachment name, else is self explanatory, can be an infinite amount of these.
						{Pos = Vector(-30.8, -94.9, 37.1), Mat = "sprites/glow1.vmt", Alpha = 180, Size = 0.8, DynLight = true, NormalColor = "255 0 0", BrakeColor = "255 0 0"},
						{Pos = Vector(30.8, -94.9, 37.1), Mat = "sprites/glow1.vmt", Alpha = 180, Size = 0.8, DynLight = true, NormalColor = "255 0 0", BrakeColor = "255 0 0"},
						{Pos = Vector(-31, -93.5, 43.5), Mat = "sprites/glow1.vmt", Alpha = 180, Size = 0.6, DynLight = true, BlinkersColor = "255 130 0"},
						{Pos = Vector(31, -93.5, 43.5), Mat = "sprites/glow1.vmt", Alpha = 180, Size = 0.6, DynLight = true, BlinkersColor = "255 130 0"},
						{Pos = Vector(-32.1, 94.4, 19.3), Mat = "sprites/glow1.vmt", Alpha = 220, Size = 0.5, DynLight = true, BlinkersColor = "255 130 0"},
						{Pos = Vector(32.1, 94.4, 19.3), Mat = "sprites/glow1.vmt", Alpha = 220, Size = 0.5, DynLight = true, BlinkersColor = "255 130 0"},
						{Pos = Vector(-20.5, 96.4, 19.5), Mat = "sprites/glow1.vmt", Alpha = 180, Size = 0.5, DynLight = true, NormalColor = "255 255 255"},
						{Pos = Vector(20.5, 96.4, 19.5), Mat = "sprites/glow1.vmt", Alpha = 180, Size = 0.5, DynLight = true, NormalColor = "255 255 255"},
						{Pos = Vector(-26.2, 95.4, 19.2), Mat = "sprites/glow1.vmt", Alpha = 180, Size = 0.5, DynLight = true, NormalColor = "255 255 255"},
						{Pos = Vector(26.2, 95.4, 19.2), Mat = "sprites/glow1.vmt", Alpha = 180, Size = 0.5, DynLight = true, NormalColor = "255 255 255"},
						{Pos = Vector(-21.5, 91, 29.8), Mat = "sprites/glow1.vmt", Alpha = 180, Size = 0.6, DynLight = true, NormalColor = "255 255 255"},
						{Pos = Vector(21.5, 91, 29.8), Mat = "sprites/glow1.vmt", Alpha = 180, Size = 0.6, DynLight = true, NormalColor = "255 255 255"},
						{Pos = Vector(-29.2, -93, 40.8), Mat = "sprites/glow1.vmt", Alpha = 180, Size = 0.5, DynLight = true, ReverseColor = "255 255 255"},
						{Pos = Vector(29.2, -93, 40.8), Mat = "sprites/glow1.vmt", Alpha = 180, Size = 0.5, DynLight = true, ReverseColor = "255 255 255"},


						{Pos = Vector(-28.5, 85, 29.8), Size = 1, GlowSize = 1, HeadLightAngle = Angle(-5, 95, 0)},
						{Pos = Vector(28.5, 85, 29.8), Size = 1, GlowSize = 1, HeadLightAngle = Angle(-5, 85, 0)}
						},
			VC_Exhaust_Dissipate = true,
			VC_Exhaust = { //Exhaust effect, only active when engine is on, can be infinite amount.
						{Pos = Vector(-15, -95, 11.9), Ang = Angle(0,0,90), EffectIdle = "Exhaust", EffectStress = "Exhaust"},
						{Pos = Vector(-15, -95, 11.9), Ang = Angle(0,0,90), EffectIdle = "Exhaust", EffectStress = "Exhaust"}
						},
			VC_ExtraSeats = { //Can be an infinite amount of seats, Pos and ExitPos can be a simple Vector() or an attachment name, other options are self explanatory.
						{Pos = Vector(18, -7, 22), Ang = Angle(0, 0, 8), EnterRange = 80, ExitAng = Angle(0, -90, 0), Model = "models/props_phx/carseat2.mdl", ModelOffset = Vector(12, 0, 4), Hide = true, DoorSounds = true, RadioControl = true},
						{Pos = Vector(18, -42, 22), Ang = Angle(0, 0, 8), EnterRange = 80, ExitAng = Angle(0, -90, 0), Model = "models/props_phx/carseat2.mdl", ModelOffset = Vector(12, 0, 4), Hide = true, DoorSounds = true, RadioControl = false},
						{Pos = Vector(-18, -42, 22), Ang = Angle(0, 0, 8), EnterRange = 80, ExitAng = Angle(0, -90, 0), Model = "models/props_phx/carseat2.mdl", ModelOffset = Vector(12, 0, 4), Hide = true, DoorSounds = true, RadioControl = false}
						},
			VC_Horn = {Sound = "vehicles/vc_horn_light.wav", Pitch = 100, Looping = false}, //Horn sound the car will use.
						
			KeyValues = {
							vehiclescript	=	"scripts/vehicles/TDMCars/golf3.txt"
							}
			}
list.Set("Vehicles", "golf3tdm", V)
*****************************************************************Example end.

For the Vector()'s use the tool included with the addon Utilities/VCMod's tools/Positioning.

You must include a function at the end of the car setup in order for the script to update after the first creation.

*****************************************************************Example start.
			VC_Horn = {Sound = "vehicles/vc_horn_light.wav", Pitch = 100, Looping = false}, //Horn sound the car will use.
						
			KeyValues = {
							vehiclescript	=	"scripts/vehicles/TDMCars/golf3.txt"
							}
			}
list.Set("Vehicles", "golf3tdm", V)

VC_MakeScripts("golf3tdm")
*****************************************************************Example end.

This function will create a brand new script in data/vcmod_old/ folder, if this function is not here, it will not recreate the script if it is already there.