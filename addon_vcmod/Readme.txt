VCMod (coderhire version)

*****************************************************************

If its laggy for you:

You can reduce the lag by using 2 console commands:
"VC_DynamicLights" - turns off the glow onto world from any car lights, Clientside, anyone can use the command.
"VC_HeadLights" - turns off the projected texture, headlights, Serverside, admins can set this if they see the server is too laggy.

Using these two commands the lights dont have to be turned off for the server not to lag anymore.

*****************************************************************

Instalation:

Place into "*/GarrysMod/addons/".

*****************************************************************

DarkRP compatibility:

Car dealer NPC auto spawns in the position you specify. Check the "/autorun/autorun/VC_DarkRP_NPC.lua" file.