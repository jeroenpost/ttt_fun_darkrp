AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
 
include('shared.lua')
 
concommand.Add("slotmachine_roll",function(ply,cmd,args)
        local Entity = ents.GetByIndex(args[1])
        if not ply.NextBetSlot then ply.NextBetSlot = 0 end
        if !Entity or !Entity:IsValid() or ply.NextBetSlot > CurTime() then return end
        ply.NextBetSlot = CurTime() + 0.25
        Entity:Bet(ply)
end)
 
function ENT:Initialize( )
        self.Entity:SetModel("models/props_borealis/bluebarrel001.mdl")
        self.Entity:PhysicsInit(SOLID_VPHYSICS)
        self.Entity:SetMoveType(MOVETYPE_NONE)
        self.Entity:SetSolid(SOLID_VPHYSICS)
		
        self:DrawShadow(false)
       
        self:SetUseType( SIMPLE_USE )
       
        self:SetNWInt(1,1)
        self:SetNWInt(2,1)
        self:SetNWInt(3,1)
       
        self.RPOriginalMass = 1000
       
        ------ For DarkRP : Not to server to remove atm when you leave the server
        timer.Simple(1,function()
                if self and self:IsValid() then
                        self.SID = nil
                        self.FPPOwnerID = nil
                end
        end)
        -------------------------------------------------------------
end
 
        function ENT:Bet(ply)
                if self.Betting or not ply:Alive() then return end
                if ply:SM_GetPlayerMoneyM() < SlotMachine_Adjust.BetPrice then return end
                ply:SM_AddPlayerMoneyM(-SlotMachine_Adjust.BetPrice)
                self.Betting = ply
                self:EmitSound("ambient/levels/labs/coinslot1.wav")
				
				hook.Run("SlotMachine_OnBet",self.Betting,SlotMachine_Adjust.BetPrice)
               
                self:SetNWInt(1,0)
                self:SetNWInt(2,0)
                self:SetNWInt(3,0)
               
                // Check Result.
                        self.Success = false
                        -- Success.
                        if math.random(1,100) <= SlotMachine_Adjust.WinRate then
                                self.Success = true
                        else -- fail
                       
                        end
               
                        local function GetPriceNum()
                                for Num,DB in pairs(SlotMachine_Adjust.Slots) do
                                        if math.random(1,100) <= DB.Rate then
                                                return Num
                                        end
                                end
                                return GetPriceNum()
                        end
                       
                        local function Generate_non_win_value()
                                local V1 = math.random(1,#SlotMachine_Adjust.Slots)
                                local V2 = math.random(1,#SlotMachine_Adjust.Slots)
                                local V3 = math.random(1,#SlotMachine_Adjust.Slots)
                                if V1 == V2 and V2 == V3 then
                                        return Generate_non_win_value()
                                else
                                        return V1,V2,V3
                                end
                        end
               
                        local Result = {}
                        if self.Success then
                                local ResultK = GetPriceNum()
                                Result[1] = ResultK
                                Result[2] = ResultK
                                Result[3] = ResultK
                        else
                                local V1,V2,V3 = Generate_non_win_value()
                                Result[1] = V1
                                Result[2] = V2
                                Result[3] = V3
                        end
                for k=1,3 do
                        timer.Simple(k,function()
                                if self and self:IsValid() then
                                        self:SetNWInt(k,Result[k])
                                        if k == 3 then
                                                self:CheckResult()
                                        end
                                end
                        end)
                end
        end
        function ENT:CheckResult()
                local V1,V2,V3 = self:GetNWInt(1),self:GetNWInt(2),self:GetNWInt(3)
                if V1 == V2 and V2 == V3 then
                       if self.Betting and self.Betting:IsValid() then
							self.Betting:SM_AddPlayerMoneyM(SlotMachine_Adjust.Slots[V1].Price)
                            local XP = (tonumber(SlotMachine_Adjust.Slots[V1].Price)/25) * self.Betting:getDarkRPVar('level') or 1
                                   self.Betting:addXP(XP,true)
							self.Betting:SM_Notify("You win! you got $".. SlotMachine_Adjust.Slots[V1].Price.. " dollar and "..XP.." XP")

                            hook.Call("winslotmachineprice",GAMEMODE,self.Betting)
                            if SlotMachine_Adjust.Slots[V1].Mainprice then
                                 hook.Call("winbiggestslotmachineprice",GAMEMODE,self.Betting)
                            end
                            hook.Call("casinoowner_getmoney",GAMEMODE, -125)
                            hook.Run("SlotMachine_OnGetReward",self.Betting,V1,self)
                       end
                else
                    hook.Call("casinoowner_getmoney",GAMEMODE, 35)
                end
                self.Betting = false
        end
 
function ENT:PhysgunPickup(ply) -- for DarkRP : Allows Owner can move ATM
        if ply:SM_IsAdmin() then
                return true
        end
end
 
function ENT:CanTool(ply, trace, mode) -- for DarkRP : Allows Only Owner to remove ATM.
        if ply:SM_IsAdmin() then
                return true
        else
                return false
        end
end