You can find the original script from CoderHire here: http://coderhire.com/scripts/view/932  
There you can find media, old comments and reviews.

##DarkRP Support##
DarkRP 2.4.3 ![Alt text](http://s3-eu-west-1.amazonaws.com/coderhire-ratings/991eb9bb1af4b20ea9f8a654cf571a59.png)  
DarkRP 2.5.0 ![Alt text](http://s3-eu-west-1.amazonaws.com/coderhire-ratings/991eb9bb1af4b20ea9f8a654cf571a59.png)  

Other custom versions of DarkRP are not supported, and don't count on my support if you're using a custom version of DarkRP with modified core features.

**This script is fully supported by [VCMod](http://coderhire.com/scripts/view/652).**  
**This script is not supported by Passenger Mod.**

##Description##
This script allows your government jobs (or any job really) to retrieve vehicles from an NPC.  
This has many great uses such as giving your police officer a car to cruise around in, to catch the criminals, or your paramedics to retrieve an ambulance and get to people faster.  The vgui is greatly inspired by my [DarkRP NPC Shop](http://coderhire.com/scripts/view/662), and these two addons works very well together!  
Below is a great list of features.  

##Features##
- ULX rank restriction.
- NPC with indicator.  
- Simple config file with multiply options to edit your version.  
- Remove old government vehicles from the menu, when the player disconnects, or when he/she changes jobs.  
- Custom vehicle exist so you don't get stuck in the side of the vehicle.  
- Easy configuration to add as many vehicles for all the teams you want.  
- Comes with 5 default vehicles already set up.  
- And much more!

##Installation##
This addon supports both DarkRP 2.4.3 and DarkRP 2.5.0.  
**You will find the version for DarkRP 2.4.3 here:**  
DarkRP Government Vehicle Station/GovStation 2.4.3/DarkRP Government Vehicle Station  
Extract the last DarkRP Government Vehicle Station to your addons.  

**You will find the version for DarkRP 2.5.0 here:**  
DarkRP Government Vehicle Station/GovStation 2.5.0/DarkRP Government Vehicle Station  
Extract the last DarkRP Government Vehicle Station to your addons.   

Once you've done that, you will want to set up the npc and the vehicle spawn.  

All you have to do, is go to the locations you want the npc to spawn at, and type "govstation\_setpos" into your console.  
Once you have done that, you will need to restart your server for the NPC to spawn at the new location.  

To set up the location for the vehicles to spawn at, follow the next step.  
Go the the location you want it to be at and type "govvehicle\_setpos" into your console.  
There is no restart required for setting the vehicle spawn position.  

Remember to aim in the right direction, so you get the right angle on these two things.  

You must be an administrator on your server to perform these actions.  

##Content##
This addon obviously needs some content.  
The cars used in this addon is Sickness Models. But any vehicle can pretty much be used in the addon. However, no SCar support!  

To download Sickness Models, install this with Tortoise SVN.  
Link to Tortoise SVN: http://tortoisesvn.net/downloads.html  
Link to Sickness Models: http://subversion.assembla.com/svn/SicknessModel  

There is no content included in the download. It is up to you to put Sickness Models on your FastDL!  
Please read the "README TO FIX MISSING SOUNDS!!!!!" in Sickness Models to install sounds and vehicle scripts.  

##Customizing##
To customize the addons settings, go to DarkRP Government Vehicle Station/lua/autorun/govstation\_config.lua.  
In the top of the file you can customize the settings shown below.  

GovStation\_MaxVehicles = 10 -- The maximum amount of vehicles allowed to be spawned by government officials. [Default = 10]  
GovStation\_NPCModel = "models/breen.mdl" -- The model of the NPC.  
GovStation\_RestrictToULXRanks = false -- Weather to restrict vehicles to specific ULX ranks. [Default = false]  

There is also two tables you need to fill out with vehicle models and the teams you use as government teams. All of this is documented in the download!  
Here is an example of a vehicle.  

GovStation\_Vehicles["GOV\_POLICE"] = { -- UNIQUE name for the vehicle!  
	Name = "Police Cruiser", -- Name of the vehicle in the menu.  
	Description = "A standart police cruiser.", -- Description of the vehicle in the menu.  
	Model = "models/sickness/lcpddr.mdl", -- The model of the vehicle.  
	Script = "scripts/vehicles/ecpd01.txt", -- The vehicle script. Can be found in sickness models/scripts/vehicles.  
	Health = 100, -- The amount of health the vehicle has (only works if you have a vehicle damage script).  
	VehicleSkin = 0, -- The vehicle skin. Some vehicles has multiply skins.  
	AllowedTeamNames = { -- Here is a list of the jobs that can get this vehicle. These names shows in the menu.  
		"Police Officer",  
		"Police Chief",  
	},  
	AllowedTeams = { -- Here is another list of the jobs that can get this vehicle. These are the actual names of the jobs from your darkrp file.  
		TEAM\_POLICE,  
		TEAM\_CHIEF,  
	},  
	ULXRanksAllowed = { -- Here is a list of the ulx ranks that a person has to be (just one of them), to retrieve this vehicle. The job is also still required.  
		"user",  
		"admin",  
	},  
}  

##Errors & Support##
If you find any problems with the script, please PM me with details of the situation and a copypaste of the error in console. Additionally, i rarely give additional support for my scripts. If there is a general error with the script, an error that you can prove happens, and is my fault. Send me a PM here on CoderHire. We'll figure it out there, and perhaps a chat on Steam after I've responded to your PM. I am also not interested in modifying you a custom version of the addon. Also not upon payment. Sorry!

Conflicting addons is not to be said if I will support that or not. This is something I will decide upon confrontation about a conflicting addon. If you have some sort of proof that an addon is conflicting with my addon, please send me a PM with the details you might have.

Thank you!