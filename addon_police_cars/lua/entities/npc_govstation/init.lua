AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

local GovStationNPC
function SpawnShopNPC()	
	if not file.IsDir("craphead_scripts", "DATA") then
		file.CreateDir("craphead_scripts", "DATA")
	end
	
	if not file.IsDir("craphead_scripts/gov_station/".. string.lower(game.GetMap()) .."", "DATA") then
		file.CreateDir("craphead_scripts/gov_station/".. string.lower(game.GetMap()) .."", "DATA")
	end
	
	if not file.Exists( "craphead_scripts/gov_station/".. string.lower(game.GetMap()) .."/govstation_location.txt", "DATA" ) then
		file.Write("craphead_scripts/gov_station/".. string.lower(game.GetMap()) .."/govstation_location.txt", "0;-0;-0;0;0;0", "DATA")
	end
	
	local PositionFile = file.Read("craphead_scripts/gov_station/".. string.lower(game.GetMap()) .."/govstation_location.txt", "DATA")
	 
	local ThePosition = string.Explode( ";", PositionFile )
		
	local TheVector = Vector(ThePosition[1], ThePosition[2], ThePosition[3])
	local TheAngle = Angle(tonumber(ThePosition[4]), ThePosition[5], ThePosition[6])
		
	GovStationNPC = ents.Create("npc_govstation")
	GovStationNPC:SetModel(GovStation_NPCModel)
	GovStationNPC:SetPos(TheVector)
	GovStationNPC:SetAngles(TheAngle)

	GovStationNPC:Spawn()
	GovStationNPC:SetMoveType(MOVETYPE_NONE)
	GovStationNPC:SetSolid( SOLID_BBOX )
	GovStationNPC:SetCollisionGroup(COLLISION_GROUP_PLAYER)


end
--timer.Simple(1, SpawnShopNPC)

function ENT:Initialize()
    self:SetModel(GovStation_NPCModel)
    self:SetMoveType(MOVETYPE_NONE)
    self:SetSolid( SOLID_BBOX )
    self:SetCollisionGroup(COLLISION_GROUP_PLAYER)
end

function ENT:AcceptInput(ply, caller)
	if caller:IsPlayer() && !caller.CantUse && !caller.MenuOpen then
		caller.CantUse = true
		caller.MenuOpen = true
		timer.Simple(3, function()  caller.CantUse = false end)

		if caller:IsValid() then
			umsg.Start("CH_GovStation", caller)
			umsg.End()
		end
	end
end

util.AddNetworkString("CH_CloseVehicleMenu")
net.Receive("CH_CloseVehicleMenu", function(length, ply)
	ply.MenuOpen = false
end)