if not file.Exists( "craphead_scripts/gov_station/".. string.lower(game.GetMap()) .."/govvehicle_location.txt", "DATA" ) then
	file.Write("craphead_scripts/gov_station/".. string.lower(game.GetMap()) .."/govvehicle_location.txt", "0;-0;-0;0;0;0", "DATA")
end

function CH_GovStation_Position( ply, cmd )
	if ply:IsAdmin() then
		local HisVector = string.Explode(" ", tostring(ply:GetPos()))
		local HisAngles = string.Explode(" ", tostring(ply:GetAngles()))
		
		file.Write("craphead_scripts/gov_station/".. string.lower(game.GetMap()) .."/govstation_location.txt", ""..(HisVector[1])..";"..(HisVector[2])..";"..(HisVector[3])..";"..(HisAngles[1])..";"..(HisAngles[2])..";"..(HisAngles[3]).."", "DATA")
		ply:ChatPrint("New position for the government station has been succesfully set. Please restart your server!")
	else
		ply:ChatPrint("Only administrators can perform this action")
	end
end
concommand.Add("govstation_setpos", CH_GovStation_Position)

function CH_GovVehicle_Position( ply )
	if ply:IsAdmin() then
		local HisVector = string.Explode(" ", tostring(ply:GetPos()))
		local HisAngles = string.Explode(" ", tostring(ply:GetAngles()))
		
		file.Write("craphead_scripts/gov_station/".. string.lower(game.GetMap()) .."/govvehicle_location.txt", ""..(HisVector[1])..";"..(HisVector[2])..";"..(HisVector[3])..";"..(HisAngles[1])..";"..(HisAngles[2])..";"..(HisAngles[3]).."", "DATA")
		ply:ChatPrint("New position for the government vehicles has been succesfully set. The new position is now in effect!")
	else
		ply:ChatPrint("Only administrators can perform this action")
	end
end
concommand.Add("govvehicle_setpos", CH_GovVehicle_Position)

local CurGovVehicles = 0

util.AddNetworkString("GovStation_SpawnVehicle")
net.Receive("GovStation_SpawnVehicle", function(length, ply)
	local item = net.ReadString()
    if not GovStation_Vehicles[item] then return end
	
	local Name = GovStation_Vehicles[item].Name
	local Model = GovStation_Vehicles[item].Model
	local Script = GovStation_Vehicles[item].Script
	local Health = GovStation_Vehicles[item].Health
	local Skin = GovStation_Vehicles[item].VehicleSkin
	local Limit = GovStation_Vehicles[item].Limit
	local AllowedTeams = GovStation_Vehicles[item].AllowedTeams
	
	if ply:InVehicle() then return false end
	
	if ply.HasGovVehicle then
		DarkRP.notify(ply, 1, 5, "You already own a government vehicle!")
		return
	end
	
	if CurGovVehicles == GovStation_MaxVehicles then
		DarkRP.notify(ply, 1, 5, "The limitation of maximum  government vehicles has been reached!")
		return
	end
	
	if not table.HasValue( AllowedTeams, ply:Team() ) then
		DarkRP.notify(ply, 1, 5, "You can not retrieve the ".. Name .." as a ".. team.GetName(ply:Team()) .."!")
		return
	end
	
	if GovStation_RestrictToULXRanks then
		if not table.HasValue( ULXRanksAllowed, ply:GetUserGroup() ) then
			DarkRP.notify(ply, 1, 5, "You are not the required ulx rank to retrieve the ".. Name .."!")
			return
		end
	end
	
	DarkRP.notify(ply, 1, 5, "You have retrieved the ".. Name .."!")
	
	local PositionFile = GovStationVehSpawn[string.lower(game.GetMap())]
	local ThePosition = string.Explode( ";", PositionFile )
	local TheVector = Vector(ThePosition[1], ThePosition[2], ThePosition[3])
	local TheAngle = Angle(tonumber(ThePosition[4]), ThePosition[5], ThePosition[6])

	local GovVehicle = ents.Create( "prop_vehicle_jeep" )
	GovVehicle:SetKeyValue( "vehiclescript", Script )
	GovVehicle:SetPos( TheVector )
	GovVehicle:SetAngles( TheAngle )
	GovVehicle:SetRenderMode(RENDERMODE_TRANSADDFRAMEBLEND)
	GovVehicle:SetModel( Model )
	GovVehicle:SetSkin( Skin ) 
	GovVehicle:Spawn()
	GovVehicle:Activate()
	GovVehicle:SetHealth( Health ) 
	GovVehicle:SetNWInt( "Owner", ply:EntIndex() )
	GovVehicle:keysOwn( ply )
    GovVehicle.isGovVehicle = true
	
	ply.HasGovVehicle = true
	CurGovVehicles = CurGovVehicles + 1
end)

util.AddNetworkString("GovVehicle_RemoveCurrent")
net.Receive("GovVehicle_RemoveCurrent", function(length, ply)
	
	if ply.HasGovVehicle then
		for _, ent in pairs(ents.GetAll()) do
			if table.HasValue( GovStation_VehiclesUsed, ent:GetModel() ) then
				if ent:GetNWInt("Owner") == ply:EntIndex() then
					ent:Remove()
					DarkRP.notify(ply, 1, 5, "Your government vehicle has been removed!")
				end
			end
		end
	else
		DarkRP.notify(ply, 1, 5, "You don't have a government vehicle!")
	end

end)

function GovVehicle_Removal( ent )
    if  istable(GovStation_VehiclesUsed) then
        if table.HasValue( GovStation_VehiclesUsed, ent:GetModel() ) then
            player.GetByID(ent:GetNWInt("Owner")).HasGovVehicle = false
            CurGovVehicles = CurGovVehicles - 1
        end
    end
end
hook.Add("EntityRemoved", "GovVehicle_Removal", GovVehicle_Removal)

function GovVehicle_Disconnect( ply )
	for _, ent in pairs(ents.GetAll()) do
		if table.HasValue( GovStation_VehiclesUsed, ent:GetModel() ) then
			if ent:GetNWInt("Owner") == ply:EntIndex() then
				ent:Remove()
			end
		end
	end
end
hook.Add("PlayerDisconnected", "GovVehicle_Disconnect", GovVehicle_Disconnect)

function GovVehicle_JobChange( ply )
	for _, ent in pairs(ents.FindByClass("prop_vehicle_jeep")) do
		if not table.HasValue( GovStation_TeamsUsed, ply:Team() ) then
			if ply.HasGovVehicle then
				if ent:GetNWInt("Owner") == ply:EntIndex() then
					if ent:IsValid() then
						ent:Remove()
					end
				end
			end
		end
	end
end


function GovVehicle_CustomExit(ply, vehicle)
	if table.HasValue( GovStation_VehiclesUsed, vehicle:GetModel() ) then
		ply:SetPos( vehicle:GetPos() + Vector(-90,125,20) )
	end
end
hook.Add("PlayerLeaveVehicle", "GovVehicle_CustomExit", GovVehicle_CustomExit)