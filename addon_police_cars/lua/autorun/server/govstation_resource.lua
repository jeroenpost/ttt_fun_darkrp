-- This resource file includes resource.AddFile for all the default vehicles in this script! Both models and materials!
function GOVSTATION_AddDir( dir )
    local list = file.Find( dir.."/*", "DATA" )
    for _, fdir in pairs( list ) do
        if fdir != ".svn" then
            GOVSTATION_AddDir( dir.."/"..fdir )
        end
    end
  
    for k,v in pairs( file.Find( dir.."/*", "GAME") ) do
        resource.AddFile( dir.."/"..v )
    end
end

GOVSTATION_AddDir("materials/models/gtaiv/vehicles")

-- SWAT Truck
resource.AddFile("models/sickness/stockade2dr.dx80")
resource.AddFile("models/sickness/stockade2dr.dx90")
resource.AddFile("models/sickness/stockade2dr.mdl")
resource.AddFile("models/sickness/stockade2dr.phy")
resource.AddFile("models/sickness/stockade2dr.sw")
resource.AddFile("models/sickness/stockade2dr.vvd")

GOVSTATION_AddDir("materials/models/gtaiv/vehicles/stockade")
GOVSTATION_AddDir("materials/models/gtaiv/vehicles/ecpd")
GOVSTATION_AddDir("materials/models/gtaiv/vehicles/stallion")

