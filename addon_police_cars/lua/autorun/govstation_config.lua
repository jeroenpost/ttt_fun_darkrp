--[[
Below is the configs.
Modify these to your needs. Please read all the text in green (notepad++). If the text is not colored, read all the text notes (they are marked like this: -- text).
It's important that you read all the notes before you modify this config file!
--]]

GovStation_MaxVehicles = 6 -- The maximum amount of vehicles allowed to be spawned by government officials. [Default = 10]
GovStation_NPCModel = "models/breen.mdl" -- The model of the NPC.
GovStation_RestrictToULXRanks = false -- Weather to restrict vehicles to specific ULX ranks. [Default = false]

timer.Simple(5, function() 

GovStation_VehiclesUsed = { -- All the vehicle models you are using in the "Model" part below should be put in this table below!
    "models/sickness/towtruckdr.mdl"
    ,"models/sentry/crownviccvpi.mdl",
    "models/sentry/07crownvic_cvpi.mdl",
    "models/sentry/crownvicuc.mdl",
    "models/tdmcars/bus.mdl",
    "models/sickness/truckfire.mdl",
	"models/sickness/stockade2dr.mdl" -- THE LAST LINE SHOULD NOT HAVE A COMMA AT THE END. BE AWARE OF THIS WHEN EDITING THIS!
}

GovStation_TeamsUsed = { -- All the teams you are using under AllowedTeams should be put in this table below!
	TEAM_SWAT,
	TEAM_POLICE,
	TEAM_MEDIC,
	TEAM_SECRETSERVICE,
	TEAM_CHIEF,
    TEAM_FIREFIGHTER,
	TEAM_TOWTRUCK,
    TEAM_BUSDRIVER
}

--[[
Below is the table of vehicles.
Customize the list below to add new vehicles. Make sure you follow the instructions correctly.

Example item. Explenation of what each line does.

GovStation_Vehicles["GOV_POLICE"] = { -- UNIQUE name for the vehicle!  
	Name = "Police Cruiser", -- Name of the vehicle in the menu.  
	Description = "A standart police cruiser.", -- Description of the vehicle in the menu.  
	Model = "models/sickness/lcpddr.mdl", -- The model of the vehicle.  
	Script = "scripts/vehicles/ecpd01.txt", -- The vehicle script. Can be found in sickness models/scripts/vehicles.  
	Health = 100, -- The amount of health the vehicle has (only works if you have a vehicle damage script).  
	VehicleSkin = 0, -- The vehicle skin. Some vehicles has multiply skins.  
	AllowedTeamNames = { -- Here is a list of the jobs that can get this vehicle. These names shows in the menu.  
		"Police Officer",
		"Police Chief",
	},
	AllowedTeams = { -- Here is another list of the jobs that can get this vehicle. These are the actual names of the jobs from your darkrp file.  
		TEAM_POLICE,
		TEAM_CHIEF,
	},
	ULXRanksAllowed = { -- Here is a list of the ulx ranks that a person has to be (just one of them), to retrieve this vehicle. The job is also still required.
		"user",
		"admin",
	},
}

--]]
GovStationVehSpawn = {}
GovStationVehSpawn["rp_downtown_v4c_v2"] = "-2378.452393;-983.456238;-192.501022;0.000;-90.647;0.000"
GovStationVehSpawn["rp_downtown_v4c_v3_chirax"] = "-3134;-464;-85;0.000;-90.647;0.000"
GovStationVehSpawn["rp_downtown_v4c_v2_drp_ext"] = "891;2520;-132;50.000;-270.647;0.000"

GovStation_Vehicles = {}

GovStation_Vehicles["GOV_POLICE"] = { 
	Name = "Police Cruiser",
	Description = "A standard police cruiser.",
	Model = "models/sentry/crownviccvpi.mdl",
	Script = "scripts/vehicles/sentry/07crownvicpolice.txt",
	Health = 100,
	VehicleSkin = 0,
	AllowedTeamNames = {
		"Police Officer",
		"Police Chief",
	},
	AllowedTeams = {
		TEAM_POLICE,
		TEAM_CHIEF,
	},
	ULXRanksAllowed = {
		"user",
		"admin",
	},
}

GovStation_Vehicles["GOV_POLICECHIEF"] = { 
	Name = "Police Chief Cruiser",
	Description = "A police Interceptor for the police chief.",
	Model = "models/sentry/07crownvic_cvpi.mdl",
	Script = "scripts/vehicles/sentry/07crownvicpolice.txt",
	Health = 100,
	VehicleSkin = 0,
	AllowedTeamNames = {
		"Police Chief",
	},
	AllowedTeams = {
		TEAM_CHIEF,
	},
	ULXRanksAllowed = {
		"user",
	},
}



GovStation_Vehicles["GOV_SWAT"] = { 
	Name = "S.W.A.T Truck",
	Description = "An armored swat unit truck.",
	Model = "models/sickness/stockade2dr.mdl",
	Script = "scripts/vehicles/stockade.txt",
	Health = 400,
	VehicleSkin = 0,
	AllowedTeamNames = {
		"S.W.A.T",
	},
	AllowedTeams = {
		TEAM_SWAT,
	},
	ULXRanksAllowed = {
		"user",
	},
}

GovStation_Vehicles["GOV_SECRETSERVICE"] = {
    Name = "Secret Service Vehicle ",
    Description = "An undercover buffalo for the secret service agents.",
    Model =  "models/sentry/crownvicuc.mdl",
    Script = "scripts/vehicles/sentry/crownvicpolice.txt",
    Health = 100,
    VehicleSkin = 0,
    AllowedTeamNames = {
        "Secret Service Agent",
    },
    AllowedTeams = {
        TEAM_SECRET_AGENT,
    },
    ULXRanksAllowed = {
        "user",
    },
}

GovStation_Vehicles["GOV_SECRETSERVICE2"] = {
    Name = "Secret Service Vehicle 2",
    Description = "An undercover buffalo for the secret service agents.",
    Model =  "models/sentry/07crownvic_uc.mdl",
    Script = "scripts/vehicles/sentry/crownvicpolice.txt",
    Health = 100,
    VehicleSkin = 0,
    AllowedTeamNames = {
        "Secret Service Agent",
    },
    AllowedTeams = {
        TEAM_SECRET_AGENT,
    },
    ULXRanksAllowed = {
        "user",
    },
}

GovStation_Vehicles["GOV_BUS"] = {
    Name = "City Bus",
    Description = "City Bus to transport people around town",
    Model = "models/tdmcars/bus.mdl",
    Script = "scripts/vehicles/TDMCars/bus.txt",
    Health = 400,
    VehicleSkin = 0,
    AllowedTeamNames = {
        "Busdriver",
    },
    AllowedTeams = {
        TEAM_BUSDRIVER,
    },
    ULXRanksAllowed = {
        "user",
    },
}

GovStation_Vehicles["GOV_FIRE"] = {
    Name = "Firetruck",
    Description = "FireTruck, used to extingish fires",
    Model = "models/sickness/truckfire.mdl",
    Script = "scripts/vehicles/firetruck.txt",
    Health = 600,
    VehicleSkin = 0,
    AllowedTeamNames = {
        "FireFighter",
    },
    AllowedTeams = {
        TEAM_FIREFIGHTER,
    },
    ULXRanksAllowed = {
        "user",
    },
}

GovStation_Vehicles["GOV_TOW"] = {
    Name = "TowTruck",
    Description = "Used to tow other cars",
    Model = "models/sickness/towtruckdr.mdl",
    Script = "scripts/vehicles/tow.txt",
    Health = 200,
    VehicleSkin = 0,
    AllowedTeamNames = {
        "TowTruck Driver",
    },
    AllowedTeams = {
        TEAM_TOWER,
    },
    ULXRanksAllowed = {
        "user",
    },
}

end)