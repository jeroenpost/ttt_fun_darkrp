timer.Simple(5, function() 

surface.CreateFont("GOV_UiBold", {
	font = "Tahoma", 
	size = 13, 
	weight = 700
})

surface.CreateFont("GOV_Trebuchet24", {
	font = "Trebuchet MS", 
	size = 24, 
	weight = 900
})
	
surface.CreateFont("GOV_Trebuchet22", {
	font = "Trebuchet MS", 
	size = 22, 
	weight = 900
})

surface.CreateFont("GOV_Trebuchet20", {
	font = "Trebuchet MS", 
	size = 20, 
	weight = 900
})

end)

function GovStation_Menu()
	local VehicleMenu = vgui.Create( "DFrame" )
	VehicleMenu:SetSize( 570, 625 ) 
	VehicleMenu:Center() 
	VehicleMenu:SetTitle( "" )  
	VehicleMenu:SetVisible( true )
	VehicleMenu:SetDraggable( true ) 
	VehicleMenu:ShowCloseButton( false )
	VehicleMenu:MakePopup()
	VehicleMenu:SizeToContents()
	VehicleMenu.Paint = function(CHPaint)
		-- Draw the menu background color.		
		draw.RoundedBox( 6, 0, 0, CHPaint:GetWide(), CHPaint:GetTall(), Color( 255, 255, 255, 150 ) )

		-- Draw the outline of the menu.
		surface.SetDrawColor(0,0,0,255)
		surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), CHPaint:GetTall())

		surface.SetDrawColor(0,0,0,255)
		surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), 25)
		--surface.DrawOutlinedRect(1, 1, CHPaint:GetWide(), 25)

		-- Draw the top title.
		draw.SimpleText("Government Vehicle Station", "GOV_UiBold", 85,12.5, Color(70,70,70,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
		
	local GUI_Main_Exit = vgui.Create("DButton", VehicleMenu)
	GUI_Main_Exit:SetSize(16,16)
	GUI_Main_Exit:SetPos(550,5)
	GUI_Main_Exit:SetText("")
	GUI_Main_Exit.Paint = function()
		surface.SetMaterial(Material("icon16/cross.png"))
		surface.SetDrawColor(200,200,0,200)
		surface.DrawTexturedRect(0,0,GUI_Main_Exit:GetWide(),GUI_Main_Exit:GetTall())
	end
	GUI_Main_Exit.DoClick = function()
		VehicleMenu:Remove()
		net.Start("CH_CloseVehicleMenu")
		net.SendToServer()
	end
	
	local GUI_RemoveTruck = vgui.Create("DButton", VehicleMenu)	
	GUI_RemoveTruck:SetSize(250,20)
	GUI_RemoveTruck:SetPos(155,600)
	GUI_RemoveTruck:SetText("")
	GUI_RemoveTruck.Paint = function()
		draw.RoundedBox(8,1,1,GUI_RemoveTruck:GetWide()-2,GUI_RemoveTruck:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 125 -- x pos
		struc.pos[2] = 10 -- y pos
		struc.color = Color(255,255,255,255) -- Red
		struc.text = "Remove Current Vehicle" -- Text
		struc.font = "GOV_UiBold" -- Font
		struc.xalign = TEXT_ALIGN_CENTER-- Horizontal Alignment
		struc.yalign = TEXT_ALIGN_CENTER -- Vertical Alignment
		draw.Text( struc )
	end
	GUI_RemoveTruck.DoClick = function()
		net.Start("GovVehicle_RemoveCurrent")
		net.SendToServer()
		
		VehicleMenu:Remove()
		net.Start("CH_CloseVehicleMenu")
		net.SendToServer()
	end
	
	local TheListPanel = vgui.Create( "DPanelList", VehicleMenu )
	TheListPanel:SetTall( 565 )
	TheListPanel:SetWide( 560 )
	TheListPanel:SetPos( 5, 30 )
	TheListPanel:EnableVerticalScrollbar( true )
	TheListPanel:EnableHorizontal( true )
		
	for k, v in pairs( GovStation_Vehicles ) do
		if v.Name then
			local ItemList = vgui.Create("DPanelList")
			ItemList:SetTall( 150 )
			ItemList:SetWide( 560 )
			ItemList:SetPos( 10, 30 )
			ItemList:SetSpacing( 5 )
			ItemList.Paint = function()
				draw.RoundedBox(8,0,2,ItemList:GetWide(),ItemList:GetTall(),Color( 20, 20, 20, 180 ))
			end
				
			local ItemBackground = vgui.Create( "DPanel", ItemList )
			ItemBackground:SetPos( 0, 10 )
			ItemBackground:SetSize( 560, 132.5 )
			ItemBackground.Paint = function() -- Paint function
				draw.RoundedBoxEx(8,1,1,ItemBackground:GetWide()-2,ItemBackground:GetTall()-2,Color(70, 70, 70, 200), false, false, false, false)
			end
				
			local ItemDisplay = vgui.Create( "SpawnIcon", ItemBackground )
			ItemDisplay:SetPos( 5, 5 )
			ItemDisplay:SetModel( v.Model )
			ItemDisplay:SetToolTip(false)
			ItemDisplay:SetSize(120,120)
			ItemDisplay.PaintOver = function()
				return
			end
			ItemDisplay.OnMousePressed = function()
				return false
			end
			
			ZoomButton = vgui.Create("DImageButton", ItemDisplay)
			ZoomButton:SetMaterial( "icon16/magnifier_zoom_in.png" )
			ZoomButton:SetPos(2.5,2.5)
			ZoomButton:SetSize(16,16)
			ZoomButton.DoClick = function()
					VehicleMenu:Remove()
					GovStation_DisplayMenu( k )
				end
			
			local ItemName = vgui.Create( "DLabel", ItemBackground )
			ItemName:SetPos( 125, 10 )
			ItemName:SetFont( "GOV_Trebuchet24" )
			ItemName:SetColor( Color(255,255,255,255) )
			ItemName:SetText( v.Name )
			ItemName:SizeToContents()
			
			local ItemPrice = vgui.Create( "DLabel", ItemBackground )
			ItemPrice:SetPos( 125, 32.5 )
			ItemPrice:SetFont( "GOV_Trebuchet20" )
			ItemPrice:SetColor( Color(255,255,255,255) )
			ItemPrice:SetText( "Allowed Teams: "..table.concat(v.AllowedTeamNames,", ") )
			ItemPrice:SizeToContents()
			
			local RequiredULXRank = vgui.Create( "DLabel", ItemBackground )
			RequiredULXRank:SetPos( 125, 50 )
			RequiredULXRank:SetFont( "GOV_Trebuchet20" )
			RequiredULXRank:SetColor( Color(255,255,255,255) )
			if GovStation_RestrictToULXRanks then
				RequiredULXRank:SetText( "Allowed ULX Rank: "..table.concat(v.ULXRanksAllowed,", ") )
			else
				RequiredULXRank:SetText( "Allowed ULX Rank: None" )
			end
			RequiredULXRank:SizeToContents()
				
			local ItemDescription = vgui.Create( "DLabel", ItemBackground )
			ItemDescription:SetPos( 125, 70 )
			ItemDescription:SetFont( "GOV_UiBold" )
			ItemDescription:SetColor( Color(255,255,255,255) )
			ItemDescription:SetText( v.Description.."\nThis vehicle has ".. v.Health .." health." )
			ItemDescription:SetSize(440, 30)
			ItemDescription:SetWrap(true)
			
			local BuyItem = vgui.Create("DButton", ItemBackground)
			BuyItem:SetPos( 360, 95 )
			BuyItem:SetSize( 200, 30 )
			BuyItem:SetText("")
			if GovStation_RestrictToULXRanks then
				if table.HasValue( v.AllowedTeams, LocalPlayer():Team() ) then
					if table.HasValue( v.ULXRanksAllowed, LocalPlayer():GetUserGroup() ) then
						BuyItem:SetToolTip( "You can retrieve this vehicle!" )
					else
						BuyItem:SetToolTip( "You can't retrieve this vehicle! (Not required ulx rank)" )
					end
				else
					BuyItem:SetToolTip( "You can't retrieve this vehicle! (Not required team)" )
				end
			else
				if table.HasValue( v.AllowedTeams, LocalPlayer():Team() ) then
					BuyItem:SetToolTip( "You can retrieve this vehicle!" )
				else
					BuyItem:SetToolTip( "You can't retrieve this vehicle!" )
				end
			end
			BuyItem.Paint = function(panel)
				draw.RoundedBoxEx(8,1,1,BuyItem:GetWide()-2,BuyItem:GetTall()-2,Color(0, 0, 0, 130), true, false, true, false)
					
				local struc = {}
				struc.pos = {}
				struc.pos[1] = 100
				struc.pos[2] = 15
				struc.color = Color(255,255,255,255)
				struc.text = "Retrieve Vehicle" 
				struc.font = "GOV_Trebuchet22" 
				struc.xalign = TEXT_ALIGN_CENTER
				struc.yalign = TEXT_ALIGN_CENTER
				draw.Text( struc )
			end
			BuyItem.DoClick = function()
				VehicleMenu:Remove()
				net.Start("CH_CloseVehicleMenu")
				net.SendToServer()
				
				net.Start("GovStation_SpawnVehicle")
					net.WriteString(k)
				net.SendToServer()
			end
			
			local CanRetrieve = vgui.Create( "DImage", ItemBackground )
			CanRetrieve:SetPos( 372.5, 102.5 )
			if GovStation_RestrictToULXRanks then
				if table.HasValue( v.AllowedTeams, LocalPlayer():Team() ) then
					if table.HasValue( v.ULXRanksAllowed, LocalPlayer():GetUserGroup() ) then
						CanRetrieve:SetImage( "icon16/tick.png" )
					else
						CanRetrieve:SetImage( "icon16/cross.png" )
					end
				else
					CanRetrieve:SetImage( "icon16/cross.png" )
				end
			else
				if table.HasValue( v.AllowedTeams, LocalPlayer():Team() ) then
					CanRetrieve:SetImage( "icon16/tick.png" )
				else
					CanRetrieve:SetImage( "icon16/cross.png" )
				end
			end
			CanRetrieve:SizeToContents()
			
			TheListPanel:AddItem( ItemList )
		end
	end
end
usermessage.Hook("CH_GovStation", GovStation_Menu)

function GovStation_DisplayMenu( item )
	local DisplayMenu = vgui.Create( "DFrame" )
	DisplayMenu:SetSize( 400, 400 ) 
	DisplayMenu:Center() 
	DisplayMenu:SetTitle( "" )  
	DisplayMenu:SetVisible( true )
	DisplayMenu:SetDraggable( true ) 
	DisplayMenu:ShowCloseButton( false )
	DisplayMenu:MakePopup()
	DisplayMenu:SizeToContents()
	DisplayMenu.Paint = function(CHPaint)
		-- Draw the menu background color.		
		draw.RoundedBox( 6, 0, 0, CHPaint:GetWide(), CHPaint:GetTall(), Color( 255, 255, 255, 150 ) )

		-- Draw the outline of the menu.
		surface.SetDrawColor(0,0,0,255)
		surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), CHPaint:GetTall())

		surface.SetDrawColor(0,0,0,255)
		surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), 25)
		--surface.DrawOutlinedRect(1, 1, CHPaint:GetWide(), 25)

		-- Draw the top title.
		draw.SimpleText("Government Station - Vehicle Display", "GOV_UiBold", 110,12.5, Color(70,70,70,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	
	local DisplayBackground = vgui.Create( "DPanel", DisplayMenu )
	DisplayBackground:SetPos( 5, 30 )
	DisplayBackground:SetSize( 390, 335 )
	DisplayBackground.Paint = function() -- Paint function
		draw.RoundedBox(8,1,1,DisplayBackground:GetWide()-2,DisplayBackground:GetTall()-2,Color(70, 70, 70, 200))
		
		draw.SimpleText(GovStation_Vehicles[item].Name, "GOV_Trebuchet24", 195,15, Color(0, 0, 0, 220), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		
		draw.SimpleText("Allowed Teams: ".. table.concat(GovStation_Vehicles[item].AllowedTeamNames,", "), "GOV_Trebuchet22", 195,35, Color( 255, 255, 255, 220 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	
	local DisplayModel = vgui.Create("DModelPanel", DisplayBackground)
	DisplayModel:SetModel( GovStation_Vehicles[item].Model )
	DisplayModel:SetPos( -70, -50 )
	DisplayModel:SetSize( 550, 550 )
	DisplayModel:GetEntity():SetAngles(Angle(255, 255, 255))
	DisplayModel:SetCamPos( Vector( 255, 255, 80 ) )
	DisplayModel:SetLookAt( Vector( 0, 0, 0 ) )
	DisplayModel:SetSkin(GovStation_Vehicles[item].VehicleSkin)
	
	local ReturnToStore = vgui.Create("DButton", DisplayMenu)
	ReturnToStore:SetSize( 200, 30 )
	ReturnToStore:SetPos( 100, 370 )
	ReturnToStore:SetText("")
	ReturnToStore.Paint = function(panel)
		draw.RoundedBoxEx(8,1,1,ReturnToStore:GetWide()-2,ReturnToStore:GetTall()-2,Color(0, 0, 0, 130), true, true, false, false)
					
		local struc = {}
		struc.pos = {}
		struc.pos[1] = 100
		struc.pos[2] = 15
		struc.color = Color(255,255,255,255)
		struc.text = "Return To Station" 
		struc.font = "GOV_Trebuchet22" 
		struc.xalign = TEXT_ALIGN_CENTER
		struc.yalign = TEXT_ALIGN_CENTER
		draw.Text( struc )
	end
	ReturnToStore.DoClick = function()
		DisplayMenu:Remove()
		GovStation_Menu()
	end
end