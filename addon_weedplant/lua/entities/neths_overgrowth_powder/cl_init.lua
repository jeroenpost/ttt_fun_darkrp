include("shared.lua")

function ENT:Initialize()
	self.IsOvergrowthPowder = true
end

function ENT:Draw()
	self:DrawModel()
end

local BasePos_x, BasePos_y = ScrW()/2, ScrH()/2
local function DrawPowderInfo()
	if LocalPlayer():GetEyeTrace().Entity.IsOvergrowthPowder then

		local Powder = LocalPlayer():GetEyeTrace().Entity
		local Dist = LocalPlayer():GetPos():Distance(Powder:GetPos())

		if Dist < 400 then
			local Alpha = math.Clamp(280 - (0.50 * Dist), 0, 255)
			draw.RoundedBox( 4, BasePos_x-65, BasePos_y-50, 130, 100, Color( 35, 35, 35, Alpha ) )
			draw.RoundedBox( 4, BasePos_x-60, BasePos_y-45, 120, 20, Color( 255, 190, 0, Alpha ) )
			draw.SimpleText( "Overgrowth Powder", "DermaDefaultBold", BasePos_x, BasePos_y-28, Color( 255, 255, 255, Alpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP )
			draw.SimpleText( "Allows plant overgrowth", "DermaDefault", BasePos_x, BasePos_y-10, Color( 255, 255, 255, Alpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP )

			draw.SimpleText( "There's a slight chance of", "DermaDefault", BasePos_x, BasePos_y+35, Color( 255, 0, 0, Alpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP )
			draw.SimpleText( "losing the plant", "DermaDefault", BasePos_x, BasePos_y+45, Color( 255, 0, 0, Alpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP )
		end
	end
end		
hook.Add( "HUDPaint", "DrawPowderInfo", DrawPowderInfo )