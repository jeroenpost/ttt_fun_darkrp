AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/props_lab/jar01b.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)

	local phys = self:GetPhysicsObject()
	phys:Wake()

	self:InitTimer()
end

function ENT:StartTouch( ent )
	if ent:GetClass() == "neths_weed_plant" then
		if ent.CanOvergrowth == false and ent.Level == 5 then
			ent.CanOvergrowth = true
			self:Remove()
		end
	end
end

function ENT:InitTimer()
	timer.Simple( 5, function()
		if self:IsValid() then
			self:CastEffect()
			self:InitTimer()
		end
	end )
end

function ENT:CastEffect()
	local Pos, Ang = LocalToWorld( Vector( 0, 0, 10 ), Angle(0,0,90), self:GetPos(), self:GetAngles())

	local ed = ed or EffectData()
	ed:SetOrigin( Pos )
	ed:SetMagnitude(1)
	ed:SetScale(1)
	ed:SetRadius(1)
	util.Effect("effect_overgrowth_powder", ed)
end
