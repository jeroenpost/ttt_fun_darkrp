include("shared.lua")

local cl_Plant = {}

function ENT:Initialize()
	self.IsPlant 		= true

	self.Watering		= 0
	self.Level			= 0
	self.Experience		= 0
	self.IsDead			= false
	self.ExpReq			= 0
	self.SellAmount		= 0
end

function ENT:Draw()
	self:DrawModel()
end

local BasePos_x, BasePos_y = ScrW()/2, ScrH()/2
function cl_Plant.DrawPlantInfo()
	if LocalPlayer():GetEyeTrace().Entity.IsPlant then
		local Plant = LocalPlayer():GetEyeTrace().Entity
		local Dist = LocalPlayer():GetPos():Distance(Plant:GetPos())
		if Dist < 400 then
			local Owner = "Disconnected"
				if Plant.dt.owning_ent:IsValid() then
					Owner = Plant.dt.owning_ent:Nick()
				end
			local Alpha = math.Clamp(280 - (0.50 * Dist), 0, 255)
			draw.RoundedBox( 4, BasePos_x-100, BasePos_y-50, 200, 100, Color( 35, 35, 35, Alpha ) )
			draw.RoundedBox( 4, BasePos_x-95, BasePos_y-45, 190, 20, Color( 72, 221, 0, Alpha ) )
			draw.SimpleText( "Owned by: "..Owner, "DermaDefaultBold", BasePos_x, BasePos_y-28, Color( 255, 255, 255, Alpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP )
			draw.SimpleText( "Level: "..Plant.Level, "ChatFont", BasePos_x, BasePos_y-5, Color( 255, 255, 255, Alpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP )
			draw.SimpleText( "Exp: "..Plant.Experience.."/"..Plant.ExpReq, "ChatFont", BasePos_x, BasePos_y+10, Color( 255, 255, 255, Alpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP )
			if Plant.IsDead == true then
				draw.SimpleText( "Hydration: "..Plant.Watering.."% (DEAD)", "ChatFont", BasePos_x, BasePos_y+25, Color( 255, 0, 0, Alpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP )
			else
				draw.SimpleText( "Hydration: "..Plant.Watering.."%", "ChatFont", BasePos_x, BasePos_y+25, Color( 255, 255, 255, Alpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP )
			end
			draw.SimpleText( "Worth: $"..Plant.SellAmount, "ChatFont", BasePos_x, BasePos_y+40, Color( 255, 255, 255, Alpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP )
		end
	end
end
hook.Add( "HUDPaint", "DrawPlantInfo", cl_Plant.DrawPlantInfo )

function cl_Plant.ReceiveInfo()
	local Plant = net.ReadEntity()
	local entTbl = net.ReadTable()
	
	Plant.Watering 		= entTbl.Watering
	Plant.Level 		= entTbl.Level
	Plant.Experience 	= entTbl.Experience
	Plant.IsDead 		= entTbl.IsDead
	Plant.ExpReq		= entTbl.ExpReq
	Plant.SellAmount	= entTbl.SellAmount
end
net.Receive( 'NET_PlantInfo', cl_Plant.ReceiveInfo )

function cl_Plant.OpenMenu()
	local Plant = net.ReadEntity()

	local Menu = vgui.Create( 'DFrame' )
	Menu:SetTitle( 'Plant Actions' )
	Menu:SetSize( 400, 115 )
	Menu:MakePopup()
	Menu.Paint = function()
		draw.RoundedBox( 4, 0, 0, Menu:GetWide(), Menu:GetTall(), Color( 35, 35, 35, 255 ) )
	end
	Menu:SetPos(ScrW()/2-Menu:GetWide()/2, ScrH())
	Menu:MoveTo( ScrW()/2-Menu:GetWide()/2, ScrH()/2-Menu:GetTall()/2, 0.25, 0, 0.5)

	local LeftPanel = vgui.Create( 'DPanel', Menu )
	LeftPanel:SetSize( 190, 20 )
	LeftPanel:SetPos( 5, 25 )
	LeftPanel:SetBackgroundColor( Color( 72, 221, 0, 255 ) )

	local LeftLabel = vgui.Create( "DLabel", LeftPanel )
	LeftLabel:SetFont( "ChatFont" )
	LeftLabel:SetText( "Stats" )
	LeftLabel:SizeToContents()
	LeftLabel:CenterHorizontal()
	LeftLabel:CenterVertical()

	local LevelLabel = vgui.Create( "DLabel", Menu )
	LevelLabel:SetFont( "DermaDefaultBold" )
	LevelLabel:SetText( "Level: "..Plant.Level )
	LevelLabel:SizeToContents()
	LevelLabel:SetPos( 5, 50 )

	local ExpLabel = vgui.Create( "DLabel", Menu )
	ExpLabel:SetFont( "DermaDefaultBold" )
	ExpLabel:SetText( "Exp: "..Plant.Experience.."/"..Plant.ExpReq )
	ExpLabel:SizeToContents()
	ExpLabel:SetPos( 5, 65 )

	local WateringLabel = vgui.Create( "DLabel", Menu )
	WateringLabel:SetFont( "DermaDefaultBold" )
	if Plant.IsDead == true then
		WateringLabel:SetText( "Hydration: "..Plant.Watering.."% (DEAD)" )
		WateringLabel:SetTextColor( Color( 255, 0, 0, 255 ) )
	else
		WateringLabel:SetText( "Hydration: "..Plant.Watering.."%" )
	end
	WateringLabel:SizeToContents()
	WateringLabel:SetPos( 5, 80 )

	local SellAmountLabel = vgui.Create( "DLabel", Menu )
	SellAmountLabel:SetFont( "DermaDefaultBold" )
	SellAmountLabel:SetText( "Worth: $"..Plant.SellAmount )
	SellAmountLabel:SizeToContents()
	SellAmountLabel:SetPos( 5, 95 )

	local RightPanel = vgui.Create( 'DPanel', Menu )
	RightPanel:SetSize( 190, 20 )
	RightPanel:SetPos( 205, 25 )
	RightPanel:SetBackgroundColor( Color( 72, 221, 0, 255 ) )

	local RightLabel = vgui.Create( "DLabel", RightPanel )
	RightLabel:SetFont( "ChatFont" )
	RightLabel:SetText( "Actions" )
	RightLabel:SizeToContents()
	RightLabel:CenterHorizontal()
	RightLabel:CenterVertical()

	local WaterButton = vgui.Create( "DButton", Menu )
	WaterButton:SetPos( 205, 50 )
	WaterButton:SetText( "Water" )
	WaterButton:SetSize( 190, 20 )
	if Plant.IsDead then 
		WaterButton:SetDisabled( true )
	end
	WaterButton.DoClick = function()
		net.Start( 'NET_PlantDoAction' )
			net.WriteInt( 1, 8 )
			net.WriteEntity( Plant )
			net.WriteEntity( LocalPlayer() )
		net.SendToServer()
		Menu:Close()
	end

	local SellButton = vgui.Create( "DButton", Menu )
	SellButton:SetPos( 205, 70 )
	SellButton:SetText( "Sell" )
	SellButton:SetSize( 190, 20 )
	if Plant.IsDead then 
		SellButton:SetDisabled( true )
	end
	SellButton.DoClick = function()
        local id = Plant:EntIndex()
        for k,v in pairs(ents.FindByClass("neths_weed_plant")) do
            if v:EntIndex() == id then
                Plant2 = v
            end
        end
        if IsValid(Plant2) then
		net.Start( 'NET_PlantDoAction' )
			net.WriteInt( 2, 8 )
			net.WriteEntity( Plant2 )
			net.WriteEntity( LocalPlayer() )
		net.SendToServer()
        end
		Menu:Close()
	end

	local SeizeButton = vgui.Create( "DButton", Menu )
	SeizeButton:SetPos( 205, 90 )
	SeizeButton:SetText( "Seize" )
	SeizeButton:SetSize( 190, 20 )
	SeizeButton.DoClick = function()
		net.Start( 'NET_PlantDoAction' )
			net.WriteInt( 3, 8 )
			net.WriteEntity( Plant )
			net.WriteEntity( LocalPlayer() )
		net.SendToServer()
		Menu:Close()
	end
end
net.Receive( 'NET_OpenPlantMenu', cl_Plant.OpenMenu )