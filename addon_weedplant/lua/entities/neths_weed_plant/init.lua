AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

util.AddNetworkString( 'NET_PlantInfo' )
util.AddNetworkString( 'NET_OpenPlantMenu' )
util.AddNetworkString( 'NET_PlantDoAction' )

local sv_Plant = {}

sv_Plant.GrowTime 	= 30	-- Amount of time after which XP is being added and the effect is being cast, 	default = 30
sv_Plant.ExpMul 	= 25	-- Amount of XP plant gets per GrowTime											default = 25
sv_Plant.MoneyPerXP = 5		-- Amount of cash 1 point of XP is worth										default = 5

sv_Plant.ExpTbl = {}
sv_Plant.ExpTbl[1] = { ExpReq = 125, 	MoneyMul = 1, 		Mdl = "models/nv_weed_plant/plant_small.mdl" }
sv_Plant.ExpTbl[2] = { ExpReq = 250, 	MoneyMul = 1.25, 	Mdl = "models/nv_weed_plant/plant_medium.mdl" }
sv_Plant.ExpTbl[3] = { ExpReq = 375, 	MoneyMul = 1.50,	Mdl = "models/nv_weed_plant/plant_medium.mdl" }
sv_Plant.ExpTbl[4] = { ExpReq = 500, 	MoneyMul = 1.75,	Mdl = "models/nv_weed_plant/plant_big.mdl" }
sv_Plant.ExpTbl[5] = { ExpReq = 625, 	MoneyMul = 2,		Mdl = "models/nv_weed_plant/plant_big.mdl" }
sv_Plant.ExpTbl[6] = { ExpReq = 750, 	MoneyMul = 2.25,	Mdl = "models/nv_weed_plant/plant_big.mdl" }
sv_Plant.ExpTbl[7] = { ExpReq = 1000, 	MoneyMul = 2.5,		Mdl = "models/nv_weed_plant/plant_big.mdl" }

function ENT:Initialize()
	self:SetModel("models/nv_weed_plant/plant_small.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
	local phys = self:GetPhysicsObject()
	phys:Wake()

	//	Base Properties
	self.Watering		= 50 --Max 100
	self.Level			= 1
	self.Experience		= 0
	self.IsDead			= false
	self.ExpReq			= 0
	self.SellAmount		= 0
	self.CanOvergrowth 	= false

	self:InitTimer()

	sv_Plant.SendInfo( self )
end

function ENT:InitTimer()
	timer.Simple( sv_Plant.GrowTime, function()
		if self:IsValid() then
			self:Grow()
			self:InitTimer()
		end
	end )
end

function ENT:Use( activator, caller )
	net.Start( 'NET_OpenPlantMenu' )
		net.WriteEntity( self )
	net.Send( caller )
end

function ENT:Grow()
	if self.Watering == 0 then 
		self.IsDead = true 
		self.SellAmount = 0
		sv_Plant.SendInfo( self )
	end
	if self.IsDead == true then 
		return 
	end
	if self.Level == 5 and self.Experience == sv_Plant.ExpTbl[self.Level].ExpReq and self.CanOvergrowth == false then
		return
	end
	if self.Level == #sv_Plant.ExpTbl and self.Experience == sv_Plant.ExpTbl[self.Level].ExpReq and self.CanOvergrowth == true then
		return
	end

	self.Experience = self.Experience + sv_Plant.ExpMul
	self.Watering = self.Watering - 10
	self.SellAmount = self.Experience * ( sv_Plant.MoneyPerXP * sv_Plant.ExpTbl[self.Level].MoneyMul )

	if self.Experience >= sv_Plant.ExpTbl[self.Level].ExpReq and self.Level < 5 then
		self.Experience = 0
		self.SellAmount = 0
		self.Level = self.Level + 1
		if sv_Plant.ExpTbl[self.Level].Mdl then
			self:SetModel(sv_Plant.ExpTbl[self.Level].Mdl)
		end
	elseif self.Experience >= sv_Plant.ExpTbl[self.Level].ExpReq and self.CanOvergrowth == true and self.Level < #sv_Plant.ExpTbl then
		self.Experience = 0
		self.SellAmount = 0
		self.Level = self.Level + 1
		if sv_Plant.ExpTbl[self.Level].Mdl then
			self:SetModel(sv_Plant.ExpTbl[self.Level].Mdl)
		end
	end

	local ed = EffectData()
	ed:SetOrigin( self:GetPos()+Vector( 0, 0, 35 ) )
	ed:SetMagnitude(1)
	ed:SetScale(1)
	ed:SetRadius(1)
	util.Effect("effect_weed_puff", ed)

	sv_Plant.SendInfo( self )
end

function sv_Plant.SendInfo( ent )
	local entTbl = {}
	entTbl.Watering 	= ent.Watering
	entTbl.Level 		= ent.Level
	entTbl.Experience 	= ent.Experience
	entTbl.IsDead 		= ent.IsDead
	entTbl.ExpReq 		= sv_Plant.ExpTbl[ent.Level].ExpReq
	entTbl.SellAmount 	= ent.SellAmount

	net.Start( 'NET_PlantInfo' )
		net.WriteEntity( ent )
		net.WriteTable( entTbl )
	net.Broadcast()
end

function sv_Plant.ReceiveAction()
	local PLANT_DO_WATER 	= 1
	local PLANT_DO_SELL 	= 2
	local PLANT_DO_SEIZE	= 3

	local Action 	= net.ReadInt( 8 )
	local Plant 	= net.ReadEntity()
	local Caller	= net.ReadEntity()

    if not Plant:EntIndex() then return end
    local Plant2 = false
    for k,v in pairs(ents.FindByClass("neths_weed_plant")) do
        if v:EntIndex() == Plant:EntIndex() then
            Plant2 = v
        end
    end

    if not IsValid(Plant2) then

        return
    end

    Plant = Plant2

	if Action == PLANT_DO_WATER then
		if Plant.Watering <= 0 then return end
		Plant.Watering = 100
		DarkRP.notify( Caller, 2, 3, "You have watered the plant")
	end
	if Action == PLANT_DO_SELL then
		if Plant.IsDead == true then
			DarkRP.notify( Caller, 2, 3, "You can't sell a dead plant!")
			return
		end
		Caller:addMoney( Plant.SellAmount )
		DarkRP.notify( Caller, 2, 3, "You have made $"..Plant.SellAmount.." by selling the plant")
		Plant:Remove()

		local ed = EffectData()
		ed:SetOrigin( Plant:GetPos()+Vector( 0, 0, 35 ) )
		ed:SetMagnitude(1)
		ed:SetScale(1)
		ed:SetRadius(1)
		util.Effect("effect_weed_puff", ed)
	end
	if Action == PLANT_DO_SEIZE then
		DarkRP.notify( Caller, 2, 3, "You have seized the plant")
		Plant:Remove()
	end

	sv_Plant.SendInfo( Plant )
end
net.Receive( 'NET_PlantDoAction', sv_Plant.ReceiveAction )