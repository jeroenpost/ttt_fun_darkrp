include( "shared.lua" )

local fmt = string.format

local dmv_msg = fmt( [[Welcome to the DMV.

For a custom License Plate, enter any 7 character
combination of uppercase letters, numbers or dashes.

You will be charged $%s.]], string.Comma( LPlates.CustomPlateCost ) )

net.Receive( "npc_dmv_openmenu", function( len )
	local frame = vgui.Create( "DFrame" )
		frame:SetTitle( "DMV" )
		frame:SetSize( 275, 172 )
		frame:Center()
		frame:MakePopup()
		frame.OldPaint = frame.Paint
		function frame:Paint( w, h )
			self:OldPaint( w, h )

			surface.SetDrawColor( Color( 45, 45, 45, 255 ) )
			surface.DrawRect( 5, 27, self:GetWide() - 10, self:GetTall() - 32 )
		end

	local lblinfo = vgui.Create( "DLabel", frame )
		lblinfo:SetPos( 10, 30 )
		lblinfo:SetText( dmv_msg )
		lblinfo:SizeToContents()

	local lentry = vgui.Create( "DTextEntry", frame )
		lentry:SetPos( 8, lblinfo.y + lblinfo:GetTall() + 7 )
		lentry:SetWide( frame:GetWide() - 16 )
		function lentry:OnTextChanged()
			local txt = self:GetText()

			txt = string.gsub( txt, "[^%a%d-]", "" )
			txt = string.gsub( txt, "%l", function( m )
				return string.upper( m )
			end )
			txt = string.Left( txt, 7 )

			self:SetText( txt )
			self:SetCaretPos( string.len( txt ) )
		end

	local btnreg = vgui.Create( "DButton", frame )
		btnreg:SetText( "Register Plate" )
		btnreg:SetPos( frame:GetWide() / 2, lentry.y + lentry:GetTall() + 5 )
		btnreg:SetWide( (frame:GetWide() / 2) - 8 )
		function btnreg:DoClick()
			local txt = lentry:GetText()

			if string.len( txt ) < 1 then return end

			frame:Close()

			local fr = vgui.Create( "DFrame" )
				fr:SetTitle( "Verify Purchase" )
				fr:SetTall( 78 )
				fr:MakePopup()
				fr:SetBackgroundBlur( true )
				fr.OldPaint = fr.Paint
				function fr:Paint( w, h )
					self:OldPaint( w, h )

					surface.SetDrawColor( Color( 45, 45, 45, 255 ) )
					surface.DrawRect( 5, 27, self:GetWide() - 10, self:GetTall() - 32 )
				end

			local lblver = vgui.Create( "DLabel", fr )
				lblver:SetText( fmt( "Are you sure you want to purchase this plate for $%s?", string.Comma( LPlates.CustomPlateCost ) ) )
				lblver:SizeToContents()
				lblver:SetPos( 8, 30 )

			fr:SetWide( lblver:GetWide() + 16 )
			fr:Center()

			local yoff = lblver.y + lblver:GetTall() + 5

			local btnaccept = vgui.Create( "DButton", fr )
				btnaccept:SetText( "Accept" )
				btnaccept:SetWide( (fr:GetWide() / 3) * 2 - 9 )
				btnaccept:SetPos( 8, yoff )
				function btnaccept:DoClick()
					net.Start( "npc_dmv_buyplate" )
						net.WriteString( txt )
					net.SendToServer()

					fr:Close()
				end

			local btncancel = vgui.Create( "DButton", fr )
				btncancel:SetText( "Cancel" )
				btncancel:SetWide( (fr:GetWide() / 3) - 8 )
				btncancel:SetPos( ((fr:GetWide() / 3 ) * 2) + 1, yoff )
				function btncancel:DoClick()
					fr:Close()
				end
		end
end )

hook.Add("PostDrawOpaqueRenderables", "towtruck Dealter_dmv", function()
    for _, ent in pairs (ents.FindByClass("npc_dmv")) do
        if ent:GetPos():Distance(LocalPlayer():GetPos()) < 1000 then
            local Ang = ent:GetAngles()

            Ang:RotateAroundAxis( Ang:Forward(), 90)
            Ang:RotateAroundAxis( Ang:Right(), -90)

            cam.Start3D2D(ent:GetPos()+ent:GetUp()*80, Ang, 0.35)
            draw.SimpleTextOutlined( 'DMV', "HUDNumber5", 0, 0, Color( 0, 120, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP, 1, Color(0, 0, 0, 255))
            cam.End3D2D()
        end
    end
end)

