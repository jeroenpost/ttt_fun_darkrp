AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "cl_init.lua" )

include( "shared.lua" )

util.AddNetworkString( "npc_dmv_openmenu" )
util.AddNetworkString( "npc_dmv_buyplate" )

--because fptje is stupid
local function CanAfford( ply, amt )
	if ply.CanAfford then
		return ply:CanAfford( amt ) 
	else
		return ply:canAfford( amt )
	end
end

local function AddMoney( ply, amt )
	if ply.AddMoney then
		ply:AddMoney( amt )
	else
		ply:addMoney( amt )
	end
end

function ENT:Initialize()
    self:SetModel("models/humans/group01/male_02.mdl")
	self:SetSolid( SOLID_BBOX )

	self:SetHullType( HULL_HUMAN )
	self:SetHullSizeNormal()

	self:SetNPCState( NPC_STATE_SCRIPT )
	
	self:CapabilitiesAdd( CAP_ANIMATEDFACE )
	self:CapabilitiesAdd( CAP_TURN_HEAD )

	self:SetUseType( SIMPLE_USE )
end

function ENT:AcceptInput( name, activator, ply )	
	if not (name == "Use" and ply:IsPlayer()) then return end

	net.Start( "npc_dmv_openmenu" )
	net.Send( ply )
end

net.Receive( "npc_dmv_buyplate", function( len, ply )
	local pserial = net.ReadString()

	if not CanAfford( ply, LPlates.CustomPlateCost ) then
		ply:SendLua( [[chat.AddText(Color(255,0,0),"Insufficient funds.")]] )

		return
	end

	if string.len( pserial ) < 1 or string.len( pserial ) > 7 or string.find( pserial, "[^%a%d-]" ) then
		ply:SendLua( [[chat.AddText(Color(255,0,0),"Invalid plate serial.")]] )

		return
	end

	local result = sql.QueryRow( string.format( 
		[[SELECT * FROM %s WHERE plate=%s]],
		LPlates.TableName,
		sql.SQLStr( pserial )
	) )

	if result then
		ply:SendLua( [[chat.AddText(Color(255,0,0),"Plate serial already in use.")]] )

		return
	end

	AddMoney( ply, -LPlates.CustomPlateCost )

	LPlates.SetPlateInfo( ply, pserial )
end )

