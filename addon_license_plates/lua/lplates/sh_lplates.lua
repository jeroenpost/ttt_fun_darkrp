LPlates = {}

local vehicle_offsets = {}

function LPlates.RegisterVehiclePlates( vname, ... )
	vehicle_offsets[vname] = { ... }
end

function LPlates.GetVehiclePlates( vname )
	return vehicle_offsets[vname]
end
