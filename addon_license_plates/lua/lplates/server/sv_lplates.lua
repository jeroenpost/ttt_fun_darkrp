
local fmt = string.format
local sqlstr = sql.SQLStr

local function randomLetter()
	return string.char( string.byte( 'A' ) + math.random( 0, 25 ) )
end

local function generatePlateSerial()
	return fmt( 
		[[%s%s%s-%i%i%i]], 
		randomLetter(), randomLetter(), randomLetter(),
		math.random( 0, 9 ), math.random( 0, 9 ), math.random( 0, 9 )
	)
end

local vscript_classes = {
	"prop_vehicle_jeep",
	"prop_vehicle_jeep_old",
	"prop_vehicle_prisoner_pod",
	"prop_vehicle_airboat"
}

function LPlates.GetVehicleName( veh )
	if not (IsValid( veh ) and veh:IsVehicle()) then return end

	local eclass = veh:GetClass()

	if table.HasValue( vscript_classes, eclass ) then
		return veh.VehicleScriptName or veh.VehicleName --fix for certain car dealer scripts
	end

	return eclass
end

function LPlates.SetPlateInfo( ply, pserial )
	local steamid = sqlstr( (type( ply ) == "string") and ply or ply:SteamID() )

	local result = sql.QueryRow( fmt(
		[[SELECT * FROM %s WHERE steamid=%s]],
		LPlates.TableName,
		steamid
	) )

	local qstr

	if not result then
		qstr = fmt( 
			[[INSERT INTO %s VALUES (%s, %s)]], 
			LPlates.TableName, 
			steamid,
			sqlstr( pserial )
		)

	else
		qstr = fmt(
			[[UPDATE %s SET plate=%s WHERE steamid=%s]],
			LPlates.TableName,
			sqlstr( pserial ),
			steamid
		)

		local oldpserial = LPlates.GetPlateInfo( ply )

		for _,veh in pairs( ents.GetAll() ) do
			if not 
				(IsValid( veh ) and veh:IsVehicle() and 
					veh:GetNWString( "plate_serial" ) == oldpserial) then continue end
			
			veh:SetNWString( "plate_serial", pserial )
		end

	end

	sql.Query( qstr )
end

function LPlates.GetPlateInfo( ply )
	local steamid = sqlstr( (type( ply ) == "string") and ply or ply:SteamID() )

	local result = sql.QueryRow( fmt(
		[[SELECT * FROM %s WHERE steamid=%s]],
		LPlates.TableName, 
		steamid
	) )

	if not result then
		local pserial

		repeat 
			pserial = generatePlateSerial() 
		until not sql.QueryRow( fmt( 
			[[SELECT * FROM %s WHERE plate=%s]],
			LPlates.TableName,
			sqlstr( pserial )
		) )

		LPlates.SetPlateInfo( ply, pserial )
		
		return pserial
	end

	return result.plate
end

function LPlates.SetupVehiclePlates( ply, veh )
	local vname = LPlates.GetVehicleName( veh )

	if not (IsValid( ply ) and IsValid( veh ) and LPlates.GetVehiclePlates( vname )) then return end

	local pserial = LPlates.GetPlateInfo( ply )

	veh:SetNWString( "plate_serial", pserial )
	veh:SetNWString( "veh_name", vname )
end

hook.Add( "Initialize", "sv_lplates_createtable", function()
	if not sql.TableExists( LPlates.TableName ) then
		local qstr = fmt( [[
			CREATE TABLE %s (
				steamid TEXT PRIMARY KEY NOT NULL,
				plate TEXT
			)
		]], LPlates.TableName )

		sql.Query( qstr )
	end
end )

hook.Add( "InitPostEntity", "sv_lplates_spawndmvs", function()
	if TRUE or not LPlates.DMVEnabled then return end

	local map = string.lower( game.GetMap() )

	for _,linfo in pairs( LPlates.DMVLocations ) do
		if not (linfo.Map and string.lower( linfo.Map ) == map ) then continue end
		
		local ent = ents.Create( "npc_dmv" )
			ent:SetModel( linfo.Model or LPlates.DMVDefaultModel )
			ent:SetPos( linfo.Pos )
			ent:SetAngles( linfo.Ang )
			ent:Spawn()
			ent:DropToFloor()
	end
end )

hook.Add( "PlayerSpawnedVehicle", "sv_lplates_setplate", LPlates.SetupVehiclePlates )
