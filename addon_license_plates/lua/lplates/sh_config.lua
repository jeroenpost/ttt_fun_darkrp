
LPlates.DistanceFade = 800 --cutoff distance
LPlates.DrawTextShadow = true --should draw text shadow
LPlates.DrawScrews = true --should draw plate screws

local TextColor = Color( 0, 0, 0, 255 )
local TextShadowColor = Color( 170, 170, 170, 255 )

LPlates.PlateTextTop = "TTT-FUN" --text on top of plate
LPlates.PlateTextTopColor = TextColor --text on top of plate color
LPlates.PlateTextTopShadowColor = TextShadowColor --text on top of plate shadow color

LPlates.PlateTextBottom = "Ride 'n Shine" --text on bottom plate
LPlates.PlateTextBottomColor = TextColor
LPlates.PlateTextBottomShadowColor = TextShadowColor

LPlates.PlateTextSerialColor = TextColor --plate serial number color
LPlates.PlateTextSerialShadowColor = TextShadowColor --plate serial number shadow color

LPlates.PlateColor = Color( 255, 255, 255, 255 ) --plate background color
LPlates.PlateDividerColor = Color( 0, 0, 0, 255 ) --plate divider color (the lines)
LPlates.ScrewColor = Color( 125, 125, 125, 255 ) --plate screw color

LPlates.TableName = "license_plates" --sqlite table name

LPlates.DMVEnabled = true --enables DMV NPC for custom plates
LPlates.CustomPlateCost = 1500 --price of custom plates

LPlates.DMVDefaultModel = "models/humans/group01/male_02.mdl" --default DMV NPC model, if none are provided
LPlates.DMVLocations = {}

local function AddDMVLocation( map, pos, ang, model ) --model is optional
	table.insert( LPlates.DMVLocations, { Map = map, Pos = pos, Ang = ang, Model = model } )
end

--Put DMV Locations here

--AddDMVLocation( "gm_construct", Vector( 750, -335, -80 ), Angle( 0, 180, 0 ) )
AddDMVLocation( "rp_downtown_evilmelon_v1", Vector( -2925, -429, 50 ), Angle( 0, -140, 0 ) )
