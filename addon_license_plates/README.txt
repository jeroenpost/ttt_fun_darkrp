- INSTALLATION

Just like any other addon. Extract to garrysmod/addons and you're good to go.

- Configuration

For changing the colors, text or custom plate cost, find the variable labeled with
what you are trying to change in lua/lplates/sh_config.lua.

For adding DMV Locations, see lua/lplates/sh_config.lua.
Two locations are already added, so you can just copy and modify those. 
The first parameter is the map name, the second is the position of the DMV NPC, 
and the third is the angle (direction) it will be facing. You can get these last
two values by typing "cl_showpos 1" in console and standing where you would like the
NPC to spawn and using the readout in the top right hand corner for the POSition and
ANGle. You can add as many DMV NPCs to a map as you like.

To add plates to new vehicles, simply use RegisterVehiclePlates() and pass the offsets
and sizes of the plate(s) you would like to add in lua/lplates/sh_vehicledef.lua.
If you are unsure how to do this, just copy one of the vehicle definitions in there already.
The first parameter is the name of the vehicle (in the vehicle script), if you're adding a
vehicle entity, just use the entity's class. All proceeding parameters are tables with the 
information for that plate. Getting your plate in the right position may take some tweaking, 
which is why it's best to do this in single player with the plates addon installed on your 
client. Just save the file when you adjust the position, angle or scale and it will automatically 
update on the vehicle in-game. Once you have everything the way you like it, upload the 
sh_vehicledef.lua file to your server and overwrite the one that's on there. 

I've added a fix that should make this work with the popular vehicle dealer scripts. However,
if the plates aren't appearing on a vehicle through a dealer (or some other vehicle spawner)
and they appear in the spawn menu, the spawner is not spawning them correctly. You need to add
LPlates.SetupVehiclePlates( ply, vehicle ) into the vehicle spawn function and change "ply"
to the buyer player, and "vehicle" to the vehicle entity being spawned.

- Contact

If you have any issues that weren't addressed here, or just need additional help, feel free to send me a PM or open a support ticket
on ScriptFodder.