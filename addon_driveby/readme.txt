This addon allows players to shoot from inside a vehicle.

This works with VCMod and other addons that allow cars to have more than one seat.

/*------- Instructions For Use -------*/
	Step 1: Install into `garrysmod/garrysmod/` directory
	Step 2: After properly installed, navigate yourself to `garrysmod/garrysmod/addons/etg_driveby_system/lua/autorun/server/system_driveby.lua`
	Step 3: Once inside this lua file, scroll all the way down to the bottom, which will bring you to our Table.
	Step 4: I left the table empty except for 1 example. This is because YOU, the developer need to figure out which cars deserve this feature, and which do not.
		Some cars have small windows, making it difficult to climb out. Other cars you might not want people to have the ability to drive by.
		I suggest you keep the table exclusive to expensive cars, because........ noobs will abuse.
		

/*------- Trouble Shooting -------*/
	If need be, there are always mistakes to be made. If that's the case, please feel free to contact me on ScriptFodder.com,
	
	My Facepunch page, which I check daily is:
		http://facepunch.com/member.php?u=335167
		
	I read private messages within 1-3 days.