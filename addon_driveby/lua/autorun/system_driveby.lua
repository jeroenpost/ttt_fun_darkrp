if SERVER then
	AddCSLuaFile( "system_driveby.lua" )
	
	return
end

net.Receive( "dbs_network_en", function( )
	LocalPlayer( ).DBShooting = true
end )

net.Receive( "dbs_network_dis", function( )
	LocalPlayer( ).DBShooting = false
end )

hook.Add( "Move", "DBS_NOCLIP_CONTROL", function( ply, mvStruc )
	if ply == LocalPlayer( ) and ply.DBShooting then
		mvStruc:SetForwardSpeed( 0 )
		mvStruc:SetUpSpeed( 0 )
		mvStruc:SetSideSpeed( 0 )
		
		return true
	end
	
	return false
end )