util.AddNetworkString( "dbs_network_en" )
util.AddNetworkString( "dbs_network_dis" )

DB = { }

function DB.ToggleDriveBy( ply )
	if !ply or !ply:IsValid( ) then return end
	--if ply:GetVehicle( ):GetDriver( ) == ply then return end
	
	if !ply.DBShooting then
		ply.DBShooting = true
		DB.EnableDriveBy( ply )
		
		net.Start( "dbs_network_en" )
		net.Send( ply )
	else
		ply.DBShooting = nil
		DB.DisableDriveBy( ply )
		
		net.Start( "dbs_network_dis" )
		net.Send( ply )
	end
end


--Testing Purposes

/*concommand.Add( "set_db_seat", function( ply, _, _ )
	local ent = ply:GetEyeTrace( ).Entity
	local mainEnt = ents.FindByClass( "prop_vehicle_jeep" )[1]
	
	if !ent then return end
	
	ent:SetParent( mainEnt )
end )*/

hook.Add( "Think", "DBS_THINK", function( )
	for i, v in pairs( ents.FindByClass( "prop_vehicle_prisoner_pod" )) do
		local parent = v:GetParent( )
		
		if parent and parent:IsValid( ) then
			if parent:GetClass( ) == "prop_vehicle_jeep" then
				if !v:GetTable( ).ParentCar then
					v:GetTable( ).ParentCar = parent
				end
			end
		end
	end
end )

hook.Add( "Move", "DBS_NOCLIP_CONTROL", function( ply, mvStruc )
	if ply.DBShooting then
		mvStruc:SetForwardSpeed( 0 )
		mvStruc:SetUpSpeed( 0 )
		mvStruc:SetSideSpeed( 0 )
		
		return true
	end
	
	return false
end )

function DB.SetAnimationMan( ply )
	if !ply or !ply:IsValid( ) then return end
end

hook.Add( "KeyPress", "DB.KeyPressedHook", function( ply, key )	
	local vehicle = ply:GetVehicle( )
	
	if ply.DBShooting then
		if key == IN_USE or key == IN_JUMP or key == IN_DUCK or key == IN_FORWARD or key == IN_BACK or key == IN_MOVELEFT or key == IN_MOVERIGHT then
			DB.ToggleDriveBy( ply )
		end
	elseif vehicle:IsValid( ) then
		if vehicle:GetTable( ).ParentCar then
			if key == IN_JUMP or key == IN_DUCK or key == IN_FORWARD or key == IN_BACK or key == IN_MOVELEFT or key == IN_MOVERIGHT then
				DB.ToggleDriveBy( ply )
			end
		end
	end
end )

function DB.PlayerDeathHook( ply )
	if !ply.DBShooting then return end
	
	ply.DBSeatEnt.IsOccupied = nil
	ply.DBSeatEnt = nil
	
	ply:SetParent( )
	ply:SetMoveType( MOVETYPE_WALK )
	
	if !ply.DBCarEnt or !ply.DBCarEnt:IsValid( ) then return end
	
	ply.DBCarEnt.DBCarrage = ply.DBCarEnt.DBCarrage or 1
		ply.DBCarEnt.DBCarrage = ( ply.DBCarEnt.DBCarrage - 1 )
end; hook.Add( "PlayerDeath", "DB.PlayerDeathHook", DB.PlayerDeathHook );

function DB.PlayerDisconnectedHook( ply )
	if !ply.DBShooting then return end
	
	ply.DBSeatEnt.IsOccupied = nil
	ply.DBSeatEnt = nil
	
	if !ply.DBCarEnt or !ply.DBCarEnt:IsValid( ) then return end
	
	ply.DBCarEnt.DBCarrage = ply.DBCarEnt.DBCarrage or 1
		ply.DBCarEnt.DBCarrage = ( ply.DBCarEnt.DBCarrage - 1 )
end; hook.Add( "PlayerDisconnected", "DB.PlayerDisconnectedHook", DB.PlayerDisconnectedHook );

function DB.EnableDriveBy( ply )
	if !ply or !ply:IsValid( ) then return end
	if !ply.DBShooting then return end
	if !ply:Alive( ) then return end
	
	local carEnt = ply:GetVehicle( )
	local seatEnt = carEnt
	
	if !carEnt or !carEnt:IsValid( ) then return end

	if !carEnt:GetTable( ).ParentCar or !carEnt:GetTable( ).ParentCar:IsValid( ) then return end

	--if ( carEnt:GetVelocity( ):Length( ) / 17.6 ) >= 15 then ply:ChatPrint( "The vehicle seems to be going too fast!" ) return end
	
	carEnt = carEnt:GetTable( ).ParentCar
	
	
	if !carEnt:GetModel( ) then return end
	if !DBSPositions[carEnt:GetModel( )] then
         DBSPositions[carEnt:GetModel( )] = DBSPositions["default"]
    end
	
	carEnt.DBCarrage = carEnt.DBCarrage or 0
	
	if DBSPositions[carEnt:GetModel( )] and !DBSPositions[carEnt:GetModel( )][( carEnt.DBCarrage + 1 )] then return end
	
	carEnt.DBCarrage = ( carEnt.DBCarrage + 1 )
	
	carEnt:CallOnRemove(( "DB.VehicleRemoved_ID_"..carEnt:EntIndex( )), function( )
		DB.CarRemoved( seatEnt )
	end )
	
	ply.DBSeatEnt = seatEnt
	ply.DBCarEnt = carEnt
	ply.DBSeatEnt.IsOccupied = true
	
	ply:ExitVehicle( )
	
	ply:SetParent( seatEnt )
	ply:SetPos( DBSPositions[carEnt:GetModel( )][carEnt.DBCarrage] )
	ply:SetAngles( angle_zero )
	ply:SetMoveType( MOVETYPE_NOCLIP )
	ply:SetAngles( angle_zero )
end

function DB.DisableDriveBy( ply )
	if !ply or !ply:IsValid( ) then return end
	if ply.DBShooting then return end
	
	if !ply.DBSeatEnt or !ply.DBSeatEnt:IsValid( ) then return end
	if !ply.DBCarEnt or !ply.DBCarEnt:IsValid( ) then return end
	if ply.DBCarEnt.DBCarrage <= 0 then return end
	
	ply:SetMoveType( MOVETYPE_WALK )
	ply:SetParent( )
	ply:EnterVehicle( ply.DBSeatEnt )
	
	ply.DBSeatEnt.IsOccupied = nil
	ply.DBSeatEnt = nil
	
	ply.DBCarEnt.DBCarrage = ply.DBCarEnt.DBCarrage or 1
		ply.DBCarEnt.DBCarrage = ( ply.DBCarEnt.DBCarrage - 1 )
end

function DB.CarRemoved( seatEnt )
	for _, ply in pairs( player.GetAll( )) do
		if ply.DBShooting and ply:GetParent( ) == seatEnt then
			ply.DBSeatEnt = nil
			ply.DBCarEnt = nil
			ply.DBShooting = nil
			
			if ply.DBSeatEnt and ply.DBSeatEnt:IsValid( ) then
				ply.DBSeatEnt.IsOccupied = nil
					ply.DBSeatEnt = nil
			end
			
			ply:SetParent( )
			ply:SetAngles( angle_zero )
			ply:SetPos( ply:GetPos( ) + Vector( 0, 0, 10 ))
			ply:SetMoveType( MOVETYPE_WALK )
		end
	end
end

function DB.PlayerDisconnected( ply )
	if !ply or !ply:IsValid( ) then return end
	if !ply.DBShooting then return end
	if !ply.DBSeatEnt then return end
	if !ply.DBCarEnt then return end
	
	ply.DBSeatEnt.IsOccupied = nil
	ply.DBSeatEnt = nil
	
	ply.DBCarEnt.DBCarrage = ply.DBCarEnt.DBCarrage or 1
		ply.DBCarEnt.DBCarrage = ( ply.DBCarEnt.DBCarrage - 1 )
end

hook.Add( "PlayerDisconnected", "DB.PlayerDisconnected", DB.PlayerDisconnected )







/*
	This table represents a car.
	
	Indexed by model path!!
		(
			If you want a model, look at the car and type this into console:
			lua_run_cl print( LocalPlayer( ):GetEyeTrace( ).Entity:GetModel( ))
			
			Also, don't forget that `sv_allowcslua` must be set to `1`
		)
		
	Organized by a Vector
		(
			These are seperated by comas: ,
			Vector(
				X Left Or Right,
				Y Forward Or Back,
				Z Up or Down
			)
		)
		
	You can have as many vectors inside each car as you like. For cars that have four windows, it makes sense to allow Drive-Bys for all 4 seats.
	
	Guys, I did not include more because I haven't done the work yet. I might include more examples in a newer update.
	
	If you REALLY need help with understanding, please feel free to open a ticket.
*/


DBSPositions = {
	["models/tdmcars/gmcvan.mdl"] = { Vector( 20, 0, 10 ), Vector( 0, 50, 0 ) },
	["default"] = { Vector( 20, 0, 10 ), Vector( 0, 50, 0 ) }
}