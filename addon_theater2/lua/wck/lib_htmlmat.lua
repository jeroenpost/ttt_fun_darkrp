local htmlmat = {}
wck.htmlmat = htmlmat

htmlmat.MatCache = htmlmat.MatCache or {}
htmlmat.Progress = "idle"

local html_w, html_h = 1024, 1024

local shader = "VertexLitGeneric"

local matdata = {}
matdata["$basetexturetransform"] = "center .5 .5 scale 1.15 1.15 rotate 0 translate 0.015 0"

-- https://github.com/edunad/sprayurl/blob/master/lua/autorun/sprayurl_init.lua#L357

function htmlmat.CreatePanel(url, successCb, errorCb)
	local pnl = htmlmat.Panel
	if not IsValid(htmlmat.Panel) then
		pnl = vgui.Create("DHTML")
		htmlmat.Panel = pnl

		pnl:SetVisible(false)
	end
	timer.Destroy("htmlmat_PanelRemover")

	pnl:SetSize(html_w, html_h)

	timer.Create("HTMLMat_Timeout", 7, 1, function()
		errorCb("loading image timed out")
		if not IsValid(pnl) then return end

		pnl:Remove()
	end)

	pnl:AddFunction("htmlmat", "Finished", function(w, h)
		timer.Destroy("HTMLMat_Timeout")

		timer.Simple(0.5, function()
			if not IsValid(pnl) then
				errorCb("panel invalidated during Finished timer?")
				return
			end
			pnl:UpdateHTMLTexture()
			local html_mat = pnl:GetHTMLMaterial()
			if not html_mat then
				errorCb("html_mat == nil")
				return
			end

			local vertex_mat = CreateMaterial("htmlmat:" .. url, shader, matdata)
			local tex = pnl:GetHTMLMaterial():GetTexture("$basetexture")

			tex:Download()
			vertex_mat:SetTexture("$basetexture", tex)

			successCb(vertex_mat, w, h)

			--timer.Create("htmlmat_PanelRemover", 5, 1, function()
				htmlmat.Panel:Remove()
			--end)
		end)
	end)
	pnl:AddFunction("htmlmat", "Error", function(err)
		timer.Destroy("HTMLMat_Timeout")

		errorCb(err)

		--timer.Create("htmlmat_PanelRemover", 5, 1, function()
			htmlmat.Panel:Remove()
		--end)
	end)

	pnl:SetHTML([[
<html>
<head>
</head>
<style type="text/css">
html {
overflow:hidden;
}
#image {
max-width: ]] .. html_w .. [[px;
max-height: ]] .. html_h .. [[px;
margin: auto;
position: absolute;
top: 0; left: 0; bottom: 0; right: 0;
}
</style>
<body style="padding: 0px; margin: 0px;">
<script type="text/javascript">
function imageError()
{
htmlmat.Error("Image not found!");
};
window.onerror = function(message, file, lineNumber)
{
htmlmat.Error("Unknown Error :<");
};
function imageLoaded()
{
var image = document.getElementById("image");
if(image.width > 2816 || image.height > 1704) {
htmlmat.Error("Image too big! ( Max : 2816x1704 )");
} else {
htmlmat.Finished(image.width, image.height);
}
};
</script>
<img id="image" src="]] .. url .. [[" onerror="imageError()" onload="imageLoaded()" onabort="imageError()" />
</body>
</html>
	]])
end

htmlmat._Queue = {}

function htmlmat.Queue(url, callback)
	table.insert(htmlmat._Queue, {
		url = url,
		cb = callback
	})
end

function htmlmat.Process()
	if htmlmat.Progress ~= "idle" then return end

	local item = table.remove(htmlmat._Queue, 1)
	if not item then return end

	local cached = htmlmat.MatCache[item.url]
	if cached then
		item.cb{mat = cached}
		return
	end

	htmlmat.Progress = "loading"
	htmlmat.CreatePanel(item.url, function(mat, w, h)
		item.cb{mat = mat}
		htmlmat.MatCache[item.url] = mat

		htmlmat.Progress = "idle"
	end, function(err)
		item.cb{err = err}
		print("[HTMLMat] Loading material for url '", item.url, "' failed: ", err)

		htmlmat.Progress = "idle"
	end)
end
hook.Add("Think", "WCKProcessHTMLMats", htmlmat.Process)


concommand.Add("htmlmattest", function()
	local mat, w, h

	htmlmat.Queue("http://i.imgur.com/lc15V.jpg?" .. os.time(), function(data)
		if data.mat then
			mat = data.mat
		end
		if data.err then print("fail: ", data.err) end
	end)

	hook.Add("HUDPaint", "TestHTMLMat", function()
		if not mat then return end

		surface.SetMaterial(mat)
		surface.SetDrawColor(255, 255, 255)
		surface.DrawTexturedRect(0, 0, 512, 512)

		surface.DrawOutlinedRect(0, 0, 512, 512)
	end)
end)
