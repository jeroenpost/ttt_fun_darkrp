local t = nettable.get("WCKQueue")

local function canBypass(ply)
	if ply.query then return ply:query("ulx wckcontroller") end
	return ply:IsAdmin()
end

local plyCache = {}
timer.Create("WCK_WeaponAreaChecker", 1, 0, function()
	if not t.cinemas then return end

	plyCache = {}
	local plys = player.GetAll()

	for cid,c in pairs(t.cinemas) do
		if c.stripSweps then
			for _,ply in pairs(plys) do
				if wck.IsWithinPlayRange(ply:EyePos(), cid) then
					plyCache[ply] = true
				end
			end
		end
	end
end)

hook.Add("PlayerSpawnProp", "WCK_BlockProps", function(ply, model)
	if plyCache[ply] and not canBypass(ply) then
		ply:ChatPrint("Sorry! No prop spawning in cinema.")
		return false
	end
end)

hook.Add("StartCommand", "WCK_ForceHolstered", function(ply, cmd)
	if plyCache[ply] and not canBypass(ply) then
		local holsterWep = ply:GetWeapon("keys")
		if not IsValid(holsterWep) then holsterWep = ply:GetWeapon("weapon_physgun") end

		if IsValid(holsterWep) and ply:GetActiveWeapon() ~= holsterWep then
			ply:ChatPrint("Sorry! No weapons in cinema.")
			cmd:SelectWeapon(holsterWep)
		end
	end
end)
