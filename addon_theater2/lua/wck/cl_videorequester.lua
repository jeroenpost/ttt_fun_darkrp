local SKIN = {}

SKIN.DermaVersion	= 1
SKIN.GwenTexture	= Material( "gwenskin/GModDefault.png" )

function SKIN:PaintFrame(panel, w, h)
	surface.SetDrawColor(255, 255, 255)
	surface.DrawRect(0, 0, w, h)

	surface.SetDrawColor(127, 127, 127)
	surface.DrawOutlinedRect(0, 0, w, h)

	surface.SetDrawColor(50, 50, 50)
	surface.DrawRect(0, 0, w, 25)
end
function SKIN:PaintButton( panel, w, h )
	-- dont draw image buttons
	if panel:GetText() == "" then return end

	if panel.Depressed or panel:IsSelected() or panel:GetToggle() then
		-- TODO
	elseif ( panel:GetDisabled() ) then
		surface.SetDrawColor(170, 170, 170)
	elseif ( panel.Hovered ) then
		surface.SetDrawColor(100, 100, 100)
	else
		surface.SetDrawColor(50, 50, 50)
	end

	surface.DrawRect(0, 0, w, h)

	surface.SetDrawColor(0, 0, 0)
	surface.DrawOutlinedRect(0, 0, w, h)
end

derma.DefineSkin("WyoziCK", "l337", SKIN)

local RequestFrame
local function VideoSelector(onAccept)
	local frame = vgui.Create("DFrame")
	frame:SetTitle("WCK Video Requester")
	frame:SetSkin("WyoziCK")
	frame:SetSize(ScrW() * 0.8, ScrH() * 0.8)
	frame:Center()

	RequestFrame = frame

	local top = frame:Add("DPanel")
	top:Dock(TOP)

	local htmlcontrols = top:Add("DPanel")
	htmlcontrols:Dock(LEFT)
	htmlcontrols.Paint = function(_, w, h)
		surface.SetDrawColor(80, 80, 80)
		surface.DrawRect(0, 0, w, h)
	end

	local btn_back = htmlcontrols:Add("DImageButton")
	btn_back:SetMaterial("gui/HTML/back")
	btn_back:SetSize(24, 24)
	btn_back:SetDisabled(true)

	local urlpanel = vgui.Create("DTextEntry", top)
	urlpanel:Dock(FILL)

	local reqbutton = top:Add("DButton")
	reqbutton:Dock(RIGHT)
	reqbutton:SetWide(200)
	reqbutton:SetText("Request this video")
	reqbutton:SetTextColor(Color(200, 200, 200, 255))

	reqbutton.Think = function(pself)
		local vid = wck.medialib.load("media").GuessService(urlpanel:GetText())

		local enabled = vid ~= nil
		pself:SetDisabled(not enabled)
	end

	reqbutton.DoClick = function()
		onAccept(urlpanel:GetText())

		frame:Close()
	end

	local html = vgui.Create("DHTML", frame)
	html:Dock(FILL)
	html.History = {}

	-- Stupid garryHTML makes the page white while loading.
	html.Paint = function() end

	local oldcm = html.ConsoleMessage
	html.ConsoleMessage = function(pself, msg)
		if not msg then return end

		-- Filter some things out
		if string.find(msg, "XMLHttpRequest") then return end
		if string.find(msg, "Unsafe JavaScript attempt to access") then return end

		return oldcm(pself, msg)
	end

	html:OpenURL("http://www.youtube.com")

	html:AddFunction("gmod", "SetUrl", function(url)
		if vgui.GetKeyboardFocus() == urlpanel then return end
		if html:IsLoading() then return end

		if string.StartWith(url, "data") then url = "cinema://start" end

		urlpanel:SetText(url)

		if urlpanel.Url and urlpanel.Url ~= url and html.History[#html.History] ~= urlpanel.Url then
			table.insert(html.History, urlpanel.Url)
		end
		urlpanel.Url = url

		btn_back:SetDisabled(#html.History == 0)
	end)
	html:AddFunction("gmod", "SetVolume", function(vol)
		RunConsoleCommand("wwc_volume", vol)
	end)

	btn_back.DoClick = function()
		local prev = table.remove(html.History, #html.History)
		if prev then
			html:OpenURL(prev)
		end
	end

	urlpanel.OnEnter = function()
		html:OpenURL(urlpanel:GetText())
	end

	timer.Create("WCKRequesterURLUpdater", 0.5, 0, function()
		if not IsValid(html) then
			timer.Remove("WCKRequesterURLUpdater")
			return
		end

		html:QueueJavascript("gmod.SetUrl(window.location.href)")
	end)

	frame:MakePopup()
end

local was_esc_down
hook.Add("Think", "WCKVidRequestEscClose", function()
	if not IsValid(RequestFrame) then return end

	local is_esc_down = input.IsKeyDown(KEY_ESCAPE)
	local esc_pressed = is_esc_down ~= was_esc_down and is_esc_down
	was_esc_down = is_esc_down

	if esc_pressed then
		local function CancelGUIOpen()
			if gui.IsGameUIVisible () then
				gui.HideGameUI ()
			else
				gui.ActivateGameUI ()
			end
		end

		RequestFrame:Close()
		CancelGUIOpen()
	end
end)

function wck.OpenVideoRequester(placeholderUrl, onAccept)
	VideoSelector(onAccept)
end
