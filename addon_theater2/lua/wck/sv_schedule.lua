local sched_data = nettable.get("WCKSchedule")

concommand.Add("wck_schedule", function(ply, cmd, args)
	local url = args[1]
	if not url then ply:ChatPrint("please provide an url") return end

	local time = tonumber(args[2]) or 3600
	local theater = args[3] or "main"

	local cinema = wck.cinema.Get(theater)
	if not cinema or not cinema:canControl(ply) then
		return ply:ChatPrint("no permission")
	end

	local service = wck.medialib.load("media").guessService(url)
	if not service then ply:ChatPrint("invalid url given") return end

	service:query(url, function(err, data)
		table.insert(sched_data, {
			url = url,
			title = data.title,
			duration = data.duration,
			time = os.time() + time,
			theater = theater
		})
		nettable.commit(sched_data)
	end)

	ply:ChatPrint("Video scheduled!")
end)

concommand.Add("wck_cancel", function(ply, cmd, args)
	if ply:IsValid() and not ply:IsSuperAdmin() then return ply:ChatPrint("no permission") end

	local url = args[1]

	for _,s in pairs(sched_data) do
		if s.url == url then
			table.RemoveByValue(sched_data, s)
		end
	end
	nettable.commit(sched_data)
end)

util.AddNetworkString("wck_schedulenotification")

local t = nettable.get("WCKQueue")
timer.Create("WCK_ScheduleChecker", 1, 0, function()
	local time = os.time()
	for k,d in pairs(sched_data) do
		if time >= d.time then
			local cinema = wck.cinema.GetOrCreate(d.theater)
			if cinema then
				cinema:Play(d.url)

				net.Start("wck_schedulenotification")
				net.WriteString(d.title)
				net.Broadcast()
			else
				MsgN("[WCK] Scheduled theater not found: ", d.theater)
			end

			table.remove(sched_data, k)
			nettable.commit(sched_data)
		end
	end
end)
