local t = nettable.get("WCKQueue")

local plyCache = {}
timer.Create("WCK_VoiceAreaChecker", 1, 0, function()
	if not t.cinemas then return end

	plyCache = {}
	local plys = player.GetAll()

	for cid,c in pairs(t.cinemas) do
		if c.muteVoice then
			for _,ply in pairs(plys) do
				if wck.IsWithinPlayRange(ply:EyePos(), cid) then
					plyCache[ply] = true
				end
			end
		end
	end
end)

hook.Add("PlayerCanHearPlayersVoice", "WCK_PlayerVoiceMuter", function(listener, talker)
	if plyCache[talker] then return false end
end)
