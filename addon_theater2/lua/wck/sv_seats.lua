local map_seats = {}

-- debug command
concommand.Add("wck_hitpos", function(ply)
	if not ply:IsSuperAdmin() then return end

	local hp = ply:GetEyeTrace().HitPos
	MsgN(string.format("Vector(%.0f, %.0f, %.0f),", hp.x, hp.y, hp.z))
end)

do -- rp_downtown_evilmelon_v2_fix
	local t = {}
	map_seats["rp_downtown_evilmelon_v2_fix"] = t

	-- Sofa chair groups
	local group_pos = {
		{4, Vector(222.902802, 3198.602539, 148)},
		{4, Vector(223, 3477, 152)},
		{4, Vector(312, 3478, 148)},
		{4, Vector(331, 3197, 128)},
		{4, Vector(542, 3487, 9)},
		{4, Vector(474, 3486, 33)},
		{4, Vector(372, 3484, 57)},

		{3, Vector(541, 3199, 9)},
		{3, Vector(472, 3198, 33)},
		{3, Vector(366, 3198, 57)},
	}

	for _,data in pairs(group_pos) do
		local seats = data[1]
		local sp = data[2]
		for i=0,seats-1 do
			local pos = sp + Vector(0, 46*i, 0)
			t[#t+1] = {pos = pos, angle = Angle(0, -90, 0), minmax = Vector(-30, -30, -30)}
		end
	end
end
do -- rp_downtown_v4c_v2
	local t = {}
	map_seats["rp_downtown_v4c_v2"] = t

	-- Sofa chair groups
	local group_pos = {
		Vector(-1578, 1773, -197),
		Vector(-1582, 1834, -221),
		Vector(-1579, 1889, -245),
		Vector(-1578, 1947, -269),
		Vector(-1785, 1946, -269),
		Vector(-1904, 1948, -269),
		Vector(-1907, 1889, -245),
		Vector(-1788, 1890, -245),
		Vector(-1788, 1835, -221),
		Vector(-1907, 1833, -221),
		Vector(-1905, 1772, -197),
		Vector(-1789, 1776, -197),
	}

	for _,sp in pairs(group_pos) do
		for i=0,3 do
			local pos = sp + Vector(-28*i, 0, 0)
			t[#t+1] = {pos = pos, angle = Angle(0, 0, 0), minmax = Vector(-20, -20, -20)}
		end
	end
end
do -- rp_headattackcity_v1
	local rawdata = {
		-- row no.1
		Vector(-8176, 9457, 217),
		Vector(-8204, 9457, 217),
		Vector(-8232, 9457, 217),
		Vector(-8260, 9457, 217),
		Vector(-8176, 9522, 209),
		Vector(-8204, 9522, 209),
		Vector(-8232, 9522, 209),
		Vector(-8260, 9522, 209),
		Vector(-8176, 9587, 201),
		Vector(-8204, 9587, 201),
		Vector(-8232, 9587, 201),
		Vector(-8260, 9587, 201),
		Vector(-8176, 9652, 193),
		Vector(-8204, 9652, 193),
		Vector(-8232, 9652, 193),
		Vector(-8260, 9652, 193),
		Vector(-8176, 9717, 185),
		Vector(-8204, 9717, 185),
		Vector(-8232, 9717, 185),
		Vector(-8260, 9717, 185),
		-- row no.2
		Vector(-8376, 9457, 217),
		Vector(-8404, 9457, 217),
		Vector(-8432, 9457, 217),
		Vector(-8460, 9457, 217),
		Vector(-8376, 9522, 209),
		Vector(-8404, 9522, 209),
		Vector(-8432, 9522, 209),
		Vector(-8460, 9522, 209),
		Vector(-8376, 9587, 201),
		Vector(-8404, 9587, 201),
		Vector(-8432, 9587, 201),
		Vector(-8460, 9587, 201),
		Vector(-8376, 9652, 193),
		Vector(-8404, 9652, 193),
		Vector(-8432, 9652, 193),
		Vector(-8460, 9652, 193),
		Vector(-8376, 9717, 185),
		Vector(-8404, 9717, 185),
		Vector(-8432, 9717, 185),
		Vector(-8460, 9717, 185),
		-- row no.3
		Vector(-8176, 9792, 177),
		Vector(-8204, 9792, 177),
		Vector(-8232, 9792, 177),
		Vector(-8260, 9792, 177),
		Vector(-8176, 9862, 177),
		Vector(-8204, 9862, 177),
		Vector(-8232, 9862, 177),
		Vector(-8260, 9862, 177),
		-- row no.4
		Vector(-8373, 9861, 177),
		Vector(-8401, 9861, 177),
		Vector(-8429, 9861, 177),
		Vector(-8457, 9861, 177),
	}

	local t = {}
	map_seats["rp_headattackcity_v1"] = t

	for _,d in pairs(rawdata) do
		t[#t+1] = {pos = d, angle = Angle(0, 0, 0), minmax = Vector(-20, -20, -20)}
	end
end
do -- rp_rockford_v2
	local rawdata = {
		-- left
		Vector(-864, 1193, 860),
		Vector(-904, 1193, 860),
		Vector(-944, 1193, 860),
		Vector(-864, 1273, 828),
		Vector(-904, 1273, 828),
		Vector(-944, 1273, 828),
		Vector(-864, 1353, 796),
		Vector(-904, 1353, 796),
		Vector(-944, 1353, 796),
		Vector(-864, 1433, 764),
		Vector(-904, 1433, 764),
		Vector(-944, 1433, 764),
		Vector(-864, 1513, 732),
		Vector(-904, 1513, 732),
		Vector(-944, 1513, 732),
		Vector(-864, 1593, 700),
		Vector(-904, 1593, 700),
		Vector(-944, 1593, 700),
		Vector(-864, 1673, 668),
		Vector(-904, 1673, 668),
		Vector(-944, 1673, 668),
		Vector(-864, 1753, 636),
		Vector(-904, 1753, 636),
		Vector(-944, 1753, 636),
		Vector(-864, 1833, 604),
		Vector(-904, 1833, 604),
		Vector(-944, 1833, 604),
		Vector(-864, 1913, 572),
		Vector(-904, 1913, 572),
		Vector(-944, 1913, 572),

		-- mid
		Vector(-1056, 1194, 859),
		Vector(-1096, 1194, 859),
		Vector(-1136, 1194, 859),
		Vector(-1176, 1194, 859),
		Vector(-1216, 1194, 859),
		Vector(-1256, 1194, 859),
		Vector(-1296, 1194, 859),
		Vector(-1336, 1194, 859),
		Vector(-1376, 1194, 859),
		Vector(-1416, 1194, 859),
		Vector(-1456, 1194, 859),
		Vector(-1496, 1194, 859),
		Vector(-1056, 1274, 827),
		Vector(-1096, 1274, 827),
		Vector(-1136, 1274, 827),
		Vector(-1176, 1274, 827),
		Vector(-1216, 1274, 827),
		Vector(-1256, 1274, 827),
		Vector(-1296, 1274, 827),
		Vector(-1336, 1274, 827),
		Vector(-1376, 1274, 827),
		Vector(-1416, 1274, 827),
		Vector(-1456, 1274, 827),
		Vector(-1496, 1274, 827),
		Vector(-1056, 1354, 795),
		Vector(-1096, 1354, 795),
		Vector(-1136, 1354, 795),
		Vector(-1176, 1354, 795),
		Vector(-1216, 1354, 795),
		Vector(-1256, 1354, 795),
		Vector(-1296, 1354, 795),
		Vector(-1336, 1354, 795),
		Vector(-1376, 1354, 795),
		Vector(-1416, 1354, 795),
		Vector(-1456, 1354, 795),
		Vector(-1496, 1354, 795),
		Vector(-1056, 1434, 763),
		Vector(-1096, 1434, 763),
		Vector(-1136, 1434, 763),
		Vector(-1176, 1434, 763),
		Vector(-1216, 1434, 763),
		Vector(-1256, 1434, 763),
		Vector(-1296, 1434, 763),
		Vector(-1336, 1434, 763),
		Vector(-1376, 1434, 763),
		Vector(-1416, 1434, 763),
		Vector(-1456, 1434, 763),
		Vector(-1496, 1434, 763),
		Vector(-1056, 1514, 731),
		Vector(-1096, 1514, 731),
		Vector(-1136, 1514, 731),
		Vector(-1176, 1514, 731),
		Vector(-1216, 1514, 731),
		Vector(-1256, 1514, 731),
		Vector(-1296, 1514, 731),
		Vector(-1336, 1514, 731),
		Vector(-1376, 1514, 731),
		Vector(-1416, 1514, 731),
		Vector(-1456, 1514, 731),
		Vector(-1496, 1514, 731),
		Vector(-1056, 1594, 699),
		Vector(-1096, 1594, 699),
		Vector(-1136, 1594, 699),
		Vector(-1176, 1594, 699),
		Vector(-1216, 1594, 699),
		Vector(-1256, 1594, 699),
		Vector(-1296, 1594, 699),
		Vector(-1336, 1594, 699),
		Vector(-1376, 1594, 699),
		Vector(-1416, 1594, 699),
		Vector(-1456, 1594, 699),
		Vector(-1496, 1594, 699),
		Vector(-1056, 1674, 667),
		Vector(-1096, 1674, 667),
		Vector(-1136, 1674, 667),
		Vector(-1176, 1674, 667),
		Vector(-1216, 1674, 667),
		Vector(-1256, 1674, 667),
		Vector(-1296, 1674, 667),
		Vector(-1336, 1674, 667),
		Vector(-1376, 1674, 667),
		Vector(-1416, 1674, 667),
		Vector(-1456, 1674, 667),
		Vector(-1496, 1674, 667),
		Vector(-1056, 1754, 635),
		Vector(-1096, 1754, 635),
		Vector(-1136, 1754, 635),
		Vector(-1176, 1754, 635),
		Vector(-1216, 1754, 635),
		Vector(-1256, 1754, 635),
		Vector(-1296, 1754, 635),
		Vector(-1336, 1754, 635),
		Vector(-1376, 1754, 635),
		Vector(-1416, 1754, 635),
		Vector(-1456, 1754, 635),
		Vector(-1496, 1754, 635),
		Vector(-1056, 1834, 603),
		Vector(-1096, 1834, 603),
		Vector(-1136, 1834, 603),
		Vector(-1176, 1834, 603),
		Vector(-1216, 1834, 603),
		Vector(-1256, 1834, 603),
		Vector(-1296, 1834, 603),
		Vector(-1336, 1834, 603),
		Vector(-1376, 1834, 603),
		Vector(-1416, 1834, 603),
		Vector(-1456, 1834, 603),
		Vector(-1496, 1834, 603),
		Vector(-1056, 1914, 571),
		Vector(-1096, 1914, 571),
		Vector(-1136, 1914, 571),
		Vector(-1176, 1914, 571),
		Vector(-1216, 1914, 571),
		Vector(-1256, 1914, 571),
		Vector(-1296, 1914, 571),
		Vector(-1336, 1914, 571),
		Vector(-1376, 1914, 571),
		Vector(-1416, 1914, 571),
		Vector(-1456, 1914, 571),
		Vector(-1496, 1914, 571),

		-- right
		Vector(-1607, 1193, 860),
		Vector(-1647, 1193, 860),
		Vector(-1687, 1193, 860),
		Vector(-1607, 1273, 828),
		Vector(-1647, 1273, 828),
		Vector(-1687, 1273, 828),
		Vector(-1607, 1353, 796),
		Vector(-1647, 1353, 796),
		Vector(-1687, 1353, 796),
		Vector(-1607, 1433, 764),
		Vector(-1647, 1433, 764),
		Vector(-1687, 1433, 764),
		Vector(-1607, 1513, 732),
		Vector(-1647, 1513, 732),
		Vector(-1687, 1513, 732),
		Vector(-1607, 1593, 700),
		Vector(-1647, 1593, 700),
		Vector(-1687, 1593, 700),
		Vector(-1607, 1673, 668),
		Vector(-1647, 1673, 668),
		Vector(-1687, 1673, 668),
		Vector(-1607, 1753, 636),
		Vector(-1647, 1753, 636),
		Vector(-1687, 1753, 636),
		Vector(-1607, 1833, 604),
		Vector(-1647, 1833, 604),
		Vector(-1687, 1833, 604),
		Vector(-1607, 1913, 572),
		Vector(-1647, 1913, 572),
		Vector(-1687, 1913, 572),
	}

	local t = {}
	map_seats["rp_rockford_v2"] = t

	for _,d in pairs(rawdata) do
		t[#t+1] = {pos = d, angle = Angle(0, 0, 0), minmax = Vector(-20, -20, -20)}
	end
end

map_seats["rp_rockford_v2a"] = map_seats["rp_rockford_v2"]
map_seats["rp_rockford_v2b"] = map_seats["rp_rockford_v2a"]

local function DidHitChair(tr, data)
	-- theres already an entity there
	if IsValid(data.Ent) then return false end

	local hp = tr.HitPos

	if data.minmax or (data.min and data.max) then
		local min = data.min or data.minmax
		local max = data.max or -min

		local diff = hp - data.pos
		if (diff.x >= min.x and diff.y >= min.y and diff.z >= min.z) and
		   (diff.x <= max.x and diff.y <= max.y and diff.z <= max.z) then
			return true, diff:LengthSqr()
		end
	end

	local distsqr = hp:DistToSqr(data.pos)
	if distsqr < 35^2 then
		return true, distsqr
	end

	return false
end

local function GetClosestChair(tr)
	local mapdata = map_seats[string.lower(game.GetMap())]
	if not mapdata then return end

	if tr.HitPos:Distance(tr.StartPos) > 128 then return end

	local score, data = math.huge
	for _,seat in pairs(mapdata) do
		local b, bscore = DidHitChair(tr, seat)
		if b and bscore < score then
			score, data = bscore, seat
		end
	end

	return data
end

hook.Add("KeyRelease", "WCK_SeatEnter", function(ply, key)
	if key ~= IN_USE then return end
	if IsValid(ply:GetVehicle()) then return end
	if ply.WCK_SeatLeftTime and ply.WCK_SeatLeftTime > CurTime()-0.2 then return end

	local tr = ply:GetEyeTrace()
	local chair = GetClosestChair(tr)
	if not chair then return end

	-- :((((((
	local ent = ents.Create("prop_vehicle_prisoner_pod")
	ent:SetModel("models/nova/airboat_seat.mdl")
	ent:SetKeyValue("vehiclescript","scripts/vehicles/prisoner_pod.txt")
	ent:SetPos(chair.pos)
	ent:SetAngles(chair.angle)
	ent:SetNotSolid(true)
	ent:SetNoDraw(true)

	ent:Spawn()
	ent:Activate()

	local phys = ent:GetPhysicsObject()
	if IsValid(phys) then
		phys:EnableMotion(false)
	end

	ent:SetCollisionGroup(COLLISION_GROUP_DEBRIS_TRIGGER)

	ent.WCK_SeatFor = ply
	ply.WCK_SeatEnterPos = ply:GetPos()
	ply.WCK_SeatEnteredTime = CurTime()
	ply.WCK_SeatEnt = ent

	chair.Ent = ent

	ply:EnterVehicle(ent)
end)

local function CheckVehicleExit(veh, ply)
	if ply.WCK_SeatLeftTime and ply.WCK_SeatLeftTime > CurTime()-0.1 then return end
	if veh.WCK_Removing then return end

	if veh.WCK_SeatFor then
		ply.WCK_SeatLeftTime = CurTime()
		ply:ExitVehicle()

		if ply.WCK_SeatEnterPos then
			ply:SetPos(ply.WCK_SeatEnterPos)
		end

		veh.WCK_Removing = true
		veh:Remove()

		return false
	end
end
hook.Add("CanExitVehicle", "WCK_TryLeave", CheckVehicleExit)

local function CheckVehicleExit2(ply)
	if ply:IsPlayer() then
		local veh = ply:GetVehicle()

		if IsValid(veh) then
			CheckVehicleExit(veh, ply)
		end
	end
end
hook.Add("PlayerLeaveVehicle", "WCK_VehicleDed", CheckVehicleExit2)
hook.Add("PlayerDeath", "WCK_VehicleDed", CheckVehicleExit2)
hook.Add("PlayerSilentDeath", "WCK_VehicleDed", CheckVehicleExit2)
hook.Add("EntityRemoved", "WCK_VehicleDed", CheckVehicleExit2)
