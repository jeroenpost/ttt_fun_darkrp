local t = nettable.get("WCKQueue")
t.cinemas = t.cinemas or {}

wck.tdata = {}
function wck.tdata.Exists(cid)
	return t.cinemas[cid] ~= nil
end
function wck.tdata.Get(cid)
	return t.cinemas[cid]
end
function wck.tdata.Create(cid)
	t.cinemas[cid] = {}
	nettable.commit(cid)
	return wck.tdata.Get(cid)
end

wck.cinema = {}
function wck.cinema.Exists(id)
	return t.cinemas[id] ~= nil
end
function wck.cinema.Get(id)
	return t.cinemas[id]
end
function wck.cinema.GetAll()
	return t.cinemas
end
function wck.cinema.Create(id)
	t.cinemas[id] = setmetatable({}, wck.MT_Cinema)
	nettable.commit(t)
	return wck.cinema.Get(id)
end
function wck.cinema.GetOrCreate(id)
	local ci = wck.cinema.Get(id)
	if ci then return ci end

	return wck.cinema.Create(id)
end
