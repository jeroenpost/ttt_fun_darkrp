local TR_MASK = MASK_SOLID
local TR_FILTER = function(ent)
	local clz = ent:GetClass()
	if clz == "func_door" or clz == "prop_door_rotating" then return true end

	return false
end

if CLIENT then
	local wck_testtr = CreateConVar("wck_testvistr", "0")
	hook.Add("HUDPaint", "WCK_VisibilityTrace", function()
		if not wck_testtr:GetBool() then return end

		local tr = util.TraceLine {
			start = LocalPlayer():EyePos(),
			endpos = LocalPlayer():EyePos() + LocalPlayer():GetAimVector() * 10000,
			mask = TR_MASK,
			filter = TR_FILTER
		}
		if tr.Hit then debugoverlay.Sphere(tr.HitPos, 8, 0.05, nil) end
		draw.SimpleText(tostring(tr.Entity), "DermaDefault", 10, 10)
	end)
end

local function TestVis(pos1, pos2)
	local startPos, endPos = pos1, pos2
	local dist = startPos:Distance(endPos)
	if dist < 256 then return true end
	if dist > 1024 then return false end

	local tr = util.TraceLine {
		start = startPos,
		endpos = endPos,
		mask = TR_MASK,
		filter = TR_FILTER
	}
	return not tr.Hit
end

function wck.IsWithinPlayRange(pos, cid)
	for _,v in pairs(ents.FindByClass("wck_projector")) do
		if v:GetCinemaId() == cid and TestVis(pos, v:GetPos()) then return true end
	end
	for _,v in pairs(ents.FindByClass("wck_controller")) do
		if v:GetCinemaId() == cid and TestVis(pos, v:GetPos()) then return true end
	end
	return false
end

function wck.GetPlayerCountAround(cid)
	local c = 0
	for _,ply in pairs(player.GetAll()) do
		if wck.IsWithinPlayRange(ply:EyePos(), cid) then
			c = c+1
		end
	end
	return c
end

-- amount of votes to skip
function wck.GetSkipAmount(total)
	return math.ceil(total/2)
end
