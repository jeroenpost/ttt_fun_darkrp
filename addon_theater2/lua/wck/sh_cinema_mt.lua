local Cinema = wck.MT_Cinema or {}
wck.MT_Cinema = Cinema

-- give it the metatable touch
Cinema.__index = Cinema

if CLIENT then return end

function Cinema:canControl(cl)
	if TEAM_CINEMAOWNER and cl:Team() == TEAM_CINEMAOWNER then return true end
	if cl.query then return cl:query("ulx wckcontroller") end

	return cl:IsAdmin()
end

function Cinema:Update()
	nettable.commit("WCKQueue")

	if self.cur then
		self:TurnMapLightsOff()
	else
		self:TurnMapLightsOn()
	end
end

function Cinema:IsForceSkipping()
	return self.cur ~= nil and self.cur.skip
end
function Cinema:ForceSkip()
	if self.cur then self.cur.skip = true end
end

function Cinema:IsPlaying()
	if self.cur == nil then return false end
	local el, dur = self:GetElapsed(), self:GetDuration()
	if dur == -1 then return true end

	return el < dur
end

-- -1 if streaming
function Cinema:GetDuration()
	if not self.cur then return 0 end
	return self.cur.duration or -1
end

function Cinema:GetElapsed()
	if not self.cur then return 0 end
	return CurTime() - self.cur.startTime
end

function Cinema:Play(url, data)
	if url == nil then
		self.cur = nil
		self:Update()
		return
	end
	if data and data.querydata then
		local requester = data.requester

		local qd = data.querydata
		self.cur = {url = url, title = qd.title, duration = qd.duration, startTime = CurTime(), requester = requester}

		self:Update()
		return
	end

	local service = wck.medialib.load("media").guessService(url)
	if not service then
		if data and data.onError then data.onError("Trying to play media with invalid service") end
		return
	end

	service:query(url, function(err, qd)
		if qd then
			local requester = data and data.requester
			self.cur = {url = url, title = qd.title, duration = qd.duration, startTime = CurTime(), requester = requester}
			self:Update()
			if data and data.onSuccess then data.onSuccess(qd) end
		else
			if data and data.onError then data.onError("Failed to play media: " .. (err or "unknown error")) end
		end
	end)
end

local cvar_blacklist = CreateConVar("wck_blacklist", "", FCVAR_ARCHIVE)
local function isBlacklisted(title)
	local blstring = cvar_blacklist:GetString():Trim()
	if blstring == "" then return false end

	local list = string.Split(blstring, ",")
	for _,word in pairs(list) do
		if title:lower():find(word:lower()) then
			return true, word
		end
	end

	return false
end

local cvar_video_maxduration = CreateConVar("wck_visqueue_maxduration", "0", FCVAR_ARCHIVE, "maximum video duration for videos queued in visitor controller")
local cvar_video_blacklistUrlServices = CreateConVar("wck_visqueue_blacklisturls", "1", FCVAR_ARCHIVE)
function Cinema:Queue(url, data)
	self.queue = self.queue or {}

	local serviceBlacklist

	if cvar_video_blacklistUrlServices:GetBool() then
		serviceBlacklist = {"webradio", "webm", "mp4", "webaudio"}
	end

	local service = wck.medialib.load("media").guessService(url, {blacklist = serviceBlacklist})
	if not service then
		if data and data.onError then data.onError("Trying to queue media with invalid service") end
		return
	end

	service:query(url, function(err, qd)
		if qd then
			local bl, blword = isBlacklisted(qd.title)
			if bl then
				if data and data.onError then
					data.onError("Unable to queue given video: title contains blacklisted word '" .. tostring(blword) .. "'")
				end
				return
			end

			local max_duration = cvar_video_maxduration:GetFloat()
			if data and data.visitor_controller and max_duration > 0 and (not qd.duration or qd.duration > max_duration) then
				if data and data.onError then
					data.onError("Unable to queue given video: too long (max " .. tostring(cvar_video_maxduration:GetFloat()) .. "s). Try asking cinema owner to play it!")
				end
				return
			end

			local requester = data and data.requester
			table.insert(self.queue, {url = url, title = qd.title, duration = qd.duration, requester = requester})
			self:Update()
			if data and data.onSuccess then data.onSuccess(qd) end
		else
			if data and data.onError then data.onError("Failed to queue media: " .. (err or "unknown error")) end
		end
	end)
end

function Cinema:ConnectToMapLights(targetname)
	self._mapLightsTargetname = targetname
end
function Cinema:TurnMapLightsOn()
	if self._mapLightsTargetname then
		for _,e in pairs(ents.FindByName(self._mapLightsTargetname)) do
			e:Fire("FadeToPattern", "s", 0)
		end
	end
end
function Cinema:TurnMapLightsOff()
	if self._mapLightsTargetname then
		for _,e in pairs(ents.FindByName(self._mapLightsTargetname)) do
			e:Fire("FadeToPattern", "b", 0)
		end
	end
end

-- Calculates skip amounts, propagates them to appropriate entities and finally
-- returns the skip amount
function Cinema:SkipThink(cid)
	local projs, controllers = {}, {}

	-- find controllers with this cinema id
	for _,e in pairs(ents.FindByClass("wck_projector")) do
		if e:GetCinemaId() == cid then
			projs[#projs + 1] = e
		end
	end
	for _,e in pairs(ents.FindByClass("wck_controller")) do
		if e:GetCinemaId() == cid then
			controllers[#controllers + 1] = e
		end
	end

	-- count of players within range to
	local c = 0
	for _,ply in pairs(player.GetAll()) do
		local withinRange = false

		for _,e in pairs(projs) do
			if e:TestPVS(ply) then withinRange = true break end
		end
		if not withinRange then
			for _,e in pairs(controllers) do
				if e:TestPVS(ply) then withinRange = true break end
			end
		end

		if withinRange then
			c = c+1
		end
	end

	local total = c
	local skipAmount = math.ceil(total/2)

	for _,e in pairs(ents.FindByClass("wck_controller_vis")) do
		if e:GetCinemaId() == cid then
			e:UpdateSkipPlayerAmount(skipAmount)
		end
	end

	return skipAmount
end