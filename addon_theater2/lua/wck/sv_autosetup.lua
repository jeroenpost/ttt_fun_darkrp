-- Hello there
-- You must be here to set up theater entity positions. This is the wrong way to do it, but worry not.
-- Simply spawn a projector/whatever entity ingame, position it properly, hold down 'c', right click the entity
-- and press 'Make persistent'.
--
-- If that does not work for the gamemode you're on, feel free to leave a ticket or add me on Steam. Cheers.

local autosetup_maps = {
	["rp_downtown_v4c_v2"] = {
		{
			projector = {pos = Vector(-1795.750000, 1679.062500, -120.281250), angle = Angle(-0.483398, 180, 9.228516), fov = 52},
			control = {pos = Vector(-1706.343750, 1525.531250, 22.781250), angle = Angle(0.175781, -180, -0.131836)},
			viscontrol = {pos = Vector(-1562.250000, 1706.468750, -138.843750), angle = Angle(0, -180, 0)}
		}
	},
	["rp_downtown_evilmelon_v2_fix"] = {
		{
			projector = {pos = Vector(339.593750, 3399.781250, 155.718750), angle = Angle(0.043945, 88.198242, 6.855469), fov = 63},
			control = {pos = Vector(743.906250, 3664.375000, 286.812500), angle = Angle(-0.175781, 90.307617, -0.043945)},
			viscontrol = {pos = Vector(596.000000, 3109.312500, 35.687500), angle = Angle(-0.043945, 90.043945, -0.219727)},

			projector_stand = {model = "models/props_trainstation/trainstation_post001.mdl", pos = Vector(336.406250, 3400.250000, 104.375000), angle = Angle(0, 0, 0)}
		}
	},
	["rp_rockford_v2"] = {
		{
			projector = {pos = Vector(-1268.562500, 1533.062500, 864.531250), angle = Angle(-0.131836, -178.461914, 6.679688), fov = 60, use3d2d = true},
			control = {pos = Vector(-1000.125000, 1163.437500, 889.000000), angle = Angle(0, 90, 0)},
			viscontrol = {pos = Vector(-1716.625000, 1967.218750, 601.937500), angle = Angle(0, 0, 0)}
		}
	},
	["resort"] = {
		{
			projector = {pos = Vector(1882.750000, -178.062500, -147.718750), angle = Angle(-0.527344, -89.780273, 16.083984), fov = 50, use3d2d = true},
			viscontrol = {pos = Vector(1835.843750, -436.531250, -173.750000), angle = Angle(0, 90, 0)},

			lights = {targetname = "theater1_lights"}
		}
	}
}
local autosetup_patterns = {
	-- Presumption: cinema coordinates will never change
	["rp_rockford_v2.+"] = autosetup_maps["rp_rockford_v2"]
}

local autosetup_cvar = CreateConVar("wck_autosetup", "1", FCVAR_ARCHIVE)

local function SpawnEnt(clz, data)
	local e = ents.Create(clz)
	e:SetPos(data.pos)
	e:SetAngles(data.angle)

	if data.model then e:SetModel(data.model) end

	e:Spawn()

	local phys = e:GetPhysicsObject()
	if IsValid(phys) then phys:EnableMotion(false) end

	return e
end

hook.Add("InitPostEntity", "WyoziCK_AutoSetup", function()
	if not autosetup_cvar:GetBool() then return end

	local lowermap = string.lower(game.GetMap())
	local mapdata = autosetup_maps[lowermap]

	if not mapdata then
		for patt,md in pairs(autosetup_patterns) do
			if lowermap:match(patt) then
				mapdata = md
				break
			end
		end

		if not mapdata then return end
	end

	MsgC(Color(255, 127, 0), "Wyozi Cinema Kit autosetup complete.\n")

	for _,theater in pairs(mapdata) do
		local proj = theater.projector
		if proj then
			local e = SpawnEnt("wck_projector", {pos = proj.pos, angle = proj.angle})
			if proj.fov then e:SetProjectorFOV(proj.fov) end
			if proj.use3d2d then e:SetUse3D2D(true) end

			local proj_stand = theater.projector_stand
			if proj_stand then
				SpawnEnt("prop_physics", {pos = proj_stand.pos, angle = proj_stand.angle, model = proj_stand.model})
			end
		end
		if theater.control then
			local control = theater.control
			SpawnEnt("wck_controller", {pos = control.pos, angle = control.angle})
		end
		if theater.viscontrol then
			local viscontrol = theater.viscontrol
			SpawnEnt("wck_controller_vis", {pos = viscontrol.pos, angle = viscontrol.angle})
		end
		if theater.lights then
			local cinema = wck.cinema.GetOrCreate("main") -- TODO cinema id
			cinema:ConnectToMapLights(theater.lights.targetname)
		end
	end
end)
