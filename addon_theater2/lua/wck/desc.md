Wyozi Cinema Kit is the way to add cinema functionality to your server. Whether you want playerbase controlled Cinema gamemode functionality or simply host admin-controlled movie nights, WCK makes it possible.

##Media
[Controller](https://www.youtube.com/watch?v=S1yaDpyeyGc)  
[Visitor Controller](https://www.youtube.com/watch?v=JmYYGSFMxqc)

##Included entities
- Projector
	- Projects videos using env_projectedtexture, which supports arbitrarily sized and shaped projection screens (eg. projecting on a round wall) and adds to realism
	- Also supports 3D2D video projection, which might work better in some maps/lighting configurations.
- Cinema Owner controller
	- Allows moderating the video queue  
	- Cinema owner can control whether players can voteskip or votequeue videos  
	- Cinema owner can also control if voice chat should be muted/weapons holstered inside cinema  
- Cinema Visitor controller (to suggest or queue videos)
	- If visitor controller is spawned players can add videos to the queue directly simulating the functionality of Cinema gamemode.
	- Also allows voteskipping videos
- Posters
	- The image will be fetched from internet. No additional downloads needed.  
	- Cached into a texture. No performance hit after initial load.  
	- Usage is not limited to theaters. Posters can be used for general roleplay as well.
- Schedule screen
	- Allows admins to schedule a video to start at a certain time and displays it on the screen.

##Features
- First class DarkRP and sandbox support. If you are hosting some other gamemode and are not sure about compatibility, please contact me.
- Automatically adds sittable seat entities to some popular maps' theaters (rp_downtown_v4c_v2, rp_downtown_evilmelon_v2 etc)  
- Queue support (voteskipping, forceskipping, queue management)  
- Intuitive 3D2D controls, which make it easy to manage the queue and change settings.  
- Supports Youtube, Vimeo, Twitch and more by default.
- Simple installation that does not require editing config files and works out-of-box for popular DarkRP maps.  
- Multiple cinema support (each WCK entity has a cinema id property)  
- Flexible and customizable. All of the following configurations are possible:
	- Theater with only admin control  
	- Theater with only cinema owner control  
	- Theater where videos are queued by players and moderated by cinema owner  
	- Theater where videos are queued only by players.  

## Installation
Unzip addon to addons folder and restart server. WCK automatically places projectors and controllers for some popular darkrp maps. If not, you'll have to spawn the entities yourself from the spawnmenu and make them persistent using the Garry's Mod context menu.
