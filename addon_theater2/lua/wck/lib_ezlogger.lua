EzLogger = {}

EzLogger.Color = {
	White = Color(255, 255, 255),
	Black = Color(0, 0, 0),

	WhiteSmoke = Color(236,236,236),
	Lynch = Color(108, 122, 137),
	Pumice = Color(210, 215, 211),

	Red = Color(255, 0, 0),
	Chestnut = Color(210, 77, 87),
	Pomegranate = Color(242, 38, 19),

	Green = Color(0, 255, 0),
	Eucalyptus = Color(38, 166, 91),
	Emerald = Color(63, 195, 128),

	Blue = Color(0, 0, 255),
	SanMarino = Color(68,108,179),
	RoyalBlue = Color(65, 131, 215),
	SteelBlue = Color(75, 119, 190),
	FountainBlue = Color(92, 151, 191),

	Yellow = Color(255, 255, 0),
	RipeLemon = Color(247, 202, 24),
	CreamCan = Color(245, 215, 110),

	Orange = Color(255, 127, 0),
	Jaffa = Color(235, 151, 78),

	Purple = Color(102, 51, 153),
	Plum = Color(145, 61, 136),
}
EzLogger.NormalColor = EzLogger.Color.White

if CLIENT then
	local function ReadableColor(orig)
		local h, s, v = ColorToHSV(orig)
		return HSVToColor((h + 180)%360, 1 - s, 1 - v)
	end

	concommand.Add("logger_colors", function()
		local tile_size = 100

		hook.Add("HUDPaint", "EzLogger_ShowColors", function()
			local grid_side = math.ceil(math.sqrt(table.Count(EzLogger.Color)))

			local i = 1
			for k,v in pairs(EzLogger.Color) do
				local y = math.floor((i-1) / grid_side)
				local x = (i-1) % grid_side

				surface.SetDrawColor(v)
				surface.DrawRect(x*tile_size, y*tile_size, tile_size, tile_size)

				draw.SimpleText(k, "DermaDefaultBold", x*tile_size + 5, y*tile_size + tile_size - 20, ReadableColor(v))

				i = i+1
			end
		end)

		timer.Create("EzLoggerColorHide", 10, 1, function() hook.Remove("HUDPaint", "EzLogger_ShowColors") end)
	end)
end

local obj = {}
obj.__index = obj

function EzLogger.NiceColorPlease()
	return HSVToColor(math.random(0, 360), 0.5, 0.8)
end

function EzLogger.wrap(clr, ...)
	local t = {...}
	return function()
		local t2 = {clr}
		table.Add(t2, t)
		t2[#t2+1] = EzLogger.NormalColor
		return unpack(t2)
	end
end

function EzLogger.Format(v, treatAsUnknown)
	if type(v) == "Player" then
		return team.GetColor(v), (treatAsUnknown and ("Player[" .. v:Nick() .. "]") or v:Nick()), EzLogger.NormalColor
	elseif type(v) == "Color" then
		return treatAsUnknown and ("Color(".. v.r ..", ".. v.g ..", ".. v.b .. (v.a ~= 255 and (", " .. v.a) or "") .. ")") or v
	elseif type(v) == "function" then
		return v(treatAsUnknown)
	elseif type(v) == "table" then
		local t = {"{"}

		if table.IsSequential(v) then
			local commaNeeded = false
			for i=1, #v do
				if commaNeeded then
					table.insert(t, ", ")
				else
					commaNeeded = true
				end

				table.Add(t, {EzLogger.Format(v[i], true)})
			end
		else
			local commaNeeded = false
			for k,v in pairs(v) do
				if commaNeeded then
					table.insert(t, ", ")
				else
					commaNeeded = true
				end

				table.Add(t, {EzLogger.Format(k, true)})
				table.insert(t, " = ")
				table.Add(t, {EzLogger.Format(v, true)})
			end
		end

		table.insert(t, "}")

		return EzLogger.Format(EzLogger.wrap(EzLogger.Color.Pumice, unpack(t)))
	elseif type(v) == "Vector" then
		return string.format("Vec(%f, %f, %f)", v.x, v.y, v.z)
	elseif type(v) == "Angle" then
		return string.format("Ang(%f, %f, %f)", v.p, v.y, v.r)
	elseif type(v) == "number" then
		return EzLogger.Format(EzLogger.wrap(EzLogger.Color.Plum, v))
	else
		return treatAsUnknown and string.format("%q", tostring(v)) or tostring(v)
	end
end
function EzLogger.FormatStr(...)
	local retTable = {EzLogger.NormalColor}

	for _,v in pairs{...} do
		local t = {EzLogger.Format(v)}
		table.Add(retTable, t)
	end

	return retTable
end

function obj:subCat(name, clr)
	self.subCats = self.subCats or {}

	local cat = self.subCats[name]
	if cat then return cat end

	local cats = {}
	table.Add(cats, self.cats)
	table.insert(cats, {name = name, clr = clr or EzLogger.NiceColorPlease()})

	cat = setmetatable({cats = cats}, obj)
	self.subCats[name] = cat

	return cat
end

function obj:printCategory()
	for _,cat in pairs(self.cats) do
		MsgC(cat.clr, "[" .. cat.name .. "]")
	end

	if #self.cats > 0 then Msg(" ") end
end
function obj:printVarargs(...)
	local formatted = EzLogger.FormatStr(...)
	MsgC(unpack(formatted))
	MsgN("")
end

function obj:i(...)
	self:printCategory()
	self:printVarargs(...)
end
function obj:w(...)
	self:printCategory()
	self:printVarargs(...)
end
function obj:d(...)
	self:printCategory()
	self:printVarargs(...)
end

local instance = setmetatable({cats = {}}, obj)
function EzLogger.new(...)
	return instance:subCat(...)
end

function EzLogger.i(...)
	instance:i(...)
end
function EzLogger.w(...)
	instance:w(...)
end
function EzLogger.d(...)
	instance:d(...)
end
