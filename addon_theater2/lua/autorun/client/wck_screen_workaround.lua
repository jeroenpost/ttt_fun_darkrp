-- Some maps are stupid and use textures for projecting screens that don't work
-- with env_projectedtexture, we replace the texture with something better in
-- those maps.
--
-- Code partially from CapsAdmin's TOD, cheers

CreateMaterial("wck_debugblack", "VertexLitGeneric", {
	["$basetexture"] = "color/white"
})

local workaround_maps = {
	["rp_downtown_v4c_v2"] = {"cs_italy/black"},
	["rp_downtown_evilmelon_v2_fix"] = {"cs_italy/black"},
	["rp_eastcoast"] = {"lights/white001"}
	--["rp_industrial17_v1"] = {"dev/dev_combinemonitor_1"}
}

local replaced_textures = {}

local function ReplaceTexture(path, to)
	path = path:lower()

	local mat = Material(path)

	if mat and not mat:IsError() then

		local typ = type(to)
		local tex

		if typ == "string" then
			tex = Material(to):GetTexture("$basetexture")
		elseif typ == "ITexture" then
			tex = to
		elseif typ == "Material" then
			tex = to:GetTexture("$basetexture")
		else return false end

		replaced_textures[path] = replaced_textures[path] or {}

		replaced_textures[path].OldTexture = replaced_textures[path].OldTexture or mat:GetTexture("$basetexture")
		replaced_textures[path].NewTexture = tex

		mat:SetTexture("$basetexture",tex)

		return mat
	end

	return false
end

hook.Add("Think", "wck_replacetextures", function()
	if not LocalPlayer():IsValid() then return end

	local map = game.GetMap():lower()
	local workaround_textures = workaround_maps[map]

	if workaround_textures then
		for k,v in pairs(workaround_textures) do
			ReplaceTexture(v, "models/alyx/plyr_sheet")
		end
	end

	hook.Remove("Think", "wck_replacetextures")
end)
