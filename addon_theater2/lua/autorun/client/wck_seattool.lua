WCKST = WCKST or {}
WCKST.seat = WCKST.seat or { count = 1, offset = Vector() }

concommand.Add("wck_st_setseat", function(ply, cmd, args)
	local seats = tonumber(args[1])
	local offset = Vector(tonumber(args[2]), tonumber(args[3]), tonumber(args[4]))

	WCKST.seat = { count = seats, offset = offset }
end)
concommand.Add("wck_st_setrowstart", function(ply, cmd, args)
	local offset = Vector(tonumber(args[1]) or 0, tonumber(args[2]) or 0, tonumber(args[3]) or 0)
	WCKST.rowstart = ply:GetEyeTrace().HitPos + offset
end)
concommand.Add("wck_st_setrow", function(ply, cmd, args)
	local seats = tonumber(args[1])
	local offset = Vector(tonumber(args[2]), tonumber(args[3]), tonumber(args[4]))

	WCKST.row = { count = seats, offset = offset }
end)

local function GetSeats()
	if not WCKST.rowstart then return {} end

	local row = WCKST.row or { count = 1, offset = vector_origin }
	local x = {}

	for i=1,row.count do
		local base = WCKST.rowstart + row.offset*(i-1)

		for i=1,WCKST.seat.count do
			local pos = base + WCKST.seat.offset * (i-1)
			x[#x+1] = pos
		end
	end

	return x
end

concommand.Add("wck_st_print", function()
	for _,pos in pairs(GetSeats()) do
		MsgN(string.format("Vector(%d, %d, %d),", pos.x, pos.y, pos.z))
	end
end)

hook.Add("Think", "WCKST", function()
	if not WCKST then return end

	for _,pos in pairs(GetSeats()) do
		debugoverlay.Sphere(pos, 8, 0.05)
	end
end)
