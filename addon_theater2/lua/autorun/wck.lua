wck = {}

function wck.include_cl(file)
	if SERVER then AddCSLuaFile(file) end
	if CLIENT then return include(file) end
end
function wck.include_sv(file)
	if SERVER then return include(file) end
end
function wck.include_sh(file)
	return wck.include_cl(file) or wck.include_sv(file)
end

wck.medialib = wck.include_sh("wck/lib_medialib.lua")
wck.tdui = wck.include_cl("wck/lib_tdui.lua")
wck.include_sh("wck/lib_nettable.lua")
wck.include_cl("wck/lib_htmlmat.lua")
wck.include_sh("wck/lib_ezlogger.lua")

wck.include_sh("wck/sh_config.lua")

wck.include_cl("wck/cl_videorequester.lua")

wck.include_sh("wck/sh_cinema_mt.lua")
wck.include_sv("wck/sv_api.lua")

wck.include_sv("wck/sv_autosetup.lua")
wck.include_sv("wck/sv_queue.lua")
wck.include_sv("wck/sv_seats.lua")
wck.include_sv("wck/sv_voiceblocker.lua")
wck.include_sv("wck/sv_weaponblocker.lua")

wck.include_cl("wck/cl_schedule.lua")
wck.include_sv("wck/sv_schedule.lua")
