AddCSLuaFile()

ENT.PrintName		= "Controller"
ENT.Author			= "Wyozi"
ENT.Category		= "Wyozi Cinema Kit"

ENT.Editable		= true
ENT.Spawnable		= true
ENT.AdminOnly		= true

ENT.Type = "anim"
ENT.RenderGroup = RENDERGROUP_BOTH
ENT.Model = "models/props/de_nuke/NuclearControlBox.mdl"

function ENT:SetupDataTables()
	self:NetworkVar("String", 0, "CinemaId", { KeyName = "cinemaid", Edit = { type = "String", category = "Cinema", order = 1 } })

	if SERVER then
		self:SetCinemaId("main")
	end
end

function ENT:Initialize()
	if SERVER then
		self:SetModel(self.Model)
		self:PhysicsInit(SOLID_VPHYSICS)

		self:SetColor(Color(0, 0, 0))

		self:SetCinemaId("main")
		wck.cinema.GetOrCreate(self:GetCinemaId())
	end
end

function ENT:CPPICanPickup()
	return false
end

local cfg = {
	{
		text = "Visitor queue",
		id = "visQueueDisallowed",
		value = function(ent, tdata)
			return not (tdata and tdata.visQueueDisallowed)
		end
	},
	{
		text = "Visitor voteskip",
		id = "voteSkipDisallowed",
		value = function(ent, tdata)
			return not (tdata and tdata.voteSkipDisallowed)
		end
	},
	{
		text = "Strip weapons",
		id = "stripSweps",
		value = function(ent, tdata)
			return (tdata and tdata.stripSweps)
		end
	},
	{
		text = "Mute voicechat",
		id = "muteVoice",
		value = function(ent, tdata)
			return (tdata and tdata.muteVoice)
		end
	},
}

local icon_yes = Material("icon16/accept.png")
local icon_no = Material("icon16/cross.png")

local clr_yes = Color(135, 211, 124)
local clr_no = Color(210, 77, 87)

local function darker(clr)
	local h, s, v = ColorToHSV(clr)
	return HSVToColor(h, s + 0.2, v)
end

local tdui = wck.tdui

local icon_cancel = Material("icon16/cancel.png")
function ENT:DrawTranslucent()
	local distsq = self:GetPos():DistToSqr(LocalPlayer():EyePos())
	local maxdist = wck.VISITORCONTROL_MAXUSEDISTANCE_SQ
	if distsq > maxdist then return end

	local t = nettable.get("WCKQueue")
	local tdata = t.cinemas and t.cinemas[self:GetCinemaId()]

	local uiy = 40

	self.ui = self.ui or tdui.Create()

	self.ui:Rect(-100, 0, 200, 220, Color(0, 0, 0, 235), Color(255, 255, 255))
	self.ui:Text("Cinema Control", "DermaLarge", 0, 5)

	self.ui:Line(-100, 35, 100, 35)

	local CFG_X_START = -92
	local CFG_X_OFFSET = 95

	local cfg_x = CFG_X_START
	for k,v in ipairs(cfg) do
		local y = uiy
		--[[
		uiy = uiy+15
		self.ui:Text(v.text, "DermaDefault", -50, y)

		if self.ui:Button(v.value(self, tdata), "DermaDefault", 0, y, 90, 15) then
			net.Start("WCK_Configure")
			net.WriteString(self:GetCinemaId())
			net.WriteString(v.id)
			net.SendToServer()
		end]]
		local clr = v.value(self, tdata) and clr_yes or clr_no
		if self.ui:Button(v.text, "DermaDefault", cfg_x, y, 90, 15, clr, darker(clr)) then
			net.Start("WCK_Configure")
			net.WriteString(self:GetCinemaId())
			net.WriteString(v.id)
			net.SendToServer()
		end

		cfg_x = cfg_x + CFG_X_OFFSET
		if cfg_x >= (CFG_X_START+CFG_X_OFFSET*2) then
			cfg_x = CFG_X_START
			uiy = uiy + 20
		end
	end

	self.ui:Line(-100, uiy, 100, uiy)
	uiy = uiy+5

	if self.ui:Button("Add to queue", "DermaDefault", -95, uiy, 75, 15) then
		wck.OpenVideoRequester("https://www.youtube.com/watch?v=9SAxySPfOro#rand=" .. CurTime(), function(url)
			net.Start("WCK_Add")
			net.WriteString(self:GetCinemaId())
			net.WriteString(url)
			net.SendToServer()
		end)
	end
	if self.ui:Button("Schedule", "DermaDefault", -15, uiy, 60, 15) then
		self:OpenScheduleScreen()
	end
	if self.ui:Button("Skip", "DermaDefault", 50, uiy, 45, 15) then
		net.Start("WCK_Skip")
		net.WriteString(self:GetCinemaId())
		net.SendToServer()
	end

	uiy = uiy+20

	local q = tdata and tdata.queue or {}
	for k,v in pairs(q) do
		local y = uiy
		uiy = uiy + 17
		self.ui:Rect(-90, y, 180, 15)

		local TEXT_BOX_X = -70
		local TEXT_BOX_WIDTH = 155
		local PADDING = 20

		local title = v.title or "-unknown-"

		surface.SetFont("DermaDefault")
		local tw = surface.GetTextSize(title)
		local overflows = tw > TEXT_BOX_WIDTH

		local tx = 0

		if overflows then
			self.ui:EnableRectStencil(TEXT_BOX_X, y+1, TEXT_BOX_WIDTH, 15)
			tx = -PADDING + (CurTime() * 45) % (tw-TEXT_BOX_WIDTH + PADDING*2)
		end
		self.ui:Text(title, "DermaDefault", TEXT_BOX_X - tx, y + 1, nil, TEXT_ALIGN_LEFT)
		if overflows then
			self.ui:DisableStencil()
		end

		self.ui:Mat(icon_cancel, -90 + 3, y + 3, 9, 9)
		if self.ui:Button("", "DermaDefault", -90, y, 15, 15) then
			net.Start("WCK_Remove")
			net.WriteString(self:GetCinemaId())
			net.WriteUInt(k, 16)
			net.SendToServer()
		end
	end

	self.ui:Cursor()
	self.ui:Render(self:LocalToWorld(Vector(3.2, -0.5, 12)), self:LocalToWorldAngles(Angle(0, 180, 0)), 0.11)
end

function ENT:OpenScheduleScreen()
	local frame = vgui.Create("DFrame")
	frame:SetTitle("WCK Video Scheduler")
	frame:SetSkin("WyoziCK")
	frame:SetSize(300, 100)
	frame:Center()

	local day_combo = frame:Add("DComboBox")
	day_combo:SetPos(10, 30)
	day_combo:SetSize(150, 25)
	day_combo:SetValue("Select date")

	local cur_time = os.time() + 120 -- add two minute buffer to defaults

	local function AddChoice(prefix, dayOffset, select)
		local t = cur_time + (dayOffset * 60 * 60 * 24)
		local d, m, y = os.date("%d", t), os.date("%m", t), os.date("%Y", t)

		local dmy = string.format("%s.%s.%s", d, m, y)
		day_combo:AddChoice(prefix and string.format("%s (%s)", prefix, dmy) or dmy, {day = tonumber(d), month = tonumber(m), year = tonumber(y)}, select)
	end

	-- TODO Fix order
	AddChoice("Today", 0, true)
	AddChoice("Tomorrow", 1)
	for i=1,5 do
		AddChoice(nil, 1 + i)
	end

	local lbl = frame:Add("DLabel")
	lbl:SetText("at")
	lbl:SetPos(170, 35)
	lbl:SetTextColor(Color(0, 0, 0))
	lbl:SizeToContents()

	local hourcb = frame:Add("DNumberWang")
	hourcb:SetMinMax(0, 23)
	hourcb:SetPos(190, 30)
	hourcb:SetSize(40, 25)
	hourcb:SetValue(tonumber(os.date("%H", cur_time)))

	local lbl = frame:Add("DLabel")
	lbl:SetText(":")
	lbl:SetPos(235, 35)
	lbl:SetTextColor(Color(0, 0, 0))
	lbl:SizeToContents()

	local mincb = frame:Add("DNumberWang")
	mincb:SetMinMax(0, 59)
	mincb:SetDecimals(0)
	mincb:SetPos(243, 30)
	mincb:SetSize(40, 25)
	mincb:SetValue(tonumber(os.date("%M", cur_time)))

	local btn = frame:Add("DButton")
	btn:SetPos(10, 65)
	btn:SetSize(280, 25)
	btn:SetText("Choose Video")
	btn.DoClick = function()
		local dateObj = select(2, day_combo:GetSelected())
		local timeObj = {hour = hourcb:GetValue(), min = mincb:GetValue(), sec = 0}

		local datetime = {}
		table.Merge(datetime, dateObj)
		table.Merge(datetime, timeObj)

		local targetTime = os.time(datetime)
		local remaining = targetTime - os.time()
		if remaining > 0 then
			wck.OpenVideoRequester("", function(url)
				remaining = targetTime - os.time() -- update

				RunConsoleCommand("wck_schedule", url, remaining, self:GetCinemaId())

				if IsValid(frame) then frame:Close() end
			end)
		else
			LocalPlayer():ChatPrint("That datetime already happened!")
		end
	end

	frame:MakePopup()
end

function ENT:CanEditVariables(ply)
	return ply:IsAdmin()
end
