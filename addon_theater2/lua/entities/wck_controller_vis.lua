AddCSLuaFile()

ENT.PrintName		= "Visitor Controller"
ENT.Author			= "Wyozi"
ENT.Category		= "Wyozi Cinema Kit"

ENT.Editable		= true
ENT.Spawnable		= true
ENT.AdminOnly		= true

ENT.Type = "anim"
ENT.RenderGroup = RENDERGROUP_BOTH
ENT.Model = "models/props/de_nuke/NuclearControlBox.mdl"

function ENT:SetupDataTables()
	self:NetworkVar("String", 0, "CinemaId", { KeyName = "cinemaid", Edit = { type = "String", category = "Cinema", order = 1 } })

	if SERVER then
		self:SetCinemaId("main")
	end
end

function ENT:Initialize()
	if SERVER then
		self:SetModel(self.Model)
		self:PhysicsInit(SOLID_VPHYSICS)

		self:SetColor(Color(0, 0, 0))

		self:SetCinemaId("main")
	end
end

function ENT:CPPICanPickup()
	return false
end

local function OpenWCKConfig()
	local frame = vgui.Create("DFrame")
	frame:SetTitle("WCK Settings")
	frame:SetSkin("WyoziCK")
	frame:SetSize(300, 150)
	frame:Center()

	local form = vgui.Create("DForm", frame)
	form:Dock(FILL)
	form:SetName("Miscellaneous")

	form:NumSlider("Volume", "wck_volume", 0, 1, 1)

	frame:MakePopup()
end

local tdui = wck.tdui
local icon_cog = Material("icon16/cog.png", "smooth")
function ENT:DrawTranslucent()
	local distsq = self:GetPos():DistToSqr(LocalPlayer():EyePos())
	local maxdist = wck.VISITORCONTROL_MAXUSEDISTANCE_SQ
	if distsq > maxdist then return end

	local fadeMaxDist = maxdist * 0.5
	local visFrac = math.Remap(distsq, fadeMaxDist, maxdist, 1, 0)

	render.SetBlend(visFrac)
	surface.SetAlphaMultiplier(visFrac)

	local t = nettable.get("WCKQueue")
	local tdata = t.cinemas and t.cinemas[self:GetCinemaId()]

	local uiy = 40

	self.ui = self.ui or tdui.Create()

	self.ui:Rect(-100, 0, 200, 220, Color(0, 0, 0, 235), Color(255, 255, 255))
	self.ui:Text("Cinema Control", "DermaLarge", 0, 5)
	self.ui:Text("for visitors", "DermaDefaultBold", 85, 2, Color(255, 127, 0), TEXT_ALIGN_RIGHT)

	self.ui:Line(-100, 35, 100, 35)

	if (not tdata or not tdata.visQueueDisallowed) and self.ui:Button("Queue", "DermaDefault", -90, uiy, 70, 15) then
		wck.OpenVideoRequester("https://www.youtube.com/watch?v=9SAxySPfOro#rand=" .. CurTime(), function(url)
			net.Start("WCK_VisitorAdd")
			net.WriteEntity(self)
			net.WriteString(self:GetCinemaId())
			net.WriteString(url)
			net.SendToServer()
		end)
	end

	local count = self:GetNWInt("wrskipc", 0)
	if (not tdata or not tdata.voteSkipDisallowed) and self.ui:Button(string.format("Vote skip (%d/%d)", ((tdata and tdata.cur and tdata.cur.skippers and #tdata.cur.skippers) or 0), count), "DermaDefault", -15, uiy, 85, 15) then
		net.Start("WCK_VisitorSkip")
		net.WriteEntity(self)
		net.WriteString(self:GetCinemaId())
		net.SendToServer()
	end

	self.ui:Mat(icon_cog, 78, uiy + 3, 9, 9)
	if self.ui:Button("", "DermaDefault", 75, uiy, 15, 15) then
		OpenWCKConfig()
	end

	uiy = uiy+20

	local q = tdata and tdata.queue or {}
	for k,v in pairs(q) do
		local y = uiy
		uiy = uiy + 17
		self.ui:Rect(-90, y, 180, 15)

		local TEXT_BOX_X = -85
		local TEXT_BOX_WIDTH = 170
		local PADDING = 20

		local title = v.title or "-unknown-"

		surface.SetFont("DermaDefault")
		local tw = surface.GetTextSize(title)
		local overflows = tw > TEXT_BOX_WIDTH

		local tx = 0

		if overflows then
			self.ui:EnableRectStencil(TEXT_BOX_X, y+1, TEXT_BOX_WIDTH, 15)
			tx = -PADDING + (CurTime() * 45) % (tw-TEXT_BOX_WIDTH + PADDING*2)
		end
		self.ui:Text(title, "DermaDefault", TEXT_BOX_X - tx, y + 1, nil, TEXT_ALIGN_LEFT)
		if overflows then
			self.ui:DisableStencil()
		end
	end

	self.ui:Cursor()

	self.ui:Render(self:LocalToWorld(Vector(3.2, -0.5, 12)), self:LocalToWorldAngles(Angle(0, 180, 0)), 0.11)

	render.SetBlend(1)
	surface.SetAlphaMultiplier(1)
end

function ENT:UpdateSkipPlayerAmount(total)
	self:SetNWInt("wrskipc", total)
end

function ENT:CanEditVariables(ply)
	return ply:IsAdmin()
end
