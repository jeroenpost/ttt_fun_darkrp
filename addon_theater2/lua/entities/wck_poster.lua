AddCSLuaFile()

ENT.PrintName		= "Poster"
ENT.Author			= "Wyozi"
ENT.Category		= "Wyozi Cinema Kit"

ENT.Editable		= true
ENT.Spawnable		= true
ENT.AdminOnly		= true

ENT.Type = "anim"
ENT.RenderGroup = RENDERGROUP_BOTH
ENT.Model = "models/props/cs_office/offpaintingb.mdl"

function ENT:SetupDataTables()
	self:NetworkVar("String", 0, "ImageUrl", { KeyName = "imageurl", Edit = { type = "String", category = "Cinema", order = 1 } })
end

function ENT:Initialize()
	if SERVER then
		self:SetModel(self.Model)
		self:PhysicsInit(SOLID_VPHYSICS)
	end
end

if CLIENT then
	function ENT:Think()
		if self.ImgRequest then
			local url = self:GetImageUrl()
			local qurl = self.QueryingUrl
			if url ~= qurl then
				self.QueryingUrl = url

				wck.htmlmat.Queue(url, function(data)
					if data.mat then
						self.ImgMat = data.mat
					end
					if data.err then print("poster fetch fail: ", data.err) end
				end)
			end
		end
	end
	function ENT:Draw()
		self.ImgRequest = true

		if self.ImgMat then render.MaterialOverrideByIndex(1, self.ImgMat) end
		self:DrawModel()
	end
end
