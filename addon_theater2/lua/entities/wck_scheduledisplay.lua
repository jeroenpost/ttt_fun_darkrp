AddCSLuaFile()

ENT.PrintName		= "Schedule Display"
ENT.Author			= "Wyozi"
ENT.Category		= "Wyozi Cinema Kit"

ENT.Editable		= true
ENT.Spawnable		= true
ENT.AdminOnly		= true

ENT.Type = "anim"
ENT.RenderGroup = RENDERGROUP_BOTH
ENT.Model = "models/props/cs_office/TV_plasma.mdl"

function ENT:SetupDataTables()
	self:NetworkVar("String", 0, "CinemaId", { KeyName = "cinemaid", Edit = { type = "String", category = "Cinema", order = 1 } })

	if SERVER then
		self:SetCinemaId("main")
	end
end

function ENT:Initialize()
	if SERVER then
		self:SetModel(self.Model)
		self:PhysicsInit(SOLID_VPHYSICS)

		self:SetCinemaId("main")
	end
end

function ENT:CPPICanPickup()
	return false
end

local tdui = wck.tdui

local sched_data = nettable.get("WCKSchedule")

local clr_yes = Color(135, 211, 124)
local clr_no = Color(210, 77, 87)

local function ToNiceDate(time)
	if system.GetCountry() == "US" then
		return os.date("%m/%d", time)
	end
	return os.date("%d.%m.", time)
end

local gray = Color(190, 190, 190)
function ENT:DrawTranslucent()
	self.ui = self.ui or tdui.Create()

	self.ui:Rect(-250, 0, 250*2, 295, Color(0, 0, 0, 235), Color(255, 255, 255))
	self.ui:Text("Cinema Schedule Screen", "DermaLarge", 0, 5)
	self.ui:Line(-250, 40, 250, 40)

	self.ui:Rect(130, 265, 120, 30)
	self.ui:Text("Time: " .. os.date("%H:%M"), "Trebuchet24", 240, 267, nil, TEXT_ALIGN_RIGHT)

	self.ui:Text("title", "Trebuchet24", -105, 50, gray)
	self.ui:Text("time", "Trebuchet24", 65, 50, gray)
	self.ui:Text("theater", "Trebuchet24", 175, 50, gray)

	local sched = table.Copy(sched_data)
	table.SortByMember(sched, "time", true)

	if #sched == 0 then
		sched = { {title = "No scheduled events"} }
	end

	for k,v in pairs(sched) do
		local y = 45 + k*35
		self.ui:Line(-220, y, 220, y, tdui.COLOR_ORANGE)

		local title = v.title

		self.ui:EnableRectStencil(-210, y, 208, 30)
		self.ui:Text(title, "Trebuchet24", -210, y + 5, nil, TEXT_ALIGN_LEFT)
		self.ui:DisableStencil()

		if v.time then
			self.ui:Line(0, y + 5, 0, y + 30, tdui.COLOR_ORANGE)

			local diff = v.time - os.time()

			local txt, clr
			if diff < (60*5) then
				txt, clr = "soon!", HSVToColor((CurTime()*60) % 360, 0.5, 0.9)
			elseif os.date("%d.%m.%y", v.time) == os.date("%d.%m.%y") then
				txt, clr = "today", clr_yes
			else
				txt, clr = ToNiceDate(v.time), clr_no
			end
			self.ui:Text(txt, "Trebuchet24", 35, y + 5, clr)

			self.ui:Text(os.date("%H:%M", v.time), "Trebuchet24", 70, y + 5, nil, TEXT_ALIGN_LEFT)
		end

		if v.theater then
			self.ui:Line(128, y + 5, 128, y + 30, tdui.COLOR_ORANGE)
			self.ui:Text(v.theater, "Trebuchet24", 140, y + 5, nil, TEXT_ALIGN_LEFT)
		end
	end

	self.ui:Render(self:LocalToWorld(Vector(6, 0, 35.2)), self:LocalToWorldAngles(Angle(0, 180, 0)), 0.11)
end
