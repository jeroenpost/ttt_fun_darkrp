ENT.Type = "anim"

ENT.PrintName		= "Projector"
ENT.Author			= "Wyozi"
ENT.Category		= "Wyozi Cinema Kit"

ENT.Editable		= true
ENT.Spawnable		= true
ENT.AdminOnly		= true

function ENT:SetupDataTables()
	self:NetworkVar("String", 0, "CinemaId", { KeyName = "cinemaid", Edit = { type = "String", category = "Cinema", order = 1 } })
	self:NetworkVar("String", 1, "PlaceholderVideo", { KeyName = "placeholdervid", Edit = { type = "String", category = "Cinema", order = 2 } })
	self:NetworkVar("Float", 0, "ProjectorFOV", { KeyName = "projectorfov", Edit = { type = "Float", min = 30, max = 170, category = "Cinema", order = 3 } })
	self:NetworkVar("Float", 1, "ProjectorBrightness", { KeyName = "projectorbrightness", Edit = { type = "Float", min = 1, max = 50, category = "Cinema", order = 4 } })

	self:NetworkVar("Int", 0, "ProjType", { KeyName = "projectionType", Edit = { type = "Combo", values = {["Normal"] = 0, ["3D2D"] = 1, ["3D2D on props"] = 2}, category = "Cinema", order = 5 } })

	if SERVER then
		self:SetCinemaId("main")
		self:SetPlaceholderVideo("")
		self:SetProjectorFOV(60)
		self:SetProjectorBrightness(10)
		self:SetProjType(0)
	end
end

function ENT:GetUse3D2D() return self:GetProjType() > 0 end
function ENT:SetUse3D2D(b) self:SetProjType(b and 1 or 0) end

ENT.Model = "models/props/cs_office/projector.mdl"
ENT.LightOrigin = Vector(1, -5, 4.3)
-- Returns the forward vector. Might need changing if you change the model
function ENT:GetFwdVector()
	local vec = self:GetRight()
	vec.z = vec.z + 0.1
	vec:Normalize()
	return vec
end

function ENT:GetTheaterData()
	local t = nettable.get("WCKQueue")
	return t.cinemas and t.cinemas[self:GetCinemaId()]
end

function ENT:CPPICanPickup()
	return false
end

function ENT:CanEditVariables(ply)
	return ply:IsAdmin()
end
