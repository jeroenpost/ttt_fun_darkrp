include("shared.lua")

ENT.RenderGroup = RENDERGROUP_BOTH

function ENT:Initialize()
	self.PixVis = util.GetPixelVisibleHandle()

	self.RTTexName = "WCKProjector" .. self:EntIndex()
end

local matLight = Material( "sprites/light_ignorez" )
local matBeam = Material( "effects/lamp_beam" )

function ENT:Draw()
	self:DrawModel()
	self:DrawProjector()
end

ENT.ProjParamCache = {
	{ get = function(self) return self:GetProjectorFOV() end, apply = function(self, val) self.projTex:SetHorizontalFOV(val) self.projTex:SetVerticalFOV(val) end},
	{ get = function(self) return self:GetProjectorBrightness() end, apply = function(self, val) self.projTex:SetBrightness(val) end},
	{ get = function(self) return self:LocalToWorld(self.LightOrigin) end, apply = function(self, val) self.projTex:SetPos(val) end},
	{ get = function(self) return self:GetFwdVector():Angle() end, apply = function(self, val) self.projTex:SetAngles(val) end},
}

function ENT:UpdateProjector(force)
	local updated = not not force

	local cacheVals = self.ProjParamCacheVals
	if not cacheVals then
		cacheVals = {}
		self.ProjParamCacheVals = cacheVals
	end

	for i=1, #self.ProjParamCache do
		local cobj = self.ProjParamCache[i]
		local fetchedVal = cobj.get(self)

		if force or cacheVals[i] ~= fetchedVal then
			cobj.apply(self, fetchedVal)
			cacheVals[i] = fetchedVal
			updated = true
		end
	end

	if updated then self.projTex:Update() end
end

function ENT:DrawTranslucent()
	local LightOrigin = self:LocalToWorld(self.LightOrigin)
	local LightNrm = self:GetFwdVector()
	local ViewNormal = LightOrigin - EyePos()
	local Distance = ViewNormal:Length()
	ViewNormal:Normalize()
	local ViewDot = ViewNormal:Dot( LightNrm * -1 )
	local LightPos = LightOrigin + LightNrm * 5

	-- glow sprite
	render.SetMaterial( matBeam )

	render.StartBeam(2)
		render.AddBeam( LightPos + LightNrm * 1, 32, 0, Color( 255, 255, 255, 70) )
		render.AddBeam( LightPos + LightNrm * 80, 32, 1, Color( 255, 255, 255, 0) )
	render.EndBeam()

	if ViewDot >= 0 then
		render.SetMaterial(matLight)
		local visibility = util.PixelVisible(LightPos, 16, self.PixVis)

		if not visibility then return end

		local Size = math.Clamp(Distance * visibility * ViewDot * 1, 32, 256)

		Distance = math.Clamp( Distance, 32, 800 )
		local Alpha = math.Clamp( (1000 - Distance) * visibility * ViewDot, 0, 100 )
		local Col = self:GetColor()
		Col.a = Alpha

		render.DrawSprite( LightPos, Size, Size, Col, visibility * ViewDot )
		render.DrawSprite( LightPos, Size*0.4, Size*0.4, Color(255, 255, 255, Alpha), visibility * ViewDot )
	end
end


local cvar_hdrFix = CreateConVar("wck_hdrfix", "0")
local nilToneMappingScale = Vector(1, 1, 1)
function ENT:DrawProjector()
	if self:GetUse3D2D() then
		if IsValid(self.projTex) then
			self.projTex:Remove()
		end

		local hdrFix = cvar_hdrFix:GetBool()

		local oldTMSL
		if hdrFix then
			oldTMSL = render.GetToneMappingScaleLinear()
			-- hacky fix for hdr problem. Set tonemappingscale to something that should be noop
			if oldTMSL.r < 1 then
				oldTMSL = nil
			else
				render.SetToneMappingScaleLinear(nilToneMappingScale)
			end
		end
		self:DrawTDUIScreen()
		if hdrFix and oldTMSL then
			render.SetToneMappingScaleLinear(oldTMSL)
		end
	else
		if not IsValid(self.projTex) then
			self.projTex = ProjectedTexture()
			self.projTex:SetEnableShadows(false)
			self.projTex:SetNearZ(12)
			self.projTex:SetFarZ(1024)
			self.projTex:SetColor(Color(255, 255, 255))
			self.projTex:SetTexture("WCKProjector" .. self:EntIndex())

			self:UpdateProjector(true)
		else
			self:UpdateProjector()
		end
	end
end

local cvar_vol = CreateConVar("wck_volume", "1", FCVAR_ARCHIVE)
hook.Add("F4MenuTabs", "WCK_DarkRPTab", function()
	local form = vgui.Create("DForm", dsettings)
	form:SetName("Wyozi Cinema Kit Settings")

	form:NumSlider("Media playback volume", "wck_volume", 0, 1, 1)

	DarkRP.addF4MenuTab("Cinema", form)
end)

function ENT:GetPlayingUrl()
	local tdata = self:GetTheaterData()
	if tdata and tdata.cur then
		return tdata.cur.url
	end
	local placeholder = self:GetPlaceholderVideo()
	if placeholder and placeholder ~= "" then
		return placeholder
	end
end

local log_wck = EzLogger.new("WyoziCK")
function ENT:Think()
	local tdata = self:GetTheaterData()
	local shouldPlay = self:IsWithinPlayRange()
	local shouldPlayUrl = self:GetPlayingUrl()

	if IsValid(self.Clip) and (not shouldPlay or not shouldPlayUrl) then
		self.Clip:stop()
		self.Clip = nil
	elseif shouldPlay and shouldPlayUrl and (not IsValid(self.Clip) or self.Clip:getUrl() ~= shouldPlayUrl) then
		if IsValid(self.Clip) then
			self.Clip:stop()
			self.Clip = nil
		end

		local link = shouldPlayUrl
		local elapsed = CurTime() - ((tdata and tdata.cur and tdata.cur.startTime) or CurTime())

		local service = wck.medialib.load("media").guessService(link)
		local mediaclip = service:load(link)

		mediaclip:seek(elapsed)
		mediaclip:play()
		self.Clip = mediaclip

		local requester = tdata and tdata.cur and tdata.cur.requester
		if requester then
			local reqstr = string.format("%s (%s)", IsValid(requester) and requester:Nick() or "unknown", IsValid(requester) and requester:SteamID() or "")
			log_wck:i("Playing ", EzLogger.wrap(EzLogger.Color.FountainBlue, tdata.cur.title or "unknown"), " requested by ", reqstr)
		end

		self.SetVolume = nil
	end

	local targetVol = cvar_vol:GetFloat() or 1
	local setVol = self.SetVolume or 1
	if IsValid(self.Clip) and targetVol ~= setVol then
		self.Clip:setVolume(targetVol)
		self.SetVolume = targetVol
	end
end

function ENT:OnRemove()
	if IsValid(self.Clip) then self.Clip:stop() end
	if IsValid(self.projTex) then self.projTex:Remove()	end
end

function ENT:IsWithinPlayRange()
	return not self:IsDormant()
end

surface.CreateFont("WCK_ScreenFont", {
	font = "Roboto",
	size = 50
})
surface.CreateFont("WCK_ScreenFontSmall", {
	font = "Roboto",
	size = 16
})

local noice_icons = {
	Material("icon16/contrast.png"),
	Material("icon16/application_osx_terminal.png"),
	Material("icon16/box.png"),
	Material("icon16/brick.png"),
	Material("icon16/car.png"),
	Material("icon16/eye.png"),
	Material("icon16/drink.png"),
	Material("icon16/sport_soccer.png"),
	Material("icon16/cart.png"),
	Material("icon16/sport_football.png"), -- if the world made any sense this would be "handegg.png"
	Material("icon16/wand.png"),
	Material("icon16/weather_rain.png"),
	Material("icon16/weather_sun.png"),
	Material("icon16/world.png"),
	Material("flags16/fi.png"),
	Material("icon16/ipod.png"),
	nil -- cuz Material returns 2 values WTFFFFFFFFFFFFFFFFFFFFF
}

local inst = tonumber("76561198080010880") or "local"

local idle_screens = {}

local s_clr,s_rect,s_trect,s_poly = surface.SetDrawColor, surface.DrawRect, surface.DrawTexturedRect, surface.DrawPoly

-- summer
local c_sun, c_beach, c_ocean = Color(248, 148, 6), Color(249, 191, 59), Color(31, 58, 147)
local m_sun = Material("sprites/light_ignorez")
local p_ocean = {}; for i=1, 16 do p_ocean[i] = {x = 0, y = 0} end
table.insert(idle_screens, {
	test = function()
		local d, m = tonumber(os.date("%d")), tonumber(os.date("%m"))
		return m >= 6 and m <= 8
	end,
	draw = function(x, y, w, h)
		-- sky
		local skyStrength = 1 - math.abs(12 - tonumber(os.date("%H"))) / 12
		s_clr(HSVToColor(204, 0.8, 0.15 + 0.7 * skyStrength))
		s_rect(x, y, w, h/2)

		-- sun
		s_clr(c_sun)
		surface.SetMaterial(m_sun)
		local skScaled = (skyStrength ^ 2)
		local s = skScaled * 400
		local sYOff = (1 - skScaled) * h * 0.3
		s_trect(x + w*0.85 - s, y + h * 0.2 - s + sYOff, s*2, s*2)

		-- beach
		s_clr(c_beach)
		s_rect(x, y + h/2, w, h/2)

		-- ocean
		local oc_w = w * 0.7
		s_clr(c_ocean)
		draw.NoTexture()
		p_ocean[1].x = x; p_ocean[1].y = y + h/2; p_ocean[2].x = x + oc_w; p_ocean[2].y = y + h/2 -- upper edge
		p_ocean[16].x = x; p_ocean[16].y = y + h;
		for i=1,13 do
			local seg = p_ocean[2 + i]
			seg.x = x + oc_w - (i ^ 2) * 1.5 + math.sin(CurTime() + i) * 20
			seg.y = y + h/2 + (i / 13 * h/2)
		end
		s_poly(p_ocean)
	end
})

-- normal
table.insert(idle_screens, {
	test = function() return true end,
	draw = function(x, y, w, h)
		local clr = HSVToColor(((CurTime()*10)%360), 0.5, 0.3)
		surface.SetDrawColor(clr.r, clr.g, clr.b, 200)
		surface.DrawRect(x, y, w, h)

		for i,icon in pairs(noice_icons) do
			local seed = i

			local tick = (CurTime() + seed*40)

			local speed = seed % 4 + 1 + (seed * 0.05)
			local loltick = (CurTime() * 2 + tick * speed * 20) % w

			surface.SetDrawColor(255, 255, 255)
			surface.SetMaterial(icon)

			local iw, ih = 16, 16
			if icon:GetName():match("^flags16") then iw, ih = 20, 12 end
			surface.DrawTexturedRect(loltick, y + (h*3/4) + math.sin(loltick / 30) * 30, iw, ih)
		end
	end
})

local idlescreen
for _,t in pairs(idle_screens) do
	if t.test() then
		idlescreen = t.draw
		break
	end
end

local function DrawIdleScreen(x, y, w, h)
	idlescreen(x, y, w, h)

	surface.SetDrawColor(100, 100, 100)
	surface.DrawRect(x + 50, y + h/2 - 50, w-100, 100)

	draw.SimpleText("No video playing", "WCK_ScreenFont", w/2, y + h/2, Color(0, 0, 0), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)

	draw.SimpleText(string.format("WCK [ %s ]", inst), "WCK_ScreenFontSmall", w/2, y + h - 15, Color(200, 200, 200, 100), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
end

local function formatSeconds(sec)
	local hours = math.floor(sec / (60 * 60))
	sec = sec % (60 * 60)

	local mins = math.floor(sec / 60)
	sec = sec % 60

	local secs = sec

	if hours > 0 then
		return string.format("%02d:%02d:%02d", hours, mins, secs)
	else
		return string.format("%02d:%02d", mins, secs)
	end
end

local scr_aspectRatio = 9 / 16

local rt_w, rt_h = 768, 768
local rt_draw_h = rt_w * scr_aspectRatio
local rt_draw_yoffset = (rt_h-rt_draw_h)/2

local TEXFLAG_ANISOTROPIC = 0x0010
local TEXFLAG_NODEPTH =  0x800000

local PROJRT_TEXFLAGS = bit.bor(TEXFLAG_ANISOTROPIC, TEXFLAG_NODEPTH)

function ENT:RenderToVideoRT()
	if not self:IsWithinPlayRange() then return end

	local rt = GetRenderTargetEx(
		self.RTTexName,
		rt_w, rt_h,
		RT_SIZE_NO_CHANGE,
		MATERIAL_RT_DEPTH_NONE,
		PROJRT_TEXFLAGS,
		CREATERENDERTARGETFLAGS_UNFILTERABLE_OK,
		IMAGE_FORMAT_BGR888)

	render.PushRenderTarget(rt)
	render.Clear(0, 0, 0, 255, false, false)
	cam.Start2D()

	self:DrawVideo(0, rt_draw_yoffset, rt_w, rt_draw_h)

	cam.End2D()
	render.PopRenderTarget()
end

function ENT:DrawVideo(x, y, w, h)
	if IsValid(self.Clip) then
		self.Clip:draw(x, y, w, h)

		local meta = self.Clip:lookupMetadata()
		if meta then
			local elapsed = self.Clip:getTime()
			local total = meta.duration or 0
			draw.SimpleText(string.format("%s [ %s / %s ]", meta.title or "", formatSeconds(elapsed), formatSeconds(total)), "WCK_ScreenFontSmall", x + w/2, y + h + 8, Color(200, 200, 200, 100), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		end
	else
		DrawIdleScreen(x, y, w, h)
	end
end

local TDUI_WIDTH = 1000
local TDUI_HEIGHT = 1000

local tdui_draw_h = TDUI_WIDTH * scr_aspectRatio
local tdui_draw_yoffset = (TDUI_HEIGHT-tdui_draw_h)/2

local self_ent
local tdui_tr_filter = function(e)
	return e ~= self_ent and not e:IsPlayer()
end

function ENT:DrawTDUIScreen()
	local midpos = self:LocalToWorld(self:OBBCenter())

	local projType = self:GetProjType()

	local vecdir = self:GetFwdVector()
	local trOpts = {
		start = midpos,
		endpos = midpos + vecdir * 10000,
		mask = MASK_SOLID_BRUSHONLY,
		filter = self
	}

	if projType == 2 then
		trOpts.mask = MASK_SOLID
		self_ent = self
		trOpts.filter = tdui_tr_filter
	end

	local tr = util.TraceLine(trOpts)

	local pos = tr.HitPos + tr.HitNormal*5 -- mul by normal to kill z-fights
	local ang = tr.HitNormal:Angle()
	local fov_scale = self:GetProjectorFOV() / 55
	local draw_scale = math.Clamp(tr.StartPos:Distance(tr.HitPos) / 1000 * fov_scale, 0, 10)

	-- Rotate the screen to be in correct angle (ie. facing towards source)
	ang:RotateAroundAxis(ang:Right(), -90)
	ang:RotateAroundAxis(ang:Up(), 90)

	local s_width = TDUI_WIDTH * draw_scale
	local s_height = TDUI_HEIGHT * draw_scale * 1.0

	pos = pos - ang:Right() * s_height * 0.5
	pos = pos - ang:Forward() * s_width * 0.5


	local b = pos
	local a = pos + ang:Right() * s_height + ang:Forward() * s_width

	--debugoverlay.Sphere(a, 8, 0.1); debugoverlay.Sphere(b, 8, 0.1)
	self:SetRenderBoundsWS(a, b)

	--local mins, maxs = Vector(-128, -64, -5), Vector(128, 64, 5)
	--render.DrawWireframeBox(pos, ang, mins, maxs, Color(255, 255, 255), true)
	--debugoverlay.Line(midpos, tr.HitPos, 0.1, Color(255, 0, 0))
	--debugoverlay.Line(midpos, pos, 0.1, Color(0, 0, 255))

	cam.Start3D2D(pos, ang, draw_scale)
		self:DrawVideo(0, tdui_draw_yoffset, TDUI_WIDTH, tdui_draw_h)
	cam.End3D2D()
end

hook.Add("HUDPaint", "WCKProjectorImage", function()
	for _,e in pairs(ents.FindByClass("wck_projector")) do
		if not e:GetUse3D2D() then
			e:RenderToVideoRT()
		end
	end
end)
