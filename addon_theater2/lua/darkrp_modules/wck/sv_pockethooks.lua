local ents = {
	["wck_projector"] = true,
	["wck_controller"] = true,
	["wck_controller_vis"] = true,
	["wck_poster"] = true
}

hook.Add("canPocket", "WCK_PreventPocketing", function(ply, ent)
	if ents[ent:GetClass()] then return false, "You can't pocket that, silly" end
end)
