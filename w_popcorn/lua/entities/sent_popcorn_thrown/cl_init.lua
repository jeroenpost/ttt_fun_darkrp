include("shared.lua")

local function kernel_init(particle, vel)
	particle:SetColor(255,255,255,255)
	particle:SetVelocity( vel or VectorRand():GetNormalized() * 15)
	particle:SetGravity( Vector(0,0,-200) )
	particle:SetLifeTime(0)
	particle:SetDieTime(math.Rand(5,10))
	particle:SetStartSize(2)
	particle:SetEndSize(0)
	particle:SetStartAlpha(255)
	particle:SetEndAlpha(255)
	particle:SetCollide(true)
	particle:SetBounce(0.25)
	particle:SetRoll(math.pi*math.Rand(0,1))
	particle:SetRollDelta(math.pi*math.Rand(-10,10))
end

function ENT:Initialize()
	
	self.emitter = ParticleEmitter(self:GetPos())
	
	if self and self:EntIndex() != 0 then
		local kt = "kernel_timer"..self:EntIndex()
		timer.Create(kt,0.01,0,function()
			if !self.emitter then
				timer.Stop(kt)
				return 
			end
			if math.Rand(0,1) < 0.33 then
				local particle = self.emitter:Add( "particle/popcorn-kernel", self:GetPos() + VectorRand():GetNormalized() * 4 )
				if particle then
					kernel_init(particle)
				end
			end
		end)
		timer.Simple(10, function ()
			timer.Destroy(kt)
			if self and self.emitter then
				self.emitter:Finish()
			end
		end)
	end

	net.Receive("Popcorn_Explosion",function () 

		if !self then return end
		local pos = net.ReadVector()
		local norm = net.ReadVector()
		local bucketvel = net.ReadVector()
		local entid = net.ReadFloat()
		
		timer.Destroy("kernel_timer"..entid)
		local emitter = self.emitter or ParticleEmitter(pos)
		for i = 1,150 do
            if emitter and isfunction(emitter.Add) then
                local particle = emitter:Add( "particle/popcorn-kernel", pos )
                if particle then
                    local dir = VectorRand():GetNormalized()
                    kernel_init(particle, ((-norm)+dir):GetNormalized() * math.Rand(0,200) + bucketvel*0.5 )

                end
            end
		end
        if ( self.Emitter ) then
            self.Emitter:Finish()
        end
	end)
end
