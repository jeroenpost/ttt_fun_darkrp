local meta = FindMetaTable("Player")

function meta:RXCar_PlayerCanBuyCar(CarVehicleName)
	local CarData = RXCar_Util_GetCarData(CarVehicleName)
	local CarVehicleData = RXCar_Util_GetVehicleData(CarVehicleName)
	
	if !CarData then 
		return false,"Error 1"
	end
	
	if !CarVehicleData then
		return false,"Error 2"
	end
	
	if D3DCar_Meta:GetMoney(self) < CarData.CarPrice then
		return false,"Not Enough Money"
	end
	
	for k,v in pairs(CarData.AvailableTeam or {}) do
		if !table.HasValue(CarData.AvailableTeam,self:Team()) then
			return false,"Job Limitation"
		end
	end

    if SERVER then
        if CarData.AvailableGroup and table.Count(CarData.AvailableGroup) > 0 then
            local canbuy = false
            local ranks = ""
            if istable(CarData.AvailableGroup ) then
                if gb.compare_rank(self, CarData.AvailableGroup) then
                    canbuy = true
                end
            end

            if not canbuy then
                self:PrintMessage( HUD_PRINTTALK, "You need to be VIP"  )
                self:Send3DShopNotice("You need to have the right VIP rank to buy this car")
                return  false,"You need to be VIP"
            end

            --if !table.HasValue(CarData.AvailableGroup,string.lower(self:GetNWString("usergroup"))) then
            --	return false,"ULX Rank Limitation"
            --end
        end
    end
	
	if CarVehicleName == "Airboat" then
		return false,"Blocked By Server"
	end
	
	
	return true
end

function meta:RXCar_PlayerCanSpawnCar(CarVehicleName,InvUID)
	local CarData = RXCar_Util_GetCarData(CarVehicleName)
	local CarVehicleData = RXCar_Util_GetVehicleData(CarVehicleName)
	
	if !CarData then 
		return false,"Error 1"
	end
	
	if !CarVehicleData then
		return false,"Error 2"
	end
	
	if CarVehicleName == "Airboat" then
		return false,"Blocked By Server"
	end
	
	for k,v in pairs(CarData.AvailableTeam or {}) do
		if !table.HasValue(CarData.AvailableTeam,self:Team()) then
			return false,"Job Limitation"
		end
    end

    if SERVER then
        if CarData.AvailableGroup and table.Count(CarData.AvailableGroup) > 0 then
            local canbuy = false
            local ranks = ""
            if istable(CarData.AvailableGroup ) then
                if gb.compare_rank(self, CarData.AvailableGroup) then
                    canbuy = true
                end
            end

            if not canbuy then
                self:PrintMessage( HUD_PRINTTALK, "You need to be VIP"  )
                self:Send3DShopNotice("You need to have the right VIP rank to spawn this car")
                return  false,"You need to be VIP"
            end

            --if !table.HasValue(CarData.AvailableGroup,string.lower(self:GetNWString("usergroup"))) then
            --	return false,"ULX Rank Limitation"
            --end
        end
    end

	
	return true
end