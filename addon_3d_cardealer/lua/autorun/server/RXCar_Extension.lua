hook.Add("PhysgunPickup", "RXCar Allow Vehicle Pickup", function(ply, ent)
	if ent and ent:IsValid() and ent.RX3DCar then
		return false
	end
end)


hook.Add("OnPlayerChangedTeam","3DCAR PlayerChangeJob",function(ply,PrevTeam,NewTeam)
	if D3DCarConfig.Check_Remove_JobOnlyCars then
		local UserSID = ply:SteamID()
		local Count = 0
		for k,v in pairs(RXCar_GetPlayer3DCars(ply)) do
			if v.CData.AvailableTeam then
				if !table.HasValue(v.CData.AvailableTeam,ply:Team()) then
					Count = Count + 1
					timer.Simple(0.1*Count,function()
						--RXCar_SaveCarIntoInventory(UserSID,v)
                        local car= v
                        car.RXCar_IgnoreRemoveHook = true
                        car:Remove()
					end)
					D3DCar_Meta:Notify(ply,"Your " .. v.CData.CarName .. " was removed and saved to your inventory because of job limitation")
				end
			end
		end
	end
end)
