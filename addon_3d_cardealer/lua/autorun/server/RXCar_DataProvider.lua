function RXCar_Provider_GetInventory(PlySID,CallBack)

    local sendTable = {
        ['key']     = gb_json.settings.key,
        ['action']  = "get_player_darkrp_cars",
        ['steam']   = tostring(PlySID),
    }
    http.Post( gb_json.settings.url, sendTable, function(data)
        local data = util.JSONToTable(data)
        CallBack(data)
    end);
end

function RXCar_Provider_SaveInventory(PlySID,InventoryData,CallBack)

	local Data = InventoryData or {}

    local sendTable = {
        ['key']     = gb_json.settings.key,
        ['action']  = "set_player_darkrp_cars",
        ['steam']   = tostring(PlySID),
        ['cardata'] = tostring( util.TableToJSON(Data) )
    }
    http.Post( gb_json.settings.url, sendTable, function(nulp)
        CallBack(Data)
    end);
end

