//=========== CONVAR
	local MaxCarSpeed = 1000

//=========== CONVAR END


local STR = TunerElement_CreateStruct("exhaust")
STR.PrintName = "Exhaust"
STR.Icon = "weather_clouds"
STR.Max = 2

STR:Editor_AddSpacer("Position")
local VAR = STR:AddVars("Pos_x","number")
VAR.Min = -300 VAR.Max = 300 

local VAR = STR:AddVars("Pos_y","number")
VAR.Min = -300 VAR.Max = 300 

local VAR = STR:AddVars("Pos_z","number")
VAR.Min = -300 VAR.Max = 300 VAR.StartValue = 10


STR:Editor_AddSpacer("Angle")
local VAR = STR:AddVars("Angle_p","number")
VAR.Min = 0 VAR.Max = 360 

local VAR = STR:AddVars("Angle_y","number")
VAR.Min = 0 VAR.Max = 360 

local VAR = STR:AddVars("Angle_r","number")
VAR.Min = 0 VAR.Max = 360 


local MAT_BEAM = Material("sprites/physgbeamb")


	function STR:Render(TuneSysEnt,Vars,ForceRender)
		if TuneSysEnt:GetClass() != "rm_car_tune_sys" then return end
		if !TuneSysEnt:GetParent() or !TuneSysEnt:GetParent():IsValid() then return end
		
		local Emitter = TuneSysEnt:GetEmitter()
		
		TuneSysEnt.ExhaustDelay = TuneSysEnt.ExhaustDelay or {}
		
		if (TuneSysEnt.ExhaustDelay[Vars["Pos_x"]] or 0) < CurTime() then
			TuneSysEnt.ExhaustDelay[Vars["Pos_x"]] = CurTime() + 0.02
			
			local SpeedPer = math.min(1,(TuneSysEnt:GetParent():GetVelocity():Length()/MaxCarSpeed))
		
			local EntAngle = TuneSysEnt:GetAngles()
			local Pos1 = TuneSysEnt:GetPos()
			Pos1 = Pos1 + EntAngle:Forward() * Vars["Pos_x"]
			Pos1 = Pos1 + EntAngle:Right() * Vars["Pos_y"]
			Pos1 = Pos1 + EntAngle:Up() * Vars["Pos_z"]
		
			local RC = math.random(200,255)
		
			local Smoke = Emitter:Add("particle/smokesprites_000" .. math.random(1,9), Pos1 )
				Smoke:SetVelocity( (TuneSysEnt:GetAngles() + Angle(Vars["Angle_p"],Vars["Angle_y"],Vars["Angle_r"])):Forward()*100 )
				Smoke:SetDieTime( math.Rand(0.7,1.2) )
				Smoke:SetStartAlpha( 10 )
				Smoke:SetEndAlpha( 30 )
				Smoke:SetAirResistance( 100 )
				Smoke:SetStartSize( 2 )
				Smoke:SetEndSize( SpeedPer*80 )				 		
				Smoke:SetColor( RC,RC,RC )
			
		end
	end
	
	function STR:RenderOnEditor(TuneSysEnt,Vars)
		render.SetMaterial( MAT_BEAM )
		
		local EntAngle = TuneSysEnt:GetAngles()
		
		local Pos1 = TuneSysEnt:GetPos()
		Pos1 = Pos1 + EntAngle:Forward() * Vars["Pos_x"]
		Pos1 = Pos1 + EntAngle:Right() * Vars["Pos_y"]
		Pos1 = Pos1 + EntAngle:Up() * Vars["Pos_z"]
		
		local AngleK = Angle(Vars["Angle_p"],Vars["Angle_y"],Vars["Angle_r"])
		
		AngleK = AngleK + TuneSysEnt:GetAngles()
		local Pos2 = Pos1 + AngleK:Forward() * 50
		
		render.DrawBeam( Pos1,Pos2, 1, 1, 1, Color(150,150,150,255) ) 
		render.DrawBeam( Pos2,Pos2 + AngleK:Right()*5 - AngleK:Forward()*5, 1, 1, 1, Color(150,150,150,255) ) 
		render.DrawBeam( Pos2,Pos2 - AngleK:Right()*5 - AngleK:Forward()*5, 1, 1, 1, Color(150,150,150,255) ) 
	end
	
	

TunerElement_Register(STR)