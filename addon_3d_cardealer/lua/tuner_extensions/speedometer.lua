local VAR_DEGREE = 250 -- Well designed. i recommend this.
local VAR_DENSITY = 20 -- Well designed. i recommend this.
local VAR_STARTANGLE = 90 -- Well designed. i recommend this.

local VAR_MAXSPEED = 2000 -- Max speed that speedometer can detect.

local VAR_DISPLAYSPEED_DIVIDE = 10 -- so. now maxspeed is 2000. and divide the speed with this value. so 2000 / 10 = 200.
									-- this isn't important. but player will think 2000km/h is weird speed. so divide 10 and display 200 km/h.

local STR = TunerElement_CreateStruct("speedometer")
STR.PrintName = "Speedometer"
STR.Icon = "clock"
STR.Max = 1

STR:Editor_AddSpacer("Scale")
local VAR = STR:AddVars("Scale","number")
VAR.Min = 0.01 VAR.Max = 0.2 VAR.Decimal = 2

STR:Editor_AddSpacer("Position")
local VAR = STR:AddVars("Pos_x","number")
VAR.Min = -300 VAR.Max = 300 VAR.Decimal = 1

local VAR = STR:AddVars("Pos_y","number")
VAR.Min = -300 VAR.Max = 300 VAR.Decimal = 1

local VAR = STR:AddVars("Pos_z","number")
VAR.Min = -300 VAR.Max = 300 VAR.Decimal = 1 VAR.StartValue = 10


STR:Editor_AddSpacer("Angle")
local VAR = STR:AddVars("Angle_p","number")
VAR.Min = 0 VAR.Max = 360 

local VAR = STR:AddVars("Angle_y","number")
VAR.Min = 0 VAR.Max = 360 

local VAR = STR:AddVars("Angle_r","number")
VAR.Min = 0 VAR.Max = 360 

STR:Editor_AddSpacer("CircleColor")
STR:AddVars("Color","color")

local MAT_BEAM = Material("sprites/physgbeamb")
local MAT_CIRCLE = Material("particle/Particle_Ring_Wave_Additive")


	function STR:Render(TuneSysEnt,Vars,ForceRender)
	
		local EntAngle = TuneSysEnt:GetAngles()
		local Pos1 = TuneSysEnt:GetPos()
		Pos1 = Pos1 + EntAngle:Forward() * Vars["Pos_x"]
		Pos1 = Pos1 + EntAngle:Right() * Vars["Pos_y"]
		Pos1 = Pos1 + EntAngle:Up() * Vars["Pos_z"]	
				
		local AngleK = (TuneSysEnt:GetAngles() + Angle(Vars["Angle_p"],Vars["Angle_y"],Vars["Angle_r"]))
				
		local RealSpeed = 0
		if TuneSysEnt:GetParent() and TuneSysEnt:GetParent():IsValid() then
			RealSpeed = TuneSysEnt:GetParent():GetVelocity():Length()
		end
		
		local SpeedPercent = RealSpeed
		SpeedPercent = SpeedPercent/VAR_MAXSPEED
		SpeedPercent = math.min(SpeedPercent,1)
		
		SpeedPercent = 1-SpeedPercent
				
		cam.Start3D2D( Pos1, AngleK, Vars["Scale"] )
			surface.SetDrawColor(0,0,0,255)
			surface.DrawRect(0,0,200,200)
			
			surface.SetMaterial(MAT_CIRCLE)
			surface.SetDrawColor(Vars["Color"].r,Vars["Color"].g,Vars["Color"].b,Vars["Color"].a)
			surface.DrawTexturedRect(0,0,200,200)
			
			surface.SetMaterial(MAT_BEAM)
			surface.SetDrawColor(255,255,255,255)
			
			for k = 1 , (VAR_DENSITY*2) do
				local Degree = VAR_STARTANGLE-(VAR_DEGREE-180)/2 + (k-1) * (VAR_DEGREE/(VAR_DENSITY*2))
				local Degree_rad = math.rad(Degree)
				
				local XP = 100
				local YP = 100
				
				if k%2 == 0 then
					XP = XP + math.sin(Degree_rad)*95
					YP = YP + math.cos(Degree_rad)*95
					surface.DrawTexturedRectRotated(XP,YP,3,5,Degree)
				else
					XP = XP + math.sin(Degree_rad)*93
					YP = YP + math.cos(Degree_rad)*93
					surface.DrawTexturedRectRotated(XP,YP,5,10,Degree)
					
					XP = XP - math.sin(Degree_rad)*15
					YP = YP - math.cos(Degree_rad)*15
				
					draw.SimpleText(math.Round((1-(k/(VAR_DENSITY*2)))*VAR_MAXSPEED/VAR_DISPLAYSPEED_DIVIDE), "RXCarFont_TrebLW_S15", XP,YP, Color(255,255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
				end
				
			end
			
			surface.SetDrawColor(0,0,0,255)
			surface.DrawRect(0,170,200,40)
			
			-- meter
			surface.SetMaterial(MAT_BEAM)
			surface.SetDrawColor(255,255,255,255)
			
			
			
			local DEG = VAR_STARTANGLE-(VAR_DEGREE-180)/2 + VAR_DEGREE*SpeedPercent
			
				local XP = 100 + math.sin(math.rad(DEG))*50
				local YP = 100 + math.cos(math.rad(DEG))*50
				surface.DrawTexturedRectRotated(XP,YP,5,80,DEG)
				
			draw.SimpleText(math.Round(RealSpeed/VAR_DISPLAYSPEED_DIVIDE) .. "km/h", "RXCarFont_CoolVLW_S30", 100,135, Color(255,255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
			
		cam.End3D2D()
	end
	
	function STR:RenderOnEditor(TuneSysEnt,Vars)
			
	end
	
	

TunerElement_Register(STR)