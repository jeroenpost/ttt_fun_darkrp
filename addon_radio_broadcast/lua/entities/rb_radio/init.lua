AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include('shared.lua')

function ENT:Initialize( )
	self:SetModel( "models/props/cs_office/radio.mdl" )
	self:SetUseType( SIMPLE_USE )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetUseType( SIMPLE_USE )
	
	self:GetPhysicsObject():Wake()
end

function ENT:Use( activator )
	local int = self:GetNWInt( "rb_tick", 0 ) + 1
	local mic = ents.FindByClass( "rb_mic" )[ int ]
	
	if ( !mic ) then int = 0 end
	
	self:SetNWInt( "rb_tick", int )
end