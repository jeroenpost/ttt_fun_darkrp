include('shared.lua')

function ENT:GetCurMic()
	if ( ( self.lastMicLook or 0 ) > CurTime() ) then return self.curMic end
	
	self.lastMicLook = CurTime() + 1
	
	local mic = ents.FindByClass( "rb_mic" )[ self:GetNWInt( "rb_tick") ]
	
	self.curMic = mic
	
	return self.curMic
end

function ENT:Draw()
	self:DrawModel()
	
	local ang = self:GetAngles()
	
	ang:RotateAroundAxis( ang:Up(), 90 )
	ang:RotateAroundAxis( ang:Forward(), 90 )
	
	local mic = self:GetCurMic()
	local bool = IsValid( mic )
	
	local owner = ( IsValid( mic ) and mic:GetOwnerShip() ) or NULL
	local name = ( IsValid( owner ) and owner:Nick() ) or "UNKNOWN"
	
	local col = ( bool and Color( 0, 255, 0 ) ) or Color( 255, 0, 0 )
	local text = ( bool and name ) or "OFF" 
	
	cam.Start3D2D( self:LocalToWorld(Vector(2.4,0,7)), ang, .1 )
		draw.RoundedBox( 2, 0, 5, 70, 25, col )
		draw.SimpleText( text, "DermaDefault", 35, 18, color_white, 1, 1 )
	cam.End3D2D()
end

function ENT:OnRemove()
	if ( self.station ) then
		if ( self.station.Stop ) then
			self.station:Stop()
		end
	end
end

function ENT:Think()
	if ( ( self.lastThink or 0 ) > CurTime() ) then return end

	self.lastThink = CurTime() + 1
	
	local mic = ents.FindByClass( "rb_mic" )[ self:GetNWInt( "rb_tick") ]
	
	if ( !IsValid( mic ) ) then 
		if ( self.station ) then
			self.station:Stop()
			self.station = nil
		end
		
		return 
	end
	
	local sid = mic:GetNWString( "rb_url" )
	
	if ( sid == "" or self.stationURL ~= sid or !mic:GetNWBool( "rb_power" ) ) then
		if ( self.station ) then
			self.station:Stop()
			self.station = nil
		end
		
		self.stationURL = sid
	elseif ( !self.station ) then
		if ( self.querying_station ) then return end
		
		self.querying_station = true
		
		sound.PlayURL( "http://yp.shoutcast.com/sbin/tunein-station.pls?id="..sid, "3d", function( station )
			if ( !IsValid( station ) ) then
				self.station = nil
				self.querying_station = nil
				return
			end
			
			self.station = station
			
			station:SetPos( self:GetPos() )
			station:Play()
			
			self.querying_station = nil
		end )
		
		self.station = html
	end
	
	if ( IsValid( self.station ) ) then
		self.station:SetPos( self:GetPos() )
	end
end