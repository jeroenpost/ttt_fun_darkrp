DEFINE_BASECLASS( "base_anim" )

ENT.Base 				= "base_anim"
ENT.PrintName			= "Radio Broadcast: Mic"
ENT.Author				= "Bloodwave"
ENT.Contact				= ""
ENT.Purpose				= ""
ENT.Instructions		= ""

ENT.Spawnable			= false
ENT.AdminOnly			= false

function ENT:GetOwnerShip()
	if ( ( self.lastOwnerTick or 0 ) > CurTime() ) then 
		return self.ownership 
	end
	
	self.lastOwnerTick = CurTime() + 1
	self.ownership = RB:playerBySteamID( self:GetNWString( "rb_owner" ) )
	
	return self.ownership
end

RB.Commands = {}

local function addCommand( tab )
	RB.Commands[ #RB.Commands + 1 ] = tab
end	

addCommand({
	Name = "Set Upright",
	Icon = "icon16/arrow_up.png",
	Func = function( ent, ply )
		ent:GetPhysicsObject():EnableMotion( false )
			ent:SetAngles( Angle( 0, 45, 0 ) )
			
		timer.Simple( .5, function()
			if ( IsValid( ent ) ) then
				ent:GetPhysicsObject():EnableMotion( true )
			end
		end )
	end
})

addCommand({
	Name = "Toggle Power",
	Icon = "icon16/connect.png",
	Func = function( ent, ply )
		ent:SetNWBool( "rb_power", !ent:GetNWBool( "rb_power" ) )
	end 
})

addCommand({
	Name = "Play a song",
	Icon = "icon16/music.png",
	Func = function( ent, ply )
		net.Start( "rb musicbrowse" )
			net.WriteEntity( ent )
		net.Send( ply )
	end
})

addCommand({
	Name = "Register Ownership",
	Icon = "icon16/key.png",
	Func = function( ent, ply )
		for k,v in pairs( ents.FindByClass( "rb_mic" ) ) do
			if ( v:GetNWString( "rb_owner" ) == ply:SteamID() ) then
				ply:ChatPrint( "You already own a mic!" )
				return
			end
		end
		
		ent:SetNWString( "rb_owner", ply:SteamID() )
		ply:ChatPrint( "You now own this mic." )
	end
})