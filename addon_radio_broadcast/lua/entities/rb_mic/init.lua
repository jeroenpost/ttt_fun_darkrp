AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include('shared.lua')

function ENT:Initialize( )
	self:SetModel( "models/props_trainstation/trainstation_post001.mdl" )
	self:SetUseType( SIMPLE_USE )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetUseType( SIMPLE_USE )
	
	self:GetPhysicsObject():SetMass( 1 )
end

util.AddNetworkString( "rb use" )
util.AddNetworkString( "rb cmd" )

function ENT:Use( activator )
	net.Start( "rb use" )	
		net.WriteEntity( self )
	net.Send( activator )
end

net.Receive( "rb cmd", function( length, ply )
	local key = net.ReadInt( 32 )
	local ent = net.ReadEntity()

	local data = RB.Commands[ key ]
	
	if ( data ) then
		data.Func( ent, ply )
	end
end )