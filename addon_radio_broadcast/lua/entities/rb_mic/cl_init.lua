include('shared.lua')

local ModelInfo = {
	{
		Model = "models/props_c17/lampshade001a.mdl",
		Pos = Vector( 0, 3, 54 ),
		Ang = Angle( 10, 90, 120 ),
		Scale = .65
	},
	{
		Model = "models/props_radiostation/radio_antenna01.mdl",
		Pos = Vector( 0, 0, 55 ),
		Ang = Angle( 0, 0, 120 ),
		Scale = .02
	},
	{
		Model = "models/props_c17/Frame002a.mdl",
		Pos = Vector( 0, 0, 0 ),
		Ang = Angle( 90, 0, 0 ),
		Scale = .3
	}
}

function ENT:Initialize()
	self.Models = {}
	
	for k,v in pairs( ModelInfo ) do
		self.Models[ k ] = ClientsideModel( v.Model )
		self.Models[ k ]:SetNoDraw( true )
	end
end

function ENT:Draw()
	self:DrawModel()
	
	for k, ent in pairs( self.Models ) do
		local data = ModelInfo[ k ]
		
		local pos = self:LocalToWorld( data.Pos )
		local ang = self:GetAngles()
		
		ang:RotateAroundAxis( ang:Right(), data.Ang.p )
		ang:RotateAroundAxis( ang:Forward(), data.Ang.y )
		ang:RotateAroundAxis( ang:Up(), data.Ang.r )
		
		ent:SetModelScale( data.Scale, 0 )
		
		ent:SetRenderOrigin( pos )
		ent:SetRenderAngles( ang )
		ent:DrawModel()
	end
	
	local ang = self:GetAngles()
	
	ang:RotateAroundAxis( ang:Up(), 180 )
	ang:RotateAroundAxis( ang:Forward(), 90 )
	
	local col = ( self:GetNWBool( "rb_power" ) and Color( 0, 255, 0 ) ) or Color( 255, 0, 0 )
	local text = ( self:GetNWBool( "rb_power" ) and "ONLINE" ) or "OFFLINE" 
	
	cam.Start3D2D( self:LocalToWorld(Vector(3.5,4,58)), ang, .25 )
		draw.RoundedBox( 12, 0, 0, 30, 30, col )
		draw.SimpleText( text, "DermaDefault", 15, 15, color_white, 1, 1 )
	cam.End3D2D()
end

net.Receive( "rb use", function()
	local ent = net.ReadEntity()
	
	gui.EnableScreenClicker( true )
	
	local menu = DermaMenu()
	
	local owner = RB:playerBySteamID( ent:GetNWString( "rb_owner" ) )
	
	if ( IsValid( owner ) ) then
		menu:AddOption( owner:Nick() ):SetIcon( "icon16/user.png" )
		menu:AddSpacer()
	end
	
	for k,v in pairs( RB.Commands ) do
		menu:AddOption( v.Name, function() 
			net.Start( "rb cmd" )
				net.WriteInt( k, 32 )
				net.WriteEntity( ent )
			net.SendToServer()
		end ):SetIcon( v.Icon )
	end
	
	function menu:OnRemove()
		gui.EnableScreenClicker( false )
	end
	
	menu:Open()
end )