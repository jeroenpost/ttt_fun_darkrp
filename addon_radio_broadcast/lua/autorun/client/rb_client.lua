local function getShoutcastID( url )
	if ( !string.find( url, "shoutcast.com" ) ) then return nil end
	
	local pos = string.find( url, "=" )
	
	if ( !pos ) then return nil end
	
	return string.sub( url, pos+1, string.len( url ) )
end

net.Receive( "rb musicbrowse", function()
	local mic = net.ReadEntity()
	
	local frame = vgui.Create( "DFrame" )
	frame:SetSize( 310, 120 )
	frame:Center()
	frame:MakePopup()
	frame:SetTitle( "Radio Broadcast - Music Select" )
	
	local url = vgui.Create( "DTextEntry", frame )
	url:SetText( "Enter shoutcast station link" )
	url:SetPos( 30, 35 )
	url:SetSize( 250, 20 )
	
	local search = vgui.Create( "DButton", frame )
	search:SetPos( 30, 60 )
	search:SetSize( 100, 20 )
	search:SetText( "Search Shoutcast" )
	
	function search:DoClick()
		gui.OpenURL( "http://www.shoutcast.com" )
	end
	
	local enter = vgui.Create( "DButton", frame )
	enter:SetPos( 30 + 5 + search:GetWide(), 60 )
	enter:SetSize( 70, 20 )
	enter:SetText( "Enter" )
	
	function enter:DoClick()
		local url = url:GetValue():Trim()
		local id = getShoutcastID( url )
		
		if ( !id ) then
			LocalPlayer():ChatPrint( "You didnt enter a valid shoutcast URL!" )
			return
		end
		
		sound.PlayURL( "http://yp.shoutcast.com/sbin/tunein-station.pls?id="..id, "3d", function( station )
			if ( !IsValid( station ) ) then
				LocalPlayer():ChatPrint( "This station is not supported, sorry!" )
				return
			end
			
			station:Stop()
		end )
		
		net.Start( "rb musicplay" )
			net.WriteString( id )
			net.WriteEntity( mic )
		net.SendToServer()
	end
	
	local stop = vgui.Create( "DButton", frame )
	stop:SetText( "Stop" )
	stop:SetPos( 30 + 5 + search:GetWide() + enter:GetWide() + 5, 60 )
	stop:SetSize( 70, 20 )
	
	function stop:DoClick()
		net.Start( "rb musicplay" )
			net.WriteString( "stop" )
			net.WriteEntity( mic )
		net.SendToServer()
	end
	
	local tip = vgui.Create( "DLabel", frame )
	tip:SetText( "Tip: Right click the download button next to the\nstation you want and press copy link address." )
	tip:SetPos( 30, 85 )
	tip:SizeToContents()
	
end )