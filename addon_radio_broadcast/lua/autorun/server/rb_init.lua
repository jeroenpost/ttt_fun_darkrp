util.AddNetworkString( "rb musicbrowse" )
util.AddNetworkString( "rb musicplay" )

net.Receive( "rb musicplay", function( length, ply )
	local shoutid = net.ReadString()
	local mic = net.ReadEntity()
	
	if ( !IsValid( mic ) or !string.find( mic:GetClass(), "rb_" ) ) then return end
	
	mic:SetNWString( "rb_url", shoutid )
	
	if ( shoutid == "stop" ) then
		shoutid = ""
		ply:ChatPrint( "Stopped music" )
	else
		ply:ChatPrint( "Playing "..shoutid.."!" )
	end
end )

local function isNearMic( ply )
	for k, mic in pairs( ents.FindByClass( "rb_mic" ) ) do
		if ( !mic:GetNWBool("rb_power") ) then continue end
	
		if ( mic:GetPos():Distance( ply:GetPos() ) <= 150 ) then
			return true, mic
		end
	end
	
	return nil, nil
end

local function allNearRadios( mic )
	local tab = {}
	
	for k, ply in pairs( player.GetAll() ) do
		local plyPos = ply:GetPos()
	
		for k, radio in pairs( ents.FindByClass( "rb_radio" ) ) do
			local radio_mic = ents.FindByClass("rb_mic")[ radio:GetNWInt("rb_tick") ]
			
			if ( radio:GetPos():Distance( plyPos ) <= 150 and radio_mic == mic ) then
				tab[ ply:SteamID() ] = true
			end
		end
	end
	
	return tab
end

RB.voice = {}
