
local ShowEmergencyOnHUD = true


local ems_caller

net.Receive( "rphone_ems_incomming", function( len )
	local ply = net.ReadEntity()

	if !IsValid( ply ) or !ply:IsPlayer() then return end

	ems_caller = ply

	chat.AddText( Color( 255, 0, 0, 255 ), "[Dispatcher] Emergency call incomming, please respond." )
end )

net.Receive( "rphone_ems_ended", function( len )
	ems_caller = nil
end )

hook.Add( "HUDPaint", "cl_ems_show_caller", function()
	if !IsValid( ems_caller ) or !ShowEmergencyOnHUD then return end

	local tpos = ems_caller:GetShootPos() + Vector( 0, 0, 40 )
	local dist = LocalPlayer():GetShootPos():Distance( tpos )
	local spos = tpos:ToScreen()

	if !spos then return end

	render.SuppressEngineLighting( true )

	draw.SimpleText(
		"Emergency - " .. math.floor( dist ),
		"CloseCaption_Bold",
		spos.x, spos.y,
		Color( 255, 0, 0, math.cos( (CurTime() * (2/3)) * math.pi ) * 255 ),
		TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER
	)

	render.SuppressEngineLighting( false )
end )
