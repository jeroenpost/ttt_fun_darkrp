
local EmergencyNumbers = {
	"911"
}
local ResponderTeams = {
	TEAM_POLICE,
	TEAM_CHIEF,
    TEAM_SWAT,
    TEAM_FIREFIGHTER
}

timer.Simple(10,function()
    ResponderTeams = {
        TEAM_POLICE,
        TEAM_CHIEF,
        TEAM_SWAT,
        TEAM_FIREFIGHTER
    }

end)


util.AddNetworkString( "rphone_ems_incomming" )
util.AddNetworkString( "rphone_ems_ended" )

local PhoneLib = rPhone.GetLibrary( "phone" )

local calls = {}
local numbers = {}

do
	for _, num in pairs( EmergencyNumbers ) do
		num = rPhone.ToRawNumber( num )

		numbers[num] = true

		rPhone.ReserveNumber( num )
	end
end

rPhone.RegisterEventCallback( "PHONE_DIALED", function( num, ply )
	if !table.HasValue( EmergencyNumbers, num ) then return end
	if table.HasValue( ResponderTeams, ply:Team() ) then
		return false, ([[Cannot call %s as an emergency responder.]]):format( rPhone.ToNiceNumber( num ) )
	end

	local dispatchers = {}

	for _, disp in pairs( player.GetAll() ) do
		if table.HasValue( ResponderTeams, disp:Team() ) and disp != ply and !PhoneLib.IsInCall( disp ) then
			table.insert( dispatchers, disp )
		end
	end

	local disp = table.Random( dispatchers )

	if disp then
		net.Start( "rphone_ems_incomming" )
			net.WriteEntity( ply )
		net.Send( disp )

		calls[disp] = true

		return true, disp
	end

	return false, "No responders available."
end )

rPhone.RegisterEventCallback( "PHONE_CALL_ENDED", function( convid, users )
	for _, ply in pairs( users ) do
		if calls[ply] then
			net.Start( "rphone_ems_ended" )
			net.Send( ply )

			calls[ply] = nil
		end
	end		
end )
