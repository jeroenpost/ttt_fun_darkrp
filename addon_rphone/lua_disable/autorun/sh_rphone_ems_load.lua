
if SERVER then
	AddCSLuaFile()

	AddCSLuaFile( "rphone_ems/client/cl_ems.lua" )
end

hook.Add( "CreateTeams", "sh_rphone_ems_load_checkdependencies", function()
	timer.Simple( 1, function()													--a little hacky, allow all teams to be created first
		assert( rPhone, "rPhone EMS: rPhone is not installed." )
		assert( 
			rPhone.MajorVersion == 1 and
			rPhone.MinorVersion >= 4, 
			"rPhone EMS: Make sure both rPhone Emergency Services and rPhone are up-to-date."
		)

		if SERVER then
			include( "rphone_ems/server/sv_ems.lua" )
		else
			include( "rphone_ems/client/cl_ems.lua" )
		end
	end )
end )
