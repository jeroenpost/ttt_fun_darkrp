surface.CreateFont("UiBoldBank", {
	font = "Tahoma", 
	size = 25, 
	weight = 600
})

surface.CreateFont("UiSmallerThanBold", {
	font = "Tahoma", 
	size = 11, 
	weight = 600
})

surface.CreateFont("Trebuchet24", {
	font = "Trebuchet MS", 
	size = 24, 
	weight = 900
})
	
surface.CreateFont("Trebuchet22", {
	font = "Trebuchet MS", 
	size = 22, 
	weight = 900
})

surface.CreateFont("Trebuchet20", {
	font = "Trebuchet MS", 
	size = 20, 
	weight = 900
})