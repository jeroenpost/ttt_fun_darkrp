BANK_CUSTOM_MoneyTimer = 60 -- This is the time that defines when money is added to the bank. In seconds! [Default = 60]
BANK_CUSTOM_MoneyOnTime = 500 -- This is the amount of money to be added to the bank every x minutes/seconds. Defined by the setting above. [Default = 500]
BANK_Custom_Max = 30000 -- The maximum the bank can have. Set to 0 for no limit. [Default = 30000]
BANK_Custom_AliveTime = 1 -- The amount of MINUTES the player must stay alive before he will receive what the bank has. IN MINUTES! [Default = 5]
BANK_Custom_CooldownTime = 10 -- The amount of MINUTES the bank is on a cooldown after a robbery! (Doesn't matter if the robbery failed or not) [Default = 20]
BANK_Custom_RobberyDistance = 3000 -- The amount of space the player can move away from the bank entity, before the bank robbery fails. [Default = 500]
BANK_Custom_PlayerLimit = 7 -- The amount of players there must be on the server before you can rob the bank. [Default = 5]
BANK_Custom_KillReward = 1000 -- The amount of money a person is rewarded for killing the bank robber. [Default = 1000]
BANK_Custom_PoliceRequired = 3 -- The amount of police officers there must be before a person can rob the bank. [Default = 3]
BANK_Custom_DropMoneyOnSucces = false -- Should money drop from the bank when a robbery is succesful? true/false option. [Default = false]

RequiredTeams = { -- These are the names of the jobs that counts with the option above, the police required. The amount of players on these teams are calcuated together in the count.
	"Civil Protection",
	"Civil Protection Chief", "S.W.A.T" -- THE LAST LINE SHOULD NOT HAVE A COMMA AT THE END. BE AWARE OF THIS WHEN EDITING THIS!
}

GovernmentTeams = { -- These are the teams that will receive a notify when a player is trying to rob a bank. Use the actual team name, as shown below.
	"Civil Protection",
	"Civil Protection Chief",
	"Mayor","S.W.A.T" -- THE LAST LINE SHOULD NOT HAVE A COMMA AT THE END. BE AWARE OF THIS WHEN EDITING THIS!
}

AllowedTeams = { -- These are the teams that are allowed to rob the bank.

    "Bank Robber",
     "Joker","Venom","Mafia","Mafia Boss" -- THE LAST LINE SHOULD NOT HAVE A COMMA AT THE END. BE AWARE OF THIS WHEN EDITING THIS!
}

-- Design options for the bank entity display.
BANK_Design_BankVaultName = Color(153, 0, 0, 255)
BANK_Design_BankVaultNameBoarder = Color(0, 0, 0, 255)

BANK_Design_BankVaultAmount = Color(0, 153, 0, 255)
BANK_Design_BankVaultAmountBoarder = Color(0, 0, 0, 255)

BANK_Design_BankVaultCountdownName = Color(150, 150, 150, 255)
BANK_Design_BankVaultCountdownBoarderName = Color(0, 0, 0, 255)
BANK_Design_BankVaultCountdown = Color(150, 150, 150, 255)
BANK_Design_BankVaultCountdownBoarder = Color(0, 0, 0, 255)

BANK_Design_BankVaultCooldownName = Color(150, 150, 150, 255)
BANK_Design_BankVaultCooldownBoarderName = Color(0, 0, 0, 255)
BANK_Design_BankVaultCooldown = Color(150, 150, 150, 255)
BANK_Design_BankVaultCooldownBoarder = Color(0, 0, 0, 255)

-- DON'T WORRY ABOUT THIS!
function util.RobberyFormatNumber( n )
    if (!n or type(n) != "number") then
        return 0
    end
	
    if n >= 1e14 then return tostring(n) end
    n = tostring(n)
    sep = sep or ","
    local dp = string.find(n, "%.") or #n+1
    for i=dp-4, 1, -3 do
        n = n:sub(1, i) .. sep .. n:sub(i+1)
    end
    return n
end