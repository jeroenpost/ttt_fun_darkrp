AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/props_junk/cardboard_box004a.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
	local phys = self:GetPhysicsObject()
	phys:Wake()
end

function ENT:Use(activator,caller)
	if !IsValid(caller) and self.used then return end
	if(caller:Team() != FARMCFG.Job) then
		caller:ChatPrint("You are not a farmer!")
		return
	end
	self.used = true
	caller:setDarkRPVar( "OrangeSeeds", caller:getDarkRPVar( "OrangeSeeds" ) + FARMCFG.OrangeSeeds )
	self:Remove()
end



function ENT:StartTouch(ent)

end

function ENT:Think()

end

function ENT:OnRemove()
	if !IsValid(self.dt.owning_ent) then return end
	self.dt.owning_ent.maxfarming_seedboxorange = self.dt.owning_ent.maxfarming_seedboxorange -1
end