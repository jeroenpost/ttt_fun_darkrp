AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT:Initialize()
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
    self.Sellable = true
	local phys = self:GetPhysicsObject()

	phys:Wake()
end

function ENT:OnTakeDamage(dmg)
	self:Remove()
end


