AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/weapons/w_bugbait.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
	local phys = self:GetPhysicsObject()
	phys:EnableMotion(false)
	self.Fruit = {}
end

function ENT:RemoveMeAfterAWhile()
    timer.Simple(200,function()
        if not IsValid(self) then return end
        self:SetColor(Color(255,0,0,255));
    end)
    timer.Simple(600,function()
        if not IsValid(self) then return end
        self:SetColor(Color(150,0,0,255));
    end)
    timer.Simple(720,function()
        if not IsValid(self) then return end

        self:Remove()
    end)
end


function ENT:BeginGrow(Type)
	self.SeedType = Type or "Melon"
	timer.Simple(math.random(FARMCFG.SeedGrowMin, FARMCFG.SeedGrowMax), function()
		if(self.SeedType == "Orange") then
			self:SetModel("models/props_foliage/shrub_01a.mdl")
			self:PhysicsInit(SOLID_VPHYSICS)
			timer.Simple(math.random(FARMCFG.OrangeFruitGrowMin,FARMCFG.OrangeFruitGrowMax), function()
				local pos = {Vector(20,10,30), Vector(15,-10,40), Vector(-10,-20,45)}
				if(math.random(0,2) > 1) then
					pos = {Vector(20,10,30), Vector(15,-10,40), Vector(-10,0,25), Vector(-10,-20,45)}
				end
				for k,v in pairs(pos) do
					local ent = ents.Create("farmer_food_orange")
						ent:SetModel("models/props/cs_italy/orange.mdl")
						ent:SetPos(self:GetPos() + v)
						ent.Sellable = true
						ent:Spawn()
						ent:GetPhysicsObject():EnableMotion(false)
						table.insert(self.Fruit, ent)
                end
                self:RemoveMeAfterAWhile()
			end)
		elseif(self.SeedType == "Melon") then
			self:SetModel("models/props/pi_fern.mdl")
			self:PhysicsInit(SOLID_VPHYSICS)
			timer.Simple(math.random(FARMCFG.MelonFruitGrowMin,FARMCFG.MelonFruitGrowMax), function()
				local pos = {Vector(20,20,6), Vector(15,-10,6), Vector(-20,0,6)}
				if(math.random(0,2) > 1) then
					pos = {Vector(20,20,6), Vector(15,-10,6), Vector(-20,0,6), Vector(-10, -15, 6)}
				end
				for k,v in pairs(pos) do
					local ent = ents.Create("farmer_food_melon")
						ent:SetModel("models/props_junk/watermelon01.mdl")
						ent:SetPos(self:GetPos() + v)
						ent.Sellable = true
						ent:Spawn()
						ent:GetPhysicsObject():EnableMotion(false)
						table.insert(self.Fruit, ent)
                end
                self:RemoveMeAfterAWhile()
			end)
		elseif(self.SeedType == "Banana") then
			self:SetModel("models/props/de_dust/palm_tree_head_skybox.mdl")
			self:PhysicsInit(SOLID_VPHYSICS)
			timer.Simple(math.random(FARMCFG.BananaFruitGrowMin,FARMCFG.BananaFruitGrowMax), function()
				local pos = {Vector(20,20,20), Vector(15,-10,30), Vector(-20,0,25)}
				if(math.random(0,2) > 1) then
					pos = {Vector(20,20,20), Vector(15,-10,30), Vector(-20,0,25), Vector(-10, -15, 26)}
				end
				for k,v in pairs(pos) do
					local ent = ents.Create("farmer_food_banana")
						ent:SetModel("models/props/cs_italy/bananna_bunch.mdl")
						ent:SetPos(self:GetPos() + v)
						ent.Sellable = true
						ent:Spawn()
						ent:GetPhysicsObject():EnableMotion(false)
						table.insert(self.Fruit, ent)
                end
                self:RemoveMeAfterAWhile()
			end)
        elseif(self.SeedType == "Pizza") then
            self:SetModel("models/props/pi_fern.mdl")
            self:PhysicsInit(SOLID_VPHYSICS)
            timer.Simple(math.random(FARMCFG.PizzaFruitGrowMin,FARMCFG.PizzaFruitGrowMax), function()
                local pos = {Vector(20,20,6), Vector(15,-10,6), Vector(-20,0,6)}
                if(math.random(0,2) > 1) then
                    pos = {Vector(20,20,6), Vector(15,-10,6), Vector(-20,0,6), Vector(-10, -15, 6)}
                end
                for k,v in pairs(pos) do
                    local pizza = math.random(1,3)
                    local ent = ents.Create("farmer_food_pizza")
                    ent:SetModel("models/workspizza0"..pizza.."/workspizza0"..pizza..".mdl")
                    ent:SetPos(self:GetPos() + v)
                    ent.Sellable = true
                    ent:Spawn()
                    ent:GetPhysicsObject():EnableMotion(false)
                    table.insert(self.Fruit, ent)
                end
                self:RemoveMeAfterAWhile()
            end)
        end
	end)
end

function ENT:Think()

end

function ENT:OnRemove()
	for k,v in pairs(self.Fruit) do
		if(IsValid(v)) then
			local Phys = v:GetPhysicsObject()
			if(IsValid(Phys)) then
				Phys:EnableMotion(true)
				Phys:SetVelocity(Vector(0,0,-10))
            end
            timer.Simple(200,function()
                if not IsValid(v) then return end
                v:SetColor(Color(255,0,0,255));
            end)
            timer.Simple(450,function()
                if not IsValid(v) then return end
                v:SetColor(Color(150,0,0,255));
            end)
            timer.Simple(620,function()
                if not IsValid(v) then return end
                v:Remove()
            end)
		end
	end
end