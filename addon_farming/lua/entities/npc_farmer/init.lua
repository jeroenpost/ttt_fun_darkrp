AddCSLuaFile('cl_init.lua')
AddCSLuaFile('shared.lua')

include('shared.lua')

function ENT:Initialize()
	self:SetModel("models/Humans/Group03/Male_04.mdl" )
	self:SetHullType( HULL_HUMAN )
	self:SetHullSizeNormal( )
	self:SetNPCState( NPC_STATE_SCRIPT )
	self:SetSolid( SOLID_BBOX )
	self:CapabilitiesAdd( bit.bor(CAP_ANIMATEDFACE, CAP_TURN_HEAD) )
	self:SetUseType( SIMPLE_USE )
	self:DropToFloor()
	self:SetMaxYawSpeed( 90 )
end

function ENT:GetItemCounts(ply)
    if not gb_inventory then return 0 end
	local amounts = {}

	for x = 1, gb_inventory.config.ySize do
		for y = 1, gb_inventory.config.xSize do
			if ply.gb_inventory[ x ][ y ] != false then

                local classy = ply.gb_inventory[ x ][ y ].class

                if(classy == "farmer_food" or
                                    classy == "farmer_food_pizza" or
                            classy == "farmer_food_melon" or
                            classy == "farmer_food_banana" or
                            classy == "farmer_food_orange") then

                            amounts[classy] = (amounts[classy] or 0) + ply.gb_inventory[ x ][ y ].amount

                end
            end
		end
	end
	return amounts

end

function ENT:AcceptInput(name, activator, caller, data)
	if !(name == "Use" and IsValid(caller) and caller:IsPlayer()) then return end
	if(caller:Team() != FARMCFG.Job) then
		caller:ChatPrint("You are not a farmer! I only serve farmers!")
	else



        	local amounts = self:GetItemCounts(caller)
        	local total = 0

        		for k,v in pairs(amounts) do
                    local price =  FARMCFG.Priceperfood * v
                    local xp = price * 1.5
                    total = total + v
                    caller:ChatPrint("Bought "..v.." times "..k.." for $"..price.." and "..xp.." XP")
                    caller:inventory_remove_item( k, v )
                    caller:addMoney(price)
                    caller:addXP(xp)
                end

            if total < 1 then
                caller:ChatPrint("You don't have items that I can buy")
            end


	end
	if(caller:IsSuperAdmin()) then
		--SaveFarmingPositions()
	end
end

function ENT:OnRemove()
	
end

function ENT:OnPhysgunFreeze(weapon, phys, ent, ply)
	if(ply:IsSuperAdmin()) then
		--SaveFarmingPositions()
		return true
	end
	return false
end

function ENT:PhysgunPickup(ply, ent)
	return ply:IsSuperAdmin()
end
