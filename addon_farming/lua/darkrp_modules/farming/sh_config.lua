FARMCFG = {}
FARMCFG.Priceperfood = 90			// Price for each food
FARMCFG.Xpperfood = 500
FARMCFG.Job = TEAM_FARMER			// Only one job allowed
FARMCFG.MelonSeeds = 5 				// Melon seeds per box
FARMCFG.OrangeSeeds = 5 			// Orange seeds per box
FARMCFG.BananaSeeds = 5				// Bananna seeds per box
FARMCFG.PizzaSeeds = 5

FARMCFG.SeedGrowMin = 120		//Minimum time for the Seed seed to grow into a plant (Stage 1)
FARMCFG.SeedGrowMax = 300		//Maxmum time for the Seed seed to grow into a plant (Stage 1)


FARMCFG.BananaFruitGrowMin = 50		//Minimum time for the Banana plant to grow fruit (Stage 2)
FARMCFG.BananaFruitGrowMax = 300		//Maxmum time for the Banana plant to grow fruit (Stage 2)

FARMCFG.OrangeFruitGrowMin = 50		//Minimum time for the Orange plant to grow fruit (Stage 2)
FARMCFG.OrangeFruitGrowMax = 300		//Maxmum time for the Orange plant to grow fruit (Stage 2)

FARMCFG.MelonFruitGrowMin = 50 		//Minimum time for the Melon plant to grow fruit (Stage 2)
FARMCFG.MelonFruitGrowMax = 300 		//Maxmum time for the Melon plant to grow fruit (Stage 2)

FARMCFG.PizzaFruitGrowMin = 50 		//Minimum time for the Melon plant to grow fruit (Stage 2)
FARMCFG.PizzaFruitGrowMax = 300 		//Maxmum time for the Melon plant to grow fruit (Stage 2)

DarkRP.registerDarkRPVar("MelonSeeds", fn.Curry(fn.Flip(net.WriteInt), 2)(32), fn.Partial(net.ReadInt, 32))
DarkRP.registerDarkRPVar("BananaSeeds", fn.Curry(fn.Flip(net.WriteInt), 2)(32), fn.Partial(net.ReadInt, 32))
DarkRP.registerDarkRPVar("OrangeSeeds", fn.Curry(fn.Flip(net.WriteInt), 2)(32), fn.Partial(net.ReadInt, 32))
DarkRP.registerDarkRPVar("PizzaSeeds", fn.Curry(fn.Flip(net.WriteInt), 2)(32), fn.Partial(net.ReadInt, 32))
DarkRP.registerDarkRPVar("SelectionSeed", fn.Curry(fn.Flip(net.WriteInt), 2)(32), fn.Partial(net.ReadInt, 32))

function farmer_create_crop(mdl,name)
    local ENT = {}

    ENT.Base = "farmer_food"

    if ( SERVER ) then
        function ENT:Initialize()
            self:SetModel( mdl )
            self.BaseClass.Initialize(self)
        end
    end
    scripted_ents.Register( ENT, "farmer_food_"..string.lower( name ), true )
end


timer.Simple(10,function()
    FARMCFG.Job = TEAM_FARMER
    --if SERVER then
        farmer_create_crop("models/workspizza03/workspizza03.mdl","pizza")
        farmer_create_crop("models/props_junk/watermelon01.mdl","melon")
        farmer_create_crop("models/props/cs_italy/bananna_bunch.mdl" ,"banana")
        farmer_create_crop("models/props/cs_italy/orange.mdl" ,"orange")
   -- end



end)
