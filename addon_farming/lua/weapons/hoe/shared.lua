Seeds = {}
Seeds[0] = {name = "Melon"}
Seeds[1] = {name = "Banana"}
Seeds[2] = {name = "Orange" }
Seeds[3] = {name = "Pizza"}

if ( SERVER ) then

	AddCSLuaFile( "shared.lua" )

	SWEP.HoldType			= "Crowbar"
end

if ( CLIENT ) then

	SWEP.PrintName			= "Hoe"
	SWEP.Author				= "GreenBlack"
    SWEP.Category				= "GreenBlack"
	SWEP.Slot				= 0
	SWEP.SlotPos			= 7
	SWEP.ViewModelFOV		= 62
	SWEP.IconLetter			= "x"
	surface.CreateFont( "Seedfont", {
		font 		= "Arial",
		size 		= 30,
		weight 		= 1250,
		antialias 	= true
	})
end
---------------------------------------------------
 

function SWEP:Initialize()
	
end

hook.Add("HUDPaint", "DrawFarmingHUD", function()
    if not FARMCFG then return end
	if(LocalPlayer():Team() == FARMCFG.Job) then
		draw.RoundedBox( 6, 50, 50, 200, 40, Color( 0, 0, 0, 150 ) )
		local seedname = Seeds[LocalPlayer().DarkRPVars.SelectionSeed].name
		local text = LocalPlayer().DarkRPVars[seedname.."Seeds"] .. " " .. seedname .. " Seeds"
		draw.SimpleText(text, "Seedfont", 150, 70, Color(255,255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
end)

function SWEP:PrimaryAttack()
	if(self.Weapon.time2 and self.Weapon.time2 >= CurTime()) then return end
	self.Weapon.time2 = CurTime() + 0.4
	if(self.Owner:Team() != TEAM_FARMER) then
		self.Owner:ChatPrint("You are not a farmer!")
		return
	end

	local trace = self.Owner:GetEyeTrace()

	if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 175 then
		self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)
		if SERVER then
			local can, pos = self.Weapon:CanPlant(trace.HitPos)			
			for k,v in pairs(ents.FindInSphere(pos, 50)) do
				if(v:GetClass() == "farming_seedbeam") then
					self.Owner:ChatPrint("You can't plant a seed too close to another one")
					return
				end
			end
			if(can) then
				local seedname = Seeds[self.Owner.DarkRPVars.SelectionSeed].name
				local seedamount = self.Owner.DarkRPVars[seedname .. "Seeds"] or 0
				
				if(seedamount >= 1 and seedname) then
					local ent = ents.Create("farming_seedbeam")
					ent:BeginGrow(seedname)
					ent:SetPos(pos)

					ent:Spawn()
						
					self.Owner:ChatPrint("You have planted a " .. seedname .. " seed!")
					self.Owner:setDarkRPVar(seedname .."Seeds", seedamount - 1)
				else
					self.Owner:ChatPrint("You do not have enough seeds!")
				end
			else
				self.Owner:ChatPrint("You cant plant that here! Find some suitable ground.")
			end
		end		
	end

end

function SWEP:OnRemove()
end

function SWEP:SecondaryAttack()
	if(self.Weapon.time and self.Weapon.time >= CurTime()) then return end
	self.Weapon.time = CurTime() + 0.5
	if(self.Owner:Team() != TEAM_FARMER) then
		self.Owner:ChatPrint("You are not a farmer!")
		return
	end
	if SERVER then
		local SelectedSeed = self.Owner.DarkRPVars.SelectionSeed or 0
		if(SelectedSeed >= #Seeds) then
			self.Owner:setDarkRPVar("SelectionSeed", 0)
		else
			self.Owner:setDarkRPVar("SelectionSeed", SelectedSeed + 1)
		end
	end
end

function SWEP:Reload()
	if(self.Weapon.time3 and self.Weapon.time3 >= CurTime()) then return end
	self.Weapon.time3 = CurTime() + 0.2
	
	if(self.Owner:Team() != TEAM_FARMER) then
		self.Owner:ChatPrint("You are not a farmer!")
		return
	end
	
	local trace = self.Owner:GetEyeTrace()

	if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 175 then
		self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)
		if(SERVER) then
			local seednum = 0
			for k,v in pairs(ents.FindInSphere(self.Weapon:GetPos(), 20)) do
				if(v:GetClass() == "farming_seedbeam") then
					seednum = seednum + math.random(0,2)
					local seedname = v.SeedType or "Melon"
					self.Owner:setDarkRPVar(seedname.."Seeds", self.Owner:getDarkRPVar(seedname.."Seeds") + seednum)
					v:Remove()
				end
			end
			self.Owner:ChatPrint("You have picked up " .. seednum .. " seeds!")
		end
	end
end
 


-------------------------------------------------------------------
SWEP.Author   = "GreenBlack"
SWEP.Contact        = ""
SWEP.Purpose        = "A Garden Hoe."
SWEP.Instructions   = "Primary or Secondary to plant seeds"
SWEP.Spawnable      = true
SWEP.AdminSpawnable  = false
-----------------------------------------------
SWEP.ViewModel      = "models/weapons/v_crowbar.mdl"
SWEP.WorldModel   = "models/weapons/w_crowbar.mdl"
-----------------------------------------------
SWEP.Primary.Delay		= 0.3
SWEP.Primary.Recoil		= 0
SWEP.Primary.Damage		= 15
SWEP.Primary.NumShots		= 1		
SWEP.Primary.Cone		= 0
SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic   	= false
SWEP.Primary.Ammo         	= "none" 
-------------------------------------------------
SWEP.Secondary.Delay			= 60.999999999
SWEP.Secondary.Recoil			= 0
SWEP.Secondary.Damage			= 8
SWEP.Secondary.NumShots			= 1
SWEP.Secondary.Cone				= 0
SWEP.Secondary.ClipSize			= 1
SWEP.Secondary.DefaultClip		= 1
SWEP.Secondary.Automatic   		= false
SWEP.Secondary.Ammo         		= "pistol"
-------------------------------------------------

function SWEP:CanPlant(pos)
	local tracedata = {}
	tracedata.start = pos
	tracedata.filter = {self, self.Owner}
	tracedata.endpos = pos + Vector(0,0,1000)
	local traceu = util.TraceLine(tracedata)
	local NothingAbove = true
	if traceu.HitWorld and !traceu.HitSky then
		NothingAbove = false
	end

	tracedata.endpos = pos - Vector(0,0,20)
	local OnGrass = false
	local traced = util.TraceLine(tracedata)
	if(traced.HitWorld) then
		OnGrass = (traced.MatType == MAT_DIRT)
	end

    if not OnGrass then
        OnGrass = (traced.MatType == MAT_GRASS)
    end
    if not OnGrass then
        OnGrass = (traced.MatType == MAT_SAND)
    end
    if not OnGrass then
        OnGrass = (traced.MatType == MAT_SAND)
    end

	return (OnGrass and NothingAbove), traced.HitPos
end
