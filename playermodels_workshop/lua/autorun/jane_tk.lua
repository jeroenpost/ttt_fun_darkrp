list.Set( "PlayerOptionsModel", "Jane the Killer", "models/player/jennkiller_m.mdl" )
player_manager.AddValidModel( "Jane the Killer", "models/player/jennkiller_m.mdl" )
player_manager.AddValidHands( "Jane the Killer", "models/weapons/c_arms_chell.mdl", 0, "00000000" )




--Add NPC
local Category = "Jane the Killer"


local NPC = { 	Name = "Jane Everlasting", 
				Class = "npc_citizen",
				Model = "models/player/jennkiller_m.mdl",
				Health = "300",
				KeyValues = { citizentype = 4 },
                                Category = Category    }

list.Set( "NPC", "Jane Everlasting", NPC )