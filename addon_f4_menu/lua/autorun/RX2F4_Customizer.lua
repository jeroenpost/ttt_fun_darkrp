RX2F4_Adjust = {} RX2F4_RuleDB = {} function RX2F4_Rule_AddTitle(Text) table.insert(RX2F4_RuleDB,{Type="Title",Text=Text}) end function RX2F4_Rule_AddText(Text) table.insert(RX2F4_RuleDB,{Type="Text",Text=Text}) end
-- ^ Dont Touch

/* ───────────────────────────────────────────────────
	General
─────────────────────────────────────────────────── */
	-- VIPs
		-- VIP Group. ( Its ULX Group. YOU NEED ULX. these group are able to access VIP Shop or VIP Job )
			RX2F4_Adjust.VIPGroup = {"owner","superadmin","admin"} -- <KEEP THESE LOWERCASE>
	
	-- Appearance
		-- Main Screen Size
			RX2F4_Adjust.Main_Size_X = 0.8 -- 1 Mean 100% fit to your screen. screen will cover your screen. if you want half size. set this to 0.5
			RX2F4_Adjust.Main_Size_Y = 0.8 -- 1 Mean 100% fit to your screen. screen will cover your screen. if you want half size. set this to 0.5
		-- Main Screen Main Text.
			RX2F4_Adjust.Main_MainText = "TTT-FUN"
			RX2F4_Adjust.Main_SubText = ""


/* ───────────────────────────────────────────────────
	Custom Rules
─────────────────────────────────────────────────── */

	-- Custom Rule ( You may know what ' RXFR_Rule_AddTitle ' and ' RX2F4_Rule_AddText '  thing does )
		RX2F4_Rule_AddTitle("Rule")
		RX2F4_Rule_AddText[[ This is Rule 1
							Hmmmmmmm.....
							Dont Kill Ohter!
							Dont Spam!
							Have Fun!]]
		RX2F4_Rule_AddTitle("Rule 2")
		RX2F4_Rule_AddText[[ RocketMania's Black & Blue Style F4 Menu
							 Available at Coderhire.com
							Thank you!]]
							
							
/* ───────────────────────────────────────────────────
	Menu Order
─────────────────────────────────────────────────── */


RX2F4_Adjust.Menus = {}
	-- you can add or remove buttons.
	
		table.insert(RX2F4_Adjust.Menus,{
			N="Commands",P= function(PN) 
			return RX2F4_Open_MoneyCommand(PN) 
		end})
	
		table.insert(RX2F4_Adjust.Menus,{
			N="Jobs",P= function(PN)
			return RX2F4_Open_JobSelector(PN,false)  -- by setting this to ' false ' its NOT VIP Job
		end})
		
		--table.insert(RX2F4_Adjust.Menus,{
		--	N="VIP Jobs",P= function(PN)
		--	return RX2F4_Open_JobSelector(PN,true) -- by setting this to ' true ' its VIP Job
		--end})

        table.insert(RX2F4_Adjust.Menus,{
            N="Shop",P= function(PN)
                return RX2F4_Open_FilteredShop(PN)
            end})
		
		table.insert(RX2F4_Adjust.Menus,{
			N="Rules",P= function(PN)
			return RX2F4_Open_HTML(PN,"ttt-fun.com/motd/darkrp")
		end})

        table.insert(RX2F4_Adjust.Menus,{
            N="Donate",P= function(PN)
                gui.OpenURL("http://ttt-fun.com/donate/")
                return RX2F4_Open_HTML(PN,"ttt-fun.com/donate")
            end})
		

		
/* ───────────────────────────────────────────────────
	Colors  < RX Blue >
─────────────────────────────────────────────────── */
RX2F4_Adjust.Colors = {}


	-- Main
	RX2F4_Adjust.Colors.Main = {}
		
		-- Main
		RX2F4_Adjust.Colors.Main.TitleText = Color(220,220,0,255)
		RX2F4_Adjust.Colors.Main.SubTitleText = Color(220,220,0,200)
		RX2F4_Adjust.Colors.Main.Line = Color(152,156,160,100)
	
		-- Category Button
		RX2F4_Adjust.Colors.Main.CB_Text = Color(255,255,255,200)
		RX2F4_Adjust.Colors.Main.CB_Effect = Color(52,56,60,250)
        RX2F4_Adjust.Colors.Main.CB_Effect_active = Color(152,156,160,100)
		
		-- ScrollBar 
		RX2F4_Adjust.Colors.Main.SCB_Boarder = RX2F4_Adjust.Colors.Main.Line
		RX2F4_Adjust.Colors.Main.SCB_Inner = RX2F4_Adjust.Colors.Main.CB_Effect
		
		
		
		
	-- Command Menu
	RX2F4_Adjust.Colors.CM = {}
	
	RX2F4_Adjust.Colors.CM.TitleColor = Color(255,255,255,255)
	RX2F4_Adjust.Colors.CM.TitleLine = Color(0,150,255,255)


-- Money Actions
RX2F4_Adjust.Colors.CM.MA_ButtonBoarder = Color(255,255,0,20)
RX2F4_Adjust.Colors.CM.MA_ButtonText = Color(255,255,0,255)
RX2F4_Adjust.Colors.CM.MA_ButtonFX = Color(255,255,0,255)

-- DarkRP Actions
RX2F4_Adjust.Colors.CM.DA_ButtonBoarder = Color(255,120,0,20)
RX2F4_Adjust.Colors.CM.DA_ButtonText = Color(255,120,0,255)
RX2F4_Adjust.Colors.CM.DA_ButtonFX = Color(255,120,0,255)

-- Mayor Actions
RX2F4_Adjust.Colors.CM.MOA_ButtonBoarder = Color(255,255,0,20)
RX2F4_Adjust.Colors.CM.MOA_ButtonText = Color(255,255,0,255)
RX2F4_Adjust.Colors.CM.MOA_ButtonFX = Color(255,255,0,255)

-- Police Actions
RX2F4_Adjust.Colors.CM.PA_ButtonBoarder = Color(255,255,0,20)
RX2F4_Adjust.Colors.CM.PA_ButtonText = Color(255,255,0,255)
RX2F4_Adjust.Colors.CM.PA_ButtonFX = Color(255,255,0,255)




-- Job Menu
RX2F4_Adjust.Colors.JM = {}

RX2F4_Adjust.Colors.JM.TitleColor = Color(255,120,0,255)
RX2F4_Adjust.Colors.JM.TitleLine = Color(255,255,0,255)
RX2F4_Adjust.Colors.JM.Line = Color(255,100,0,20)
RX2F4_Adjust.Colors.JM.WeaponList = Color(255,200,0,200)
RX2F4_Adjust.Colors.JM.Salary_MaxText = Color(0,140,140,200)
RX2F4_Adjust.Colors.JM.HoverFX = Color( 180,180,180,10)

-- Job Hovering Panel
RX2F4_Adjust.Colors.JMHP = {}

RX2F4_Adjust.Colors.JMHP.Boarder = Color(255,120,0,255)
RX2F4_Adjust.Colors.JMHP.Description = Color(255,255,0,255)






-- Shop Menu
	RX2F4_Adjust.Colors.SM = {}
	
	RX2F4_Adjust.Colors.SM.TitleColor = Color(255,255,255,255)
	RX2F4_Adjust.Colors.SM.TitleLine = Color(0,150,255,255)
	
		-- Left Category
		RX2F4_Adjust.Colors.SM.LC_Text = Color(255,255,255,200)
		RX2F4_Adjust.Colors.SM.LC_Effect = Color(0,150,255,50)
		
		-- Item List
		RX2F4_Adjust.Colors.SM.IL_Text = Color(220,220,220,255)
		RX2F4_Adjust.Colors.SM.IL_Price = Color(255,255,0,155)
		RX2F4_Adjust.Colors.SM.IL_Line = Color(0,200,255,20)
		RX2F4_Adjust.Colors.SM.IL_ButtonFX = Color(0,150,255,20)
	
	
	
	
	-- HTML Menu
	RX2F4_Adjust.Colors.HM = {}
	
	RX2F4_Adjust.Colors.HM.AdressBarLine = Color(0,255,255,255)
	RX2F4_Adjust.Colors.HM.Button = Color(0,150,255,255)
	RX2F4_Adjust.Colors.HM.AdressText = Color(0,255,255,255)
	RX2F4_Adjust.Colors.HM.AdressTextButtomLine = Color(0,150,255,255)
	
	
	
	
	
	-- Rule Menu
	RX2F4_Adjust.Colors.RM = {}
	
	RX2F4_Adjust.Colors.RM.Title = Color(0,255,255,255)
	RX2F4_Adjust.Colors.RM.TitleLine = Color(0,150,255,255)
	RX2F4_Adjust.Colors.RM.Body = Color(0,220,255,100)
	