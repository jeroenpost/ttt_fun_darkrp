/* ───────────────────────────────────────────────────
	Colors  < Green & Blue Theme >
─────────────────────────────────────────────────── */
RX2F4_Adjust.Colors = {}


	-- Main
	RX2F4_Adjust.Colors.Main = {}
		
		-- Main
		RX2F4_Adjust.Colors.Main.TitleText = Color(0,255,0,255)
		RX2F4_Adjust.Colors.Main.SubTitleText = Color(0,255,0,255)
		RX2F4_Adjust.Colors.Main.Line = Color(0,255,0,255)
	
		-- Category Button
		RX2F4_Adjust.Colors.Main.CB_Text = Color(0,255,255,255)
		RX2F4_Adjust.Colors.Main.CB_Effect = Color(0,255,255,50)
		
		-- ScrollBar 
		RX2F4_Adjust.Colors.Main.SCB_Boarder = Color(0,255,255,255)
		RX2F4_Adjust.Colors.Main.SCB_Inner = Color(0,255,255,5)
		
		
		
		
	-- Command Menu
	RX2F4_Adjust.Colors.CM = {}
	
	RX2F4_Adjust.Colors.CM.TitleColor = Color(0,255,0,255)
	RX2F4_Adjust.Colors.CM.TitleLine = Color(0,255,255,255)
	
		-- Money Actions
		RX2F4_Adjust.Colors.CM.MA_ButtonBoarder = Color(0,255,255,20)
		RX2F4_Adjust.Colors.CM.MA_ButtonText = Color(0,255,255,255)
		RX2F4_Adjust.Colors.CM.MA_ButtonFX = Color(0,255,255,255)
	
		-- DarkRP Actions
		RX2F4_Adjust.Colors.CM.DA_ButtonBoarder = Color(0,255,0,20)
		RX2F4_Adjust.Colors.CM.DA_ButtonText = Color(0,255,0,255)
		RX2F4_Adjust.Colors.CM.DA_ButtonFX = Color(0,255,0,255)
	
		-- Mayor Actions
		RX2F4_Adjust.Colors.CM.MOA_ButtonBoarder = Color(0,255,255,20)
		RX2F4_Adjust.Colors.CM.MOA_ButtonText = Color(0,255,255,255)
		RX2F4_Adjust.Colors.CM.MOA_ButtonFX = Color(0,255,255,255)
	
		-- Police Actions
		RX2F4_Adjust.Colors.CM.PA_ButtonBoarder = Color(0,255,255,20)
		RX2F4_Adjust.Colors.CM.PA_ButtonText = Color(0,255,255,255)
		RX2F4_Adjust.Colors.CM.PA_ButtonFX = Color(0,255,255,255)
	
		
	
	-- Job Menu
	RX2F4_Adjust.Colors.JM = {}
	
	RX2F4_Adjust.Colors.JM.TitleColor = Color(0,255,0,255)
	RX2F4_Adjust.Colors.JM.TitleLine = Color(0,255,255,255)
	RX2F4_Adjust.Colors.JM.Line = Color(0,255,0,20)
	RX2F4_Adjust.Colors.JM.WeaponList = Color(0,255,255,200)
	RX2F4_Adjust.Colors.JM.Salary_MaxText = Color(0,255,255,200)
	RX2F4_Adjust.Colors.JM.HoverFX = Color(0,255,255,200)
	
		-- Job Hovering Panel
		RX2F4_Adjust.Colors.JMHP = {}
		
		RX2F4_Adjust.Colors.JMHP.Boarder = Color(0,255,0,255)
		RX2F4_Adjust.Colors.JMHP.Description = Color(0,255,0,255)
	
	
	
	
	
	
	-- Shop Menu
	RX2F4_Adjust.Colors.SM = {}
	
	RX2F4_Adjust.Colors.SM.TitleColor = Color(0,255,0,255)
	RX2F4_Adjust.Colors.SM.TitleLine = Color(0,255,255,255)
	
		-- Left Category
		RX2F4_Adjust.Colors.SM.LC_Text = Color(0,255,0,200)
		RX2F4_Adjust.Colors.SM.LC_Effect = Color(0,255,255,50)
		
		-- Item List
		RX2F4_Adjust.Colors.SM.IL_Text = Color(0,255,0,255)
		RX2F4_Adjust.Colors.SM.IL_Price = Color(0,255,255,255)
		RX2F4_Adjust.Colors.SM.IL_Line = Color(0,255,255,20)
		RX2F4_Adjust.Colors.SM.IL_ButtonFX = Color(0,255,255,20)
	
	
	
	
	-- HTML Menu
	RX2F4_Adjust.Colors.HM = {}
	
	RX2F4_Adjust.Colors.HM.AdressBarLine = Color(0,255,0,255)
	RX2F4_Adjust.Colors.HM.Button = Color(0,120,255,255)
	RX2F4_Adjust.Colors.HM.AdressText = Color(0,255,255,255)
	RX2F4_Adjust.Colors.HM.AdressTextButtomLine = Color(0,125,255,255)
	
	
	
	
	
	-- Rule Menu
	RX2F4_Adjust.Colors.RM = {}
	
	RX2F4_Adjust.Colors.RM.Title = Color(0,255,0,255)
	RX2F4_Adjust.Colors.RM.TitleLine = Color(0,255,255,255)
	RX2F4_Adjust.Colors.RM.Body = Color(0,255,150,100)
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
/* ───────────────────────────────────────────────────
	Colors  < Orange & Yellow >
─────────────────────────────────────────────────── */
RX2F4_Adjust.Colors = {}


	-- Main
	RX2F4_Adjust.Colors.Main = {}
		
		-- Main
		RX2F4_Adjust.Colors.Main.TitleText = Color(255,120,0,255)
		RX2F4_Adjust.Colors.Main.SubTitleText = Color(255,120,0,150)
		RX2F4_Adjust.Colors.Main.Line = Color(255,120,0,200)
	
		-- Category Button
		RX2F4_Adjust.Colors.Main.CB_Text = Color(255,255,0,255)
		RX2F4_Adjust.Colors.Main.CB_Effect = Color(255,120,0,50)
		
		-- ScrollBar 
		RX2F4_Adjust.Colors.Main.SCB_Boarder = Color(255,120,0,255)
		RX2F4_Adjust.Colors.Main.SCB_Inner = Color(255,120,0,5)
		
		
		
		
	-- Command Menu
	RX2F4_Adjust.Colors.CM = {}
	
	RX2F4_Adjust.Colors.CM.TitleColor = Color(255,120,0,255)
	RX2F4_Adjust.Colors.CM.TitleLine = Color(255,255,0,255)
	
		-- Money Actions
		RX2F4_Adjust.Colors.CM.MA_ButtonBoarder = Color(255,255,0,20)
		RX2F4_Adjust.Colors.CM.MA_ButtonText = Color(255,255,0,255)
		RX2F4_Adjust.Colors.CM.MA_ButtonFX = Color(255,255,0,255)
	
		-- DarkRP Actions
		RX2F4_Adjust.Colors.CM.DA_ButtonBoarder = Color(255,120,0,20)
		RX2F4_Adjust.Colors.CM.DA_ButtonText = Color(255,120,0,255)
		RX2F4_Adjust.Colors.CM.DA_ButtonFX = Color(255,120,0,255)
	
		-- Mayor Actions
		RX2F4_Adjust.Colors.CM.MOA_ButtonBoarder = Color(255,255,0,20)
		RX2F4_Adjust.Colors.CM.MOA_ButtonText = Color(255,255,0,255)
		RX2F4_Adjust.Colors.CM.MOA_ButtonFX = Color(255,255,0,255)
	
		-- Police Actions
		RX2F4_Adjust.Colors.CM.PA_ButtonBoarder = Color(255,255,0,20)
		RX2F4_Adjust.Colors.CM.PA_ButtonText = Color(255,255,0,255)
		RX2F4_Adjust.Colors.CM.PA_ButtonFX = Color(255,255,0,255)
	
		
	
	-- Job Menu
	RX2F4_Adjust.Colors.JM = {}
	
	RX2F4_Adjust.Colors.JM.TitleColor = Color(255,120,0,255)
	RX2F4_Adjust.Colors.JM.TitleLine = Color(255,255,0,255)
	RX2F4_Adjust.Colors.JM.Line = Color(255,100,0,20)
	RX2F4_Adjust.Colors.JM.WeaponList = Color(255,200,0,200)
	RX2F4_Adjust.Colors.JM.Salary_MaxText = Color(255,255,0,200)
	RX2F4_Adjust.Colors.JM.HoverFX = Color(255,255,0,10)
	
		-- Job Hovering Panel
		RX2F4_Adjust.Colors.JMHP = {}
		
		RX2F4_Adjust.Colors.JMHP.Boarder = Color(255,120,0,255)
		RX2F4_Adjust.Colors.JMHP.Description = Color(255,255,0,255)
	
	
	
	
	
	
	-- Shop Menu
	RX2F4_Adjust.Colors.SM = {}
	
	RX2F4_Adjust.Colors.SM.TitleColor = Color(255,120,0,255)
	RX2F4_Adjust.Colors.SM.TitleLine = Color(255,255,0,255)
	
		-- Left Category
		RX2F4_Adjust.Colors.SM.LC_Text = Color(255,120,0,200)
		RX2F4_Adjust.Colors.SM.LC_Effect = Color(255,255,0,50)
		
		-- Item List
		RX2F4_Adjust.Colors.SM.IL_Text = Color(255,120,0,255)
		RX2F4_Adjust.Colors.SM.IL_Price = Color(255,255,0,255)
		RX2F4_Adjust.Colors.SM.IL_Line = Color(255,200,0,20)
		RX2F4_Adjust.Colors.SM.IL_ButtonFX = Color(255,120,0,20)
	
	
	
	
	-- HTML Menu
	RX2F4_Adjust.Colors.HM = {}
	
	RX2F4_Adjust.Colors.HM.AdressBarLine = Color(255,120,0,255)
	RX2F4_Adjust.Colors.HM.Button = Color(255,255,0,255)
	RX2F4_Adjust.Colors.HM.AdressText = Color(255,50,0,255)
	RX2F4_Adjust.Colors.HM.AdressTextButtomLine = Color(255,255,0,255)
	
	
	
	
	
	-- Rule Menu
	RX2F4_Adjust.Colors.RM = {}
	
	RX2F4_Adjust.Colors.RM.Title = Color(255,120,0,255)
	RX2F4_Adjust.Colors.RM.TitleLine = Color(255,255,0,255)
	RX2F4_Adjust.Colors.RM.Body = Color(255,150,0,100)
	
	
	
	
/* ───────────────────────────────────────────────────
	Colors  < RX Blue >
─────────────────────────────────────────────────── */
RX2F4_Adjust.Colors = {}


	-- Main
	RX2F4_Adjust.Colors.Main = {}
		
		-- Main
		RX2F4_Adjust.Colors.Main.TitleText = Color(0,255,255,255)
		RX2F4_Adjust.Colors.Main.SubTitleText = Color(0,220,255,200)
		RX2F4_Adjust.Colors.Main.Line = Color(0,150,255,200)
	
		-- Category Button
		RX2F4_Adjust.Colors.Main.CB_Text = Color(0,255,255,255)
		RX2F4_Adjust.Colors.Main.CB_Effect = Color(0,120,255,50)
		
		-- ScrollBar 
		RX2F4_Adjust.Colors.Main.SCB_Boarder = Color(0,200,255,255)
		RX2F4_Adjust.Colors.Main.SCB_Inner = Color(0,200,255,5)
		
		
		
		
	-- Command Menu
	RX2F4_Adjust.Colors.CM = {}
	
	RX2F4_Adjust.Colors.CM.TitleColor = Color(0,255,255,255)
	RX2F4_Adjust.Colors.CM.TitleLine = Color(0,150,255,255)
	
		-- Money Actions
		RX2F4_Adjust.Colors.CM.MA_ButtonBoarder = Color(0,255,255,20)
		RX2F4_Adjust.Colors.CM.MA_ButtonText = Color(0,255,255,255)
		RX2F4_Adjust.Colors.CM.MA_ButtonFX = Color(0,255,255,255)
	
		-- DarkRP Actions
		RX2F4_Adjust.Colors.CM.DA_ButtonBoarder = Color(0,120,255,20)
		RX2F4_Adjust.Colors.CM.DA_ButtonText = Color(0,120,255,255)
		RX2F4_Adjust.Colors.CM.DA_ButtonFX = Color(0,120,255,255)
	
		-- Mayor Actions
		RX2F4_Adjust.Colors.CM.MOA_ButtonBoarder = Color(0,255,255,20)
		RX2F4_Adjust.Colors.CM.MOA_ButtonText = Color(0,255,255,255)
		RX2F4_Adjust.Colors.CM.MOA_ButtonFX = Color(0,255,255,255)
	
		-- Police Actions
		RX2F4_Adjust.Colors.CM.PA_ButtonBoarder = Color(0,255,255,20)
		RX2F4_Adjust.Colors.CM.PA_ButtonText = Color(0,255,255,255)
		RX2F4_Adjust.Colors.CM.PA_ButtonFX = Color(0,255,255,255)
	
		
	
	-- Job Menu
	RX2F4_Adjust.Colors.JM = {}
	
	RX2F4_Adjust.Colors.JM.TitleColor = Color(0,255,255,255)
	RX2F4_Adjust.Colors.JM.TitleLine = Color(0,150,255,255)
	RX2F4_Adjust.Colors.JM.Line = Color(0,100,255,20)
	RX2F4_Adjust.Colors.JM.WeaponList = Color(0,200,200,200)
	RX2F4_Adjust.Colors.JM.Salary_MaxText = Color(0,255,255,200)
	RX2F4_Adjust.Colors.JM.HoverFX = Color(0,150,255,10)
	
		-- Job Hovering Panel
		RX2F4_Adjust.Colors.JMHP = {}
		
		RX2F4_Adjust.Colors.JMHP.Boarder = Color(0,255,255,255)
		RX2F4_Adjust.Colors.JMHP.Description = Color(0,150,255,255)
	
	
	
	
	
	
	-- Shop Menu
	RX2F4_Adjust.Colors.SM = {}
	
	RX2F4_Adjust.Colors.SM.TitleColor = Color(0,255,255,255)
	RX2F4_Adjust.Colors.SM.TitleLine = Color(0,150,255,255)
	
		-- Left Category
		RX2F4_Adjust.Colors.SM.LC_Text = Color(0,255,255,200)
		RX2F4_Adjust.Colors.SM.LC_Effect = Color(0,150,255,50)
		
		-- Item List
		RX2F4_Adjust.Colors.SM.IL_Text = Color(0,255,255,255)
		RX2F4_Adjust.Colors.SM.IL_Price = Color(0,150,255,255)
		RX2F4_Adjust.Colors.SM.IL_Line = Color(0,200,255,20)
		RX2F4_Adjust.Colors.SM.IL_ButtonFX = Color(0,150,255,20)
	
	
	
	
	-- HTML Menu
	RX2F4_Adjust.Colors.HM = {}
	
	RX2F4_Adjust.Colors.HM.AdressBarLine = Color(0,255,255,255)
	RX2F4_Adjust.Colors.HM.Button = Color(0,150,255,255)
	RX2F4_Adjust.Colors.HM.AdressText = Color(0,255,255,255)
	RX2F4_Adjust.Colors.HM.AdressTextButtomLine = Color(0,150,255,255)
	
	
	
	
	
	-- Rule Menu
	RX2F4_Adjust.Colors.RM = {}
	
	RX2F4_Adjust.Colors.RM.Title = Color(0,255,255,255)
	RX2F4_Adjust.Colors.RM.TitleLine = Color(0,150,255,255)
	RX2F4_Adjust.Colors.RM.Body = Color(0,220,255,100)
	