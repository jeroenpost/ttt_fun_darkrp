if CLIENT then
    local Menus = {} -- DONT TOUCH THIS

    local TB2Insert = {}
    TB2Insert.FilterName = "jobs_general"
    TB2Insert.PrintName = "General"
    TB2Insert.VIP = false
    table.insert(Menus,TB2Insert)

    local TB2Insert = {}
    TB2Insert.FilterName = "jobs_animal"
    TB2Insert.PrintName = "Animals"
    TB2Insert.VIP = false
    table.insert(Menus,TB2Insert)

    local TB2Insert = {}
    TB2Insert.FilterName = "jobs_criminals"
    TB2Insert.PrintName = "Criminals"
    TB2Insert.VIP = false
    table.insert(Menus,TB2Insert)

    local TB2Insert = {}
    TB2Insert.FilterName = "jobs_government"
    TB2Insert.PrintName = "Government"
    TB2Insert.VIP = false
    table.insert(Menus,TB2Insert)

    local TB2Insert = {}
    TB2Insert.FilterName = "jobs_dealers"
    TB2Insert.PrintName = "Dealers"
    TB2Insert.VIP = false
    table.insert(Menus,TB2Insert)

    local TB2Insert = {}
    TB2Insert.FilterName = "jobs_heroes"
    TB2Insert.PrintName = "Heroes / Villains"
    TB2Insert.VIP = false
    table.insert(Menus,TB2Insert)

    local TB2Insert = {}
    TB2Insert.FilterName = "jobs_special"
    TB2Insert.PrintName = "Speciality"
    TB2Insert.VIP = false
    table.insert(Menus,TB2Insert)

    local TB2Insert = {}
    TB2Insert.FilterName = "jobs_vip"
    TB2Insert.PrintName = "VIP Jobs"
    TB2Insert.VIP = true
    table.insert(Menus,TB2Insert)

    local TB2Insert = {}
    TB2Insert.FilterName = "jobs_vipp"
    TB2Insert.PrintName = "VIP+ Jobs"
    TB2Insert.VIP = true
    table.insert(Menus,TB2Insert)

    local TB2Insert = {}
    TB2Insert.FilterName = "jobs_vippp"
    TB2Insert.PrintName = "VIP++"
    TB2Insert.VIP = true
    table.insert(Menus,TB2Insert)

    local TB2Insert = {}
    TB2Insert.FilterName = "jobs_admin"
    TB2Insert.PrintName = "Admin"
    TB2Insert.VIP = true
    table.insert(Menus,TB2Insert)

local PANEL = {}

function RX2F4_Open_JobSelector(Parent,VIPMode)
	GAMEMODE.ConnectedPlayersPanel = vgui.Create("RX2F4_M_JobSelector",Parent)
	GAMEMODE.ConnectedPlayersPanel:SetSize(Parent:GetWide(),Parent:GetTall())
	GAMEMODE.ConnectedPlayersPanel.VIPMode = VIPMode or false
	return GAMEMODE.ConnectedPlayersPanel
end
function PANEL:Init()

end
    function PANEL:Initialize()
    end

    function PANEL:Paint()

end

function PANEL:Install()
   -- self:SetDraggable(false)
    --self:ShowCloseButton(false)
    --self:SetTitle(" ")

    self.HasParent = HasParent
    self.TopLabel = vgui.Create( "DPanel" , self)
    self.TopLabel:SetPos(2,2)
    self.TopLabel:SetSize( self:GetWide(),40 )
    self.TopLabel.Paint = function(slf)
        surface.SetDrawColor( RX2F4_Adjust.Colors.SM.TitleLine )
        surface.DrawRect( 1, 39, slf:GetWide()-2, 1 )
        draw.SimpleText("Jobs", "RXF2_TrebOut_S40", 20,20, RX2F4_Adjust.Colors.SM.TitleColor, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
    end

    self.FilterList = vgui.Create("DPanelList", self)
    self.FilterList:SetPos(10,50)
    self.FilterList:SetSize(180,self:GetTall() - 60)
    self.FilterList:SetSpacing(5);
    self.FilterList:SetPadding(0);
    self.FilterList:EnableVerticalScrollbar(true);
    self.FilterList:EnableHorizontal(true);
    self.FilterList:RX2F4_PaintListBarC()
    self.FilterList.Paint = function(slf)
        surface.SetDrawColor( 0,0,0,50 )
        surface.DrawRect( 0, 0, slf:GetWide(), slf:GetTall() )
    end

    for k,v in pairs(Menus) do
        local SButton = vgui.Create( "RX2F4_DSWButton" )
        SButton:SetSize( self.FilterList:GetWide() , 30 )
        SButton.BoarderCol = Color(0,0,0,0)
        SButton.FXCol = RX2F4_Adjust.Colors.SM.LC_Effect
        SButton.TextCol = RX2F4_Adjust.Colors.SM.LC_Text
        SButton:SetTexts( v.PrintName )
        SButton:SetTextAlign(TEXT_ALIGN_LEFT,TEXT_ALIGN_CENTER)
        SButton.PaintBackGround = function(slf)
            if self.CurFilter == v.FilterName then
                surface.SetDrawColor( RX2F4_Adjust.Colors.SM.LC_Effect )
                surface.DrawRect( 0, 0, slf:GetWide(), slf:GetTall() )
            end
        end
        SButton.Click = function(slf)
            self:UpdateList(v.FilterName,v)
        end
        self.FilterList:AddItem(SButton)
    end
	
	self.PlayerList = vgui.Create("DPanelList", self)
		self.PlayerList:SetPos(190,50)
		self.PlayerList:SetSize(self:GetWide()-580,self:GetTall()-60 )
		self.PlayerList:SetSpacing(8);
		self.PlayerList:SetPadding(0);
		self.PlayerList:EnableVerticalScrollbar(true);
		self.PlayerList:EnableHorizontal(false);
		self.PlayerList:RX2F4_PaintListBarC()
		self.PlayerList.Paint = function(slf)
			surface.SetDrawColor( 0,0,0,50 )
			surface.DrawRect( 0, 0, slf:GetWide(), slf:GetTall() )
		end
		
	self.JobInfoLister = vgui.Create("DPanelList", self)
		self.JobInfoLister:SetPos(10+self.JobInfoLister:GetWide()+5,50)
		self.JobInfoLister:SetSize(1,self:GetTall()-70 )
		self.JobInfoLister:SetSpacing(8);
		self.JobInfoLister:SetPadding(0);
		self.JobInfoLister:EnableVerticalScrollbar(true);
		self.JobInfoLister:EnableHorizontal(false);
		self.JobInfoLister:RX2F4_PaintListBarC()
		self.JobInfoLister.Paint = function(slf)
			surface.SetDrawColor( 0,0,0,50 )
			surface.DrawRect( 0, 0, slf:GetWide(), slf:GetTall() )
        end

    self.JobInfo = vgui.Create("DPanel", self)
    self.JobInfo:SetPos(self:GetWide()-380,50)
    self.JobInfo:SetSize(360,self:GetTall()-60 )

    self.JobInfo.Paint = function(slf)
        surface.SetDrawColor( 90,0,0,50 )
        surface.DrawRect( 0, 0, slf:GetWide(), slf:GetTall() )
    end
		
	
		
	self:UpdateList()
end

function PANEL:GetJobInfo(Count,Model, name, description, Weapons, command, special, specialcommand, IconModel, JTB)
    self.JobInfo:Clear()
    local Info = self.JobInfo

    local jof = 0
    local eof = 50

    surface.SetFont( "RXF2_Treb_S50" )
    local wi, hi = surface.GetTextSize(name)
    if wi > 450 then
        jof = 30
    elseif wi > 350 then
        jof = 25
    elseif wi > 270  then
        jof = 10
    end

    local BGP2 = vgui.Create("DPanel", Info)
    BGP2:SetSize(Info:GetWide()-20,80)
    BGP2:SetPos(10,10)

    BGP2.Paint = function(slf)
        surface.SetDrawColor( Color(105,0,0,255) )
        surface.DrawRect( 0, 0, self:GetWide()-20, self:GetTall()-20 )
        draw.SimpleTextOutlined(name, "RXF2_Treb_S"..(eof-jof), 10,52+(jof/3), ( Color(200,200,200,255)),TEXT_ALIGN_LEFT,TEXT_ALIGN_BOTTOM,1,Color(0,0,0,255))
    end

    local weps = "";
    if JTB.weapons and #JTB.weapons > 0 then
        weps = "\n\nWeapons:"
        for k,v in pairs(JTB.weapons or {}) do
            weps = weps.."\n- " .. v
        end
    end
    local LABEL = vgui.Create("DTextEntry",Info)
    LABEL:SetText(description..""..weps)
    LABEL:SetWrap(true)
   -- LABEL:SetColor(RX2F4_Adjust.Colors.JMHP.Description)
    LABEL:SetSize(Info:GetWide()-20, 268)
    LABEL:SetPos(10,150)
    LABEL:SetDrawBackground(false)
    LABEL:SetTextColor(Color(255, 255, 255, 255));
    LABEL:SetMultiline(true)

    local PMD = vgui.Create( "Login_PlayerModelDrawer", Info )
    PMD:SetPos( 280,0)
    PMD:SetSize( 75, 175 )
    PMD:SetUp()
    PMD.PMEntity:SetModel(IconModel)


    local BGP = vgui.Create("RX2F4_DSWButton", Info)
    BGP:SetSize(Info:GetWide()-20,45)
    BGP:SetPos(10,Info:GetTall() -50)
    BGP:SetBoarderColor(Color(255,255,255,255))
    BGP.FXCol = RX2F4_Adjust.Colors.JM.HoverFX
    local text  ="Become Job"
  --  if self.VIPMode and !RX2F4VIPCheck(LocalPlayer()) then
  --      text = "You don't have the required rank"
  --  end
    BGP.PaintOverlay = function(slf)
        surface.SetDrawColor( Color(105,0,0,255) )
        surface.DrawRect( 0, 0, self:GetWide()-20, self:GetTall()-20 )
        draw.SimpleTextOutlined(text, "RXF2_Treb_S30", 95,38, ( Color(200,200,200,255)),TEXT_ALIGN_LEFT,TEXT_ALIGN_BOTTOM,1,Color(0,0,0,255))
    end
    BGP.Click = function(slf)
        --if self.VIPMode and !RX2F4VIPCheck(LocalPlayer()) then return end
        local function DoChatCommand(frame)
            if special then
                local menu = DermaMenu()
                menu:AddOption("Vote" , function() RunConsoleCommand("darkrp","vote" .. command) frame:Close() end)
                menu:AddOption("Do not vote", function() RunConsoleCommand("darkrp",command) frame:Close() end)
                menu:Open()
            else
                if JTB.vote then
                    RunConsoleCommand("darkrp","vote" .. command)
                else
                    RunConsoleCommand("darkrp",command)
                end

                frame:Close()
            end
        end

        if type(Model) == "table" and #Model > 0 then
            local frame = vgui.Create("DFrame")
            frame:SetTitle("Choose model")
            frame:SetVisible(true)
            frame:MakePopup()
            frame.Paint = function(slf)
                surface.SetDrawColor( 0,0,0,200 )
                surface.DrawRect( 0, 0, slf:GetWide(), slf:GetTall() )
                slf:RX2F4_DrawBoarder()
            end

            local levels = 1
            local IconsPerLevel = math.floor(ScrW()/64)

            while #Model * (64/levels) > ScrW() do
                levels = levels + 1
            end
            frame:SetSize(math.Min(#Model * 64, IconsPerLevel*64), math.Min(90+(64*(levels-1)), ScrH()))
            frame:Center()

            local CurLevel = 1
            for k,v in pairs(Model) do
                local icon = vgui.Create("SpawnIcon", frame)
                if (k-IconsPerLevel*(CurLevel-1)) > IconsPerLevel then
                    CurLevel = CurLevel + 1
                end
                icon:SetPos((k-1-(CurLevel-1)*IconsPerLevel) * 64, 25+(64*(CurLevel-1)))
                icon:SetModel(v)
                icon:SetSize(64, 64)
                icon:SetToolTip()
                icon.DoClick = function()
                    DarkRP.setPreferredJobModel(JTB.team, v)
                    DoChatCommand(frame)
                end
            end
        else
            if special then
                local menu = DermaMenu()
                menu:AddOption("Vote", function() RunConsoleCommand("darkrp","vote"..command) end)
                menu:AddOption("Do not vote", function() RunConsoleCommand("darkrp",command) end)
                menu:Open()
            else
                if JTB.vote then
                    RunConsoleCommand("darkrp","vote" .. command)
                else
                    RunConsoleCommand("darkrp",command)
                end
            end
        end
    end

    -- self.JobInfo:AddItem(IP)
end


function PANEL:UpdateList(filter,DB)
    self.CurFilter = filter
    if not filter then filter = "jobs_general" end
	self.PlayerList:Clear()
    self.JobInfo:Clear()
    local JobInfo = self.JobInfo

	local List = self.PlayerList
		local function AddIcon(Count,Model, name, description, Weapons, command, special, specialcommand)
			local JTB = {}
			local PlayerCount = 0
			for a,b in pairs(RPExtraTeams) do
				if b.name == name then
					for k,v in pairs(player.GetAll()) do
						if v:Team() == a then
							PlayerCount = PlayerCount + 1
						end
					end
					JTB = b
                    JTB.team = a
				end
			end
			
			local BGP = vgui.Create("RX2F4_DSWButton")
			BGP:SetSize(List:GetWide(),70)
			BGP:SetBoarderColor(Color(0,0,0,0))
			BGP.FXCol = RX2F4_Adjust.Colors.JM.HoverFX
			BGP.PaintOverlay = function(slf)
				surface.SetDrawColor( RX2F4_Adjust.Colors.JM.Line )
				surface.DrawRect( 1, slf:GetTall()-1, slf:GetWide()-2, 1 )
				draw.SimpleText(name, "RXF2_Treb_S30", 80,28, ( Color(255,255,255,255)),TEXT_ALIGN_LEFT,TEXT_ALIGN_BOTTOM,1,Color(0,0,0,255))
				draw.SimpleText("Salary : $" .. JTB.salary, "RXF2_TrebOut_S20", 90,25, RX2F4_Adjust.Colors.JM.Salary_MaxText)
				draw.SimpleText("Max : " .. PlayerCount .. " / " .. JTB.max, "RXF2_TrebOut_S20", 90,45, RX2F4_Adjust.Colors.JM.Salary_MaxText)

                if JTB.vipName then
                     draw.SimpleText("Vip Level : " .. JTB.vipName, "RXF2_TrebOut_S20", 290,25, Color(0,150,0,255))
                end
                local level = JTB.level or 0;

                if level > 0 then
                      local colo = Color(0,150,0,255)
                      if LocalPlayer():getDarkRPVar('level') < level then
                          colo = Color(150,0,0,255)
                      end
                     draw.SimpleText("Level Needed : " ..level, "RXF2_TrebOut_S20", 265,45, colo)
                end


            end
			BGP:RX2F4_PanelAnim_Fade({Speed=0.2,Delay=Count/20,Fade=10})
			

			
			local icon = vgui.Create("ModelImage",BGP)
			icon:SetSize(BGP:GetTall(),BGP:GetTall())
			local IconModel = Model
			if type(Model) == "table" then
				IconModel = Model[math.random(#Model)]
			end
			icon:SetModel(IconModel)
                 BGP.Click = function(slf)

                     self:GetJobInfo(Count,Model, name, description, Weapons, command, special, specialcommand, IconModel, JTB)
					end

				self.PlayerList:AddItem(BGP)
		end
	
		local JCount = 0
	
		for k,v in ipairs(RPExtraTeams) do
            if !v[filter] then continue end
			if LocalPlayer():Team() ~= k and GAMEMODE:CustomObjFitsMap(v) then
				local nodude = true
				
				
				if self.VIPMode and !v.VIPOnly then 
				--	nodude = false
				end
				
				if !self.VIPMode and v.VIPOnly then 
				--	nodude = false
				end
				
				if v.admin == 1 and not LocalPlayer():IsAdmin() then
					nodude = false
				end
				if v.admin > 1 and not LocalPlayer():IsSuperAdmin() then
					nodude = false
				end
				if v.customCheck and not v.customCheck(LocalPlayer()) then
					nodude = false
				end

				if (type(v.NeedToChangeFrom) == "number" and LocalPlayer():Team() ~= v.NeedToChangeFrom) or (type(v.NeedToChangeFrom) == "table" and not table.HasValue(v.NeedToChangeFrom, LocalPlayer():Team())) then
					nodude = false
				end

				if nodude then
					local weps = "no extra weapons"
					if #v.weapons > 0 then
						weps = table.concat(v.weapons, "\n")
					end
					if (not v.RequiresVote and v.vote) or (v.RequiresVote and v.RequiresVote(LocalPlayer(), k)) then
						local condition = ((v.admin == 0 and LocalPlayer():IsAdmin()) or (v.admin == 1 and LocalPlayer():IsSuperAdmin()) or LocalPlayer().DarkRPVars["Priv"..v.command])
						if not v.model or not v.name or not v.description or not v.command then chat.AddText(Color(255,0,0,255), "Incorrect team! Fix your shared.lua!") return end
						JCount = JCount + 1
						AddIcon(JCount,v.model, v.name, v.description, weps,v.command, condition,v.command)
					else
						if not v.model or not v.name or not v.description or not v.command then chat.AddText(Color(255,0,0,255), "Incorrect team! Fix your shared.lua!") return end
						JCount = JCount + 1
						AddIcon(JCount,v.model, v.name, v.description, weps,v.command)
					end
				end
			end
		end
	
	
end
vgui.Register("RX2F4_M_JobSelector",PANEL,"DPanel")

end