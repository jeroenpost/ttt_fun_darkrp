include("shared.lua")

function ARMORY_RestartCooldown( um )
	LocalPlayer().RobberyCooldown = CurTime() + um:ReadLong()
end
usermessage.Hook("ARMORY_RestartCooldown", ARMORY_RestartCooldown)

function ARMORY_KillCooldown() 
	LocalPlayer().RobberyCooldown = 0
end
usermessage.Hook("ARMORY_KillCooldown", ARMORY_KillCooldown)

function ARMORY_RestartCountdown( um )
	LocalPlayer().RobberyCountdown = CurTime() + um:ReadLong()
end
usermessage.Hook("ARMORY_RestartTimer", ARMORY_RestartCountdown)

function ARMORY_KillCountdown() 
	LocalPlayer().RobberyCountdown = 0
end
usermessage.Hook("ARMORY_KillTimer", ARMORY_KillCountdown)

function ENT:Initialize()
end

function ENT:Draw()
	self:DrawModel()
    if self.Entity:GetPos():Distance(LocalPlayer():GetPos()) < 800 then
	--local pos = self:GetPos() + Vector(0, 0, 1) * math.sin(CurTime() * 2) * 2
	local PlayersAngle = LocalPlayer():GetAngles()
	local ang = self:GetAngles()
	
	ang:RotateAroundAxis(ang:Right(), -90)
	ang:RotateAroundAxis(ang:Up(), 90)

    local pos = self:LocalToWorld(Vector(9, 0, 0))
    local angle = self:GetAngles()

	cam.Start3D2D(pos, ang, 1)
		if LocalPlayer().RobberyCooldown and LocalPlayer().RobberyCooldown > CurTime() then
			draw.SimpleTextOutlined("Robbery Cooldown", "ARMORY_ScreenText", 0, -115, ARMORY_DESIGN_CooldownTextColor, 1, 1, 1.5, ARMORY_DESIGN_CooldownTextBoarder)
			draw.SimpleTextOutlined(string.ToMinutesSeconds(math.Round(LocalPlayer().RobberyCooldown - CurTime())), "UiSmallerThanBold", 0, -100, ARMORY_DESIGN_CooldownTimerTextColor, 1, 1, 1.5, ARMORY_DESIGN_CooldownTimerTextBoarder)
		end
		if LocalPlayer().RobberyCountdown and LocalPlayer().RobberyCountdown > CurTime() then
			draw.SimpleTextOutlined("Robbery Countdown", "ARMORY_ScreenText", 0, -115, ARMORY_DESIGN_CountdownTextColor, 1, 1, 1.5, ARMORY_DESIGN_CountdownTextBoarder)
			draw.SimpleTextOutlined(string.ToMinutesSeconds(math.Round(LocalPlayer().RobberyCountdown - CurTime())), "UiSmallerThanBold", 0, -100, ARMORY_DESIGN_CountdownTimerTextColor, 1, 1, 1.5, ARMORY_DESIGN_CountdownTimerTextBoarder)
		end
		draw.SimpleTextOutlined("Police Armory", "ARMORY_ScreenText", 0, -80, ARMORY_DESIGN_ArmoryTextColor, 1, 1, 1.5, ARMORY_DESIGN_ArmoryTextBoarder)
    cam.End3D2D()
    end
end

local screenmodel = ClientsideModel("models/props/cs_office/TV_plasma.mdl", RENDERGROUP_TRANSLUCENT)
screenmodel:SetNoDraw(true)

local function DrawArmoryScreen() // front 1 back 2
	local armory = ents.FindByClass("police_armory")
	local scale = Vector( 1, 1, 1 )

	local mat = Matrix()
	mat:Scale(scale)
	screenmodel:EnableMatrix("RenderMultiply", mat)
	
	local eyePos = EyePos()
	
	local ArmoryMoneyAmount = ARMORY_MONEY_Max
	local ArmoryAmmoAmount = ARMORY_AMMO_Max
	local ArmoryShipmentAmount = ARMORY_SHIPMENTS_Max
	
	for i=1, #armory do
		if (armory[i]:GetPos()-eyePos):Length2DSqr() < 5923535 then
			
			local ang = armory[i]:GetAngles()
			ang:RotateAroundAxis(ang:Up(), 90)
			local textpos = armory[i]:GetPos() + ang:Right() * 15 + ang:Up() * 52
			local screenpos = armory[i]:GetPos() + ang:Right() * 10 + ang:Up() * 15

			ang:RotateAroundAxis(ang:Forward(), 90)

			local armoryangle = armory[i]:GetAngles()
			armoryangle:RotateAroundAxis(armoryangle:Forward(), 180)
			armoryangle:RotateAroundAxis(armoryangle:Forward(), 180)
			armoryangle:RotateAroundAxis(armoryangle:Right(), 90)
			
			local screenangle = armory[i]:GetAngles()
			screenmodel:SetRenderOrigin(screenpos)
			screenmodel:SetRenderAngles(screenangle)
			screenmodel:SetupBones()
			screenmodel:DrawModel()
			
			textpos = textpos + ang:Up()
			cam.Start3D2D(textpos, ang, 0.5)
				render.PushFilterMin(TEXFILTER.ANISOTROPIC)
					draw.RoundedBoxEx( 4, -60, 0, 117, 69, ARMORY_DESIGN_ScreenColor, false, false, false, false )
				
					draw.SimpleTextOutlined("Money: $".. util.RobberyFormatNumber(ArmoryMoneyAmount), "ARMORY_ScreenText", 0, 15, ARMORY_DESIGN_MoneyTextColor, 1, 1, 1, ARMORY_DESIGN_MoneyTextBoarder)
					
					draw.SimpleTextOutlined("Ammo: x".. util.RobberyFormatNumber(ArmoryAmmoAmount), "ARMORY_ScreenText", 0, 35, ARMORY_DESIGN_AmmoTextColor, 1, 1, 1, ARMORY_DESIGN_AmmoTextBoarder)
					
					draw.SimpleTextOutlined("Shipments: x".. util.RobberyFormatNumber(ArmoryShipmentAmount), "ARMORY_ScreenText", 0, 55, ARMORY_DESIGN_ShipmentsTextColor, 1, 1, 1, ARMORY_DESIGN_ShipmentsTextBoarder)
				render.PopFilterMin()
			cam.End3D2D()

		end
	end
end
hook.Add("PostDrawOpaqueRenderables", "DrawArmoryScreen", DrawArmoryScreen)