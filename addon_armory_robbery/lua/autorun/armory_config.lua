-- Section one. Handles money in the armory.
ARMORY_MONEY_MoneyTimer = 60 -- This is the time that defines when money is added to the armory. In seconds! [Default = 60 (1 Minute)]
ARMORY_MONEY_MoneyOnTime = 100 -- This is the amount of money to be added to the armory every x minutes/seconds. Defined by the setting above. [Default = 100]
ARMORY_MONEY_Max = 15000 -- The maximum amount of money the armory can have. Set to 0 for no limit. [Default = 15000]

-- Section two. Handles the ammonition part.
ARMORY_AMMO_AmmoTimer = 240 -- This is the time that defines when ammo is added to the armory. In seconds! [Default = 240 (4 Minutes)]
ARMORY_AMMO_AmmoOnTime = 5 -- This is the amount of ammo to be added to the armory every x minutes/seconds. Defined by the setting above. [Default = 5]
ARMORY_AMMO_Max = 100 -- The maximum amount of ammo the armory can have. Set to 0 for no limit. [Default = 100]

-- Section tree. Handles the shitment part.
ARMORY_SHIPMENTS_ShipmentsTimer = 600 -- This is the time that defines when shipments are added to the armory. In seconds! [Default = 360 (6 Minutes)]
ARMORY_SHIPMENTS_ShipmentsOnTime = 1 -- This is the amount of shipments to be added to the armory every x minutes/seconds. Defined by the setting above. [Default = 1]
ARMORY_SHIPMENTS_Max = 5 -- The maximum amount of shipments the armory can have. Set to 0 for no limit. [Default = 5]
ARMORY_SHIPMENTS_Amount = 2 -- Amount of weapons inside of one shipment. [Default = 10]

-- General settings.
ARMORY_Custom_AliveTime = 2 -- The amount of MINUTES the player must stay alive before he will receive what the armory has. IN MINUTES! [Default = 5]
ARMORY_Custom_CooldownTime = 10 -- The amount of MINUTES the armory is on a cooldown after a robbery! (Doesn't matter if the robbery failed or not) [Default = 20]
ARMORY_Custom_RobberyDistance = 1500 -- The amount of space the player can move away from the armory entity, before the robbery fails. [Default = 500]
ARMORY_Custom_PlayerLimit = 5 -- The amount of players there must be on the server before you can rob the armory. [Default = 5]
ARMORY_Custom_KillReward = 1000 -- The amount of money a person is rewarded for killing the armory robber. [Default = 1000]
ARMORY_Custom_PoliceRequired = 3 -- The amount of police officers there must be before a person can rob the armory. [Default = 3]

RequiredTeams = { -- These are the names of the jobs that counts with the option above, the police required. The amount of players on these teams are calcuated together in the count.
	"Civil Protection",
	"Civil Protection Chief", "S.W.A.T" -- THE LAST LINE SHOULD NOT HAVE A COMMA AT THE END. BE AWARE OF THIS WHEN EDITING THIS!
}

GovernmentTeams = { -- These are the teams that will receive a notify when a player is trying to rob the armory. Use the actual team name, as shown below.
	"Civil Protection",
	"Civil Protection Chief",
	 "S.W.A.T" -- THE LAST LINE SHOULD NOT HAVE A COMMA AT THE END. BE AWARE OF THIS WHEN EDITING THIS!
}

AllowedTeams = { -- These are the teams that are allowed to rob the bank.

    "Bank Robber",
    "Joker","Venom","Mafia", "Mafia Boss" -- THE LAST LINE SHOULD NOT HAVE A COMMA AT THE END. BE AWARE OF THIS WHEN EDITING THIS!
}

-- Design options for the armory entity display.
ARMORY_DESIGN_ArmoryTextColor = Color(60, 60, 60, 255)
ARMORY_DESIGN_ArmoryTextBoarder = Color(0,0,0,255)

ARMORY_DESIGN_CooldownTextColor = Color(60, 60, 60, 255)
ARMORY_DESIGN_CooldownTextBoarder = Color(0,0,0,255)

ARMORY_DESIGN_CountdownTextColor = Color(60, 60, 60, 255)
ARMORY_DESIGN_CountdownTextBoarder = Color(0,0,0,255)

ARMORY_DESIGN_CooldownTimerTextColor = Color(150, 150, 150, 255)
ARMORY_DESIGN_CooldownTimerTextBoarder = Color(0,0,0,255)

ARMORY_DESIGN_CountdownTimerTextColor = Color(150, 150, 150, 255)
ARMORY_DESIGN_CountdownTimerTextBoarder = Color(0,0,0,255)

ARMORY_DESIGN_ScreenColor = Color( 60, 60, 60, 200 )

ARMORY_DESIGN_MoneyTextColor = Color(60, 100, 60, 255)
ARMORY_DESIGN_MoneyTextBoarder = Color(0,0,0,255)

ARMORY_DESIGN_AmmoTextColor = Color(60, 60, 100, 255)
ARMORY_DESIGN_AmmoTextBoarder = Color(0,0,0,255)

ARMORY_DESIGN_ShipmentsTextColor = Color(100, 60, 60, 255)
ARMORY_DESIGN_ShipmentsTextBoarder = Color(0,0,0,255)

-- Weapon armory configuration.
ARMORY_WEAPON_Weapon1Name = "AK47"
ARMORY_WEAPON_Weapon1WepName = "fas2_ak47_arm"
ARMORY_WEAPON_Weapon1AmmoType = "smg1"
ARMORY_WEAPON_Weapon1AmmoAmount = 30
ARMORY_WEAPON_Weapon1Model = "models/weapons/w_rif_ak47.mdl"

ARMORY_WEAPON_Weapon2Name = "M4A1"
ARMORY_WEAPON_Weapon2WepName = "weapon_real_cs_m4a1_arm"
ARMORY_WEAPON_Weapon2AmmoType = "smg1"
ARMORY_WEAPON_Weapon2AmmoAmount = 30
ARMORY_WEAPON_Weapon2Model = "models/weapons/w_rif_m4a1.mdl"

ARMORY_WEAPON_Weapon3Name = "MP5A5"
ARMORY_WEAPON_Weapon3WepName = "fas2_mp5a5_arm"
ARMORY_WEAPON_Weapon3AmmoType = "smg1"
ARMORY_WEAPON_Weapon3AmmoAmount = 30
ARMORY_WEAPON_Weapon3Model = "models/weapons/w_smg_mp5.mdl"

ARMORY_WEAPON_Cooldown = 15 -- Amount of minutes between being able to retrieve a weapon from the police armory as a government official. [Default = 5]
ARMORY_WEAPON_ArmorAmount = 25 -- How much armor should the police jobs get when they press E on the armory. [Default = 100]
ARMORY_WEAPON_Enabled = true -- Should the weapon armory for police jobs be enabled or not? true/false option. [Default = true]

-- Don't modify this
function util.RobberyFormatNumber( n )
    if (!n or type(n) != "number") then
        return 0
    end
	
    if n >= 1e14 then return tostring(n) end
    n = tostring(n)
    sep = sep or ","
    local dp = string.find(n, "%.") or #n+1
    for i=dp-4, 1, -3 do
        n = n:sub(1, i) .. sep .. n:sub(i+1)
    end
    return n
end