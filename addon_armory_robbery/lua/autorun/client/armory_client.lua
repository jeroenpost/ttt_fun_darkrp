surface.CreateFont("UiBold", {
	font = "Tahoma", 
	size = 14, 
	weight = 600
})

surface.CreateFont("UiSmallerThanBold", {
	font = "Tahoma", 
	size = 11, 
	weight = 600
})

surface.CreateFont("Trebuchet24", {
	font = "Trebuchet MS", 
	size = 24, 
	weight = 900
})
	
surface.CreateFont("Trebuchet22", {
	font = "Trebuchet MS", 
	size = 22, 
	weight = 900
})

surface.CreateFont("Trebuchet20", {
	font = "Trebuchet MS", 
	size = 20, 
	weight = 900
})

surface.CreateFont("ARMORY_ScreenText", {
    font = "coolvetica", 
    size = 20, 
    weight = 420
})

function ARMORY_WeaponMenu()
	
	local GUI_Armory_Frame = vgui.Create("DFrame")
	GUI_Armory_Frame:SetTitle("")
	GUI_Armory_Frame:SetSize(620,215)
	GUI_Armory_Frame:Center()
	GUI_Armory_Frame.Paint = function(CHPaint)
		-- Draw the menu background color.		
		draw.RoundedBox( 0, 0, 25, CHPaint:GetWide(), CHPaint:GetTall(), Color( 255, 255, 255, 150 ) )

		-- Draw the outline of the menu.
		surface.SetDrawColor(0,0,0,255)
		surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), CHPaint:GetTall())
	
		draw.RoundedBox( 0, 0, 0, CHPaint:GetWide(), 25, Color( 255, 255, 255, 200 ) )
		
		surface.SetDrawColor(0,0,0,255)
		surface.DrawOutlinedRect(0, 0, CHPaint:GetWide(), 25)

		-- Draw the top title.
		draw.SimpleText("Weapon Armory", "UiBold", 60,12.5, Color(70,70,70,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	GUI_Armory_Frame:MakePopup()
	GUI_Armory_Frame:ShowCloseButton(false)
	
	local GUI_Main_Exit = vgui.Create("DButton")
	GUI_Main_Exit:SetParent(GUI_Armory_Frame)
	GUI_Main_Exit:SetSize(16,16)
	GUI_Main_Exit:SetPos(600,5)
	GUI_Main_Exit:SetText("")
	GUI_Main_Exit.Paint = function()
	surface.SetMaterial(Material("icon16/cross.png"))
	surface.SetDrawColor(200,200,0,200)
	surface.DrawTexturedRect(0,0,GUI_Main_Exit:GetWide(),GUI_Main_Exit:GetTall())
	end
	GUI_Main_Exit.DoClick = function()
		GUI_Armory_Frame:Remove()
	end
	
	-- Panel 1
	local Weapon1Panel = vgui.Create( "DPanel", GUI_Armory_Frame )
	Weapon1Panel:SetSize( 200, 180 )
	Weapon1Panel:SetPos( 10, 30 )
	Weapon1Panel.Paint = function()
		draw.RoundedBox(8,1,1,Weapon1Panel:GetWide()-2,Weapon1Panel:GetTall()-2,Color(20,20,20,80))	
	end
	
	local Weapon1Display = vgui.Create("DModelPanel", Weapon1Panel)
	Weapon1Display:SetModel( ARMORY_WEAPON_Weapon1Model )
	Weapon1Display:SetPos( -500, -440 )
	Weapon1Display:SetSize( 1200, 1200 )
	Weapon1Display:GetEntity():SetAngles(Angle(255, 255, 255))
	Weapon1Display:SetCamPos( Vector( 255, 255, 80 ) )
	Weapon1Display:SetLookAt( Vector( 0, 0, 0 ) )
	
	local GUI_Weapon1Take = vgui.Create("DButton", Weapon1Panel)	
	GUI_Weapon1Take:SetSize(190,25)
	GUI_Weapon1Take:SetPos(5,150)
	GUI_Weapon1Take:SetText("")
	GUI_Weapon1Take.Paint = function()
		draw.RoundedBox(8,1,1,GUI_Weapon1Take:GetWide()-2,GUI_Weapon1Take:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 100 -- x pos
		struc.pos[2] = 12.5 -- y pos
		struc.color = Color(255,255,255,255) -- Red
		struc.text = "Retrieve ".. ARMORY_WEAPON_Weapon1Name -- Text
		struc.font = "UiBold" -- Font
		struc.xalign = TEXT_ALIGN_CENTER-- Horizontal Alignment
		struc.yalign = TEXT_ALIGN_CENTER -- Vertical Alignment
		draw.Text( struc )
	end
	GUI_Weapon1Take.DoClick = function()
		net.Start("ARMORY_RetrieveWeapon")
			net.WriteString("weapon1")
		net.SendToServer()
		
		GUI_Armory_Frame:Remove()
	end
	
	-- Panel 2
	local Weapon2Panel = vgui.Create( "DPanel", GUI_Armory_Frame )
	Weapon2Panel:SetSize( 200, 180 )
	Weapon2Panel:SetPos( 210, 30 )
	Weapon2Panel.Paint = function()
		draw.RoundedBox(8,1,1,Weapon2Panel:GetWide()-2,Weapon2Panel:GetTall()-2,Color(20,20,20,80))	
	end
	
	local Weapon2Display = vgui.Create("DModelPanel", Weapon2Panel)
	Weapon2Display:SetModel( ARMORY_WEAPON_Weapon2Model )
	Weapon2Display:SetPos( -500, -440 )
	Weapon2Display:SetSize( 1200, 1200 )
	Weapon2Display:GetEntity():SetAngles(Angle(255, 255, 255))
	Weapon2Display:SetCamPos( Vector( 255, 255, 80 ) )
	Weapon2Display:SetLookAt( Vector( 0, 0, 0 ) )
	
	local GUI_Weapon2Take = vgui.Create("DButton", Weapon2Panel)	
	GUI_Weapon2Take:SetSize(190,25)
	GUI_Weapon2Take:SetPos(5,150)
	GUI_Weapon2Take:SetText("")
	GUI_Weapon2Take.Paint = function()
		draw.RoundedBox(8,1,1,GUI_Weapon2Take:GetWide()-2,GUI_Weapon2Take:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 100 -- x pos
		struc.pos[2] = 12.5 -- y pos
		struc.color = Color(255,255,255,255) -- Red
		struc.text = "Retrieve ".. ARMORY_WEAPON_Weapon2Name -- Text
		struc.font = "UiBold" -- Font
		struc.xalign = TEXT_ALIGN_CENTER-- Horizontal Alignment
		struc.yalign = TEXT_ALIGN_CENTER -- Vertical Alignment
		draw.Text( struc )
	end
	GUI_Weapon2Take.DoClick = function()
		net.Start("ARMORY_RetrieveWeapon")
			net.WriteString("weapon2")
		net.SendToServer()
		
		GUI_Armory_Frame:Remove()
	end
	
	-- Panel 3
	local Weapon3Panel = vgui.Create( "DPanel", GUI_Armory_Frame )
	Weapon3Panel:SetSize( 200, 180 )
	Weapon3Panel:SetPos( 410, 30 )
	Weapon3Panel.Paint = function()
		draw.RoundedBox(8,1,1,Weapon3Panel:GetWide()-2,Weapon3Panel:GetTall()-2,Color(20,20,20,80))	
	end
	
	local Weapon3Display = vgui.Create("DModelPanel", Weapon3Panel)
	Weapon3Display:SetModel( ARMORY_WEAPON_Weapon3Model )
	Weapon3Display:SetPos( -500, -440 )
	Weapon3Display:SetSize( 1200, 1200 )
	Weapon3Display:GetEntity():SetAngles(Angle(255, 255, 255))
	Weapon3Display:SetCamPos( Vector( 255, 255, 80 ) )
	Weapon3Display:SetLookAt( Vector( 0, 0, 0 ) )
	
	local GUI_Weapon3Take = vgui.Create("DButton", Weapon3Panel)	
	GUI_Weapon3Take:SetSize(190,25)
	GUI_Weapon3Take:SetPos(5,150)
	GUI_Weapon3Take:SetText("")
	GUI_Weapon3Take.Paint = function()
		draw.RoundedBox(8,1,1,GUI_Weapon3Take:GetWide()-2,GUI_Weapon3Take:GetTall()-2,Color(0, 0, 0, 130))

		local struc = {}
		struc.pos = {}
		struc.pos[1] = 100 -- x pos
		struc.pos[2] = 12.5 -- y pos
		struc.color = Color(255,255,255,255) -- Red
		struc.text = "Retrieve ".. ARMORY_WEAPON_Weapon3Name -- Text
		struc.font = "UiBold" -- Font
		struc.xalign = TEXT_ALIGN_CENTER-- Horizontal Alignment
		struc.yalign = TEXT_ALIGN_CENTER -- Vertical Alignment
		draw.Text( struc )
	end
	GUI_Weapon3Take.DoClick = function()
		net.Start("ARMORY_RetrieveWeapon")
			net.WriteString("weapon3")
		net.SendToServer()
		
		GUI_Armory_Frame:Remove()
	end
end
usermessage.Hook("ARMORY_Weapon_Menu", ARMORY_WeaponMenu)