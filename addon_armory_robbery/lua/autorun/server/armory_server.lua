function ARMORY_ServerInitilize()
	ArmoryIsBeingRobbed = false
end
timer.Simple(1, function() ARMORY_ServerInitilize() end)

function ARMORY_PlayerDeath( ply, inflictor, attacker )
	if ply.IsRobbingArmory and attacker:IsPlayer() then
		DarkRP.notify(ply, 1, 5,  "You have failed to rob the police armory!")
		ply:unWanted(nil)
		attacker:addMoney(ARMORY_Custom_KillReward)
		
		for k, v in pairs(player.GetAll()) do
			if table.HasValue( GovernmentTeams, team.GetName(v:Team()) ) then
				DarkRP.notify(v, 1, 7,  "The police armory robbery has failed!")
			end
		end
		
		umsg.Start("ARMORY_KillTimer")
		umsg.End()
						
		ARMORY_StartCooldown()
						
		ply.IsRobbingArmory = false
		ArmoryIsBeingRobbed = false
	end
end
hook.Add("PlayerDeath", "ARMORY_PlayerDeath", ARMORY_PlayerDeath)


function ARMORY_PlayerArrested( ply, inflictor, attacker )
    if ply.IsRobbingArmory then
        DarkRP.notify(ply, 1, 5,  "You have failed to rob the police armory!")
        ply:unWanted(nil)

        for k, v in pairs(player.GetAll()) do
            if table.HasValue( GovernmentTeams, team.GetName(v:Team()) ) then
                DarkRP.notify(v, 1, 7,  "The police armory robbery has failed!")
            end
        end

        umsg.Start("ARMORY_KillTimer")
        umsg.End()

        ARMORY_StartCooldown()

        ply.IsRobbingArmory = false
        ArmoryIsBeingRobbed = false
    end
end
hook.Add("playerArrested", "ARMORY_PlayerArrested", ARMORY_PlayerArrested)

local NextBankFailcheck = 0
function ARMORY_RobberyFailCheck()
    if NextBankFailcheck < CurTime() then
        NextBankFailcheck = CurTime() + 3
    ArmoryRobber = nil
	for k, v in pairs(player.GetAll()) do
		if v.IsRobbingArmory then
			ArmoryRobber = v
			break
		end
	end
	
	if IsValid(ArmoryRobber) then
		for _, ent in pairs(ents.FindByClass("police_armory")) do
			if ent:IsValid() && ArmoryRobber:GetPos():Distance(ent:GetPos()) >= ARMORY_Custom_RobberyDistance then
				if ArmoryIsBeingRobbed then
                    if not  ArmoryRobber.nextFailBank or ArmoryRobber.nextFailBank < CurTime() then
                        ArmoryRobber.nextFailBank = CurTime() + 15
                        DarkRP.notify(ArmoryRobber, 1, 5,  "GET BACK WITHIN 5 SECONDS OR THE ROBBERY WILL FAIL")
                        return
                    end

                    if  ArmoryRobber.nextFailBank - CurTime() > 10 then return end
					DarkRP.notify(ArmoryRobber, 1, 5,  "You have moved to far away from the police armory, and the robbery has failed!")
					ArmoryRobber:unWanted(nil)
					
					for k, v in pairs(player.GetAll()) do
						if table.HasValue( GovernmentTeams, team.GetName(v:Team()) ) then
							DarkRP.notify(v, 1, 7,  "The police armory robbery has failed!")
						end
					end
			
					umsg.Start("ARMORY_KillTimer")
					umsg.End()
									
					ARMORY_StartCooldown()
									
					ArmoryRobber.IsRobbingArmory = false
					ArmoryIsBeingRobbed = false
					ArmoryRobber = nil
				end
			end
		end
    end
    end
end
hook.Add("Tick", "ARMORY_RobberyFailCheck", ARMORY_RobberyFailCheck)

function ARMORY_BeginRobbery( ply )
	local RequiredTeamsCount = 0
	local RequiredPlayersCounted = 0

	if table.HasValue( GovernmentTeams, team.GetName(ply:Team()) ) then
		if ARMORY_WEAPON_Enabled then
			if ARMORY_WEAPON_ArmorAmount > 0 then
                if ply:Team() == TEAM_SWAT and ply:Armor() < 80 then
                    ply:SetArmor( 80 )
                    DarkRP.notify(ply, 1, 5,  "You have been given 80 armor!")

                elseif ply:Team() == TEAM_CHIEF and ply:Armor() < 50 then
                    ply:SetArmor( 50 )
                    DarkRP.notify(ply, 1, 5,  "You have been given 50 armor!")
                elseif  ply:Armor() < ARMORY_WEAPON_ArmorAmount then
				ply:SetArmor( ARMORY_WEAPON_ArmorAmount )
				DarkRP.notify(ply, 1, 5,  "You have been given ".. ARMORY_WEAPON_ArmorAmount .." armor!")
                end
			end
			
			umsg.Start("ARMORY_Weapon_Menu", ply)
			umsg.End()
			return
		else
			DarkRP.notify(ply, 1, 5,  "The police armory is currently disabled!")
			return
		end
	end
	
	for k, v in pairs(player.GetAll()) do
		RequiredPlayersCounted = RequiredPlayersCounted + 1
		
		if table.HasValue( RequiredTeams, team.GetName(v:Team()) ) then
			RequiredTeamsCount = RequiredTeamsCount + 1
		end
		
		if RequiredPlayersCounted == #player.GetAll() then
			if RequiredTeamsCount < ARMORY_Custom_PoliceRequired then
				DarkRP.notify(ply, 1, 5, "There has to be "..ARMORY_Custom_PoliceRequired.." police officers before you can rob the police armory.")
				return
			end
		end
	end
	
	if ArmoryCooldown then
		DarkRP.notify(ply, 1, 5,  "You cannot rob the police armory yet!")
		return
	end

	if ArmoryIsBeingRobbed then
		DarkRP.notify(ply, 1, 5, "The police armory is already being robbed!")
		return
	end
	if #player.GetAll() < ARMORY_Custom_PlayerLimit then
		DarkRP.notify(ply, 1, 5, "There must be "..ARMORY_Custom_PlayerLimit.." players before you can rob the police armory.")
		return
	end
	if not table.HasValue( AllowedTeams, team.GetName(ply:Team()) )  then
		DarkRP.notify(ply, 1, 5, "You are not allowed to rob the police armory with your current team!")
		return
	end
	
	
	for k, v in pairs(player.GetAll()) do
		if table.HasValue( GovernmentTeams, team.GetName(v:Team()) ) then
			DarkRP.notify(v, 1, 7,  "The police armory is being robbed!")
		end
	end
	
	ArmoryIsBeingRobbed = true
	DarkRP.notify(ply, 1, 5, "You have began a robbery on the police armory!")
	DarkRP.notify(ply, 1, 10, "You must stay alive for ".. ARMORY_Custom_AliveTime .." minutes to receive everything the armory has.")
	DarkRP.notify(ply, 1, 13, "If you go to far away from the police armory vault, the robbery will also fail!")
	ply.IsRobbingArmory = true
	ply:wanted(nil, "Armory Robbery")
				
	umsg.Start("ARMORY_RestartTimer")
		umsg.Long(ARMORY_Custom_AliveTime * 60)
	umsg.End()
				
	timer.Simple( ARMORY_Custom_AliveTime * 60, function()
		if ply.IsRobbingArmory then
			for k, v in pairs(player.GetAll()) do
				if table.HasValue( GovernmentTeams, team.GetName(v:Team()) ) then
					DarkRP.notify(v, 1, 7,  "The police armory robbery has succeeded and everything inside is now long gone!")
				end
			end
						
			ply:unWanted(nil)
			umsg.Start("ARMORY_KillTimer")
			umsg.End()
						
			ARMORY_StartCooldown()
						
			ply.IsRobbingArmory = false
			
			-- Reward Money
			DarkRP.notify(ply, 1, 5,  "Congratulations! You have successfully robbed the police armory.")
			DarkRP.notify(ply, 1, 5,  "You have been given $"..util.RobberyFormatNumber(ARMORY_MONEY_Max).." and a lot of gear has been dropped from the armory.")
			ply:addMoney(ARMORY_MONEY_Max )

			
			-- Reward Ammo
			ARMORY_SpawnAmmo()

			
			-- Reward Shipments
			for i = 1, ARMORY_SHIPMENTS_Max do
				ARMORY_SpawnShipments( ply )
            end

            ARMORY_SpawnXP(ply)

			
			ArmoryIsBeingRobbed = false
		end
	end)
end

function ARMORY_StartCooldown()
	ArmoryCooldown = true
	umsg.Start("ARMORY_RestartCooldown")
		umsg.Long(ARMORY_Custom_CooldownTime * 60)
	umsg.End()
	
	timer.Simple( ARMORY_Custom_CooldownTime * 60, function()
		ArmoryCooldown = false
		umsg.Start("ARMORY_KillCooldown")
		umsg.End()
	end)
end



function ARMORY_Disconnect( ply )
	if ply.IsRobbingArmory then
		ply:unWanted(nil)
			
		for k, v in pairs(player.GetAll()) do
			if table.HasValue( GovernmentTeams, team.GetName(v:Team()) ) then
				DarkRP.notify(v, 1, 7,  "The police armory robbery has failed!")
			end
		end
			
		umsg.Start("ARMORY_KillTimer")
		umsg.End()
							
		ARMORY_StartCooldown()
							
		ply.IsRobbingArmory = false
		ArmoryIsBeingRobbed = false
	end
end
hook.Add( "PlayerDisconnected", "ARMORY_Disconnect", ARMORY_Disconnect )

function ARMORY_SpawnAmmo()

	local found = table.Random(GAMEMODE.AmmoTypes)
	 
	for _, ent in pairs(ents.FindByClass("police_armory")) do
		ammopos = ent:GetPos() + Vector(70,0,math.random(20,150))
	end
	
	local ARMORY_DroppedAmmo = ents.Create("spawned_weapon")
	ARMORY_DroppedAmmo:SetModel(found.model)
	ARMORY_DroppedAmmo.ShareGravgun = true
	ARMORY_DroppedAmmo:SetPos(ammopos)
	ARMORY_DroppedAmmo.nodupe = true
	function ARMORY_DroppedAmmo:PlayerUse(user, ...)
		user:GiveAmmo(ARMORY_AMMO_Max, found.ammoType)
		self:Remove()
		return true
	end
	ARMORY_DroppedAmmo:Spawn()
end

function ARMORY_SpawnXP( ply )
    local spawn = 0
    while spawn < 7 do
        spawn = spawn + 1
        for _, ent in pairs(ents.FindByClass("police_armory")) do
            shipmentpos = ent:GetPos() + Vector(70,0,math.random(20,150))
        end
        local ARMORY_DroppedShipment = ents.Create("gb_xp")
               ARMORY_DroppedShipment:SetPos(shipmentpos)
        ARMORY_DroppedShipment.nodupe = true
        ARMORY_DroppedShipment:Spawn()
    end
end

function ARMORY_SpawnShipments( ply )
	
	local foundKey
	for k,v in pairs(CustomShipments) do
		foundKey = math.random(table.Count(CustomShipments))
    end

    -- Try again till we have a weapon
    if not CustomShipments[foundKey].shop_weapons then
        return ARMORY_SpawnShipments(ply)
    end
	
	for _, ent in pairs(ents.FindByClass("police_armory")) do
		shipmentpos = ent:GetPos() + Vector(70,0,math.random(20,150))
	end
	
	local ARMORY_DroppedShipment = ents.Create("spawned_shipment")
	ARMORY_DroppedShipment.SID = ply.SID
	ARMORY_DroppedShipment:Setowning_ent(ply)
	ARMORY_DroppedShipment:SetContents(foundKey, ARMORY_SHIPMENTS_Amount)

	ARMORY_DroppedShipment:SetPos(shipmentpos)
	ARMORY_DroppedShipment.nodupe = true
	ARMORY_DroppedShipment:Spawn()
	ARMORY_DroppedShipment:SetPlayer(ply)
	ARMORY_DroppedShipment:SetModel("models/Items/item_item_crate.mdl")
	ARMORY_DroppedShipment:PhysicsInit(SOLID_VPHYSICS)
	ARMORY_DroppedShipment:SetMoveType(MOVETYPE_VPHYSICS)
	ARMORY_DroppedShipment:SetSolid(SOLID_VPHYSICS)

	local phys = ARMORY_DroppedShipment:GetPhysicsObject()
	phys:Wake()

	if CustomShipments[foundKey].onBought then
		CustomShipments[foundKey].onBought(ply, CustomShipments[foundKey], weapon)
	end
	hook.Call("playerBoughtShipment", nil, ply, CustomShipments[foundKey], weapon)
end

util.AddNetworkString("ARMORY_RetrieveWeapon")
net.Receive("ARMORY_RetrieveWeapon", function(length, ply)
    if ply.isbannedgbbanhackstuff then return end
	local TheWeapon = net.ReadString()

    -- GREENBLACK hack thingies
    if not table.HasValue( GovernmentTeams, team.GetName(ply:Team()) ) then
        DarkRP.notify(ply, 1, 5,  "Dont try hacking please.")
        DarkRP.printMessageAll(HUD_PRINTCENTER, ply:Nick().." was hacking" )
        DarkRP.notifyAll(HUD_PRINTCENTER, 10, ply:Nick().." was hacking. Armory hack. Banned permanently. SteamID: "..ply:SteamID() )

       if IsValid(ply) then
        ply:Ban(0) -- 24 hours
        ply:Kick("You know why. Fuck off.")
           ply.isbannedgbbanhackstuff = true
        RunConsoleCommand("ulx","banid", ply:SteamID(), "0", "You know why. Fuck off.")
        end
        return
    end

    local closeenough = false
    for _, ent in pairs(ents.FindByClass("police_armory")) do
        if ent:IsValid() and ply:GetPos():Distance(ent:GetPos()) < ARMORY_Custom_RobberyDistance then
            closeenough = true
        end
    end
    if not closeenough then
        DarkRP.notify(ply, 1, 5,  "Dont try hacking please.")
        DarkRP.printMessageAll(HUD_PRINTCENTER, ply:Nick().." was hacking" )
        DarkRP.notifyAll(HUD_PRINTCENTER, 10, ply:Nick().." was hacking. Armory hack2. Banned permanently. SteamID: "..ply:SteamID() )
        RunConsoleCommand("ulx","banid", ply:SteamID(), "0", "You know why. Fuck off.")
        if IsValid(ply) then
            ply:Ban(0) -- 24 hours
            ply:Kick("You know why. Fuck off.")
        end
        return
    end
    -- ===============================

	if ply.WeaponArmoryDelay then
		DarkRP.notify(ply, 1, 5,  "Please wait before retrieving another weapon from the armory.")
		return
	end
    local wep = false
	if TheWeapon == "weapon1" then
		ply:Give(ARMORY_WEAPON_Weapon1WepName)
        wep = ply:GetWeapon( ARMORY_WEAPON_Weapon1WepName )
		ply:GiveAmmo(ARMORY_WEAPON_Weapon1AmmoAmount, ARMORY_WEAPON_Weapon1AmmoType)
		DarkRP.notify(ply, 1, 5,  "You have retrieved an ".. ARMORY_WEAPON_Weapon1Name .." with ".. ARMORY_WEAPON_Weapon1AmmoAmount .." bullets.")
	elseif TheWeapon == "weapon2" then

		ply:Give(ARMORY_WEAPON_Weapon2WepName)
        wep = ply:GetWeapon( ARMORY_WEAPON_Weapon2WepName )
		ply:GiveAmmo(ARMORY_WEAPON_Weapon2AmmoAmount, ARMORY_WEAPON_Weapon2AmmoType)
		DarkRP.notify(ply, 1, 5,  "You have retrieved an ".. ARMORY_WEAPON_Weapon2Name .." with ".. ARMORY_WEAPON_Weapon2AmmoAmount .." bullets.")
	elseif TheWeapon == "weapon3" then
		ply:Give(ARMORY_WEAPON_Weapon3WepName)
        wep = ply:GetWeapon( ARMORY_WEAPON_Weapon3WepName )
		ply:GiveAmmo(ARMORY_WEAPON_Weapon3AmmoAmount, ARMORY_WEAPON_Weapon3AmmoType)
		DarkRP.notify(ply, 1, 5,  "You have retrieved an ".. ARMORY_WEAPON_Weapon3Name .." with ".. ARMORY_WEAPON_Weapon3AmmoAmount .." bullets.")
    end

    -- Make the wep undropable

    if IsValid(wep) then wep.cannotdrop = true end
	
	ply.WeaponArmoryDelay = true
	timer.Simple(ARMORY_WEAPON_Cooldown * 60, function()
		if IsValid(ply) then
			ply.WeaponArmoryDelay = false
		end
	end)
	
end)