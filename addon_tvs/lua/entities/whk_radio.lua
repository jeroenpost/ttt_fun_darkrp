AddCSLuaFile()

ENT.Base = "whk_mediasource"

ENT.PrintName		= "Radio"
ENT.Author			= "Wyozi"
ENT.Category		= "Wyozi Home Kit"

ENT.Spawnable		= true
ENT.AdminOnly		= true

ENT.CanSetUrl = false

ENT.Model = "models/props_lab/citizenradio.mdl"

ENT.Price = 200

function ENT:OnChannelChanged(name, old, new)
	local station = whk.RadioStations[(new % #whk.RadioStations)]
	self:SetUrl(station and station.Link or "")
end

function ENT:SetupDataTables()
	self.BaseClass.SetupDataTables(self)

	self:NetworkVar("Int", 2, "Channel")
	self:NetworkVarNotify("Channel", self.OnChannelChanged)
end

ENT.RenderGroup = RENDERGROUP_BOTH

if CLIENT then
	surface.CreateFont("WHK_RadioFontS", {
		font = "Roboto",
		size = 18
	})
end

local back_poly = {
	{ x = 20, y = 118 },
	{ x = 0, y = 105 },
	{ x = 20, y = 92 },
}
local fwd_poly = {
	{ x = 80, y = 92 },
	{ x = 100, y = 105 },
	{ x = 80, y = 118 },
}
function ENT:DrawTranslucent()
	self.p = self.p or tdui.Create()
	local p = self.p

	local station = whk.RadioStations[(self:GetChannel() % #whk.RadioStations)]

	p:Polygon(back_poly, Color(230, 230, 230))
	if p:Button("", "DermaLarge", -8, 85, 42, 40, Color(0, 0, 0, 0)) then
		net.Start("whk_radioswitch")
		net.WriteEntity(self)
		net.WriteBool(false)
		net.SendToServer()
	end

	p:Polygon(fwd_poly, Color(230, 230, 230))
	if p:Button("", "DermaLarge", 67, 85, 42, 40, Color(0, 0, 0, 0)) then
		net.Start("whk_radioswitch")
		net.WriteEntity(self)
		net.WriteBool(true)
		net.SendToServer()
	end

	p:Cursor()

	p:Render(self:LocalToWorld(Vector(9.7, 0, 18)), self:LocalToWorldAngles(Angle(0, 180, 0)), 0.1)

	cam.Start3D2D(self:LocalToWorld(Vector(8.5, 0, 18)), self:LocalToWorldAngles(Angle(0, 90, 90)), 0.1)

	surface.SetDrawColor(0, 0, 0, 250)
	surface.DrawRect(-55, 30, 160, 30)
	draw.SimpleText(station and station.Name or "-", "WHK_RadioFontS", -50, 36)
	cam.End3D2D()
end

if SERVER then
	util.AddNetworkString("whk_radioswitch")
	net.Receive("whk_radioswitch", function(len, cl)
		local ent = net.ReadEntity()
		if not IsValid(ent) or ent:GetClass() ~= "whk_radio" or not ent:CanInteract(cl) then return end

		local fwd = net.ReadBool()
		ent:SetChannel(ent:GetChannel() + (fwd and 1 or -1))
	end)
end
