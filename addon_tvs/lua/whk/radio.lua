whk.RadioStations = {
	{
		Name = "HouseTime.FM",
		Link = "http://listen.housetime.fm/dsl.pls"
	},
	{
		Name = "Drive Radio",
		Link = "http://listen.radionomy.com/drive.m3u"
	},
	{
		Name = "Soma.fm Groove Salad",
		Link = "http://somafm.com/groovesalad.pls"
	},
	{
		Name = "Soma.fm Beat Blender",
		Link = "http://somafm.com/beatblender.pls"
	},
	{
		Name = "Plus FM",
		Link = "http://www.plusfm.net/plusfm.pls"
	}
}

-- You can use following resources for more stations:

-- https://www.internet-radio.com/
-- To get a link that works for home kit rightclick the "PLS" button and copy the link location.
-- The link you have should look something like this "https://www.internet-radio.com/servers/tools/playlistgenerator/?u=http://us1.internet-radio.com:8180/listen.pls&t=.pls"
-- Now, remove everything before ?u= and after &t= so that the link looks like this: "http://us1.internet-radio.com:8180/listen.pls"
-- This is a link you can use with home kit

-- If you have your own .pls or .m3u links, they should also work directly with home kit.
