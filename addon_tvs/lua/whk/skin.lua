local surface = surface
local draw = draw
local Color = Color

local function LoadSkin()
	local defaultSkin = SKIN

	local SKIN = {}

	SKIN.PrintName 		= "WHK Derma Skin"
	SKIN.Author 		= "Wyozi"
	SKIN.DermaVersion	= 1
	SKIN.GwenTexture	= Material( "gwenskin/GModDefault.png" )

	-- credits: http://lua.2524044.n2.nabble.com/COW-lazy-copying-of-lua-tables-tp7305645p7305793.html
	local function makeRecursiveCOW(o)
	    return setmetatable({}, {
	        __index = function(t, key)
				local v = o[key]
				if type(v) == "table" then
					v = makeRecursiveCOW(v)
				end
				rawset(t, key, v)
				return v
			end,
	        __newindex =
	            function(t, k, v)
	                setmetatable(t, nil)
	                for ck, cv in pairs(o) do t[ck] = cv end
	                t[k] = v
	            end,
	    })
	end

	-- default skin table (or darkrp skin table) is global
	local defaultColors = defaultSkin and defaultSkin.Colours

	SKIN.Colours = makeRecursiveCOW(defaultColors)
	SKIN.Colours.Label.Default = Color(0, 0, 0)

	SKIN.Colours.Button.Normal = Color(230, 230, 230)
	SKIN.Colours.Button.Hover = Color(230, 230, 230)
	SKIN.Colours.Button.Down = Color(230, 230, 230)

	function SKIN:PaintFrame(panel, w, h)
		surface.SetDrawColor(255, 255, 255)
		surface.DrawRect(0, 0, w, h)

		surface.SetDrawColor(127, 127, 127)
		surface.DrawOutlinedRect(0, 0, w, h)

		surface.SetDrawColor(50, 50, 50)
		surface.DrawRect(0, 0, w, 25)
	end
	function SKIN:PaintButton( panel, w, h )
		-- dont draw image buttons
		if panel:GetText() == "" then return end

		if panel.Depressed or panel:IsSelected() or panel:GetToggle() then
			-- TODO
		elseif ( panel:GetDisabled() ) then
			surface.SetDrawColor(170, 170, 170)
		elseif ( panel.Hovered ) then
			surface.SetDrawColor(100, 100, 100)
		else
			surface.SetDrawColor(50, 50, 50)
		end

		surface.DrawRect(0, 0, w, h)

		surface.SetDrawColor(0, 0, 0)
		surface.DrawOutlinedRect(0, 0, w, h)

		-- hack warning
		panel.UpdateColours = function() end
		panel:SetTextStyleColor(Color(200, 200, 200, 255))
	end

	function SKIN:PaintCollapsibleCategory( panel, w, h )
		surface.SetDrawColor(70, 70, 70)
		surface.DrawRect(0, 0, w, 20)

		surface.SetDrawColor(0, 0, 0)
		surface.DrawOutlinedRect(0, 0, w, 20)

		--[[
		if ( !panel:GetExpanded() && h < 40 ) then
			return self.tex.CategoryList.Header( 0, 0, w, h )
		end
		self.tex.CategoryList.Inner( 0, 0, w, h )
		]]
	end

	derma.DefineSkin( "WHKSkin", "Wyozi Home Kit Derma skin", SKIN )
end

concommand.Add("whk_reloadskin", LoadSkin)

-- hack because default derma skin isn't loaded on file load
hook.Add("InitPostEntity", "WHK_InitDermaSkin", LoadSkin)
