--[[
	Copyright (C) Chessnut - All Rights Reserved
	Unauthorized copying of this file, via any medium is strictly prohibited. Proprietary and confidential.
	Written by Chessnut (chessnutist@gmail.com), December 2014
--]]

util.AddNetworkString("NewsTicker")
util.AddNetworkString("NewsTickerFull")

hook.Add("SetupPlayerVisibility", "newsVisibility", function(client, viewEntity)
	local channnels = team.GetPlayers(TEAM_NEWS)

	for k, v in ipairs(ents.FindByClass("news_tv")) do
		if (v:GetOn() and client:Visible(v)) then
			local person = channnels[v:GetChannel()]

			if (IsValid(person)) then
				local weapon = person:GetActiveWeapon()

				if (IsValid(weapon) and weapon:GetClass() == "news_camera") then
					AddOriginToPVS(person:GetPos())
				end
			end
		end
	end
end)

local VOICE_RANGE = 480 ^ 2

hook.Add("PlayerCanHearPlayersVoice", "newsVoice", function(listener, speaker)
	local weapon = speaker:GetActiveWeapon()

	if (!IsValid(weapon) or weapon:GetClass() != "news_camera") then
		return
	end

	local channels = team.GetPlayers(TEAM_NEWS)
	local position = listener:GetPos()

	for k, v in ipairs(ents.FindByClass("news_tv")) do
		if (v:GetOn() and channels[v:GetChannel()] == speaker and position:DistToSqr(v:GetPos()) <= VOICE_RANGE) then
			return true
		end
	end
end)

concommand.Add("news_sound", function(client, command, arguments)
	if (client:Team() == TEAM_NEWS and (client.nextSound or 0) < CurTime()) then
		local index = tonumber(arguments[1])

		if (!index or !NEWS_SOUNDS[index]) then
			return
		end

		for k, v in ipairs(ents.FindByClass("news_tv")) do
			if (client == team.GetPlayers(TEAM_NEWS)[v:GetChannel()]) then
				local source = NEWS_SOUNDS[index][2]

				if (type(source) == "table") then
					source = table.Random(source)
				end

				v:EmitSound(source, 75)
			end
		end

		client.nextSound = CurTime() + 0.75
	end
end)

concommand.Add("rp_disabletv", function(client, command, argments)
	if (!IsValid(client) or client:IsSuperAdmin()) then
		for k, v in ipairs(ents.FindByClass("news_tv")) do
			v:Remove()
		end

		SetGlobalBool("tvDisabled", true)

		for k, v in ipairs(player.GetAll()) do
			v:ChatPrint("Purchasing of TVs has been disabled by "..(IsValid(client) and client:Name() or "Console"))
		end
	end
end)

concommand.Add("rp_enabletv", function(client, command, argments)
	if (!IsValid(client) or client:IsSuperAdmin()) then
		SetGlobalBool("tvDisabled", false)

		for k, v in ipairs(player.GetAll()) do
			v:ChatPrint("Purchasing of TVs has been enabled by "..(IsValid(client) and client:Name() or "Console"))
		end
	end
end)

hook.Add("PlayerInitialSpawn", "NewsTickerSender", function(client)
	timer.Simple(5, function()
		if (IsValid(client)) then
			net.Start("NewsTickerFull")
				net.WriteTable(NewsTickers)
			net.Send(client)
		end
	end)
end)

net.Receive("NewsTicker", function(length, client)
	if (client:Team() == TEAM_NEWS) then
		local text = net.ReadString():sub(1, 500)

		NewsTickers[client] = NewsTickers[client] or {}
		client:EmitSound("buttons/button14.wav", 60)

		net.Start("NewsTicker")
			net.WriteEntity(client)
			net.WriteString(text)
		net.Broadcast()
	end
end)