--[[
	Copyright (C) Chessnut - All Rights Reserved
	Unauthorized copying of this file, via any medium is strictly prohibited. Proprietary and confidential.
	Written by Chessnut (chessnutist@gmail.com), December 2014
--]]

include("shared.lua")

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

function ENT:SpawnFunction(client, trace)
	local entity = ents.Create("news_tv")
	entity:SetPos(trace.HitPos + Vector(0, 0, 24))
	entity:Spawn()

	return entity
end

function ENT:Initialize()
	self:SetModel("models/props_phx/rt_screen.mdl")
	self:SetSolid(SOLID_VPHYSICS)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)

	local physObj = self:GetPhysicsObject()

	if (IsValid(physObj)) then
		physObj:Wake()
	end
end

function ENT:Use(activator)
	if ((self.nextPush or 0) < CurTime()) then
		if (!self:GetOn()) then
			self:SetOn(true)
			self:SetChannel(0)
		elseif (self:GetChannel() >= team.NumPlayers(TEAM_NEWS)) then
			self:SetOn(false)
		else
			self:SetChannel(self:GetChannel() + 1)

			if (self:GetChannel() > team.NumPlayers(TEAM_NEWS)) then
				self:SetOn(false)
			end
		end

		self:EmitSound("buttons/lightswitch2.wav", 100, self:GetOn() and 120 or 100)
		self.nextPush = CurTime() + 1
	end
end