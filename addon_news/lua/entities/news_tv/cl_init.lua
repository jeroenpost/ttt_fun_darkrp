--[[
	Copyright (C) Chessnut - All Rights Reserved
	Unauthorized copying of this file, via any medium is strictly prohibited. Proprietary and confidential.
	Written by Chessnut (chessnutist@gmail.com), December 2014
--]]

include("shared.lua")

local gradient = Material("gui/gradient")
local gradient2 = Material("vgui/gradient-u")
local noise = surface.GetTextureID("effects/security_noise2")

SCREEN_CACHE = {}
SCREEN_STATE = {}
SCREEN_MAX_FPS = 1 / 25

function ENT:setupScreen(client)
	if (SCREEN_CACHE[client:UniqueID()]) then
		return SCREEN_CACHE[client:UniqueID()]
	end

	local texture = GetRenderTarget("newsScreen"..client:UniqueID(), 565, 330)
	local uniqueID = "DrawNewsScreen"..client:UniqueID()

	local function callback()
		if (SCREEN_STATE[client]) then
			if (IsValid(client) and SCREEN_STATE[client]) then
				local oldRT = render.GetRenderTarget()
				local oldW, oldH = ScrW(), ScrH()

				render.SetRenderTarget(texture)
					render.SetViewPort(0, 0, 565, 330)
						render.Clear(255, 255, 255, 255, true, true)

						local camera = client:GetActiveWeapon()

						render.RenderView({
							origin = camera:GetPos() + camera:GetUp()*20 + camera:GetForward()*8,
							angles = client:EyeAngles(),
							x = 0,
							y = 0,
							w = 565,
							h = 330,
							drawhud = false,
							drawviewmodel = false,
							znear = 24
						})

						SCREEN_STATE[client] = false
					render.SetViewPort(0, 0, oldW, oldH)
				render.SetRenderTarget(oldRT)

				timer.Adjust(uniqueID, math.max(RealFrameTime() * 7.5, SCREEN_MAX_FPS), 0, callback)
			end
		end
	end

	timer.Create(uniqueID, 1 / 20, 0, callback)

	local screen = CreateMaterial("newsScreen"..client:UniqueID(), "UnlitGeneric")
	screen:SetTexture("$basetexture", texture)
	screen:Recompute()

	SCREEN_CACHE[client:UniqueID()] = screen

	return screen
end

local color_green = Color(0, 255, 0)
local VIEW_RANGE = 750 ^ 2

function ENT:Draw()
	self:DrawModel()

	local position = self:GetPos() + self:GetUp()*35.5 + self:GetForward()*6.1 + self:GetRight()*28.1
	local angles = self:GetAngles()
	angles:RotateAroundAxis(angles:Up(), 90)
	angles:RotateAroundAxis(angles:Forward(), 90)

	local right = self:GetRight()
	local mins = self:LocalToWorld(self:OBBMins())
	local maxs = self:LocalToWorld(self:OBBMaxs())

	render.PushCustomClipPlane(right, right:Dot(maxs) + 1)
	render.PushCustomClipPlane(-right, -right:Dot(mins) + 1)
	render.EnableClipping(true)

	cam.Start3D2D(position, angles, 0.1)
		local channel = self:GetChannel()
		local channels = team.GetPlayers(TEAM_NEWS)
		local client = channels[channel]

		if (channel > 0 and IsValid(client) and self.client != client) then
			self.screen = self:setupScreen(client)
			self.client = client
		end

		if (self:GetOn() and (channel == 0 or self.screen) and LocalPlayer():GetPos():DistToSqr(self:GetPos()) <= VIEW_RANGE) then
			if (!IsValid(client) or channel == 0) then
				surface.SetDrawColor(255, 0, 0)
				surface.SetTexture(noise)
				surface.DrawTexturedRect(math.random(-1, 1), math.random(-1, 1), 567, 332)

				surface.SetDrawColor(50, 50, 50, math.random(45, 100))
				surface.DrawRect(0, 0, 565, 330)

				draw.SimpleText("NO SIGNAL", "BudgetLabel", 282 + (math.sin(RealTime() * 1.5) * 80), 165 + (math.cos(RealTime() * 1.5) * 30), color_white, 1, 1)
			else
				local camera = client:GetActiveWeapon()

				if (IsValid(camera) and camera:GetClass() == "news_camera") then
					SCREEN_STATE[client] = true

					surface.SetDrawColor(255, 255, 255)
					surface.SetMaterial(self.screen)
					surface.DrawTexturedRect(0, 0, 565, 330)
				else
					surface.SetDrawColor(HSVToColor((RealTime() * 5) % 360, 0.15, 1))
					surface.DrawRect(0, 0, 565, 330)

					draw.SimpleText("We are currently off-air.", "DermaDefaultBold", 282, 165, color_black, 1, 1)
					draw.SimpleText(os.date("Today is %A. The time is %I:%M:%S %p."), "DermaDefault", 282, 181, color_black, 1, 1)
				end

				local color = Color(0, 0, 255)

				self:drawLogo(channel, color)
				self:drawTicker(color, client)
			end

			draw.SimpleText("CH "..channel, "BudgetLabel", 557, 4, color_green, 2, 0)
		else
			surface.SetDrawColor(0, 0, 0)
			surface.DrawRect(0, 0, 565, 330)
		end
	cam.End3D2D()

	render.EnableClipping(false)
	render.PopCustomClipPlane()
	render.PopCustomClipPlane()
end

function ENT:drawLogo(channel, color)
	surface.SetDrawColor(color)
	surface.SetMaterial(gradient2)
	surface.DrawTexturedRect(8, 8, 48, 48)

	surface.SetDrawColor(color.r, color.g, color.b, 150)
	surface.DrawRect(8, 8, 48, 48)

	surface.SetDrawColor(0, 0, 0, 180)
	surface.DrawRect(8, 8, 48, 8)

	draw.SimpleText("CHANNEL", "newsTiny", 32, 12, color_white, 1, 1)

	surface.SetDrawColor(250, 30, 30)
	surface.DrawRect(8, 54, 48, 8)

	draw.SimpleText("N E W S", "newsTiny", 32, 58, color_white, 1, 1)

	draw.SimpleText(channel, "newsNumber", 32, 34, color_black, 1, 1)
	draw.SimpleText(channel, "newsNumber", 31, 33, color_white, 1, 1)

	local alpha = 240 + math.sin(RealTime() * 5)*50

	draw.SimpleTextOutlined("LIVE", "newsItalics", 64, 34, ColorAlpha(color_white, alpha), 0, 1, 1, ColorAlpha(color_black, alpha))
end

function ENT:drawTicker(color, client)
	surface.SetDrawColor(color)
	surface.SetMaterial(gradient)
	surface.DrawTexturedRect(0, 300, 565, 30)

	self.w = draw.SimpleTextOutlined(NewsTickers[client] or os.date("The current time is %I:%M:%S %p."):upper(), "newsNormal", self.scroll or 0, 315, color_white, 0, 1, 1, color_black)
end

function ENT:Think()
	if (self.w) then
		self.scroll = (self.scroll or 0) - (RealFrameTime() * 150)

		if (self.scroll < -self.w) then
			self.scroll = 565
		end
	end
end