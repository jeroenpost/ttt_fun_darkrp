EasyFuel

INSTALLATION:
Drag and drop the "easyfuel" folder to your garrysmod/addons folder.

CONFIGURATION:
In-game convars:
FUEL_Timer (Def. 5) -- Interval in seconds between takinf fuel
FUEL_Loss (Def. 1)  -- Amount of fuel to lose every timer time (times by velocity)

USAGE:
In-game console commands:
FUEL_Management -- Opens the fuel management window
FUEL_GetModel   -- Gets the model of the vehicle you are looking at

HELP:
If you need help, contact me on CoderHire via PM or TICKET for bugs, and I will get back to you. 