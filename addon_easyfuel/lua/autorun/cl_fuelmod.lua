--[[==============================]]--
--[[     FUEL MOD BY BOT DAN      ]]--
--[[     RECODE VERSION 1.00      ]]--
--[[==============================]]--

--[[      PREVENT BAD THINGS      ]]--
if (SERVER) then return end
include( "fuelmenu/cl_managepanel.lua" )
local FUEL_Anim = 0

--[[          ADD FONTS           ]]--
surface.CreateFont( "FUEL_FontNumbers", {
	font = "Arial",
	size = 24,
	weight = 500,
	antialias = true,
} )
surface.CreateFont( "FUEL_FontLetters", {
	font = "Arial",
	size = 12,
	weight = 500,
	antialias = true,
} )

--[[      HUD PAINT FUNCTION      ]]--
function FUEL_PaintHUD( )
	local ply = LocalPlayer()
	local veh = ply:GetVehicle()
	if ply and IsValid( ply ) and veh and IsValid( veh ) and (veh.FUEL_Amount != nil) and veh.FUEL_MaxAmount and (veh.FUEL_MaxAmount != nil) then
		local FUEL_FuelClamp = math.Clamp( veh.FUEL_Amount / veh.FUEL_MaxAmount * 100, 0, 100 )
		FUEL_Anim = math.Approach( FUEL_Anim, FUEL_FuelClamp, 0.4 )
		
		local wide = 25		-- Width of the HUD
		local high = 125	-- Hight of the HUD
		local offset = 25	-- How many Px the HUD is from the bottom-right
        local offset2 = 125	-- How many Px the HUD is from the bottom-right
		local border = 4	-- How wide the Transparent dark border is around the HUD (Background)
		
		local R = 255	-- R of RGB Color value of HUD
		local G = 185	-- G of RGB Color value of HUD
		local B = 0		-- B of RGB Color value of HUD
		local color = Color( R, G, B )
		
		local sum = ( high - (border * 2) )*( FUEL_Anim / 100 ) + border * 2
		
		draw.RoundedBox( 4, ScrW() - wide - offset - ( border / 2 ), ScrH() - high - offset2 - ( border / 2 ), wide + border, high + border, Color( 0, 0, 0, 240 ) )
		draw.RoundedBox( 4, ScrW() - wide - offset, ScrH() - sum - offset2, wide, sum, color )
		if ( sum < 40 ) then
			draw.DrawText( math.Round(FUEL_FuelClamp).."%", "FUEL_FontNumbers", ScrW() - wide / 2 - offset + 1, ScrH() - offset2 - 40 + 1, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
			draw.DrawText( math.Round(FUEL_FuelClamp).."%", "FUEL_FontNumbers", ScrW() - wide / 2 - offset, ScrH() - offset2 - 40, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
			draw.DrawText( "F  U  E  L", "FUEL_FontLetters", ScrW() - wide / 2 - offset + 1, ScrH() - offset2 - 40 + 1 + 20, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
			draw.DrawText( "F  U  E  L", "FUEL_FontLetters", ScrW() - wide / 2 - offset, ScrH() - offset2 - 40 + 20, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
		else
			draw.DrawText( math.Round(FUEL_FuelClamp).."%", "FUEL_FontNumbers", ScrW() - wide / 2 - offset + 1, ScrH() - sum - offset2 + 1, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
			draw.DrawText( math.Round(FUEL_FuelClamp).."%", "FUEL_FontNumbers", ScrW() - wide / 2 - offset, ScrH() - sum - offset2, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
			draw.DrawText( "F  U  E  L", "FUEL_FontLetters", ScrW() - wide / 2 - offset + 1, ScrH() - sum - offset2 + 1 + 20, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
			draw.DrawText( "F  U  E  L", "FUEL_FontLetters", ScrW() - wide / 2 - offset, ScrH() - sum - offset2 + 20, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
		end
	end
end
hook.Add( "HUDPaint", "FUEL_PaintHUD", FUEL_PaintHUD )

--[[     GET FUEL NET FUNCTION    ]]--
function FUEL_RecieveData( )
	local ply = LocalPlayer()
	local veh = net.ReadEntity()
	local fuel = net.ReadFloat()
	local maxfuel = net.ReadFloat()
	if veh and IsValid( veh ) then
		veh.FUEL_Amount = fuel
		veh.FUEL_MaxAmount = maxfuel
	end
end
net.Receive( "FUEL_NetSendData", FUEL_RecieveData )
