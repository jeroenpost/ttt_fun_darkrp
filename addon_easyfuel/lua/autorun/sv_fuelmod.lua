--[[==============================]]--
--[[     FUEL MOD BY BOT DAN      ]]--
--[[     RECODE VERSION 1.00      ]]--
--[[==============================]]--

--[[    ADDING IMPORTANT THINGS   ]]--
if (SERVER) then
	-- Add needed files to client --
	AddCSLuaFile( "cl_fuelmod.lua" )
	AddCSLuaFile( "fuelmenu/cl_managepanel.lua" )
	
	-- Add net messages --
	util.AddNetworkString( "FUEL_NetSendTable" )
	util.AddNetworkString( "FUEL_NetSendData" )
	util.AddNetworkString( "FUEL_NetNewTable" )
	util.AddNetworkString( "FUEL_NetAskTable" )
end

--[[    DEFINE CONVAR NUMBERS     ]]--
CreateConVar( "FUEL_Timer", 3, FCVAR_ARCHIVE, "Sets the time between taking fuel from vehicle" )
CreateConVar( "FUEL_Loss", 1, FCVAR_ARCHIVE, "Sets the amount of fuel lost per timer times by velocity modifier" )

--[[       DEFINE VARIABLES       ]]--
local FUEL_VehicleTable = {}
local veh = FindMetaTable( "Entity" )

--[[     INITIALISE FUEL DATA     ]]--
function FUEL_InitialiseData( )
	if (SERVER) then
		if not file.Exists( "FUEL_DataFile.txt", "DATA" ) then
			file.Write( "FUEL_DataFile.txt", "" )
		else
			local FUEL_JSONData = util.JSONToTable( file.Read( "FUEL_DataFile.txt", "DATA" ) )
			-- k = model, v = fuel --
			for k, v in pairs( FUEL_JSONData ) do
				if (k != false) and (k != nil) and (v != false) and (v != nil) then
					FUEL_AddVehicle( k, v )
				end
			end
		end
	end
end
--hook.Add( "Initialize", "FUEL_InitialiseData", FUEL_InitialiseData )

--[[     ADD VEHICLE TO TABLE     ]]--
function FUEL_AddVehicle( model, fuel )
	if (model == false) or (model == nil) then return end 
	if (fuel == false) or (fuel == nil) then return end
	if (fuel <= 0 ) then return end
	FUEL_VehicleTable[model] = fuel
end

--[[    SET MAX FUEL FUNCTION     ]]--
function veh:FUEL_SetMaxFuel( )
	self.FUEL_Amount = self:FUEL_GetMaxFuel( )
end

--[[      SET FUEL FUNCTION       ]]--
function veh:FUEL_SetFuel( amount )
	local FUEL_Round = math.Clamp( amount, 0, self:FUEL_GetMaxFuel( ) )
	self.FUEL_Amount = FUEL_Round
end

--[[    GET MAX FUEL FUNCTION     ]]--
function veh:FUEL_GetMaxFuel( )
	return FUEL_VehicleTable[self:GetModel( )]
end

--[[      GET FUEL FUNCTION       ]]--
function veh:FUEL_GetFuel( )
	return self.FUEL_Amount
end

--[[   PLAYER ENTER VEHICLE HOOK  ]]--
function FUEL_EnterVehicle( ply, veh )
	if not ply or not IsValid( ply ) then return end
	if not veh or not IsValid( veh ) then return end
	if (veh:FUEL_GetMaxFuel( ) == nil) then return end
	if (veh:FUEL_GetFuel( ) == nil) then veh:FUEL_SetMaxFuel( ) end
	if (veh:FUEL_GetFuel( ) > 0) then
		veh:Fire( "TurnOn" )
		veh:FUEL_LoopCheck( ply )
	else
		veh:Fire( "TurnOff" )
	end
	veh:FUEL_SendFuelLevel( ply )
end
hook.Add( "PlayerEnteredVehicle", "FUEL_EnterVehicle", FUEL_EnterVehicle )

--[[   PLAYER EXIT VEHICLE HOOK   ]]--
function FUEL_ExitVehicle( ply, veh )
	if not ply or not IsValid( ply ) then return end
	if not veh or not IsValid( veh ) then return end
	if (veh:FUEL_GetMaxFuel( ) == nil) then return end
	if (veh:FUEL_GetFuel( ) == nil) then return end
	-- Prevent multiple timers from happening --
	timer.Destroy( "FUEL_Timer-"..veh:EntIndex( ) )
end
hook.Add( "PlayerLeaveVehicle", "FUEL_ExitVehicle", FUEL_ExitVehicle )

--[[  VEHICLE LOOP CHECK FUNCTION ]]--
function veh:FUEL_LoopCheck( ply )
	if not ply or not IsValid( ply ) then return end
	if not self or not IsValid( self ) then return end
	if (self:FUEL_GetMaxFuel( ) == nil) then return end
	if (self:FUEL_GetFuel( ) == nil) then self:FUEL_SetMaxFuel( ) end
	if (self:GetDriver( ) != ply) then return end
	
	-- Better speed measure than just GetVelocity() --
	if (self:GetVelocity():Length() > 1) then
		local FUEL_Speed = math.Round( self:GetVelocity():Length() )
		local FUEL_Modfy = math.Round( FUEL_Speed / 1000, 6 )
		self:FUEL_SetFuel( self:FUEL_GetFuel( ) - FUEL_Modfy * GetConVarNumber( "FUEL_Loss" ) )
		if (self:FUEL_GetFuel( ) <= 0) then
			self:FUEL_SetFuel( 0 )
			self:Fire( "TurnOff" )
			self:FUEL_SendFuelLevel( ply )
			return
		end
	end
	self:FUEL_SendFuelLevel( ply )
	
	-- Create a timer with a unique ID so it can be ended --
	timer.Create( "FUEL_Timer-"..self:EntIndex( ), GetConVarNumber( "FUEL_Timer" ), 1, function() self:FUEL_LoopCheck( ply ) end )
end

--[[     SEND FUEL LEVEL DATA     ]]--
function veh:FUEL_SendFuelLevel( ply )
	net.Start( "FUEL_NetSendData" )
		net.WriteEntity( self )
		net.WriteFloat( self:FUEL_GetFuel( ) )
		net.WriteFloat( self:FUEL_GetMaxFuel( ) )
	net.Send( ply )
end

--[[       UPDATE FUEL DATA       ]]--
function FUEL_UpdateData( )
	if (SERVER) then
		if not file.Exists( "FUEL_DataFile.txt", "DATA" ) then
			file.Write( "FUEL_DataFile.txt", "" )
		end
		local FUEL_VehicleTableUpdate = net.ReadTable( )
		local FUEL_JSONData = util.TableToJSON( FUEL_VehicleTableUpdate )
		table.Empty( FUEL_VehicleTable )
		file.Write( "FUEL_dataFile.txt", FUEL_JSONData )
		-- k = model, v = fuel --
		for k, v in pairs( FUEL_VehicleTableUpdate ) do
			if (k != false) and (k != nil) and (v != false) and (v != nil) then
			FUEL_AddVehicle( k, v )
		end
		FUEL_SendData( player.GetAll( ) )
		end
	end
end
--net.Receive( "FUEL_NetNewTable", FUEL_UpdateData )

--[[        SEND FUEL DATA        ]]--
function FUEL_SendData( ply )
	net.Start( "FUEL_NetSendTable" )
		net.WriteTable( FUEL_VehicleTable )
	net.Send( ply )
end
hook.Add( "PlayerInitialSpawn", "FUEL_SendData", FUEL_SendData )

--[[     RECIEVE FUEL REQUEST     ]]--
function FUEL_AskedData(len,pl )
	FUEL_SendData( pl )
end
net.Receive( "FUEL_NetAskTable", FUEL_AskedData )