--[[==============================]]--
--[[     FUEL MOD BY BOT DAN      ]]--
--[[     RECODE VERSION 1.00      ]]--
--[[==============================]]--

--[[      PREVENT BAD THINGS      ]]--
if (SERVER) then return end
local FUEL_ModelTable = {}

--[[          ADD FONTS           ]]--
surface.CreateFont( "FUEL_FontPanelHeader", {
	font = "Arial",
	size = 24,
	weight = 500,
	antialias = true,
} )
surface.CreateFont( "FUEL_FontLabel", {
	font = "Arial",
	size = 16,
	weight = 500,
	antialias = true,
} )
surface.CreateFont( "FUEL_FontScrollbar", {
	font = "Arial",
	size = 12,
	weight = 500,
	antialias = true,
} )

--[[         CREATE MENU          ]]--
concommand.Add( "FUEL_Management", function( )
	local FRAMEFuelMain = vgui.Create( "DFrame" )
	FRAMEFuelMain:SetSize( 600, 400 )
	FRAMEFuelMain:Center()
	FRAMEFuelMain:SetTitle( " " )
	FRAMEFuelMain:SetVisible( true )
	FRAMEFuelMain:SetDraggable( true )
	FRAMEFuelMain:ShowCloseButton( false )
	FRAMEFuelMain:MakePopup()
	FRAMEFuelMain.Paint = function( self, w, h )
		draw.RoundedBoxEx( 8, 0, 24, w, h-24, Color( 100, 100, 100 ), false ,false, true, true )
		draw.RoundedBoxEx( 8, 0, 0, w-100, 24, Color( 50, 50, 50 ), true )
		surface.SetDrawColor( 255, 255, 255 )
		surface.SetMaterial( Material( "icon16/car_add.png" ) )
		surface.DrawTexturedRect( 6, 1, 20, 20 )
		draw.DrawText( "Vehicle Management", "FUEL_FontPanelHeader", 31, 1, Color( 50, 50, 50 ), TEXT_ALIGN_LEFT )
		draw.DrawText( "Vehicle Management", "FUEL_FontPanelHeader", 30, 0, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT )
	end
	
	local closered = 150
	local BUTTONCloseFrame = vgui.Create( "DButton", FRAMEFuelMain )
	BUTTONCloseFrame:SetSize( 100, 24 )
	BUTTONCloseFrame:SetPos( FRAMEFuelMain:GetWide() - 100, 0 )
	BUTTONCloseFrame:SetText( " " )
	BUTTONCloseFrame.DoClick = function() 
		FUEL_RequestTable( )
		chat.AddText( Color( 100, 200, 255 ), "[FUEL] ", Color( 255, 255, 255 ), "Changes have been discarded!" )
		FRAMEFuelMain:Close()
	end
	BUTTONCloseFrame.Paint = function( pan, w, h )
		draw.RoundedBoxEx( 8, 0, 0, w, h, Color( closered, 100, 100 ), false, true, false, false )
		draw.DrawText( "Close", "FUEL_FontPanelHeader", 23, 1, Color( 50, 50, 50 ), TEXT_ALIGN_LEFT )
		draw.DrawText( "Close", "FUEL_FontPanelHeader", 22, 0, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT )
	end
	BUTTONCloseFrame.OnCursorEntered = function() closered = 255 end
	BUTTONCloseFrame.OnCursorExited = function () closered = 150 end
	
	if LocalPlayer():IsAdmin() or LocalPlayer():IsSuperAdmin() then
		local PANELCarView = vgui.Create( "DPanel", FRAMEFuelMain )
		local MODELCarView = vgui.Create( "DModelPanel", PANELCarView )
		local LABELModelName = vgui.Create( "DLabel", FRAMEFuelMain )
		local TEXTModelName = vgui.Create( "DTextEntry", FRAMEFuelMain )
		local BUTTONUpdateModel = vgui.Create( "DButton", FRAMEFuelMain )
		local LABELModelView = vgui.Create( "DLabel", FRAMEFuelMain )
		local LABELFuelAmt = vgui.Create( "DLabel", FRAMEFuelMain )
		local TEXTFuelAmt = vgui.Create( "DTextEntry", FRAMEFuelMain )
		local BUTTONAddVehicle = vgui.Create( "DButton", FRAMEFuelMain )
		local BUTTONSaveChanges = vgui.Create( "DButton", FRAMEFuelMain )
		local BUTTONDiscardChanges = vgui.Create( "DButton", FRAMEFuelMain )
		
		local SCROLLCarsList = vgui.Create( "DPanelList", FRAMEFuelMain )
		SCROLLCarsList:SetSize( 295, 356 )
		SCROLLCarsList:SetPos( 10, 34 )
		SCROLLCarsList:EnableHorizontal( true )
		SCROLLCarsList:EnableVerticalScrollbar( true )
		SCROLLCarsList.Padding = 5
		SCROLLCarsList.Spacing = 5
		SCROLLCarsList.Paint = function( self, w, h )
			draw.RoundedBox( 4, 0, 0, w-13, h, Color( 75, 75, 75 ) )
		end
		SCROLLCarsList.VBar.Paint = function( self, w, h )
			draw.RoundedBox( 4, 0, 0, w, h, Color( 75, 75, 75 ) )
		end
		SCROLLCarsList.VBar.btnUp.Paint = function( self, w, h )
			draw.RoundedBoxEx( 4, 0, 0, w, h, Color( 150, 150, 150 ), true, true, false, false )
			draw.DrawText( "▲", "FUEL_FontScrollbar", w/2, 0, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
		end
		SCROLLCarsList.VBar.btnDown.Paint = function( self, w, h )
			draw.RoundedBoxEx( 4, 0, 0, w, h, Color( 150, 150, 150 ), false, false, true, true )
			draw.DrawText( "▼", "FUEL_FontScrollbar", w/2, 0, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
		end
		SCROLLCarsList.VBar.btnGrip.Paint = function( self, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color( 150, 150, 150 ) )
		end
		
		function CreateVehicleModelThing( k, v )
			local iconr, icong, iconb = 85, 85, 85
			local PANELModelCnotainer = vgui.Create( "DPanel", FRAMEFuelMain )
			PANELModelCnotainer:SetSize( 64, 64 )
			PANELModelCnotainer.Paint = function( self, w, h )
				draw.RoundedBox( 4, 0, 0, w, h, Color( iconr, icong, iconb ) )
			end
			
			local MODELCarThumb = vgui.Create( "DModelPanel", PANELModelCnotainer )
			MODELCarThumb:SetSize( 64, 64 )
			MODELCarThumb:SetModel( k )
			MODELCarThumb:SetColor( Color( 255, 255, 255 ) )
			MODELCarThumb:SetCamPos( Vector( 200, 200, 200 ) )
			MODELCarThumb.LayoutEntity = function( Entity ) return end
			MODELCarThumb:SetParent( PANELModelCnotainer )
			
			local BUTTONInfoMenu = vgui.Create( "DButton", MODELCarThumb )
			BUTTONInfoMenu:SetSize( 64, 64 )
			BUTTONInfoMenu:SetPos( 0, 0 )
			BUTTONInfoMenu:SetText( " " )
			BUTTONInfoMenu.DoClick = function() 
				local x, y = gui.MousePos()
				local MENUOptions = vgui.Create( "DMenu" )
				local MENUOptionOne = MENUOptions:AddOption( "Remove" )
				MENUOptionOne:SetIcon( "icon16/cross.png" )
				MENUOptionOne.DoClick = function()
					FUEL_ModelTable[k] = false
					PANELModelCnotainer:Remove()
					chat.AddText( Color( 100, 200, 255 ), "[FUEL] ", Color( 255, 255, 255 ), "Vehicle removed! (not saved)" )
				end
				MENUOptions:SetPos( x, y )
				MENUOptions:AddSpacer()
				local MENUInfo = MENUOptions:AddSubMenu( "Information" )
				local MENUInfoOption = MENUInfo:AddSubMenu( "Fuel" )
				local MENUInfoOptionOne = MENUInfoOption:AddOption( FUEL_ModelTable[k] ):SetIcon( "icon16/key.png" )
				local MENUInfoOptionTwo = MENUInfo:AddSubMenu( "Model" )
				local MenuInfoOptionTwoInfo = MENUInfoOptionTwo:AddOption( k )
				MenuInfoOptionTwoInfo:SetIcon( "icon16/eye.png" )
				MenuInfoOptionTwoInfo.DoClick = function()
					SetClipboardText( k )
				end 
				MENUOptions:AddSpacer()
				local MENUOptionTwo = MENUOptions:AddOption( "Edit" )
				MENUOptionTwo:SetIcon( "icon16/page_white_edit.png" )
				MENUOptionTwo.DoClick = function()
					TEXTModelName:SetText( k )
					TEXTFuelAmt:SetText( FUEL_ModelTable[k] )
					MODELCarView:SetModel( k )
				end
				MENUOptions:Open()
			end
			BUTTONInfoMenu.Paint = function( self, w, h ) end
			BUTTONInfoMenu.OnCursorEntered = function()
				iconr, icong, iconb = 85, 150, 85
			end
			BUTTONInfoMenu.OnCursorExited = function()
				iconr, icong, iconb = 85, 85, 85
			end
			
			SCROLLCarsList:AddItem( PANELModelCnotainer )
		end
		
		for k, v in pairs( FUEL_ModelTable ) do
			CreateVehicleModelThing( k, v )
		end
		
		LABELModelName:SetPos( 310, 34 )
		LABELModelName:SetText( "Model Name: (Must end in .mdl)" )
		LABELModelName:SetFont( "FUEL_FontLabel" )
		LABELModelName:SizeToContents()
		
		TEXTModelName:SetSize( 230, 20 )
		TEXTModelName:SetPos( 310, 50 )
		TEXTModelName:SetText( "models/" )
		TEXTModelName.OnTextChanged = function()
			MODELCarView:SetModel( TEXTModelName:GetValue() )
		end

		BUTTONUpdateModel:SetSize( 50, 20 )
		BUTTONUpdateModel:SetPos( FRAMEFuelMain:GetWide() - 60, 50 )
		BUTTONUpdateModel:SetText( " " )
		BUTTONUpdateModel.DoClick = function() MODELCarView:SetModel( TEXTModelName:GetValue() ) end
		BUTTONUpdateModel.Paint = function( pan, w, h )
			draw.RoundedBoxEx( 8, 0, 0, w, h, Color( 50, 50, 50 ), false, true, false, true )
			draw.RoundedBoxEx( 8, 0, 1, w-1, h-2, Color( 255, 255, 255 ), false, true, false, true )
			draw.DrawText( "Update", "FUEL_FontScrollbar", 22, 0, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
			draw.DrawText( "Model", "FUEL_FontScrollbar", 22, 8, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
		end
		
		
		LABELFuelAmt:SetPos( 310, 70 )
		LABELFuelAmt:SetText( "Fuel Amount: (Default: 100)" )
		LABELFuelAmt:SetFont( "FUEL_FontLabel" )
		LABELFuelAmt:SizeToContents()
		
		TEXTFuelAmt:SetSize( 280, 20 )
		TEXTFuelAmt:SetPos( 310, 86 )
		TEXTFuelAmt:SetText( "100" )
		
		LABELModelView:SetPos( 310, 106 )
		LABELModelView:SetText( "Model View:" )
		LABELModelView:SetFont( "FUEL_FontLabel" )
		LABELModelView:SizeToContents()
		
		PANELCarView:SetSize( 280, 200 )
		PANELCarView:SetPos( 310, 124 )
		PANELCarView.Paint = function( self, w, h )
			draw.RoundedBox( 4, 0, 0, w, h, Color( 85, 85, 85 ) )
		end
			
		MODELCarView:SetPos( 0, 0 )
		MODELCarView:SetSize( 280, 200 )
		MODELCarView:SetCamPos( Vector( 200, 200, 200 ) )
		MODELCarView.LayoutEntity = function( Entity ) return end
		
		local addgreen = 150
		BUTTONAddVehicle:SetSize( 280, 24 )
		BUTTONAddVehicle:SetPos( 310, 330 )
		BUTTONAddVehicle:SetText( " " )
		BUTTONAddVehicle.DoClick = function() 
			if FUEL_ModelTable[TEXTModelName:GetValue()] then
				FUEL_ModelTable[TEXTModelName:GetValue()] = tonumber(TEXTFuelAmt:GetValue())
				chat.AddText( Color( 100, 200, 255 ), "[FUEL] ", Color( 255, 255, 255 ), "Vehicle updated! (not saved)" )
			else
				FUEL_ModelTable[TEXTModelName:GetValue()] = tonumber(TEXTFuelAmt:GetValue())
				CreateVehicleModelThing( TEXTModelName:GetValue(), tonumber(TEXTFuelAmt:GetValue()) )
				chat.AddText( Color( 100, 200, 255 ), "[FUEL] ", Color( 255, 255, 255 ), "Vehicle added! (not saved)" )
			end
		end
		BUTTONAddVehicle.Paint = function( pan, w, h )
			draw.RoundedBox( 8, 0, 0, w, h, Color( 100, addgreen, 100  ) )
			draw.DrawText( "Add/Update Vehicle", "FUEL_FontLabel", 141, 5, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
			draw.DrawText( "Add/Update Vehicle", "FUEL_FontLabel", 140, 4, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
			surface.SetDrawColor( 200, 200, 200 )
			surface.SetMaterial( Material( "icon16/arrow_left.png" ) )
			surface.DrawTexturedRect( 6, 1, 22, 22 )
		end
		BUTTONAddVehicle.OnCursorEntered = function() addgreen = 255 end
		BUTTONAddVehicle.OnCursorExited = function () addgreen = 150 end
		
		local saveblue = 150
		BUTTONSaveChanges:SetSize( 135, 24 )
		BUTTONSaveChanges:SetPos( 310, 360 )
		BUTTONSaveChanges:SetText( " " )
		BUTTONSaveChanges.DoClick = function() 
			FUEL_SendTable( )
			chat.AddText( Color( 100, 200, 255 ), "[FUEL] ", Color( 255, 255, 255 ), "Changes have been saved!" )
			FRAMEFuelMain:Close()
		end
		BUTTONSaveChanges.Paint = function( pan, w, h )
			draw.RoundedBox( 8, 0, 0, w, h, Color( 100, 100, saveblue  ) )
			draw.DrawText( "Save Changes", "FUEL_FontLabel", 68.25, 5, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
			draw.DrawText( "Save Changes", "FUEL_FontLabel", 67.25, 4, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
		end
		BUTTONSaveChanges.OnCursorEntered = function() saveblue = 255 end
		BUTTONSaveChanges.OnCursorExited = function () saveblue = 150 end
		
		local discardred = 150
		BUTTONDiscardChanges:SetSize( 135, 24 )
		BUTTONDiscardChanges:SetPos( 455, 360 )
		BUTTONDiscardChanges:SetText( " " )
		BUTTONDiscardChanges.DoClick = function() 
			FUEL_RequestTable( )
			chat.AddText( Color( 100, 200, 255 ), "[FUEL] ", Color( 255, 255, 255 ), "Changes have been discarded!" )
			FRAMEFuelMain:Close()
		end
		BUTTONDiscardChanges.Paint = function( pan, w, h )
			draw.RoundedBox( 8, 0, 0, w, h, Color( discardred, 100, 100  ) )
			draw.DrawText( "Discard Changes", "FUEL_FontLabel", 68.25, 5, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
			draw.DrawText( "Discard Changes", "FUEL_FontLabel", 67.25, 4, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
		end
		BUTTONDiscardChanges.OnCursorEntered = function() discardred = 255 end
		BUTTONDiscardChanges.OnCursorExited = function () discardred = 150 end
	else
		local LABELUnavailable = vgui.Create( "DLabel", FRAMEFuelMain )
		LABELUnavailable:SetPos( 30, 170 )
		LABELUnavailable:SetText( "You need to be Admin or Superadmin to edit vehicle data!" )
		LABELUnavailable:SetFont( "FUEL_FontPanelHeader" )
		LABELUnavailable:SizeToContents()
	end
end)

--[[     ADD MODEL COMMAND        ]]--
concommand.Add( "FUEL_GetModel", function( )
	if LocalPlayer():GetEyeTrace().Entity:IsVehicle() then
		chat.AddText( Color( 100, 200, 255 ), "[FUEL] ", Color( 255, 255, 255 ), LocalPlayer():GetEyeTrace().Entity:GetModel() )
	else
		chat.AddText( Color( 100, 200, 255 ), "[FUEL] ", Color( 255, 255, 255 ), "That is not a vehicle/it has no model!" )
	end
end) 

--[[  ASK FOR NEW TABLE ON CLOSE  ]]--
function FUEL_RequestTable( )
	net.Start( "FUEL_NetAskTable")
		net.WriteEntity( LocalPlayer( ) )
	net.SendToServer( )
end

--[[   SEND NEW TABLE TO SERVER   ]]--
function FUEL_SendTable( )
	net.Start( "FUEL_NetNewTable")
		net.WriteTable( FUEL_ModelTable )
	net.SendToServer( )
end

--[[   RECIEVE ASKED-FOR TABLE    ]]--
function FUEL_RecieveTable( )
	FUEL_ModelTable = net.ReadTable( )
end
net.Receive( "FUEL_NetSendTable", FUEL_RecieveTable )