AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include( "shared.lua" )

function ENT:Initialize( )
	self:SetModel( "models/props_c17/FurnitureShelf001b.mdl" )
   -- self:SetMaterial("models/effects/vol_light001" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
		
    local phys = self:GetPhysicsObject()
	if ( phys:IsValid() ) then
		phys:Wake()
	end

end

function ENT:OnTakeDamage( dmg ) 
	return false
end


function ENT:Use( activator, caller )
    if not activator.nextgasbuy then activator.nextgasbuy = 0 end
    if activator.nextgasbuy > CurTime() then return end
    activator.nextgasbuy = CurTime() + 2





    umsg.Start("gb_gasstation_menu", caller, price)
    umsg.End()

end

if SERVER then
    util.AddNetworkString("Gb_gasstation_BuyGas")
    net.Receive("Gb_gasstation_BuyGas", function(length, ply)
        local price = 0
        local Vehicle
        for k, v in pairs(ents.FindInSphere(ply:GetPos(),VEHUPGRADE_VehicleToNPCDistance)) do
            --local doorData = v:getDoorData()
            if v.VehicleTable and v.OwnerID == ply:SteamID() then
                 price = (v:FUEL_GetMaxFuel() - (v.FUEL_Amount)) * 5
                 Vehicle = v
                break
            end
        end


        if not IsValid(Vehicle) then
            DarkRP.notify(ply, 1, 5,  "Make sure your car is close the gas station!")
            return
        end

        if ply:getDarkRPVar("money") < price then
            DarkRP.notify(ply, 1, 5,  "You don't have enough money to buy gas, you need $"..price.." to fill up your car")
            return
        end

        DarkRP.notify(ply, 1, 5,  "Car's fuel is refilled for $"..price)
        Vehicle:FUEL_SetMaxFuel( )
        Vehicle:EmitSound( "player/footsteps/wade4.wav", 100, 100 )

        ply:addMoney(-price);

    end)

    util.AddNetworkString("Gb_gasstation_BuyJerrycan")
    net.Receive("Gb_gasstation_BuyJerrycan", function(length, ply)

        if ply:getDarkRPVar("money") < 500 then
            DarkRP.notify(ply, 1, 5,  "You don't have enough money to buy gas, you need $500 for a jerrycan")
            return
        end

        local jerrycan = ents.Create("jerrycan")
        jerrycan:SetPos(ply:GetPos() + Vector(0,0,100))
        jerrycan:SetAngles(ply:GetAngles())
        jerrycan:Spawn()
        DarkRP.notify(ply, 1, 5,  "Bought a jerry can for $500")
        ply:addMoney(-500);

    end)
end