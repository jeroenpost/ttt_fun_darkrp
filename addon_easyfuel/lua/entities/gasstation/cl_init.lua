include( "shared.lua" )

function gb_gasstation_menu( price )
    local frame = vgui.Create("DFrame")
    frame:SetTitle( "Gas Station" )
    frame:SetSize(300,120)
    frame:Center()
    frame:MakePopup()

    local button = vgui.Create("DButton",frame)
    button:SetPos(30,30)
    button:SetSize(240,30)
    button:SetText("Fill up your car for $5 per gallon")
    button.DoClick = function()
         net.Start("Gb_gasstation_BuyGas")
         net.SendToServer()
         frame:SetVisible(false)
    end

    local button = vgui.Create("DButton",frame)
    button:SetPos(30,75)
    button:SetSize(240,30)
    button:SetText("Buy a full Jerrycan $500")
    button.DoClick = function()
        net.Start("Gb_gasstation_BuyJerrycan")
        net.SendToServer()
        frame:SetVisible(false)
    end

end
usermessage.Hook("gb_gasstation_menu", gb_gasstation_menu)