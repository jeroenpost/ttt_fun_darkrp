include( "shared.lua" )

surface.CreateFont( "FUEL_FontEntityNumbers", {
	font = "Arial",
	size = 72,
	weight = 500,
	antialias = true,
} )
surface.CreateFont( "FUEL_FontEntityLetters", {
	font = "Arial",
	size = 24,
	weight = 500,
	antialias = true,
} )

function ENT:Draw( )
    self:DrawModel( )
	self:SetColor( Color( 255, 50, 50 ) )
	local pos = self:GetPos( )
	local ang = self:GetAngles( )
	
	ang:RotateAroundAxis( ang:Forward(), 90 )
	ang:RotateAroundAxis( ang:Right(), 90 )
	
	cam.Start3D2D( pos + ang:Up() * 4 - ang:Forward() * 8.5 - ang:Right() * 6.5, ang, 0.1 )
		draw.RoundedBox( 0, 0, 0, 170, 165, Color( 255, 200, 200, 50 ) )
		draw.RoundedBox( 0, 0, 0, 170, 25, Color( 255, 75, 75, 255 ) )
		draw.DrawText( "Jerry Can", "FUEL_FontEntityLetters", 86, 1, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
		draw.DrawText( "Jerry Can", "FUEL_FontEntityLetters", 85, 0, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
		draw.DrawText( self:GetNWInt( "FUEL_Carrying" ).."%", "FUEL_FontEntityNumbers", 86, 26, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
		draw.DrawText( self:GetNWInt( "FUEL_Carrying" ).."%", "FUEL_FontEntityNumbers", 85, 25, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
		draw.DrawText( "F     U     E     L", "FUEL_FontEntityLetters", 86, 91, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
		draw.DrawText( "F     U     E     L", "FUEL_FontEntityLetters", 85, 90, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
	cam.End3D2D()
	
	ang:RotateAroundAxis( ang:Right(), 180 )
	cam.Start3D2D( pos + ang:Up() * 4 - ang:Forward() * 8.5 - ang:Right() * 6.5, ang, 0.1 )
		draw.RoundedBox( 0, 0, 0, 170, 165, Color( 255, 200, 200, 50 ) )
		draw.RoundedBox( 0, 0, 0, 170, 25, Color( 255, 75, 75, 255 ) )
		draw.DrawText( "Jerry Can", "FUEL_FontEntityLetters", 86, 1, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
		draw.DrawText( "Jerry Can", "FUEL_FontEntityLetters", 85, 0, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
		draw.DrawText( self:GetNWInt( "FUEL_Carrying" ).."%", "FUEL_FontEntityNumbers", 86, 26, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
		draw.DrawText( self:GetNWInt( "FUEL_Carrying" ).."%", "FUEL_FontEntityNumbers", 85, 25, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
		draw.DrawText( "F     U     E     L", "FUEL_FontEntityLetters", 86, 91, Color( 50, 50, 50 ), TEXT_ALIGN_CENTER )
		draw.DrawText( "F     U     E     L", "FUEL_FontEntityLetters", 85, 90, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER )
	cam.End3D2D()
end


