AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include( "shared.lua" )


function ENT:Initialize( )
	self:SetModel( "models/props_junk/metalgascan.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
		
    local phys = self:GetPhysicsObject()
	if ( phys:IsValid() ) then
		phys:Wake()
	end
	
	self.FUEL_Carrying = 100
	self.FUEL_CanUse = true
	self:SetNWInt( "FUEL_Carrying", math.Round( self.FUEL_Carrying ) )
end

function ENT:OnTakeDamage( dmg ) 
	return false
end

function ENT:Use( activator, caller )
    return
end

function ENT:Touch( hitEnt )
 	if IsValid( hitEnt ) then
 		if hitEnt:IsVehicle( ) then
			if (hitEnt:FUEL_GetMaxFuel( ) != nil) then
				if self.FUEL_CanUse then
					if (hitEnt:FUEL_GetFuel( ) == hitEnt:FUEL_GetMaxFuel( )) then return end
					if (self.FUEL_Carrying >= hitEnt:FUEL_GetMaxFuel( ) - hitEnt:FUEL_GetFuel( )) then
						self.FUEL_Carrying = self.FUEL_Carrying-(hitEnt:FUEL_GetMaxFuel( ) - hitEnt:FUEL_GetFuel( ))
						self:SetNWInt( "FUEL_Carrying", math.Round( self.FUEL_Carrying ) )
						self:EmitSound( "player/footsteps/wade4.wav", 100, 50 )
						self:EmitSound( "physics/metal/metal_box_impact_hard3.wav" )
						hitEnt:FUEL_SetMaxFuel( )
						print( "1" )
					else
						self:EmitSound( "player/footsteps/wade4.wav", 100, 50 )
						self:EmitSound( "physics/metal/metal_box_impact_hard3.wav" )
						hitEnt:FUEL_SetFuel( hitEnt:FUEL_GetFuel( ) + self.FUEL_Carrying )
						self.FUEL_CanUse = false
						self:Remove( )
						print( "2" )
					end
					if (self.FUEL_Carrying <= 0) then
						self:Remove( )
					end
				end
			end
		end
	end
 end