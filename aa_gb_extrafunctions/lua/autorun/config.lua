-- Global Config File
if SERVER then
   AddCSLuaFile( "config.lua" )
end

if( not ConVarExists( "server_id" ) ) then 
    CreateConVar( "server_id", "9999", { FCVAR_ARCHIVE }, "The ID of the Server" ) 
end

server_id = GetConVarNumber("server_id");


hook.Add("PlayerInitialSpawn","syncConvars", function()
    RunConsoleCommand( "server_id", GetConVar("server_id"):GetString() )
end)



_globals = {}
_globals['slayed'] = {}
_globals['specialperson'] = {}


heavyserver = true


serverSettings = {}

AFKCONFIG = {}
AFKCONFIG.warnTime = 3 --This is the time in minuits it takes before the warning message is giving
AFKCONFIG.kickTime = 9999999
AFKCONFIG.mainWarnMessage = "You are AFK"-- Message to be displayed when the user is warned, Dont make this too long of it may go off the screen
AFKCONFIG.subWarnMessage = "It's fine, but you'll get demoted from your job soon,"-- Sub message to be displayed when the user is warned
AFKCONFIG.subWarnMessage2 = "you don't get salary/XP anymore and playtime doesn't count"
AFKCONFIG.subWarnMessage3 = ""
AFKCONFIG.kickReason = "You were kicked for being AFK too long" -- If you dont know this then you are stupid, JK {CODE BLUE} LOVES EVERYONE!
AFKCONFIG.souldKickAdmins = false --Should it kick addmins for being AFK?