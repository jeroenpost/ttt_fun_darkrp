

--Check if ply is admin
--Check if ply is admin



if SERVER then


    timer.Simple(10,function()
        RunConsoleCommand("ulx","groupallow","user","ulx dlog");
        RunConsoleCommand("ulx","groupallow","user","ulx dlogs");
    end)

 local gb_damagelog = {}



 local lasthit = false
local lasthit2 = false
local lasthitdmg = false
 hook.Add("EntityTakeDamage","makesomedamagelogs", function( ent, dmginfo)

     local inflictor = dmginfo:GetInflictor( )

     if ent:IsPlayer() and inflictor:IsPlayer() then

         local damage =dmginfo:GetDamage()
         if lasthit == inflictor and lasthit2 == ent then
            table.remove(gb_damagelog)
             lasthitdmg = lasthitdmg + damage
             table.insert(gb_damagelog, os.date( "%I:%M" )..": "..inflictor:Nick().."  ("..inflictor:SteamID()..") damaged "..ent:Nick().." ("..ent:SteamID()..") with "..lasthitdmg.." Combined damage" )
         else
             lasthitdmg = damage
             table.insert(gb_damagelog, os.date( "%I:%M" )..": "..inflictor:Nick().."  ("..inflictor:SteamID()..") damaged "..ent:Nick().." ("..ent:SteamID()..") with "..lasthitdmg.." damage" )
         end

         if damage >= ent:Health() then
             table.insert(gb_damagelog, inflictor:Nick().." ("..inflictor:SteamID()..") KILLED "..ent:Nick().." ("..ent:SteamID()..")" )
         end

         lasthit = inflictor
         lasthit2 = ent

     end
     if #gb_damagelog > 250 then
         table.remove(gb_damagelog, 1)
     end

 end)

 hook.Add("playerArrested","gb_dlogs_arrested",function(ply,thetime,ply2)
    if IsValid(ply) and IsValid(ply2) and tonumber(thetime) > 1 then
        table.insert(gb_damagelog, "ARREST: "..ply2:Nick().." ("..ply2:SteamID()..") arrested "..ply:Nick().."  ("..ply:SteamID()..") for "..thetime.." seconds" )
    end
 end)

    hook.Add('playerUnArrested', 'gb_dlogs_unarrested', function(ply, ply2)
        if IsValid(ply) and IsValid(ply2) then
            table.insert(gb_damagelog, "UNARREST: "..ply2:Nick().." ("..ply2:SteamID()..") unarrested "..ply:Nick().."  ("..ply:SteamID()..") " )
        end
    end)

    hook.Add("PlayerDeath", "gb_dlogs_pdeath", function(ply,inflictor, attacker)
        if IsValid(ply) and IsValid(attacker) then
            if ply == attacker then
                table.insert(gb_damagelog, "SUICIDE: "..ply:Nick().." ("..ply:SteamID()..") SUICIDED" )
            else
           -- table.insert(gb_damagelog, "KILL: "..ply:Nick().." ("..ply:SteamID()..") killed "..attacker:Nick().."  ("..attacker:SteamID()..") " )
            end
        end
    end)

    hook.Add("OnPlayerChangedTeam","gb_addjobchangetologs",function(ply,num1,num2)
        if IsValid(ply)  then
            table.insert(gb_damagelog, "JOBCHANGE: "..ply:Nick().." ("..ply:SteamID()..") changed from "..gb_getTeamNameByNumber(num1).."  to "..gb_getTeamNameByNumber(num2).." " )
        end
    end)

 -- Hook for damagelogs
 hook.Add("addToDamageLog","gb_dlogs_adddamagelost",function(message)
         table.insert(gb_damagelog, message )
 end)

 -- Hit completed
 hook.Add("onHitCompleted","gb_dlogs_hitcomplete",function(hitman, target, customer)
     if IsValid(hitman) and IsValid(target)  and IsValid(customer) then
         table.insert(gb_damagelog, "HIT-COMPLETE: "..hitman:Nick().."  ("..hitman:SteamID()..") killed "..target:Nick().."  ("..target:SteamID().."). Requested by: "..customer:Nick().." ("..customer:SteamID()..")" )
     end
 end)

 -- Hit accepted
 hook.Add("onHitAccepted","gb_dlogs_hitaccepted",function(hitman, target, customer)
     if IsValid(hitman) and IsValid(target)  and IsValid(customer) then
         table.insert(gb_damagelog, "HIT-ACCEPTED: "..hitman:Nick().."  ("..hitman:SteamID()..") Accepted to kill "..target:Nick().."  ("..target:SteamID().."). Requested by: "..customer:Nick().." ("..customer:SteamID()..")" )
     end
 end)


 hook.Add("onPlayerDemoted","gb_dlogs_demoted",function(ply2,ply,reason )
     if IsValid(ply) and IsValid(ply2)  then
         table.insert(gb_damagelog, "DEMOTE: "..ply2:Nick().."  ("..ply2:SteamID()..") demoted "..ply:Nick().."  ("..ply:SteamID().."). Reason: "..reason )
     end
 end)

 util.AddNetworkString("admin_getDamageLogFromServer")
 util.AddNetworkString("admin_getDamageLog")

  net.Receive( "admin_getDamageLogFromServer", function(len,ply)

      if not gb.is_jrmod(ply) and not gb.is_donator(ply) then return end

     local damagelog = ''
     local good = "<span style='color:green;'>"
     local hgood = "<span style='color:orange;'>"
     local bad = "<span style='color:red;'>"
     local lelse = "<span style='color:blue;'>"
     local lend = "</span><br/>"

     for k, txt in ipairs(gb_damagelog) do

        local killl = string.find( txt, " KILLED ")

        txt = string.gsub(txt, "<", "_")
        txt = string.gsub(txt, ">", "_")

		if killl  then
		 good = "<div style='color:white;background:green;font-weight:bold;margin:3px;'>"
		 hgood = "<div style='color:white;background:#698705;font-weight:bold;margin:3px;'>"
		 bad = "<div style='color:white;background:red;font-weight:bold;margin:3px;'>"
		 lelse = "<div style='color:#050587;font-weight:bold;margin:3px;'>"
		 lend = "</div>"
		else
		 good = "<span style='color:#1E5520;'>"
		 hgood = "<span style='color:#53551E;'>"
		 bad = "<span style='color:#551E1E;'>"
		 lelse = "<span style='color:#1E1E55;'>"
		 lend = "</span><br/>"
        end


        damagelog = damagelog..hgood..txt..lend


     end


     damagelog = "<div style='background:white;color:black;padding:10px;font-size:11px;font-family:Arial,sans'>"..damagelog.."<br/><br/>\n*** Damage log end.</div>"
     print('Sending the damagelog to '..ply:Nick())
     net.Start("admin_getDamageLog")
      local data = util.Compress(damagelog)
      net.WriteUInt(#data, 32);
      net.WriteData( data, #data )
      net.Send(ply)
  end)

end


if CLIENT then

    local dlogsHtml = {}
    local dlogFrame = {}

      net.Receive( "admin_getDamageLog", function()
          print('receiving damagelogs')
          local leng =  net.ReadUInt(32);
          local data = net.ReadData(leng)
          data =  util.Decompress(data)
          if IsValid(dlogsHtml) then
              dlogsHtml:SetHTML( leng.." "..data )
          else
              print("Dlogs windows is closed")
          end
      end)


    local function gb_print_damagelog( ply)
      if (not IsValid(ply)) then return end
     -- if gb.is_jrmod(ply) or   then
        if true then


           dlogFrame = vgui.Create("DFrame")
           dlogFrame:SetPos(200, 100)
           dlogFrame:SetSize(790, 600)
           dlogFrame:SetTitle("TTT-FUN Damage Logs viewer")
           dlogFrame:SetVisible(true)
           dlogFrame:SetDraggable(true)
           dlogFrame:ShowCloseButton(true)
           dlogFrame:SetScreenLock(true)
           dlogFrame:Center()
           dlogFrame:SetBackgroundBlur( true )
           dlogFrame.Paint = function()
              draw.RoundedBox( 8, 0, 0, dlogFrame:GetWide(), dlogFrame:GetTall(), Color( 0, 0, 0, 252 ) )
           end
          dlogFrame:MakePopup()

        dlogsHtml = vgui.Create( "HTML", dlogFrame )
        dlogsHtml:SetPos( 20, 20 )
        dlogsHtml:SetSize( 750, 560 )
        --dlogsHtml:SetHTML( "<div style='color:white'>Loading logs....</div>" )



          net.Start("admin_getDamageLogFromServer")
	    net.WriteEntity( ply ) --Dummy thing
          net.SendToServer()

      end
    end
concommand.Add("gb_print_damagelog", gb_print_damagelog)

end


