
-- Regen health after time
timer.Create("GiveHeathevery15seconds",15,0,function()
    for f, v in pairs(player.GetAll()) do
        if v:Health() < 100 then
            v:SetHealth(v:Health()+1)
        end
        if v:Health() > 200 then
            v:SetHealth(200)
        end
        if v:Armor() > 100 then
            v:SetArmor(100)
        end
    end
end)