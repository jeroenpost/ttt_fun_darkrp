--resource.AddWorkshop( "117935890" ) -- Rp_Downtown_v4c_v3   25MB
print("Loaded resources - Server")


resource.AddWorkshop( "248302805" ) -- DARKRP
resource.AddWorkshop( "106516163" ) -- PlayX                800KB
resource.AddWorkshop( "231869336" ) -- DarkRP TTT-FUn
--resource.AddWorkshop( "112986621" ) -- Drugs Mod            3.5 MB
resource.AddWorkshop( "231694111" ) -- Fire system          3.5 MB
resource.AddWorkshop( "180507408" ) -- FA:S
resource.AddWorkshop("255762648") -- TTT-FUN Camos


resource.AddWorkshop("549224132") -- Skateboards
resource.AddWorkshop("159078744") -- Skater mega pack (needed for skateboards)


resource.AddWorkshop( "121961644" ) -- Police models
resource.AddWorkshop( "233519629" ) -- Agent Smith
resource.AddWorkshop( "204120003" ) -- MAffia playermodels
resource.AddWorkshop( "341518179" ) -- General Scales


resource.AddWorkshop( "536740257" ) -- Santa Clause

resource.AddWorkshop( "262279954" ) -- Dragon

resource.AddWorkshop( "311688648" ) -- Jane the killer (vampire)
resource.AddWorkshop( "232066478" ) -- Arcade Machines

resource.AddWorkshop( "150455514"  ) -- Hoverboards

resource.AddWorkshop( "471076772" ) -- Bitcoin miner

resource.AddWorkshop( "388725208" ) -- Grafity swep


resource.AddWorkshop( "307537243" ) -- TTT-FUN Cars
resource.AddWorkshop( "119146471" ) -- TDM Bugatti
resource.AddWorkshop( "144415557" ) -- Police Cars + crown victoria
resource.AddWorkshop( "112606459" ) -- TDM Base             7.8 MB
resource.AddWorkshop( "225810491" ) -- TDM Multibrand       31 MB

local  currentMap = string.lower(game.GetMap())

if currentMap == "rp_downtown_evilmelon_v1" then
    resource.AddWorkshop( "145177582" ) -- MAP
end
if currentMap == "rp_california_r" then
    resource.AddWorkshop( "259346741" ) -- MAP
end

if currentMap == "rp_downtown_v4c_v3" then
    resource.AddWorkshop( "117935890" ) -- MAP
end

if currentMap == "rp_downtown_v4c_v3" then
    resource.AddWorkshop( "117935890" ) -- MAP
end

if currentMap == "rp_downtown_v4c_v3_chirax" then
    resource.AddWorkshop( "411063554" ) -- MAP
end

resource.AddFile("resource/fonts/bebas.ttf")
resource.AddFile("resource/fonts/bebasneue.ttf")

resource.AddFile("sound/web/webfire.mp3")
resource.AddFile("materials/gui/ttt-fun.png")