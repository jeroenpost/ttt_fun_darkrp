ulx.nr_roles = {}
local function updatenrroles()
    table.Empty( ulx.nr_roles )
    table.insert(ulx.nr_roles,"traitor")
    table.insert(ulx.nr_roles,"detective")
    table.insert(ulx.nr_roles,"innocent")

end
hook.Add( ULib.HOOK_UCLCHANGED, "ULXNRRolesUpdate", updatenrroles )
updatenrroles() -- Init

function ulx.tttnrforcerole(calling_ply, target_ply, role, rounds)

    role = string.lower(role)
    local tplyname = target_ply:Nick()

    rounds = tonumber(rounds)
    if rounds < 1 then
        rounds = 1
    end

    if (role == "traitor") then
        ULib.tsayColor( calling_ply, false, Color( 255,64,64 ), tplyname, Color( 210,180,140 ), " will be fored to ", Color( 0,154,205 ), "Traitor", Color( 210,180,140 ), " next Round!" )
        target_ply:SetPData( "IPDIforceTnr", 1)

    elseif (role == "detective") then
        ULib.tsayColor( calling_ply, false, Color( 255,64,64 ), tplyname, Color( 210,180,140 ), " will be fored to ", Color( 0,154,205 ), "Detective", Color( 210,180,140 ), " next Round!" )
        target_ply:SetPData( "IPDIforceDnr", 1)

    elseif (role == "innocent") then
        ULib.tsayColor( calling_ply, false, Color( 255,64,64 ), tplyname, Color( 210,180,140 ), " will be fored to ", Color( 0,154,205 ), "Innocent", Color( 210,180,140 ), " for ", Color( 162,205,90 ), ""..rounds.."", Color( 210,180,140 )," Round!" )
        target_ply:SetPData( "IPDIforceInr", rounds)

    else
        ULib.tsayError( calling_ply, "Role unkown..", true )
    end

end
local tttnrforcerole = ulx.command( "TTT Admin", "ulx tttnrforcerole", ulx.tttnrforcerole, "!fnr" )
tttnrforcerole:addParam{ type=ULib.cmds.PlayerArg }
tttnrforcerole:addParam{ type=ULib.cmds.StringArg, completes=ulx.nr_roles, hint="NR Role", error="invalid role \"%s\" specified", ULib.cmds.restrictToCompletes }
tttnrforcerole:addParam{ type=ULib.cmds.StringArg, hint="Rounds (Inno ONLY)" }
tttnrforcerole:defaultAccess( ULib.ACCESS_ADMIN )
tttnrforcerole:help( "Force player to choosen role in follwing round." )





local CATEGORY_NAME = "TTT Admin"

--[[corpse_remove][removes the corpse given.]
@param  {[Ragdoll]} corpse [The corpse to be removed.]
--]]
function corpse_remove(corpse)
    CORPSE.SetFound(corpse, false)
    if string.find(corpse:GetModel(), "zm_", 6, true) then
        corpse:Remove()
    elseif corpse.player_ragdoll then
        corpse:Remove()
    end
end

--[[corpse_identify][identifies the given corpse.]
@param  {[Ragdoll]} corpse [The corpse to be identified.]
--]]
function corpse_identify(corpse)
    if corpse then
        local ply = player.GetByUniqueID(corpse.uqid)
        ply:SetNWBool("body_found", true)
        CORPSE.SetFound(corpse, true)
    end
end
--[End]----------------------------------------------------------------------------------------

function corpse_find(v)
    for _, ent in pairs( ents.FindByClass( "prop_ragdoll" )) do
        if ent.uqid == v:UniqueID() and IsValid(ent) then
            return ent or false
        end
    end
end

------------------------------ Startspecialround ------------------------------
function ulx.startspecialround( calling_ply,  rounde )
    rounde = tonumber(rounde)
    if specialRound.Modes[rounde] ~= nil then
        specialRound.forcedRound =  rounde
        RunConsoleCommand("ttt_roundrestart")
        timer.Simple(10,function() specialRound.forcedRound = false end)
        if IsValid(calling_ply) then
        calling_ply:PS_Notify( "yay, special round" )
            end
    else
        if IsValid(calling_ply) then calling_ply:PS_Notify( "Round doesnt exist" ) end
    end
end
local startspecialround = ulx.command( "TTT Fun", "ulx startspecialround", ulx.startspecialround, "!startspecialround" )
startspecialround:addParam{ type=ULib.cmds.NumArg, min=1, max = 20, default=1, hint="round", ULib.cmds.round }
startspecialround:defaultAccess( ULib.ACCESS_SUPERADMIN )
startspecialround:help( "Starts a special round." )

------------------------------ Snowballs ------------------------------
function ulx.snowballs( calling_ply, target_plys, hours )
    specialRound.snowballs()
    ulx.fancyLogAdmin( calling_ply, "#A Started a snowballfight")
end
local snowballs = ulx.command( "TTT Fun", "ulx snowballs", ulx.snowballs, "!snowballs" )

snowballs:defaultAccess( ULib.ACCESS_SUPERADMIN )
snowballs:help( "Start a snowballfight" )

------------------------------ sent to 13 ------------------------------
function ulx.s13( calling_ply, target_plys )

    for i=1, #target_plys do
        target_plys[ i ]:SendLua("LocalPlayer():ConCommand('connect 46.21.152.74:27030')")
    end

    ulx.fancyLogAdmin( calling_ply, "#A threw #T to server 13", target_plys )
end
local s13 = ulx.command( "TTT Fun", "ulx s13", ulx.s13, "!s13" )
s13:addParam{ type=ULib.cmds.PlayersArg }

s13:defaultAccess( ULib.ACCESS_ADMIN )
s13:help( "Throw person to server 13 (RDMMER)" )



------------------------------ DeathTalking Admin ------------------------------
function ulx.talkwhiledeath( calling_ply, target_plys, seconds )
	 
    for i=1, #target_plys do
		--target_plys[ i ]:ConCommand("setPlayerTime "..amount)
		target_plys[ i ].IsAbleToTalkWhileDeath = true
		target_plys[i]:PS_Notify( "You are able to talk for "..seconds.." seconds while death" )
                timer.Simple(seconds, function()
                    if IsValid(target_plys[ i ]) then
                        target_plys[ i ].IsAbleToTalkWhileDeath = false
                    end
                end)
    end
    
ulx.fancyLogAdmin( calling_ply, "#A set #T to be able to talk to the living for #i seconds", target_plys, seconds )
end
local talkwhiledeath = ulx.command( "TTT Fun", "ulx talkwhiledeath", ulx.talkwhiledeath, "!talkwhiledeath" )
talkwhiledeath:addParam{ type=ULib.cmds.PlayersArg }
talkwhiledeath:addParam{ type=ULib.cmds.NumArg, min=15, max = 300, default=60, hint="Time in seconds", ULib.cmds.round }
talkwhiledeath:defaultAccess( ULib.ACCESS_SUPERADMIN )
talkwhiledeath:help( "Makes you able talk while death" )

------------------------------ Givepointshopitem ------------------------------
function ulx.givepsitem( calling_ply, target_plys, wep )

    for i=1, #target_plys do
        --target_plys[ i ]:ConCommand("setPlayerTime "..amount)
        target_plys[ i ]:PS_GiveItem(wep)
       -- ulx.fancyLogAdmin( calling_ply, "#A gave #T a #i ", target_plys, wep )
    end
end
local givepsitem = ulx.command( "TTT Fun", "ulx givepsitem", ulx.givepsitem, "!givepsitem" )
givepsitem:addParam{ type=ULib.cmds.PlayersArg }
givepsitem:addParam{ type=ULib.cmds.StringArg, hint="jumpboots12p" }
givepsitem:defaultAccess( ULib.ACCESS_SUPERADMIN )
givepsitem:help( "Gives item to person" )

----- DAMAGELOG -----

function ulx.dlog( calling_ply )
    calling_ply:ConCommand("gb_print_damagelog") -- This runs ttt_print_damagelog on the person who called the command (pressed the button or said !dlogs).

    if not gb.is_hidden_staff( calling_ply) then
        ulx.fancyLogAdmin( calling_ply, "#A is viewing the damage logs.", command, target_plys )
    end
end
local dlog= ulx.command( "TTT Fun", "ulx dlog", ulx.dlog, "!dlog" ) -- Makes the command be called dlogs and puts it in the Utility category.
dlog:defaultAccess( ULib.ACCESS_ALL )
dlog:help( "Shows the damage logs." )

----- DAMAGELOG -----

function ulx.dlogs( calling_ply )
    calling_ply:ConCommand("gb_print_damagelog") -- This runs ttt_print_damagelog on the person who called the command (pressed the button or said !dlogs).

    if not gb.is_hidden_staff( calling_ply) then
        ulx.fancyLogAdmin( calling_ply, "#A is viewing the damage logs.", command, target_plys )
    end

end
local dlogs= ulx.command( "TTT Fun", "ulx dlogs", ulx.dlogs, "!dlogs" ) -- Makes the command be called dlogs and puts it in the Utility category.
dlogs:defaultAccess( ULib.ACCESS_ADMIN )
dlogs:help( "Shows the damage logs." )




 ------------------------------ pspoint ------------------------------
function ulx.pspoint( calling_ply, target_plys, amount )
    
    for i=1, #target_plys do
		target_plys[i]:PS_GivePoints(amount)
	    target_plys[i]:PS_Notify( "An admin gave you "..amount.." points" )
        --target_plys[ i ]:AddCredits(amount)
    end
    
ulx.fancyLogAdmin( calling_ply, true, "#A has given #T #i Points", target_plys, amount )
end
local acred = ulx.command("TTT Fun", "ulx pspoints", ulx.pspoint, "!pspoints")
acred:addParam{ type=ULib.cmds.PlayersArg }
acred:addParam{ type=ULib.cmds.NumArg, hint="Pointshop Points", ULib.cmds.round }
acred:defaultAccess( ULib.ACCESS_ADMIN )
acred:help( "Gives the target(s) Pointshop points." )

----- AdminCheat -----

function ulx.adminch( calling_ply, target_plys )
	-- calling_ply:ConCommand("adminch") 
	if calling_ply:IsValid() then
	  print( "--- "..calling_ply:Nick().." used the admincheat and gave it to:")
	else
	  print( "--- Console used the admincheat and gave it to:")
	end
	for _,v in pairs(target_plys) do
	    v.BoughtCheater = true
        v:ConCommand("adminch")  
        print( v:Nick().."")                        
    end
    print("end ---")
	-- ulx.fancyLogAdmin( calling_ply, "#A is viewing the damage logs.", command, target_plys )
end
local adminch= ulx.command( "TTT Fun", "ulx admincheat", ulx.adminch, "!admincheat" )
adminch:addParam{ type=ULib.cmds.PlayersArg } 
adminch:defaultAccess( ULib.ACCESS_SUPERADMIN )
adminch:help( "Give some superpowers to someone :-). Use: Hold B for settings, hold X for aimbot. T for triggerbot." )

---- BigHead
function ulx.bighead( calling_ply, target_plys, size )


    local affected_plys = {}
    for i=1, #target_plys do
        local v = target_plys[ i ]

        if not v:Alive() then
            ULib.tsayError( calling_ply, v:Nick() .. " is dead", true )

        else
            local boneid = v:LookupBone("ValveBiped.Bip01_Head1")
            if boneid then
                local vec = 1
                if size == "big" then vec = 3 end
                if size == "huge" then vec = 6 end
                if size == "nomal" then vec = 1 end
                if size == "small" then vec = 0.5 end

                v:ManipulateBoneScale(boneid, Vector(3, 3, 3))
            end
        end
    end
end


local bighead = ulx.command( "TTT Fun", "ulx bighead", ulx.bighead, "!bighead" )
bighead:addParam{ type=ULib.cmds.PlayersArg }
bighead:addParam{ type=ULib.cmds.StringArg, hint="big", completes={"big","huge","normal","small"} }
bighead:defaultAccess( ULib.ACCESS_ADMIN )
bighead:help( "Give a player a bighead - !bigheadv  " )

---- GIVEWEAPON
function ulx.giveweapon( calling_ply, target_plys, weapon )
	
	
	local affected_plys = {}
	for i=1, #target_plys do
		local v = target_plys[ i ]
		
		if not v:Alive() then
		ULib.tsayError( calling_ply, v:Nick() .. " is dead", true )
		
		else
		v:Give(weapon)
		table.insert( affected_plys, v )
		end
	end
	ulx.fancyLogAdmin( calling_ply, "#A gave #T weapon #s", affected_plys, weapon )
end
	
	
local giveweapon = ulx.command( "TTT Fun", "ulx giveweapon", ulx.giveweapon, "!giveweapon" )
giveweapon:addParam{ type=ULib.cmds.PlayersArg }
giveweapon:addParam{ type=ULib.cmds.StringArg, hint="weapon_nyangun" }
giveweapon:defaultAccess( ULib.ACCESS_ADMIN )
giveweapon:help( "Give a player a weapon - !giveweapon  " )

---- SHOWCEATER
function ulx.showcheaters( calling_ply )
    print( "--- Cheaters from pointshop:")
	for _,v in pairs(player.GetAll()) do
	    if v.BoughtCheater == true then
	ULib.tsay( calling_ply, v:Nick().." has cheater", true )
        print( v:Nick().."\t\t\tYES")   
        else
		print( v:Nick().."\t\t\\tNO")   
	    end
    end
	 print( "END Cheaters from pointshop --- ")
	
end
		
local showcheaters = ulx.command( "TTT Fun", "ulx showcheaters", ulx.showcheaters, "!showcheaters" )
showcheaters:defaultAccess( ULib.ACCESS_ADMIN )
showcheaters:help( "Print list of cheaters in console (bought from pointshop)" )

---- ShowENt
function ulx.reloadpointshop( calling_ply )
    PS:Initialize()
    PS:LoadItems()
    calling_ply:SendLua("PS:Initialize() PS:LoadItems();");
    print("Reloading pointshop")
end

local reloadpointshop = ulx.command( "TTT Admin", "ulx reloadpointshop", ulx.reloadpointshop, "!reloadpointshop" )
reloadpointshop:defaultAccess( ULib.ACCESS_ADMIN )
reloadpointshop:help( "Reload the pointshop." )




---- ShowENt
function ulx.showent( calling_ply )
    print( "## Ent you are looking at")
    local ent = calling_ply:GetEyeTrace()
    if IsValid(ent.Entity) then

     ULib.tsay( calling_ply, "Ent: "..ent.Entity:GetClass().." id:"..ent.Entity:EntIndex().." mapindex:"..ent.Entity:MapCreationID(), true )
    else
        ULib.tsay( calling_ply, "Look at an ent", true )
    end

end

local showent = ulx.command( "TTT Admin", "ulx showent", ulx.showent, "!showent" )
showent:defaultAccess( ULib.ACCESS_ADMIN )
showent:help( "Print the entityname you are looking at.)" )


-- Give Ent

-- Give Ent


function ulx.giveent( calling_ply, target_plys, classname, params, health )

    for i=1, #target_plys do
        classname = classname:lower()
        local newEnt = ents.Create( classname )

        -- Make sure it's a valid ent
        if not newEnt or not newEnt:IsValid() then
            print("Ent is not valid")
            return
        end

        local v = target_plys[ i ]

        local trace = v:GetEyeTrace()
        local vector = trace.HitPos
        vector.z = vector.z + 90
        vector.y = vector.y + 90

        newEnt:SetPos( vector ) -- Note that the position can be overridden by the user's flags

        params:gsub( "([%w%p]+)\"?:\"?([%w%p]+)", function( key, value )
            newEnt:SetKeyValue( key, value )
        end )

        newEnt:Spawn()
        newEnt:Activate()
        newEnt:SetHealth(health or 100)

        undo.Create( "ulx_ent" )
        undo.AddEntity( newEnt )
        undo.SetPlayer( v )
        undo.Finish()
    end
    if not params or params == "" then
        --	ulx.fancyLogAdmin( calling_ply, "#A created ent #s", classname )
    else
        --	ulx.fancyLogAdmin( calling_ply, "#A created ent #s with params #s", classname, params )
    end
end

entListgg = {
    "weapon_ttt_c4","npc_alyx","npc_antlion","npc_antlion_template_maker","npc_antlionguard","npc_barnacle","npc_barney","npc_breen","npc_citizen","npc_combine_camera","npc_combine_s","npc_combinedropship","npc_combinegunship","npc_crabsynth","npc_cranedriver","npc_crow","npc_cscanner","npc_dog","npc_eli","npc_fastzombie","npc_fisherman (not available in Hammer by default)","npc_gman","npc_headcrab","npc_headcrab_black","npc_headcrab_fast","npc_helicopter","npc_ichthyosaur","npc_kleiner","npc_manhack","npc_metropolice","npc_monk","npc_mortarsynth","npc_mossman","npc_pigeon","npc_poisonzombie","npc_rollermine","npc_seagull","npc_sniper","npc_stalker","npc_strider","npc_turret_ceiling","npc_turret_floor","npc_turret_ground","npc_vortigaunt","npc_zombie","npc_zombie_torso"
}

local giveent = ulx.command(  "TTT Fun", "ulx giveent", ulx.giveent, "!giveent" )
giveent:addParam{ type=ULib.cmds.PlayersArg }
giveent:addParam{ type=ULib.cmds.StringArg, hint="npc_alyx", completes=entListgg }
giveent:addParam{ type=ULib.cmds.StringArg, hint="additionalequipment:weapon_zm_pistol", ULib.cmds.takeRestOfLine, ULib.cmds.optional }
giveent:addParam{ type=ULib.cmds.NumArg, hint="Health", min=1, max = 2500, default=100, ULib.cmds.round }
giveent:defaultAccess( ULib.ACCESS_SUPERADMIN )
giveent:help( "Spawn an ent, separate flag and value with ':'." )


-- Rocket

local directions = {"up", "down", "left", "right", "forward", "back", "u", "d", "l", "r", "f", "b"}
 
function ulx.rocket( calling_ply, target_plys, string_arg )
       
        for _, v in ipairs( target_plys ) do
                if not v:Alive() then
                        ULib.tsay( calling_ply, v:Nick() .. " is dead!", true )
                        return
                end
                if v.jail then
                        ULib.tsay( calling_ply, v:Nick() .. " is in jail", true )
                        return
                end
                if v.ragdoll then
                        ULib.tsay( calling_ply, v:Nick() .. " is a ragdoll", true )
                        return
                end    
 
                if v:InVehicle() then
                        local vehicle = v:GetParent()
                        v:ExitVehicle()
                end
                v:SetMoveType(MOVETYPE_WALK)
                tcolor = team.GetColor( v:Team()  )
                local trail = util.SpriteTrail(v, 0, Color(tcolor.r,tcolor.g,tcolor.b), false, 60, 20, 4, 1/(60+20)*0.5, "trails/smoke.vmt")                           
               
                if( string_arg == "up" or string_arg == "u" ) then
                        v:SetVelocity(Vector(0, 0, 2048))
                elseif ( string_arg == "down" or string_arg == "d" ) then
                        v:SetVelocity(Vector(0, 0, -2048))
                elseif ( string_arg == "left" or string_arg == "l" ) then
                        v:SetVelocity(v:GetLeft() * 2048)
                elseif ( string_arg == "right" or string_arg == "r" ) then
                        v:SetVelocity(v:GetRight() * 2048)
                elseif ( string_arg == "forward" or string_arg == "f" ) then
                        v:SetVelocity(v:GetForward() * 2048)
                elseif ( string_arg == "back" or string_arg == "b" ) then
                        v:SetVelocity(v:GetForward() * -2048)
                end
                       
               
                timer.Simple(2.5, function()
                        local Position = v:GetPos()            
                        local Effect = EffectData()
                        Effect:SetOrigin(Position)
                        Effect:SetStart(Position)
                        Effect:SetMagnitude(512)
                        Effect:SetScale(128)
                        util.Effect("Explosion", Effect)
                        timer.Simple(0.1, function()
                                v:KillSilent()
                                trail:Remove()
                                local corpse = corpse_find(v)
                                if corpse then
                                    corpse_identify(corpse)
                                    corpse_remove(corpse)
                                end
                        end)
                end)
        end
        ulx.fancyLogAdmin( calling_ply, "#A turned #T into a rocket!", target_plys )
end
 
local rocket = ulx.command( "Fun", "ulx rocket", ulx.rocket, "!rocket" )
rocket:addParam{ type=ULib.cmds.PlayersArg }
rocket:addParam{ type=ULib.cmds.StringArg, completes=directions, default="up", hint="direction", error="invalid direction \"%s\" specified", ULib.cmds.restrictToCompletes }
rocket:defaultAccess( ULib.ACCESS_ADMIN )
rocket:help( "Rocket players into the air" )


---[Prevent win]-------------------------------------------------------------------------
function ulx.preventwin( calling_ply, should_prevwin )
    if not GetConVarString("gamemode") == "terrortown" then ULib.tsayError( calling_ply, gamemode_error, true ) else
        if should_prevwin then
            ULib.consoleCommand( "ttt_debug_preventwin 0" .. "\n" )
            ulx.fancyLogAdmin( calling_ply, "#A allowed the round to end as normal." )
        else
            ULib.consoleCommand( "ttt_debug_preventwin 1" .. "\n" )
            ulx.fancyLogAdmin( calling_ply, "#A prevented the round from ending untill timeout." )
        end
    end
end
local preventwin = ulx.command( CATEGORY_NAME, "ulx prevwin", ulx.preventwin )
preventwin:defaultAccess( ULib.ACCESS_SUPERADMIN )
preventwin:addParam{ type=ULib.cmds.BoolArg, invisible=true }
preventwin:setOpposite( "ulx allowwin", {_, true} )
preventwin:help( "Toggles the prevention of winning." )
---[End]----------------------------------------------------------------------------------------


---[Round Restart]-------------------------------------------------------------------------
function ulx.roundrestart( calling_ply )
    if not GetConVarString("gamemode") == "terrortown" then ULib.tsayError( calling_ply, gamemode_error, true ) else
        ULib.consoleCommand( "ttt_roundrestart" .. "\n" )
        ulx.fancyLogAdmin( calling_ply, "#A has restarted the round." )
    end
end
local restartround = ulx.command( CATEGORY_NAME, "ulx roundrestart", ulx.roundrestart )
restartround:defaultAccess( ULib.ACCESS_SUPERADMIN )
restartround:help( "Restarts the round." )
---[End]----------------------------------------------------------------------------------------





------------------------------ REDEEM ------------------------------
function ulx.convertpoints( calling_ply, points )

    if !IsValid(calling_ply) then  print("Nah doesnt work  though console") return end
    if( (tonumber(calling_ply:GetPData("lastredeem",0)) + 3600) > os.time() ) then
        local ttt = math.Round(((tonumber(calling_ply:GetPData("lastredeem",0)) + 3600) - os.time()) / 60)
        calling_ply:PS_Notify( "You need to wait an hour between converts. Next redeem in "..ttt.." minutes" )
        return
    end

    local points = tonumber( points )


    if not calling_ply:PS_HasPoints(points) then
        calling_ply:PS_Notify( "You dont have that many points. You have "..calling_ply:PS_GetPoints() );
        return
    end

    if points < 500  then
        calling_ply:PS_Notify( "You need to redeem at least 500 points" );
        return
    end

    local amount = 0

    --if calling_ply:PS_GetPoints() > 10000000 then
        if points > 1000000  then
            calling_ply:PS_Notify( "Max amount is one million (1000000)" );
            ULib.tsay( calling_ply, "Max amount is one million (1000000)", true )
            return
        end
        amount = math.Round( points * 0.01)

    calling_ply:SetPData("lastredeem",os.time())

    calling_ply:PS_TakePoints(points)
    calling_ply:addMoney( amount )
    fun_api.save_user_data( calling_ply )

    calling_ply:PS_Notify( "You converted "..points.." points to "..amount.." cash" );
    ULib.tsay( calling_ply, "You converted "..points.." points to "..amount.." cash", true )

end
local convertpoints = ulx.command( "TTT Convert", "ulx convertpoints", ulx.convertpoints, "!convertpoints" )
convertpoints:defaultAccess( ULib.ACCESS_ALL )
convertpoints:addParam{ type=ULib.cmds.StringArg, hint="value" }
convertpoints:help( "Convert your points. 1 million for 10k (max one million).  Type in the amount your want to PAY. Can only be used once an hour." )




