local CATEGORY_NAME = "Menus"


if file.Exists( "lua/ulx/modules/cl/donatemenu.lua", "GAME" ) or ulx.donatemenu_exists then
    CreateConVar( "donatefile", "ulx_donate.txt" ) -- Garry likes to add and remove this cvar a lot, so it's here just in case he removes it again.
    local function sendDonate( ply )

        ULib.clientRPC( ply, "ulx.rcvDonate", true, "https://ttt-fun.com/donate/" .. "?steam="..ulx.base64enc(ply:SteamID())  )
        ply.ulxHasDonate = nil

    end

    local function showDonate( ply )
        local showDonate = GetConVarString( "ulx_showDonate" )
        if showDonate == "0" then return end
        if not ply:IsValid() then return end -- They left, doh!

        sendDonate( ply, showDonate )
        ULib.clientRPC( ply, "ulx.showDonateMenu" )
    end
    --hook.Add( "PlayerInitialSpawn", "showDonate", showDonate )

    function ulx.donate( calling_ply )
        if not calling_ply:IsValid() then
            Msg( "You can't see the donate from the console.\n" )
            return
        end


        showDonate( calling_ply )
    end
    local donatemenu = ulx.command( CATEGORY_NAME, "ulx donate", ulx.donate, "!donate" )
    donatemenu:defaultAccess( ULib.ACCESS_ALL )
    donatemenu:help( "Show the donate" )

end


if file.Exists( "lua/ulx/modules/cl/globalbanmenu.lua", "GAME" ) or ulx.globalbanmenu_exists then
    CreateConVar( "globalbanfile", "ulx_globalban.txt" ) -- Garry likes to add and remove this cvar a lot, so it's here just in case he removes it again.
    local function sendGlobalban( ply )

        ULib.clientRPC( ply, "ulx.rcvGlobalban", true, "https://ttt-fun.com/adminpanel/globalbans/" .. "?steam="..ulx.base64enc(ply:SteamID())  )
        ply.ulxHasGlobalban = nil

    end

    local function showGlobalban( ply )
        local showGlobalban = GetConVarString( "ulx_showGlobalban" )
        if showGlobalban == "0" then return end
        if not ply:IsValid() then return end -- They left, doh!

        sendGlobalban( ply, showGlobalban )
        ULib.clientRPC( ply, "ulx.showGlobalbanMenu" )
    end
    --hook.Add( "PlayerInitialSpawn", "showGlobalban", showDonate )

    function ulx.globalban( calling_ply )
        if not calling_ply:IsValid() then
            Msg( "You can't see the globalban from the console.\n" )
            return
        end


        showGlobalban( calling_ply )
    end
    local globalbanmenu = ulx.command( "Utility", "ulx globalban", ulx.globalban, "!globalban" )
    globalbanmenu:defaultAccess( ULib.ACCESS_ADMIN )
    globalbanmenu:help( "Show the globalban" )

end




if file.Exists( "lua/ulx/modules/cl/modhelp.lua", "GAME" ) or ulx.modmenu_exists then
    CreateConVar( "modfile", "ulx_mod.txt" ) -- Garry likes to add and remove this cvar a lot, so it's here just in case he removes it again.
    local function sendmod( ply )

        ULib.clientRPC( ply, "ulx.rcvMod", true, "https://ttt-fun.com/staff/rules/" .. "?steam="..ulx.base64enc(ply:SteamID())  )
        ply.ulxHasMod = nil

    end

    local function showMod( ply )
        local showMod = GetConVarString( "ulx_showMod" )
        if showMod == "0" then return end
        if not ply:IsValid() then return end -- They left, doh!

        sendMod( ply, showMod )
        ULib.clientRPC( ply, "ulx.showModMenu" )
    end
    --hook.Add( "PlayerInitialSpawn", "showMod", showDonate )

    function ulx.mod( calling_ply )
        if not calling_ply:IsValid() then
            Msg( "You can't see the mod help from the console.\n" )
            return
        end


        showMod( calling_ply )
    end
    local modmenu = ulx.command( "Utility", "ulx mod", ulx.mod, "!mod" )
    modmenu:defaultAccess( ULib.ACCESS_ALL )
    modmenu:help( "Show the moderator help" )

end



function ulx.base64enc(data)
    local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    return ((data:gsub('.', function(x)
        local r,b='',x:byte()
        for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
        return r;
    end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
        if (#x < 6) then return '' end
        local c=0
        for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
        return b:sub(c+1,c+1)
    end)..({ '', '==', '=' })[#data%3+1])
end