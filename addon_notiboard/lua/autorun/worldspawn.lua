NOTIBOARD_DATA = NOTIBOARD_DATA or {} // pos, angle, class data
NOTIBOARDS = {
	"bt_notiboard_big",
	"bt_notiboard_css",
	"bt_notiboard_small",
	"bt_notiboard_big",
	"bt_notiboard_medium",
}
properties.Add("makeNotiPerma",
{
	MenuLabel	=	"Make this Notiboard Perma",
	Order		=	4000,
	MenuIcon	=	"icon16/box.png",

	Filter		=	function(self, ent, ply)
						return (ply:IsSuperAdmin() and table.HasValue(NOTIBOARDS, ent:GetClass()) and !ent:GetNWBool("PermaNoti", false))
					end,

	Action		=	function(self, ent)
						if not IsValid(ent) then return end
						net.Start("NotiAdminRegister")
							net.WriteEntity(ent)
							net.WriteUInt(1, 1)
						net.SendToServer()
					end
})

properties.Add("removeNotiPerma",
{
	MenuLabel	=	"Revoke Perma Property",
	Order		=	4001,
	MenuIcon	=	"icon16/wrench.png",

	Filter		=	function(self, ent, ply)
						return (ply:IsSuperAdmin() and table.HasValue(NOTIBOARDS, ent:GetClass()) and ent:GetNWBool("PermaNoti", false))
					end,

	Action		=	function(self, ent)
						print(self, ent)
						if not IsValid(ent) then return end
						net.Start("NotiAdminRegister")
							net.WriteEntity(ent)
							net.WriteUInt(0, 1)
						net.SendToServer()
					end
})

if SERVER then
	for _, dir in ipairs( { "blacktea/", "blacktea/notiboards/" } ) do
		if not ( file.IsDir( dir, "DATA" ) ) then
			file.CreateDir( dir )
		end
	end

	util.AddNetworkString("NotiAdminRegister")
	
	local function UpdatePermaNoti()
		for k, v in pairs(NOTIBOARD_DATA) do
			if v.ent:IsValid() then
				v.class = v.ent:GetClass()
				v.pos = v.ent:GetPos()
				v.ang = v.ent:GetAngles()
				v.text = v.ent:GetNWString("text")
				v.title = v.ent:GetNWString("title")
			else
				table.remove(NOTIBOARD_DATA, k)
			end
		end	
		
		file.Write( "blacktea/notiboards/"..string.lower( string.lower(game.GetMap()) ) .. ".txt", util.TableToJSON( NOTIBOARD_DATA ) )
	end
	
	hook.Add("ShutDown", "NotiPerma",UpdatePermaNoti)

	local function createnotiboard()
		if file.Exists("blacktea/notiboards/" .. string.lower( string.lower(game.GetMap()) ) .. ".txt", "DATA") then
			NOTIBOARD_DATA = util.JSONToTable( file.Read( "blacktea/notiboards/" .. string.lower( string.lower(game.GetMap()) ) .. ".txt" ) )
		end

		for k, v in pairs(NOTIBOARD_DATA) do
			local ent = ents.Create(v.class)
			ent:SetPos(v.pos)
			ent:SetAngles(v.ang)
			ent:Spawn()
			ent:SetNWString("title", v.title)
			ent:SetNWString("text", v.text)
			
			v.ent = ent
			ent:SetMoveType(MOVETYPE_NONE)
			ent:GetPhysicsObject():Sleep()
			ent:SetNWBool("PermaNoti", true)
		end
	end
	
	hook.Add("PostCleanupMap","NotiPerma",createnotiboard)

	hook.Add("InitPostEntity", "NotiPerma", createnotiboard)
	
	net.Receive("NotiAdminRegister", function(len, client)
		if !client:IsSuperAdmin() then return end
		
		if !(!client.nextBoard or client.nextBoard < CurTime()) then
			client:ChatPrint("Try again later!")
		return end
	
		client.nextBoard = CurTime() + 3
		
		local ent = net.ReadEntity()
		local perma = net.ReadUInt(1)
		
		if ent and ent:IsValid() then
			if perma == 1 then
				ent:SetNWBool("PermaNoti", true)
				client:ChatPrint("Added Perma Notiboard")

				table.insert(NOTIBOARD_DATA, {
					ent = ent, 
					class = ent:GetClass(),
					pos = ent:GetPos(), 
					ang = ent:GetAngles(), 
					title = ent:GetNWString("title"), 
					text = ent:GetNWString("text")
				})
			
				UpdatePermaNoti()
				file.Write( "blacktea/notiboards/"..string.lower( string.lower(game.GetMap()) ) .. ".txt", util.TableToJSON( NOTIBOARD_DATA ) )
			else
				for k, v in pairs(NOTIBOARD_DATA) do
					if v.ent == ent then
						client:ChatPrint("Removed Perma Notiboard")
						v.ent:SetNWBool("PermaNoti", false)
						table.remove(NOTIBOARD_DATA, k)
						
						return	
					end
				end
			end
		end
	end)
end