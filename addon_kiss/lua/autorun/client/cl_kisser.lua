local fpPanels = {}
if not kisser then kisser = {} end

net.Receive("kisser.request", function()
    local pros = net.ReadEntity()
    local price = net.ReadInt(16)
    local id = net.ReadInt(16)

    kisser.openRequest(pros, price, id)
end)

function kisser.openRequest(ply, price, id)
    for k, v in pairs(fpPanels) do
        if !ValidPanel(v) then
        table.remove(fpPanels, k)
        end
    end

    if IsValid(ply)  then
        surface.PlaySound("common/warning.wav")
        local col = Color(255,255,255, 190)
        local acol = Color(45,25,200, 120)

        local backPanel = vgui.Create("DFrame")
        backPanel:SetSize(300, 150)
        backPanel:SetPos(10 + (310 * #fpPanels), 200)
        backPanel:SetTitle("Server Name")
        backPanel:ShowCloseButton(false)

        backPanel.Paint = function(self)
            if (!IsValid(ply)) then
            backPanel:Close()
            return
            end

            surface.SetDrawColor(20, 20, 20, 200)
            surface.DrawRect(0, 0, self:GetSize())

            surface.SetTextColor(col)
            if (string.len(ply:Nick()) <= 10) then
                surface.SetFont("pimpMenuHeader")
            else
                surface.SetFont("pimpMenuSubHeader")
            end

            local text = ply:Nick() .. " wants to kiss you"

            local w,h = surface.GetTextSize(text)
            surface.SetTextPos(self:GetWide()/2 - w/2, 30)
            surface.DrawText(text)
        end

        local ppid = #fpPanels + 1;
        table.insert(fpPanels, backPanel)

        local accept = vgui.Create("DButton", backPanel)
        accept:SetPos(10, 60)
        accept:SetSize(backPanel:GetWide() - 20, 30)
        accept:SetText(" ")

        accept.Paint = function(self)
            local color = acol
            if (self.Hovered) then
                color = col
            end

            surface.SetDrawColor(color)
            surface.DrawRect(0, 0, self:GetSize())

            surface.SetFont("pimpMenuHeader")
            surface.SetTextColor(245, 245, 245, 255)

            local text = "Accept Request"
            local w,h = surface.GetTextSize(text)

            surface.SetTextPos(self:GetWide()/2 - w/2, self:GetTall()/2 - h/2)
            surface.DrawText(text)
        end

        accept.DoClick = function()
            if (ply:GetPos():Distance(LocalPlayer():GetPos()) > 300) then
                kisser.notify(LocalPlayer(), "You need to be closer to accept the request!")
                return
            end
            LocalPlayer():ConCommand("kisser_acceptrequest " .. id)
            if (fpPanels[ppid]) then
                table.remove(fpPanels, ppid)
            end
            backPanel:Close()

            gui.EnableScreenClicker(false)
        end

        local deny = vgui.Create("DButton", backPanel)
        deny:SetPos(10, 100)
        deny:SetSize(backPanel:GetWide() - 20, 30)
        deny:SetText(" ")

        deny.Paint = function(self)
            local color = acol
            if (self.Hovered) then
                color = col
            end

            surface.SetDrawColor(color)
            surface.DrawRect(0, 0, self:GetSize())

            surface.SetFont("pimpMenuHeader")
            surface.SetTextColor(245, 245, 245, 255)

            local text = "Deny Request"
            local w,h = surface.GetTextSize(text)

            surface.SetTextPos(self:GetWide()/2 - w/2, self:GetTall()/2 - h/2)
            surface.DrawText(text)
        end

        deny.DoClick = function()
            if (fpPanels[ppid]) then
                table.remove(fpPanels, ppid)
            end
            backPanel:Close()

            gui.EnableScreenClicker(false)
        end


        timer.Simple(38, function()
            if ValidPanel(backPanel) then
                backPanel:Remove()
            end
        end)
    end
end