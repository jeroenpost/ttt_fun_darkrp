
AddCSLuaFile("autorun/client/cl_kisser.lua")
util.AddNetworkString("kisser.request")
if not kisser then kisser = {} end

if not kisser.activeRequests then kisser.activeRequests = {} end

concommand.Add("kisser_acceptrequest", function(ply, __, args)
    if (args and args[1]) then
        if (type(tonumber(args[1])) == "number") then
            if (kisser.activeRequests[tonumber(args[1])]) then
                local req = kisser.activeRequests[tonumber(args[1])]
                if (IsValid(req.pros) and (CurTime() - req.time) <= 40 ) then
                    if not ply.acceptedkisser then ply.acceptedkisser = {} end
                    ply.acceptedkisser[req.pros:SteamID()] = true
                    req.pros._pRequestee = nil
                    req.pros:SetFunVar("kissAccepted"..ply:SteamID(),1)
                    kisser.notify(req.pros, ply:Nick().." accepted your kiss")

                    table.remove(kisser.activeRequests, tonumber(args[1]))
                    timer.Simple(120,function()
                        if IsValid(ply) and  ply.acceptedkisser then ply.acceptedkisser[req.pros:SteamID()] = false end
                    end)
                end
            end
        end
    end
end)

