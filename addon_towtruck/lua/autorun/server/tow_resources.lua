
resource.AddFile("models/sickness/towtruckdr.dx80")
resource.AddFile("models/sickness/towtruckdr.dx90")
resource.AddFile("models/sickness/towtruckdr.mdl")
resource.AddFile("models/sickness/towtruckdr.phy")
resource.AddFile("models/sickness/towtruckdr.sw")
resource.AddFile("models/sickness/towtruckdr.vvd")

resource.AddFile("materials/models/gtaiv/vehicles/tow/vehicle_generic_smallspecmap.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_detail2.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_glasswindows2.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_tyrewallblack.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/tow/towtruck_interior.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/vehicle_generic_doorshut.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/tow/slamvan_lights_glass.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/tow/towtruck_detail.vmt")

resource.AddFile("materials/models/gtaiv/vehicles/tow/vehicle_generic_smallspecmap.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/tow/vehicle_generic_smallspecmap2.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/tow/vehicle_generic_smallspecmap3.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/tow/vehicle_generic_smallspecmap4.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/tow/vehicle_generic_smallspecmap5.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/tow/vehicle_generic_smallspecmap6.vmt")

resource.AddFile("materials/models/gtaiv/vehicles/tow/vehicle_generic_smallspecmap7.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/tow/vehicle_generic_smallspecmap8.vmt")
resource.AddFile("materials/models/gtaiv/vehicles/tow/vehicle_generic_smallspecmap10.vmt")


resource.AddFile("materials/models/gtaiv/vehicles/ecpd/white.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/ecpd/norm.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/ecpd/black.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/ecpd/brown.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/stallion/norm.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/steed/orange.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/minivan/blue.vmt")

resource.AddFile("materials/models/gtaiv/vehicles/merit/vehicle_generic_tyrewall_normal.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/tow/limegreen.vtf")
resource.AddFile("materials/models/gtaiv/vehicles/tow/darkred.vtf")

--[[
resource.AddFile("sound/vehicles/lt/lt_idle_loop.wav")
resource.AddFile("sound/vehicles/lt/lt_reverse.wav")
resource.AddFile("sound/vehicles/lt/lt_shutdown.wav")
resource.AddFile("sound/vehicles/lt/lt_slowdown.wav")
resource.AddFile("sound/vehicles/lt/lt_start.wav")
resource.AddFile("sound/vehicles/lt/lt_trottle_1st.wav")
resource.AddFile("sound/vehicles/lt/lt_trottle_2nd.wav")
]]--
