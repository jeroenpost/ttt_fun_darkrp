AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:SpawnFunction(ply, tr, ClassName)
	if not tr.Hit then return end

	local ent = ents.Create(ClassName)
	ent:SetPos(tr.HitPos)
	ent:Spawn()
	ent:Activate()

	return ent
end

function ENT:OnEntityCopyTableFinish(data)
	-- these kill the duplicator with cyclic references if kept in
	data.Game = nil
	data.Table = nil
end

hook.Add("CanProperty", "CasinoKit_BlockTablePeripheralPersist", function(ply, property, ent)
	if property == "persist" and IsValid(ent:GetParent()) and ent:GetParent().CasinoKitTable then
		ply:ChatPrint("[CasinoKit] Persist the table instead of the dealer or the seats!")
		return false
	end
end)

hook.Add("canPocket", "CasinoKit_PreventPock", function(ply, ent)
	if ent:GetParent().CasinoKitTable then
		return false
	end
end)

-- models/props/cs_office/table_meeting.mdl
ENT.Model = "models/props/de_tides/restaurant_table.mdl"
ENT.SeatCount = 4

function ENT:OnDealerIdChanged(varname, oldvalue, newvalue)
	local function ParseDealerId(id)
		local dealer = CasinoKit.getDealer(newvalue)
		if dealer then return dealer end

		-- provided a model path; we'll create a temp dealer object for that
		if util.IsValidModel(id) then
			return { Model = id }
		end
	end

	local dealer = ParseDealerId(newvalue)
	if dealer and IsValid(self.Dealer) then
		self.Dealer:SetDealerObj(dealer)
	end
end

local function removePlyFromVehicle(ply, veh)
	ply.CasinoKit_SeatLeftTime = CurTime()
	ply:ExitVehicle()

	if ply.CasinoKit_SeatEnterPos then
		ply:SetPos(ply.CasinoKit_SeatEnterPos)
	end
end

ENT.DealerPosOffset = Vector(40, 0, 0)
ENT.DealerAngOffset = Angle(0, 180, 0)

function ENT:Initialize()
	self:SetModel(self.Model)
	self:PhysicsInit(SOLID_VPHYSICS)

	self:SetUseType(SIMPLE_USE)

	self.Table = CasinoKit.classes.Table(self.SeatCount)
	self.Table:on("playerRemoved", function(ply, seatIndex)
		local gmodPly = ply:getGmodPlayer()
		if not IsValid(gmodPly) then return end

		local seatEnt = self.SeatEnts[seatIndex]
		if IsValid(seatEnt) and gmodPly == seatEnt:GetDriver() then
			removePlyFromVehicle(gmodPly, seatEnt)
			self:OnGameEvent("PlayerLeftTable", {ply = ply, seat = seatEnt})
		end
	end)

	self.SeatEnts = {}
	for i=1, self.SeatCount do
		local lpos, lang = self:SeatIndexOrientation(i)

		local ent = ents.Create("prop_vehicle_prisoner_pod")
		ent:SetModel("models/nova/chair_office02.mdl")
		ent:SetKeyValue("vehiclescript","scripts/vehicles/prisoner_pod.txt")
		ent:SetKeyValue("limitview","0")
		ent:SetPos(self:LocalToWorld(lpos))
		lang:RotateAroundAxis(lang:Up(), -90)
		ent:SetAngles(self:LocalToWorldAngles(lang))
		ent:Spawn()
		ent:Activate()

		-- darkrp
		ent.DoorData = { NonOwnable = true }
		if ent.setKeysNonOwnable then ent:setKeysNonOwnable(true) end

		ent:SetMoveType(MOVETYPE_NONE)
		ent:SetParent(self)

		ent.SeatIndex = i
		ent:SetNWInt("SeatIndex", i)
		self.SeatEnts[i] = ent

		-- HOOK.ADD WITH ENT PREPENDS THE ENT TO ARGS; WHY? IDK :RRRRRRRRRRRRRRRRRRRR
		hook.Add("CanPlayerEnterVehicle", ent, function(_, ply, _ent)
			if ent == _ent then
				-- make sure we're not trying to sit on someone else's seat
				local plyInSeat = self.Game:getPlayerInSeat(ent.SeatIndex)
				if plyInSeat and plyInSeat:getGmodPlayer() ~= ply then
					ply:ChatPrint(CasinoKit.L"#seatreserved")
					return false
				elseif not plyInSeat then

					-- if seat is empty, make sure that we're not already sitting somewhere
					local op = self.Game:getPlayerByEnt(ply)
					if op and op:getSeatIndex() ~= ent.SeatIndex then
						ply:ChatPrint(CasinoKit.L"#notyourseat #tryanotherseat")
						return false
					end

				end

				local b, reason = self.Game:canGmodPlayerSitIn(ply, ent.SeatIndex)
				if not b then
					ply:ChatPrint(CasinoKit.L("#cannotenterseat #because " .. tostring(reason or "#unknownreason")))
					return false
				end

				ply.CasinoKit_SeatEnterPos = ply:GetPos()
			end
		end)
		hook.Add("PlayerEnteredVehicle", ent, function(_, ply, _ent)
			if ent == _ent then
				local p = CasinoKit.classes.Player(ply)
				self.Table:addPlayer(p, ent.SeatIndex)

				self:OnGameEvent("PlayerJoinedTable", {ply = ply, seat = _ent})
			end
		end)
		hook.Add("PlayerLeaveVehicle", ent, function(_, ply, _ent)
			if ent == _ent then
				self.Table:removePlayerAtSlot(ent.SeatIndex)
				self:OnGameEvent("PlayerLeftTable", {ply = ply, seat = _ent})
			end
		end)

		local function PlayerTryLeaveTable(_, veh, ply)
			if veh ~= ent then return end

			if ply.CasinoKit_SeatLeftTime and ply.CasinoKit_SeatLeftTime > CurTime()-0.1 then return end

			removePlyFromVehicle(ply, veh)

			self.Table:removePlayerAtSlot(ent.SeatIndex)
			self:OnGameEvent("PlayerLeftTable", {ply = ply, seat = ent})

			return false
		end
		hook.Add("CanExitVehicle", ent, PlayerTryLeaveTable)

		local function PlayerLeftTable(_, ply)
			local veh = ply:GetVehicle()
			if ent == veh then
				self.Table:removePlayerAtSlot(ent.SeatIndex)
				self:OnGameEvent("PlayerLeftTable", {ply = ply, seat = veh})
			end
		end
		hook.Add("PlayerDeath", ent, PlayerLeftTable)
		hook.Add("PlayerSilentDeath", ent, PlayerLeftTable)
		hook.Add("PlayerDisconnected", ent, PlayerLeftTable)

		local phys = ent:GetPhysicsObject()
		if IsValid(phys) then
			phys:EnableMotion(false)
		end
	end

	timer.Simple(1, function()
		self.Dealer = ents.Create("casinokit_dealernpc")
		self.Dealer:SetPos(self:LocalToWorld(self.DealerPosOffset))
		self.Dealer:SetAngles(self:LocalToWorldAngles(self.DealerAngOffset))
		self.Dealer:Spawn()

		self.Dealer:SetMoveType(MOVETYPE_NONE)
		self.Dealer:SetParent(self)
	end)

	local gameClass = CasinoKit.classes[self.GameClass]
	assert(gameClass, "game '" .. tostring(self.GameClass) .. "' does not exist!")

	self.Game = self.Table:createGame(gameClass, self)
	self:SetClGameClass(self.Game:getClClass())
end

util.AddNetworkString("casinokit_gameinput")
net.Receive("casinokit_gameinput", function(len, cl)
	local table = net.ReadEntity()
	if not IsValid(table) or not table.CasinoKitTable then cl:ChatPrint("Invalid table ent.") return end

	local game = table.Game
	if not game then cl:ChatPrint("No game ongoing in table.") return end

	local seatIndex = net.ReadUInt(8)

	local gamePlayer

	-- First try to find gameplayer from seat directly
	if seatIndex ~= 0 then
		gamePlayer = game:getPlayerInSeat(seatIndex)
		if gamePlayer and gamePlayer:getGmodPlayer() ~= cl then cl:ChatPrint("That gameplayer is not you!") return end
	end

	-- if gamePlayer not found via seat directly, try from all gamePlayers
	if not gamePlayer then
		gamePlayer = game:getPlayerByEnt(cl)
	end

	local dataLen = net.ReadUInt(16)
	local data = net.ReadData(dataLen)
	if not data then cl:ChatPrint("No data") return end
	local buffer = CasinoKit.classes.Buffer(data)

	-- if still no gamePlayer, fallback to onOutsidePlayerInput
	if not gamePlayer then
		if game.onOutsidePlayerInput then
			game:onOutsidePlayerInput(cl, buffer)
		else
			cl:ChatPrint("No GamePlayer found")
		end
	else
		game:onPlayerInput(gamePlayer, buffer)
	end
end)

function ENT:SpawnCard(targ)
	local c = ents.Create("casinokit_card")
	c:SetPos(self:LocalToWorld(Vector(25, 0, 33)))
	c:SetAngles(self:LocalToWorldAngles(Angle(90, 0, 0)))
	c:SetParent(self)
	c:Spawn()

	c:AnimMove(self:LocalToWorld(targ), 1)
	return c
end

function ENT:OnRemove()
	if self.Game then self.Game:cleanup() end
end

function ENT:OnGameEvent(name, data)
	if IsValid(self.Dealer) then
		if name == "PlayerJoinedTable" then
			self.Dealer:SayVoicePhrase("greet")
		elseif name == "PlayerLeftTable" then
			self.Dealer:SayVoicePhrase("leave")
		end
	end
end

function ENT:OnGameConfigReceived(key, value)

end

util.AddNetworkString("casinokit_tablecfg")
net.Receive("casinokit_tablecfg", function(len, cl)
	local tbl = net.ReadEntity()
	local key = net.ReadString()
	local value = net.ReadType()

	if not IsValid(tbl) or not tbl.CasinoKitTable then return end
	if not tbl:CanConfigureTable(cl) then return cl:ChatPrint("permission denied") end

	tbl:OnGameConfigReceived(key, value)
end)

function ENT:PersistTableSettings(tbl)
	self.Game:persistTableSettings(tbl)
end
function ENT:RestoreTableSettings(tbl)
	self.Game:restoreTableSettings(tbl)
end

hook.Add("PlayerDisconnected", "CasinoKit_KickDCdFromGame", function(ply)
	for _,t in pairs(ents.GetAll()) do
		if t.CasinoKitTable and t.Game then
			for _,tp in pairs(t.Game:getPlayersByEnt(ply)) do
				t.Game:kickPlayer(tp)
			end
		end
	end
end)

concommand.Add("casinokit_createdebugply", function(ply, cmd, args)
	if not ply:IsSuperAdmin() then return end

	local tr = ply:GetEyeTrace()
	local e = tr.Entity
	local table = e:GetParent()
	assert(table.CasinoKitTable)

	local p = CasinoKit.classes.Player(ply)
	table.Table:addPlayer(p, e.SeatIndex)

	ply:ChatPrint("debug ply created")
end)
