local ply = FindMetaTable("Player")

function ply:CKit_GetChips()
	return self:GetNWInt("CK_Chips", 0)
end
function ply:CKit_CanAffordChips(n)
	return n >= 0 and self:CKit_GetChips() >= n
end

if SERVER then
	hook.Add("PlayerInitialSpawn", "CasinoKit_RestorePChips", function(ply)
		local chips = tonumber(ply:GetPData("CK_Chips")) or 0
		ply:SetNWInt("CK_Chips", chips)
	end)
	function ply:CKit_SetChips(chips)
		self:SetNWInt("CK_Chips", chips)
		self:SetPData("CK_Chips", chips)
	end

	local chipSounds = {"chipsHandle1.ogg", "chipsHandle3.ogg", "chipsHandle4.ogg", "chipsHandle5.ogg", "chipsHandle6.ogg"}
	function ply:CKit_AddChips(chips, reason)
		self:CKit_SetChips(self:CKit_GetChips() + chips)

		if chips > 0 then
			local strength = math.Clamp(chips / 1000, 0, 1)
			self:EmitSound("casinokit/" .. table.Random(chipSounds), 35 + strength * 75)
		end
	end
end
