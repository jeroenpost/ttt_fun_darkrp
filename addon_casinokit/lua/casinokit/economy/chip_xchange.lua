local function findTableEnt(e)
	for d=0, 3 do
		if not IsValid(e) then return end
		if e.CasinoKitTable then return e end

		e = e:GetParent()
	end
end

util.AddNetworkString("casinokit_chipexchange")
net.Receive("casinokit_chipexchange", function(len, cl)
	local ent = net.ReadEntity()

	local currencyId = net.ReadString()
	local money2chips = net.ReadBool()

	local amountNum = net.ReadUInt(32)
	if amountNum < 0 then cl:ChatPrint("amount<0?") return end -- shouldnt be possible but idk

	local currency = CasinoKit.getCurrency(currencyId)
	if not currency then return end

	if money2chips then
		if not currency:canPlayerAfford(cl, amountNum) then
			cl:ChatPrint("cannot afford") return
		end
		currency:addPlayerCurrency(cl, -amountNum, "currency->chip exchange")

		local chips = math.floor(currency:getExchangeRateFromCurrencyToChips(cl) * amountNum)
		cl:CKit_AddChips(chips, "chip exchange")

		local s = CasinoKit.L("[CasinoKit] #currencytochips", amountNum, chips)
		cl:ChatPrint(s)
	else
		if cl:CKit_GetChips() < amountNum then
			cl:ChatPrint("cannot afford") return
		end
		cl:CKit_AddChips(-amountNum, "chip exchange")

		local baseRate, feeRate = currency:getExchangeRateFromChipsToCurrency(cl)
		local baseMoney, feeMoney = math.floor(baseRate * amountNum), math.floor((feeRate or 0) * amountNum)

		currency:addPlayerCurrency(cl, baseMoney, "chip->currency exchange")

		local s = CasinoKit.L("[CasinoKit] #chipstocurrency", amountNum, baseMoney)
		cl:ChatPrint(s)

		-- give table owner his fee moneys
		if feeMoney > 0 then
			local table = findTableEnt(ent)
			if IsValid(table) then
				local tableOwner = table.TableOwner
				if IsValid(tableOwner) then
					currency:addPlayerCurrency(tableOwner, feeMoney, "player exchange fee")
				end
			end
		end
	end
end)
