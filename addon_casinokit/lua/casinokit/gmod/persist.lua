function CasinoKit.restorePersistedEnts()
	local text = file.Read("casinokit_persist.txt", "DATA")
	if not text then return end

	local json = util.JSONToTable(text)
	if not json then return end

	local map = game.GetMap():lower()
	local mapTable = json[map]
	if not mapTable then return end

	for _,etbl in pairs(mapTable) do
		local e = ents.Create(etbl.class)
		e:SetPos(etbl.pos)
		e:SetAngles(etbl.ang or Angle(0, 0, 0))
		e:Spawn()
		e:Activate()

		e:SetNWBool("CasinoKit_Persistent", true)

		if etbl.tableSettings and e.RestoreTableSettings then
			e:RestoreTableSettings(etbl.tableSettings)
		end

		local phys = e:GetPhysicsObject()
		if IsValid(phys) then phys:EnableMotion(false) end
	end
end
hook.Add("PersistenceLoad", "CasinoKit_RestorePersistedEnts", function() timer.Simple(3, CasinoKit.restorePersistedEnts) end)

local function Save()
	local json = util.JSONToTable(file.Read("casinokit_persist.txt", "DATA") or "{}")
	local map = game.GetMap():lower()

	json[map] = {}
	for _,e in pairs(ents.GetAll()) do
		if (e.CasinoKitPersistable or e.CasinoKitTable) and e:GetNWBool("CasinoKit_Persistent") then
			local psettings = nil

			if e.PersistTableSettings then
				psettings = {}
				e:PersistTableSettings(psettings)
			end

			table.insert(json[map], {
				class = e:GetClass(),
				pos = e:GetPos(),
				ang = e:GetAngles(),

				tableSettings = psettings
			})
		end
	end

	file.Write("casinokit_persist.txt", util.TableToJSON(json))
end
hook.Add("PersistenceSave", "CasinoKit_SavePersistedEnts", Save)

concommand.Add("casinokit_printpersistdata", function(p)
	local e = p:GetEyeTrace().Entity
	if not IsValid(e) then return end

	local posString = string.format("[%d %d %d]", e:GetPos().x, e:GetPos().y, e:GetPos().z)
	local angString = string.format("{%d %d %d}", e:GetAngles().p, e:GetAngles().y, e:GetAngles().r)
	p:PrintMessage(HUD_PRINTCONSOLE, [[
	{
		"class": "]] .. e:GetClass() .. [[",
		"pos": "]] .. posString .. [[",
		"ang": "]] .. angString .. [["
	}
	]])
end)

-- if not sandbox need to persist manually
hook.Add("ShutDown", "CKitSavePersistenceOnShutdown", function() if not (GM or GAMEMODE).IsSandboxDerived then Save() end end)
hook.Add("InitPostEntity", "CKitPersistenceInit", function() if not (GM or GAMEMODE).IsSandboxDerived then timer.Simple(3, CasinoKit.restorePersistedEnts) end end)