-- PermaPropsTool By Malboro


TOOL.Name = "#tool.permaprops.name"
TOOL.Category = "Tools"
TOOL.Command = nil
TOOL.ConfigName = ""
TOOL.AddToMenu = false
if CLIENT then

	language.Add( "tool.permaprops.name", "PermaProps" )
	language.Add( "tool.permaprops.desc", "Save a prop for server restarts" )
	language.Add( "tool.permaprops.0", "Left click: make permanent; Right click: make temporary; Reload: update permanent entity" )
	
end

function TOOL:LeftClick()

	RunConsoleCommand("perma_save")

end

function TOOL:RightClick()

	RunConsoleCommand("perma_remove")	

end

function TOOL:Reload()

	RunConsoleCommand("perma_update")

end

function TOOL.BuildCPanel( panel )
    
	panel:AddControl( "Header", { Text = "PermaProps", Description	= "Save a prop for server restarts\nBy Malboro" }  )

end