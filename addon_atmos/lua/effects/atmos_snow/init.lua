
local maxsnow = CreateClientConVar( "atmos_cl_snowperparticle", 16, true, false );

function EFFECT:Init( data )
	
	self.Data = data;

	self.em3D = ParticleEmitter( EyePos(), false );
	
	self.Live = true;

	atmos_log( "SnowEffect init" );

end

local data, m, n, p, pos, ed;

function EFFECT:Think()
	
	data = self.Data;
	m = data:GetMagnitude();
	n = data:GetRadius();

	data:SetOrigin( LocalPlayer():GetPos() );

	if( !AtmosSnowing ) then

		self:Die();

	end

	if( self.em3D ) then

		self.em3D:SetPos( data:GetOrigin() );
		
		for i = 1, maxsnow:GetInt() do
			
			pos = data:GetOrigin() + Vector( math.random( -m, m ), math.random( -m, m ), math.random( m, 2 * m ) );
			
			if( atmos_Outside( pos ) ) then
				
				p = self.em3D:Add( "particle/snow", pos );
				
				p:SetAngles( Angle(math.random(360), math.random(360), math.random(360)) );
				p:SetVelocity( Vector( 0, 0, 0 ) );
				p:SetDieTime( 5 );
				p:SetStartAlpha( 230 );
				p:SetStartSize( 2 );
				p:SetEndSize( 1 );
				p:SetColor( 255, 255, 255 );
				p:SetGravity( Vector(0,0,math.Rand(-30, -200)) )
				p:SetCollide( false );
				
			end
			
		end
		
	end

	if( !self.Live ) then

		if( self.em3D ) then

			self.em3D:Finish();

		end

		atmos_log( "SnowEffect killed" );

		return self.Live;

	end

	return true;
	
end

function EFFECT:Die()

	self.Live = false;

end

function EFFECT:Render()
	
end
