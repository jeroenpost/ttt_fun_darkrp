
ENT.DrawDistance = 300 -- distance at which the screen no longer draws
ENT.FrameRate = 2 -- frame rate of camera (frames per second)
ENT.FOV = 70 -- field of view (degrees)
ENT.ShowStatic = true -- whether to show static when camera takes damage
