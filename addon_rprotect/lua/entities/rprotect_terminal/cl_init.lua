
include( "shared.lua" )
include( "sh_config.lua" )

local mat = CreateMaterial( "rprotect_terminal_viewmat", "UnlitGeneric", { } )
local viewWidth, viewHeight = 512, 300
local inrt = false
local rts = {}

local function addListSpacer( list, height, vmargins, color )
	vmargins = vmargins or 0

	local spacer = vgui.Create( "DPanel", list )
		spacer:SetTall( height + (vmargins * 2) )

	if color then
		spacer.Paint = function( self, w, h )
			surface.SetDrawColor( color )
			surface.DrawRect( 0, vmargins, w, height )
		end
	else
		spacer.Paint = nil
	end

	list:Add( spacer )
end

net.Receive( "rprotect_terminal_settings", function( len )
	local ent = net.ReadEntity()

	if !IsValid( ent ) then return end

	local owner = ent:Getowning_ent()
	local isOwner = LocalPlayer() == owner
	local nonOwnerEdit = false
	local feed

	if isOwner then
		nonOwnerEdit = net.ReadBool()
	end

	local frame = vgui.Create( "DFrame" )
		frame:SetTitle( "Terminal Settings" )
		frame:SetSize( 256, 512 )
		frame:Center()
		frame:MakePopup()

	local scroll = vgui.Create( "DScrollPanel", frame )
		scroll:SetPadding( 5 )
		scroll:SetPos( 5, 30 )
		scroll:SetSize( frame:GetWide() - 10, frame:GetTall() - 35 )

	local list = vgui.Create( "DListLayout", scroll )

	scroll.OldPerformLayout = scroll.PerformLayout
	scroll.PerformLayout = function( ... )
		list:SetWide( scroll:InnerWidth() - (scroll:GetVBar():IsVisible() and 2 or 0) )

		scroll.OldPerformLayout( ... )
	end


	local cbAllowEdit

	if isOwner then
		cbAllowEdit = vgui.Create( "DCheckBoxLabel", list )
			cbAllowEdit:SetText( "Allow others to edit" )
			cbAllowEdit:SetChecked( nonOwnerEdit )
			cbAllowEdit:SizeToContents()

		list:Add( cbAllowEdit )

		addListSpacer( list, 2, 4, Color( 255, 255, 255, 100 ) )
	end

	local lblCam = vgui.Create( "DLabel", list )
		lblCam:SetText( "Cameras" )
		lblCam:SizeToContents()

		list:Add( lblCam )


	local cams = {}

	for _, cam in pairs( ents.FindByClass( "rprotect_camera*" ) ) do -- wildcard for perm cameras
		if cam:Getowning_ent() == owner then
			table.insert( cams, cam )
		end
	end

	table.sort( cams, function( c1, c2 )
		local c1n = c1:GetDeviceName()
		local c2n = c2:GetDeviceName()

		-- assumes they're both cameras and follow the same naming convention when sorting
			-- (not always the case for permanent cameras, but this menu should never appear for those anyway)
			
		return (#c1n < #c2n) or ((#c1n == #c2n) and (c1n < c2n))
	end )

	for _, cam in ipairs( cams ) do
		addListSpacer( list, 4, 0 )

		local pnl = vgui.Create( "DPanel" )
			pnl:SetTall( 35 )
			pnl.Paint = function( self, w, h )
				surface.SetDrawColor( 75, 75, 75, 255 )
				surface.DrawRect( 0, 0, w, h )
			end

		local lblName = vgui.Create( "DLabel", pnl )
			lblName:SetText( cam:GetDeviceName() )
			lblName:SizeToContents()
			

		local btnSet = vgui.Create( "DButton", pnl )
			btnSet:SetText( "Set Feed" )
			btnSet:SetSize( 75, 20 )
			btnSet.DoClick = function()
				feed = cam

				frame:Remove()
			end
			

		pnl.PerformLayout = function()
			lblName:CenterVertical( 0.5 )
			lblName:AlignLeft( 5 )

			btnSet:CenterVertical( 0.5 )
			btnSet:AlignRight( 5 )
		end

		list:Add( pnl )
	end


	frame.OnRemove = function()
		if !feed and (!isOwner or nonOwnerEdit == cbAllowEdit:GetChecked()) then return end

		net.Start( "rprotect_terminal_settings" )
			net.WriteEntity( ent )

			if isOwner then
				net.WriteBool( cbAllowEdit:GetChecked() )
			end

			net.WriteBool( feed != nil )

			if feed then
				net.WriteEntity( feed )
			end
		net.SendToServer()
	end
end )

-- HUDPaint was causing flashlight and crosshair flickering, which PreRender seemed to fix
hook.Add( "PreRender", "rprotect_terminal_drawrts", function()
	if inrt then return end

	inrt = true

	for _, term in pairs( ents.FindByClass( "rprotect_terminal" ) ) do
		if term:GetNeedsRT() then
			local rtdata = rts[term]

			if !rtdata then
				for t, data in pairs( rts ) do
					if (SysTime() - data.lastUse) >= 0.5 then
						rts[term] = rts[t]
						rts[t] = nil

						break
					end
				end

				rts[term] = rts[term] or {
					rt = GetRenderTarget(
						"rprotect_termview_" .. table.Count( rts ),
						512, 512
					)
				}

				rtdata = rts[term]
			end

			term:RenderToRT( rtdata.rt )

			rtdata.lastUse = SysTime()
		end
	end

	inrt = false
end )

hook.Add( "ShouldDrawLocalPlayer", "rprotect_terminal_nodrawlp", function()
	if inrt then return true end 
end )


function ENT:Draw()
	self.BaseClass.Draw( self )

	local feed = self:GetCamera()

	local pos = self:LocalToWorld( Vector( 6.1, -27.9, 35.3 ) )
	local ang = self:LocalToWorldAngles( Angle( 0, 90, 90 ) )
	local scale = 0.109

	cam.Start3D2D( pos, ang, scale )

		surface.SetDrawColor( 0, 0, 0, 255 )
		surface.DrawRect( 0, 0, viewWidth, viewHeight )

		if !inrt and LocalPlayer():GetPos():Distance( self:GetPos() ) <= self.DrawDistance then
			if self.canDrawRT then
				mat:SetTexture( "$basetexture", self.rt )

				surface.SetDrawColor( 255, 255, 255, 255 )
				surface.SetMaterial( mat )
				surface.DrawTexturedRectUV(
					0, 0, viewWidth, viewHeight, 
					0, 0, 
					math.min( viewWidth / self.rt:GetMappingWidth(), 1 ), 
					math.min( viewHeight / self.rt:GetMappingHeight(), 1 )
				)
			else
				draw.SimpleText(
					(IsValid( feed ) and feed.Damageable and feed:GetHP() <= 0)
						and "No Signal"
						or  "No Feed",
					"Trebuchet24",
					viewWidth / 2, viewHeight / 2,
					Color( 255, 0, 0, 255 ),
					TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER
				)
			end

			self.needsRT = IsValid( feed ) and (!feed.Damageable or feed:GetHP() > 0)
		end

	cam.End3D2D()

end

function ENT:GetNeedsRT()
	local ret = self.needsRT

	self.needsRT = false
	self.canDrawRT = ret -- because if we return true, we're guaranteed to have it next frame

	return ret 
end

do
	-- try to squeeze a little more performance out of this

	local random = math.random
	local drawRect = surface.DrawRect

	function ENT:DrawStatic( alpha )
		if alpha <= 0 then return end

		local x, y = 0, 0

		local xl, xh = 2, 12 -- width range
		local yl, yh = 2, 6  -- row height range

		local w, h

		surface.SetDrawColor( 0, 0, 0, alpha )
		drawRect( 0, 0, viewWidth, viewHeight )

		surface.SetDrawColor( 150, 150, 150, alpha )
		
		while y < viewHeight do
			h = random( yl, yh )
			h = (h <= (viewHeight - y)) and h or (viewHeight - y)

			while x < viewWidth do
				w = random( xl, xh )
				w = (w <= (viewWidth - x)) and w or (viewWidth - x)
				
				if (w % 2) == 0 then -- so we only need to call random once
					drawRect( x, y, w, h )
				end

				x = x + w
			end

			x = 0
			y = y + h
		end
	end
end

-- was having problems with RenderView in ENT:Draw(), so did it here instead
function ENT:RenderToRT( rt )
	self.lastUpdate = self.lastUpdate or 0

	if self.FrameRate == 0 or (SysTime() - self.lastUpdate) >= (1 / self.FrameRate) then
		local feed = self:GetCamera()

		if !IsValid( feed ) then return end

		local salpha = 0

		if feed.Damageable then
			local hfrac = math.Clamp( feed:GetHP() / feed.StartingHealth, 0, 1 )
			local threshold = 0.8

			if hfrac < threshold then
				salpha = math.sin( ((threshold - hfrac) / threshold) * (math.pi / 2) ) * 0.75
				salpha = salpha * 255
			end
		end

		render.PushRenderTarget(
			rt,
			0, 0,
			viewWidth, viewHeight
		)
			render.ClearDepth()
			render.Clear( 0, 0, 0, 0 )

			cam.Start2D()

				render.RenderView( {
					origin = feed:LocalToWorld( Vector( 5, 0, 0 ) ),
					angles = feed:LocalToWorldAngles( Angle( 0, 0, 0 ) ),
					x = 0, y = 0,
					w = viewWidth, h = viewHeight,
					fov = self.FOV,

					dopostprocess = false,
					drawhud = false,
					drawmonitors = false,
					drawviewmodel = false
				} )

				if self.ShowStatic then
					self:DrawStatic( salpha )
				end

				surface.SetDrawColor( 255, 255, 255, 255 )
				surface.DrawOutlinedRect(
					viewWidth / 4, viewHeight / 4,
					viewWidth / 2, viewHeight / 2
				)

				surface.DrawCircle(
					viewWidth / 2, viewHeight / 2,
					viewHeight / 24,
					Color( 255, 255, 255, 255 )
				)

				draw.SimpleText(
					"REC",
					"Trebuchet24",
					viewWidth - 10, 5,
					Color( 255, 0, 0, 255 ),
					TEXT_ALIGN_RIGHT, TEXT_ALIGN_BOTTOM
				)

				draw.SimpleTextOutlined(
					feed:GetDeviceName(),
					"Trebuchet24",
					10, 5,
					Color( 255, 255, 255, 255 ),
					TEXT_ALIGN_LEFT, TEXT_ALIGN_BOTTOM,
					1, Color( 0, 0, 0, 255 )
				)
			
			cam.End2D()

		render.PopRenderTarget()

		self.rt = rt
		self.lastUpdate = SysTime()
	end
end

function ENT:GetTip()
	-- don't overlay tip on top of screen
end
