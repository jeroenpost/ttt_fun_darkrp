
ENT.Type = "anim"
ENT.Base = "rprotect_base"
ENT.PrintName = "Terminal"
ENT.Author = "Dan"
ENT.Spawnable = false
ENT.AdminSpawnable = false

ENT.Damageable = false -- not designed to be damageable, so don't enable


function ENT:SetupDataTables()
	self.BaseClass.SetupDataTables( self )

	self:AddNetworkVar( "Entity", "Camera" )
end
