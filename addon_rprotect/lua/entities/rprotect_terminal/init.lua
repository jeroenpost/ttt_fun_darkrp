
AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "sh_config.lua" )

include( "shared.lua" )
include( "sh_config.lua" )

util.AddNetworkString( "rprotect_terminal_settings" )

net.Receive( "rprotect_terminal_settings", function( len, ply )
	local ent = net.ReadEntity()
	local owner = ent:Getowning_ent()
	local isOwner = ply == owner

	if !IsValid( ent ) or ent:GetClass() != "rprotect_terminal" or 
		(!isOwner and !ent.nonOwnerEdit) then return end

	if isOwner then
		ent.nonOwnerEdit = net.ReadBool()
	end

	if net.ReadBool() then
		local cam = net.ReadEntity()

		if !IsValid( cam ) or !cam:GetClass():StartWith( "rprotect_camera" ) or -- for perm cameras
			cam:Getowning_ent() != owner then return end

		ent:SetCamera( cam )
	end
end )

hook.Add( "SetupPlayerVisibility", "rprotect_terminal_setviewbounds", function( ply )
	for _, term in pairs( ents.FindByClass( "rprotect_terminal" ) ) do
		if ply:GetPos():Distance( term:GetPos() ) <= term.DrawDistance then
			local cam = term:GetCamera()

			if IsValid( cam ) then
				AddOriginToPVS( cam:GetPos() )
			end
		end
	end
end )


function ENT:Initialize()
	self:SetModel( "models/props_phx/rt_screen.mdl" )
	
	self.BaseClass.Initialize( self )

	self.nonOwnerEdit = false
end

function ENT:Use( ply )
	if ply != self:Getowning_ent() and !self.nonOwnerEdit then return end

	net.Start( "rprotect_terminal_settings" )
		net.WriteEntity( self )

		if ply == self:Getowning_ent() then
			net.WriteBool( self.nonOwnerEdit )
		end
	net.Send( ply )
end
