
-- default shared settings

ENT.HintDrawDistance = 40 -- how far away should we draw the hint 
ENT.HealthDrawDistance = 1000 -- how far away should we draw the health
ENT.ManualFixDistance = 100 -- how close do we need to be in order to fix a broken device

ENT.BreakSound = "buttons/button8.wav" -- sound when a device breaks
ENT.FixSound = "buttons/button9.wav" -- sound when a device is fixed
