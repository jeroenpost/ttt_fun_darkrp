
include( "shared.lua" )
include( "sh_config.lua" )

hook.Add( "Think", "rprotect_show_hint", function()
	local trace = LocalPlayer():GetEyeTrace()
	local pos = trace.HitPos
	local ent = trace.Entity

	if !IsValid( ent ) or ent.Base != "rprotect_base" then return end 
	if pos:Distance( LocalPlayer():EyePos() ) > ent.HintDrawDistance then return end

	local tip = ent:GetTip()

	if tip then
		AddWorldTip( nil, tip, nil, nil, ent )
	end
end )

hook.Add( "HUDPaint", "rprotect_show_repair_cost", function()
	local trace = LocalPlayer():GetEyeTrace()
	local ent = trace.Entity

	if !IsValid( ent ) or ent.Base != "rprotect_base" or ent:GetHP() > 0 then return end
	if !ent.Damageable or !ent.Breakable or ent.FixMode != "manual" then return end
	if ent:GetPos():Distance( LocalPlayer():EyePos() ) > ent.ManualFixDistance then return end

	draw.SimpleTextOutlined(
		"Repair for $" .. (ent.ManualFixCost or 0),
		"Trebuchet24",
		ScrW() / 2, ScrH() / 2,
		Color( 255, 255, 255, 255 ),
		TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER,
		1, Color( 0, 0, 0, 255 )
	)
end )


function ENT:GetTip()
	local str = self:GetDeviceName()
	local owner = self:Getowning_ent()

	if IsValid( owner ) and owner:IsPlayer() then
		str = str .. "\nOwned by: " .. owner:Nick()
	end

	return str
end

function ENT:Draw()
	self:DrawModel()

	local eye = LocalPlayer():EyePos()
	local pos = self:LocalToWorld( Vector( 0, 0, self:OBBMaxs().z + 5 ) )

	if !self.Damageable or eye:Distance( pos ) > self.HealthDrawDistance then return end
	
	local hp = self:GetHP()

	if !self.drawHealth then
		if hp <= 0 or (hp < self.StartingHealth and LocalPlayer():GetEyeTrace().Entity == self) then
			self.drawHealth = true
			self.healthAppearTime = CurTime()
			self.lastHealthDraw = CurTime()
		end
	elseif (CurTime() - self.lastHealthDraw) > 2.5 or hp >= self.StartingHealth then
		self.drawHealth = false
	else
		local ang = (eye - pos):Angle()
			ang:RotateAroundAxis( ang:Up(), 90 )
			ang:RotateAroundAxis( ang:Forward(), 90 )
		
		local alpha = ((CurTime() - self.healthAppearTime) / 0.5) * 255
		local hfrac = math.Clamp( hp / self.StartingHealth, 0, 1 )

		local w, h = 150, 20
		local scale = 0.15

		cam.Start3D2D( pos, ang, scale )

			surface.SetDrawColor( (1 - hfrac) * 255, hfrac * 255, 0, alpha )
			surface.DrawRect( 
				-w / 2, -h / 2,
				hfrac * w, h
			)

			surface.SetDrawColor( 255, 255, 255, alpha )
			surface.DrawOutlinedRect( 
				-w / 2, -h / 2,
				w, h
			)

			if hfrac <= 0 then
				draw.SimpleText(
					"Offline", 
					"Default", 
					0, 0, 
					Color( 255, 255, 255, 255 ),
					TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER
				)
			end

		cam.End3D2D()

		self.lastHealthDraw = CurTime()
	end
end
