
AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "sh_config.lua" )

include( "shared.lua" )
include( "sh_config.lua" )

hook.Add( "playerWanted", "rprotect_suppress_wanted_msg", function( ply, actor, reason )
	if actor == nil then return true end -- if actor is nil then suppress the message
end )

-- might need to be used by our derivatives

function ENT.CanAfford( ply, amt )
	local func = ply.CanAfford or ply.canAfford

	return func and func( ply, amt )
end

function ENT.AddMoney( ply, amt )
	local func = ply.AddMoney or ply.addMoney

	if func then
		func( ply, amt )
	end
end


function ENT:Initialize()	
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	
	self:SetUseType( SIMPLE_USE )
	
	local owner = self:Getowning_ent()

	self.Owned = IsValid( owner )

	if self.Owned then
		self.OwnerSteamID = owner:SteamID()

		if self.CPPISetOwner then
			self:CPPISetOwner( owner )
		end
	end
	
	local phys = self:GetPhysicsObject()

	if IsValid( phys ) then 
		phys:Wake()
	end

	self:FindAvailableName()

	if self.Damageable then
		self:SetHP( self.StartingHealth )
	end
end

function ENT:Use( ply )
	if !IsValid( ply ) or !ply:IsPlayer() then return end
	if !self.Damageable or !self.Breakable or self.FixMode != "manual" or self:GetHP() > 0 then return end
	if self:GetPos():Distance( ply:EyePos() ) > self.ManualFixDistance then return end

	if self.CanAfford( ply, self.ManualFixCost ) then
		self.AddMoney( ply, -self.ManualFixCost )

		self:SetHP( self.StartingHealth )

		if self.FixSound then
			self:EmitSound( self.FixSound )
		end

		return true -- tell our derivatives that we've captured this use event
	end
end

function ENT:Think()
	local lt = self.lastThink
	self.lastThink = CurTime()

	if !lt or !self.Damageable then return end

	local hp = self:GetHP()

	if hp > 0 then
		if self.AutoRepair and hp < self.StartingHealth then
			local dt = CurTime() - lt

			self:SetHP( math.min( hp + dt * self.AutoRepairRate, self.StartingHealth ) )
		end
	else
		if self.FixMode == "auto" and (CurTime() - self.lastBreakTime) >= self.AutoFixDelay then
			self:SetHP( self.StartingHealth )

			if self.FixSound then
				self:EmitSound( self.FixSound )
			end
		end
	end
end

function ENT:OnTakeDamage( dmg )
	if !self.Damageable or self:GetHP() <= 0 then return end

	self:SetHP( math.max( self:GetHP() - dmg:GetDamage(), 0 ) )

	if self.Breakable and self:GetHP() <= 0 then
		local attacker = dmg:GetAttacker()

		if self.BreakSound then
			self:EmitSound( self.BreakSound )
		end

		if self.SparkOnBreak then
			local edata = EffectData()
				edata:SetEntity( self )

			util.Effect( "entity_remove", edata ) 
		end

		if self.WantedOnBreak and IsValid( attacker ) and attacker.wanted then
			attacker:wanted( nil, "Destroyed " .. self:GetDeviceName() )
		end

		self.lastBreakTime = CurTime()
	end
end

function ENT:FindAvailableName()
	local cnt = 0

	for _, ent in pairs( ents.FindByClass( self.ClassName ) ) do
		if ent.Owned == self.Owned and ent != self and -- work for unowned permanent entities too
			(!ent.Owned or ent.OwnerSteamID == self.OwnerSteamID) then

			cnt = cnt + 1
		end
	end

	self:SetDeviceName( self.PrintName .. ' ' .. (cnt + 1) )
end
