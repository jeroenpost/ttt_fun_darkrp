
ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.PrintName = ""
ENT.Author = "Dan"
ENT.Spawnable = false
ENT.AdminSpawnable = false

-- these should be overridden

ENT.Damageable = true
ENT.StartingHealth = 100

ENT.AutoRepair = true
ENT.AutoRepairRate = 2

ENT.Breakable = true
ENT.SparkOnBreak = true
ENT.WantedOnBreak = false

ENT.FixMode = "manual" -- or "auto"
ENT.AutoFixDelay = 30
ENT.ManualFixCost = 500


function ENT:AddNetworkVar( type, name )
	self.nwvarscnt = self.nwvarscnt or {}

	self.nwvarscnt[type] = (self.nwvarscnt[type] or -1) + 1

	self:NetworkVar( type, self.nwvarscnt[type], name )
end

function ENT:SetupDataTables()
	self:AddNetworkVar( "Entity", "owning_ent" )

	self:AddNetworkVar( "String", "DeviceName" )

	self:AddNetworkVar( "Float", "HP" )
end
