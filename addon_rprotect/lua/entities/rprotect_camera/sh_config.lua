
-- these are explained in the README

ENT.Damageable = true
ENT.StartingHealth = 100

ENT.AutoRepair = true
ENT.AutoRepairRate = 1

ENT.Breakable = true
ENT.SparkOnBreak = true

ENT.FixMode = "manual"
ENT.AutoFixDelay = 30
ENT.ManualFixCost = 500
