
AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "sh_config.lua" )

include( "shared.lua" )
include( "sh_config.lua" )


function ENT:Initialize()
	local bsize = 12
	local bvec = Vector( bsize, bsize, bsize )

	self:SetModel( "models/tools/camera/camera.mdl" )

	self.BaseClass.Initialize( self )

	self:PhysicsInitBox( -bvec, bvec )

	if self:IsInWorld() then
		self:SetPos( self:GetPos() + Vector( 0, 0, bsize + 5 ) )
	end

	local phys = self:GetPhysicsObject()

	if IsValid( phys ) then
		phys:SetMass( 10 )
	end
end
