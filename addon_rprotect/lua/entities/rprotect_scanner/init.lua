
AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "sh_config.lua" )

include( "shared.lua" )
include( "sh_config.lua" )

util.AddNetworkString( "rprotect_scanner_settings" )

net.Receive( "rprotect_scanner_settings", function( len, ply )
	local ent = net.ReadEntity()
	local sound = net.ReadString()

	if !IsValid( ent ) or ent:GetClass() != "rprotect_scanner" or 
		ent:Getowning_ent() != ply or !ent.Sounds[sound] then return end

	ent.sound = sound
	ent.mode = net.ReadBit()
	ent.group = {}

	for i = 1, net.ReadUInt( 8 ) do
		ent.group[net.ReadString()] = true
	end

	if ent.TextAlertsEnabled then
		ent.sendText = net.ReadBool()
	end
end )


function ENT:Initialize()
	self:SetModel( "models/Items/battery.mdl" )
	
	self.BaseClass.Initialize( self )

	self.sound = self.DefaultSound
	self.mode = 0 -- include = 0, exclude = 1
	self.group = {}
	self.sendText = true

	for _, sinfo in pairs( self.Sounds ) do
		if !sinfo.Duration then
			sinfo.Duration = SoundDuration( sinfo.Path )
		end
	end
end

function ENT:OnRemove()
	if self.sounding then
		self.soundPatch:Stop()
	end
end

function ENT:Use( ply )
	if self.BaseClass.Use( self, ply ) then return end
	if !self.Owned or (ply != self:Getowning_ent() and !self.nonOwnerEdit) then return end

	net.Start( "rprotect_scanner_settings" )
		net.WriteEntity( self )
		net.WriteString( self.sound )
		net.WriteBit( self.mode == 1 )

		net.WriteUInt( table.Count( self.group ), 8 )

		for steamid in pairs( self.group ) do
			net.WriteString( steamid )
		end

		if self.TextAlertsEnabled then
			net.WriteBool( self.sendText )
		end
	net.Send( ply )
end

function ENT:Think()
	self.BaseClass.Think( self )

	if self.Damageable and self:GetHP() <= 0 then 
		self:StopSounding()

		return
	end

	self:NextThink( CurTime() )

	if self.sounding then
		if (CurTime() - self.soundStart) >= self.SoundDuration then
			self:StopSounding()
		else
			local sinfo = self.Sounds[self.sound]

			if !sinfo.Loops and 
				(CurTime() - self.lastSoundStart) > (sinfo.Duration + (sinfo.LoopDelay or 0)) then

				self.soundPatch:Stop()
				self.soundPatch:PlayEx( sinfo.Volume, sinfo.Pitch )

				self.lastSoundStart = CurTime()
			end
		end
	else
		local trace = self:TraceBeam()
		local ent = trace.Entity

		if !IsValid( ent ) then return true end

		local ply

		if ent:IsPlayer() then
			ply = ent
		elseif ent.Getowning_ent then
			ply = ent:Getowning_ent()
		elseif ent.CPPIGetOwner then
			ply = ent:CPPIGetOwner()
		end

		local isWorldProp = !IsValid( ply ) or !ply:IsPlayer()

		if  isWorldProp and !self.AlertOnWorldProp then return true end
		if !isWorldProp and ply == self:Getowning_ent() then return true end
		if !isWorldProp and (
			(self.mode == 0 and  self.group[ply:SteamID()]) or
			(self.mode == 1 and !self.group[ply:SteamID()])
		) then return true end

		local sinfo = self.Sounds[self.sound]

		self.sounding = true
		self.soundStart = CurTime()
		self.lastSoundStart = self.soundStart
		self.soundPatch = CreateSound( self, sinfo.Path )

		self.soundPatch:PlayEx( sinfo.Volume, sinfo.Pitch )

		local owner = self:Getowning_ent()

		if self.TextAlertsEnabled and self.sendText and IsValid( owner ) and owner:IsPlayer() then
			local msg = self:GetDeviceName() .. " was tripped"

			if !isWorldProp then
				if self.IncludeNameInAlert then
					msg = msg .. ([[ by %s]]):format( ply:Nick() )
				end

				if self.IncludeTeamInAlert then
					msg = msg .. ([[ (%s)]]):format( team.GetName( ply:Team() ) )
				end
			end

			msg = msg .. '.'

			owner:ChatPrint( msg )
		end
	end

	return true
end

function ENT:StopSounding()
	if self.sounding then
		self.soundPatch:Stop()

		self.sounding = false
	end
end
