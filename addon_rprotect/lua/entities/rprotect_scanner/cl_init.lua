
include( "shared.lua" )
include( "sh_config.lua" )

local mat = CreateMaterial( "rprotect_scanner_beam", "UnlitGeneric", {
	["$basetexture"] = "vgui/white",
	["$additive"] = 1,
	["$vertexcolor"] = 1,
	["$vertexalpha"] = 1
} )

local function addListSpacer( list, height, vmargins, color )
	vmargins = vmargins or 0

	local spacer = vgui.Create( "DPanel", list )
		spacer:SetTall( height + (vmargins * 2) )

	if color then
		spacer.Paint = function( self, w, h )
			surface.SetDrawColor( color )
			surface.DrawRect( 0, vmargins, w, height )
		end
	else
		spacer.Paint = nil
	end

	list:Add( spacer )
end

net.Receive( "rprotect_scanner_settings", function( len )
	local ent = net.ReadEntity()

	if !IsValid( ent ) then return end

	local sound = net.ReadString()

	if !ent.Sounds[sound] then return end

	local mode = net.ReadBit()
	local group = {}

	for i = 1, net.ReadUInt( 8 ) do
		group[net.ReadString()] = true
	end

	local textAlertsEnabled = ent.TextAlertsEnabled
	local sendText

	if textAlertsEnabled then
		sendText = net.ReadBool()
	end


	local frame = vgui.Create( "DFrame" )
		frame:SetTitle( "Scanner Settings" )
		frame:SetSize( 256, 512 )
		frame:Center()
		frame:MakePopup()

	local scroll = vgui.Create( "DScrollPanel", frame )
		scroll:SetPadding( 5 )
		scroll:SetPos( 5, 30 )
		scroll:SetSize( frame:GetWide() - 10, frame:GetTall() - 35 )

	local list = vgui.Create( "DListLayout", scroll )

	scroll.OldPerformLayout = scroll.PerformLayout
	scroll.PerformLayout = function( ... )
		list:SetWide( scroll:InnerWidth() - (scroll:GetVBar():IsVisible() and 2 or 0) )

		scroll.OldPerformLayout( ... )
	end


	local pnlSound = vgui.Create( "DPanel", list )
		pnlSound:SetTall( 30 )
		pnlSound.Paint = nil

	local lblSound = vgui.Create( "DLabel", pnlSound )
		lblSound:SetText( "Sound:" )
		lblSound:SizeToContents()

	local cmbSound = vgui.Create( "DComboBox", pnlSound )

	local sounds = table.GetKeys( ent.Sounds )

	table.sort( sounds )

	for _, name in pairs( sounds ) do
		cmbSound:AddChoice( name, nil, name == sound )
	end

	pnlSound.PerformLayout = function()
		pnlSound:SetTall( math.max( lblSound:GetTall(), cmbSound:GetTall() ) )

		lblSound:AlignLeft()
		lblSound:CenterVertical( 0.5 )

		cmbSound:SetWide( pnlSound:GetWide() - lblSound:GetWide() - 5 )
		cmbSound:AlignRight()
		cmbSound:CenterVertical( 0.5 )
	end

	list:Add( pnlSound )

	
	local cbSendText

	if textAlertsEnabled then
		addListSpacer( list, 4, 0 )

		local pnlSendText = vgui.Create( "DPanel", list )
			pnlSendText:SetTall( 30 )
			pnlSendText.Paint = nil

		local lblSendText = vgui.Create( "DLabel", pnlSendText )
			lblSendText:SetText( "Send text alert:" )
			lblSendText:SizeToContents()

		cbSendText = vgui.Create( "DCheckBox", pnlSendText )
			cbSendText:SetChecked( sendText )

		pnlSendText.PerformLayout = function()
			pnlSendText:SetTall( math.max( lblSendText:GetTall(), cbSendText:GetTall() ) )

			lblSendText:AlignLeft()
			lblSendText:CenterVertical( 0.5 )

			cbSendText:CenterVertical( 0.5 )
			cbSendText:SetPos( lblSendText.x + lblSendText:GetWide() + 5, cbSendText.y )
		end

		list:Add( pnlSendText )
	end


	local pnlMode = vgui.Create( "DPanel", list )
		pnlMode:SetTall( 30 )
		pnlMode.Paint = nil

	local lblMode = vgui.Create( "DLabel", pnlMode )
		lblMode:SetText( "Mode:" )
		lblMode:SizeToContents()

	local btnMode = vgui.Create( "DButton", pnlMode )
		btnMode.mode = mode
		btnMode:SetText( btnMode.mode == 0 and "Include selected" or "Exclude selected" )
		btnMode:SetTextColor( Color( 255, 255, 255, 255 ) )
		btnMode.DoClick = function( self )
			self.mode = self.mode == 0 and 1 or 0

			self:SetText( self.mode == 0 and "Include selected" or "Exclude selected" )
		end
		btnMode.Paint = function( self, w, h )
			if self:IsDown() then
				surface.SetDrawColor( 55, 55, 55, 255 )
			elseif self:IsHovered() then
				surface.SetDrawColor( 65, 65, 65, 255 )
			else
				surface.SetDrawColor( 75, 75, 75, 255 )
			end

			self:DrawFilledRect()
		end


	pnlMode.PerformLayout = function()
		lblMode:AlignLeft()
		lblMode:CenterVertical( 0.5 )

		btnMode:SetWide( pnlMode:GetWide() - lblMode:GetWide() - 5 )
		btnMode:AlignRight()
		btnMode:CenterVertical( 0.5 )
	end

	list:Add( pnlMode )

	addListSpacer( list, 2, 4, Color( 255, 255, 255, 100 ) )


	local lblGroup = vgui.Create( "DLabel", list )
		lblGroup:SetText( "Group Selection" )
		lblGroup:SizeToContents()

		list:Add( lblGroup )


	local players = player.GetAll()

	table.sort( players, function( p1, p2 )
		if group[p1:SteamID()] and !group[p2:SteamID()] then
			return true
		elseif !group[p1:SteamID()] and group[p2:SteamID()] then
			return false
		end
		
		return p1:Nick() < p2:Nick()
	end )

	for _, ply in pairs( players ) do
		if ply == LocalPlayer() then continue end -- since we have this open we're owner

		addListSpacer( list, 4, 0 )

		local btnPly = vgui.Create( "DButton", pnlMode )
			btnPly.selected = group[ply:SteamID()]
			btnPly:SetText( ply:Nick() )
			btnPly:SetTextColor( Color( 255, 255, 255, 255 ) )
			btnPly.DoClick = function( self )
				self.selected = !self.selected

				group[ply:SteamID()] = self.selected or nil -- removes from group table entirely
			end
			btnPly.Paint = function( self, w, h )
				local col = self.selected and 150 or 75

				if self:IsDown() then
					col = col - 20
				elseif self:IsHovered() then
					col = col - 10
				end

				surface.SetDrawColor( col, col, col, 255 )
				self:DrawFilledRect()
			end

			list:Add( btnPly )
	end


	frame.OnRemove = function()
		net.Start( "rprotect_scanner_settings" )
			net.WriteEntity( ent )
			net.WriteString( cmbSound:GetValue() )
			net.WriteBit( btnMode.mode == 1 )

			net.WriteUInt( table.Count( group ), 8 )

			for steamid in pairs( group ) do
				net.WriteString( steamid )
			end

			if textAlertsEnabled then
				net.WriteBool( cbSendText:GetChecked() )
			end
		net.SendToServer()
	end
end )


function ENT:Initialize()
	self:SetRenderBounds( 
		self:OBBMins() - Vector( self.BeamLength, 0, 0 ), 
		self:OBBMaxs()
	)
end

function ENT:Draw()
	self.BaseClass.Draw( self )

	if self.Damageable and self:GetHP() <= 0 then return end

	local trace = self:TraceBeam()

	render.SuppressEngineLighting( true )

		render.SetMaterial( mat )
		render.DrawBeam(
			trace.StartPos, 
			trace.HitPos,
			0.2,
			0, 1,
			Color( 255, 0, 0, 255 )
		)

	render.SuppressEngineLighting( false )
end
