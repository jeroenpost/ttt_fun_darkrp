
-- these are explained in the README

ENT.Damageable = true
ENT.StartingHealth = 100

ENT.AutoRepair = true
ENT.AutoRepairRate = 1

ENT.Breakable = true
ENT.SparkOnBreak = true

ENT.FixMode = "manual"
ENT.AutoFixDelay = 30
ENT.ManualFixCost = 500


ENT.AlertOnWorldProp = true -- alert if an unowned prop/entity crosses the beam
ENT.BeamLength = 200 -- length of the beam

ENT.TextAlertsEnabled = true -- allow text alerts
ENT.IncludeNameInAlert = true -- include offender's name in text alert
ENT.IncludeTeamInAlert = true -- include offender's team in text alert

ENT.SoundDuration = 10 -- how long to sound for (seconds)
ENT.DefaultSound = "Alarm" -- default sound when spawned (must be in the table below)
ENT.Sounds = {
	["Alarm"] = {
		Path = "ambient/alarms/alarm1.wav", -- path to sound
		Pitch = 100, -- pitch (100 = normal; 0 <= pitch <= 255)
		Volume = 1, -- volume (1 = normal/max; 0 <= volume <= 1)
		Loops = true -- whether the sound loops or we'll have to loop it manually
	},
	--[[["Turkey"] = {
		Path = "turkey_gobble.wav",
		Pitch = 100,
		Volume = 1,
		Loops = false, -- this sound does not loop
		LoopDelay = 0.1 -- since this sound does not loop automatically, we can specify the delay between loops
	}]]--
}
