
ENT.Type = "anim"
ENT.Base = "rprotect_base"
ENT.PrintName = "Scanner"
ENT.Author = "Dan"
ENT.Spawnable = false
ENT.AdminSpawnable = false

ENT.WantedOnBreak = false


function ENT:TraceBeam()
	local pos = self:LocalToWorld( Vector( -4.25, 0, 8.25 ) )
	local dir = self:GetForward() * -self.BeamLength
	
	return util.QuickTrace( pos, dir )
end
