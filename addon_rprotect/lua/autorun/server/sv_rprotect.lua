
local RemoveOnDisconnect = true -- whether to remove rProtect entities when a player disconnects


if RemoveOnDisconnect then
	timer.Create( "sv_rprotect_cleanup", 5, 0, function()
		for _, ent in pairs( ents.GetAll() ) do
			if IsValid( ent ) and ent.Base == "rprotect_base" and 
				ent.Owned and ent.Getowning_ent and !IsValid( ent:Getowning_ent() ) then

				ent:Remove()
			end
		end
	end )
else
	hook.Add( "PlayerInitialSpawn", "sv_rprotect_setowner", function( ply )
		local sid = ply:SteamID()

		for _, ent in pairs( ents.GetAll() ) do
			if IsValid( ent ) and ent.Base == "rprotect_base" and ent.OwnerSteamID == sid then
				ent:Setowning_ent( ply )

				if ent.CPPISetOwner then
					ent:CPPISetOwner( ply )
				end
			end
		end
	end )
end
