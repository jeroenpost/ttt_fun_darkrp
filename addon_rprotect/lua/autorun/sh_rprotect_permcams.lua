
AddCSLuaFile()

local addCameraTerminalPair

-- default permanent camera settings (you can create different settings for different sets of cameras)
-- these options are explained in the README

local cameraSettings = {
	Damageable = true,
	StartingHealth = 100,

	AutoRepair = true,
	AutoRepairRate = 2,

	Breakable = true,
	SparkOnBreak = true,
	WantedOnBreak = true,

	FixMode = "auto",
	AutoFixDelay = 30,
	ManualFixCost = 500
}

local function setupCameras()
	--[[ 

	Order is:
		camera position, camera angles,
		terminal position, terminal angles,
		camera settings table,
		(optional) camera name

	Here's an example:

	addCameraTerminalPair(
		Vector( 0, 0, 0 ), Angle( 0, 0, 0 ),
		Vector( 0, 0, 100 ), Angle( 0, 0, 0 ),
		cameraSettings,
		"Camera 1"
	)

	For step-by-step instructions, see the README.

	]]
end


local camtypes = {}

if SERVER then
	hook.Add( "InitPostEntity", "sv_rprotect_permcams_spawn", setupCameras )
else
	hook.Add( "Initialize", "sv_rprotect_permcams_spawn", setupCameras )

	concommand.Add( "rprotect_getposang", function()
		local ent = LocalPlayer():GetEyeTrace().Entity

		if !IsValid( ent ) then return end

		local pos = ent:GetPos()
		local ang = ent:GetAngles()

		print(
			([[Vector( %f, %f, %f ), Angle( %f, %f, %f ),]]):format(
				pos.x, pos.y, pos.z,
				ang.p, ang.y, ang.r
			)
		)
	end )
end

addCameraTerminalPair = function( campos, camangs, termpos, termangs, camsetts, name )
	assert( camsetts, "rProtect: You must provide a camera settings table." )

	if !camtypes[camsetts] then
		local ENT = table.Copy( scripted_ents.Get( "rprotect_camera" ) )
		local class = "rprotect_camera_perm" .. table.Count( camtypes )

		for k, v in pairs( camsetts ) do
			ENT[k] = v
		end

		camtypes[camsetts] = class

		scripted_ents.Register( ENT, class )
	end

	if SERVER then
		local cam = ents.Create( camtypes[camsetts] )
			cam:SetPos( campos )
			cam:SetAngles( camangs )
			cam:Spawn()

		if name then
			cam:SetDeviceName( name )
		end

		local phys = cam:GetPhysicsObject()

		if IsValid( phys ) then
			phys:EnableMotion( false )
		end

		local term = ents.Create( "rprotect_terminal" )
			term:SetPos( termpos )
			term:SetAngles( termangs )
			term:SetCamera( cam )
			term:Spawn()

		phys = term:GetPhysicsObject()

		if IsValid( phys ) then
			phys:EnableMotion( false )
		end


		cam.PhysgunPickup = function() return false end
		cam.CanTool = cam.PhysgunPickup

		term.PhysgunPickup = cam.PhysgunPickup
		term.CanTool = cam.CanTool
	end
end
