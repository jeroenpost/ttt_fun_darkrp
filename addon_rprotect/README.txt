- Installation

Extract to garrysmod/addons.



- Configuration

All configuration options exist in the files called 'sh_config.lua' in the entity folders and then various other configuration options
are in the files in the autorun folder. Instructions for the ones in autorun are included in those files, and configuration options specific
to a single entity are included in the comments of the corresponding sh_config files. You should NOT edit any of the entity files other than
its sh_config file.

You can enable/disable whether to remove rProtect devices on player disconnect in autorun/server/sv_rprotect.lua

You can change the prices and maximum number of a particular device in autorun/sh_rprotect_addents.lua. If you wish to add the entities yourself
manually, just comment out the AddEntity() calls or remove/rename the file to have a different extension.

You can add permanent cameras that spawn with the map in autorun/sh_rprotect_permcams.lua


*** To add permanent cameras ***

 Go into sh_rprotect_permcams.lua and modify the cameraSettings table as you like (the options in that table are explained below). Then find the
 example addCameraTerminalPair( ... ) and copy that. The vector and angle on the first line are for the camera, and the vector and angle on the
 second line are for the terminal. Spawn a camera, put it where you want, look at it and then in your console run `rprotect_getposang`, take what
 it prints and paste that over the first vector and angle in addCameraTerminalPair(). Then do the same with the terminal but paste it on the second
 line. Here's a step-by-step example.

 1. Start with:

        addCameraTerminalPair(
		Vector( 0, 0, 0 ), Angle( 0, 0, 0 ),   -- camera position and angle
		Vector( 0, 0, 100 ), Angle( 0, 0, 0 ), -- terminal position and angle
		cameraSettings,                        -- the settings table (you can make different ones if you want)
		"Camera 1"                             -- (optional) the name of the camera shown on the terminal
	)

 2. Spawn a camera, place it where you want, look at it and then run rprotect_getposang. You should get some code like this in your console:

        Vector( 873.143433, -218.612091, -72.775146 ), Angle( 0.000000, -45.016479, 0.000000 ),

 3. Put that code into the code you started with (DON'T FORGET THE LAST COMMA!), so it should now look like:

        addCameraTerminalPair(
		Vector( 873.143433, -218.612091, -72.775146 ), Angle( 0.000000, -45.016479, 0.000000 ),
		Vector( 0, 0, 100 ), Angle( 0, 0, 0 ),
		cameraSettings,
		"Camera 1"
	)

 4. Repeat Step 2 with the terminal. Let's say it gives you the following code for that:

        Vector( -4424.254395, 3216.135742, 1078.370850 ), Angle( 17.742920, -133.742065, 33.920288 ),

 5. Plug that code into the second line of the code you started with (DON'T FORGET THE LAST COMMA!), so it should now look like:


        addCameraTerminalPair(
		Vector( 873.143433, -218.612091, -72.775146 ), Angle( 0.000000, -45.016479, 0.000000 ),
		Vector( -4424.254395, 3216.135742, 1078.370850 ), Angle( 17.742920, -133.742065, 33.920288 ),
		cameraSettings,
		"Camera 1"
	)

 6. Done! Repeat until you have all the cameras that you want set up. As a side note, you don't need to give the name ("Camera 1") at the end,
    it'll generate that automatically. It's just an option, so you can delete that and the comma after cameraSettings if you want.


*** Options that apply to multiple device types are explained below. ***

 - Damageable ~ whether the device has damage enabled

 - StartingHealth ~ starting health of the device

 - AutoRepair ~ whether the device will automatically repair itself (regain HP) when it takes damage
 
 - AutoRepairRate ~ rate at which the device repairs itself (HP per second)

 - Breakable ~ whether the device can be "broken" when HP reaches 0

 - SparkOnBreak ~ whether to do a spark effect when it breaks

 - WantedOnBreak ~ whether to have DarkRP mark the player who broke the device as wanted (for things like CP cameras)

 - FixMode ~ accepts "auto" or "manual". If in manual mode, a player will have to fix the device by pressing USE on it after it breaks. If in auto
             repair mode, the device will automatically fix itself after a certain amount of time.

 - AutoFixDelay ~ how long it should stay broken before fixing itself in auto fix mode

 - ManualFixCost ~ how much it should cost for a player to fix a broken device in manual fix mode



- Contact

If you have any issues that weren't addressed here, or just need additional help, feel free to send me a PM or open a support ticket
on ScriptFodder.