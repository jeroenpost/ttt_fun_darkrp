##DarkRP Support##
DarkRP 2.4.3 ![Alt text](http://s3-eu-west-1.amazonaws.com/coderhire-ratings/991eb9bb1af4b20ea9f8a654cf571a59.png)  
DarkRP 2.5.0 ![Alt text](http://s3-eu-west-1.amazonaws.com/coderhire-ratings/991eb9bb1af4b20ea9f8a654cf571a59.png)  

Other custom versions of DarkRP are not supported, and don't count on my support if you're using a custom version of DarkRP with modified core features.

##Description##
Here's another simple vehicle addition to your server.  
It's a vehicle alarm.  
You can purchase the vehicle alarm in the F4 menu, and plant it on your vehicle (or any other vehicle).  
Just make it collide with the vehicle, and it will be activated.  
It get's activated if somebody who does not own the vehicle, enters it.  
There is also a config to notify the owner when it get's stolen.  

##Features##
- Entity in F4 menu.  
- Alarm sound.  
- Notification for owner (VEHALARM\_NotifyOwner true/false)

##Installation##
This addon supports both DarkRP 2.4.3 and DarkRP 2.5.0.  
**You will find the version for DarkRP 2.4.3 here:**  
DarkRP Vehicle Alarm/VehAlarm 2.4.3/DarkRP Vehicle Alarm  
Extract the last DarkRP Vehicle Alarm to your addons.  

**You will find the version for DarkRP 2.5.0 here:**  
DarkRP Vehicle Alarm/VehAlarm 2.5.0/DarkRP Vehicle Alarm  
Extract the last DarkRP Vehicle Alarm to your addons.  

##Content##
Counter Strike Source is required for the bomb and the defuse kit. These models can be replaced if you wish.  
Just edit it in the entities.  

##Customizing##
To customize the addons settings, go to DarkRP Vehicle Alarm/lua/autorun/server/alarm_server.lua.  
In the top of the file you can customize the settings shown below.  

VEHALARM_NotifyOwner = true  
VEHALARM_AlarmSound = "craphead_scripts/vehalarm/car_alarm.mp3"  

##Errors & Support##
If you find any problems with the script, please create a support ticket with details of the situation and a copy paste of the error in console. Additionally, i rarely give additional support for my scripts. If there is a general error with the script, an error that you can prove happens, and is my fault. Send me a PM here on CoderHire. We'll figure it out there, and perhaps a chat on Steam after I've responded to your PM. I am also not interested in modifying you a custom version of the addon. Also not upon payment. Sorry!

Conflicting addons is not to be said if I will support that or not. This is something I will decide upon confrontation about a conflicting addon. If you have some sort of proof that an addon is conflicting with my addon, please send me a PM with the details you might have.

Thank you!