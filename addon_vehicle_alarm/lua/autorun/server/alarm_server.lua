resource.AddFile("sound/craphead_scripts/vehalarm/car_alarm.mp3")

-- General Config
VEHALARM_NotifyOwner = true
VEHALARM_AlarmSound = "craphead_scripts/vehalarm/car_alarm.mp3"

function VEHALARM_EnterVehicle( ply, veh )
	local trace = ply:GetEyeTrace()

	timer.Simple(1,function()
        if not veh:IsVehicle() then
            return
        end
        local doorData = veh:getDoorData()

                if not veh:isKeysOwnedBy( ply ) and ply.ReallyInCar and veh:GetDriver() == ply then

                    if veh.AlarmActive then

                        if not veh.AlarmOn then
                            veh:EmitSound(VEHALARM_AlarmSound)
                            VC_HazardLightsOn(veh)

                            veh.AlarmOn = true
                            timer.Simple(23, function()
                                if not IsValid(veh) then return end
                                veh.AlarmOn = false
                                VC_HazardLightsOff(veh)
                            end)


                            if VEHALARM_NotifyOwner and IsValid(doorData.owner) then
                                DarkRP.notify( doorData.owner, 0, 5, "Somebody is stealing your vehicle!" )

                            end
                        end
                    end

                end
    end)
end
hook.Add("PlayerUse", "VEHALARM_EnterVehicle", VEHALARM_EnterVehicle)

function VEHALARM_FakeLeave( ply, veh )
	ply.ReallyInCar = false
end
hook.Add("PlayerLeaveVehicle", "VEHALARM_FakeLeave", VEHALARM_FakeLeave)

function VEHALARM_FakeEntered( ply, veh )
	ply.ReallyInCar = true
    VEHALARM_EnterVehicle(ply,veh)
end
hook.Add("PlayerEnteredVehicle", "VEHALARM_FakeEntered", VEHALARM_FakeEntered)