AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()
	self:SetModel( "models/props_lab/reciever01d.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	
	local phys = self:GetPhysicsObject()
	phys:Wake()
end

function ENT:PhysicsCollide( data, physobj )
	if data.DeltaTime > .5 then
		if data.HitEntity:GetClass() == "prop_vehicle_jeep" then			
			local TheVehicle = data.HitEntity
			TheVehicle.AlarmActive = true
			
			DarkRP.notify( self:Getowning_ent(), 0, 5, "The car alarm is now active!" )
			self:Remove()
		end
	end
end