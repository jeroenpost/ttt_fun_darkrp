Greetings, friend. 
If you're reading this file, you probably bought this script and thank you for that. :)

Instruction how to set spawnpoint for Cocaine Dealer:
- To set spawnpoint type in console "ecl_npc_spawn <uniqueID>"
- To remove spawnpoint type in console "ecl_npc_remove <uniqueID>"

Instruction how to set spawnpoint for Coca Plants:
- To set spawnpoint type in console "ecl_plant_spawn <uniqueID>"
- To remove spawnpoint type in console "ecl_plant_remove <uniqueID>"

So, instruction how to use this lab.:
1: You should find a coca plantations.
2: He needs to buy or get somewhere a box for collecting leaves.
3: So collect them now!
4: If box is filled, put leaves into a Kerosin.
5: Fill the Kerosin.
6: When it will be filled, shake it.
7: After that, you should put them into box with water to Draft them. Do this 2 times.
8: Shake this box.
9: Now, you should clean this mix with a Sulfuric Acid. Do this 2 times.
10: Put cleaned mixtures in a Pot.
11: Place Pot on a Stove and heat it up to the temperature about 72 degrees.
12: Wait until it be cool downed to the needed temperature.
13: So now you have a dirty cooked drug. Put it in the Gasoline and wait.
14: Goodjob, you got your first Cocaine! To get it from Gasoline, press Use.
15: Lets find a dealer to sell him your drug. When you will find him, put it near his legs and press Use.
16: Nice! Do you like your money in your pocket? :)