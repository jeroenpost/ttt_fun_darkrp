function ECL:SetLanguage(name)
	if SERVER then 
		print("[ECL]: 'ecl_config.lua' has been successfully loaded.")
		print("[ECL]: 'ecl_functions.lua' has been successfully loaded.")
	end

	local lang = self:GetLanguage(name);

	if lang then 
		if CLIENT then 
			ECL.Language = lang;
		else
			print("[ECL]: Language '"..name.."' has been successfully set.")
		end
	end
end

function ECL:CreateLanguage(name, tbl)
	ECL.Languages[name] = tbl;
end

function ECL:GetLanguage(name) 
	AddCSLuaFile("languages/ecl_language_"..name..".lua")
	include("languages/ecl_language_"..name..".lua")

	local lang = ECL.Languages[name]
	if lang then
		return lang;
	else
		ErrorNoHalt("[ECL]: Cannot find '"..name.."' language.")
	end;
end
