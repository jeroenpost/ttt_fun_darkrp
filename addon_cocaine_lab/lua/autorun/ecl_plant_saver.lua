if SERVER then
timer.Simple(5,
	function()
		if !file.IsDir("jb", "DATA") then
			file.CreateDir("jb", "DATA");
		end;

		if !file.IsDir("jb/ecl_plants", "DATA") then
			file.CreateDir("jb/ecl_plants", "DATA");
		end;
		   
		if !file.IsDir("jb/ecl_plants/"..string.lower(game.GetMap()).."", "DATA") then
			file.CreateDir("jb/ecl_plants/"..string.lower(game.GetMap()).."", "DATA");
		end;
	 
		for k, v in pairs(file.Find("jb/ecl_plants/"..string.lower(game.GetMap()).."/*.txt", "DATA")) do
			local treePosFile = file.Read("jb/ecl_plants/"..string.lower(game.GetMap()).."/"..v, "DATA");
			 
			local spawnNumber = string.Explode(" ", treePosFile);         
				   
			local npc = ents.Create("ecl_plant");
			npc:SetPos(Vector(spawnNumber[1], spawnNumber[2], spawnNumber[3]));
			npc:SetAngles(Angle(tonumber(spawnNumber[4]), spawnNumber[5], spawnNumber[6]));
			npc:Spawn();
		end;
	end
	);


        local function spawnPlantPos(ply, cmd, args)
            if (ply:IsAdmin() or ply:IsSuperAdmin()) then
                local fileNpcName = args[1];

                if !fileNpcName then
                ply:SendLua("local text = string.Replace(ECL.Language.Spawn.ChooseID, '<%donttouch%>', 'Plant'); local tab = {Color(255,165,0,255), ECL.Language.Spawn.ECL..[[ - ]], Color(255,255,255), [[]]..text..[[]] } chat.AddText(unpack(tab))");
                return;
                end;

                if file.Exists( "jb/ecl_plants/"..string.lower(game.GetMap()).."/ecl_plant_".. fileNpcName ..".txt", "DATA") then
                    ply:SendLua("local text = string.Replace(ECL.Language.Spawn.AlreadyInUseID, '<%donttouch%>', '"..fileNpcName.."'); local tab = {Color(255,165,0,255), ECL.Language.Spawn.ECL..[[ - ]], Color(255,255,255), [[]]..text..[[]] } chat.AddText(unpack(tab))");
                    return;
                end;

                local npcVector = string.Explode(" ", tostring(ply:GetEyeTrace().HitPos));
                local npcAngles = string.Explode(" ", tostring(ply:GetAngles()+Angle(0, -180, 0)));

                local npc = ents.Create("ecl_plant");
                npc:SetPos(ply:GetEyeTrace().HitPos);
                npc:SetAngles(ply:GetAngles()+Angle(0, -180, 0));
                npc:Spawn();
                npc:SetMoveType(MOVETYPE_NONE)

                file.Write("jb/ecl_plants/"..string.lower(game.GetMap()).."/ecl_plant_".. fileNpcName ..".txt", ""..(npcVector[1]).." "..(npcVector[2]).." "..(npcVector[3]).." "..(npcAngles[1]).." "..(npcAngles[2]).." "..(npcAngles[3]).."", "DATA");
                ply:SendLua("local text = string.Replace(ECL.Language.Spawn.NewPosSet, '<%donttouch%>', 'Plant'); local tab = {Color(255,165,0,255), ECL.Language.Spawn.ECL..[[ - ]], Color(255,255,255), [[]]..text..[[]] } chat.AddText(unpack(tab))");
            else
                ply:SendLua("local tab = {Color(255,165,0,255), ECL.Language.Spawn.ECL..[[ - ]], Color(255,255,255), [[]]..ECL.Language.Spawn.OnlyAdmins..[[]] } chat.AddText(unpack(tab))");
            end;
        end;

        concommand.Add("ecl_plant_spawn", spawnPlantPos);

        local function spawnNpcPos(ply, cmd, args)
            if (ply:IsAdmin() or ply:IsSuperAdmin()) then
                local fileNpcName = args[1];

                if !fileNpcName then
                ply:SendLua("local text = string.Replace(ECL.Language.Spawn.ChooseID, '<%donttouch%>', 'NPC'); local tab = {Color(255,165,0,255), ECL.Language.Spawn.ECL..[[ - ]], Color(255,255,255), [[]]..text..[[]] } chat.AddText(unpack(tab))");
                return;
                end;

                if file.Exists( "jb/ecl/"..string.lower(game.GetMap()).."/ecl_npc_".. fileNpcName ..".txt", "DATA") then
                    ply:SendLua("local text = string.Replace(ECL.Language.Spawn.AlreadyInUseID, '<%donttouch%>', '"..fileNpcName.."'); local tab = {Color(255,165,0,255), ECL.Language.Spawn.ECL..[[ - ]], Color(255,255,255), [[]]..text..[[]] } chat.AddText(unpack(tab))");
                    return;
                end;

                local npcVector = string.Explode(" ", tostring(ply:GetEyeTrace().HitPos));
                local npcAngles = string.Explode(" ", tostring(ply:GetAngles()+Angle(0, -180, 0)));

                local npc = ents.Create("ecl_npc");
                npc:SetPos(ply:GetEyeTrace().HitPos);
                npc:SetAngles(ply:GetAngles()+Angle(0, -180, 0));
                npc:Spawn();
                npc:SetMoveType(MOVETYPE_NONE)

                file.Write("jb/ecl/"..string.lower(game.GetMap()).."/ecl_npc_".. fileNpcName ..".txt", ""..(npcVector[1]).." "..(npcVector[2]).." "..(npcVector[3]).." "..(npcAngles[1]).." "..(npcAngles[2]).." "..(npcAngles[3]).."", "DATA");
                ply:SendLua("local text = string.Replace(ECL.Language.Spawn.NewPosSet, '<%donttouch%>', 'NPC'); local tab = {Color(255,165,0,255), ECL.Language.Spawn.ECL..[[ - ]], Color(255,255,255), [[]]..text..[[]] } chat.AddText(unpack(tab))");
            else
                ply:SendLua("local tab = {Color(255,165,0,255), ECL.Language.Spawn.ECL..[[ - ]], Color(255,255,255), [[]]..ECL.Language.Spawn.OnlyAdmins..[[]] } chat.AddText(unpack(tab))");
            end;
        end;
        concommand.Add("ecl_npc_spawn", spawnNpcPos);

        ECL.PotThink = true;

        function ECL:PotThink(ent)
            local cleaned = ent:GetNWInt("cleaned");
            local ingnited = ent:GetNWBool("ingnited");
            local maxAmount = ent:GetNWInt("max_amount");
            if ent.nextTick < CurTime() then
                local temp = ent:GetNWInt("temperature")

                if ingnited and cleaned == maxAmount then
                    ent:SetNWInt("temperature", temp+math.random(1,2))
                    ent:PlaySound();
                end;
                ent.nextTick = CurTime() + 1;
                ent:SetNWBool("ingnited", false)
                if !ingnited then
                ent:StopPlay();
                end

                if !ingnited and temp > ECL.Pot.Temperature then
                ent:SetNWInt("temperature", temp-math.random(0,1));
                end;

                if temp > ECL.Pot.ExplodeTemperature then
                    ent:StopPlay();
                    ent:Explode();
                end;
            end;
        end;

        ECL.StoveThink = true;

        function ECL:StoveThink(ent)
            local Ang = ent:GetAngles();
            Ang:RotateAroundAxis(Ang:Forward(), 90);

            local poses = {
                [1] = ent:GetPos()+Ang:Right()*-19.8+Ang:Forward()*-9.75+Ang:Up()*11.5,
                [3] = ent:GetPos()+Ang:Right()*-19.8+Ang:Forward()*2.75+Ang:Up()*11.5,
                [2] = ent:GetPos()+Ang:Right()*-19.8+Ang:Forward()*-9.75+Ang:Up()*-11.2,
                [4] = ent:GetPos()+Ang:Right()*-19.8+Ang:Forward()*2.75+Ang:Up()*-11.2
            };

            local plates = {
                [1] = ent:GetNWBool("left-top"),
                [2] = ent:GetNWBool("right-top"),
                [3] = ent:GetNWBool("left-bottom"),
                [4] = ent:GetNWBool("right-bottom")
            };
            local unabled = 0
            for k, v in pairs(plates) do
                if v then
                    unabled = unabled + 1;
                    local pos = poses[k];
                    local entities = ents.FindInSphere(pos,1);
                    for k2, ent in pairs(entities) do
                        local class = ent:GetClass();

                        if class == "ecl_pot" then
                            ent:SetNWBool("ingnited", true);
                        end
                    end
                end
            end

            if unabled > 0 then
                local gas = ent:GetNWInt("gas");
                if gas > 0 then
                    ent:SetNWInt("gas", math.Round(gas - 1.5*unabled));
                else
                    ent:SetNWBool("left-top", false);
                    ent:SetNWBool("left-bottom", false);
                    ent:SetNWBool("right-top", false);
                    ent:SetNWBool("right-bottom", false);
                end;
            end;
        end;

	local function removePlantPos(ply, cmd, args)
		if (ply:IsAdmin() or ply:IsSuperAdmin()) then
			local fileNpcName = args[1];
				   
			if !fileNpcName then
				ply:SendLua("local tab = {Color(255,165,0,255), ECL.Language.Spawn.ECL..[[ - ]], Color(255,255,255), [[]]..ECL.Language.Spawn.EnterUniqueID..[[]] } chat.AddText(unpack(tab))");
				return;
			end;
					   
			if file.Exists("jb/ecl_plants/"..string.lower(game.GetMap()).."/ecl_plant_"..fileNpcName..".txt", "DATA") then
				file.Delete("jb/ecl_plants/"..string.lower(game.GetMap()).."/ecl_plant_"..fileNpcName..".txt");
				ply:SendLua("local text = string.Replace(ECL.Language.Spawn.Remove, '<%donttouch%>', 'Plant'); local tab = {Color(255,165,0,255), ECL.Language.Spawn.ECL..[[ - ]], Color(255,255,255), [[]]..text..[[]] } chat.AddText(unpack(tab))");
				return;
			end;
				   
		else
			ply:SendLua("local tab = {Color(255,165,0,255), ECL.Language.Spawn.ECL..[[ - ]], Color(255,255,255), [[]]..ECL.Language.Spawn.OnlyAdmins..[[]] } chat.AddText(unpack(tab))");                       
		end;
	end;
	concommand.Add("ecl_plant_remove", removePlantPos);
end