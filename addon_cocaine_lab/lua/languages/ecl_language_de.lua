local Language = {}
Language.Spawn = {}
Language.Dealer = {}
Language.Entities = {}
 
Language.Spawn.ECL 					= "Enh. Kokain-Labor"; 
Language.Spawn.ChooseID 			= "Wählen Sie eine eindeutige ID für Ihre <%donttouch%>."; 
Language.Spawn.AlreadyInUseID 		= "Diese einzigartige ID wird bereits verwendet wird, wählen Sie einen anderen oder Typ 'ecl_npc_remove <%donttouch%>' in Konsole, diese zu entfernen."; 
Language.Spawn.NewPosSet 			= "Neue Position für den <%donttouch%> wurde gesendet."; 
Language.Spawn.OnlyAdmins 			= "Dies können nur Superadmins oder Admins benutzen."; 
Language.Spawn.EnterUniqueID 		= "Bitte gebe eine ID ein!"; 
Language.Spawn.Remove 				= "Das <%donttouch%> wurde Gelöscht. Restarte deinen Server!";
Language.Dealer.CannotFind          = "Was ist das? Legen Sie es auf dem Boden in der Nähe von meinem Bein ab.";
Language.Dealer.GoodStuff           = "Arrr! Guter Stoff. Dies ist dein Geld, du hast es Erhalten also RAUS HIER!";
Language.Entities.Cocaine           = "Kokain";
Language.Entities.DraftedLeaves     = "Bearbeitete Blätter";
Language.Entities.Water             = "Wasser";
Language.Entities.LeavesInKerosin   = "Blätter in Kerosin";
Language.Entities.Gas               = "Gas";
Language.Entities.Filled            = "Gefüllt";
Language.Entities.Gasoline          = "Benzin";
Language.Entities.CookedDirtyDrug   = "Schmutzige Gekochte Drogen";
Language.Entities.Kerosin           = "Kerosin";
Language.Entities.CocaLeaves        = "Coca-Blätter";
Language.Entities.Box               = "Box";
Language.Entities.CocaineDealer     = "Kokain Verkäufer";
Language.Entities.Plant             = "Pflanze";
Language.Entities.IsNotSeeded       = "Das ist nicht ausgesät";
Language.Entities.ShakeIt           = "schütteln Sie es";
Language.Entities.ReadyToUse        = "Einsatzbereit";
Language.Entities.WaitAbout         = "Bitte Warten";
Language.Entities.Sec               = "Sekunde.";
Language.Entities.BoxIsFilled       = "Die Box ist voll";
Language.Entities.IsSeeded          = "ist ausgesät";
Language.Entities.Temperature       = "Temperatur";
Language.Entities.CocaSeed          = "Coca Samen";
Language.Entities.Pot               = "Pflanze";
Language.Entities.CleanedLeaves     = "gereinigt Blätter";
Language.Entities.Plates            = "Die Platten";
Language.Entities.SulfuricAcid      = "Schwefelsäure";
 
ECL.Languages["de"] = Language;