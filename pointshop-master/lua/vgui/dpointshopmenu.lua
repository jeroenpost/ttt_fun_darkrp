
local PurpleColorTop = Color(25,60,72)
local PurpleColorBottom = Color(47,103,128)

local PointshopWidth = 1024

surface.CreateFont('PS_Heading', { font = 'coolvetica', size = 35 })
surface.CreateFont('PS_Heading2', { font = 'coolvetica', size = 16 })
surface.CreateFont('PS_Heading3', { font = 'coolvetica', size = 19 })

surface.CreateFont('PS_PlayerName', { font = 'calibri', size = 19 })
surface.CreateFont('PS_SmallerText', { font = 'calibri', size = 16 })
surface.CreateFont('PS_CatName', { font = 'calibri', size = 15 })

surface.CreateFont('PS_Subcats', { font = 'calibri', size = 40 })
surface.CreateFont('PS_SubcatDesc', { font = 'calibri', size = 18 })

local ALL_ITEMS = 1
local OWNED_ITEMS = 2
local UNOWNED_ITEMS = 3

local SHOP_STORE = 1
local SHOP_INVENTORY = 2
local SHOP_DONATE = 3
local SHOP_ADMIN = 4

local grad_up = surface.GetTextureID('vgui/gradient_up')
local grad_dw = surface.GetTextureID('vgui/gradient_down')

local SHOP_TABS = {"Store", "Inventory", "Donate for ranks & points"}

local ILYA = {} -- poot

local function BuildItemMenu(menu, ply, itemstype, callback)
        local plyitems = ply:PS_GetItems()


        for _, categoryid in ipairs(PS.Config.Categories) do

                local CATEGORY = PS.Categories[ categoryid[1] ]

                local catmenu = menu:AddSubMenu(CATEGORY.Name)

                for item_id, ITEM in pairs(PS.Items) do
                        if ITEM.Category == CATEGORY.Name then
                                if itemstype == ALL_ITEMS or (itemstype == OWNED_ITEMS and plyitems[item_id]) or (itemstype == UNOWNED_ITEMS and not plyitems[item_id]) then
                                        catmenu:AddOption(ITEM.Name, function() callback(item_id) end)
                                end
                        end
                end
        end
end

local function BuildItemUserMenu(menu, ply,  itemstype, callback)
    local plyitems = ply:PS_GetItems()

    local catmenu = menu:AddSubMenu("Your Items")
    local steamid = LocalPlayer():SteamID();

    table.sort(PS.Items, "Name", true)

    for item_id, ITEM in pairs(PS.Items) do
        if CLIENT and ITEM.Owner then
            if (steamid == ITEM.Owner) and ( (itemstype == OWNED_ITEMS and plyitems[item_id]) or (itemstype == UNOWNED_ITEMS and not plyitems[item_id]
            )) then
                catmenu:AddOption(ITEM.Name, function() callback(item_id) end)
            end
        end
    end

end

local function ShopGetCategoryOwnedCount( cat )
	local n = 0
	local _items = LocalPlayer():PS_GetItems()

	for _, item in pairs(PS.Items)do
		if item.Category == cat.Name and _items[_] then
			n = n + 1
		end
	end

	return n > 0
end

function ILYA:RemoveElemList( s )
	if IsValid(s.SideScroll) then
		s.SideScroll:Remove()
	end
	if IsValid(s.previewpanel) then
		s.previewpanel:Remove()
	end
	if IsValid(s.notify) then
		s.notify:Remove()
	end
	if IsValid(s.Scroll) then
		s.Scroll:Remove()
	end
	if IsValid(s.givebutton) then
		s.givebutton:Remove()
	end
end

function ILYA:NotifyAccess( s, t )
	s.notify = vgui.Create( 'DLabel', s)
	s.notify:SetText(t or 'No Access!')
	s.notify:SetPos( 260, 110 )
	s.notify:SetSize( s:GetWide()-270, 20 )
	s.notify:SetFont('PS_CatName')
	s.notify:SetTextColor(Color(100,100,100,170))
end

function ILYA:CreatePlayerOptions( s )

end


function ILYA:CreateDonatePage( s )

    s.Scroll = vgui.Create( 'DScrollPanel', s)
    s.Scroll:SetSize( s:GetWide()-260, s:GetTall()-210)
    s.Scroll:SetPos( 255, 105 )
    s.Scroll.VBar.Paint = function( s, w, h )
        draw.RoundedBox( 4, 3, 13, 8, h-24, Color(0,0,0,70))
    end
    s.Scroll.VBar.btnUp.Paint = function( s, w, h ) end
    s.Scroll.VBar.btnDown.Paint = function( s, w, h ) end
    s.Scroll.VBar.btnGrip.Paint = function( s, w, h )
        draw.RoundedBox( 4, 5, 0, 4, h+22, Color(0,0,0,70))
    end

    local html = vgui.Create( "HTML", s.Scroll )
    html:SetSize( s:GetWide() , s:GetTall()  )
    html:SetPos( 0, 0 )
    html:OpenURL( "https://ttt-fun.com/donate" )
end

function ILYA:CreateAdminMenu( s )
	s.Scroll = vgui.Create( 'DScrollPanel', s)
    s.Scroll:SetSize( s:GetWide()-260, s:GetTall()-210)
    s.Scroll:SetPos( 255, 105 )
	s.Scroll.VBar.Paint = function( s, w, h )
		draw.RoundedBox( 4, 3, 13, 8, h-24, Color(0,0,0,70))
	end
	s.Scroll.VBar.btnUp.Paint = function( s, w, h ) end
	s.Scroll.VBar.btnDown.Paint = function( s, w, h ) end
	s.Scroll.VBar.btnGrip.Paint = function( s, w, h )
		draw.RoundedBox( 4, 5, 0, 4, h+22, Color(0,0,0,70))
	end

	local ClientsList = vgui.Create('DListView', s.Scroll)
	ClientsList:SetSize( s:GetWide()-260, s:GetTall()-210 )

	local _nice_paint = function( s, w, h )
		draw.RoundedBox( 0, 1, 1, w-2, h-2, Color(57, 61, 72))
		draw.RoundedBox( 0, 2, 2, w-4, h-4, Color(255, 255, 255, 10))
	end

	ClientsList:SetMultiSelect(false)
	local _1 = ClientsList:AddColumn('Name')
	local _2 = ClientsList:AddColumn('Points')
	local _3 = ClientsList:AddColumn('Items')

	_2:SetFixedWidth(60)
	_3:SetFixedWidth(60)

	_1.Header.Paint = _nice_paint
	_2.Header.Paint = _nice_paint
	_3.Header.Paint = _nice_paint

	_1.Header:SetTextColor(Color(255,255,255))
	_2.Header:SetTextColor(Color(255,255,255))
	_3.Header:SetTextColor(Color(255,255,255))

	ClientsList.Paint = function( ss, w, h )
	end

	ClientsList.OnClickLine = function(parent, line, selected)
		local ply = line.Player

		local menu = DermaMenu()

		menu:AddOption('Set '..PS.Config.PointsName..'...', function()
			Derma_StringRequest(
				"Set "..PS.Config.PointsName.." for " .. ply:GetName(),
				"Set "..PS.Config.PointsName.." to...",
				"",
				function(str)
					if not str or not tonumber(str) then return end

					net.Start('PS_SetPoints')
						net.WriteEntity(ply)
						net.WriteInt(tonumber(str), 32)
					net.SendToServer()
				end
			)
		end)

		menu:AddOption('Give '..PS.Config.PointsName..'...', function()
			Derma_StringRequest(
				"Give "..PS.Config.PointsName.." to " .. ply:GetName(),
				"Give "..PS.Config.PointsName.."...",
				"",
				function(str)
					if not str or not tonumber(str) then return end

					net.Start('PS_GivePoints')
						net.WriteEntity(ply)
						net.WriteInt(tonumber(str), 32)
					net.SendToServer()
				end
			)
		end)

		menu:AddOption('Take '..PS.Config.PointsName..'...', function()
			Derma_StringRequest(
				"Take "..PS.Config.PointsName.." from " .. ply:GetName(),
				"Take "..PS.Config.PointsName.."...",
				"",
				function(str)
					if not str or not tonumber(str) then return end

					net.Start('PS_TakePoints')
						net.WriteEntity(ply)
						net.WriteInt(tonumber(str), 32)
					net.SendToServer()
				end
			)
		end)

		menu:AddSpacer()

		BuildItemMenu(menu:AddSubMenu('Give Item'), ply, UNOWNED_ITEMS, function(item_id)
			net.Start('PS_GiveItem')
				net.WriteEntity(ply)
				net.WriteString(item_id)
			net.SendToServer()
		end)

		BuildItemMenu(menu:AddSubMenu('Take Item'), ply, OWNED_ITEMS, function(item_id)
			net.Start('PS_TakeItem')
				net.WriteEntity(ply)
				net.WriteString(item_id)
			net.SendToServer()
		end)

		menu:Open()
	end

	s.Scroll:AddItem(ClientsList)
	s.ClientsList = ClientsList

end

function ILYA:CreateUserCustomMenu( s )
    s.Scroll = vgui.Create( 'DScrollPanel', s)
    s.Scroll:SetSize( s:GetWide()-260, s:GetTall()-210)
    s.Scroll:SetPos( 255, 105 )
    s.Scroll.VBar.Paint = function( s, w, h )
        draw.RoundedBox( 4, 3, 13, 8, h-24, Color(0,0,0,70))
    end
    s.Scroll.VBar.btnUp.Paint = function( s, w, h ) end
    s.Scroll.VBar.btnDown.Paint = function( s, w, h ) end
    s.Scroll.VBar.btnGrip.Paint = function( s, w, h )
        draw.RoundedBox( 4, 5, 0, 4, h+22, Color(0,0,0,70))
    end

    local ClientsList = vgui.Create('DListView', s.Scroll)
    ClientsList:SetSize( s:GetWide()-260, s:GetTall()-210 )

    local _nice_paint = function( s, w, h )
        draw.RoundedBox( 0, 1, 1, w-2, h-2, Color(57, 61, 72))
        draw.RoundedBox( 0, 2, 2, w-4, h-4, Color(255, 255, 255, 10))
    end

    ClientsList:SetMultiSelect(false)
    local _1 = ClientsList:AddColumn('Name')

    _1.Header.Paint = _nice_paint
    _1.Header:SetTextColor(Color(255,255,255))

    ClientsList.Paint = function( ss, w, h )
    end

    ClientsList.OnClickLine = function(parent, line, selected)
        local ply = line.Player

        local menu = DermaMenu()


        BuildItemUserMenu(menu:AddSubMenu('Give Item'), ply, UNOWNED_ITEMS, function(item_id)
            net.Start('PS_GiveItem')
            net.WriteEntity(ply)
            net.WriteString(item_id)
            net.SendToServer()
        end)

        BuildItemUserMenu(menu:AddSubMenu('Take Item'), ply, OWNED_ITEMS, function(item_id)
            net.Start('PS_TakeItem')
            net.WriteEntity(ply)
            net.WriteString(item_id)
            net.SendToServer()
        end)

        menu:Open()
    end

    s.Scroll:AddItem(ClientsList)
    s.ClientsList = ClientsList

end

function PS_GetSubcategories( cat )

    for _,i in ipairs( PS.Config.Categories ) do
        if i[1] == cat then
            return i
        end
    end

end

local scrolX = 255
local scrolY = 105

function ILYA:CreateItemList( s, inv )
	local _items = LocalPlayer():PS_GetItems()

	local CATEGORY = PS.Categories[s.CurrentCat]

    if not CATEGORY or not LocalPlayer().PS_Points  then
        LocalPlayer():PrintMessage( HUD_PRINTCENTER, "Pointshop isn't loaded yet. Please wait a few seconds and try again" )
        return
    end

    s.Scroll = vgui.Create( 'DScrollPanel', s)
    s.Scroll:SetSize( s:GetWide()-460, s:GetTall()-110)
    s.Scroll:SetPos( scrolX, scrolY )
	s.Scroll.VBar.Paint = function( s, w, h )
		draw.RoundedBox( 4, 3, 13, 8, h-24, Color(200,200,200,255))
	end
	s.Scroll.VBar.btnUp.Paint = function( s, w, h ) end
	s.Scroll.VBar.btnDown.Paint = function( s, w, h ) end
	s.Scroll.VBar.btnGrip.Paint = function( s, w, h )
		draw.RoundedBox( 4, 5, 0, 4, h+22, Color(150,150,150,255))
    end

	local _c = 7
	local itemNumber = 0
    local itemRows = 0

	s.Grid = vgui.Create( 'DGrid', s.Scroll )
	s.Grid:SetPos( 0, 72 )
	s.Grid:SetCols( _c )
	s.Grid:SetColWide( 72)
	s.Grid:SetRowHeight( 72 )

	--s.Scroll:AddItem(s.Grid)

	s.previewpanel = vgui.Create('DPointShopPreview', s)
	s.previewpanel:SetSize( 200, 340 )
	s.previewpanel:SetPos( s:GetWide()-205, 105 )
	local _oldpaint = s.previewpanel.Paint
	s.previewpanel.Paint = function( s, w, h )
		draw.RoundedBox( 0, 0, 0, w, h, Color(57, 61, 72))
		draw.RoundedBox( 0, 1, 1, w-2, h-2, Color(255, 255, 255, 10))
		draw.RoundedBox( 0, 2, 2, w-4, h-4, Color(57, 61, 72))
		_oldpaint( s, w, h )
	end

	 if PS.Config.CanPlayersGivePoints then
         s.givebutton = vgui.Create('DButton', s)
         s.givebutton:SetText('')
		 s.givebutton:SetSize(200, 20)
		 s.givebutton:SetPos(s:GetWide()-205, 105+345)
		 s.givebutton.Paint = function( ss, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color(57, 61, 72))
			draw.RoundedBox( 0, 1, 1, w-2, h-2, Color(255, 255, 255, 10))
			draw.RoundedBox( 0, 2, 2, w-4, h-4, Color(57, 61, 72))
			draw.SimpleText('Give '..PS.Config.PointsName, 'PS_CatName', w/2, h/2, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)

		 end
		 s.givebutton.DoClick = function()
             vgui.Create('DPointShopGivePoints')
         end
     end

    -- The title ####################
            local header = vgui.Create('DLabel', s.Scroll)
            header:SetSize(700,40)
            --DockMargin(10, 120, 240, 10)
            header:SetColor(Color(0,0,0,255))
            header:SetFont( "PS_Subcats" )
            header:SetText( CATEGORY.Name )
            header:SetPos(0,0)

            local header2 = vgui.Create('DLabel',s.Scroll)
            header2:SetSize(700,20)
            header2:SetColor(Color(80,80,80,255))
            header2:SetFont( "PS_SubcatDesc" )
            header2:SetText( CATEGORY.Desc or " " )
            header2:SetPos(0,40)

    -- The title end ####################
    local allopenitems = {}
	for _, item in pairs(PS.Items)do
		if item.Category == CATEGORY.Name then
			if inv then
				if not _items[_] then continue end
			end
            itemNumber = itemNumber +1

            local model = PS_CreateItem( item, s, 0)

			s.Grid:AddItem(model)
		end
        end

        local subcategories = {}
        local subcats = PS_GetSubcategories( s.CurrentCat )
        if not subcats or not subcats[2] then return end

        for _, subcat in ipairs(subcats) do
            if _ < 2 then continue end
            if not  PS.Categories[subcat] then print( subcat.." wrong") continue end
            subcategories[ _ ] = PS.Categories[subcat];
            end

            -- SUBCATEGORIES
            s.Grid2 = {}
            j = 0
            local positionTop = 0
            for _,subCat in pairs(subcategories) do
                itemRows = math.ceil(itemNumber/_c) + itemRows
                itemNumber = 0
                j = j+1

                positionTop =  (itemRows * 72) + (j*85) + 85

                s.Grid2[j] = vgui.Create( 'DGrid', s.Scroll )
                s.Grid2[j]:SetPos( 0, positionTop )
                s.Grid2[j]:SetCols( _c )
                s.Grid2[j]:SetColWide( 72 )
                s.Grid2[j]:SetRowHeight( 72 )

                s.Scroll:AddItem(s.Grid2[j])

                if subCat.Desc and subCat.Name  then

                   local header = vgui.Create('DLabel', s.Scroll)
                    header:SetSize(700,40)
                    --DockMargin(10, 120, 240, 10)
                    header:SetColor(Color(0,0,0,255))
                    header:SetFont( "PS_Subcats" )
                    header:SetText( subCat.Name )
                    header:SetPos( 0, positionTop-70 )


                   local header2 = vgui.Create('DLabel', s.Scroll)
                    header2:SetSize(700,20)
                    header2:SetColor(Color(80,80,80,255))
                    header2:SetFont( "PS_SubcatDesc" )
                    header2:SetText( subCat.Desc )
                    header2:SetPos( 0 , positionTop-30)
                end

                for _, item in pairs(PS.Items)do
                    if item.Category == subCat.Name then
                        if inv then
                            if not _items[_] then continue end
                        end
                                itemNumber = itemNumber +1

                            local model = PS_CreateItem( item, s, positionTop - 70)
                                s.Grid2[j]:AddItem(model)
                            end
                        end

             end

end

function PS_CreateItem( item, s, postop)

    local model = vgui.Create('DPointShopItem', s.Grid)
    model:SetData(item)
    model:SetSize(64,64)
    model.old_cursorentered = model.OnCursorEntered
    model.old_cursorexited = model.OnCursorExited

    function model:OnCursorEntered()

        local model2 = vgui.Create('DPointShopItem',  s.Scroll)
        local x, y = model:GetPos()
        local xpos = (x - 25) --+ scrolX
        local ypos = (y + 50) + postop
        model2:SetData(item)
        model2:SetSize(128,128)
        model2:SetPos( xpos, ypos )
        model:SetSize(1,1)

        function model2:OnCursorExited()
            model:SetSize(64,64)
            model:old_cursorexited()
            self:Remove()
        end

        function model2:DoClick()
            model:DoClick()
        end

        model2.Paint = function( s, w, h )
            draw.RoundedBox( 0, 0, 0, w, h, Color(52,52,52,200))
            draw.RoundedBox( 0, 0, 0, w-1, h-1, Color(205,205,205,255))
        end

        self:old_cursorentered()
    end

    function model:OnCursorExited()
        model:old_cursorexited()
    end

    model.Paint = function( s, w, h )
        draw.RoundedBox( 0, 0, 0, w, h, Color(52,52,52,200))
        draw.RoundedBox( 0, 0, 0, w-1, h-1, Color(205,205,205,255))
        s.BarColor = Color(0,0,0,0)
        s.DoStripes = false

        local points = PS.Config.CalculateBuyPrice(LocalPlayer(), s.Data)

        if not LocalPlayer():PS_HasPoints(points) then
            s.BarColor = Color(214, 139, 139, 5)
            s.DoStripes = true
        end

        if LocalPlayer():PS_HasItem(s.Data.ID) then
            s.BarColor = Color(139, 214, 143, 5)
            s.DoStripes = true
        end

        if s.DoStripes and not inv then
            for _ = 0, 6 do
                surface.SetMaterial(Material("vgui/white"))
                surface.SetDrawColor(s.BarColor)
                surface.DrawTexturedRectRotated(_*20, _*20, 200, 15, 45)
            end
        end
    end
    return model
end

function ILYA:CreateShopSideBar( s, inv, Player )
	s.SideScroll = vgui.Create( 'DScrollPanel', s)
    s.SideScroll:SetSize( 250, s:GetTall()-150)
    s.SideScroll:SetPos( 0, 150 )
	s.SideScroll.VBar.Paint = function( s, w, h )
        draw.RoundedBox( 4, 3, 13, 8, h-24, Color(0,0,0,70))
	end
	s.SideScroll.VBar.btnUp.Paint = function( s, w, h ) end
	s.SideScroll.VBar.btnDown.Paint = function( s, w, h ) end
	s.SideScroll.VBar.btnGrip.Paint = function( s, w, h )
		draw.RoundedBox( 4, 5, 0, 4, h+22, Color(0,0,0,70))
	end

	if Player then
		local btn = vgui.Create( 'DButton', s.SideScroll)
		btn:SetPos( 0, 0 )
		btn:SetSize( 250, 50 )
		btn:SetText("")
		btn.OnCursorEntered = function(ss) ss.s = true end
		btn.OnCursorExited = function(ss) ss.s = false end
		btn.Paint = function( ss, w, h )
			draw.RoundedBox(0, 0, 0, w, h, Color(52, 56, 67, 50))
				surface.SetDrawColor(0,0,0,70)
				surface.SetTexture(grad_dw)
				surface.DrawTexturedRect( 0, 0, w, h/5 )
					surface.SetDrawColor(0,0,0,70)
					surface.SetTexture(grad_up)
					surface.DrawTexturedRect( 0, h-h/5, w, h/5 )
			draw.SimpleText('Players', 'PS_CatName', 29, h/2, color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
			draw.RoundedBox(0, 0, 0, w, 1, Color(64, 68, 80))
			draw.RoundedBox(0, 0, h-1, w, 1, Color(64, 68, 80))
		end
		return
	end

	local i = 0
	for _, catid in ipairs(PS.Config.Categories) do

        local cat = PS.Categories[catid[1]]

        if not cat then print( "###"..catid[1].." broken.........") continue end

		if inv then
			--if not ShopGetCategoryOwnedCount( cat ) then continue end
		end
		if cat.AllowedUserGroups and #cat.AllowedUserGroups > 0 then
            if not table.HasValue(cat.AllowedUserGroups, LocalPlayer():PS_GetUsergroup()) then
             --   continue
             end
        end
        if cat.CanPlayerSee then
            --if not cat:CanPlayerSee(LocalPlayer()) then
             --    continue
            --end
        end
		local btn = vgui.Create( 'DButton', s.SideScroll)
		btn:SetPos( 0, i*49 )
		btn:SetSize( 250, 50 )
		btn:SetText("")
		btn.OnCursorEntered = function(ss) ss.s = true end
		btn.OnCursorExited = function(ss) ss.s = false end
		btn.Paint = function( ss, w, h )
			if _ == s.CurrentCat then
				draw.RoundedBox(0, 0, 0, w, h, Color(52, 56, 67, 50))
				surface.SetDrawColor(0,0,0,70)
				surface.SetTexture(grad_dw)
				surface.DrawTexturedRect( 0, 0, w, h/5 )
				surface.SetDrawColor(0,0,0,70)
				surface.SetTexture(grad_up)
				surface.DrawTexturedRect( 0, h-h/5, w, h/5 )
			elseif ss.s then
				draw.RoundedBox(0, 0, 0, w, h, Color(52, 56, 67, 50))
				surface.SetDrawColor(0,0,0,30)
				surface.SetTexture(grad_dw)
				surface.DrawTexturedRect( 0, 0, w, h/5 )
				surface.SetDrawColor(0,0,0,30)
				surface.SetTexture(grad_up)
				surface.DrawTexturedRect( 0, h-h/5, w, h/5 )
			else
				draw.RoundedBox(0, 0, 0, w, h, Color(52, 56, 67))
			end
			surface.SetDrawColor(0,0,0,100)
			surface.SetMaterial(Material('icon16/'..cat.Icon..'.png'))
			surface.DrawTexturedRect( 5, h/2-8, 16, 16 )
			draw.SimpleText(string.upper(cat.Name), 'PS_CatName', 29, h/2, color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
			draw.RoundedBox(0, 0, 0, w, 1, Color(64, 68, 80))
			draw.RoundedBox(0, 0, h-1, w, 1, Color(64, 68, 80))
		end
		btn.DoClick = function()
			s.CurrentCat = catid[1]
			ILYA:SetShopTab( s )
		end
		i = i + 1
	end
end

function ILYA:SetShopTab( s )
    if s.ShopPage == SHOP_DONATE then
        -- ILYA:CreateShopSideBar( s )
        ulx.showDonateMenu( )
        s.ShopPage = SHOP_STORE
        self.ShopPage = SHOP_STORE
        PS:ToggleMenu()
        return

    end


	ILYA:RemoveElemList( s )
	if s.ShopPage == SHOP_STORE then
		ILYA:CreateShopSideBar( s )
		ILYA:CreateItemList( s )
	elseif s.ShopPage == SHOP_INVENTORY then
		ILYA:CreateShopSideBar( s, true )
		ILYA:CreateItemList( s, true )
	else
		if (PS.Config.AdminCanAccessAdminTab and LocalPlayer():IsSuperAdmin()) or (PS.Config.SuperAdminCanAccessAdminTab and LocalPlayer():IsSuperAdmin()) then
			ILYA:CreateShopSideBar( s, true, true )
			ILYA:CreateAdminMenu( s )
		else
			ILYA:NotifyAccess( s, 'You do not have the proper permissions to access this page or it has been disabled.' )
        end
        --if (LocalPlayer():PS_HasCustomItem()) then
        --    ILYA:CreateUserCustomMenu( s )
        --end
	end
end

local PANEL = {}

function PANEL:Init()
	self:SetSize( math.Clamp( PointshopWidth, 0, ScrW() ), math.Clamp( 768, 0, ScrH() ) )
	self:SetPos((ScrW() / 2) - (self:GetWide() / 2), (ScrH() / 2) - (self:GetTall() / 2))

	self.ShopPage = self.ShopPage or SHOP_STORE
	self.CurrentCat = self.CurrentCat or PS.Config.Categories[1][1]
	self.CurrentAdmin = self.CurrentAdmin or 1
	self.SelectedPlayer = self.SelectedPlayer or 1
	local _CATEGORY = PS.Categories[self.CurrentCat]

	local ava = vgui.Create( 'AvatarImage', self)
	ava:SetPlayer(LocalPlayer(), 64)
	ava:SetSize( 64, 64 )
	ava:SetPos( 23, 50-32 )

	local name = vgui.Create( 'DLabel', self)
	name:SetText(LocalPlayer():Nick())
	name:SetPos( 23+64+6, (50-32)+5 )
	name:SetSize( 150, 20 )
	name:SetFont('PS_PlayerName')
	name:SetTextColor(color_white)

	local point = vgui.Create( 'DLabel', self)
	point:SetText(string.Comma(LocalPlayer():PS_GetPoints()) .. " " .. PS.Config.PointsName or "Points")
	point:SetPos( 23+64+6, (50-12) )
	point:SetSize( 150, 20 )
	point:SetFont('PS_SmallerText')
	point:SetTextColor(Color(255,255,255,50))
    point.Think = function(s)
        s:SetText(string.Comma(LocalPlayer():PS_GetPoints()) .. " " .. PS.Config.PointsName)
    end


    local point = vgui.Create( 'DLabel', self)
	point:SetText(LocalPlayer():SteamID())
	point:SetPos( 23+64+6, (50+3) )
	point:SetSize( 150, 20 )
	point:SetFont('PS_SmallerText')
	point:SetTextColor(Color(255,255,255,50))

	local close = vgui.Create( 'DButton', self)
	close:SetPos( self:GetWide()-70, -5 )
	close:SetSize( 60, 25 )
	close:SetText('')
	close.Paint = function( s, w, h )
		draw.RoundedBox( 0, 0, 0, w, h, Color(57, 61, 72))
		draw.RoundedBox( 0, 1, 1, w-2, h-2, Color(255, 255, 255, 10))
		draw.RoundedBox( 0, 2, 2, w-4, h-4, Color(57, 61, 72))
		draw.SimpleText( 'CLOSE', 'PS_CatName', w/2, h/2, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	close.DoClick = function()
		PS:ToggleMenu()
	end

	-- if you want to use the diamond mask, change PointshopMaskCircle to PointshopMaskDiamond
	local mask = vgui.Create( 'PointshopMaskCircle', self )
	mask:SetSize( 64, 64 )
	mask:SetPos( 23, 50-32 )

	for _, tab in pairs(SHOP_TABS)do
		local btn = vgui.Create( 'DButton', self)
		btn:SetPos( 250 + (((self:GetWide()-250)/3) * (_-1)), 60 )
		btn:SetSize( 250, 40 )
		btn:SetText("")
		btn.OnCursorEntered = function(s) s.s = true end
		btn.OnCursorExited = function(s) s.s = false end
		btn.Paint = function( s, w, h )
			draw.SimpleText(string.upper(tab), 'PS_CatName', w/2, h/2, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
			if _ == self.ShopPage then
				surface.SetFont('PS_CatName')
				local _w, _h = surface.GetTextSize(string.upper(tab))
				draw.RoundedBox(0, (w/2 - _w/2)-1, h/2 + _h/2, _w, 1, Color(0,0,0,80))
			end
		end
		btn.DoClick = function()
			self.ShopPage = _
			ILYA:SetShopTab( self )
		end
	end


	ILYA:SetShopTab( self )
end

function PANEL:Think()
	if self.ShopPage == SHOP_ADMIN and IsValid(self.ClientsList) then
		local lines = self.ClientsList:GetLines()

		for _, ply in pairs(player.GetAll()) do
			local found = false

			for _, line in pairs(lines) do
				if line.Player == ply then
					found = true
				end
			end

			if not found then
				self.ClientsList:AddLine(ply:GetName(), ply:PS_GetPoints(), table.Count(ply:PS_GetItems())).Player = ply
			end
		end

		for i, line in pairs(lines) do
			if IsValid(line.Player) then
				local ply = line.Player

				line:SetValue(1, ply:GetName())
				line:SetValue(2, ply:PS_GetPoints())
				line:SetValue(3, table.Count(ply:PS_GetItems()))
			else
				self.ClientsList:RemoveLine(i)
			end
		end
	end
end

function PANEL:Paint( w, h )

	draw.RoundedBox(0, 0, 0, w, h, Color(245, 244, 249))
	draw.RoundedBox(0, 0, 0, 250, h, Color(57, 61, 72))
	draw.RoundedBox(0, 0, 0, 250, 100, Color(52, 56, 67))
	draw.RoundedBox(0, 0, 99, 250, 1, Color(64, 68, 80))

	-- Purple bar
	draw.RoundedBox(0, 250, 0, w-250, 60, PurpleColorTop)
	draw.RoundedBox(0, 250, 60, w-250, 40, PurpleColorBottom)

    draw.RoundedBox( 0, 250, 100, w-455, h-105, Color(220,220,220,240))

	surface.SetFont('PS_Heading')
	local _w, _h = surface.GetTextSize('TTT-FUN POINTSHOP')
	draw.SimpleText('TTT-FUN POINTSHOP', 'PS_Heading', 250+((w-250)/2-_w), 12, color_white)
	--draw.SimpleText('by _Undefined', 'PS_Heading2', 252+((w-250)/2), 26, color_white)

	local title = ''
	if self.ShopPage == SHOP_STORE then
		title = 'PLAYER SHOP'
	elseif self.ShopPage == SHOP_ADMIN then
		title = 'ADMINISTRATION'
	else
		title = 'PLAYER INVENTORY'
	end

	draw.SimpleText( title, 'PS_CatName', 125, 125, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
end

hook.Add('RenderScreenspaceEffects', 'ps.draw.nicebgblur', function()
	if PS.ShopMenu and PS.ShopMenu:IsVisible() then
		DrawToyTown( 10, ScrH() )
	end
end)

vgui.Register('DPointShopMenu', PANEL)

local POLY = {}

function POLY:Paint( w, h )
	local tbl = {
		[1] = {
			{ x = 0, y = 0},
			{ x = w/2, y = 0},
			{ x = 0, y = h/2}
		},
		[2] = {
			{ x = 0, y = h/2},
			{ x = w/2, y = h},
			{ x = 0, y = h}
		},
		[3] = {
			{ x = w, y = h/2},
			{ x = w, y = h},
			{ x = w/2, y = h}
		},
		[4] = {
			{ x = w/2, y = 0},
			{ x = w, y = 0},
			{ x = w, y = h/2}
		},
	}

	for _, poly in pairs(tbl)do
		surface.SetMaterial(Material('vgui/white'))
		surface.SetDrawColor(Color(52, 56, 67))
		surface.DrawPoly(poly)
	end
end

vgui.Register('PointshopMaskDiamond', POLY)

local _POLY = {}

function _POLY:Paint( w, h )
	for a = 1, 13 do -- For extra aa
		surface.SetMaterial(Material('vgui/white'))
		surface.SetDrawColor(Color(52, 56, 67))
		for i = 0, 360, a do
			local _i = i * math.pi/180;
			surface.DrawTexturedRectRotated(math.cos(_i)*(40)+w/2, math.sin(_i)*(40)+h/2, 15, a*2, -i )
		end
	end
end

vgui.Register('PointshopMaskCircle', _POLY)
