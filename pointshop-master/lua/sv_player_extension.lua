local Player = FindMetaTable('Player')

function Player:PS_PlayerSpawn()
	if not self:PS_CanPerformAction() then return end
	
	-- TTT ( and others ) Fix
	if TEAM_SPECTATOR != nil and self:Team() == TEAM_SPECTATOR then return end
	if TEAM_SPEC != nil and self:Team() == TEAM_SPEC then return end
	
	timer.Simple(1, function()
        if not self.PS_Items then return end
		for item_id, item in pairs(self.PS_Items) do
			local ITEM = PS.Items[item_id]
			if ITEM and item.Equipped then
                                if ITEM.WeaponClass then
                                    self:PS_DropWeaponsSameSlot(ITEM.WeaponClass)
                                    self:PS_HolsterOthers( item_id )
                                end 
				ITEM:OnEquip(self, item.Modifiers)
				self.PS_Equipped[ item_id ] = true
			end
                        if !ITEM then
                            -- print("Something is going on with "..item_id)
                        end
		end
	end)
end

function Player:PS_PlayerDeath()
	for item_id, item in pairs(self.PS_Items) do
                local ITEM = PS.Items[item_id]
		if ITEM and item.Equipped then			
			ITEM:OnHolster(self, item.Modifiers)
		end
                if !ITEM then
                  --  print("Something is going on with "..item_id)
                end
	end
end

function Player:PS_DropWeaponsSameSlot(weapon)

        for k,v in pairs(self:GetWeapons())do
                    wep = weapons.Get(weapon)
               if wep && wep.Kind && v.Kind == wep.Kind then
                    if v.Kind && !self.PS_Dropped_weapon[v.Kind] then
                        self.PS_Dropped_weapon[v.Kind] = true
                        WEPS.DropNotifiedWeapon(self,v, true)
                    else
                        self:StripWeapon( v:GetClass() ) 
                    end
               end
        end

end

function Player:PS_HolsterOthers(item_id)
    local weapon = PS.Items[item_id]
    if not weapon then return  end

        if weapon and weapon.WeaponClass then
            weaponKind = weapons.Get(weapon.WeaponClass).Kind
            if( weaponKind ) then
                for k,v in pairs(self.PS_Items)do
                
                        local ITEM = PS.Items[k]
                        if item_id != k && v.Equipped && ITEM && !ITEM.SingleUse && ITEM.WeaponClass then
                            if weapons.Get(ITEM.WeaponClass).Kind == weaponKind then
                                ITEM:OnHolster(self)
                                self:StripWeapon( ITEM.WeaponClass) 
                                self:PS_HolsterItem(k)
                            end
                        end
                end
            end
        end

end


function Player:PS_PlayerInitialSpawn()
	--self.PS_Points = 0
	--self.PS_Items = {}
	self.PS_Equipped = {}
        self.PS_Dropped_weapon = {}
        self.PS_EquippedThisRound = {}
	
	-- Send stuff
	timer.Simple(1, function()
		if IsValid(self) then
			--self:PS_LoadData()
			self:PS_SendClientsideModels()
		end
	end)
	
	if PS.Config.NotifyOnJoin then
		if PS.Config.ShopKey ~= '' then
			timer.Simple(5, function() -- Give them time to load up
				self:PS_Notify('Press ' .. PS.Config.ShopKey .. ' to open PointShop!')
			end)
		end
		
		if PS.Config.ShopCommand ~= '' then
			timer.Simple(5, function() -- Give them time to load up
				self:PS_Notify('Type ' .. PS.Config.ShopCommand .. ' in console to open PointShop!')
			end)
		end
		
		if PS.Config.ShopChatCommand ~= '' then
			timer.Simple(5, function() -- Give them time to load up
				self:PS_Notify('Type ' .. PS.Config.ShopChatCommand .. ' in chat to open PointShop!')
			end)
		end
		
		 timer.Simple(10, function() -- Give them time to load up
		    if not IsValid(self) then 
		      return 
		    end 
		    self:PS_Notify('You have ' .. self:PS_GetPoints() .. ' points to spend!') 
		  end) 
	end

	if PS.Config.CheckVersion and PS.BuildOutdated and self:IsAdmin() then
		timer.Simple(5, function()
			self:PS_Notify("PointShop is out of date, please tell the server owner!")
		end)
	end
	
	if PS.Config.PointsOverTime then
		timer.Create('PS_PointsOverTime_' .. self:UniqueID(), PS.Config.PointsOverTimeDelay * 60, 0, function()
			self:PS_GivePoints(PS.Config.PointsOverTimeAmount)
		--	self:PS_Notify("You've been given ", PS.Config.PointsOverTimeAmount, " points for playing on the server!")
		end)
	end
end

function Player:PS_PlayerDisconnected()
	self:PS_Save()
	PS.ClientsideModels[self] = nil
	
	if timer.Exists('PS_PointsOverTime_' .. self:UniqueID()) then
		timer.Destroy('PS_PointsOverTime_' .. self:UniqueID())
	end
end

function Player:PS_Save()
	PS:SetPlayerData(self, self.PS_Points, self.PS_Items)
end

function Player:PS_LoadData()
	self.PS_Points = 0
	self.PS_Items = {}
	
	PS:GetPlayerData(self, function(points, items)
		self.PS_Points = points
		self.PS_Items = items
		
		self:PS_SendPoints()
		self:PS_SendItems()
	end)
end

function Player:PS_CanPerformAction()
	local allowed = true
	
	if self.IsSpec and self:IsSpec() then allowed = false end
	if not self:Alive() then allowed = false end
	
	if not allowed then
		self:PS_Notify('You need to be alive to equip this item')
                self:PS_Notify('All normal weapons can not be bought while dead')
                self:PS_Notify("Just bought a permaweapon? Don't worry, it will spawn next round")
	end
	
	return allowed
end

-- points

function Player:PS_GivePoints(points)
    if not IsValid(self) or not  self.PS_Points then return end
    self.PS_Points = self.PS_Points + points
	self:PS_SendPoints()
end

function Player:PS_TakePoints(points)
	self.PS_Points = self.PS_Points - points >= 0 and self.PS_Points - points or 0
	self:PS_SendPoints()
end

function Player:PS_SetPoints(points)
	self.PS_Points = points
	self:PS_SendPoints()
end

function Player:PS_GetPoints()
	return self.PS_Points and self.PS_Points or 0
end

function Player:PS_HasPoints(points)
	return self.PS_Points >= points
end

-- give/take items

function Player:PS_GiveItem(item_id)
	if not PS.Items[item_id] then return false end
	
	self.PS_Items[item_id] = { Modifiers = {}, Equipped = false }
	
	self:PS_SendItems()
	
	return true
end

function Player:PS_TakeItem(item_id)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	
	self.PS_Items[item_id] = nil
	
	self:PS_SendItems()
	
	return true
end

-- buy/sell items

function Player:PS_BuyItem(item_id)
	local ITEM = PS.Items[item_id]
	if not ITEM then return false end

        local cat_name = ITEM.Category
	local CATEGORY = PS:FindCategoryByName(cat_name)
	
	local points = PS.Config.CalculateBuyPrice(self, ITEM)
	
	if not self:PS_HasPoints(points) then return false end 
	
	if not ITEM.buyWhenDeath and not CATEGORY.buyWhenDeath then
	  if not self:PS_CanPerformAction() then return end
	end
	
	if ITEM.AdminOnly and not self:IsAdmin() then
		self:PS_Notify('This item is Admin only!')
		return false
	end
	
	if ITEM.AllowedUserGroups and #ITEM.AllowedUserGroups > 0 then
		if not table.HasValue(ITEM.AllowedUserGroups, self:PS_GetUsergroup()) and not table.HasValue(ITEM.AllowedUserGroups, self.baseGroup)  then
			self:PS_Notify('You\'re not in the right group to buy this item!')
			return false
		end
                if  ITEM.NotAllowedUserGroups and table.HasValue(ITEM.NotAllowedUserGroups, self:PS_GetUsergroup()) and  table.HasValue(ITEM.AllowedUserGroups, self.baseGroup)  then
			self:PS_Notify('You\'re not in the right group to buy this item!')
			return false
		end
	end
	
	
	
	if CATEGORY.AllowedUserGroups and #CATEGORY.AllowedUserGroups > 0 then
		if not table.HasValue(CATEGORY.AllowedUserGroups, self:PS_GetUsergroup()) and not table.HasValue(CATEGORY.AllowedUserGroups, self.baseGroup)  then
			self:PS_Notify('You\'re not in the right group to buy this item!')
			return false
		end

               
	end

         if CATEGORY.NotAllowedUserGroups and  table.HasValue(CATEGORY.NotAllowedUserGroups, self:PS_GetUsergroup()) and  table.HasValue(CATEGORY.NotAllowedUserGroups, self.baseGroup) then
			self:PS_Notify('You\'re not in the right group to buy this item!')
			return false
		end
	
	if CATEGORY.CanPlayerSee then
		if not CATEGORY:CanPlayerSee(self) then
			self:PS_Notify('You\'re not a donator!')
			return false
		end
	end
	
	if ITEM.CanPlayerBuy then -- should exist but we'll check anyway
		local allowed, message
		if ( type(ITEM.CanPlayerBuy) == "function" ) then
			allowed, message = ITEM:CanPlayerBuy(self)
		elseif ( type(ITEM.CanPlayerBuy) == "boolean" ) then
			allowed = ITEM.CanPlayerBuy
		end
		
		if not allowed then
			self:PS_Notify(message or 'You\'re not allowed to buy this item!')
			return false
		end
	end
	
	self:PS_TakePoints(points)
	
	self:PS_Notify('Bought ', ITEM.Name, ' for ', points, ' points.')
	
        if ITEM.WeaponClass then
           self:PS_DropWeaponsSameSlot(ITEM.WeaponClass)
        end

	ITEM:OnBuy(self)
	
	if ITEM.SingleUse then
		self:PS_Notify('Single use item. You\'ll have to buy this item again next time!')
		return
	end

	self:PS_GiveItem(item_id)
	self:PS_EquipItem(item_id)
end

function Player:PS_SellItem(item_id)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	
	local ITEM = PS.Items[item_id]
	
	if ITEM.CanPlayerSell then -- should exist but we'll check anyway
		local allowed, message
		if ( type(ITEM.CanPlayerSell) == "function" ) then
			allowed, message = ITEM:CanPlayerSell(self)
		elseif ( type(ITEM.CanPlayerSell) == "boolean" ) then
			allowed = ITEM.CanPlayerSell
		end
		
		if not allowed then
			self:PS_Notify(message or 'You\'re not allowed to sell this item!')
			return false
		end
	end

	local points = PS.Config.CalculateSellPrice(self, ITEM)
	self:PS_GivePoints(points)
	
	ITEM:OnHolster(self)
	ITEM:OnSell(self)
	
	self:PS_Notify('Sold ', ITEM.Name, ' for ', points, ' points.')
	
	return self:PS_TakeItem(item_id)
end

function Player:PS_HasItem(item_id)
	return self.PS_Items[item_id] or false
end

function Player:PS_HasItemEquipped(item_id)
	if not self:PS_HasItem(item_id) then return false end
	
	return self.PS_Items[item_id].Equipped or false
end

function Player:PS_NumItemsEquippedFromCategory(cat_name)
	local count = 0
	
	for item_id, item in pairs(self.PS_Items) do
		local ITEM = PS.Items[item_id]
                if ITEM then
                    if ITEM.Category == cat_name and item.Equipped then
			count = count + 1
                    end
                end
	end
	
	return count
end

function checkEquippedThisRound( ply, item )

    if ply.PS_EquippedThisRound[item] then
        ply:PS_Notify("You already equipped this item this round")
        return true
    end
    return false
end


-- equip/hoster items

function Player:PS_EquipItem(item_id)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	if not self:PS_CanPerformAction() then return false end
        if checkEquippedThisRound( self, item_id ) then return end

	local ITEM = PS.Items[item_id]
	
	local cat_name = ITEM.Category
	local CATEGORY = PS:FindCategoryByName(cat_name)
	
	if CATEGORY and CATEGORY.AllowedEquipped > -1 then
		if self:PS_NumItemsEquippedFromCategory(cat_name) + 1 > CATEGORY.AllowedEquipped then
			self:PS_Notify('Only ' .. CATEGORY.AllowedEquipped .. ' item' .. (CATEGORY.AllowedEquipped == 1 and '' or 's') .. ' can be equipped from this category!')
			return false
		end
	end
	
	self.PS_Items[item_id].Equipped = true
        if GAMEMODE.round_state != ROUND_PREP && !ITEM.CanBuyMultipleTimes then 
            self.PS_EquippedThisRound[item_id] = true
        end
        if ITEM.WeaponClass then
            self:PS_DropWeaponsSameSlot(ITEM.WeaponClass)
            self:PS_HolsterOthers( item_id )
        end
	ITEM:OnEquip(self, self.PS_Items[item_id].Modifiers)
	
	self:PS_Notify('Equipped ', ITEM.Name, '.')
	
	self:PS_SendItems()
end

function Player:PS_HolsterItem(item_id)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	if not self:PS_CanPerformAction() then return false end
	
	self.PS_Items[item_id].Equipped = false
	
	local ITEM = PS.Items[item_id]
	ITEM:OnHolster(self)
	
	self:PS_Notify('Holstered ', ITEM.Name, '.')
	
	self:PS_SendItems()
end

-- modify items

function Player:PS_ModifyItem(item_id, modifications)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	if not type(modifications) == "table" then return false end
	if not self:PS_CanPerformAction() then return false end
	
	local ITEM = PS.Items[item_id]
	
	for key, value in pairs(modifications) do
		self.PS_Items[item_id].Modifiers[key] = value
	end
	
	ITEM:OnModify(self, self.PS_Items[item_id].Modifiers)
	
	self:PS_SendItems()
end

-- clientside Models

function Player:PS_AddClientsideModel(item_id)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	
	net.Start('PS_AddClientsideModel')
		net.WriteEntity(self)
		net.WriteString(item_id)
	net.Broadcast()
	
	if not PS.ClientsideModels[self] then PS.ClientsideModels[self] = {} end
	
	PS.ClientsideModels[self][item_id] = item_id
end

function Player:PS_RemoveClientsideModel(item_id)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	if not PS.ClientsideModels[self] or not PS.ClientsideModels[self][item_id] then return false end
	
	net.Start('PS_RemoveClientsideModel')
		net.WriteEntity(self)
		net.WriteString(item_id)
	net.Broadcast()
	
	PS.ClientsideModels[self][item_id] = nil
end

-- menu stuff

function Player:PS_ToggleMenu(show)
	net.Start('PS_ToggleMenu')
	net.Send(self)
end

-- send stuff

function Player:PS_SendPoints(wat)
	self:PS_Save()
	
	net.Start('PS_Points')
		net.WriteEntity(self)
		net.WriteInt(self.PS_Points, 32)
	net.Broadcast()
end

function Player:PS_SendItems(wat)
	self:PS_Save()
	
	net.Start('PS_Items')
		net.WriteEntity(self)
		net.WriteTable(self.PS_Items)
	net.Send(self)
end

function Player:PS_SendClientsideModels()	
	net.Start('PS_SendClientsideModels')
		net.WriteTable(PS.ClientsideModels)
	net.Send(self)
end

-- notifications

function Player:PS_Notify(...) 
   if not IsValid(self) then 
     return 
   end 
   local str = table.concat({...}, '') 
   self:SendLua('notification.AddLegacy("' .. str .. '", NOTIFY_GENERIC, 5)') 
end 
