PS.Config = {}

-- Edit below

PS.Config.DataProvider = 'json'

PS.Config.Branch = "https://raw.github.com/adamdburton/pointshop/master/" -- Master is most stable, used for version checking.
PS.Config.CheckVersion = false -- Do you want to be notified when a new version of Pointshop is avaliable?

PS.Config.ShopKey = "" -- F1, F2, F3 or F4, or blank to disable
PS.Config.ShopCommand = '' -- Console command to open the shop, set to blank to disable
PS.Config.ShopChatCommand = '' -- Chat command to open the shop, set to blank to disable

PS.Config.NotifyOnJoin = true -- Should players be notified about opening the shop when they spawn?

PS.Config.PointsOverTime = true -- Should players be given points over time?
PS.Config.PointsOverTimeDelay = 1 -- If so, how many minutes apart?
PS.Config.PointsOverTimeAmount = 10 -- And if so, how many points to give after the time?

PS.Config.AdminCanAccessAdminTab = false -- Can Admins access the Admin tab?
PS.Config.SuperAdminCanAccessAdminTab = true -- Can SuperAdmins access the Admin tab?

PS.Config.CanPlayersGivePoints = true -- Can players give points away to other players?
PS.Config.DisplayPreviewInMenu = true -- Can players see the preview of their items in the menu?
PS.Config.PointsName = "Points"
PS.Config.SortItemsBy = "Name"

-- Edit below if you know what you're doing

PS.Config.Categories = {
    [1] = { "accessories", "headshatsmasks",  "followers", },
    [2] = { "playermodels",  "playermodels-member", "playermodels-donator", "playermodels_donatorplus", "playermodel_superdonator",},
    [3] = { "powerups" },
    [4] = { "snacks" },
    [5] = { "teleports" },
    [6] = { "trails", "trails-member", "trails_donator", "trails_donatorplus",  "trails_superdonator" },
    [7] = { "followers" },

}


PS.Config.CalculateBuyPrice = function(ply, item)
	-- You can do different calculations here to return how much an item should cost to buy.
	-- There are a few examples below, uncomment them to use them.
	
	-- Everything half price for admins:
	 if ply:IsAdmin() then return math.Round(item.Price * 0.5) end
	
	-- 25% off for the 'donators' group
	 if ply:IsUserGroup('donator') or ply:IsUserGroup('donatorplus') or ply:IsUserGroup('superdonator') then return math.Round(item.Price * 0.5) end
	
	return item.Price
end

PS.Config.CalculateSellPrice = function(ply, item)
       local priceItem = PS.Config.CalculateBuyPrice( ply, item)
	return math.Round(priceItem * 0.80) -- 75% or 3/4 (rounded) of the original item price
end