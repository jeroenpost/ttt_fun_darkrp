ITEM.Name = 'GreenBlack'
ITEM.Price = 12500
ITEM.Material = 'materials/vgui/entities/green_black_rp'
ITEM.WeaponClass = 'green_black_rp'
ITEM.SingleUse = false


function ITEM:OnEquip(ply)

	ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end