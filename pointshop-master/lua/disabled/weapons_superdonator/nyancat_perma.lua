ITEM.Name = 'NyanGun'
ITEM.Price = 12500
ITEM.Material = 'nyan/selection.png'
ITEM.WeaponClass = 'weapon_nyangun'
ITEM.SingleUse = false


function ITEM:OnEquip(ply)

	ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end