ITEM.Name = 'Perma Physics Gun'
ITEM.Price = 20000
ITEM.Model = 'models/weapons/w_physics.mdl'
ITEM.Skin = 2
ITEM.WeaponClass = 'weapon_physgun'
ITEM.SingleUse = false

function ITEM:CanPlayerBuy(ply)

    return true

end


function ITEM:OnEquip(ply)
    if ply:Team() ~= TEAM_HUMAN then
        return
    end
	ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end