CATEGORY.Name = 'Super Donator Weapons'
CATEGORY.Desc = 'Weapons for Super Donators. '
CATEGORY.Icon = 'key'
CATEGORY.NotAllowedUserGroups = { "user","member", "demotedmember","","donator","donator_plus","donatorplus", "jr_mod", "jr_admin" }
CATEGORY.Order = 3
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "weapons"
CATEGORY.buyWhenDeath = true