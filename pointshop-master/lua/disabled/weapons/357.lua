ITEM.Name = '357 Magnum'
ITEM.Price = 200
ITEM.Model = 'models/weapons/w_357.mdl'
ITEM.WeaponClass = 'weapon_357'
ITEM.SingleUse = true

function ITEM:CanPlayerBuy(ply)
    if ply:Team() ~= TEAM_HUMAN then
          ply:PrintMessage( HUD_PRINTCENTER, "You cannot buy weapons as a Prop" )
        return false
    end
    return true

end

function ITEM:OnBuy(ply)
	ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end