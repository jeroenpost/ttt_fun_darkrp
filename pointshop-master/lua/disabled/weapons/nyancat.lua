ITEM.Name = 'NyanGun'
ITEM.Price = 200
ITEM.Material = 'nyan/selection.png'
ITEM.WeaponClass = 'weapon_nyangun'
ITEM.SingleUse = true

function ITEM:CanPlayerBuy(ply)

    return true

end

function ITEM:OnBuy(ply)
	ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end