if( SERVER ) then
	AddCSLuaFile( "shared.lua" )
end

if( CLIENT ) then
	SWEP.PrintName = "Bronze Pickaxe"
	SWEP.Slot = 3
	SWEP.SlotPos = 3
	SWEP.DrawAmmo = true
	SWEP.DrawCrosshair = false
end

SWEP.Chance			= 10

SWEP.Author			= "Bloodwave"
SWEP.Instructions	= "Left click while looking at a mineral ore will mine it"
SWEP.Contact		= ""
SWEP.Purpose		= ""

SWEP.ViewModelFOV	= 60
SWEP.ViewModelFlip	= false

SWEP.Spawnable		= true
SWEP.AdminSpawnable	= true

SWEP.NextStrike		= 0

SWEP.ViewModel      = "models/weapons/v_stone_pickaxe.mdl"
SWEP.WorldModel   	= "models/weapons/w_stone_pickaxe.mdl"
--
SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1	
SWEP.Primary.Automatic   	= true
SWEP.Primary.Ammo         	= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic   	= true
SWEP.Secondary.Ammo         = "none"

function SWEP:Initialize()
	self:SetHoldType( "melee" )
end

function SWEP:Deploy()
    self.NextCheck = 0
    self.NextRemove = CurTime() + 120
end

function SWEP:randomore()
	local all_rares = {}
	
	for k,v in pairs( MINE.Ores ) do
		if ( v.Chance ) then
			all_rares[ #all_rares + 1 ] = { Name = k, Chance = v.Chance }
		end
	end
	
	table.SortByMember( all_rares, "Chance", function(a, b) return a > b end )
	
	for k,v in ipairs( all_rares ) do
		if ( math.Rand( 0, 100 ) <= v.Chance ) then
			self.Owner:EmitSound("buttons/lever8.wav")
			return v.Name
		end
	end
	
	return MINE.DefaultOre
end

function SWEP:PrimaryAttack()
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )

	self:SetNextPrimaryFire( CurTime() + 1 )
	
	if ( CLIENT ) then return end

    local tr = self.Owner:MineTraceLine( 70 )
	local ent = tr.Entity

	if ( !IsValid( ent ) or ent:GetClass() ~= "mineral_ore" or ent.Decay <= 0 ) then
		self.Owner:EmitSound( "weapons/iceaxe/iceaxe_swing1.wav" )
		return
	end
	
	if ( math.Rand( 0, 100 ) > self.Chance ) then
		self.Owner:EmitSound( "physics/concrete/rock_impact_hard"..math.random(1,6)..".wav" )
		return
	end
	
	if ( ent.Decay ) then
		--ent.Decay = ent.Decay - 1
		
		if ( ent.Decay <= 0 ) then
			local pos = ent:GetPos()
			local mdl = ent:GetModel()
			
			ent:MineFadeout( function()
				timer.Simple( MINE.MineralOreDecayWait, function()
					local ent = ents.Create( "mineral_ore" )
					ent:SetModel( mdl )
					ent:SetPos( pos )
					ent:Spawn()
				end )
			end )
		end
	end

	MINE:HandleOre( self:randomore(), self.Owner, tr )
	self.Owner:EmitSound( "physics/concrete/rock_impact_hard"..math.random(1,6)..".wav" )
end

function SWEP:SecondaryAttack()

end

SWEP.LastMessage = 0
SWEP.NextCheck = 0
function SWEP:Think()
    if IsValid(self.Owner)  then
        if not self.Owner.lastLetgo then self.Owner.lastLetgo = CurTime() end
        if self.Owner:KeyDown(IN_ATTACK) then
             if (self.Owner.lastLetgo + 60) < CurTime() and self.LastMessage < CurTime() then
                    self.Owner:PrintMessage( HUD_PRINTCENTER, "Your pickaxe is about to break. Put it down for 3 seconds or it will snap." )
                    self.NextRemove = CurTime() + 10
                    self.LastMessage = CurTime() + 15
                end
             if (self.Owner.lastLetgo + 60) < CurTime() and  self.NextRemove < CurTime() then
                 self:EmitSound("physics/wood/wood_plank_break4.wav")
                 self:Remove()
             end
        elseif  self.NextCheck < CurTime() then
            self.Owner.lastLetgo = CurTime()
            self.NextCheck = CurTime() + 2
            self.NextRemove = CurTime() + 120
        end
    end
end