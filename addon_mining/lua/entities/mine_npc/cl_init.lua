include("shared.lua")

ENT.RenderGroup = RENDERGROUP_BOTH
	
local function CraftSection( f, dl )
	for k, v in pairs( MINE.Craftable ) do
		local p = vgui.Create( "DPanel" )
		p:SetTall( 64 )

		function p:Paint( w, h )
			surface.SetDrawColor( 20, 20, 20, 240 )
			surface.DrawRect( 0, 0, w, h )
			
			surface.SetDrawColor( 50, 150, 50, 150 )
			surface.DrawRect( w * .83, 0, w * .17, h )
			
			surface.SetDrawColor( 200, 200, 200, 200 )
			surface.DrawOutlinedRect( 0, 0, w, h )
		end	
		
		local m2 = vgui.Create( "DModelPanel", p )
		m2:SetSize( 64, 64 )

		m2:SetCamPos( Vector( 0, 16, 0 ) )
		m2:SetLookAt( Vector( 0, 0, 0 ) )
		
		m2:SetModel( v.Model, 64 )
	
		if ( v.Color ) then
			m2:SetColor( v.Color )
		end
	
		local nlbl = vgui.Create( "DLabel", p )
		nlbl:SetText( k )
		nlbl:SetFont( "Mine18" )
		nlbl:SetPos( 75, 10 )
		nlbl:SizeToContents()
		
		local dlbl = vgui.Create( "DLabel", p )
		dlbl:SetText( v.Desc )
		dlbl:SetPos( 80, 25 )
		dlbl:SizeToContents()
		
		local matl = vgui.Create( "DPanelList", p )
		matl:EnableVerticalScrollbar( true )
		
		for k,v in pairs( v.Materials ) do
			local mat = vgui.Create ( "DLabel", p )
			mat:SetText( k.." - "..v.."x" )
			mat:SetTextColor( color_white )
			mat:SizeToContents()
			
			matl:AddItem( mat )
		end
		
		local c = vgui.Create( "DButton", p )
		c:SetText( "Craft" )
		
		function c:DoClick()
			net.Start("MineServer")
				net.WriteString( "craft" )
				net.WriteEntity( f.npc )
				net.WriteString( k )
			net.SendToServer()
		end
		
		function p:PerformLayout()
			matl:SetSize( ( self:GetWide() * .17 ), self:GetTall() - 10 )
			matl:SetPos( ( self:GetWide() * .83 ) + 5, 10 )
			
			c:SetPos( ( self:GetWide() * .75 ) - ( ( c:GetWide() / 2 ) ), 4 )
		end
	
		dl:AddItem( p )
	end
	
	f:SetTall( math.Min( ( #dl:GetItems() * 64 ) + 100, 500 ) )
end
	
local function SellSection( f, dl )
	for k, v in pairs( MINE.Ores ) do
		if ( !v.Price ) then continue end
		
		local p = vgui.Create( "DPanel" )
		p:SetTall( 64 )

		function p:Paint( w, h )
			surface.SetDrawColor( 20, 20, 20, 240 )
			surface.DrawRect( 0, 0, w, h )
			
			surface.SetDrawColor( 50, 150, 50, 150 )
			surface.DrawRect( w * .83, 0, w * .17, h )
			
			surface.SetDrawColor( 200, 200, 200, 200 )
			surface.DrawOutlinedRect( 0, 0, w, h )
		end	
		
		local m2 = vgui.Create( "DModelPanel", p )
		m2:SetSize( 64, 64 )

		m2:SetCamPos( Vector( 0, 16, 0 ) )
		m2:SetLookAt( Vector( 0, 0, 0 ) )
		
		m2:SetModel( v.Model, 64 )
			
		if ( v.Color ) then
			m2:SetColor( v.Color )
		end
		
		if ( v.Material ) then
			m2.Entity:SetMaterial( v.Material )
		end	
	
		local nlbl = vgui.Create( "DLabel", p )
		nlbl:SetText( k )
		nlbl:SetFont( "Mine18" )
		nlbl:SetPos( 75, 10 )
		nlbl:SizeToContents()
		
		local dlbl = vgui.Create( "DLabel", p )
		dlbl:SetText( v.Desc )
		dlbl:SetPos( 80, 25 )
		dlbl:SizeToContents()
		
		local plbl = vgui.Create( "DLabel", p )
		plbl:SetText( "$"..v.Price )
		plbl:SetFont( "Mine24" )
		plbl:SizeToContents()
		
		local sell = vgui.Create( "DButton", p )
		sell:SetText( "Sell" )
		
		function sell:DoClick()
			net.Start("MineServer")
				net.WriteString( "sell" )
				net.WriteEntity( f.npc )
				net.WriteString( k )
			net.SendToServer()
		end
		
		function p:PerformLayout()
			sell:SetPos( self:GetWide() - sell:GetWide() - 15, self:GetTall() - sell:GetTall() - 10 )
			plbl:SetPos( self:GetWide() - plbl:GetWide() - 15, self:GetTall() - plbl:GetTall() - 35 )
		end
	
		dl:AddItem( p )
	end
	
	f:SetTall( math.Min( ( #dl:GetItems() * 64 ) + 100, 620 ) )
end
	
local function OpenNPCMenu( npc, dir )
	local f = vgui.Create( "MineFrame" )
	f:SetSize( ScrW() * .500, ScrH() * .600 )
	f:Center()
	f:SetTitle( "Mineral Buyer" )
	f:MakePopup()
	
	f.npc = npc
	
	local dl = vgui.Create( "DPanelList", f )
	dl:SetSize( f:GetWide() - 15, f:GetTall() - 45 )
	dl:SetPos( 5, 40 )
	dl:SetSpacing( 8 )
	dl:EnableVerticalScrollbar( true )
	
	if ( dir == "sell" ) then
		SellSection( f, dl )
	elseif ( dir == "craft" ) then
		CraftSection( f, dl )
	end
end
	
	
local Buttons = {
	["I'd like to sell"] = function( f )
		OpenNPCMenu( f.npc, "sell" )
	end,
	["I'd like to craft"] = function( f )
		OpenNPCMenu( f.npc, "craft" )
	end,
	["Nevermind"] = function( f )
		
	end
}	


local function NPCQuestioner( npc )
	local f = vgui.Create( "MineFrame" )
	f:SetSize( ScrW() * .500, ScrH() * .250 )
	f:Center()
	f:SetTitle( "Mineral Buyer" )
	f:MakePopup()
	
	f.npc = npc
	
	local i = vgui.Create( "DPanel", f )
	i:SetPos( 0, 35 )
	i:SetSize( f:GetWide(), 64 )
	
	function i:Paint( w,h )
		surface.SetDrawColor( 200, 200, 200, 100 )
		surface.SetTexture( surface.GetTextureID("gui/center_gradient") )
		surface.DrawTexturedRect( 0, 0, w, h )
	
		surface.SetDrawColor( 50, 50, 50, 150 )
		surface.DrawRect( 0, 0, w, h )
	end
	
	local m2 = vgui.Create( "SpawnIcon", i )
	m2:SetSize( 64, 64 )
	m2:SetModel( npc:GetModel(), 64 )
	m2:SetToolTip()
	
	local desc = vgui.Create( "DLabel", i )
	desc:SetText( "Welcome to my little shop, what are you interested in today?" )
	desc:SetFont( "Mine18" )
	desc:SetTextColor( Color( 240, 240, 240, 240 ) )
	desc:SizeToContents()
	desc:Center()
	
	local dl = vgui.Create( "DPanelList", f )
	dl:SetSize( f:GetWide() - 4, 77 )
	dl:SetPos( 2, f:GetTall() - dl:GetTall() - 5 )
	dl:EnableVerticalScrollbar( true )
	dl:SetSpacing( 2 )
	dl:SetPadding( 2 )
	
	for name, func in pairs( Buttons ) do
		local b = vgui.Create("DButton" )
		b:SetText( name )
		
		function b:DoClick( )
			func( f )
			f:Close()
		end	
		
		dl:AddItem( b )
	end
end
	
net.Receive("Mine_NPCMenu", function()
	local npc = net.ReadEntity()
	
	NPCQuestioner( npc )
end)

    hook.Add("PostDrawOpaqueRenderables", "Ore Dealter", function()
        for _, ent in pairs (ents.FindByClass("mine_npc")) do
            if ent:GetPos():Distance(LocalPlayer():GetPos()) < 500 then
                local Ang = ent:GetAngles()

                Ang:RotateAroundAxis( Ang:Forward(), 90)
                Ang:RotateAroundAxis( Ang:Right(), -90)

                cam.Start3D2D(ent:GetPos()+ent:GetUp()*90, Ang, 0.35)
                draw.SimpleTextOutlined( 'Ore Dealer', "HUDNumber5", 0, 0, Color( 255, 255, 60, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP, 1, Color(0, 0, 0, 255))
                cam.End3D2D()
            end
        end
    end)
