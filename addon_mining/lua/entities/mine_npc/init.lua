AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include( "shared.lua" )

function ENT:Initialize()
	self:SetHullType( HULL_HUMAN )
	self:SetHullSizeNormal()

	self:SetSolid( SOLID_BBOX ) 
	self:SetNPCState( NPC_STATE_SCRIPT )
	
	self:CapabilitiesAdd( CAP_ANIMATEDFACE + CAP_TURN_HEAD )

	self:SetMaxYawSpeed( 90 )
	
	self:SetUseType(SIMPLE_USE)
    self:SetModel("models/player/blockdude.mdl")
	
	self:DropToFloor()
end 

function ENT:StartTouch( ent )
	if ( ent.Ore and ent.Price and IsValid( ent._mineowner ) ) then
		self:ProcessSale( ent._mineowner, ent.Ore )
		ent:Remove()
	end
end

timer.Simple( 1, function()
	hook.Add("GravGunOnPickedUp", "Mine:DddSetupOwner", function( ply, ent )
		ent._mineowner = ply
	end)
end)

util.AddNetworkString("Mine_NPCMenu")
util.AddNetworkString( "MineServer" )

function ENT:AcceptInput( name, activator, ply )
	if ( !IsValid( ply ) or !ply:IsPlayer() or name != "Use" ) then return end

	net.Start( "Mine_NPCMenu" )
		net.WriteEntity( self )
	net.Send( ply )
end

function ENT:ProcessSale( ply, ore )
	MINE:ProcessMoney( ply, MINE.Ores[ ore ].Price )
	ply:ChatPrint( "You've sold a "..ore.." for $"..MINE.Ores[ ore ].Price.."!" )
	
	if ( ( self.lthanks or 0 ) < CurTime() ) then
		self:EmitSound( "vo/k_lab/ba_geethanks.wav" )
		self.lthanks = CurTime() + 2
	end
end

net.Receive( "MineServer", function( length, ply )
	local prtcl = net.ReadString()
	local npc = net.ReadEntity()
	
	if ( !IsValid( npc ) or npc:GetPos():Distance( ply:GetPos() ) > 200 ) then return end
	
	if ( prtcl == "sell" ) then
		local ore = net.ReadString()
		local d = MINE.Ores[ ore ]
		
		if ( !d or !d.Price ) then
			ply:ChatPrint( "You can't sell a "..ore.."!" )
			return
		end
		
		if ( MINE:RemoveItem( ply, ore, 1 ) ) then
			npc:ProcessSale( ply, ore )
		else
			ply:ChatPrint( "You don't have a "..ore.." in your inventory!" )
		end
	elseif ( prtcl == "craft" ) then
		local ent = net.ReadString()
		local d = MINE.Craftable[ ent ] 
		
		if ( !d ) then
			ply:ChatPrint( "You can't craft a "..ent.."!" )
			return
		end
		
		for k,v in pairs( d.Materials ) do
			if ( MINE:GetItemCount( ply, k ) < v ) then
				ply:ChatPrint("You don't have enough "..k.."!")
				return
			end
		end
		
		for k,v in pairs( d.Materials ) do
			if ( !MINE:RemoveItem( ply, k, v ) ) then
				ply:ChatPrint( "Failed to collect all resources, bypassed count check." )
				return
			end
		end
		
		MINE:AddItem( ply, ent )
	end
end)