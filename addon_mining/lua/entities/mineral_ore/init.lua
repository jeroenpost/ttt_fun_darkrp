AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include( "shared.lua" )

function ENT:Initialize()
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetUseType( SIMPLE_USE )
    self:SetModel("models/props_wasteland/rockcliff_cluster03a.mdl")

    local phys = self:GetPhysicsObject()
	
	if IsValid( phys ) then
		phys:EnableMotion( false )
	end
	
	if ( MINE.MineralOreDecay ) then
		self.Decay = MINE.MineralOreDecayMax
	else
        self.Decay = 60000000
    end
	self:DropToFloor()
end 

function ENT:Use( ply )
	ply:ChatPrint( MINE.MineralOreInfo )
end