-- DEVELOPED FULLY BY BLOODWAVE - http:--steamcommunity.com/id/bloodwave
-- YOU'RE NOT ENTITLED TO DISTRUBUTE THIS ADDON, THIS IS PRIVATE
-- These are the functions this addon uses
-- You typically won't touch this unless you're a scripter
-- If you're lost, the friendly config file is @ mine/config.lua

MINE = {}
MINE.NPC = {}
MINE.MineralOreSpawn = {}
MINE.Ores = {}
MINE.Craftable = {}

function MINE:AddSpawn( map, pos, model, ang )
	MINE.MineralOreSpawn[ map ] = MINE.MineralOreSpawn[ map ] or {}
	
	table.insert( MINE.MineralOreSpawn[ map ], {
		pos = pos,
		model = model,
		ang = ang
	} )
end

function MINE:AddCraft( name, tab )
	MINE.Craftable[ name ] = tab
end

function MINE:AddOre( name, tab )
	MINE.Ores[ name ] = tab
end

function MINE:AddNPC( map, pos, model, ang )
	MINE.NPC[ map ] = MINE.NPC[ map ] or {}
	
	table.insert( MINE.NPC[ map ], {
		pos = pos,
		model = model,
		ang = ang
	} )
end

function MINE:CreatePickaxe( name, class, chance )
	local SWEP = {}
	
	SWEP.Base = "bronze_pickaxe"
	SWEP.Chance = chance
	SWEP.PrintName = name
    SWEP.CanDrop = true
	
	weapons.Register( SWEP, class, true )
end

function MINE:CreateOre( name, data )
	local mdl = ( type( data.Model ) == "table" and table.Random( data.Model ) ) or data.Model

	local ENT = {}
	
	ENT.Base = "base_gmodentity"
	ENT.Ore = name
	ENT.Price = data.Price
	ENT.RenderGroup = RENDERGROUP_TRANSLUCENT
	
	if ( SERVER ) then
		function ENT:Initialize()
			self:SetModel( mdl )
			self:PhysicsInit( SOLID_VPHYSICS )
			self:SetMoveType( MOVETYPE_VPHYSICS )
			self:SetSolid( SOLID_VPHYSICS )
			self:SetUseType( SIMPLE_USE )
			self:SetCollisionGroup( COLLISION_GROUP_PLAYER )
			
			if ( data.Color ) then
				self:SetColor( data.Color )
			end
			
			if ( data.Material ) then
				self:SetMaterial( data.Material )
			end
			
			if ( MINE.OreDecay ) then
				self.Decay = CurTime() + MINE.OreDecayWait
			end
			
			self:GetPhysicsObject():Wake()
		end
		
		function ENT:Think()
			if ( self.MineFading and !MINE:ShouldFade( self ) ) then
				self:MineFadeoutKill()
			end
		
			if ( !self.Decay ) then return end
			
			if ( self.Decay < CurTime() and MINE:ShouldFade( self ) ) then
				self:MineFadeout()
				self.Decay = nil
			end
		end
		
		function ENT:Use( ply )
			ply:ChatPrint( string.format( MINE.OreInfo, name ) )
		end
	end
	
	scripted_ents.Register( ENT, "ore_"..string.lower( name ), true )
end


if ( SERVER ) then
	include( "mine/sv_funcs.lua" )
	
	AddCSLuaFile( "mine/config.lua" )
	include( "mine/config.lua" )
else
	include( "mine/config.lua" )
end

if ( SERVER ) then
	include( "mine/sh_init.lua" )
	AddCSLuaFile( "mine/sh_init.lua" )
else
	include( "mine/sh_init.lua" )
end

if ( SERVER ) then
	resource.AddFile("models/weapons/v_stone_pickaxe.mdl")
	resource.AddFile("models/weapons/w_stone_pickaxe.mdl")
	
	resource.AddFile("materials/models/weapons/pickaxe/stone/stone.vtf")
	resource.AddFile("materials/models/weapons/pickaxe/stone/stone_n.vtf")
	resource.AddFile("materials/models/weapons/pickaxe/stone/pickaxe01.vmt")
end