surface.CreateFont("Mine18", {
	size = 18,
	weight = 500,
	antialias = true,
	shadow = false,
	font = "Trebuchet MS"
})


surface.CreateFont("Mine24", {
	size = 24,
	weight = 500,
	antialias = true,
	shadow = false,
	font = "Trebuchet MS"
})
	
local PANEL = {}

function PANEL:Init()
	self.btnMaxim:Remove()
	self.btnMinim:Remove()
	self.btnClose:Remove()
	
	self.btnClose = vgui.Create("DImageButton", self)
	self.btnClose:SetImage("icon16/cancel.png")
	
	self.btnClose.DoClick = function()
		self:Close()
	end
	
	self.lblTitle:SetFont( "Mine18" )
end

function PANEL:PerformLayout()
    self.btnClose:SetPos( self:GetWide() - 22, 4 )
	self.btnClose:SetSize( 16, 16 )
	
	self.lblTitle:SizeToContents()
    self.lblTitle:SetPos( ( self:GetWide() / 2 ) - ( self.lblTitle:GetWide() / 2 ), 5 )
end

function PANEL:Paint( w, h )
	draw.RoundedBox( 2, 0, 0, w, h, Color( 60, 60, 60, 155 ) )
	
	surface.SetDrawColor( 150, 150, 150, 230 )
	surface.DrawRect( 0, 0, w, 25 )
	
	surface.SetDrawColor( color_black )
	surface.DrawOutlinedRect( 0, 0, w, h )
	surface.DrawLine( 0, 25, w, 25 )
end

vgui.Register( "MineFrame", PANEL, "DFrame" )
