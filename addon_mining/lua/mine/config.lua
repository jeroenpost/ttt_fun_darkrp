-- DEVELOPED FULLY BY BLOODWAVE - http:--steamcommunity.com/id/bloodwave
-- YOU'RE NOT ENTITLED TO DISTRUBUTE THIS ADDON, THIS IS PRIVATE
-- THIS CURRENTLY WORKS FOR BASEWARS AND DARKRP, IF YOU HAVE A CUSTOM GAMEMODE CONTACT ME ON STEAM AND I'LL LOOK INTO IT.

-- Desc: this is what will display when they press E on the mineral ore
MINE.MineralOreInfo = "This is a mineral ore that can be savaged with a pickaxe." 

-- if set to false, the ores will never remove.
-- Desc: do you want the mineral ore to decay after being mined so much and respawn?
MINE.MineralOreDecay = false

-- Desc: how many times does it need to be hit successfully with a pickaxe to fade away?
MINE.MineralOreDecayMax = 6000000

-- Desc: how long will it take the ore mineral to respawn back? ( seconds )
MINE.MineralOreDecayWait = 1

-- The default ore, if we don't get anything rare :(
MINE.DefaultOre = "Coal"

-- HIGHLY RECOMMENED to keep this at true, or your server may suffer lag
-- Desc: Have ores remove after a certain amount of seconds?
MINE.OreDecay = true

-- Desc: How long does it take to remove? ( in seconds )
MINE.OreDecayWait = 60

-- Desc: this is what will display when you press E on the actual ore
MINE.OreInfo = "This is  %s."
-- Desc: The information of ores
-- Chances are based on a percentage of 100, if the chance is 80, there is a 80%
-- If you want a ore that can't be sold, but can be crafted with, just remove the Price parameter.
MINE:AddOre( "Coal", {
	Model = "models/props_junk/rock001a.mdl",
	Desc = "Your basic ore",
	Price = 100,
    Chance = 50
} )

MINE:AddOre( "Iron", {
    Model = "models/props_junk/rock001a.mdl",
    Desc = "Useful metal",
    Price = 200,
    Chance = 30
} )

MINE:AddOre( "Gold", {
    Model = "models/props_junk/rock001a.mdl",
    Desc = "Gold stone",
    Color = Color( 255, 215, 0 ),
    Material = "models/player/shared/gold_player",
    Price = 400,
    Chance = 5
} )

MINE:AddOre( "Ruby", {
	Model = "models/props_junk/rock001a.mdl",
	Desc = "A shiny red beauty of a gem",
	Color = Color( 255, 20, 20 ),
	Material = "models/shiny",
	Price = 700, -- price it sells for in the npc, remove this line if you dont want it to be sellable
	Chance = 3 -- percentage its based on the chance to mine one
} )

MINE:AddOre( "Diamond", {
	Model = "models/props_junk/rock001a.mdl",
	Desc = "One of the rareist gems known to man",
	Material = "models/shiny",
	Color = color_white,
	Price = 2200, -- price it sells for in the npc, remove this line if you dont want it to be sellable
	Chance = 1 -- percentage its based on the chance to mine one
} )

MINE:AddOre( "Emerald", {
	Model = "models/props_junk/rock001a.mdl",
	Desc = "A shiny green beauty of a gem",
	Color = Color( 20, 255, 20 ),
	Material = "models/shiny",
	Price = 1500, -- price it sells for in the npc, remove this line if you dont want it to be sellable
	Chance = 2 -- percentage its based on the chance to mine one
} )

MINE:AddOre( "Sapphire", {
	Model = "models/props_junk/rock001a.mdl",
	Desc = "A shiny blue beauty of a gem",
	Color = Color( 0, 20, 255 ),
	Material = "models/shiny",
	Price = 1750, -- price it sells for in the npc, remove this line if you dont want it to be sellable
	Chance = 1.5 -- percentage its based on the chance to mine one
} )


-- PICKAXES
MINE:AddCraft( "Iron Pickaxe", {
    Model = "models/weapons/w_stone_pickaxe.mdl", -- model
    Color = Color( 50, 50, 50, 255 ), -- color
    Materials = { ["Coal"] = 2, ["Iron"] = 6 }, -- materials needed to craft this pickaxe!
    Desc = "This iron pickaxe has a 20% of successfully mining ore", -- desc
    PreSpawn = function( ply, data )
        ply:Give( "iron_pickaxe" )
    end
} )

MINE:AddCraft( "Gold Pickaxe", {
	Model = "models/weapons/w_stone_pickaxe.mdl", -- model
	Color = Color( 255, 215, 0, 255 ), -- color
	Materials = {  ["Coal"] = 2,["Iron"] = 2, ["Gold"] = 6 }, -- materials needed to craft this pickaxe!
	Desc = "This gold pickaxe has a 25% of successfully mining ore", -- desc
	PreSpawn = function( ply, data )
		ply:Give( "gold_pickaxe" )
	end
} )

MINE:AddCraft( "Emerald Pickaxe", {
	Model = "models/weapons/w_stone_pickaxe.mdl", -- model
	Color = Color( 0, 255, 0, 255 ), -- color
	Materials = {  ["Coal"] = 3,["Iron"] = 4, [ "Emerald" ] = 4 }, -- materials needed to craft this pickaxe!
	Desc = "This emerald pickaxe has a 30% of successfully mining ore", -- description
	PreSpawn = function( ply, data ) -- leave this
		ply:Give( "emerald_pickaxe" )
	end
} )

MINE:AddCraft( "Sapphire Pickaxe", {
    Model = "models/weapons/w_stone_pickaxe.mdl", -- model
    Color = Color( 20, 20, 255, 255 ), -- color
    Materials = {  ["Coal"] = 3, ["Iron"] = 4, [ "Sapphire" ] = 4 }, -- materials needed to craft this pickaxe!
    Desc = "This sapphire pickaxe has a 35% of successfully mining ore", -- description
    PreSpawn = function( ply, data ) -- leave this
        ply:Give( "sapphire_pickaxe" )
    end
} )

MINE:AddCraft( "Ruby Pickaxe", {
    Model = "models/weapons/w_stone_pickaxe.mdl", -- model
    Color = Color( 255, 0, 0, 255 ), -- color
    Materials = {  ["Coal"] = 3, ["Iron"] = 4, [ "Ruby" ] = 5 }, -- materials needed to craft this pickaxe!
    Desc = "This ruby pickaxe has a 40% of successfully mining ore", -- description
    PreSpawn = function( ply, data ) -- leave this
        ply:Give( "ruby_pickaxe" )
    end
} )

MINE:AddCraft( "Diamond Pickaxe", {
    Model = "models/weapons/w_stone_pickaxe.mdl", -- model
    Color = Color( 200, 200, 255, 255 ), -- color
    Materials = {  ["Coal"] = 3, ["Iron"] = 4, [ "Diamond" ] = 5 }, -- materials needed to craft this pickaxe!
    Desc = "This diamond pickaxe has a 45% of successfully mining ore", -- description
    PreSpawn = function( ply, data ) -- leave this
        ply:Give( "diamond_pickaxe" )
    end
} )

-- MINE:CreatePickaxe( name of pickaxe, class of pickaxe, chance of pickaxe being successfull out of 100 )

MINE:CreatePickaxe( "Diamond Pickaxe", "diamond_pickaxe", 45 )
MINE:CreatePickaxe( "Ruby Pickaxe", "ruby_pickaxe", 40 )
MINE:CreatePickaxe( "Sapphire Pickaxe", "sapphire_pickaxe", 35 )
MINE:CreatePickaxe( "Emerald Pickaxe", "emerald_pickaxe", 30 )
MINE:CreatePickaxe( "Gold Pickaxe", "gold_pickaxe", 25 )
MINE:CreatePickaxe( "Iron Pickaxe", "iron_pickaxe", 20 )

-- grab the position by using getpos in console, it will give you a position and a angle
-- MINE.AddSpawn( map, position, model, angle )
-- MINE.AddNPC( map, position, model, angle )

MINE:AddSpawn( "gm_construct", Vector( 0, 100, 0 ), "models/props/cs_militia/militiarock05.mdl" )
MINE:AddSpawn( "gm_construct", Vector( 0, 300, 0 ), "models/props_wasteland/rockcliff01b.mdl" )

MINE:AddNPC( "gm_construct", Vector( 0, 0, 0 ), "models/monk.mdl", Angle( 90, 0, 0 ) )

MINE:AddSpawn( "rp_evocity_v2d", Vector( 1663.469238, 13634.670898, 162.197281 ), "models/props_wasteland/rockcliff01b.mdl" )
MINE:AddSpawn( "rp_evocity_v2d", Vector( 2000.518066, 13806.448242, 167.099823 ), "models/props_wasteland/rockcliff01b.mdl" )
MINE:AddSpawn( "rp_evocity_v2d", Vector( 1666.522949, 13196.161133, 162.031250 ), "models/props_wasteland/rockcliff01b.mdl" )
MINE:AddSpawn( "rp_evocity_v2d", Vector( 1256.607910, 13021.322266, 162.448547 ), "models/props_wasteland/rockcliff01b.mdl" )

MINE:AddNPC( "rp_evocity_v2d", Vector( 1064.001587, 14413.899414, 193.039978 ), "models/alyx.mdl", Angle( 0, 270, 0 ) )

MINE:AddSpawn( "rp_bangclaw", Vector( 9432.061523, -1018.367188, 128.031250 ), "models/props_wasteland/rockcliff01b.mdl" )
MINE:AddSpawn( "rp_bangclaw", Vector( 9931.095703, -1365.657104, 128.031250 ), "models/props_wasteland/rockcliff01b.mdl" )
MINE:AddSpawn( "rp_bangclaw", Vector( 9348.874023, -1638.944336, 128.031250 ), "models/props_wasteland/rockcliff01b.mdl" )

MINE:AddNPC( "rp_bangclaw", Vector( 9178.022461, -2453.659424, 128.031250 ), "models/alyx.mdl", Angle( 0, 180, 0 ) )

hook.Add("Initialize", "DefaultPickaxeChanceSuccess", function()
	weapons.Get( "bronze_pickaxe" ).Chance = 25
end)