-- DEVELOPED FULLY BY BLOODWAVE - http:--steamcommunity.com/id/bloodwave
-- YOU'RE NOT ENTITLED TO DISTRUBUTE THIS ADDON, THIS IS PRIVATE
-- These are the functions this addon uses
-- You typically won't touch this unless you're a scripter
-- If you're lost, the friendly config file is @ mine/config.lua

for name, data in pairs( MINE.Ores ) do
	MINE:CreateOre( name, data )
end

if ( CLIENT ) then 
	hook.Add( "HUDPaint", "Mine:DrawOres", function()
		local ply = LocalPlayer()
		local ent = ply:GetEyeTrace().Entity
		
		if ( IsValid( ent ) ) then
			if ( string.Left( ent:GetClass(), 4 ) == "ore_" ) then
				local pos = ent:GetPos():ToScreen()
				draw.SimpleText( ent.Ore, "Mine18", pos.x, pos.y, color_white, 1, 1 )
				draw.SimpleText( "$"..ent.Price, "Mine18", pos.x, pos.y+20, color_white, 1, 1 )
			elseif( ent:GetClass() == "mineral_ore" ) then
				local pos = ent:GetPos():ToScreen()
				draw.SimpleText( "Mineral Ore", "Mine18", pos.x, pos.y, color_white, 1, 1 )
				draw.SimpleText( "Pickaxe", "Mine18", pos.x, pos.y+20, color_white, 1, 1 )
			end
		end
	end )
	
	return
end

hook.Add("InitPostEntity", "Mine:SpawnNPC", function()
	local data = MINE.NPC[ string.lower(game.GetMap()):lower() ] or {}
	
	for k,v in pairs( data ) do
		local npc = ents.Create( "mine_npc" )
		npc:SetPos( v.pos )
		
		if ( v.ang ) then
			npc:SetAngles( v.ang )
		end
		
		npc:SetModel( v.model )
		
		npc:Spawn()
		npc:Activate()
	end
	
	local data = MINE.MineralOreSpawn[ string.lower(game.GetMap()):lower() ] or {}
	
	for k,v in pairs( data ) do
		local ent = ents.Create( "mineral_ore" )
		ent:SetModel( v.model )
		ent:SetPos( v.pos )
		
		if ( v.ang ) then
			ent:SetAngles( v.ang )
		else
			ent:SetAngles( Angle( 0, math.random( -360, 360 ), 0 ) )
		end
		
		ent:Spawn()
	end
end)