-- DEVELOPED FULLY BY BLOODWAVE - http:--steamcommunity.com/id/bloodwave
-- YOU'RE NOT ENTITLED TO DISTRUBUTE THIS ADDON, THIS IS PRIVATE
-- These are the functions this addon uses
-- You typically won't touch this unless you're a scripter
-- If you're lost, the friendly config file is @ mine/config.lua

local EMeta = FindMetaTable( "Entity" )

function EMeta:MineFadeoutKill()
	local str = "fd_"..self:EntIndex()

	if ( !timer.Exists( str ) ) then return end
	
	timer.Destroy( str )
	
	self.MineFading = nil
	self:SetColor( color_white )
	self:SetRenderMode( RENDERMODE_NORMAL )
end

function EMeta:MineFadeout( cb )	
	local str = "fd_"..self:EntIndex()

	if ( timer.Exists( str ) ) then return end
	
	self:SetCollisionGroup( COLLISION_GROUP_WORLD )
	self.MineFading = true
	
	timer.Create( str, .01, 0, function()
		if ( !IsValid( self ) ) then
			timer.Destroy( str )
			return
		end
		
		local col = self:GetColor()
		local alp = col.a - 1
		
		self:SetColor( Color( col.r, col.g, col.b, alp ) )
		self:SetRenderMode( RENDERMODE_TRANSALPHA )
	
		if ( alp <= 0 ) then
			self:Remove()
			
			if ( cb ) then
				cb()
			end	
		end
	end )
end

local PMeta = FindMetaTable( "Player" )

function PMeta:MineTraceLine( dis )
	local pos = self:GetShootPos()
	
	local tr = util.TraceLine({
		start = pos,
		endpos = pos + ( self:GetAimVector() * dis ), 
		filter = { self },
	})
	
	return tr
end

-- THIS WORKS WITH DEFAULT DARKRP - VERIFIED
-- THIS SHOULD WORK WITH BASEWARS - SOMEWHAT VERIFIED

function MINE:ProcessMoney( ply, money )
	ply:addMoney( money )
end

-- should the ore be allowed to fade away and dissappear? ( default darkrp keeps the entitie, so this had to be put in place )
function MINE:ShouldFade( ent )
	return ( not ent.OldCollisionGroup )
end

function MINE:AddItem( ply, name ) -- hacky, but we gotta do what we gotta do
	local data = self.Craftable[ name ]
	
	if ( data.PreSpawn ) then
		data.PreSpawn( ply, data )
		return
	end

	local ent = ents.Create( data.Ent )
	ent:SetPos( ply:MineTraceLine( 70 ).HitPos )

	if ( MINE.Craftable[ name ].OnSpawn ) then
		MINE.Craftable[ name ].OnSpawn( data, ent, ply )
	end
	
	ent:Spawn()
end

-- counts how many of the item is in your inventory, works for default darkrp.
function MINE:GetItemCount( ply, name )
	local amt = 0 
	
	name = string.lower( "ore_"..name )

    local number = ply:inventory_count_item( name )

	--for k, v in pairs( ply:getPocketItems() ) do
	--	if ( v.class == "ore_"..name ) then
	--		amt = amt + 1
	--	end
	--end
	
	return (number or 0)
end

-- this checks to see if they have the item in whatever inventory mod you have, this works for default darkrp pocket
function MINE:RemoveItem( ply, name, amt )
	name = string.lower( name )

	if ( self:GetItemCount( ply, name ) < amt ) then
		return false
	end

    return ply:inventory_remove_item( "ore_"..name, amt )

end

-- if you don't want the ores to spawn, you can make it place in there inventory.
-- this will just spawn it
function MINE:HandleOre( ore, ply, tr )
	local ore = ents.Create( "ore_"..string.lower(ore) )
	ore:SetPos( tr.HitPos - ( ply:GetAngles():Forward() * 20 ) )
	ore:Spawn()
    local xp = tonumber(ore.Price) or 5
    ply:addXP((xp*1), true)
end