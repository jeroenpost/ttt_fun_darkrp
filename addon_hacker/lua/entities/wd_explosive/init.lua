AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")


function ENT:Initialize()
	self:SetModel("models/props_c17/SuitCase001a.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self.EntHealth = 100
	local phys = self:GetPhysicsObject()
	phys:Wake()
end

function ENT:Explode()
	self:EmitSound("weapons/c4/c4_beep1.wav", 100, 100)
	timer.Simple(2, function()

		if not self:IsValid() then return end

		local explode = ents.Create( "env_explosion" )
		explode:SetPos( self:GetPos() )
		explode:Spawn()
		explode:SetKeyValue( "iMagnitude", "250" )
		explode:Fire( "Explode", 0, 0 )
		explode:EmitSound( "weapon_AWP.Single", 400, 400 )

		self:Remove()
	end)
end

function ENT:OnTakeDamage(dmg)
	self:TakePhysicsDamage(dmg)

	self.EntHealth = math.Clamp(self.EntHealth - dmg:GetDamage(), 0, 200)

	local vPoint = self:GetPos()
	local effectdata = EffectData()
	effectdata:SetStart(vPoint)
	effectdata:SetOrigin(vPoint)
	effectdata:SetScale(1)
	util.Effect("StunstickImpact", effectdata)

	if(self.EntHealth == 0) then
		local explode = ents.Create( "env_explosion" )
		explode:SetPos( self:GetPos() )
		explode:Spawn()
		explode:SetKeyValue( "iMagnitude", "250" )
		explode:Fire( "Explode", 0, 0 )
		explode:EmitSound( "weapon_AWP.Single", 400, 400 )

		self:Remove()
	end
end