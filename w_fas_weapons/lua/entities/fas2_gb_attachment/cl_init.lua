include("shared.lua")

function ENT:Initialize()
end

function ENT:Draw()
	self:DrawModel()
end

function ENT:Think()
end

hook.Add("PostDrawOpaqueRenderables", "GbAttachment3D", function()
    for _, ent in pairs (ents.FindByClass("fas2_gb_attachment")) do
        if ent:GetPos():Distance(LocalPlayer():GetPos()) < 150 then
            local Ang = ent:GetAngles()

            Ang:RotateAroundAxis( Ang:Forward(), 90)
            Ang:RotateAroundAxis( Ang:Right(), -90)

           -- if not ent.nwname then
                ent.nwname = ent:GetNWString("PrintName", "")
           -- end

            cam.Start3D2D(ent:GetPos()+ent:GetUp()*18, Ang, 0.35)
            draw.SimpleTextOutlined( ent.nwname, "GB_entname", 0, 0, Color( 255, 60, 60, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP, 1, Color(0, 0, 0, 255))
            cam.End3D2D()
        end
    end
end)
